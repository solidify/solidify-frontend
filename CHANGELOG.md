# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [5.5.9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.5.8...v5.5.9) (2025-02-24)


### Features

* allow href navigation from button toolbar ([b62bd8c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b62bd8cabd6dd2ebbe788017fe1e06c74faa7e52))

### [5.5.8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.5.7...v5.5.8) (2025-02-20)


### Features

* add interceptor to allow to force deserialization of http error ([ae05ec8](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ae05ec8ef29c245503533a6974a89eb6c62d089b))


### Bug Fixes

* avoid to display error when twitter is blocked by extension ([2e82055](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2e82055897996aeb1d85572c7a55fe9baf2979dd))

### [5.5.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.5.6...v5.5.7) (2025-02-20)


### Bug Fixes

* **multi-relation-2-tiers:** allow to delete only a specific association ([609af54](https://gitlab.unige.ch/solidify/solidify-frontend/commit/609af54167a7ad5b53fee87c574938fbd1797701))

### [5.5.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.5.5...v5.5.6) (2025-02-19)


### Features

* allow to configure frequency of app status update available ([685d379](https://gitlab.unige.ch/solidify/solidify-frontend/commit/685d379a30fb8a4e24f0f91879a335e61175c278))


### Bug Fixes

* **error-handler:** display error from backend when content type is text ([1cc3982](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1cc3982bfea05b5666b43fe78e7e86c528c95390))

### [5.5.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.5.4...v5.5.5) (2025-02-11)


### Bug Fixes

* improve forbidden values validator to allow to case insensitive ([9fd7bd0](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9fd7bd0ef37d540ddaf6054ddc44ba3d60e4a0f2))
* prevent error logged when release project with pnpm ([b20744f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b20744f9686c4a45ed9614722fcd903b9f186468))

### [5.5.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.5.3...v5.5.4) (2025-02-04)


### Bug Fixes

* **keyword-input:** avoid to make form as dirty if blur field without change inside ([15c813a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/15c813ae34ce2b89d080db40b1707aa5718cccfc))

### [5.5.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.5.2...v5.5.3) (2025-02-04)


### Features

* allow to persist last citation selected ([83fcc8c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/83fcc8c6edad9372f6de2c799456ecfed79a1696))

### [5.5.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.5.1...v5.5.2) (2025-02-03)


### Bug Fixes

* able to download multiples resources in parallel ([355d6e2](https://gitlab.unige.ch/solidify/solidify-frontend/commit/355d6e2d51320340d923281056e30fd886912e7e))
* keyword input component marks form as pristine for each change ([2e57340](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2e57340c03c0a097ffd649da17e123fb18b07fb6))

### [5.5.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.5.0...v5.5.1) (2025-01-23)


### Bug Fixes

* **file-drop-zone:** exclude zero-byte files in filter ([77c4a0a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/77c4a0ad34a69786def08cc3d924228211162478))

## [5.5.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.4.4...v5.5.0) (2025-01-23)


### ⚠ BREAKING CHANGES

* **file-upload:** fileDropped output send File[] instead of FileList

### Features

* **file-upload:** filter directory and allow to filter by extensions ([3e0e4a6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3e0e4a6ad3d3cb8e1995c8a0c38fff5eda6c2862))

### [5.4.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.4.3...v5.4.4) (2025-01-14)


### Bug Fixes

* **orcid:** retrieve orcid conf form backend ([4964ec0](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4964ec097a57edad4fdaf61946e70befc11edc84))

### [5.4.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.4.2...v5.4.3) (2025-01-09)

### [5.4.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.4.1...v5.4.2) (2025-01-09)


### Features

* **datatable:** allow to pre-select item in list ([7d875ec](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7d875ec03b2ab8e33d377e409cc8c7793af80a75))


### Bug Fixes

* manage correctly date format when culture is missing on supported language ([cc59d3c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/cc59d3c11f9e6e90f06a25c0dd99d86295b98823))

### [5.4.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.4.0...v5.4.1) (2024-12-23)

## [5.4.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.3.6...v5.4.0) (2024-12-20)


### ⚠ BREAKING CHANGES

* in AppState thant extends SolidifyAppState, add `@Inject(LOCALE_ID) protected readonly _locale: string` as first param

### Features

* manage local different from default language manage by application ([aae8562](https://gitlab.unige.ch/solidify/solidify-frontend/commit/aae856286da112b4e39be609d74dafdd5fd0af78))

### [5.3.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.3.5...v5.3.6) (2024-12-17)

### [5.3.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.3.4...v5.3.5) (2024-12-17)


### Features

* add state multi relation 2 tiers ([2514b4b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2514b4b7d92ed8f3b2ef4f42187113e3e8fa677c))

### [5.3.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.3.3...v5.3.4) (2024-12-05)


### Features

* improve file drop zone directive ([5aefe19](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5aefe19da6ecb791cf7c039cf7e2738dd77a1729))

### [5.3.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.3.2...v5.3.3) (2024-12-04)

### [5.3.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.3.1...v5.3.2) (2024-12-03)


### Features

* **csv:** add preview for csv ([ae7a78c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ae7a78c72ab3aaa4dab591dbb89b7b0aed718f3f))
* **xlsx:** improve preview for xlsx by allowing to scroll and allow to display all sheets ([851fc28](https://gitlab.unige.ch/solidify/solidify-frontend/commit/851fc28d9c0cae20f9cfaf3d668e50017da84de0))


### Bug Fixes

* remove restriction on docx and xlsx preview to match extension, content type and pronomId in same time ([391708e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/391708e4a910afc37fc36a032955d3518b841fad))

### [5.3.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.3.0...v5.3.1) (2024-11-25)


### Bug Fixes

* preview for xlsx and docx ([3b8504a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3b8504aeb3abd9b5ead62a34c45ef89354647680))

## [5.3.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.2.7...v5.3.0) (2024-11-22)


### Features

* **datatable:** allow to use navigate link instead of button on datatable ([9dbc397](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9dbc39764ca1c19f1ea3fff29742635960c952f9))

### [5.2.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.2.6...v5.2.7) (2024-11-21)


### Features

* add orcid service ([7ed420d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7ed420d547fd618b96e4d783d718ae66c7ea1c3f))


### Bug Fixes

* lint error ([8a26325](https://gitlab.unige.ch/solidify/solidify-frontend/commit/8a263253cd3a05fab04baa1efc0cfc2ee0d4f9fc))

### [5.2.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.2.5...v5.2.6) (2024-11-20)


### Features

* add markdown preview ([afdd14d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/afdd14dd515de7edd05bd3d2f42200e931b41f15))


### Bug Fixes

* allow to copy text inside chip ([13dd251](https://gitlab.unige.ch/solidify/solidify-frontend/commit/13dd2511001853566abadcb69ed489d6eb623a93))
* glitch when body content is 100vh, scrollbar appear but should not ([3f887f6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3f887f612fb9ef5699c8a7954157c5c7ba7d67b2))
* **multi-select:** avoid error when press enter when content panel open ([8afd743](https://gitlab.unige.ch/solidify/solidify-frontend/commit/8afd74340d3376e4285176ede71bddd4372ce475))

### [5.2.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.2.4...v5.2.5) (2024-11-08)


### Bug Fixes

* **SSR:** allow only usage of support language for SSR ([37bac94](https://gitlab.unige.ch/solidify/solidify-frontend/commit/37bac94aac8dcd526645f5958ce3579099c7f38e))
* **SSR:** allow to force to regenerate cache ([9193875](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9193875be6f2ee53d78b5c9f6f9b29ed97d1c659))
* **SSR:** return expired cache then ask to regenerate it ([b8e1e3d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b8e1e3d00fc45f2059b6d48aee3520096b1d3893))

### [5.2.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.2.3...v5.2.4) (2024-11-05)


### Bug Fixes

* deprecated sass properties location ([a6bf7ac](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a6bf7ac6528e0ed73b66d06e7aa5e40616d1738e))

### [5.2.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.2.2...v5.2.3) (2024-10-24)


### Bug Fixes

* avoid wrong usage of visualizer between docx, xlsx and pptx due to same mimeType and Puid for this files ([7ab0df1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7ab0df16a755286eeb8367217739874d388a0750))

### [5.2.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.2.1...v5.2.2) (2024-10-24)


### Features

* **preview:** add preview for xlsx file ([69ce3cd](https://gitlab.unige.ch/solidify/solidify-frontend/commit/69ce3cd4339b0c647e08845f0fad6e5e8fec3531))

### [5.2.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.2.0...v5.2.1) (2024-10-23)


### ⚠ BREAKING CHANGES

* use MemoizedUtil instead of base state (ResourceState, CompositionState...)

### Features

* allow to retrieve Signal from ngxs store ([03b6ea6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/03b6ea6cd18af4809ce4b0f71e6d9391b130aaeb))


* remove static selectors method from states to force usage from memoized util ([4f4acd8](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4f4acd888753dd64ca0765226624085e85b574ca))

## [5.2.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.25...v5.2.0) (2024-10-21)


### ⚠ BREAKING CHANGES

* migrate to angular 18

### Features

* **image:** allow to customize fallback image or provide icon on image display component ([6bcbdd2](https://gitlab.unige.ch/solidify/solidify-frontend/commit/6bcbdd20e69e7408826a04e960320da0c28c3cb4))


### Bug Fixes

* **doi:** do not display error when unable to retrieve short doi ([2d24453](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2d2445389bb07dc7d12f957f574037049ed4680f))


* migrate to angular 18 ([74978b8](https://gitlab.unige.ch/solidify/solidify-frontend/commit/74978b87e89a41643cf65db2ca6a60918cd1c59b))

### [5.1.25](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.24...v5.1.25) (2024-10-07)


### Bug Fixes

* **regex:** restore url regex in regex util ([72aa15e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/72aa15ec89c9b1ee4f11502bc0fd176e2bbc2410))

### [5.1.24](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.23...v5.1.24) (2024-10-03)


### Bug Fixes

* add missing stuff on global banner ([b5c46da](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b5c46daf710f62da58bea21f8809c889d3c5d0f8))

### [5.1.23](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.22...v5.1.23) (2024-10-03)

### [5.1.22](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.21...v5.1.22) (2024-09-30)


### Bug Fixes

* **resource file state:** add clean method ([877cf9b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/877cf9b94c3c31d959aace8d4ab4e9480a2f3976))

### [5.1.21](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.20...v5.1.21) (2024-09-27)

### [5.1.20](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.19...v5.1.20) (2024-09-27)


### Bug Fixes

* memory leak with native subscription ([0ab370e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0ab370ed0da47f739ab551b48dbd737d32cd579d))

### [5.1.19](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.18...v5.1.19) (2024-09-26)


### Bug Fixes

* **dialog:** restore position of focus after dialog is closed even if there is no subscription on the result of this method ([3a33102](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3a3310225f88388ca313039758ee185f7889360b))

### [5.1.18](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.17...v5.1.18) (2024-09-25)


### Bug Fixes

* **style:** avoid all links to blink when one is hover ([55f922e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/55f922ec4b02f0571c163196c4455d9defdbde15))

### [5.1.17](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.16...v5.1.17) (2024-09-24)

### [5.1.16](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.15...v5.1.16) (2024-09-19)


### Bug Fixes

* **dialog:** restore focus position after closing dialog ([a0a478f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a0a478fa024032950fb2b9749e1effb6068d8aba))

### [5.1.15](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.14...v5.1.15) (2024-09-17)


### Bug Fixes

* **datatable:** avoid glitch on draggable placeholder in header when sticky ([dcdf89e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/dcdf89ecfe290ec60812d7e96cfad039fea8d8d8))

### [5.1.14](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.13...v5.1.14) (2024-09-17)


### Features

* allow method post to download files in memory ([fa3dce8](https://gitlab.unige.ch/solidify/solidify-frontend/commit/fa3dce8cc7c80ddfe1a37425e3f2720c230db90f))

### [5.1.13](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.12...v5.1.13) (2024-09-16)


### Bug Fixes

* **url-input-navigation:** display error when detected on form ([b0b4226](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b0b42266cd5ea393df7411e5bf6a9764abffe9bd))

### [5.1.12](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.11...v5.1.12) (2024-09-11)


### Bug Fixes

* **datatable:** missing filters values when open datatable with values on query parameters ([132450b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/132450beb0b0178d433b9064db6c2cc157e5d214))

### [5.1.11](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.10...v5.1.11) (2024-09-09)


### Bug Fixes

* **rich text editor:** allow to work with cdkDrag ([7d5555a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7d5555ad21b486db3b51c3de700fd60897ec9075))

### [5.1.10](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.9...v5.1.10) (2024-08-30)


### Bug Fixes

* **orcid:** manage case when redirect with error ([fb76970](https://gitlab.unige.ch/solidify/solidify-frontend/commit/fb76970f107cf5407e8936f92594816b29e1d574))

### [5.1.9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.8...v5.1.9) (2024-08-30)


### Bug Fixes

* **facet:** inner html on facet helper description ([2cdfdf9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2cdfdf9cac671b9998d7eb2cb954bec11e98061a))

### [5.1.8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.7...v5.1.8) (2024-08-30)


### Bug Fixes

* **facet:** manage case where no translation are available ([0d43ba1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0d43ba10434546965e50221c0a43c5d3142f9ac9))

### [5.1.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.6...v5.1.7) (2024-08-29)


### Features

* **facet:** display question mark on facet list when description is available ([1c310a2](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1c310a2037af7230359eff991ecf0f94724f43f4))


### Bug Fixes

* add success style to mat button ([7644748](https://gitlab.unige.ch/solidify/solidify-frontend/commit/76447488f39d1ca6805d9b09952318c07d700a07))

### [5.1.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.5...v5.1.6) (2024-08-28)


### Features

* **OaiMetadataPrefix:** support XML class field ([59dc4be](https://gitlab.unige.ch/solidify/solidify-frontend/commit/59dc4be448a146e50fb54efad262c63f5a40f654))
* **orcid:** add redirect and orcid button ([f857064](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f857064a5a993f6c8aefe00e6fe317eb4e8e73e8))

### [5.1.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.4...v5.1.5) (2024-08-15)


### Features

* new method in UrlQueryParamHelper to check if a query parameter value exists in the current url ([9139c6c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9139c6c4373401be3f214a06cd9b1b4ab9ea43a7))

### [5.1.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.3...v5.1.4) (2024-07-30)


### Features

* **form:** open automatically sections collapsed when form control with error ([e111e21](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e111e2115d4b44d545c1c9ac2d5c80b7479dc0b3))

### [5.1.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.2...v5.1.3) (2024-07-24)

### [5.1.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.1...v5.1.2) (2024-07-17)


### Bug Fixes

* replace format number pipe to use default angular decimal pipe instead and allow to filter size column ([1205e01](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1205e0170ef9b9c1937ae728c7a4af1828dcccaa))

### [5.1.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.1.0...v5.1.1) (2024-07-17)

## [5.1.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.25...v5.1.0) (2024-07-16)


### ⚠ BREAKING CHANGES

* update-package-version-param.script.ts was remove. Please see package.json boilerplate to use new method for release

* add new ts-node script to generate release and support next iteration ([7ca203d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7ca203daa359c5bd409344274fbc7c46aaf2d057))

### [5.0.25](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.24...v5.0.25) (2024-07-15)


### Features

* allow to format number with thousands separator and limit decimal ([495bb6b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/495bb6b6696866cf8d18cc5ae60905d8b4b793f7))

### [5.0.24](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.23...v5.0.24) (2024-07-11)


### Bug Fixes

* position of mat tooltip ([2b6dd18](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2b6dd18a348cc865ad159cdc9f927aec5594720c))

### [5.0.23](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.22...v5.0.23) (2024-07-11)


### Bug Fixes

* position of mat tooltip ([92847ed](https://gitlab.unige.ch/solidify/solidify-frontend/commit/92847ed1f6765d4c03dfb66a22bda20ae3f1aca7))

### [5.0.22](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.21...v5.0.22) (2024-07-11)


### Features

* **ssr:** allow to use list of resource id to evict cache in cache message ([949b59c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/949b59cc1166dddb0119f132574fdcd79cdd3b65))

### [5.0.21](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.20...v5.0.21) (2024-07-09)


### Bug Fixes

* **file-visualizer:** display correctly error message due to size exceeded ([f2e1d0c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f2e1d0cb5047dbfb54ee1ae0c6ca6d72e0ea48f3))

### [5.0.20](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.19...v5.0.20) (2024-07-03)


### Features

* **array util:** add method to sort using callback ([60d164b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/60d164b83d25975be27451ab3c3687d026fc9f88))
* **data table:** sort filter value alphabetically ([d48df66](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d48df66dd44048d4a91ed9cee144b5ea02502232))

### [5.0.19](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.18...v5.0.19) (2024-07-03)


### Features

* **array util:** add extract method ([cf7965a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/cf7965ae5c13eb79812f4333da1b1450ced7ca99))

### [5.0.18](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.17...v5.0.18) (2024-07-02)


### Features

* add new is tools for empty object ([8cbe2c4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/8cbe2c488864e3678911b5a1bedb746d90e6a534))
* **color:** add method to add alpha to a color ([9d8b412](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9d8b412abd08fde8a83723f048fdcbbeb15b16d5))

### [5.0.17](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.16...v5.0.17) (2024-06-27)


### Bug Fixes

* **overlay:** prevent overlay still open when cursor start on <a> tag ([0e97a12](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0e97a12a09572963da197deaa7e23425490556fa))

### [5.0.16](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.15...v5.0.16) (2024-06-12)


### Features

* **rich-text-editor:** by default use tab to switch between input ([a745709](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a74570996603a1d7c60e02d473e1318f30d8e20b))


### Bug Fixes

* **rich-text-editor:** avoid build error due to Quill typing ([55d4965](https://gitlab.unige.ch/solidify/solidify-frontend/commit/55d49654a75394161923b828ed8b8277758b23b7))

### [5.0.15](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.14...v5.0.15) (2024-06-11)

### [5.0.14](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.13...v5.0.14) (2024-05-27)


### Bug Fixes

* ssr with migration to angular domino and add script to patch ssr cache to reuse it ([c6b6cb6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c6b6cb69bc94f8d06212b01be5a07d3eab99e9f8))

### [5.0.13](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.12...v5.0.13) (2024-05-23)


### Bug Fixes

* manage navigator back action on redirect path ([fba2673](https://gitlab.unige.ch/solidify/solidify-frontend/commit/fba26734d5558797c265d4359df08382f5c75469))

### [5.0.12](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.11...v5.0.12) (2024-05-22)


### Features

* **router:** allow to force refresh current component on navigate ([343c14c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/343c14ca9f675499ba1f91b84e62d4de0519def1))

### [5.0.11](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.10...v5.0.11) (2024-05-21)


### Features

* **ssr:** add active MQ stomp error log ([83a143b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/83a143b4aed0447fafaae2aef411bd502421aa25))
* **ssr:** improve boilerplate for ssr ([4fef6b0](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4fef6b0471ae326f9ecc449911d3306986d9ad2c))


### Bug Fixes

* ssr render after angular 17 update ([4a7f994](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4a7f994996586c6638676d3ccd886f09805b9780))

### [5.0.10](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.9...v5.0.10) (2024-05-17)


### Features

* **code editor:** allow to copy content ([e4eafaa](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e4eafaa0b6af0243c26aa1a98a6e670538408245))


### Bug Fixes

* store all cookie on root path ([1a05876](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1a058769c07e102455f6e9648f8c48232315606d))
* use / as default cookie path when base href is missing ([400c18a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/400c18a371c3afb5b329ac8c24445d1fbe91975e))

### [5.0.9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.8...v5.0.9) (2024-05-16)

### [5.0.8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.7...v5.0.8) (2024-05-16)

### [5.0.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.6...v5.0.7) (2024-05-15)


### Bug Fixes

* display error correctly on snackbar ([663cb89](https://gitlab.unige.ch/solidify/solidify-frontend/commit/663cb89ae297ff44c37185021dd2cf950e8fbc26))

### [5.0.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.5...v5.0.6) (2024-05-15)


### Features

* **confirm dialog:** display message with line return ([c92ad75](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c92ad7585db53f79df432926609f40929bffdd1e))
* **store util:** allow to exec parallel and sequential dispatch without timeout ([03e5e2d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/03e5e2d4d72b3778dd2238294198dcda5ddb357f))

### [5.0.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.4...v5.0.5) (2024-05-14)


### Bug Fixes

* **store util:** improve dispatch sequential and parallel action ([685f5f1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/685f5f152ddf4080800bd703a3e6a72c2edf923b))

### [5.0.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.3...v5.0.4) (2024-05-08)


### Bug Fixes

* **multi select:** avoid mdc chip error ([2b28289](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2b28289d28dd136ee2136660c538cf894cbc3331))

### [5.0.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.2...v5.0.3) (2024-05-06)


### Bug Fixes

* add material override rule for mat form field ([8e3ec83](https://gitlab.unige.ch/solidify/solidify-frontend/commit/8e3ec835feb1025a7be3877710a41a424ea15b87))

### [5.0.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.1...v5.0.2) (2024-05-03)


### Features

* allow to retrieve all validation errors ([dccc70f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/dccc70fd4b86bdad399d1d21ea42e021b5246460))

### [5.0.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0...v5.0.1) (2024-04-29)


### Bug Fixes

* **paginator:** when place in form, avoid to enter in edit mode when click on page size selector ([1bea445](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1bea445541bb30b81f05939b73abef9dac745592))

## [5.0.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta71...v5.0.0) (2024-04-23)


### Features

* **data table:** allow to display actions in line also in wrapped action button ([196813d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/196813d807475f2521b1839f67e5fc1370d3a209))

## [5.0.0-beta71](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta70...v5.0.0-beta71) (2024-04-18)


### Bug Fixes

* **datatable:** avoid error on datatable without field ([11d39aa](https://gitlab.unige.ch/solidify/solidify-frontend/commit/11d39aa7f984177663dd5080abf1246fdd8bb4fd))

## [5.0.0-beta70](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta69...v5.0.0-beta70) (2024-04-18)


### Bug Fixes

* **datatable:** clean correctly filter with button remove all filters ([0fb7736](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0fb77364154b4f2501cba8c4e32b9939652573d6))
* **download service:** allow to provide custom download token endpoint ([198463d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/198463ddc30b1700a60eefae31809d6aa456bef5))

## [5.0.0-beta69](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta68...v5.0.0-beta69) (2024-04-18)

## [5.0.0-beta68](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta67...v5.0.0-beta68) (2024-04-17)


### Bug Fixes

* **text file visualizer:** use array buffer to avoid interface freeze ([4caf0e9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4caf0e923b6092c63d51339553dea4f42a0b42ec))

## [5.0.0-beta67](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta66...v5.0.0-beta67) (2024-04-12)


### Bug Fixes

* **text preview:** base highlight size limit for preview on extension instead of highlight languages ([a09ebb5](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a09ebb50d7601cd34ce4d94e7828007c9a2ae98e))

## [5.0.0-beta66](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta65...v5.0.0-beta66) (2024-04-12)


### Features

* **text visualizer:** allow to disable highlight depending on file size ([9df9d83](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9df9d83ba53b26a7d89dbaa23de365e92a0e46e5))

## [5.0.0-beta65](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta64...v5.0.0-beta65) (2024-04-03)

## [5.0.0-beta64](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta63...v5.0.0-beta64) (2024-04-02)

## [5.0.0-beta63](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta62...v5.0.0-beta63) (2024-03-26)


### Bug Fixes

* **resource role container:** prevent scroll to top when paginate ([f9baf1f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f9baf1fb3210bfdf23226ab88fd81864c828d0b0))

## [5.0.0-beta62](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta61...v5.0.0-beta62) (2024-03-26)


### Features

* **datatable:** allow pagination in memory, allow to custom display of data cell value and allow to use string data list ([8eadf5a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/8eadf5a585b77e5dfad09e88ff454ad12c41750e))


### Bug Fixes

* **carousel:** prevent browser cache ([f76bd6b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f76bd6bba12e8a0163580f113f4b577158018ec5))
* test ([cded3d9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/cded3d9e046107f26a07038127c6862c2fc05de9))

## [5.0.0-beta61](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta60...v5.0.0-beta61) (2024-03-19)


### Bug Fixes

* **array url:** avoid typing error ([9cd5c69](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9cd5c69154608882c269cbc87add67f43e9b1932))

## [5.0.0-beta60](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta59...v5.0.0-beta60) (2024-03-18)

## [5.0.0-beta59](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta58...v5.0.0-beta59) (2024-03-14)

## [5.0.0-beta58](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta57...v5.0.0-beta58) (2024-03-14)


### Features

* add code editor component ([6949fc1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/6949fc121cfd54fae6f5152fed7d8debf9e4c5e6))

## [5.0.0-beta57](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta56...v5.0.0-beta57) (2024-03-07)


### Features

* **text visualizer:** allow to manage list of language to enable highlight ([da43747](https://gitlab.unige.ch/solidify/solidify-frontend/commit/da43747a7ac63687cebd463f6f95d3298623e780))


### Bug Fixes

* display untreated validation errors in error handler ([05e3ac1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/05e3ac1f5e7356ee0e7ea675747992e415893af4))
* **home:** avoid null pointer exception when current folder is undefined ([fb6fcb6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/fb6fcb6e6a4104d88ee4871129a34ffc8f2d8770))

## [5.0.0-beta56](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta55...v5.0.0-beta56) (2024-03-04)


### Bug Fixes

* catch error from validation errors when uploading a file ([99e430f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/99e430ffca7fa28aba82b6673de608cf5d347f4e))

## [5.0.0-beta55](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta54...v5.0.0-beta55) (2024-03-04)


### Bug Fixes

* avoid error on state when environment is undefined ([3b9c336](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3b9c336c5233ff521589f98169fbb41e2c52436f))

## [5.0.0-beta54](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta53...v5.0.0-beta54) (2024-03-04)


### Features

* **global banner:** add admin module for manage global banner ([678af0c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/678af0c9e3ee64718f03a1919641b55d792d5716))

## [5.0.0-beta53](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta52...v5.0.0-beta53) (2024-02-23)


### Bug Fixes

* **admin:** oai set, oai pmh and index field alias detail view ([3682444](https://gitlab.unige.ch/solidify/solidify-frontend/commit/368244454c8fed7903358ead5e2806ce49f8edb5))

## [5.0.0-beta52](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta51...v5.0.0-beta52) (2024-02-23)


### Features

* add data file upload in progress ([1c94312](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1c943124b4506248928c14024bc23dd316d4129d))

## [5.0.0-beta51](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta50...v5.0.0-beta51) (2024-02-20)


### Features

* **oai:** add oaiPmh provider helper to obtain url ([5b1dafc](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5b1dafc2bb520f5ef7e1789e5a27a716801b1fa7))

## [5.0.0-beta50](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta49...v5.0.0-beta50) (2024-02-20)

## [5.0.0-beta49](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta48...v5.0.0-beta49) (2024-02-20)

## [5.0.0-beta48](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta47...v5.0.0-beta48) (2024-02-19)


### Features

* add resource role component ([56f0a9c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/56f0a9c178ef2015558a76bc35d20f858c21db33))

## [5.0.0-beta47](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta46...v5.0.0-beta47) (2024-02-13)


### ⚠ BREAKING CHANGES

* **ssr:** allow to work on ssr route with custom paramId

### Features

* **index field alias:** add admin module for index field alias ([a9f1826](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a9f18266d7e3ab6594dfcae841337734c3584cbf))


### Bug Fixes

* **folder-tree:** avoid null pointer exception with currentFolder ([147a452](https://gitlab.unige.ch/solidify/solidify-frontend/commit/147a4521f45981d1d547b7f064352c2e294eb378))
* **ssr:** add in cache if no existing cache found and add method to associate ssr route to active mq message ([08d510e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/08d510e28b2e2811f0ea236bd0c169e1a1784001))
* **ssr:** allow callback to retrieve id in cache eviction message and allow to wait a delay to refresh cache after an active mq messages ([37e7a8e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/37e7a8e83ee24d0ba3df2895a1fddee11ea7a120))
* **ssr:** allow to define additional ssr route at runtime ([6d678d7](https://gitlab.unige.ch/solidify/solidify-frontend/commit/6d678d719e5b3fa455313613e363919796f645ad))
* **ssr:** allow to work on ssr route with custom paramId ([0671a40](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0671a4099ca979592f59deb016ad1ca7b1acafa5))
* **ssr:** manage case resource is deleted and allow to not refresh cache after deletion ([b982d3a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b982d3a4a330e274f9a705199b2a7fbcc03a0e58))
* **ssr:** use last modification date instead of creation date for cache expiration ([130497b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/130497bc14faeb32a9b718d40442edf45e476cfb))

## [5.0.0-beta46](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta45...v5.0.0-beta46) (2024-01-31)


### Features

* **upload:** allow to type fileUploadWrapper ([68afae1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/68afae187432a5ffa29ad0d62a0072f2d2b807e0))


### Bug Fixes

* **SSR:** SSR cache eviction method to retrieve custom resourceId ([74d1db8](https://gitlab.unige.ch/solidify/solidify-frontend/commit/74d1db83e4064af6c06dcdafc1cf3d92c983b566))

## [5.0.0-beta45](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta44...v5.0.0-beta45) (2024-01-26)


### Bug Fixes

* compute current folder after a delete ([8b0f9cc](https://gitlab.unige.ch/solidify/solidify-frontend/commit/8b0f9ccbcdb808a1122f983909f7e0fb4c7c0df2))
* **DownloadService:** [AOU-1797] remove non ISO-8859-1 chars from filename when downloading file in memory ([2f58ae0](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2f58ae09be960663da9d7d62173538b00483b661))
* **folder-tree:** remove delete button on root level ([f133455](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f1334551eb4ce73c9b8d106ba908ed920e155f65))

## [5.0.0-beta44](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta43...v5.0.0-beta44) (2024-01-19)


### Bug Fixes

* force to preserve technical cookie for cookie preferences and notify when new cookies are available ([bb421d3](https://gitlab.unige.ch/solidify/solidify-frontend/commit/bb421d35ce7961440cf70d9f833b01d6210f6c40))

## [5.0.0-beta43](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta42...v5.0.0-beta43) (2024-01-18)

## [5.0.0-beta42](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta41...v5.0.0-beta42) (2024-01-18)


### Features

* allow to use SVG as thumbnail ([d9c6349](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d9c6349da6887d69acc0800c6d16bb422cec58f5))

## [5.0.0-beta41](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta40...v5.0.0-beta41) (2024-01-17)


### Features

* add folder tree presentational ([5cfb000](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5cfb000842a18d372e8edc824410e383a85f8e09))

## [5.0.0-beta40](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta39...v5.0.0-beta40) (2024-01-16)


### Features

* enable text preview for file "nt", "sparql", "adoc", "md", "py", "js" and "java" ([a7655a2](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a7655a22148d965211698428747a05d66e578cbd))

## [5.0.0-beta39](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta38...v5.0.0-beta39) (2024-01-12)


### Features

* able to preview files with status COMPLETED and allow to override supported status ([7271ffb](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7271ffb66425d2e5b4463f0d23e65cee8d856b1b))

## [5.0.0-beta38](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta37...v5.0.0-beta38) (2024-01-12)


### Features

* **file:** add file upload tile ([805e6c9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/805e6c98d9ddfb60278f9bbeec7b2ec645153c94))

## [5.0.0-beta37](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta36...v5.0.0-beta37) (2024-01-05)


### Features

* support csv preview as text file ([705438f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/705438f9436e3f6f61e4e1e7247ff5e81cf80160))

## [5.0.0-beta36](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta35...v5.0.0-beta36) (2024-01-04)

## [5.0.0-beta35](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta34...v5.0.0-beta35) (2024-01-03)

## [5.0.0-beta34](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta33...v5.0.0-beta34) (2023-12-22)


### ⚠ BREAKING CHANGES

* project should use angular 17.0

### Features

* add rdfs as a format supported as text ([097991f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/097991fb5a12cac1a04cbb905966bc5f6b9cd2e0))
* **oai:** allow to define short url for oaiPmh ([b00e74e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b00e74ef04ee8ad9bde5dc49e4db53328999f793))
* **preview:** support image with jpg extension ([75121d7](https://gitlab.unige.ch/solidify/solidify-frontend/commit/75121d77d41b4efa79ee3476f4b5c2fd3a644d46))
* update to angular 17 ([3109a61](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3109a61c3499507226f265c082f54ff6643f013b))

## [5.0.0-beta33](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta32...v5.0.0-beta33) (2023-12-14)

## [5.0.0-beta32](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta31...v5.0.0-beta32) (2023-12-11)


### Bug Fixes

* **regex util:** add uri regex ([ebb3871](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ebb387155264182a4dcf4bc55c205abcc88ba929))

## [5.0.0-beta31](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta30...v5.0.0-beta31) (2023-12-11)


### Bug Fixes

* **router extension:** allow to work correctly with angular 16 ([ee72028](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ee72028bb2912f1ccfbde3df954fd586ecd24b41))

## [5.0.0-beta30](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta29...v5.0.0-beta30) (2023-12-07)

## [5.0.0-beta29](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta28...v5.0.0-beta29) (2023-12-07)


### Features

* breadcrumbselector could be also a BaseStateModel ([36c15c1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/36c15c1748f2835fe85e0f48fe428abea7ca5d76))


### Bug Fixes

* color check with angular 16 ([4b10ac7](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4b10ac7bca94e32b5bf5e04ff8822f9d495a4636))
* remove deprecated angular configuration ([886bcd0](https://gitlab.unige.ch/solidify/solidify-frontend/commit/886bcd0e977f70d462273323d736463d25321bae))

## [5.0.0-beta28](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta27...v5.0.0-beta28) (2023-12-04)


### Bug Fixes

* dynamic favicon depending on theme ([88c892e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/88c892e83e3335435c0ecbaf3a50e17f16a45d8c))

## [5.0.0-beta27](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta26...v5.0.0-beta27) (2023-12-04)


### Features

* add page to test app colors ([fd71601](https://gitlab.unige.ch/solidify/solidify-frontend/commit/fd7160127b67f37d03d9434b4b52709e35dca11f))
* **token dialog:** add login mfa button ([6758555](https://gitlab.unige.ch/solidify/solidify-frontend/commit/6758555e0b52a6182519685828369e0536723161))


### Bug Fixes

* logout before mfa login ([ea3b775](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ea3b7753e22bf6374fd644c5cac302bbe47f9eec))

## [5.0.0-beta26](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta25...v5.0.0-beta26) (2023-11-27)


### ⚠ BREAKING CHANGES

* add "_actions$: Actions" in app component

### Features

* Allow to preview RDF/RML files as text file ([6cb9a63](https://gitlab.unige.ch/solidify/solidify-frontend/commit/6cb9a63f523599d68261d40a297e53e044d20d18))
* **keyword input:** allow to disable individually all default delimiters ([cf5394b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/cf5394bd3b79dde497002c8edc97d2f79c4f054b))
* **ssr:** add filesystem logging and refactor to share easily context ([3c14f68](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3c14f68b65a68e6192c7e5df29aacf4b6e70350c))
* **ssr:** add integrity check after generation of cache, and regenerate gzip version when missing ([867f877](https://gitlab.unige.ch/solidify/solidify-frontend/commit/867f8772eaa198f7d92ad8197fcb94786c3f3773))
* **ssr:** add parameters for worker queue ([4376ecd](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4376ecd7b11ef976f76d177f31862af1befd2c20))
* **ssr:** add parameters to manage worker resource, arg and numbers of instance ([3bda23e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3bda23e3bccaf44f6665b76cec5228ef76f23ae2))
* **ssr:** add SSR worker to delegate rendering tasks ([dfc4002](https://gitlab.unige.ch/solidify/solidify-frontend/commit/dfc4002d0062243d927b1d08ea39e0ba01ff3ac3))
* **ssr:** allow to configure max files and max size for log rotation ([128a95e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/128a95e214db85a1715ed23f9182b15deeb8ba19))
* **ssr:** allow to define a custom function to retrieve the id of the resource in the cache message received in activeMQ ([f8a7681](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f8a7681b9e2cf0d628fe49233f47a42065f89913))
* **ssr:** allow to provide activeMQ login and password ([90ec032](https://gitlab.unige.ch/solidify/solidify-frontend/commit/90ec032d6094521ed76258e67f48575365f378fd))
* **ssr:** allow to setup trim size of md5 to generate cache path ([588b6a8](https://gitlab.unige.ch/solidify/solidify-frontend/commit/588b6a85a75dafb85df5a47642fc53b34667b402))
* **ssr:** allow worker to detect if needed to generate cache or not ([ffaefeb](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ffaefeb4757a3ed99edcf690f6fa73698e33e5a2))
* **ssr:** cache entry without culture, allow to store on custom path, allow to define default language ([1fa7e69](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1fa7e691fb39aa31d56c61279fec9cc002fa8ac0))
* **ssr:** manage time to live on cached element ([0054e18](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0054e182ae99f6f7cb3ebde4ea1d8f2825c9d0a0))
* **ssr:** send message in queue to ask to generate cache when cache is evicted ([f4f0940](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f4f094044e7343daf9c4d25c3c923e9359e83abb))
* **ssr:** use thread pool ([1eb88a9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1eb88a9b91bb42ed8c82dd0d2a6a8f825b1924d4))
* update nginx config to return static version ([184dadd](https://gitlab.unige.ch/solidify/solidify-frontend/commit/184dadd721fa5b1c056b2a2da8c3f2c92a53a208))


### Bug Fixes

* allow admin to connect when app is in maintenance mode and consent is not accepted ([d750a04](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d750a04cc3731e68dc047e94fd06e1bb2a4b25bd))
* correct attribute is bootstrap from ([17e2675](https://gitlab.unige.ch/solidify/solidify-frontend/commit/17e26752a046b95bce9db07e2d7770ae8a51ac80))
* retrieve carousel only when component displayed and allow to disable transfer state for carousel and translation ([b975a89](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b975a890e21e87f456f33b12a7097fa3ec3d0f12))
* **ssr:** allow to revoke cache with list of resource id ([12ef281](https://gitlab.unige.ch/solidify/solidify-frontend/commit/12ef281da98cb0370319d4e81162c7851488d996))
* **ssr:** avoid memory leak by instanciate a new window object before each render ([c1e5736](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c1e5736590a695d5e5bdc8f40f4ecf5f3da27eaf))
* **ssr:** avoid multiple call due to multi registred default state actions ([b757814](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b757814c510cceef60e1c9742b9ec1bc8a01babf))
* **ssr:** change the way to determine serverLocation and fix browserLocation ([d91f437](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d91f4379a2b8da88136e214db7a8adba8da6c69d))
* **ssr:** consume only one message on activeMQ queue for cache generation in worker ([5fef859](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5fef859ee993df23a37db7d816f153c3a26f4eff))
* **ssr:** fix log to hide ping pong activeMq log and to display server configuration ([677eae9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/677eae9e1eb7dba6c9788a350361453bbab3472e))
* **ssr:** remove specific char in logs ([b099021](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b0990216e25b5c638951e7fe122e16fd4a761da6))


### refacto

* change way to init application ([a06b8e2](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a06b8e2cccae32aadce0afbd8360c11f67e5b4c0))

## [5.0.0-beta25](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta24...v5.0.0-beta25) (2023-10-31)


### Bug Fixes

* don't cancel action when navigate on edit page ([f486e52](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f486e52355915fd23f39be3c9393bd0c434d06bb))
* truncate with ellipsis the values of the multi-select components ([04fd0e6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/04fd0e646514457196320c726c42fa2ca71f6a1e))

## [5.0.0-beta24](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta23...v5.0.0-beta24) (2023-10-24)


### Bug Fixes

* provide server and browser location ([9d8eb85](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9d8eb85bb26e86d9aeee1374a7e6a0a7fc219c85))

## [5.0.0-beta23](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta22...v5.0.0-beta23) (2023-10-24)


### Features

* enable static gzip compression ([b8b0b11](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b8b0b11be7b7c71573f607a7154e60fd5d94b382))
* **ssr:** manage active mq to cache evict when needed ([bb6e30d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/bb6e30d377ba1fa917b926974b7ae58cc4d7f2b9))


### Bug Fixes

* update boilerplate for gzip and ssr ([196cc68](https://gitlab.unige.ch/solidify/solidify-frontend/commit/196cc68ed3bcc309363832c086f9b86c2852f927))
* **upload image:** set blob name with object assign ([5f38d42](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5f38d42604bec52207af74286059f0811dd3d168))

## [5.0.0-beta22](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta21...v5.0.0-beta22) (2023-10-13)


### Bug Fixes

* disable initial critical css in SSR ([781039f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/781039f6e92c808e47183f77a0d7160d86464fb6))
* do not send oauth token on carousel request ([1001bea](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1001bea50355cf743c861224c27854ef75567a71))
* initialize variables by values injected in constructor ([6fcce22](https://gitlab.unige.ch/solidify/solidify-frontend/commit/6fcce222b7d436409ffd8c1bd24b2c95aab4f4c2))

## [5.0.0-beta21](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta20...v5.0.0-beta21) (2023-10-06)


### Bug Fixes

* application tour ([3a5f1d8](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3a5f1d82ded92326d20ab688c9dc119ad33aa764))

## [5.0.0-beta20](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta19...v5.0.0-beta20) (2023-10-06)


### Bug Fixes

* cancel uncompleted action ([203f3b9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/203f3b913e559b0ba242f353c5e43e59d63a004b))

## [5.0.0-beta19](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta18...v5.0.0-beta19) (2023-10-06)


### ⚠ BREAKING CHANGES

* use input subscriptSizing with 'dynamic' value instead of usage of remove-padding-mat-form-field mixin

### Bug Fixes

* remove remove-padding-mat-form-field mixin ([c3ab94c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c3ab94c3346822a9766d349691de559a7b8e8428))

## [5.0.0-beta18](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta17...v5.0.0-beta18) (2023-10-04)


### Features

* allow to define subscript sizing on input material ([60775fc](https://gitlab.unige.ch/solidify/solidify-frontend/commit/60775fce2c891da7f543f31cfc1e326939f6f97a))
* allow to inject dynamically metas from environment file ([a9ec311](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a9ec311d1fff14c0ecfd56b55b4f9287d3f4ea5b))


### Bug Fixes

* do not retrieve short id in SSR mode ([4b476d0](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4b476d0259de4af20fefb85d47a9d835de92bd8f))
* short doi call with base href ([0ba074e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0ba074e2169411ac042427dd5156fd374ceeb0be))
* update server.ts boilerplate to disable warning in SSR ([c13d57d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c13d57d6d02a41d4145b980bc5457c0eefa4a4be))

## [5.0.0-beta17](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta16...v5.0.0-beta17) (2023-09-29)

## [5.0.0-beta16](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta15...v5.0.0-beta16) (2023-09-29)


### Bug Fixes

* **keyword:** display correctly keyword input when disabled ([0b9fe71](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0b9fe7181c983b9f5925af0fb6bc3a1d7ee2453a))

## [5.0.0-beta15](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta14...v5.0.0-beta15) (2023-09-27)

## [5.0.0-beta14](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta13...v5.0.0-beta14) (2023-09-27)


### Features

* **array util:** add sort methods ([b3a2dbf](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b3a2dbfec84ddc2d04ed7d0a0b3bb2c594965b3a))


### Bug Fixes

* (home routable) fix and improve search mechanism ([6741700](https://gitlab.unige.ch/solidify/solidify-frontend/commit/67417008f80bbb788a94d54f657a29b407f35921))

## [5.0.0-beta13](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta12...v5.0.0-beta13) (2023-09-25)

## [5.0.0-beta12](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta11...v5.0.0-beta12) (2023-09-25)


### Bug Fixes

* display required marker in red since angular mdc migration ([c063676](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c063676d6d3f290960e845f7eb3ec3b9fee02dad))

## [5.0.0-beta11](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta10...v5.0.0-beta11) (2023-09-22)

## [5.0.0-beta10](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta9...v5.0.0-beta10) (2023-09-22)


### Bug Fixes

* button spinner with new material button mdc ([be4092a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/be4092a9c60ff6b895284b362057e2cea066e703))

## [5.0.0-beta9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta8...v5.0.0-beta9) (2023-09-21)

## [5.0.0-beta8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta7...v5.0.0-beta8) (2023-09-21)

## [5.0.0-beta7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta6...v5.0.0-beta7) (2023-09-20)

## [5.0.0-beta6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta5...v5.0.0-beta6) (2023-09-19)

## [5.0.0-beta5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta4...v5.0.0-beta5) (2023-09-19)

## [5.0.0-beta4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta3...v5.0.0-beta4) (2023-09-18)

## [5.0.0-beta3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta2...v5.0.0-beta3) (2023-09-18)

## [5.0.0-beta2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v5.0.0-beta1...v5.0.0-beta2) (2023-09-18)

## [5.0.0-beta1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.33...v5.0.0-beta1) (2023-09-15)

### [4.8.33](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.32...v4.8.33) (2023-09-11)


### Bug Fixes

* carousel with multi level of ul ([5e90133](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5e901338c8f12824aee85d69b69a64ac1232554f))

### [4.8.32](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.31...v4.8.32) (2023-09-11)


### Bug Fixes

* **AppCarouselState:** support embedded ul lists in carousel pages ([19c0382](https://gitlab.unige.ch/solidify/solidify-frontend/commit/19c03821b7188ed6f142beddc01a36805efa1946))

### [4.8.31](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.30...v4.8.31) (2023-09-01)


### Bug Fixes

* lint ([0a7215a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0a7215af3f6d5cafd652840b27b1cfe2103baa73))

### [4.8.30](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.29...v4.8.30) (2023-08-28)


### Features

* **meta:** allow to set multi metas with same name and allow to clean some metas with regexp ([7669af5](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7669af56ad9839941653962709e206fb2c27651a))

### [4.8.29](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.28...v4.8.29) (2023-08-28)


### Features

* allow to generate swagger url from a module ([b8f98c4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b8f98c425043d1e5f9cf7d3129d4de52ef6b3aeb))

### [4.8.28](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.27...v4.8.28) (2023-08-28)


### Features

* **application list:** allow to enable bulk action easily in list routable ([9f27347](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9f27347a33333b6cd320aacaebc3854517d75fc2))

### [4.8.27](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.26...v4.8.27) (2023-08-16)


### Bug Fixes

* allow to disable ripple effect on round button and alternative button ([de7735b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/de7735bc6a1746b6478684358a85f8dc84586fda))

### [4.8.26](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.25...v4.8.26) (2023-08-14)


### Bug Fixes

* **ButtonToolbarDetailPresentational:** change Delete button color property to ButtonColorEnum type ([2d6f17e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2d6f17e10a2eb6d10c2398e3e32d0917cdd8166d))

### [4.8.25](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.24...v4.8.25) (2023-07-27)


### Features

* **facet:** allow to use frontend translation for facet value ([ca1aeea](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ca1aeea962b8d1d6c677eca513bedb8efc1cf9c4))

### [4.8.24](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.23...v4.8.24) (2023-07-26)


### Bug Fixes

* avoid SSR error on login ([a44fefd](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a44fefd739fd4499676121936f7a0f7ca058cebf))
* **system property:** allow to override ([0dafea4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0dafea4699ccafb449b41de974d14403ffb4b4fc))

### [4.8.23](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.22...v4.8.23) (2023-07-19)


### Bug Fixes

* carousel container ([d2f8ec7](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d2f8ec74d66c2cd2bfc9a6fab0de4a3dc347533a))
* redirect after login without path to target ([75e55c0](https://gitlab.unige.ch/solidify/solidify-frontend/commit/75e55c0a34e4ed2a4c545ae35339b032f690a96e))

### [4.8.22](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.21...v4.8.22) (2023-07-14)


### Bug Fixes

* **test:** date util unit test for ci test ([bc83861](https://gitlab.unige.ch/solidify/solidify-frontend/commit/bc8386140046b87830b6792d3536575c920e8668))
* **test:** date util unit test for ci test ([d6a3283](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d6a328363282cf4ddccaad8128fefb3e051b9ef1))

### [4.8.21](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.20...v4.8.21) (2023-07-13)


### Features

* add burger menu presentational ([cb3f45e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/cb3f45ed64bce24ada63f3d5a018b8e89059132f))
* add page not found routable ([d989174](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d9891746b2b49e216000d9f781fa5db6dee12570))
* add rss module ([1bff18f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1bff18f56a72b00f69316a036df1f32d5bcdb886))
* add twitter module ([23162ea](https://gitlab.unige.ch/solidify/solidify-frontend/commit/23162ea8ed8f2fe7fddd9e214beafe341ac9cb9d))
* **application module:** add system property ([1310a4f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1310a4f3e2da5a83d35335816347e08616984b13))

### [4.8.20](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.19...v4.8.20) (2023-07-07)


### Features

* **download file:** allow to force to add extension to filename with specific extension ([ddad2bf](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ddad2bfe756c26c83792ba6716c0afbab9d0aff9))


### Bug Fixes

* **file upload input:** allow to preview in all case ([ccde4db](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ccde4dba966715f1c2dc0bc39276f49189c1b1a9))

### [4.8.19](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.18...v4.8.19) (2023-07-07)


### Bug Fixes

* define type="button" for all buttons to avoid unexpected behavior with forms ([652954e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/652954e3cfd0ff714644b1986e45fbb03cb4dda4))

### [4.8.18](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.17...v4.8.18) (2023-07-06)


### Features

* **string util:** add method replaceNonIso8859_1Chars ([bd9cbd4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/bd9cbd44c3515c50d48881e384f745b9e8b84b26))

### [4.8.17](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.16...v4.8.17) (2023-07-05)


### Features

* **file upload input:** provide required validator ([2d2b7b6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2d2b7b685bb93933d3171e53214e2157e26a6d15))

### [4.8.16](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.15...v4.8.16) (2023-07-05)


### Bug Fixes

* **file upload input:** set in form control UNCHANGED value ([bb0d46f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/bb0d46fb980bbb8b9f889f9815e4860d34cf10e7))
* **searchable tree single select:** don't propagate change when init label ([478fabd](https://gitlab.unige.ch/solidify/solidify-frontend/commit/478fabd12b20adf2b1b7c1de7eb1e1d8f96ea583))

### [4.8.15](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.14...v4.8.15) (2023-07-05)


### Features

* add file upload input ([8de13e8](https://gitlab.unige.ch/solidify/solidify-frontend/commit/8de13e8a55b29c506146a826d6337d925e55498b))

### [4.8.14](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.13...v4.8.14) (2023-06-23)


### Features

* **panel expandable:** allow to display icon ([d9411be](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d9411be325ea39f777f9a2b695eba762ca5ec677))

### [4.8.13](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.12...v4.8.13) (2023-06-19)


### Features

* add material date format and data service to retrieve date input expected ([db4ac52](https://gitlab.unige.ch/solidify/solidify-frontend/commit/db4ac52be6edeefeaff6619677837e208ccc6316))

### [4.8.12](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.11...v4.8.12) (2023-06-19)


### Features

* **token dialog:** display type of token ([ef7bfd7](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ef7bfd71835ecea68cf5acb6c066d16d538fbe85))

### [4.8.11](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.10...v4.8.11) (2023-06-16)


### Features

* **input:** allow to display prefix icon on input ([646a467](https://gitlab.unige.ch/solidify/solidify-frontend/commit/646a467dd070a74da33e232b8b45c0c43365c02b))


### Bug Fixes

* unit test ([30ebe23](https://gitlab.unige.ch/solidify/solidify-frontend/commit/30ebe237bdeaa358dc4e47fef06c7123844a5fef))

### [4.8.10](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.9...v4.8.10) (2023-06-13)


### Bug Fixes

* **oauth2:** external logout url construction ([28e4bbe](https://gitlab.unige.ch/solidify/solidify-frontend/commit/28e4bbe2e77be30c15b2974035c12f11abceaaf2))

### [4.8.9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.8...v4.8.9) (2023-06-13)


### Features

* **oauth2:** call external logout endpoint with browser when logout and before MFA login ([5a761a9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5a761a9984264a7ac216a3510776f85d8f8af113))


### Bug Fixes

* **DateUtil:** fix moment to work with unit test ([fd43ecd](https://gitlab.unige.ch/solidify/solidify-frontend/commit/fd43ecd7def5a38c39120d44ab03472f2db4728c))
* **package.json:** fix serve prod script ([293234a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/293234ab65d0f8a48b87d7c982ed8cf1e721873d))

### [4.8.8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.7...v4.8.8) (2023-06-12)


### Bug Fixes

* **oauth2:** use correct redirect url in authorization code flow after error unauthorized error ([89385ba](https://gitlab.unige.ch/solidify/solidify-frontend/commit/89385baae5b30beaf929d3e35dd1068fd1c1c828))

### [4.8.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.6...v4.8.7) (2023-06-12)


### Bug Fixes

* **oauth2:** do not use refresh token when MFA needed ([c51e67d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c51e67d5dfeb0a54d088cb4d64cba76c38c79a40))

### [4.8.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.5...v4.8.6) (2023-06-12)


### Bug Fixes

* don't show download error when MFA needed on download endpoint ([ede1fdd](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ede1fdd3e2010437b1f33b552f0e1596f7da12b0))

### [4.8.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.4...v4.8.5) (2023-06-09)


### Bug Fixes

* **oauth2:** don't sent oauth2 token on saml logout endpoint ([ade5fa4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ade5fa4effa6df4f651f2f2384f78ec8271ba181))

### [4.8.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.3...v4.8.4) (2023-06-09)


### Features

* add ark menu ([dd4430f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/dd4430f162c98c917424e1d925c34e94e3612974))

### [4.8.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.2...v4.8.3) (2023-06-08)


### Bug Fixes

* **date util:** rollback change on import of moment ([728d46e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/728d46ef4a6c5d20488c576c4a9f8f8ef3fb13b7))

### [4.8.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.1...v4.8.2) (2023-06-08)


### Features

* **url util:** add new methods and fix existing ([121fb96](https://gitlab.unige.ch/solidify/solidify-frontend/commit/121fb96866cac79ba17ebfc2cf54bf6463d33f33))


### Bug Fixes

* **MFA:** call endpoint to remove shibboleth cookie before MFA login ([b2e003b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b2e003bf5d6ea4c18550be2960377ff95853184b))

### [4.8.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.8.0...v4.8.1) (2023-06-08)


### Features

* add search module with facet component ([ca56906](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ca569068ecf262cb37fe6b091e7681baa593b9d1))


### Bug Fixes

* add @angular/service-worker patch to allow run app outside UNIGE network ([e1b7bc8](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e1b7bc80d0ce6015f5e4b735250224ac6082d3a3))

## [4.8.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.7.4...v4.8.0) (2023-06-06)


### Features

* support MFA ([ffdc076](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ffdc0761b692228c3061a2fbf3f943a30c8426f9))


### Bug Fixes

* **abstract form:** do not trigger form event when switch or leave edit mode ([2aefb4e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2aefb4e65878f39cdd47c33380ad2e9c12e8a860))

### [4.7.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.7.3...v4.7.4) (2023-06-05)


### Features

* allow to provide state when login ([e66d843](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e66d843588b1c2aa5d7e148cb73a6c8752d5f413))


### Bug Fixes

* do not display disabled field as invalid ([2be4045](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2be40452ee782c6527e8af3b5103e8e47ca53df0))

### [4.7.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.7.2...v4.7.3) (2023-05-16)


### Features

* **form validation helper:** add method to clean error on linked form control when value change ([332ae21](https://gitlab.unige.ch/solidify/solidify-frontend/commit/332ae216b0356ab346b6970e057e510d34e9e14c))

### [4.7.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.7.1...v4.7.2) (2023-05-16)


### Features

* add carousel ([c8e4a88](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c8e4a889e618e55c2a126fd20390b9f030809423))

### [4.7.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.7.0...v4.7.1) (2023-05-15)


### ⚠ BREAKING CHANGES

* **resource logo:** rename resourceLogo into resourceFile and resourceLogo action name

* **resource logo:** rename resource logo into resource file ([87acd87](https://gitlab.unige.ch/solidify/solidify-frontend/commit/87acd8701207138f19deb9b1a9cc2b0a08d53fe9))

## [4.7.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.6.9...v4.7.0) (2023-05-11)

### [4.6.9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.6.8...v4.6.9) (2023-05-11)


### Bug Fixes

* **searchable-tree-single-select:** fix behavior in form ([7a82ae3](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7a82ae338ec99d0662958fa26f8a35a29d73b866))

### [4.6.8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.6.7...v4.6.8) (2023-05-10)


### Features

* add labels params for labels to translate in button toolbar ([248fc5c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/248fc5caa7c001be7d6ae05620d45f277fedbf4d))


### Bug Fixes

* translation problem when change theme dynamically ([8e06d7f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/8e06d7fe0146c7a86a417beb8d08498b7919c70b))

### [4.6.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.6.6...v4.6.7) (2023-05-05)


### Bug Fixes

* **datatable search:** [DLCM-2484] fix search not work ([a310c6e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a310c6e9e3792d69ca488a54494a15bac321669a))

### [4.6.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.6.5...v4.6.6) (2023-05-05)


### Bug Fixes

* **keyword input:** [DLCM-2464] display error message on keyword input ([19b50e1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/19b50e1398b8b2980b4dafad58bb77d7344400bd))

### [4.6.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.6.4...v4.6.5) (2023-05-04)


### Bug Fixes

* **resource logo:** allow to download logo only when get photo ([2b8b236](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2b8b236bb3636d9e9d47ecd109642465671e055c))

### [4.6.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.6.3...v4.6.4) (2023-05-03)


### Features

* **component:** add resource name container from AOU ([6edb7a2](https://gitlab.unige.ch/solidify/solidify-frontend/commit/6edb7a2b1df54456b8e47c6ae1356f1c7741e6df))


### Bug Fixes

* lint error ([1dc659c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1dc659c59105b045c5ce92a57cf3da2843516ec4))

### [4.6.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.6.2...v4.6.3) (2023-05-03)


### Features

* **datatable:** [DLCM-2476] possibility to invert value of boolean ([e33b1fe](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e33b1fe31087b54565a681e470b9ed4c033bcdc3))

### [4.6.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.6.1...v4.6.2) (2023-05-03)

### [4.6.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.6.0...v4.6.1) (2023-05-03)


### Features

* **doi:** add component and state to manage short doi ([f9e7f19](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f9e7f19bad4ba1f670fa56fd16d690d6e215a345))
* **proxy:** add script proxy conf variable ([29a2ff6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/29a2ff6d828518981aa731bef6cf087b77218acf))


### Bug Fixes

* set dark mode in cookie ([77f9ee9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/77f9ee9b94a1afe998d8a1c4e1340524ba988a57))
* **ssr:** avoid slow perf when an error is throw ([33f66d4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/33f66d40a51b32ec8ad5dfbacb92a6e88d91ac82))

## [4.6.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.5.10...v4.6.0) (2023-04-26)


### ⚠ BREAKING CHANGES

* **resource logo:** provide DownloadService instead of HttpClient in super of state that inherit from ResourceLogoState

### Features

* **resource logo:** allow to use custom option in resource logo ([701ef1c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/701ef1c2459f48e399dd701015fcd7dbaaa8a3cc))

### [4.5.10](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.5.9...v4.5.10) (2023-04-24)


### Bug Fixes

* **ssr:** transfer state to use as singleton ([535dd73](https://gitlab.unige.ch/solidify/solidify-frontend/commit/535dd733c95f9a3d527dc1f961c18acafa0ac0ee))

### [4.5.9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.5.8...v4.5.9) (2023-04-21)

### [4.5.8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.5.7...v4.5.8) (2023-04-21)


### Features

* **ssr:** extend transfer state to manage correctly transfer state on server side or client side ([9cad2ae](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9cad2ae30eb423e4ebdfe98850fbb55287264239))

### [4.5.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.5.6...v4.5.7) (2023-04-20)


### Bug Fixes

* **ssr:** get value in transfer state only in browser side ([24f348e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/24f348ea75c045f8aaea5a85e4f8d21af6582199))

### [4.5.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.5.5...v4.5.6) (2023-04-19)


### Bug Fixes

* **ssr:** allow ssr to work systematically ([d02d361](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d02d361bbbd2f3fba954161de14261f1f3d11f5e))

### [4.5.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.5.4...v4.5.5) (2023-04-18)


### Bug Fixes

* **ssr:** fix ssr interceptor ([d8f603e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d8f603e3dd1a879634f93ae66e7c44ec6a357b31))

### [4.5.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.5.3...v4.5.4) (2023-04-18)


### Bug Fixes

* **about:** avoid readonly violation on backend module version ([5b087c8](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5b087c850800f83b8c8c8edcf240504ad7678e43))
* add missing export for cookie helper, cookie storage and file upload helper ([25dc843](https://gitlab.unige.ch/solidify/solidify-frontend/commit/25dc84341d129e017f9dbd2802f80170155bdad1))
* **ssr:** rewrite url on about page ([bfe0c8a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/bfe0c8a6a08b3b9548abd957d700c8cda03582a8))

### [4.5.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.5.2...v4.5.3) (2023-04-14)


### Bug Fixes

* **ssr interceptor:** allow to rewrite url to match internal url ([a0f34f4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a0f34f43a07bc0151de4653e7a85c6922a3415aa))

### [4.5.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.5.1...v4.5.2) (2023-04-14)


### Bug Fixes

* allow to ignore some module on about page ([860e6d5](https://gitlab.unige.ch/solidify/solidify-frontend/commit/860e6d5330d72ffb9f6f4245578ece0832464d1e))

### [4.5.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.5.0...v4.5.1) (2023-04-14)

## [4.5.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.4.0...v4.5.0) (2023-04-14)


### ⚠ BREAKING CHANGES

* remove ngx-cookie and add cookie storage and cookie helper and storage darkMode and language into cookie

### Features

* remove ngx-cookie and add cookie storage and cookie helper and storage darkMode and language into cookie ([94e0f1d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/94e0f1d5d254f03fcba09d81aa1b6981e3973d4c))


### Bug Fixes

* patch index html to support ssr build mode ([39dbea6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/39dbea6e1285c28207a5aebd3bf8c33e8cdc361b))
* **ssr:** manage correctly language and dark theme ([4f4b6b6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4f4b6b6f87e1db666a3b0e29f4dc74b7deefd4ca))

## [4.4.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.3.9...v4.4.0) (2023-04-12)


### ⚠ BREAKING CHANGES

* **ssr:** remove param app state and app user state in app component
If you use SSR only, update app routing to use 'enabledBlocking' value for initialNavigation option
in appInitializerFn call method appConfigService.mergeConfigAndInitApplication instead of appConfigService.mergeConfig
If you don't use mergeConfig, create or update existing appInitializer to call appConfigService.initApplication

### Features

* **ssr:** allow perfect transition between ssr and browser ([70e5efa](https://gitlab.unige.ch/solidify/solidify-frontend/commit/70e5efaf964664c9f8629bce2bf6e535801fa3eb))


### Bug Fixes

* login with ssr ([0fb3dd1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0fb3dd19f8abc54103c88c558900685aaa2454f3))

### [4.3.9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.3.8...v4.3.9) (2023-04-11)


### Features

* enable transferState for http translate, environment runtime, app module ([ba8ae70](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ba8ae70470be9b657fa84ab22012a904f1c20655))


### Bug Fixes

* lint error ([6da1f97](https://gitlab.unige.ch/solidify/solidify-frontend/commit/6da1f97915f6b7a7bc2517249a3cf75aaec49e6d))
* manage 404 on static files ([435d8c5](https://gitlab.unige.ch/solidify/solidify-frontend/commit/435d8c53b18ee48dc80c54182bc4b74cb0065bc1))
* **ssr:** improve performance of ssr, avoid response before rendering and manage theme correctly ([2a90fa6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2a90fa6f25775b0250714be7fcb857cb3f1ce6d4))

### [4.3.8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.3.7...v4.3.8) (2023-04-04)


### Bug Fixes

* about page with ssr ([342cc73](https://gitlab.unige.ch/solidify/solidify-frontend/commit/342cc73ccd8d5fb7e33e4f0e4c375e079988a349))
* reduce timeout on ssr for dispact action with wait ([16af0b4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/16af0b4eb307c89d36b938b1c76b827471a733e8))

### [4.3.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.3.6...v4.3.7) (2023-04-03)

### [4.3.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.3.5...v4.3.6) (2023-03-30)


### Features

* **cookie consent:** request cookie aggreement when new cookie is added ([723a086](https://gitlab.unige.ch/solidify/solidify-frontend/commit/723a08605451eb043d2e4386c8a400e91b5ebabb))


### Bug Fixes

* **datatable:** reset searchable single select in data table when clean filter ([2573235](https://gitlab.unige.ch/solidify/solidify-frontend/commit/257323507fd3a71588ca4e718f545fbb79a34096))
* **preview:** allow to display docx based only on content type or puid ([2c5c491](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2c5c491f0533ceeba421980d1c2b938905698ace))

### [4.3.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.3.4...v4.3.5) (2023-03-29)


### Features

* **keyword:** allow extra info image in chip ([4448313](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4448313cddce54ec442d6a515f87372ae815d253))


### Bug Fixes

* **keyword input:** mark input as touch when paste value ([8919902](https://gitlab.unige.ch/solidify/solidify-frontend/commit/89199027ec9446bc04bdab0b020d2faac78a6702))
* **ssr util:** improve typing of window type ([ce9f2ad](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ce9f2adc5d25dc726a14e69861adafa00b90fcbd))

### [4.3.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.3.3...v4.3.4) (2023-03-28)


### Features

* add type to override type ([aa5aa9c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/aa5aa9ca06e8858decc9f015a2f8805bb674a490))

### [4.3.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.3.2...v4.3.3) (2023-03-24)


### Features

* add dashboard button on about page ([77db596](https://gitlab.unige.ch/solidify/solidify-frontend/commit/77db5961748b207ae0168154041e6b0ad86a92a1))

### [4.3.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.3.1...v4.3.2) (2023-03-23)


### Bug Fixes

* **history dialog:** use dedicated history query param ([b815938](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b8159383acfa91bf007161372ced5e8119e2ab9d))

### [4.3.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.3.0...v4.3.1) (2023-03-23)


### Features

* **environment:** add oai pmh param ([dbe9810](https://gitlab.unige.ch/solidify/solidify-frontend/commit/dbe9810c66d68580059fb698690f81dcaad765da))

## [4.3.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.2.0...v4.3.0) (2023-03-23)


### ⚠ BREAKING CHANGES

* if field, filterableField or sortableField is not a key of Resource or nested key, suffix with as any

### Bug Fixes

* remove unused column attribute ([01956d1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/01956d15332647ffffc0557c6ca3968b1b793935))


* add jsdoc for datatable and change type for field, filterableField and sortableField ([0a1329d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0a1329d5b9b63f32a0d72be4ed1392fbd062a54f))

## [4.2.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.1.3...v4.2.0) (2023-03-21)


### Features

* **array util:** add insert method ([b0b0045](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b0b0045fd5adb8778c04bb1f565cf5a0ce23bf47))
* support ssr ([b721b53](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b721b53cf3109bc0a971d65482ada1cdfc218e23))


### Bug Fixes

* lint ([fe30007](https://gitlab.unige.ch/solidify/solidify-frontend/commit/fe300071d17136cefd57da50973ac66a0321b218))

### [4.1.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.1.2...v4.1.3) (2023-03-14)


### Bug Fixes

* **i18n:** fix labels typos ([1a86dfd](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1a86dfde58de177eb6b3453d80da043936f35b79))

### [4.1.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.1.1...v4.1.2) (2023-03-03)


### Bug Fixes

* **security service:** manage when role doesn't contains application role ([dc82694](https://gitlab.unige.ch/solidify/solidify-frontend/commit/dc826942351cdb3134a81f972fc2f945c5d833d1))

### [4.1.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.1.0...v4.1.1) (2023-02-22)


### Bug Fixes

* **error helper:** manage correctly case where httpErrorKeyToSkipInErrorHandler is empty and refactor to make debuging more easy ([c47bf2c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c47bf2cd6f3288ac11ecafa64bab35c8d1976b47))
* **upload state:** display error from backend ([54d5a72](https://gitlab.unige.ch/solidify/solidify-frontend/commit/54d5a72feb76618a952149222db390a6c671b075))
* **upload state:** prevent error when errorDto is null ([fdaabf8](https://gitlab.unige.ch/solidify/solidify-frontend/commit/fdaabf88a572fe4c36dfb12221e6959e3d5e4e41))

## [4.1.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.0.9...v4.1.0) (2023-02-17)


### ⚠ BREAKING CHANGES

* **relation 3 tiers:** on action.ts that implement Relation3TiersNameSpace, define actions UpdateItem, UpdateItemSuccess, UpdateItemFail, SetGrandChildList, SetGrandChildListSuccess, SetGrandChildListFail

### Bug Fixes

* **relation 3 tiers:** manage update of attribute and grand child list ([0151fee](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0151fee48dc0294e2eb0319a4f736c0594ac2569))

### [4.0.9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.0.8...v4.0.9) (2023-02-07)


### Bug Fixes

* **can deactivate guard:** avoid error if component not implement canDeactivate ([eec1054](https://gitlab.unige.ch/solidify/solidify-frontend/commit/eec10540cd7a816a070615f4d4770960405998b5))

### [4.0.8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.0.7...v4.0.8) (2023-02-01)


### Features

* allow to avoid to enter in edit mode when specific class is found ([0cb9a73](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0cb9a73ece7d4034a040d1fe493ecc4e905468b9))


### Bug Fixes

* define size for tile image ([f1be65d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f1be65d0219098b429b7e0cc8dcb3822c3d805f6))

### [4.0.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.0.6...v4.0.7) (2023-02-01)


### Bug Fixes

* display dedicated message when preview is not available due to status ([3837f55](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3837f559c67034bdc80c118ac4f564b35996bb6d))

### [4.0.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.0.5...v4.0.6) (2023-02-01)


### Bug Fixes

* solidify spinner ([fa2283c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/fa2283ccea6c7a167dfb2e1aa5c1fb4c56e02d04))

### [4.0.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.0.4...v4.0.5) (2023-01-31)


### Bug Fixes

* allow to customize default image ([c2a1c27](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c2a1c27a4b896feeaa058a1be2a5b98e7bf45c9c))
* **ApiActionNamePartialEnum:** Change remove to delete ([5cca3c0](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5cca3c0322829d56e3602d25edc2172eee983d22))
* **resource logo:** manage thumbnail ([a5acd25](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a5acd250d5ea781a898060861996da329bb676ea))
* **resource logo:** manage thumbnail ([f441377](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f44137740c4313c323ad4151c551c1ecd993c155))

### [4.0.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.0.3...v4.0.4) (2023-01-30)


### Features

* add parameters for image ([b70a022](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b70a022bc3911309bc1f8c007a40d99e37d8b71b))
* **image display component:** add option to hide fallback image ([1f54e39](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1f54e39f6181f2b0e0dc87fa93019890114216e8))


### Bug Fixes

* make edit available and delete available not readonly ([872f9be](https://gitlab.unige.ch/solidify/solidify-frontend/commit/872f9beab7b779e5310669e99e8084c573ae9f43))

### [4.0.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.0.2...v4.0.3) (2023-01-26)

### [4.0.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.0.1...v4.0.2) (2023-01-26)


### Features

* add util for meta ([5e84dae](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5e84dae1cb33e504aaa5beec52c5d502ae1d142c))

### [4.0.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.0.0...v4.0.1) (2023-01-25)


### Features

* allow to set meta tag in header ([c14327f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c14327f03261641b4042ed20786b96b014ff75d3))


### Bug Fixes

* **dom helper:** manage null target element ([331d260](https://gitlab.unige.ch/solidify/solidify-frontend/commit/331d2603b42831dccd043678708fb6efeb88981b))

## [4.0.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.0.0-RC3...v4.0.0) (2023-01-12)


### Features

* **oai-pmh:** add admin modules and shared state for oai pmh ([ee253e7](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ee253e73d3e9c963fc9f15052a0294e436807a1a))


### Bug Fixes

* http translate loader extra to merge correctly all files ([3a7d53a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3a7d53acfcfc9999653b6e3f48563dff1f1558e0))

## [4.0.0-RC3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.0.0-RC2...v4.0.0-RC3) (2023-01-05)


### ⚠ BREAKING CHANGES

* **i18n:** update angular.json to add assets conf to expose i18n solidify files (see application/boilerplate-resources/angular.json)
* **i18n:** update patches/@bartholomej+ngx-translate-extract+8.0.2.patch
* **i18n:** update app.module.ts LABEL_TRANSLATE useValue by {...labelTranslateInterface, ...labelTranslateSolidify}
* **i18n:** update label-translate-solidify.ts to leave only value that you want to override

### Features

* **i18n:** provide labels translations and allow to override only some labels ([c8d231e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c8d231e4e7f0ecb165fab3b1a050c347821f7731))
* **url util:** add method to convert navigate to path ([6321d66](https://gitlab.unige.ch/solidify/solidify-frontend/commit/6321d6638f77a6661a3679f2bc5449ab37b2327a))


### Bug Fixes

* type route ([7d4d1d5](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7d4d1d5b43eac91faf8202db99c5ddc1645d81f0))
* update copyright date ([65e85bd](https://gitlab.unige.ch/solidify/solidify-frontend/commit/65e85bd204009abe3f319a6d230ddba39bef58d1))

## [4.0.0-RC2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v4.0.0-RC1...v4.0.0-RC2) (2022-12-07)


### Features

* **guard:** add combined guard ([d6ee602](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d6ee602823c2c5abd7ed308bca0692ffa81b6286))

## [4.0.0-RC1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.9.3...v4.0.0-RC1) (2022-12-06)


### ⚠ BREAKING CHANGES

* revert completely fully external dependencies mechanism
* revert partially fully external dependencies mechanism
* implement fully external dependencies mechanism
* define solidify frontend optional dependencies
* update package-lock.json
* clean dependencies
* remove legacy videojs-wavesurfer dependency
* Implement _applyDarkMode from AppState to apply dark mode
* PdfViewerModule moved from SolidifyFrontendCoreModule to SolidifyFrontendFilePreviewModule (if you need to use PdfViewerModule then import SolidifyFrontendFilePreviewModule instead of SolidifyFrontendCoreModule)
* remove legacy ngx-doc-viewer dependency from boilerplate resources package.json
* File preview extracted
* Quill moved to SolidifyFrontendRichTextEditorModule and application/scss/main.scss (if you use Quill, import SolidifyFrontendRichTextEditorModule and application/scss/main.scss instead of SolidifyFrontendCoreModule and core/scss/main.scss)
* StandardErrorsHandlerService and GlobalErrorsHandlerService extracted to SolidifyFrontendErrorHandlingModule
* Application status and update version dialog extracted to SolidifyFrontendAppStatusModule
* Application status and dialog logic moved from AbstractAppComponent to SolidifyApplicationAbstractAppComponent
* Constructor parameters _dialog and _privacyPolicyTermsOfUseApprovalDialogType moved from AppState to SolidifyApplicationAppState
* Molecular visualization feature entirely removed
* Use APP_OPTIONS injection token for SolidifyAppOptions
* Directory boilerplate-resources moved to /lib/application and copy-boilerplate-resources updated
* MaintenanceModeRoutable updated (use as is OR extend and override _onRedirectToHome); banner moved
* Global banner observability moved from AbstractAppComponent to SolidifyApplicationAbstractAppComponent
* GlobalBanner component refactored from presentational to container
* GlobalBanner extracted
* AboutRoutable extracted from core to application
* Module loading extracted from core to application
* Auth directory extracted to Oauth2 and TokenDialog moved into application components
* AppState updated (use as is OR extend, override computeHttpHeadersFor... methods)
* AppState updated (use as is OR extend, override and type doLogin/doLoginSuccess/... methods)
* All user logic extracted to application
* fields moved from DefaultSolidifyAppEnvironment to CoreSolidifyEnvironment
* use CoreSolidifyEnvironment instead of MainSolidifyEnvironment
* AppState, AbstractAppComponent, MaintenanceModeRoutable and PreventLeaveMaintenanceGuardService updated (use as is OR extend and override _isLoggedInRoot OR use already existing extension); SecurityServiceInjectionToken moved
* fix application export in public_api.ts
* add SolidifyFrontendSelectModule dependency in SolidifyFrontendDataTableModule
* add SolidifyFrontendMaterialModule in core public_api
* move status history dialog to application
* fix ng-package.json entryFile
* refactor SolidifyApplicationModule components
* use SolidifyFrontendApplicationModule instead of SolidifyFrontendEResearchModule
* use SolidifyFrontendTourModule
* use SolidifyFrontendSelectModule
* use SolidifyFrontendInputModule
* use SolidifyFrontendDataTableModule
* fix module export in SolidifyFrontendCoreModule
* move multiple core presentationals and containers to e-research
* move multiple core routables to e-research
* use SolidifyFrontendRichTextEditorModule
* use SolidifyFrontendGlobalBannerModule
* replace usages of SolidifyFrontedModule by SolidifyFrontendCoreModule and other modules that will come

### Bug Fixes

* add missing base resource with labels in public api ([0321e30](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0321e30d141932ef8b634a888b2f940b958e476c))
* add missing export ([0439827](https://gitlab.unige.ch/solidify/solidify-frontend/commit/043982756bba4e65b1ce53645f248ef66b249144))
* add missing module export in global banner public api ([a452574](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a452574e34982525cb8294e15c2b8b46b24225ec))
* allow app state to be inherited ([a6f8902](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a6f8902638c48b2bf8d5bb1b27c61a5b38f283c7))
* allow to extend viewvers ([4f8bf85](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4f8bf85d8ecc93fca0010d2cc9562acb791a9f72))
* app solidify application state restirested actions ([eea67bf](https://gitlab.unige.ch/solidify/solidify-frontend/commit/eea67bf76889b0446f136b140ad82347bea8a86f))
* **language selector:** default position of tooltip and allow to custom position ([d3f4a90](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d3f4a900c7aa720eb20b1cf2e346fd4881335907))
* lint ([346043c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/346043c2a93b974f14b0cf35b290ef334a68beee))
* preview pdf ([9ef5229](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9ef5229f2d201d409ce242e0cae90b05867357d9))
* scripts to copy html and resource ([04710ae](https://gitlab.unige.ch/solidify/solidify-frontend/commit/04710ae62c5d13b5f8b3d86e14d11d23f0839d0d))
* solidify application state and user state ([5f5e761](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5f5e7611840d577bcf79b54fe9f5eadafa5422e4))
* **string util:** use replace all instead of replace on format methods ([0dc0f90](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0dc0f90a3c535823eb25a93836e2813d9a1ab1a6))
* types ([28006a8](https://gitlab.unige.ch/solidify/solidify-frontend/commit/28006a897ab1126e03240c2f8eceee5b01429fa3))


* add _applyDarkMode in AppState and implement it in SolidifyApplicationAppState ([e7afe15](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e7afe15a80cd50d92a29ff0b4d425927cae6fcaa))
* add and integrate APP_OPTIONS injection token for SolidifyAppOptions ([655133f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/655133f5d2224a09640872cb4f4b167ca4bd2313))
* add SolidifyFrontendMaterialModule in core public_api ([96081c5](https://gitlab.unige.ch/solidify/solidify-frontend/commit/96081c59377b4a1ef9e83609654189192119ab8d))
* add SolidifyFrontendSelectModule dependency in SolidifyFrontendDataTableModule ([00faf8e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/00faf8e944b245a7a8163692adbe6641ea67a93a))
* clean dependencies ([37351d6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/37351d6625916827f057a7e347a49d3ea4e0c459))
* define solidify frontend optional dependencies ([f422c7b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f422c7b92a538ce78bde66bb220fd75ec8d678e4))
* entirely removed molecular visualization feature ([b6f3c0b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b6f3c0bced17517d6d75770672564d52d476bce9))
* extract AboutRoutable from core to application ([c33b606](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c33b6060a9339a737401f546782c719a914a1010))
* extract all user logic to application ([c3b600f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c3b600f1503dd0223291ca4279b5eb6f44b16ec3))
* extract application status and update version dialog to SolidifyFrontendAppStatusModule ([fc9e41d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/fc9e41de684f178dedfae979d2e63301c995a110))
* extract authentication logic from core and implemented solidify application extension in SolidifyApplicationAppState ([6167978](https://gitlab.unige.ch/solidify/solidify-frontend/commit/616797874d494711834d02e5fb6c888d26e109ef))
* extract authentication logic usages from core to application ([e11785e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e11785e586244b283a8dbc2824f5d9c2be13f22b))
* extract banner from core to application ([2adb07f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2adb07f9d05213758d8f0a3d1e395bea5c7907f8))
* extract file preview ([5d7f99c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5d7f99c1ff25e3a9923648c678250489b5eae6cf))
* extract GlobalBanner module ([7900901](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7900901df3d50d055742c74bb7e31a4a28038136))
* extract HeaderEnum usages from core AppState to application SolidifyApplicationAppState ([ba4975f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ba4975f145af3d6fc5717edad72585b5264efe24))
* extract module loading from core to application ([ecf73ae](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ecf73aec4c8280968915ad3082d4e521e9664518))
* extract security from core and implemented solidify application extensions ([e5ede6b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e5ede6bb6183b9cf87e2cb6b4b345de79acd3449))
* extract SolidifyFrontendDataTableModule from core ([4485aef](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4485aef6198a868a0a14f03d5d4079f60bdfb998))
* extract SolidifyFrontendErrorHandlingModule with StandardErrorsHandlerService and GlobalErrorsHandlerService ([94262ba](https://gitlab.unige.ch/solidify/solidify-frontend/commit/94262bad3493e27c54335fe9cdb23996e3a2535c))
* extract SolidifyFrontendGlobalBannerModule from core ([c71bb6e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c71bb6ef83d09d3dfc972e03d4248c6c6197fed4))
* extract SolidifyFrontendInputModule from core ([9fc698b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9fc698b1463b509de1cdc351fe0b30cfa12fb1d1))
* extract SolidifyFrontendRichTextEditorModule from core ([511ac67](https://gitlab.unige.ch/solidify/solidify-frontend/commit/511ac67a694de0f80f53f8fca3bd10d004c0618a))
* extract SolidifyFrontendSelectModule from core ([dcc659a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/dcc659abc52e3afc2f5930e2e258353585195921))
* extract SolidifyFrontendTourModule from core ([e1d6755](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e1d67553ca6a3280ecf660cea74a7b66c0e5a0a3))
* fix application export in public_api.ts ([5516606](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5516606146aa8e4feb1e56ba9111e4503fa91729))
* fix module export in SolidifyFrontendCoreModule ([1cc0b39](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1cc0b3935a9266a87c3dd0fa73183ef25e7af0ef))
* fix ng-package.json entryFile ([7a39a36](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7a39a3603be8e3aa641ff3637ad54d121ae9cf10))
* implement fully external dependencies mechanism ([a10dc27](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a10dc272daf755ca9a793857b0bef6741a0fa87b))
* move application status and dialog logic from AbstractAppComponent to SolidifyApplicationAbstractAppComponent ([b7eca6a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b7eca6a20d73588c963bb7876826f1f23a6ada35))
* move constructor parameters _dialog and _privacyPolicyTermsOfUseApprovalDialogType from AppState to SolidifyApplicationAppState ([d5fe3d6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d5fe3d69bd1661404e0b435af55b66fa62181147))
* move directory boilerplate-resources to /lib/application and update copy-boilerplate-resources ([e427ce0](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e427ce0234f3b76ad8c425b64a932356ffd83fed))
* move global banner observability from AbstractAppComponent to SolidifyApplicationAbstractAppComponent ([7733eba](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7733eba795cfa3da90abc876d98fad6c99bfe1e8))
* move multiple core presentationals and containers to e-research ([cf19bd2](https://gitlab.unige.ch/solidify/solidify-frontend/commit/cf19bd26bee711be453bfe64f004c0ae8ca92207))
* move multiple core routables to e-research ([83d7a19](https://gitlab.unige.ch/solidify/solidify-frontend/commit/83d7a196527d42d286ccb1b3f9837cc5f4724070))
* move PdfViewerModule import from SolidifyFrontendCoreModule to SolidifyFrontendFilePreviewModule ([2abdf5f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2abdf5ff972c884b157c2d4fc2d12e73d2688092))
* move Quill to SolidifyFrontendRichTextEditorModule and application/scss/main.scss ([3387d44](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3387d449c9a88c56ec704c3d5ec2176a0c676e03))
* move status history dialog to application ([27ac252](https://gitlab.unige.ch/solidify/solidify-frontend/commit/27ac252357356c7383bcdc3307224b3c3eb3c010))
* moved fields from DefaultSolidifyAppEnvironment to CoreSolidifyEnvironment ([78c48b9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/78c48b94a90be03a8494b56701dc9ca16f702044))
* refactor GlobalBanner component from presentational to container ([d5fb070](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d5fb070cb7e0e746c754084a2884140aa2053ecb))
* refactor SolidifyApplicationModule components ([8625979](https://gitlab.unige.ch/solidify/solidify-frontend/commit/8625979e8b84427fbc1a96d2daeb87738ec8b575))
* remove legacy ngx-doc-viewer dependency from boilerplate resources package.json ([8018e9c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/8018e9c5778f4f531369725d4bc864842ec7485b))
* remove legacy videojs-wavesurfer dependency ([5dcb9f6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5dcb9f6df208132762ee8e8634cff4dda2fb5099))
* rename MainSolidifyEnvironment into CoreSolidifyEnvironment ([c97bacc](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c97bacccaf6dba88ef347e06ca9972d75861c880))
* rename SolidifyFrontedModule into SolidifyFrontendCoreModule ([a42a22b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a42a22bbf92e255fffe3cf406333b25ede4904e0))
* rename SolidifyFrontendEResearchModule into SolidifyFrontendApplicationModule ([37ea3a6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/37ea3a681eccdc75944b67e48f1284ced77a443a))
* revert completely fully external dependencies mechanism ([06ad9f9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/06ad9f96bfd18b9fe462dc76c6204d322556f204))
* revert partially fully external dependencies mechanism ([a91f140](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a91f140800cad54c42099d83e1bd8fee93b0ecc4))
* update package-lock.json ([1394bf7](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1394bf74441995979f5277fac7774e03ec20e1a4))

### [3.9.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.9.2...v3.9.3) (2022-11-28)


### Bug Fixes

* **url query param:** fix encode param method ([8033fca](https://gitlab.unige.ch/solidify/solidify-frontend/commit/8033fca0f219becea913b845044a2121b373375e))

### [3.9.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.9.1...v3.9.2) (2022-11-25)


### Bug Fixes

* allow to color line on searchable select ([e00713f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e00713ffb7dd2d0c542b47dcfb2dfe295e41ebfe))

### [3.9.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.9.0...v3.9.1) (2022-11-14)


### Bug Fixes

* link style in global banner ([d861172](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d8611726de818b945e48a039cc1d8c657bfd2291))

## [3.9.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.8.0...v3.9.0) (2022-11-14)


### ⚠ BREAKING CHANGES

* In app state provide in super :
- MatDialog
- type of PolicyTermsOfUseApprovalDialog or undefined if you don't want to use this feature (you can use the component provided in solidify : PrivacyPolicyTermsOfUseApprovalDialog)
Define translation for new label :
- cookieConsentSidebarPrivacyPolicyAndTermsOfUseAcceptedTitle
- cookieConsentSidebarPrivacyPolicyAndTermsOfUseAcceptedDescription
- privacyPolicyTermsOfUseApprovalDialogTitle
- privacyPolicyTermsOfUseApprovalDialogMessage
- privacyPolicyTermsOfUseApprovalDialogIAcceptPrivacyPolicy
- privacyPolicyTermsOfUseApprovalDialogIAcceptTermsOfUse
- privacyPolicyTermsOfUseApprovalDialogClose
- privacyPolicyTermsOfUseApprovalDialogConfirm

### Features

* allow to ask to user at login his consent on privacy policy and terms of use approval ([3ce3aa4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3ce3aa4d6a5edda7a9c888b9b8d1e8433c243d01))

## [3.8.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.7.7...v3.8.0) (2022-11-11)


### ⚠ BREAKING CHANGES

* update reference of enum value IconLibEnum.fontAwesome into IconLibEnum.fontAwesomeSolid

### Features

* manage other fontawesome library set of icons ([af84f47](https://gitlab.unige.ch/solidify/solidify-frontend/commit/af84f47468b02faf9b98a8361d6c1a5263285b88))

### [3.7.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.7.6...v3.7.7) (2022-11-07)


### Bug Fixes

* disable mol preview by default ([ef25bf9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ef25bf9fad95a1bb1e1eb96b4e7c8e92f4b026aa))
* **history dialog:** capitalize name provided ([ec1b1e4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ec1b1e4d9c80c7ebf20e2277e91493f04d5daa36))
* **history dialog:** remove treatment on name ([688b76f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/688b76fcc2231340ea4a879660057faf975aeca4))

### [3.7.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.7.5...v3.7.6) (2022-10-31)


### Bug Fixes

* **datatable:** instance of method that check filter enum type ([4ace9a2](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4ace9a2773bce63104f10e5ddcac7a1e8f076bde))

### [3.7.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.7.4...v3.7.5) (2022-10-28)


### Features

* add validator forbidden values ([e649db0](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e649db061976924e8006d3bc74471c672366df74))

### [3.7.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.7.3...v3.7.4) (2022-10-27)


### Features

* add back translate and manage it into datatable ([f21062f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f21062f87013397e0f361d846612c2e6126925b0))

### [3.7.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.7.2...v3.7.3) (2022-10-26)


### Bug Fixes

* **searchable tree single select:** clean highlight text of selected value ([f6e5839](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f6e5839b44c305b931bcae3e955d85d42cbdef44))

### [3.7.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.7.1...v3.7.2) (2022-10-26)


### Features

* add new component searchable tree single select ([c2d4609](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c2d4609a1583b5b559f069bee2f1d41473a278e6))
* **searchable tree multi select:** show previously selected node by default ([68e3229](https://gitlab.unige.ch/solidify/solidify-frontend/commit/68e3229616702f9946bf32fd5b9d70a1b7f7c57e))


### Bug Fixes

* lint ([2890201](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2890201d33dd234e70b916eaf02e781ff08b813e))

### [3.7.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.7.0...v3.7.1) (2022-10-26)


### Features

* add patches ([5f3fdba](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5f3fdbaa82c69d962c589907a5cc71dacd0dfe40))
* **list composite:** not allow to access to create button when parent not selected ([1613591](https://gitlab.unige.ch/solidify/solidify-frontend/commit/16135915306ef1dbf8ced1796893647cb7154304))

## [3.7.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.6.0...v3.7.0) (2022-10-25)


### ⚠ BREAKING CHANGES

* update all libs to match with new dependancies (see boilerplate-resources/package.json)

* update to angular 14 and update third parties libs to last version ([8533312](https://gitlab.unige.ch/solidify/solidify-frontend/commit/8533312ab837701a805b205da2895fa8484adb8b))

## [3.6.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.5.1...v3.6.0) (2022-10-24)


### ⚠ BREAKING CHANGES

* rename apiService method "get" into "getCollection"
* rename apiService method "getByIdInPath" into "get"
* rename apiService method "head" into "headCollection"
* rename apiService method "headByIdInPath" into "head"
* rename apiService method "patchByIdWithCustomUrl" into "patch"

* rename api service method ([cf02e25](https://gitlab.unige.ch/solidify/solidify-frontend/commit/cf02e252646c12647eafe2f7a9813d68487d6ad5))

### [3.5.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.5.0...v3.5.1) (2022-10-24)


### Features

* **url util:** add method to generate url from ngxs navigate action ([edef148](https://gitlab.unige.ch/solidify/solidify-frontend/commit/edef14893a70a5ec06a702c00d1b1e1d924eb162))

## [3.5.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.4.10...v3.5.0) (2022-10-21)


### ⚠ BREAKING CHANGES

* **resource state:** in state that extends resource state, for options apiPathGetAll and apiPathCreate, replace string value by a callback that return this string

### Bug Fixes

* lint error ([c159c61](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c159c61a90292d844da409c0f06f37f63d95b0b0))
* **resource state:** replace string to callback for option apiPathGetAll and apiPathCreate ([7273dd6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7273dd6379891fbd4f0d27cfd2ce37625441d9bc))

### [3.4.10](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.4.9...v3.4.10) (2022-10-20)


### Bug Fixes

* **rich text editor:** icon for clean style ([503d014](https://gitlab.unige.ch/solidify/solidify-frontend/commit/503d01492ba8eba17b71f5f3160a60cb85d17e01))

### [3.4.9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.4.8...v3.4.9) (2022-10-19)

### [3.4.8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.4.7...v3.4.8) (2022-10-19)


### Bug Fixes

* composition create page ([7e47670](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7e4767088e189d5958562bf81b3961ea779708d7))

### [3.4.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.4.6...v3.4.7) (2022-10-19)


### Bug Fixes

* composition form edit ([2200023](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2200023e36b37091af63cbebfb4c6c4d3b301824))
* lint remove unused import ([f7810db](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f7810db5c5d4f883871d472d3774a54ebde196e5))

### [3.4.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.4.5...v3.4.6) (2022-10-18)


### Features

* manage pre and post treatment during searchable highlight ([da9f414](https://gitlab.unige.ch/solidify/solidify-frontend/commit/da9f414e3dc72ed5ef2498fa77943d4bf88cd2bd))

### [3.4.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.4.4...v3.4.5) (2022-10-18)


### Features

* add composition screen ([20d215a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/20d215a90de91110edde8f6787204717cb7c69c1))


### Bug Fixes

* **rich text:** replace clean formatting icon with eraser ([04558f6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/04558f6953fb6d4cab42ec0cf51227e26eaf71a3))
* **searchable content:** highlight term ([05082ca](https://gitlab.unige.ch/solidify/solidify-frontend/commit/05082ca51915c307b8a5589a817273651d139b93))
* **searchable tree multi select:** use label callback in tree ([e460a41](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e460a41e8fe2ee86a1737521f7b78d8a72eb5e2f))

### [3.4.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.4.3...v3.4.4) (2022-10-12)


### Bug Fixes

* **searchable multi select:** allow to customize get by list id ([9405a3b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9405a3bab6a753f43d8f38a04c7862f0c17e6e50))

### [3.4.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.4.2...v3.4.3) (2022-10-10)


### Bug Fixes

* script to move scripts files in binary build ([56b959a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/56b959a4894c41baffbeb35c7fe6cddd0a4d19bf))

### [3.4.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.4.1...v3.4.2) (2022-10-06)


### Features

* allow to trigger external changes from parent ([a46a0d0](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a46a0d03f0550867764d121702c9bac7697e6438))


### Bug Fixes

* loading problem that break design of app ([ac534f9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ac534f92ad55079a5b1e756cf87618c42dbdd497))

### [3.4.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.4.0...v3.4.1) (2022-09-29)

## [3.4.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.19...v3.4.0) (2022-09-28)


### ⚠ BREAKING CHANGES

* **confirm dialog:** in ConfirmDialogData replace "isWithComment: true" by "withInput: ConfirmDialogInputEnum.TEXTAREA", "isWithComment: false" by "withInput: undefined", commentRequired by inputRequired, commentLabelToTranslate by inputLabelToTranslate and commentMaxLimit by inputMaxLimit

### Features

* **confirm dialog:** add possibility to display input text in confirm dialog and initialised input with value ([ea75dfe](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ea75dfe564d9a30d5d40b4db57116844a89cdfaa))

### [3.3.19](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.18...v3.3.19) (2022-09-26)


### Features

* **polling:** allow to wait time before start polling ([44ac472](https://gitlab.unige.ch/solidify/solidify-frontend/commit/44ac472894d657faff07f7c9eca680fd8bb199ce))


### Bug Fixes

* avoid error on searchable abstract content when list is undefined ([aa4039d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/aa4039d810b03b615d035ec0b6402509336a680e))

### [3.3.18](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.17...v3.3.18) (2022-09-22)


### Bug Fixes

* **searchable tree multi select:** avoid to throw error when undefined array ([27c18ef](https://gitlab.unige.ch/solidify/solidify-frontend/commit/27c18ef8268bcefabf2d04d9d510e3b9a71a50e1))

### [3.3.17](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.16...v3.3.17) (2022-09-22)


### Bug Fixes

* **all multi select:** add tack by to avoid intensive change detection ([5acf2eb](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5acf2eb682baa5e5190152dfd896650e4a8ad98e))

### [3.3.16](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.15...v3.3.16) (2022-09-19)


### Bug Fixes

* audit vulnerability and update copy scripts to work like before ([f8783fe](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f8783fe4244a8b993f35600124ac3310335ced2e))
* lint with module mode ([0c49955](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0c499555868dc65d4d70a49a71d7c7cedd45edcf))

### [3.3.15](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.14...v3.3.15) (2022-09-14)


### Bug Fixes

* **appState:** make optional highlight loader and security service ([279138f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/279138fcd5ab32fa463db4155799beaea166cc0b))

### [3.3.14](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.13...v3.3.14) (2022-09-05)


### Features

* **interceptor:** add header to bypass oauth token ([733e21f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/733e21fea4876d9feaeb75256a55457679e0d2c9))


### Bug Fixes

* don't provide oauth token on modules and request module only on about page ([0e6a4d1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0e6a4d1793d10dc02df8cd3e75b820534e025b7e))

### [3.3.13](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.12...v3.3.13) (2022-09-02)


### Bug Fixes

* [AoU-1360] Add class for treating errorDto message ([bea36a4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/bea36a450f2da1cc8789568cc916a62b7f6bb500))

### [3.3.12](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.11...v3.3.12) (2022-08-26)


### Bug Fixes

* don't send token on get-active banner endpoint ([86c157a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/86c157a6532dfaa69234f2bc94a6f00a2e69aa4c))
* **lint:** remove unused imports ([67e0321](https://gitlab.unige.ch/solidify/solidify-frontend/commit/67e03215244042ade15b194d3aac3172f07bb80f))
* rich text editor validation errors ([ac697c5](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ac697c5e61f87203135df3599046d78fce8dc5f0))
* update patch service worker for fetch assets with dynamic base href ([d4baa1a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d4baa1a3e71ed5d1b67e673bbb91dc58371b2c7c))

### [3.3.11](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.10...v3.3.11) (2022-07-25)


### Features

* add email validator ([70967f8](https://gitlab.unige.ch/solidify/solidify-frontend/commit/70967f867e2c5e4025c3b69775e93691f61ff17e))

### [3.3.10](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.9...v3.3.10) (2022-07-25)


### Bug Fixes

* **button toolbar:** toggle buttons display condition ([365b84a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/365b84aec167e3b24722fdccdff8920e24293855))

### [3.3.9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.8...v3.3.9) (2022-07-14)


### Features

* add head request on api service ([7bea1be](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7bea1bea67df4da0db658d6f090dbd625b55d478))

### [3.3.8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.7...v3.3.8) (2022-07-04)


### Bug Fixes

* avoid to get active banner when maintenance mode or server offline ([c0414a5](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c0414a5d57596cd028dfa51d13d3df04fc2fb685))

### [3.3.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.6...v3.3.7) (2022-06-30)


### Features

* allow to display tooltip on line ([3f20622](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3f206223cf2976a6fcd6972b99f7eb4fb69491d0))

### [3.3.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.5...v3.3.6) (2022-06-30)


### Features

* **datatable:** scroll to top when paginate ([969515c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/969515c4c3661914a7bea31c0aaa894f4042c151))


### Bug Fixes

* toolbar height ([166c1cd](https://gitlab.unige.ch/solidify/solidify-frontend/commit/166c1cdb366a445a81be164f7d56196d4096ed85))

### [3.3.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.4...v3.3.5) (2022-06-29)


### Features

* **datatable:** allow to display number on action button ([3445675](https://gitlab.unige.ch/solidify/solidify-frontend/commit/34456752e6efbd920dbef1a5dac4b845e006db00))


### Bug Fixes

* remove important on data table highlight ([18eef8f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/18eef8f1eb2fe5652e2823d28bbf81fd3eacccea))

### [3.3.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.3...v3.3.4) (2022-06-27)


### Bug Fixes

* avoid error when info endpoint return error ([dfb1bfa](https://gitlab.unige.ch/solidify/solidify-frontend/commit/dfb1bfaf329771e620e9df25891e3f31e34a8551))

### [3.3.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.2...v3.3.3) (2022-06-23)


### Bug Fixes

* allow to not cancel incomplete load chunk ([7015d14](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7015d14ec59b3af82d3c102cb843e71530eb39b9))

### [3.3.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.1...v3.3.2) (2022-06-22)


### Bug Fixes

* bypass service worker on upload to allow progress ([c4a7b9a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c4a7b9a54670764bb608d15c7347f5d03e953aa0))

### [3.3.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.3.0...v3.3.1) (2022-06-21)


### Features

* add get active banner into app state ([739572d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/739572d042616e89d42ebfa025392115d65f8d9a))

## [3.3.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.21...v3.3.0) (2022-06-20)


### Features

* leave and jump automatically to maintenance mode ([c260044](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c260044c840dacfbd5b5d9774dc148beab4de45a))

### [3.2.21](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.20...v3.2.21) (2022-06-20)


### Bug Fixes

* remove script index boilerplate ([b1264c3](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b1264c3f8fb5e9fb366ad2ec8e7ad32c68015fe5))
* remove service worker manual registration ([3233918](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3233918f01b075140d521cc0dec921816905e5ed))
* rollback script into boilerplate index.html ([78a185d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/78a185df4f4eb1ef565d393a61609b431b60f958))
* strict check on maintenance mode variable ([02e724d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/02e724d94fa9b442d5280f2810c38401ff13b626))

### [3.2.20](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.19...v3.2.20) (2022-06-17)


### Bug Fixes

* add cache busting to environment runtime json ([399a09c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/399a09ccc61a45419b5109b076afdf6d987e8355))
* add docker-entrypoint to boilerplate ([4479a75](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4479a750fe91bbe630ffdfec9d49d87bcd8943c2))
* avoid to display maintenance mode when no logged ([e81c9c3](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e81c9c3dcfc96f6b846391e1402455cd9039c0f5))
* docker entrypoint ([e001d34](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e001d348908e1f14d51b506c23ccd587d6525110))

### [3.2.19](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.18...v3.2.19) (2022-06-15)


### Bug Fixes

* maintenance mode with server offline ([508aa25](https://gitlab.unige.ch/solidify/solidify-frontend/commit/508aa25ace578cd49aa30c202492b92874243a37))

### [3.2.18](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.17...v3.2.18) (2022-06-15)


### Features

* add boilerplate file for solidify consumer apps ([5ebd1f9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5ebd1f99eb7283bd26f30b9bf803a07d7e148708))


### Bug Fixes

* lint error ([127714a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/127714acbb94e953059dd06e5469f92d89aa4766))

### [3.2.17](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.16...v3.2.17) (2022-06-14)


### Bug Fixes

* **upload file:** allow to view upload progression even with gzip ([1044f48](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1044f481dd82041cef7c967f22f9edc6a3140f53))

### [3.2.16](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.15...v3.2.16) (2022-06-10)


### Bug Fixes

* **rich text editor:** workarround to display correctly line break ([be7fafe](https://gitlab.unige.ch/solidify/solidify-frontend/commit/be7fafed092f7d77638f5ad65e0db2e84939424d))

### [3.2.15](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.14...v3.2.15) (2022-06-09)


### Bug Fixes

* [AoU-1255] Check duplicates before adding element in a list ([11c2396](https://gitlab.unige.ch/solidify/solidify-frontend/commit/11c2396dcd7e0711dd9d945f34d7b470e9b071b1))
* **rich text editor:** change tag strong and em to b and i and fix style of quill tooltip ([a2fef10](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a2fef109d78ce27820b5ca7973f06ef1f7d849ea))

### [3.2.14](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.13...v3.2.14) (2022-06-08)


### Features

* overlay position helper and allow to position content below input by default ([fb172c5](https://gitlab.unige.ch/solidify/solidify-frontend/commit/fb172c55fe36b8ac3ad8fc90b52e9b997c9ef15b))

### [3.2.13](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.12...v3.2.13) (2022-06-07)


### Features

* allow to define hightlight cell and add field as data-field attribute on cells ([c2f8ccd](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c2f8ccd6b7b0ea5efb7e8b28a97124d45665496c))

### [3.2.12](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.11...v3.2.12) (2022-06-07)


### Features

* **searchable tree multi select:** expand tree node when checked ([6efba18](https://gitlab.unige.ch/solidify/solidify-frontend/commit/6efba1890431a476265854fd9506e8ab24063f7a))

### [3.2.11](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.10...v3.2.11) (2022-06-02)


### Bug Fixes

* add variable for theme name ([929f8e2](https://gitlab.unige.ch/solidify/solidify-frontend/commit/929f8e2cd53da54515dd6db40b5592a61844fd37))

### [3.2.10](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.9...v3.2.10) (2022-06-02)


### Features

* add new error dto attributes ([4f0b268](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4f0b2689636e5bd2774492039c9690857710a5a8))

### [3.2.9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.8...v3.2.9) (2022-06-01)


### Bug Fixes

* autoscroll to error on abstract form or parents if element ref is missing ([3e17a42](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3e17a42873db44a40e098858c61e1bc658a93251))

### [3.2.8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.7...v3.2.8) (2022-05-25)


### Bug Fixes

* **rich text:** add translation to toolbar button tooltip ([ba528a8](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ba528a84b43d888e59cad2f08cdad0bbe061fdcd))
* **rich text:** improve toolbar ([7b6109c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7b6109cf9248f3d63ef20a8ead0bd8fa17883d21))

### [3.2.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.6...v3.2.7) (2022-05-25)


### Features

* autoscroll to first validation error ([c5a52b2](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c5a52b2fb1ac954a05d2f6a20813dd01d5a98c8f))

### [3.2.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.5...v3.2.6) (2022-05-24)


### Bug Fixes

* **rich text input:** clean wrapping tag when useless ([a540244](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a54024407752a7bbac86ccdcff5fc3e074ab262b))

### [3.2.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.4...v3.2.5) (2022-05-24)


### Bug Fixes

* **rich text:** when there is no max length ([b433e63](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b433e63c851fe54f67e3ed724fd11823b9a4dab5))

### [3.2.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.3...v3.2.4) (2022-05-23)


### Features

* [AoU-1183] add limit size for rich text area ([96130fc](https://gitlab.unige.ch/solidify/solidify-frontend/commit/96130fc5a3eed6e52277f826d72741589ebe23cd))


### Bug Fixes

* **rich text:** prevent user to exceed limit of char and allow to display raw input ([02e704c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/02e704c1bbfe32915e6e68c99fa22a3fbbceeddc))
* style rich text editor like material input ([b243da2](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b243da265e2280fe8965433759a9ab154d3a96c2))

### [3.2.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.2...v3.2.3) (2022-05-18)


### Features

* new banner for special announcements ([9cb7713](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9cb77138d14facd6a923a6ade4fcbfed0b2fe583))


### Bug Fixes

* lint error ([fe03f7b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/fe03f7b2dbb7fed8dfeb63cc9d9413cfc2841ca7))
* prevent guard to be resolved before auth process is done ([eec3233](https://gitlab.unige.ch/solidify/solidify-frontend/commit/eec3233cb796b309f972770eb2be7cc068dde865))

### [3.2.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.1...v3.2.2) (2022-05-17)


### Features

* allow partal url on allowed url parameters ([dfaa61d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/dfaa61dd12fd7c41ccc3325500f54f6306bc6e7f))

### [3.2.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.2.0...v3.2.1) (2022-05-17)


### Features

* **url input navigator:** allow to provide callback or prefix for navigation button ([98d34ff](https://gitlab.unige.ch/solidify/solidify-frontend/commit/98d34ffd019e4b6b021287b834624d54b5e6fbfe))

## [3.2.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.31...v3.2.0) (2022-05-09)


### Features

* add rich text editor ([3c952ca](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3c952caa7d6968d2bbc9b8e9cf9b5529f14ada05))
* searchable multi select tree ([24f981f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/24f981f49bbd954cbc1e4ac9e3dad7a9cfc9088a))

### [3.1.31](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.30...v3.1.31) (2022-05-09)


### Features

* **Relation2TiersState:** add new links before deleting old ones when updating relation ([2841c3c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2841c3c8f7d5e0b38fbf514a06fcf18ebd2e46eb))
* **Relation2TiersState:** new property to configure the order of the create/delete operations during update ([dcd26f7](https://gitlab.unige.ch/solidify/solidify-frontend/commit/dcd26f796c84662e097597651a5c02b4c121d86b))


### Bug Fixes

* merge Relation2TiersOptions default options with BaseState default options ([b093118](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b09311851ada06017a09d17075f9894ac532efce))
* use correct state options ([acd46e1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/acd46e17f43d5e45f60a231cb36c080837bf0648))

### [3.1.30](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.29...v3.1.30) (2022-05-05)


### Bug Fixes

* redirect to requested page if interceptor request login ([51517b9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/51517b9209c42e2d60aca64cbf1951e01260c6c3))

### [3.1.29](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.28...v3.1.29) (2022-05-02)


### Bug Fixes

* display error when 404 ([c814d87](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c814d87f450a37774e102fa44c7918f520952a62))

### [3.1.28](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.27...v3.1.28) (2022-05-02)


### Features

* allow to display a second line on snackbar and allow to copy error message if present ([85dd324](https://gitlab.unige.ch/solidify/solidify-frontend/commit/85dd3246cc8dbbc2222a752e30219a76da2f42b4))


### Bug Fixes

* do not return null is abstract form is not the expected interface ([41b37a1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/41b37a194800fdee3316ee560d909d46b0b94df7))

### [3.1.27](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.26...v3.1.27) (2022-04-28)


### Features

* **error handler:** display backend message when available on error snackbar ([453e894](https://gitlab.unige.ch/solidify/solidify-frontend/commit/453e894b6b4d58135ffe3eeeb86cab70dc079b8c))

### [3.1.26](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.25...v3.1.26) (2022-04-28)


### Bug Fixes

* is loading counter for association state ([f62467e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f62467e027309a320e4dfaccb7830bf787e7bb36))

### [3.1.25](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.24...v3.1.25) (2022-04-27)


### Bug Fixes

* lint error ([383fd6c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/383fd6cf78df85dd89b772485b95907ba2e74c03))
* template with angularCompilerOptions strictTemplates option ([5c71e35](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5c71e3500fe1a1c6098349a194bfb46773569947))

### [3.1.24](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.23...v3.1.24) (2022-04-19)


### Bug Fixes

* make some adjustment ([be392bf](https://gitlab.unige.ch/solidify/solidify-frontend/commit/be392bf2b44af5ae46dc36bcf34c0342494ee0c0))

### [3.1.23](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.22...v3.1.23) (2022-04-11)


### Features

* allow to provide extra info to overlay ([4a2c086](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4a2c086cfcd27d92401557745c4532fed9bc5fe2))


### Bug Fixes

* cookie banner z index ([e4a47ae](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e4a47aecb27d4e3bf6d8fb05114ade49f4f2ec61))

### [3.1.22](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.21...v3.1.22) (2022-04-08)


### Features

* **overlay:** add delay before open overlay ([681bd7d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/681bd7d651855cf60c0126560f748aa760cd17cf))

### [3.1.21](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.20...v3.1.21) (2022-04-08)


### Bug Fixes

* **overlay:** create getter and setter and method detect data update ([f1b429b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f1b429b4daadecd0721d57801387f8a03802e2e5))

### [3.1.20](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.19...v3.1.20) (2022-04-07)


### Bug Fixes

* prevent to display snackbar error for error ExpressionChangedAfterItHasBeenCheckedError ([a7586b4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a7586b46f3f25b817f03d21158f7951b87f351cf))

### [3.1.19](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.18...v3.1.19) (2022-04-07)


### Bug Fixes

* overlay on single searchable select ([f5e1748](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f5e1748baa3d4af4a2a71db0ae8e8ce9f78be6ab))

### [3.1.18](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.17...v3.1.18) (2022-04-07)


### Bug Fixes

* allow to customize multiple searchable select extra info in chip ([9ca73ea](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9ca73ea82b4b3f66ee4649e87820c6a46b55ff0f))

### [3.1.17](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.16...v3.1.17) (2022-04-07)


### Bug Fixes

* display of pdf file ([9bf464f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9bf464f020010be0d11f5457fe7568109b4d6f9d))

### [3.1.16](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.15...v3.1.16) (2022-04-07)


### Bug Fixes

* markdown viewer no display content ([84ef7b3](https://gitlab.unige.ch/solidify/solidify-frontend/commit/84ef7b31dba41c3f293adc57b2568d4ff70256ad))
* **NativeNotificationService:** do not close notifications automatically to allow them to be seen in all environments ([fd3ab38](https://gitlab.unige.ch/solidify/solidify-frontend/commit/fd3ab38d92b60746c67b98350aa86f687ed810e1))

### [3.1.15](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.14...v3.1.15) (2022-04-06)


### Features

* display release notes and changelog and center info on about pages ([8ca3997](https://gitlab.unige.ch/solidify/solidify-frontend/commit/8ca3997dc3bae55ebabf03cfed4c37f3dbb5846b))


### Bug Fixes

* lint error ([05bb510](https://gitlab.unige.ch/solidify/solidify-frontend/commit/05bb51088ed41dfff0169065cc1f639612ef7d23))

### [3.1.14](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.13...v3.1.14) (2022-04-06)


### Bug Fixes

* **oauth:** force to clean oauth info when revoke token fall in error ([ac576be](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ac576be062d0aa1d9b4cd823d77dd4dd6b24a03d))
* **oauth:** retry to login when refresh token is missing or wrong ([707a576](https://gitlab.unige.ch/solidify/solidify-frontend/commit/707a576b1353da273f71f5f0c61e4b8e84d6211b))

### [3.1.13](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.12...v3.1.13) (2022-04-05)


### Bug Fixes

* prevent opening a docx preview for a file other than docx ([815b873](https://gitlab.unige.ch/solidify/solidify-frontend/commit/815b87396f120c040ae0aed84bf9f477a421c324))

### [3.1.12](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.11...v3.1.12) (2022-04-04)


### Features

* display release notes on about page ([0e28fed](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0e28feddc2ff1d93941e073ed686196cd1b49372))

### [3.1.11](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.10...v3.1.11) (2022-04-01)


### Bug Fixes

* manage tooltip on ellipsis in mat select value selected and options ([4506669](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4506669cfa801755a7bdded9300dc3a39d1bac79))

### [3.1.10](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.9...v3.1.10) (2022-03-31)


### Bug Fixes

* display bad request when error 400 without validation errors ([c84bdb9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c84bdb90eee11594602913692b116a010f207e33))

### [3.1.9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.8...v3.1.9) (2022-03-31)

### [3.1.8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.7...v3.1.8) (2022-03-31)

### [3.1.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.6...v3.1.7) (2022-03-31)


### Bug Fixes

* avoid on error when delete on abstract list with a name null ([05286f4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/05286f4d389478caffa4a54fc6bb3ac76039b793))
* default value for highlight js theme ([875ddf6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/875ddf67295289c8073d7bdbe508d560f4449c6b))

### [3.1.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.5...v3.1.6) (2022-03-30)


### Bug Fixes

* clear oauth storage info if error during revokation of token ([b7035e1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b7035e1f851ec042c692b84de0af3b81e814da1d))
* on firefox unable to enter in edit mode when click on form ([9726af6](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9726af64de26be52f6d57ccad07810ec35b19221))
* url query parameters methot that check contains param ([222a702](https://gitlab.unige.ch/solidify/solidify-frontend/commit/222a70242682c2d422b3556f5c8ff1ee3f69502f))

### [3.1.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.4...v3.1.5) (2022-03-30)


### Bug Fixes

* avoid error when using button orcid with formcontrol ([767db68](https://gitlab.unige.ch/solidify/solidify-frontend/commit/767db6893f8709068bbe931b3f891d056c19ec6b))
* **datatable:** alignment of description column in status history ([2510348](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2510348799fcf75d8377bbd2ba1acd082f692c02))

### [3.1.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.3...v3.1.4) (2022-03-28)


### Bug Fixes

* manage correctly authorization module info ([804a976](https://gitlab.unige.ch/solidify/solidify-frontend/commit/804a9761b8ca003a819c203effe4f21a0ce149cb))

### [3.1.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.2...v3.1.3) (2022-03-25)

### [3.1.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.1...v3.1.2) (2022-03-25)


### Features

* allow to display overlay on single searchable select ([380a81a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/380a81a94f0e0260205be77d9c8b264bb675cf5c))
* allow to provide overlay on multi searchable select ([e20981a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e20981a27078217479d6509a543569e051d28bfa))


### Bug Fixes

* lint error ([3dae4d0](https://gitlab.unige.ch/solidify/solidify-frontend/commit/3dae4d04750e3986a2fd77bf7645856a72379b5d))
* overlay on single select with default value ([13a1d91](https://gitlab.unige.ch/solidify/solidify-frontend/commit/13a1d91226c5adfb4159dcb4f91b02d236ae8153))
* prevent click when click on multi select extra info ([6efc7e4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/6efc7e4938a49e65426900bf951131ea9b882a65))

### [3.1.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.1.0...v3.1.1) (2022-03-24)


### Bug Fixes

* validation error not reported to the form ([a394219](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a39421906fdcaa320f0254843ee6109808256e9f))

## [3.1.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.21...v3.1.0) (2022-03-23)


### Features

* display url and version of backend modules ([cb959a3](https://gitlab.unige.ch/solidify/solidify-frontend/commit/cb959a3e8fd4dd1a59eb873a75fd3ead1e5b455a))


### Bug Fixes

* improve default index for datatable ([a3e0fe9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a3e0fe9eb61ea190fcff63287fcf3b4bf223d7b9))
* lint error ([28a3afa](https://gitlab.unige.ch/solidify/solidify-frontend/commit/28a3afa7594320f0bfc1a27b4e5fef2c8048e2dd))

### [3.0.21](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.20...v3.0.21) (2022-03-15)


### Features

* display changelog into update dialog ([aabcb3d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/aabcb3dc8c60beab90238c86d08b492103a2bd81))

### [3.0.20](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.19...v3.0.20) (2022-03-11)


### Features

* allow navigator to send notification to user ([8e693d7](https://gitlab.unige.ch/solidify/solidify-frontend/commit/8e693d73c9853093a0c85e39af36e5c1c5b9f88d))


### Bug Fixes

* lint error ([61890dc](https://gitlab.unige.ch/solidify/solidify-frontend/commit/61890dc9e3332900086fc25c22e41ba178637e2d))

### [3.0.19](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.18...v3.0.19) (2022-03-11)


### Features

* allow to define key type for mapping object ([2087833](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2087833bbb47d7dd68ddb64c66c009dd491ffc7f))

### [3.0.18](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.17...v3.0.18) (2022-03-11)


### Bug Fixes

* lint error for project that copy script extract git version ([8d06222](https://gitlab.unige.ch/solidify/solidify-frontend/commit/8d062221dbf9dfb9cd1c1150a6f6e8d7884c34f4))

### [3.0.17](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.16...v3.0.17) (2022-03-10)


### Features

* add citation component ([c82d7a7](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c82d7a708e09db154e848d5e8e5774c268ae220f))

### [3.0.16](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.15...v3.0.16) (2022-03-10)


### Features

* avoid version file to be stored in browser cache and add date ([f222db5](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f222db5883f10acf21efd577b7c2699b6e19903e))

### [3.0.15](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.14...v3.0.15) (2022-03-08)


### Bug Fixes

* allow to preserve query param after authentification and refacto url query param util ([c1c1580](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c1c1580eb8dc0ca1309909e0d6139b5f7c8aa036))
* check queryParams in state redirect url ([bb7f24a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/bb7f24a1493c4ebd5369a5fbf7bb55c47fe9428d))
* remove ? from queryParams ([5f03c1c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5f03c1cd7718d6b9b19e51053406b92f86010e5d))

### [3.0.14](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.13...v3.0.14) (2022-03-04)


### Bug Fixes

* mark as dirty form attached to single select when value deleted ([d1d2fab](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d1d2fab38f7e5bbebdc58fff5ec2c71f98e553cb))
* pipe error "Method expression is not of Function type" in IDE when use it in html ([0861175](https://gitlab.unige.ch/solidify/solidify-frontend/commit/086117596377dd8cfcb573295a4a6db362a8b140))

### [3.0.13](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.12...v3.0.13) (2022-03-02)


### Features

* manage dark mode ([be4e880](https://gitlab.unige.ch/solidify/solidify-frontend/commit/be4e880e9ae30d5305294871591e5d0d54995e7e))

### [3.0.12](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.11...v3.0.12) (2022-03-02)


### Bug Fixes

* avoid to display error when token is missing on route that not require auth ([ffea010](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ffea0108e5e4e30d56a17006e31f8a239ae26f45))

### [3.0.11](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.10...v3.0.11) (2022-02-15)


### Bug Fixes

* add type for html input event ([82593ef](https://gitlab.unige.ch/solidify/solidify-frontend/commit/82593ef8575cca3aa846a78cf664f7ef2611e847))
* avoid to send to backend null access or refresh token ([0238ce4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0238ce418403cf7a2d7795d3c2da919cf18d3404))
* lint error ([c4eb9ed](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c4eb9edf81b9741f4a406f67208c9cffd8ef7f50))

### [3.0.10](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.9...v3.0.10) (2022-02-11)


### Bug Fixes

* avoid to see server offline page without translation ([706ee10](https://gitlab.unige.ch/solidify/solidify-frontend/commit/706ee102d5bdf2a1fa960a6030c9859a4120de48))
* **cookie consent:** add management of download token ([2e1c5a4](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2e1c5a435f1fe4944fa31d5f71fd9cc58800921a))
* display dedicated page depending if server is offline or app unable to load ([93bc17c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/93bc17c6f120fad8d95c7abedd7a786c6bea1d56))

### [3.0.9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.8...v3.0.9) (2022-02-10)


### Bug Fixes

* import extract-licenses from npm instead of github ([6e87180](https://gitlab.unige.ch/solidify/solidify-frontend/commit/6e87180890a932df07872584063c54711dd33a71))

### [3.0.8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.7...v3.0.8) (2022-02-10)


### Bug Fixes

* language selector is in error if theme override at runtime ([5db427f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5db427f09b7792a808a7f79d01d3d1861e717779))

### [3.0.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.6...v3.0.7) (2022-02-09)

### [3.0.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.5...v3.0.6) (2022-02-07)


### Bug Fixes

* manage error durring init application and logging ([628e4d5](https://gitlab.unige.ch/solidify/solidify-frontend/commit/628e4d5e4d8baeee18375ea10b1c92878b85a5f9))

### [3.0.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.4...v3.0.5) (2022-02-04)


### Features

* implement elasticsearch apm ([d90623d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/d90623d8bb363c0785df98dc50f1152078e46aa5))


### Bug Fixes

* [DLCM-2043] allow to ignore update dialog ([950ce32](https://gitlab.unige.ch/solidify/solidify-frontend/commit/950ce32aef8c544aafd06f87e659854d55413ca3))

### [3.0.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.3...v3.0.4) (2022-02-03)


### Bug Fixes

* [DLCM-2066] force to reload language after theme change ([31aae7d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/31aae7d0493555d6a97d323e91c1fa9ef2457ebe))

### [3.0.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.2...v3.0.3) (2022-02-03)

### [3.0.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.1...v3.0.2) (2022-02-02)

### [3.0.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v3.0.0...v3.0.1) (2022-02-02)


### Features

* [DLCM-2022] display in list action for copy id and show history ([b5d9e8c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b5d9e8cf9fbc5b246f35096fcf63fc3d7475e73d))


### Bug Fixes

* add tootlip on status with ellipsis ([b6e089c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b6e089c04037005ef26c8693701a8a82b5d3856b))
* lint config and lint error ([10fafce](https://gitlab.unige.ch/solidify/solidify-frontend/commit/10fafcee440c791264f0854403c138ca72775924))

## [3.0.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.15...v3.0.0) (2022-01-27)

### [2.4.15](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.14...v2.4.15) (2022-01-21)


### Bug Fixes

* [DLCM-2024] allow user to change own role from manager to anything else ([bd5fb31](https://gitlab.unige.ch/solidify/solidify-frontend/commit/bd5fb316a5a391ab18e4a76180a1527afa60f4ef))

### [2.4.14](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.13...v2.4.14) (2022-01-20)


### Bug Fixes

* [DLCM-2031] highligth tag in searchable select is displayed in some cases ([1df943d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/1df943d1676f3bffde4045e41eef6c884fa8f492))
* [DLCM-2032] manage download token with url with parameters ([c67485d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c67485dba354b87dc803eee8e5f4320821287751))

### [2.4.13](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.12...v2.4.13) (2022-01-18)

### [2.4.12](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.11...v2.4.12) (2022-01-18)


### Bug Fixes

* [DLCM-2023] molecul preview ([1984630](https://gitlab.unige.ch/solidify/solidify-frontend/commit/19846302ceaef572886fae30243d4e7f61775226))

### [2.4.11](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.10...v2.4.11) (2022-01-17)


### Features

* [DLCM-2007] manage cookie consent ([7885acd](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7885acd78b0306ea6c1527b8cea89e4000659978))

### [2.4.10](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.9...v2.4.10) (2022-01-17)


### Features

* [DLCM-2002] manage pluralization in translate ([f155bbc](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f155bbce4bfedb0d0cbc641afb8886ca6d572e48))

### [2.4.9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.8...v2.4.9) (2022-01-14)


### Features

* [DLCM-1985] apply sub action intention ([172426f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/172426f433614a899daf7d1c43c31a0482636c62))

### [2.4.8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.7...v2.4.8) (2022-01-14)


### Bug Fixes

* **http status:** [DLCM-2008] update enum http status code ([19d82db](https://gitlab.unige.ch/solidify/solidify-frontend/commit/19d82db92a38a550c5cce063f049ca38fa477c55))

### [2.4.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.6...v2.4.7) (2021-12-23)


### Bug Fixes

* refresh token ([f1a97bd](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f1a97bd9865954fafd47f0ad6292a5609d62a2b7))

### [2.4.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.5...v2.4.6) (2021-12-22)


### Bug Fixes

* display in error multi select with default value when no default value selected ([bee0d31](https://gitlab.unige.ch/solidify/solidify-frontend/commit/bee0d31b96e7a245a6c6b88678ec34a9d1e2c146))

### [2.4.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.4...v2.4.5) (2021-12-22)


### Bug Fixes

* manage case when last selection is no more available ([b67c910](https://gitlab.unige.ch/solidify/solidify-frontend/commit/b67c910059e46d3e57c3235ff5b005d176094a8e))

### [2.4.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.3...v2.4.4) (2021-12-21)


### Bug Fixes

* dialog util by injecting solidify class systematically ([ba81cf9](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ba81cf9297175d2bdfa3f90496c6c6f1d2eb756e))

### [2.4.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.2...v2.4.3) (2021-12-20)

### [2.4.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.1...v2.4.2) (2021-12-17)


### Features

* [DLCM-1985] improve dispatch sequential and parallel to manage sub action intention and manage timeout ([a4850c5](https://gitlab.unige.ch/solidify/solidify-frontend/commit/a4850c5db25d843ea6de304c145b0744d3e2ef42))


### Bug Fixes

* inner html on multi searchable select chip ([7dd34a3](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7dd34a30a2a92628f3255036818b53baded7a0b0))

### [2.4.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.4.0...v2.4.1) (2021-12-16)


### Bug Fixes

* required field in red when empty ([4afe3df](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4afe3df5b30b4666e44f44a9eff1d88b2da0a455))

## [2.4.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.3.9...v2.4.0) (2021-12-16)


### Bug Fixes

* [DLCM-1979] create missing success and fail actions for association state ([73a25d5](https://gitlab.unige.ch/solidify/solidify-frontend/commit/73a25d5d6950bc165486bc07d9447e6243783aaa))

### [2.3.9](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.3.8...v2.3.9) (2021-12-03)


### Features

* **preview:** [DLCM-1890] manage preview of docx ([9d90d05](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9d90d05148cbdd3f915f3b47c6a6776cb092dda1))


### Bug Fixes

* **datatable:** [DLCM-1953] spinner are missing ([7ee22be](https://gitlab.unige.ch/solidify/solidify-frontend/commit/7ee22beb604a0b2a3096a92bbe17c5749d9d49f6))

### [2.3.8](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.3.7...v2.3.8) (2021-12-02)

### [2.3.7](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.3.6...v2.3.7) (2021-12-01)


### Features

* [DLCM-1959] add user full name in change log section ([2fff51a](https://gitlab.unige.ch/solidify/solidify-frontend/commit/2fff51ae9d0e43ce7131971e3a88e3d8a442c56b))


### Bug Fixes

* **drag drop:** [DLCM-1964] drag and drop to change file relative location ([bb90d08](https://gitlab.unige.ch/solidify/solidify-frontend/commit/bb90d08a5b1fb6a351fd8b22cd14217d4942b66e))
* focus typing error ([ae5d5bd](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ae5d5bdd0a157996e7c6b90cf5695f163d08fadc))

### [2.3.6](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.3.5...v2.3.6) (2021-11-24)


### Bug Fixes

* eslint to allow to work on solidify ([20701af](https://gitlab.unige.ch/solidify/solidify-frontend/commit/20701afa05617e7f6fd48392637bc76289e4fe13))

### [2.3.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.3.4...v2.3.5) (2021-11-24)


### Features

* allow to display input required without value in error ([bbc5658](https://gitlab.unige.ch/solidify/solidify-frontend/commit/bbc5658c22cc4e43cf58b4681169e10ffca1b13c))
* provide default setting for config and allow to display debug information in production ([ab809ec](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ab809ec5be85262905440bf6c53b2c667cf5206e))

### [2.3.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.3.3...v2.3.4) (2021-11-19)


### Features

* **preview:** [DLCM-1889] manage preview for tiff file ([e5abce2](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e5abce2a0e3e5a7f2e8769b68bf4251be451a21f))

### [2.3.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.3.2...v2.3.3) (2021-11-16)


### Bug Fixes

* [DLCM-1918] remove routing hash strategy ([4d346fd](https://gitlab.unige.ch/solidify/solidify-frontend/commit/4d346fd2d7d261dd7c8161ffd428671840e27c62))

### [2.3.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.3.1...v2.3.2) (2021-11-09)


### Bug Fixes

* [AOU-1069] keyword separator comma wrongly named ([0af69d1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/0af69d118c9380f65875047fa873c2a9c3f61a27))

### [2.3.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.3.0...v2.3.1) (2021-11-04)


### Bug Fixes

* [DLCM-1903] improve tooltip zone on searchable select ([9225e5e](https://gitlab.unige.ch/solidify/solidify-frontend/commit/9225e5eedabd8fa481295b14d675ad2c9ebf720c))

## [2.3.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.2.0...v2.3.0) (2021-11-04)


### Features

* add composition clean method ([130123d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/130123dc918baa1f594771188f45680cdbb0ec41))

## [2.2.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.1.3...v2.2.0) (2021-11-01)


### Features

* **download:** [DLCM-1743] allow to use download with download token ([c1c572b](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c1c572bea9cbab06e825153b606befea9082412c))


### Bug Fixes

* lint error on js doc ([3280566](https://gitlab.unige.ch/solidify/solidify-frontend/commit/328056609bd21d6c14104f13bb9868aa541a4bf9))

### [2.1.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.1.2...v2.1.3) (2021-10-26)


### Bug Fixes

* disable ivy build in prod ([e727c0d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e727c0de1c7c3fa11f6d92f0c4171db29dd06b54))

### [2.1.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.1.1...v2.1.2) (2021-10-26)


### Features

* allow to make panel expandable title clickable ([153c41f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/153c41f45164e69295f60761a3471ca0b4b48b52))


### Bug Fixes

* add class on banner to allow overflow ([ea26b9d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ea26b9d2f401f33b503eb235ea4d1c4477d3bf86))
* build with ivy ([75f4255](https://gitlab.unige.ch/solidify/solidify-frontend/commit/75f42555a3c7ab6c532796f469548dc23ab2c882))
* remove unused material design icon lib ([f7b8b25](https://gitlab.unige.ch/solidify/solidify-frontend/commit/f7b8b256e5272c04a8001b6bdcaee7f73314a16e))

### [2.1.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.1.0...v2.1.1) (2021-10-12)


### Features

* [AOU-1891] migrate TSLint to ESLint ([e93dcc8](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e93dcc8c2911ea8eb367aa2409fa2594e790834b))

## [2.1.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.0.5...v2.1.0) (2021-10-12)


### Features

* display tooltip on inner html cell in datatable ([351fa01](https://gitlab.unige.ch/solidify/solidify-frontend/commit/351fa019aec4886023440af778a3d637d583b871))

### [2.0.5](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.0.4...v2.0.5) (2021-10-05)


### Features

* add util for url ([5859941](https://gitlab.unige.ch/solidify/solidify-frontend/commit/5859941ec967b6936f7e0d0adf2a62c02ec51dad))

### [2.0.4](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.0.3...v2.0.4) (2021-10-01)


### Bug Fixes

* [DLCM-1888] fix preview zoom and refactor code ([43a2ba1](https://gitlab.unige.ch/solidify/solidify-frontend/commit/43a2ba1209e251fe74ffbd04c41409e30d5917ea))

### [2.0.3](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.0.2...v2.0.3) (2021-10-01)


### Bug Fixes

* [DLCM-1901] remove override and overrideProperty to use new typescript override feature ([c787f7f](https://gitlab.unige.ch/solidify/solidify-frontend/commit/c787f7f33788f4b5a2ee9bcbd444b034c286f8fc))

### [2.0.2](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.0.1...v2.0.2) (2021-09-30)


### Features

* allow to add behaviod on model update ([e9b75c2](https://gitlab.unige.ch/solidify/solidify-frontend/commit/e9b75c2b5d7c517087bd2c7855af94037cb0fd93))

### [2.0.1](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v2.0.0...v2.0.1) (2021-09-24)


### Bug Fixes

* preview sound ([883796c](https://gitlab.unige.ch/solidify/solidify-frontend/commit/883796ca6c7a97dc06419551af8af119ec846f4f))
* readd tslint ([ad485db](https://gitlab.unige.ch/solidify/solidify-frontend/commit/ad485db11ed046ba9f483c745ef0a9bc6ec6f0cd))

## [2.0.0](https://gitlab.unige.ch/solidify/solidify-frontend/compare/v1.8.4...v2.0.0) (2021-09-23)


### Features

* [DLCM-1774] migration to angular 11 ([44bc90d](https://gitlab.unige.ch/solidify/solidify-frontend/commit/44bc90da583d2733404e2a3aa066f304db987983))
* [DLCM-1774] migration to angular 12 ([cc5c719](https://gitlab.unige.ch/solidify/solidify-frontend/commit/cc5c71929959eb74d01c21ba84d23af01c907624))

### [1.8.4](https://gitlab.unige.ch///compare/v1.8.3...v1.8.4) (2021-09-17)


### Bug Fixes

* add oauth prefix on token endpoint and login url ([2326bde](https://gitlab.unige.ch///commit/2326bde5ba7a66b065d63f765af632da4414e295))

### [1.8.3](https://gitlab.unige.ch///compare/v1.8.2...v1.8.3) (2021-09-17)


### Bug Fixes

* allow to preview file who need to be confirmed ([4ba15d7](https://gitlab.unige.ch///commit/4ba15d79ea0f1b98032e0fe863b36f123b8e42ae))

### [1.8.2](https://gitlab.unige.ch///compare/v1.8.1...v1.8.2) (2021-09-17)


### Bug Fixes

* display tooltip on column action ([ec41b21](https://gitlab.unige.ch///commit/ec41b21644fc4378e0410bc9cd49269c14fbeaaf))

### [1.8.1](https://gitlab.unige.ch///compare/v1.8.0...v1.8.1) (2021-09-17)


### Features

* allow to display action button on column ([01b4651](https://gitlab.unige.ch///commit/01b46517812b41072ac0a8b0e9aabff9e2a4a738))
* improve store util dispatch action to support fail case ([6e93c1d](https://gitlab.unige.ch///commit/6e93c1d67227ca68da616dfd51fdbf1ceb50242c))

## [1.8.0](https://gitlab.unige.ch///compare/v1.7.2...v1.8.0) (2021-09-16)


### Features

* allow to download in memory when size less than a minimal ([b55dacb](https://gitlab.unige.ch///commit/b55dacbb317dfe27bd925be67409ce01ea9aa6b1))


### Bug Fixes

* display notification when download in memory ([66f6559](https://gitlab.unige.ch///commit/66f6559bbf1d08525996f3dbc954e8e7c5f6ae00))

### [1.7.2](https://gitlab.unige.ch///compare/v1.7.1...v1.7.2) (2021-09-14)


### Bug Fixes

* color button success on button toolbar list ([cb22645](https://gitlab.unige.ch///commit/cb2264530645ce290ef14a07b5b51c685f5ecb49))

### [1.7.1](https://gitlab.unige.ch///compare/v1.7.0...v1.7.1) (2021-09-07)

## [1.7.0](https://gitlab.unige.ch///compare/v1.6.3...v1.7.0) (2021-09-03)


### Features

* update datatable bulk action to extend extra button toolbar ([faf081f](https://gitlab.unige.ch///commit/faf081fba20c03e49a7445102a36ce5d8ae11fcd))

### [1.6.3](https://gitlab.unige.ch///compare/v1.6.2...v1.6.3) (2021-09-03)


### Bug Fixes

* add missing translation for tab container component ([4e128e5](https://gitlab.unige.ch///commit/4e128e5989bd31c01040d0c3ac6ce417ebace97a))
* merge method ([7d08349](https://gitlab.unige.ch///commit/7d0834901bd5cab92507d2fa6db66a51dc618ecf))

### [1.6.2](https://gitlab.unige.ch///compare/v1.6.1...v1.6.2) (2021-09-02)


### Features

* data table ([7e3f808](https://gitlab.unige.ch///commit/7e3f8089a16c3c32ec0e0a7cb959a1412dc8a833))

### [1.6.1](https://gitlab.unige.ch///compare/v1.6.0...v1.6.1) (2021-08-31)


### Bug Fixes

* translate hint in select component ([9a60b04](https://gitlab.unige.ch///commit/9a60b044128d5554305177d2cc2ef6cb96e3b071))

## [1.6.0](https://gitlab.unige.ch///compare/v1.5.23...v1.6.0) (2021-08-31)


### Features

* **keyword input:** [AOU-730] use key value instead of keycode ([3f87f79](https://gitlab.unige.ch///commit/3f87f79046fe79bcf9216a7624fcd7ab30c0df24))
* allow to display mat hint on muliple and single select ([8eeafa3](https://gitlab.unige.ch///commit/8eeafa3ec2ad11f49c5161c733e58fd244fce89c))

### [1.5.23](https://gitlab.unige.ch///compare/v1.5.22...v1.5.23) (2021-08-30)


### Bug Fixes

* add css class on panel collapsed ([d1b2f79](https://gitlab.unige.ch///commit/d1b2f7988c3d6988fe6d1400a45b660ba16c0110))

### [1.5.22](https://gitlab.unige.ch///compare/v1.5.21...v1.5.22) (2021-08-30)


### Features

* improve panel expandable ([b1a43ac](https://gitlab.unige.ch///commit/b1a43ac87b28103e18b8eb0326563aae5f41df7f))

### [1.5.21](https://gitlab.unige.ch///compare/v1.5.20...v1.5.21) (2021-08-25)


### Bug Fixes

* [AoU-876] Allow preview of files that have status Confirmed ([a60e7f0](https://gitlab.unige.ch///commit/a60e7f0b4185695201f8ece45fb1efbd6bab310d))

### [1.5.20](https://gitlab.unige.ch///compare/v1.5.19...v1.5.20) (2021-08-23)


### Bug Fixes

* allow multiple and single select to detect invalid state when become invalid ([46d4085](https://gitlab.unige.ch///commit/46d4085f7843ab258ecc50d65b7c0410918debb8))

### [1.5.19](https://gitlab.unige.ch///compare/v1.5.18...v1.5.19) (2021-08-17)


### Features

* [AOU-342] add interceptor to set accept language in header ([c793f3b](https://gitlab.unige.ch///commit/c793f3bf8b702902fd4d5ddb55285edd5a574e32))


### Bug Fixes

* prevent updating header for language when value is missing ([b33542a](https://gitlab.unige.ch///commit/b33542a3379fa3ca585b8e015a4d3b2cb08c48ef))

### [1.5.18](https://gitlab.unige.ch///compare/v1.5.17...v1.5.18) (2021-08-16)


### Features

* add tool isNullOrUndefinedOrEmptyArray ([4822abe](https://gitlab.unige.ch///commit/4822abee172f549c5c53d4876cb8929b00fc1054))
* allow to provide file size limit for upload ([ce06a70](https://gitlab.unige.ch///commit/ce06a70318ebedbf45d2fba64ad4067aad78d24a))


### Bug Fixes

* style of button on button toolbar ([1c5d464](https://gitlab.unige.ch///commit/1c5d46422bbcc57a0871799a231ee9ba7c36c719))

### [1.5.17](https://gitlab.unige.ch///compare/v1.5.16...v1.5.17) (2021-08-10)


### Features

* add abstract service for manage subscription ([406bbc7](https://gitlab.unige.ch///commit/406bbc7598c2dfb3c0849dd4b1e9bc0cc2be94f4))
* add dialog util ([b8fc457](https://gitlab.unige.ch///commit/b8fc4574c2eb4314847f4a9651b9d95ff9cb19c2))
* add store util to wait action completion without dispatch ([c3b26e4](https://gitlab.unige.ch///commit/c3b26e414375d9db01f19fa525e8632e73cc5242))
* apply abstract base service where needed ([6ea56bd](https://gitlab.unige.ch///commit/6ea56bd85c9509351de51929fc45b0f939526ff2))
* expose dialog util ([3128d27](https://gitlab.unige.ch///commit/3128d27c8b030398789034e0f57821b08c740bdf))
* update and use dialog util everywhere ([6c605b3](https://gitlab.unige.ch///commit/6c605b3897b15d9baa6c513fef75b32e3fccdb2e))


### Bug Fixes

* dialog util ([6ad2e89](https://gitlab.unige.ch///commit/6ad2e899010661babb84cef4ea74210ea248a970))

### [1.5.16](https://gitlab.unige.ch///compare/v1.5.15...v1.5.16) (2021-08-10)


### Bug Fixes

* allow to provide custom position on multi and single searchable select ([427e07a](https://gitlab.unige.ch///commit/427e07a50738954386cfaf30c1b647d4a43818c7))

### [1.5.15](https://gitlab.unige.ch///compare/v1.5.14...v1.5.15) (2021-08-09)


### Bug Fixes

* dispatch parallel action method ([2941995](https://gitlab.unige.ch///commit/2941995cfad524933bef7ab218f018030d289869))
* preview service without injectable keyword ([c75dda5](https://gitlab.unige.ch///commit/c75dda53bbc9602a245cf7c986113aab0e04a054))

### [1.5.14](https://gitlab.unige.ch///compare/v1.5.13...v1.5.14) (2021-08-05)


### Features

* add interface for solidify http error response ([f8a96c3](https://gitlab.unige.ch///commit/f8a96c37837bb96541292f8a776cc32618da1ae7))


### Bug Fixes

* typing when use is tool isNullOrUndefinedOrEmpty and isNullOrUndefinedOrWhiteString ([0c4eea6](https://gitlab.unige.ch///commit/0c4eea62f3f72d89fe886cd7e4ccd7aa69c54b7d))

### [1.5.13](https://gitlab.unige.ch///compare/v1.5.12...v1.5.13) (2021-08-05)


### Bug Fixes

* dialog button color when not data provided to dialog ([cf1dd54](https://gitlab.unige.ch///commit/cf1dd54e1fa53f181ab8bbdd074752e1eab0a581))

### [1.5.12](https://gitlab.unige.ch///compare/v1.5.11...v1.5.12) (2021-08-04)


### Bug Fixes

* display selected value on searchable panel when single or multiple ([199cac1](https://gitlab.unige.ch///commit/199cac14f213f1497b9fb950536593853b7217dc))

### [1.5.11](https://gitlab.unige.ch///compare/v1.5.10...v1.5.11) (2021-08-04)


### Features

* allow to use resource label callback on datatable cell ([0a3ec09](https://gitlab.unige.ch///commit/0a3ec09681319eb64fe54991bbdfc1055c8287b8))


### Bug Fixes

* display only current value select in searchable content ([9b8863f](https://gitlab.unige.ch///commit/9b8863f7a1ed4bbd42a1f6caa73746959b1bb10c))

### [1.5.10](https://gitlab.unige.ch///compare/v1.5.9...v1.5.10) (2021-08-03)


### Features

* add new is tool ([ab430b0](https://gitlab.unige.ch///commit/ab430b0ea1abd738ce6490f1c9c6da307aa5a05b))


### Bug Fixes

* allow to translate message in chip tooltip ([cdb59a9](https://gitlab.unige.ch///commit/cdb59a95a48b8f2737e69fc4193b9db1ab1cc1de))

### [1.5.9](https://gitlab.unige.ch///compare/v1.5.8...v1.5.9) (2021-07-30)

### [1.5.8](https://gitlab.unige.ch///compare/v1.5.7...v1.5.8) (2021-07-28)


### Features

* allow to conditionally hide datatable column ([098e51b](https://gitlab.unige.ch///commit/098e51b59db7d0ee09a140348e5d2f223cf10448))

### [1.5.7](https://gitlab.unige.ch///compare/v1.5.6...v1.5.7) (2021-07-28)


### Features

* allow to cusomize color of cancel button on dialog and add enum ([42ee1bc](https://gitlab.unige.ch///commit/42ee1bc87864a2dbe74a09a277de4048e1fa4714))
* allow to not focus first element on dialog ([04c31bc](https://gitlab.unige.ch///commit/04c31bcc3d74e591340bf080cb60206d013a8a43))

### [1.5.6](https://gitlab.unige.ch///compare/v1.5.5...v1.5.6) (2021-07-27)


### Features

* add option to customize render of button ([b3be4ff](https://gitlab.unige.ch///commit/b3be4ff4864e4e9b31ee59c7665117f65470b907))
* add option to define color of cancel button in confirm dialog ([6ef6a73](https://gitlab.unige.ch///commit/6ef6a7302ff3b3606e30c58a1aee3dd1304dec71))

### [1.5.5](https://gitlab.unige.ch///compare/v1.5.4...v1.5.5) (2021-07-16)


### Bug Fixes

* **keyword input:** improve treatment of keyword pasted ([e25e01a](https://gitlab.unige.ch///commit/e25e01a4a242d7d14b92f4eb890ad784c4238946))

### [1.5.4](https://gitlab.unige.ch///compare/v1.5.3...v1.5.4) (2021-07-15)

### [1.5.3](https://gitlab.unige.ch///compare/v1.5.2...v1.5.3) (2021-07-14)


### Features

* allow to override resource state url and association state url ([402c29e](https://gitlab.unige.ch///commit/402c29efba261472df5661d6b3ebbde2fbf043af))

### [1.5.2](https://gitlab.unige.ch///compare/v1.5.1...v1.5.2) (2021-07-13)


### Features

* add upload state ([7116f25](https://gitlab.unige.ch///commit/7116f251b698f4b69fb07df71bb9643461b02d3d))


### Bug Fixes

* add typing on ngxs operator ([5dfbb9c](https://gitlab.unige.ch///commit/5dfbb9c1917a39edb6208d18f0965d9f86f04e5d))
* allow to not provide second param parameter on notifier service method ([79321df](https://gitlab.unige.ch///commit/79321df8eb89d4b183bcb6b028b118b42d6e3895))
* lint error ([c2e7389](https://gitlab.unige.ch///commit/c2e7389dd6fcf0c0bc509372ad280a3e63b5e9ca))

### [1.5.1](https://gitlab.unige.ch///compare/v1.5.0...v1.5.1) (2021-07-09)


### Features

* add file drop zone directive ([7fec423](https://gitlab.unige.ch///commit/7fec423fb84c1c332df64d4fa69391b0b6949bda))

## [1.5.0](https://gitlab.unige.ch///compare/v1.4.7...v1.5.0) (2021-07-07)


### Features

* add tooltip on section ([56f9020](https://gitlab.unige.ch///commit/56f90209d1375aa23d5e6bb7ff8d6588a94dbfb9))

### [1.4.7](https://gitlab.unige.ch///compare/v1.4.6...v1.4.7) (2021-07-06)

### [1.4.6](https://gitlab.unige.ch///compare/v1.4.5...v1.4.6) (2021-07-06)


### Features

* add ofActionCompleted improved tool ([90c392d](https://gitlab.unige.ch///commit/90c392dea54d1fb22eac562dff4ea1b91e6e7790))
* add store util method to dispatch action and wait subaction completion ([be8331d](https://gitlab.unige.ch///commit/be8331d7f3985672eb7e1dec6779ade356e8ef9b))
* add typing for parent action inheritence on base sub action ([ec16106](https://gitlab.unige.ch///commit/ec161069d28cf4c8eb4834e33e02030da39ac90c))

### [1.4.5](https://gitlab.unige.ch///compare/v1.4.4...v1.4.5) (2021-06-30)


### Bug Fixes

* add method to scroll to bottom ([4c37f36](https://gitlab.unige.ch///commit/4c37f36dfb3ba3045fb653abfc5f3c75f2ad4286))

### [1.4.4](https://gitlab.unige.ch///compare/v1.4.3...v1.4.4) (2021-06-17)

### [1.4.3](https://gitlab.unige.ch///compare/v1.4.2...v1.4.3) (2021-06-15)


### Features

* allow to defined is highlight condition in ts inheritence of list routable ([ce9e395](https://gitlab.unige.ch///commit/ce9e39568c52ae452733097ef7ea96a0c709f2eb))
* allow to highlight multiple term on searchable select ([86d63a9](https://gitlab.unige.ch///commit/86d63a9b3612e95be8fc46145d71b012d92d394b))


### Bug Fixes

* dispatch clean action instead of call clean method on resource state ([f75b04e](https://gitlab.unige.ch///commit/f75b04e8ff7ca01a04b168dc70d9283f73a4f810))

### [1.4.2](https://gitlab.unige.ch///compare/v1.4.1...v1.4.2) (2021-06-09)


### Features

* allow to drop draggable element into datatable ([48f7c28](https://gitlab.unige.ch///commit/48f7c28034015026da51c34d47acd7e3978b424b))

### [1.4.1](https://gitlab.unige.ch///compare/v1.4.0...v1.4.1) (2021-06-08)


### Features

* **keyword:** [AOU-569] keyword past split on separator detection ([3779939](https://gitlab.unige.ch///commit/3779939472d4a0d6126b7477bdfe5129c542bf15))

## [1.4.0](https://gitlab.unige.ch///compare/v1.3.11...v1.4.0) (2021-06-02)


### Features

* add delete list into resource state ([3ca6f44](https://gitlab.unige.ch///commit/3ca6f442f4eb1e3d143863a096873b1839cebb28))

### [1.3.11](https://gitlab.unige.ch///compare/v1.3.10...v1.3.11) (2021-05-31)


### Bug Fixes

* make validation util method static ([1b623e0](https://gitlab.unige.ch///commit/1b623e071e748db467a26623f67e4b96ad2e730f))

### [1.3.10](https://gitlab.unige.ch///compare/v1.3.9...v1.3.10) (2021-05-31)


### Features

* integrate mat dialog override ([4004a47](https://gitlab.unige.ch///commit/4004a47c251fdf66102bb233013425921aa6855b))


### Bug Fixes

* avoid looping when force to redirect to home page and backend is down ([2eb2683](https://gitlab.unige.ch///commit/2eb26831a20ba4ece1b28acd4c59f7c04ef5ffb7))

### [1.3.9](https://gitlab.unige.ch///compare/v1.3.8...v1.3.9) (2021-05-31)


### Features

* add constant form status valid and invalid ([edaf1ea](https://gitlab.unige.ch///commit/edaf1ea15a5ea5570de83161e6a19f0b513975cf))
* add tool is form group and form array ([b17773b](https://gitlab.unige.ch///commit/b17773b320f483dde68de0d3275fcd2b462a1a18))
* add validation util for retrieve list of error in an abstract form ([efd2247](https://gitlab.unige.ch///commit/efd224742d4089379c005844edca3ba019806cc4))


### Bug Fixes

* import of tool in new pipe ([6364983](https://gitlab.unige.ch///commit/63649831c57eb159b0d89ba36006aac30baf1c30))

### [1.3.8](https://gitlab.unige.ch///compare/v1.3.7...v1.3.8) (2021-05-20)

### [1.3.7](https://gitlab.unige.ch///compare/v1.3.6...v1.3.7) (2021-05-20)

### [1.3.6](https://gitlab.unige.ch///compare/v1.3.5...v1.3.6) (2021-05-18)


### Features

* **multi searchable select:** add possibility to display if a value is default ([48f8988](https://gitlab.unige.ch///commit/48f8988e131dd6bcc0aed481349d2b8034c7a77c))

### [1.3.5](https://gitlab.unige.ch///compare/v1.3.4...v1.3.5) (2021-05-17)


### Bug Fixes

* preloadRowsForPage datatable optimization and rename datas to data ([b07102f](https://gitlab.unige.ch///commit/b07102f05c8b211cd56c6edb152356ec90061746))

### [1.3.4](https://gitlab.unige.ch///compare/v1.3.3...v1.3.4) (2021-05-17)

### [1.3.3](https://gitlab.unige.ch///compare/v1.3.2...v1.3.3) (2021-05-17)


### Features

* add option to force login when app load ([a9ca8ff](https://gitlab.unige.ch///commit/a9ca8ff9eeb945a6c2270bea9815cb848df27c0d))
* add result action ([461605c](https://gitlab.unige.ch///commit/461605c0950f865be978fa2d041b8bfcd485270b))

### [1.3.2](https://gitlab.unige.ch///compare/v1.3.1...v1.3.2) (2021-05-12)


### Features

* **datatable:** add mousehover callback on action ([0e2e2f9](https://gitlab.unige.ch///commit/0e2e2f905bd92cadffb80ca91a2850e5e7c33fca))

### [1.3.1](https://gitlab.unige.ch///compare/v1.3.0...v1.3.1) (2021-05-10)


### Features

* add tour step presentational ([4f27f27](https://gitlab.unige.ch///commit/4f27f272f1cb08afe2ab3d847d7a52dbd0b4e072))

## [1.3.0](https://gitlab.unige.ch///compare/v1.2.22...v1.3.0) (2021-05-10)


### Features

* add possibiltiy to inject custom info on token dialog ([60c124a](https://gitlab.unige.ch///commit/60c124a36356ee11124ec444c8c6fb7d1db46f01))
* set color for created history status ([861eafd](https://gitlab.unige.ch///commit/861eafd5bcee1816f3eb683ee9786e564551dda7))


### Bug Fixes

* datatable filter line not display when should ([9a2a098](https://gitlab.unige.ch///commit/9a2a098ef41f7e50d6856f09fc59a31c783a4439))
* transform app tour service into abstract tour service ([ad15bf6](https://gitlab.unige.ch///commit/ad15bf6d57f495341b4cefb8da612f31fdecedf6))

### [1.2.22](https://gitlab.unige.ch///compare/v1.2.21...v1.2.22) (2021-05-03)


### Features

* add extends enum when required ([c62b558](https://gitlab.unige.ch///commit/c62b5586dd39a6338db338c95556e984edbbe9fc))

### [1.2.21](https://gitlab.unige.ch///compare/v1.2.20...v1.2.21) (2021-04-26)


### Bug Fixes

* allow to retrieve app logo on svg or png ([73e6ca1](https://gitlab.unige.ch///commit/73e6ca1d797df1e6d51053af9cc328387f5142d6))

### [1.2.20](https://gitlab.unige.ch///compare/v1.2.19...v1.2.20) (2021-04-26)


### Bug Fixes

* allow to provide data test into data table column without typing error ([ea1ea6b](https://gitlab.unige.ch///commit/ea1ea6b502697c4c048cefa7788c3e2d84d9f774))
* make protected some method and attribute into abstract app component ([cb61467](https://gitlab.unige.ch///commit/cb614677bc2ac463ecc6d54633dc5bfe67e328b4))

### [1.2.19](https://gitlab.unige.ch///compare/v1.2.18...v1.2.19) (2021-04-21)


### Bug Fixes

* allow to customize appearance and label position for keyword input ([61591d9](https://gitlab.unige.ch///commit/61591d9963a65846b3b442d0b618b6eb92e9f0db))

### [1.2.18](https://gitlab.unige.ch///compare/v1.2.17...v1.2.18) (2021-04-16)


### Bug Fixes

* add date util method to parse swiss date format string to date ([62d8492](https://gitlab.unige.ch///commit/62d8492d76085ed3d6a5d57cb454400575aa0187))

### [1.2.17](https://gitlab.unige.ch///compare/v1.2.16...v1.2.17) (2021-04-08)


### Bug Fixes

* make public method for bind validation error to form ([018a170](https://gitlab.unige.ch///commit/018a170d0f8c35c1df1886e669bcc78bcaac42db))

### [1.2.16](https://gitlab.unige.ch///compare/v1.2.15...v1.2.16) (2021-04-07)


### Features

* allow to display list formated in data table ([734a48f](https://gitlab.unige.ch///commit/734a48fa97dbb2c1c68c43f8b0f8c66ac0e83882))

### [1.2.15](https://gitlab.unige.ch///compare/v1.2.14...v1.2.15) (2021-04-06)


### Bug Fixes

* datatable allow to provide custom value key on searchable single select filter ([72bd276](https://gitlab.unige.ch///commit/72bd276c4f87071c86072d1b7d140ef86ef894df))

### [1.2.14](https://gitlab.unige.ch///compare/v1.2.13...v1.2.14) (2021-04-01)


### Bug Fixes

* allow to break word on specific column ([119b972](https://gitlab.unige.ch///commit/119b97270d474e8943570ffd5ad0470aab589049))

### [1.2.13](https://gitlab.unige.ch///compare/v1.2.12...v1.2.13) (2021-03-31)


### Bug Fixes

* add pipe and tool to clean html tag in string ([a041cc7](https://gitlab.unige.ch///commit/a041cc7c34195ff0e64a64046532db8494c81da1))
* performance issue due to datatable preload rows for page ([5ce99bb](https://gitlab.unige.ch///commit/5ce99bb9fb8368ddfd9cbbfd61517fe25069c774))

### [1.2.12](https://gitlab.unige.ch///compare/v1.2.11...v1.2.12) (2021-03-26)


### Bug Fixes

* tootlip on ellipsis to work on disable input ([d28f5e8](https://gitlab.unige.ch///commit/d28f5e8e6d3028ec17c8f1a71e975df9b1e49eb2))

### [1.2.11](https://gitlab.unige.ch///compare/v1.2.10...v1.2.11) (2021-03-25)


### Bug Fixes

* break work in data table cell to manage long string without space ([caba2c0](https://gitlab.unige.ch///commit/caba2c09c0bd0f26d7499cb35c671d4d2408a96b))

### [1.2.10](https://gitlab.unige.ch///compare/v1.2.9...v1.2.10) (2021-03-25)


### Features

* allow to have inner html into datatable ([f25c586](https://gitlab.unige.ch///commit/f25c58637f0254c3a1afc6e9eb5f7bfea6743f21))

### [1.2.9](https://gitlab.unige.ch///compare/v1.2.8...v1.2.9) (2021-03-22)


### Bug Fixes

* close mat menu after click on action in datatable three dot button ([bce9cb1](https://gitlab.unige.ch///commit/bce9cb13c2918090b7abb74680013e5aa7cd143c))
* http translate loader for theme file and customizable at runtime ([9b6e88a](https://gitlab.unige.ch///commit/9b6e88ac28456cc7188947e7a3c6bb0058c85b3f))
* pagination for load chunk ([93f68c1](https://gitlab.unige.ch///commit/93f68c1734530c8f26e1317e035debe79c20f4cb))

### [1.2.8](https://gitlab.unige.ch///compare/v1.2.7...v1.2.8) (2021-03-16)


### Features

* add convert date to swiss date format in date util ([0e96c6a](https://gitlab.unige.ch///commit/0e96c6a6c691aa5799f3614a3b6259aeb718aa93))

### [1.2.7](https://gitlab.unige.ch///compare/v1.2.6...v1.2.7) (2021-03-16)


### Bug Fixes

* datatable column list not throw error when is not an array ([77b8b60](https://gitlab.unige.ch///commit/77b8b60ee34cf3513bb2df14640d6fca7a9c60c0))

### [1.2.6](https://gitlab.unige.ch///compare/v1.2.5...v1.2.6) (2021-03-16)


### Bug Fixes

* force to explicit status enum to use in history dialog ([617d202](https://gitlab.unige.ch///commit/617d20294146b3a83159e215a6700b5959c3904a))

### [1.2.5](https://gitlab.unige.ch///compare/v1.2.4...v1.2.5) (2021-03-05)

### [1.2.4](https://gitlab.unige.ch///compare/v1.2.3...v1.2.4) (2021-03-03)


### Features

* allow to have confirm dialog without button ([6863bff](https://gitlab.unige.ch///commit/6863bff4e145b778ef7dd27290283ca7ff6a050d))

### [1.2.3](https://gitlab.unige.ch///compare/v1.2.2...v1.2.3) (2021-03-02)


### Bug Fixes

* default label position for single searchable select ([8adac83](https://gitlab.unige.ch///commit/8adac83be42497517d40e2e940cbf18ca7bc861e))

### [1.2.2](https://gitlab.unige.ch///compare/v1.2.1...v1.2.2) (2021-03-02)


### Bug Fixes

* searchable single select initial value for label position ([3cfc845](https://gitlab.unige.ch///commit/3cfc8454a4ae2fba0ad822752908a23ac31d8e1c))

### [1.2.1](https://gitlab.unige.ch///compare/v1.2.0...v1.2.1) (2021-03-02)


### Features

* allow to customize float label poisiton on solidify input ([d960e58](https://gitlab.unige.ch///commit/d960e58547352c1800fb47e28b628decdb3970ab))

## [1.2.0](https://gitlab.unige.ch///compare/v1.1.28...v1.2.0) (2021-03-02)


### Features

* add down arrow icon on solidify select ([f07171b](https://gitlab.unige.ch///commit/f07171b40ca957b3250311b3627907ecce814392))
* add position label for material input ([f70b89b](https://gitlab.unige.ch///commit/f70b89b0127a4aa45305edbdf58308a698c723e7))


### Bug Fixes

* missing icon ([98c631b](https://gitlab.unige.ch///commit/98c631b6d7921268d65e93a2800e93050a1f8ca5))

### [1.1.28](https://gitlab.unige.ch///compare/v1.1.27...v1.1.28) (2021-02-26)


### Features

* allow to optionally launch default request on relation 3 tier ([b9afd6e](https://gitlab.unige.ch///commit/b9afd6e87d57218129ff28f53dc85883f667402c))

### [1.1.27](https://gitlab.unige.ch///compare/v1.1.26...v1.1.27) (2021-02-25)


### Features

* backport from dlcm refacto of token dialog ([bda3594](https://gitlab.unige.ch///commit/bda3594824043ba1673845b96ee7fd536968b045))

### [1.1.26](https://gitlab.unige.ch///compare/v1.1.25...v1.1.26) (2021-02-23)

### [1.1.25](https://gitlab.unige.ch///compare/v1.1.24...v1.1.25) (2021-02-23)


### Features

* add method to increment or decrement loading counter ([ad3f16c](https://gitlab.unige.ch///commit/ad3f16c11da074feba77b48d66249519fb50c44a))

### [1.1.24](https://gitlab.unige.ch///compare/v1.1.23...v1.1.24) (2021-02-23)


### Features

* allow to provide parameters on banner message ([9bc6a4f](https://gitlab.unige.ch///commit/9bc6a4f23a5346c7f839955608841360ede2272a))
* merge html for single and multi select content searchable ([3f8cb4a](https://gitlab.unige.ch///commit/3f8cb4a9e6cc610f1341e20f24ad518058f00751))


### Bug Fixes

* build error ([73ee09a](https://gitlab.unige.ch///commit/73ee09a26232a9970c0ccbb2fc7723af7ce0574d))
* error when reset a query parameters ([9f9aa52](https://gitlab.unige.ch///commit/9f9aa5291d39520f80cb597bfc8d66b05592f3db))

### [1.1.23](https://gitlab.unige.ch///compare/v1.1.22...v1.1.23) (2021-02-11)


### Features

* improve confirm dialog ([ff5fd1b](https://gitlab.unige.ch///commit/ff5fd1bd97857edf94bfee865545c7abece8c82c))

### [1.1.22](https://gitlab.unige.ch///compare/v1.1.21...v1.1.22) (2021-02-11)


### Features

* allow to display list on datatable ([6672a20](https://gitlab.unige.ch///commit/6672a20cf2bb162cf7d0d3b6557f8130031bddfa))

### [1.1.21](https://gitlab.unige.ch///compare/v1.1.20...v1.1.21) (2021-02-10)


### Bug Fixes

* allow to use data table injected component inside solidify lib ([2b5d62e](https://gitlab.unige.ch///commit/2b5d62e750aebab36693d6ce3df048d8aec2187a))

### [1.1.20](https://gitlab.unige.ch///compare/v1.1.19...v1.1.20) (2021-02-09)


### Features

* add possibility to display extra label second line on searchable select ([737aadf](https://gitlab.unige.ch///commit/737aadf486a0ee78cf95c2ad81b25a6cb5fe6b9d))
* allow datetime pipe to manage different format ([c180748](https://gitlab.unige.ch///commit/c180748d6db9635485e29023899a077e7fa50c6a))


### Bug Fixes

* display message as error when download fail ([f10a00d](https://gitlab.unige.ch///commit/f10a00da8078061627bd6b5620965fa10b5da478))
* lint error ([f1a2e51](https://gitlab.unige.ch///commit/f1a2e51e1b9f717d1d2c098c0d59aadd95c399ad))

### [1.1.19](https://gitlab.unige.ch///compare/v1.1.18...v1.1.19) (2021-02-04)


### Features

* allow to retrieve an other column as input of inject component in datatable ([4cf9e27](https://gitlab.unige.ch///commit/4cf9e27813a4465f55b3e3f2e7e6baf7070fe188))

### [1.1.18](https://gitlab.unige.ch///compare/v1.1.17...v1.1.18) (2021-02-04)


### Bug Fixes

* allow to type new line on textarea ([638df30](https://gitlab.unige.ch///commit/638df300d501319de2414855442f976c0eb31a32))

### [1.1.17](https://gitlab.unige.ch///compare/v1.1.16...v1.1.17) (2021-02-03)


### Bug Fixes

* add fix for textarea on ios ([dcbd0ba](https://gitlab.unige.ch///commit/dcbd0ba967d12136b114de6bc0a625ce6de1d77d))
* avoid to block textarea behavior with shortcut directive ([399b094](https://gitlab.unige.ch///commit/399b094d2e1a5f3676d2c23d5716b3683678d65e))

### [1.1.16](https://gitlab.unige.ch///compare/v1.1.15...v1.1.16) (2021-02-03)


### Features

* allow to display spinner directly inside data table ([382a763](https://gitlab.unige.ch///commit/382a7635fdfbcda2d8b16c7d0ac5627a550e1223))


### Bug Fixes

* prevent error on datatable when action have no display condition ([f8f492c](https://gitlab.unige.ch///commit/f8f492c4cc9de2c36d45d8bf71cdbee2f7c6f180))
* searchable multiple select field should not display pen when ignored or cursor ([9a806b9](https://gitlab.unige.ch///commit/9a806b902f7ed7d43788f1e21afa2bb4345ea4eb))
* spinner directive prefix ([352746f](https://gitlab.unige.ch///commit/352746f5295b46a3c95febe5aad00d04757c3a59))

### [1.1.15](https://gitlab.unige.ch///compare/v1.1.14...v1.1.15) (2021-01-29)


### Features

* Allow to disable some feature on solidify app state ([0449872](https://gitlab.unige.ch///commit/0449872bbfa17e386d4d2015452d998cb79198b8))
* space delimiter for keyword component is optional ([2e961c6](https://gitlab.unige.ch///commit/2e961c6809b4bf3396640dd805f413413522fe4e))

### [1.1.14](https://gitlab.unige.ch///compare/v1.1.13...v1.1.14) (2021-01-27)


### Bug Fixes

* change rsync command since it is not compatible with windows ([d4a6945](https://gitlab.unige.ch///commit/d4a6945cf0d3d553f69821d7928bf650b427535c))
* disable schematics related scripts ([0c989de](https://gitlab.unige.ch///commit/0c989de2f2606b530407cc72ae5332cee749735c))
* replace cp --parent to rsync to be independent of OS ([00c6a37](https://gitlab.unige.ch///commit/00c6a37d663b162d3c1e7cceb59773df055ee3f0))

### [1.1.13](https://gitlab.unige.ch///compare/v1.1.12...v1.1.13) (2021-01-26)


### Features

* allow to display common value on searchable select popup ([d0c0664](https://gitlab.unige.ch///commit/d0c06644838e510bb74ebd54a5c970e60b0a832f))


### Bug Fixes

* sticky toolbar z-index ([b538d72](https://gitlab.unige.ch///commit/b538d72b1973123a4dbe5f98b7d72563bf0ce129))

### [1.1.12](https://gitlab.unige.ch///compare/v1.1.11...v1.1.12) (2021-01-21)


### Features

* add drag and drop output event on datatable ([1cac60a](https://gitlab.unige.ch///commit/1cac60ab871a31b4885056c22c3c4c02fbc115e4))
* add scroll service ([8c28d9e](https://gitlab.unige.ch///commit/8c28d9e3723092aa0452bcefe16b3c13a49f1a41))


### Bug Fixes

* data table display problems ([8c6d29c](https://gitlab.unige.ch///commit/8c6d29c6fa245bbf93080f02f9409d222832a025))
* dialogRef variable not needed ([7c737bb](https://gitlab.unige.ch///commit/7c737bbbfa1485e7f081c710a492833d9e03f0b9))

### [1.1.11](https://gitlab.unige.ch///compare/v1.1.10...v1.1.11) (2021-01-05)


### Features

* allow to translate label on multi select component ([f28f7c3](https://gitlab.unige.ch///commit/f28f7c3e0b7f1b64fafda63a4ca30357d93ec968))

### [1.1.10](https://gitlab.unige.ch///compare/v1.1.9...v1.1.10) (2021-01-05)


### Features

* add directive that dinamically add tooltip on ellipsis text ([228a55f](https://gitlab.unige.ch///commit/228a55f997848403fbb88ea69e93f0a6867cb2a3))
* add ellipsis on overflow material input value ([b10aec4](https://gitlab.unige.ch///commit/b10aec414b47b23f346364fc1acb688cad1751b4))


### Bug Fixes

* disable redundant material tooltip on single searchable select ([a3a7f7a](https://gitlab.unige.ch///commit/a3a7f7abc414dbea69a838a67562ea040b596b12))
* multi select content use labelcallback to display value ([abefb22](https://gitlab.unige.ch///commit/abefb229825a2156ee4ce5ac2db604e3210bffed))
* rename directive tooltip on ellipsis ([1b22b56](https://gitlab.unige.ch///commit/1b22b564d9e7e48a6b8aa14296c60c1098310460))
* update directive tooltip on ellipsis ([aafbf7d](https://gitlab.unige.ch///commit/aafbf7d39a0f76fdc61dbadad7bd58e0f8c2490f))

### [1.1.9](https://gitlab.unige.ch///compare/v1.1.8...v1.1.9) (2020-12-21)


### Features

* display token decoded information on token dialog ([ac61163](https://gitlab.unige.ch///commit/ac61163833ea87f95ab16a777970fce2bd21f43e))


### Bug Fixes

* allow to copy field value when disabled on firefox ([e267970](https://gitlab.unige.ch///commit/e267970707b6286c3c0bdaee668e6f9211314aa9))
* allow to copy field value without enter in edit mode on chrome ([f11e8da](https://gitlab.unige.ch///commit/f11e8da0484978fd70b0c985accfa62726b5636d))

### [1.1.8](https://gitlab.unige.ch///compare/v1.1.7...v1.1.8) (2020-12-21)


### Features

* add ribbon component ([3060b95](https://gitlab.unige.ch///commit/3060b95bb796a7d1ca5fd817024c0672e60e6cc1))

### [1.1.7](https://gitlab.unige.ch///compare/v1.1.6...v1.1.7) (2020-12-15)


### Features

* add partial enum action name ([e3bcdc0](https://gitlab.unige.ch///commit/e3bcdc0c38fcfdca1280cd3c37bc157fced429ea))
* update total state variable when get all ([823d017](https://gitlab.unige.ch///commit/823d017ca174a15dabb1b11327d4e9b4e80459a4))

### [1.1.6](https://gitlab.unige.ch///compare/v1.1.5...v1.1.6) (2020-12-08)


### Features

* add guard to check that resource exist ([c109a0f](https://gitlab.unige.ch///commit/c109a0f5c50becc953982e25d1eb2b7bb46242b3))
* allow to customize param id for guard resource exist ([b5d3535](https://gitlab.unige.ch///commit/b5d3535eb0b377ba32387e7c816800b1e814a18f))

### [1.1.5](https://gitlab.unige.ch///compare/v1.1.4...v1.1.5) (2020-12-07)


### Features

* allow to customise tag used for enter in edit mode ([cfc8b8a](https://gitlab.unige.ch///commit/cfc8b8a81ba9f4351d010cf8d63bd295ddc88026))


### Bug Fixes

* avoid to display systematically error in toast if exeptions attribute is not provided ([0b3b24c](https://gitlab.unige.ch///commit/0b3b24c7acda20933d4c1d04ce158e80b3e3a646))

### [1.1.4](https://gitlab.unige.ch///compare/v1.1.3...v1.1.4) (2020-12-04)


### Bug Fixes

* allow to use extend enum on api state for resource name attribute ([6370afb](https://gitlab.unige.ch///commit/6370afbcd6145246b458d7c21807dd1637aa3c61))

### [1.1.3](https://gitlab.unige.ch///compare/v1.1.2...v1.1.3) (2020-12-03)


### Bug Fixes

* additional information panel to init observable attribute when all input are provided ([f68e2d4](https://gitlab.unige.ch///commit/f68e2d4a259d3d6e4d0b8eab61f95e11f49b62c2))
* label on input url ([1ce5ce2](https://gitlab.unige.ch///commit/1ce5ce27a2500aeeffcfa6de3f657f42063c5526))
* lint error ([9dbdae1](https://gitlab.unige.ch///commit/9dbdae176b4c8d4b69d053b14f2fc54bd3e08d2b))

### [1.1.2](https://gitlab.unige.ch///compare/v1.1.1...v1.1.2) (2020-12-02)


### Bug Fixes

* uncomment tabs container html ([9a50295](https://gitlab.unige.ch///commit/9a50295f3235bfacaaedf8d07e9ba9dbab41eef5))

### [1.1.1](https://gitlab.unige.ch///compare/v1.1.0...v1.1.1) (2020-12-02)


### Bug Fixes

* runtime environment override for app state url ([0d21c7a](https://gitlab.unige.ch///commit/0d21c7a743d0d229cfa222adbc5a7981788fa14e))

## [1.1.0](https://gitlab.unige.ch///compare/v1.0.5...v1.1.0) (2020-12-02)


### Features

* add a standard error handler ([348e32e](https://gitlab.unige.ch///commit/348e32e8da80179ec85f6247dfb7c3e59627f08e))
* add abstract button toolbar from dlcm ([02ffdb8](https://gitlab.unige.ch///commit/02ffdb8c82d61cc5f68dd64d8a6339b69319bcea))
* add abstract can deactivate routable ([1d0f02e](https://gitlab.unige.ch///commit/1d0f02e1032b280bcc65bd751de67165dd202514))
* add abstract detail edit normal and common routable and banner state ([0991e95](https://gitlab.unige.ch///commit/0991e957ed26e9cfa197cd8f8959f4873ec2d394))
* add abstract form from dlcm ([6675b0d](https://gitlab.unige.ch///commit/6675b0df89023397ce03f38094416b8557f93915))
* add abstract home routable from dlcm ([aaa9758](https://gitlab.unige.ch///commit/aaa97585aa4c421fc8c683b77649dda58b44551d))
* add abstract list routable to dlcm ([d3e8df0](https://gitlab.unige.ch///commit/d3e8df0611e47d7413087a47df3edbde5746980d))
* add abstract resource and crud routable from dlcm ([be5c85e](https://gitlab.unige.ch///commit/be5c85e94f29c25b1c8a4567516e4b9b1565d3c1))
* add additional information panel from dlcm ([890ea65](https://gitlab.unige.ch///commit/890ea65f4be323f9b100697aee32ace3434e7866))
* add application dev guard from dlcm ([fcdf59c](https://gitlab.unige.ch///commit/fcdf59c5d1c861c66f8a670f7f1141c2f7e27798))
* add banner presentational from dlcm ([939ab80](https://gitlab.unige.ch///commit/939ab80c66ebb69da469c09f06f5895030ca326c))
* add breadcrumb from dlcm ([e9c0db6](https://gitlab.unige.ch///commit/e9c0db6287567951c171d5ad024b54cb74a6303e))
* add button status history on solidify ([9efddff](https://gitlab.unige.ch///commit/9efddff167188d2cf757dfc93195d7acead8d6dc))
* add button toolbar detail from dlcm ([cd6b29c](https://gitlab.unige.ch///commit/cd6b29cb029d717f8cb7fc71909dff052b2de759))
* add button toolbar list from dlcm ([2e23a40](https://gitlab.unige.ch///commit/2e23a40f8ddb6ab305b884ff41aedeabf0cf8dec))
* add can deactivate guard from dlcm ([2e8a07b](https://gitlab.unige.ch///commit/2e8a07b14c752812db2f7382b8014da380d205fa))
* add curd helper from dlcm ([1346e7f](https://gitlab.unige.ch///commit/1346e7fbd227e97bed4d718f125509e53d380d2e))
* add custom mat paginator service for translate material component ([c5770c0](https://gitlab.unige.ch///commit/c5770c0f186b26d3e1be479c785377e517f9ab37))
* add detail presentation component from dlcm ([b723ff8](https://gitlab.unige.ch///commit/b723ff898fe59f72269a573accb93803192be8f4))
* add download service from dlcm ([1bc92aa](https://gitlab.unige.ch///commit/1bc92aa004764ef4dfd809a717dbff65bd6c8144))
* add error skipper service from dlcm ([ee270f0](https://gitlab.unige.ch///commit/ee270f0ef5349f807a733ea20d47f7472629aae3))
* add guard prevent leave from dlcm ([d9db642](https://gitlab.unige.ch///commit/d9db6424c20cbe5e8ad38fc1f874b9529ed38631))
* add http translate loader extra from dlcm ([6ba1b9a](https://gitlab.unige.ch///commit/6ba1b9aeed460f9ba6604c040c1681b2383b3136))
* add icon app summary routable from dlcm ([3b76e58](https://gitlab.unige.ch///commit/3b76e581a19e0b511474ee6a86cce371489469fd))
* add keyword input from dlcm ([28efb17](https://gitlab.unige.ch///commit/28efb17007058d6e31aa9d822198816ab572af2d))
* add language selector from dlcm ([7527a4b](https://gitlab.unige.ch///commit/7527a4b30186f689f6cd0ab4a194d57091e71f25))
* add panel expandable form dlcm ([331da5e](https://gitlab.unige.ch///commit/331da5eb326f6849c4246d7d5313e5e1ca03048e))
* add regex util from dlcm ([6645216](https://gitlab.unige.ch///commit/6645216279a903697e93321b1ce62f68875d45bf))
* add route util methods from dlcm ([a5b8483](https://gitlab.unige.ch///commit/a5b84831128a2a5b94221284fc539829ef387a50))
* add router extension service from dlcm ([36c5a08](https://gitlab.unige.ch///commit/36c5a08eb5df36116724346156f48d6d9fb65c89))
* add security service and app user state from dlcm ([ceb56f2](https://gitlab.unige.ch///commit/ceb56f283211a6556a5c2e180d0639cb1ecee2b4))
* add snackbar component from dlcm ([bcc03e2](https://gitlab.unige.ch///commit/bcc03e2d19d4eb05ad094b678c0a36a872f0c03a))
* add star rating from dlcm ([36b9e40](https://gitlab.unige.ch///commit/36b9e40dcc0104e3fb6ed0b35eed9fcb11d03a5c))
* add status component from dlcm ([9b306c4](https://gitlab.unige.ch///commit/9b306c4a044ae40f4bfec078371d832de44ec19e))
* add status history state and dialog ([b0756f7](https://gitlab.unige.ch///commit/b0756f7179d440180a403c156e01274af4bb0280))
* add test helpers from dlcm ([b19ce8a](https://gitlab.unige.ch///commit/b19ce8a5c7778f48b6e5abc7954078fb31c18f54))
* add theme selector from dlcm ([a15a603](https://gitlab.unige.ch///commit/a15a60302e30c315041ab015fb95365c341604e5))
* add tour guide from dlcm ([70b4e4a](https://gitlab.unige.ch///commit/70b4e4a4a20d796f340ce0ee3a47058aa5f99136))
* integrate app state from dlcm ([724a5c0](https://gitlab.unige.ch///commit/724a5c0497093bab844e1ed05c3ede76da25c0cd))
* integrate update version, token dialog, server offline page and maintenance page from dlcm ([51fb9ad](https://gitlab.unige.ch///commit/51fb9ad61cfe8317c109221312f8b1a3941df1ef))
* move confirm dialog from dlcm ([ef8f5fa](https://gitlab.unige.ch///commit/ef8f5fa5ff16d0068257426484958026e81bc6cb))
* move delete dialog from dlcm ([11a7340](https://gitlab.unige.ch///commit/11a7340d2a7d0e09a4ef2e873b4bc3a676719901))
* move scss framework from dlcm ([d61b4e4](https://gitlab.unige.ch///commit/d61b4e4bd7d61cda07fff1d09d1ad0ad58da95d8))
* move search input from dlcm ([23e55e8](https://gitlab.unige.ch///commit/23e55e80ca25abb1616623e9ff3939e6afc6c0c1))
* move url input from dlcm ([ab92fc0](https://gitlab.unige.ch///commit/ab92fc09bfcb72f17b93cd9790a5897e04010002))


### Bug Fixes

* adapt to work in a project ([39548f6](https://gitlab.unige.ch///commit/39548f6c90d310e20e3557c0ad8ea014a6e2d3be))
* add abstract app component from dlcm ([83a2d27](https://gitlab.unige.ch///commit/83a2d278a1b812cb59b83de5c922b49c0e806453))
* add abstract create routable ([6dfda9e](https://gitlab.unige.ch///commit/6dfda9ef099178920b58b2210d9ea77106bdcaae))
* build error by adding dynamics and change the global app namespace ([8eba2c8](https://gitlab.unige.ch///commit/8eba2c8561e1c80775cc228f18c278a742ac8910))
* circular dependency on itself error ([5285273](https://gitlab.unige.ch///commit/5285273b92c1e84ace585e54f73593534238d449))
* data table glitch on header when no data are dispayed ([92c89a9](https://gitlab.unige.ch///commit/92c89a95eca62548c43cc66fd15f1d4a3d1d50e8))
* error build due to ngx-tour-core ([449db1a](https://gitlab.unige.ch///commit/449db1a0d531b7cff5cb09d43f4f8b46fb4311c8))
* ignore previous set language if no more in env setting ([06f69f2](https://gitlab.unige.ch///commit/06f69f230743c406e8342cea0f584556704c6070))
* import ([4ab19fc](https://gitlab.unige.ch///commit/4ab19fc462da20017b41db1b8fe54864fcb4f859))
* import ([694c7a3](https://gitlab.unige.ch///commit/694c7a301871f14a3c8a941aebb667435cef9b18))
* import tour on solidify module ([a84ba8c](https://gitlab.unige.ch///commit/a84ba8c1f0bfc2c6a351d6a6c7f4630b065ad1f5))
* lint ([2900ef3](https://gitlab.unige.ch///commit/2900ef30b3296c6bee9b69857b26561d79490a63))
* lint error ([940f6d4](https://gitlab.unige.ch///commit/940f6d4251f70ba97a61c543d89d7ac59234463f))
* remove for root on module tour mat menu module ([8b344f0](https://gitlab.unige.ch///commit/8b344f01c2b6c3efbba07ee7cdfb2b0201adc249))
* remove provide in root injection for error handler ([391cbc5](https://gitlab.unige.ch///commit/391cbc5b7e01d08ce7c59d5ef28cd5b56b60531c))
* scss import and add abstract scss file to avoid multiple import ([ce06311](https://gitlab.unige.ch///commit/ce0631106b8e9df7ac1c1f50a500940517ad22af))

### [1.0.5](https://gitlab.unige.ch///compare/v1.0.4...v1.0.5) (2020-11-18)

### [1.0.4](https://gitlab.unige.ch///compare/v1.0.3...v1.0.4) (2020-11-17)


### Features

* add tabs container from dlcm ([5056ee1](https://gitlab.unige.ch///commit/5056ee123d2a46517c897ad5c1eeccfd189c6886))

### [1.0.3](https://gitlab.unige.ch///compare/v1.0.2...v1.0.3) (2020-11-12)


### Bug Fixes

* remove http module import ([0976896](https://gitlab.unige.ch///commit/09768966315e70e041d71a2e25f7674462cd614f))

### [1.0.2](https://gitlab.unige.ch///compare/v1.0.1...v1.0.2) (2020-11-11)

### [1.0.1](https://gitlab.unige.ch///compare/v1.0.0...v1.0.1) (2020-11-10)


### Bug Fixes

* wrong import ([b3994ff](https://gitlab.unige.ch///commit/b3994ff173e0b63cd3655bf534a2fd4ccebca428))

## [1.0.0](https://gitlab.unige.ch///compare/v0.6.8...v1.0.0) (2020-11-10)


### Features

* add bulk of new component, directive, utils, helpers and service from dlcm ([646f369](https://gitlab.unige.ch///commit/646f369057e868bee5c154ca8a6ef3ec894b124e))
* change the way to provide string to translate ([ccc612c](https://gitlab.unige.ch///commit/ccc612cd1b14630ea37e887117f973fe72fdebd6))
* create a constante object to group all constant ([b31396e](https://gitlab.unige.ch///commit/b31396e192775b56bdb25b8edaaaebb535ac6255))
* migrate data table from dlcm ([42fadfb](https://gitlab.unige.ch///commit/42fadfb3bc7d1be31597f4cc4c55d370ef2bc8d4))


### Bug Fixes

* add dynamic annotation to solve build errors ([e0fb676](https://gitlab.unige.ch///commit/e0fb676dfe8a6c78730061863f38fe2b9a27d74f))
* add last label to translate into the dedicated interface ([6f4417d](https://gitlab.unige.ch///commit/6f4417df4f52a6676a76b078e9d459183e356527))
* build error on searchable content ([7e237cb](https://gitlab.unige.ch///commit/7e237cbb4dc052d9da6dbdf53c633e4ccd4fb514))
* jenkins build error ([51741d6](https://gitlab.unige.ch///commit/51741d66694444cd9282278577bc543f2b2de301))

### [0.6.8](https://gitlab.unige.ch///compare/v0.6.7...v0.6.8) (2020-11-06)


### Features

* adaptation to work with AoU project ([b2e16c7](https://gitlab.unige.ch///commit/b2e16c7e49151cfe122a3834ff37fe38c1f6bb06))
* allow to display general validation message error on unbind field and specific fields provided ([c83d950](https://gitlab.unige.ch///commit/c83d950437698dd476ea755b9aa81c0015a89099))

### [0.6.7](https://gitlab.unige.ch///compare/v0.6.6...v0.6.7) (2020-09-02)


### Features

* export type for memoized util ([94fd89b](https://gitlab.unige.ch///commit/94fd89b654796c1ce4c12eaca3271f5f4e438d04))


### Bug Fixes

* refactor composition state delete method to pass only id and not full object ([75feb13](https://gitlab.unige.ch///commit/75feb130c03dadb737d539357c286e8106e1159b))

### [0.6.6](https://gitlab.unige.ch///compare/v0.6.5...v0.6.6) (2020-08-28)


### Bug Fixes

* add log to inform when unable to bind error ([61470d4](https://gitlab.unige.ch///commit/61470d44dd98be384132d9f2fb78d93a71956f92))
* prevent error when file to bind error is not found ([1295e48](https://gitlab.unige.ch///commit/1295e4877fe2b50ba0b9df9b1b81fd655171015a))

### [0.6.5](https://gitlab.unige.ch///compare/v0.6.4...v0.6.5) (2020-07-27)


### Features

* correct schematics angular utility project imports ([e05afbd](https://gitlab.unige.ch///commit/e05afbd90b8ee619868ea546e3ae0bce8f8c377c))
* update all libraries to latest version ([f19757d](https://gitlab.unige.ch///commit/f19757d54272879075271d3bb1fe0fdb621cb6ca))


### Bug Fixes

* add missing annotation ([95c4048](https://gitlab.unige.ch///commit/95c4048a216402415784c4a1c09a5f960e8ae747))

### [0.6.4](https://gitlab.unige.ch///compare/v0.6.3...v0.6.4) (2020-07-23)


### Features

* allow to create custom parameters exernally ([2c34b49](https://gitlab.unige.ch///commit/2c34b49b24e899a712d4d0a20c76a21bef6b0910))

### [0.6.3](https://gitlab.unige.ch///compare/v0.6.2...v0.6.3) (2020-07-17)


### Features

* add facet model ([fb394df](https://gitlab.unige.ch///commit/fb394df3a8a09ec7e65f5dca14bde63c99e2352d))

### [0.6.2](https://gitlab.unige.ch///compare/v0.6.1...v0.6.2) (2020-07-10)


### Features

* add method to get key value from enum with key ([6b6553b](https://gitlab.unige.ch///commit/6b6553b5687d58c88f2c1f4f7aa23560a1bfd3aa))


### Bug Fixes

* build error ([d7c4c98](https://gitlab.unige.ch///commit/d7c4c98e900be31d009f867d6b9adc60d7ce8ea9))

### [0.6.1](https://gitlab.unige.ch///compare/v0.6.0...v0.6.1) (2020-07-07)


### Features

* correct initValue typings to be generic ([25f7e24](https://gitlab.unige.ch///commit/25f7e241edb4db31272c467a13841928699e799e))
* fix BaseResource typing ([04e3311](https://gitlab.unige.ch///commit/04e331196a5319e4c07cf3b1179401833f3c9e8b))
* fix package-lock.json ([8572a0c](https://gitlab.unige.ch///commit/8572a0cf271568eb0ec5bc2130a0f0b1d83aa570))
* fix TResource typing issues ([c9c1293](https://gitlab.unige.ch///commit/c9c1293caf1bfaa61364f4102214ed0712ea8f3f))
* update all libraries ([d329f99](https://gitlab.unige.ch///commit/d329f99d31191664a8ac437332de791a94c0ac8f))
* update sort field typing ([7000273](https://gitlab.unige.ch///commit/700027328298eeec67deb92d472dc9865f762f65))
* use schematic testing async API only ([5fdbdf7](https://gitlab.unige.ch///commit/5fdbdf71f172d6c23b1791b84879e9924eaa1504))

## [0.6.0](https://gitlab.unige.ch///compare/v0.5.13...v0.6.0) (2020-07-06)

### [0.5.13](https://gitlab.unige.ch///compare/v0.5.12...v0.5.13) (2020-07-06)


### Bug Fixes

* overide state options ([df9ce42](https://gitlab.unige.ch///commit/df9ce42ab1ba0b682651324a699a45b9bd34156e))

### [0.5.12](https://gitlab.unige.ch///compare/v0.5.11...v0.5.12) (2020-06-17)


### Features

* add method for get label of enum on enumUtil ([6d203c2](https://gitlab.unige.ch///commit/6d203c203bf2fd81709a6714f1a8f46c1eb346a2))

### [0.5.11](https://gitlab.unige.ch///compare/v0.5.10...v0.5.11) (2020-06-08)


### Features

* use oauth2 state to preserve state between request and callback ([1a6a2f2](https://gitlab.unige.ch///commit/1a6a2f26cc413879893c18172b4f1520489db2d0))

### [0.5.10](https://gitlab.unige.ch///compare/v0.5.9...v0.5.10) (2020-06-08)


### Features

* allow to use custom query parameter on all type of api call ([664879d](https://gitlab.unige.ch///commit/664879d1d0d193ae90ea80de76f2c9529924ada7))

### [0.5.9](https://gitlab.unige.ch///compare/v0.5.8...v0.5.9) (2020-06-04)


### Bug Fixes

* add missing type on memoized util ([839d4d4](https://gitlab.unige.ch///commit/839d4d4eb8c9e70277cd7b1eb338d781759771a5))

### [0.5.8](https://gitlab.unige.ch///compare/v0.5.7...v0.5.8) (2020-06-04)


### Bug Fixes

* add type on sort interface ([8aaead3](https://gitlab.unige.ch///commit/8aaead33a02525c49098572550f8165c3a39ec52))

### [0.5.7](https://gitlab.unige.ch///compare/v0.5.6...v0.5.7) (2020-06-04)

### [0.5.6](https://gitlab.unige.ch///compare/v0.5.5...v0.5.6) (2020-06-03)


### Bug Fixes

* object nested attribute util when object is null ([bfec2d4](https://gitlab.unige.ch///commit/bfec2d47225bd161a1b5e270eeeb598e9fcd33b8))

### [0.5.5](https://gitlab.unige.ch///compare/v0.5.4...v0.5.5) (2020-05-29)


### Bug Fixes

* add sort as parameter for query parameters constructor ([9f2e035](https://gitlab.unige.ch///commit/9f2e03576428dc5b61f3d1f749cae317b5d19cbb))

### [0.5.4](https://gitlab.unige.ch///compare/v0.5.3...v0.5.4) (2020-05-26)


### Bug Fixes

* manage case error append during parralel and sequential dispatch ([fff7a00](https://gitlab.unige.ch///commit/fff7a00112287c73b1ba4b81918355ed846c0c3c))

### [0.5.3](https://gitlab.unige.ch///compare/v0.5.2...v0.5.3) (2020-05-18)


### Features

* add action on resource state to load chunk list ([6396dc7](https://gitlab.unige.ch///commit/6396dc7041fde54e94988dc5070b27a0f0af3b3f))

### [0.5.2](https://gitlab.unige.ch///compare/v0.5.1...v0.5.2) (2020-05-12)

### [0.5.1](https://gitlab.unige.ch///compare/v0.5.0...v0.5.1) (2020-05-11)


### Features

* add method to capitalize string ([eac1a52](https://gitlab.unige.ch///commit/eac1a524e92b57b478a170aa1bdd6fdf0ab9017c))

## [0.5.0](https://gitlab.unige.ch///compare/v0.4.13...v0.5.0) (2020-05-11)


### Features

* allow to add action button on snackbar ([1ed158f](https://gitlab.unige.ch///commit/1ed158f443ca8be9a218c4c97078b43de505c52c))

### [0.4.13](https://gitlab.unige.ch///compare/v0.4.12...v0.4.13) (2020-04-30)


### Bug Fixes

* solidify validation directive ([f7dafac](https://gitlab.unige.ch///commit/f7daface73c4839ec43320da13e77e4fcaa8665c))

### [0.4.12](https://gitlab.unige.ch///compare/v0.4.11...v0.4.12) (2020-04-30)


### Bug Fixes

* manage case error during refresh token retrieve ([5788e53](https://gitlab.unige.ch///commit/5788e536c11e335f8f2cdb62926a781238c83cc8))
* test ([1785f15](https://gitlab.unige.ch///commit/1785f159373787dfbf8b87f57b5c8a012488566b))

### [0.4.11](https://gitlab.unige.ch///compare/v0.4.10...v0.4.11) (2020-04-24)


### Features

* add directive shortcut, autofocus and autoscroll ([8b4a37c](https://gitlab.unige.ch///commit/8b4a37cd879d8a370ab50e5218c3bc846b05d021))

### [0.4.10](https://gitlab.unige.ch///compare/v0.4.9...v0.4.10) (2020-04-23)


### Features

* add method clone to mapping object ([07bb172](https://gitlab.unige.ch///commit/07bb172af3d4d896eb46fe077e98c76105e1906c))

### [0.4.9](https://gitlab.unige.ch///compare/v0.4.8...v0.4.9) (2020-04-23)

### [0.4.8](https://gitlab.unige.ch///compare/v0.4.7...v0.4.8) (2020-04-20)


### Bug Fixes

* snackbar display problem ([476e80a](https://gitlab.unige.ch///commit/476e80a38c080858018153d5ed46293189a43a38))

### [0.4.7](https://gitlab.unige.ch///compare/v0.4.6...v0.4.7) (2020-04-09)


### Bug Fixes

* correct imports ([4b7aaeb](https://gitlab.unige.ch///commit/4b7aaeba471b37fa95ddf99273761acf438eb7d8))
* is not null nor undefined typing ([2f1b1af](https://gitlab.unige.ch///commit/2f1b1afb0bb54e5fee9a4034921762407edcf1e7))
* oauth code extract can work on hash routing stategy ([b2bb83d](https://gitlab.unige.ch///commit/b2bb83d1303c2a72bab92d57def13e2afaee0577))

### [0.4.6](https://gitlab.unige.ch///compare/v0.4.5...v0.4.6) (2020-03-27)


### Features

* iteration ([fc74a31](https://gitlab.unige.ch///commit/fc74a318fc3ba2e0a77bddf240d51cf930de9bb7))
* migrate all libraries to latest version ([f2ac77f](https://gitlab.unige.ch///commit/f2ac77f5b1487bb14d1da08377e6d23242e84f4e))

### [0.4.5](https://gitlab.unige.ch///compare/v0.4.4...v0.4.5) (2020-03-27)

### [0.4.4](https://gitlab.unige.ch///compare/v0.4.3...v0.4.4) (2020-03-26)

### [0.4.3](https://gitlab.unige.ch///compare/v0.4.2...v0.4.3) (2020-03-26)

### [0.4.2](https://gitlab.unige.ch///compare/v0.4.1...v0.4.2) (2020-03-26)

### [0.4.1](https://gitlab.unige.ch///compare/v0.4.0...v0.4.1) (2020-03-26)

## [0.4.0](https://gitlab.unige.ch///compare/v0.3.10...v0.4.0) (2020-03-26)


### Features

* fix CoreAbstractPipe ([5b2896b](https://gitlab.unige.ch///commit/5b2896b519d81bb7b223b4fe1ee637bcdf71ddec))
* migrate schematics ([00f591c](https://gitlab.unige.ch///commit/00f591c8a356699e0962d4781272605364ede1ba))
* remove unnecessary tslint import ([6c2c524](https://gitlab.unige.ch///commit/6c2c524e3cb08121659ef6290667d8b8b5c81930))
* update lib ([b59d358](https://gitlab.unige.ch///commit/b59d3586aba1f13e9fcf6fe05f68d3dea1fad871))
* update libraries and fixed subsequent issues ([763de31](https://gitlab.unige.ch///commit/763de31573441bccfc5412197c7872de61ca1787))
* update libraries to respective viable latest and fixed subsequent issues ([605704f](https://gitlab.unige.ch///commit/605704f84db64fe47b46b2842d412282e2da18e6))
* update package-lock.json ([09ec066](https://gitlab.unige.ch///commit/09ec066da92e32662422843afbe20488861e01bd))
* update package.json and package-lock.json ([ce846e4](https://gitlab.unige.ch///commit/ce846e4614d48b52c7454bc45fc9dece060ff273))
* update state management ([0c2e419](https://gitlab.unige.ch///commit/0c2e4190ffa5443ff41a84a8b3afb0dcfddaebfe))

### [0.3.10](https://gitlab.unige.ch///compare/v0.3.9...v0.3.10) (2020-03-17)

### [0.3.9](https://gitlab.unige.ch///compare/v0.3.8...v0.3.9) (2020-03-05)


### Features

* add is promise ([8154cef](https://gitlab.unige.ch///commit/8154cef))

### [0.3.8](https://gitlab.unige.ch///compare/v0.3.7...v0.3.8) (2020-03-03)


### Features

* add basic state for simple state that want to take advantage of memoized util without inherit from resourceState compositionState and so one ([1ab0dc9](https://gitlab.unige.ch///commit/1ab0dc9))
* add missing is tool pipe ([e7375ba](https://gitlab.unige.ch///commit/e7375ba))
* add possibility to type differently output than input of api call ([dd46d40](https://gitlab.unige.ch///commit/dd46d40))

### [0.3.7](https://gitlab.unige.ch///compare/v0.3.6...v0.3.7) (2020-02-25)


### Features

* add string util method for case transform ([8b4b1e8](https://gitlab.unige.ch///commit/8b4b1e8))

### [0.3.6](https://gitlab.unige.ch///compare/v0.3.5...v0.3.6) (2020-02-19)


### Bug Fixes

* change rebuild url to static public ([6b954fe](https://gitlab.unige.ch///commit/6b954fe))

### [0.3.5](https://gitlab.unige.ch///compare/v0.3.4...v0.3.5) (2020-02-19)


### Bug Fixes

* make public method rebuild url from routing util ([7d67799](https://gitlab.unige.ch///commit/7d67799))

### [0.3.4](https://gitlab.unige.ch///compare/v0.3.3...v0.3.4) (2020-02-19)

### [0.3.3](https://gitlab.unige.ch///compare/v0.3.2...v0.3.3) (2020-02-19)


### Features

* add routing util ([9e3a51b](https://gitlab.unige.ch///commit/9e3a51b))

### [0.3.2](https://gitlab.unige.ch///compare/v0.3.1...v0.3.2) (2020-02-19)


### Bug Fixes

* update pagination when listing all resources ([774d893](https://gitlab.unige.ch///commit/774d893))


### Features

* add search all method on string util ([3ceabb8](https://gitlab.unige.ch///commit/3ceabb8))

### [0.3.1](https://gitlab.unige.ch///compare/v0.3.0...v0.3.1) (2020-02-11)


### Features

* add change query parameter action to composition state ([89b9f69](https://gitlab.unige.ch///commit/89b9f69))
* add condition on change query parameter on resource to choose to refresh or not after ([872c288](https://gitlab.unige.ch///commit/872c288))
* add message parameter on notification and remove translate boolean parameter ([e8c7b06](https://gitlab.unige.ch///commit/e8c7b06))

## [0.3.0](https://gitlab.unige.ch///compare/v0.2.27...v0.3.0) (2020-02-04)

### [0.2.27](https://gitlab.unige.ch///compare/v0.2.26...v0.2.27) (2020-01-31)


### Bug Fixes

* fix pagination for AssociationState ([310a92f](https://gitlab.unige.ch///commit/310a92f))

### [0.2.26](https://gitlab.unige.ch///compare/v0.2.25...v0.2.26) (2020-01-30)


### Bug Fixes

* update query parameters at getall method for non-sql resources ([037a4b6](https://gitlab.unige.ch///commit/037a4b6))

### [0.2.25](https://gitlab.unige.ch///compare/v0.2.24...v0.2.25) (2020-01-30)


### Features

* add possibility to not cancel get all method ([9e93901](https://gitlab.unige.ch///commit/9e93901))

### [0.2.24](https://gitlab.unige.ch///compare/v0.2.23...v0.2.24) (2020-01-29)


### Features

* optimize resource AddInList action to prevent simultaneous similar request to append ([6550980](https://gitlab.unige.ch///commit/6550980))

### [0.2.23](https://gitlab.unige.ch///compare/v0.2.22...v0.2.23) (2020-01-29)


### Features

* add is observable pipe and tool ([c387e53](https://gitlab.unige.ch///commit/c387e53))

### [0.2.22](https://gitlab.unige.ch///compare/v0.2.21...v0.2.22) (2020-01-28)


### Bug Fixes

* add default value for QueryParamters page size ([45f5406](https://gitlab.unige.ch///commit/45f5406))


### Features

* add method to deep clone query parameters ([d7ee643](https://gitlab.unige.ch///commit/d7ee643))
* add override and virtual decorator as indicator ([1025e42](https://gitlab.unige.ch///commit/1025e42))

### [0.2.21](https://gitlab.unige.ch///compare/v0.2.20...v0.2.21) (2020-01-23)


### Bug Fixes

* change name of folder to project name ([8b15061](https://gitlab.unige.ch///commit/8b15061))


### Features

* add new action to manipulate list on resource state ([542b767](https://gitlab.unige.ch///commit/542b767))
* add option when add to list with id ([8189cee](https://gitlab.unige.ch///commit/8189cee))

### [0.2.20](https://gitlab.unige.ch///compare/v0.2.19...v0.2.20) (2019-12-17)


### Features

* add method get search item on query parameter util ([0ab6a2a](https://gitlab.unige.ch///commit/0ab6a2a))
* add method size on maping object util ([5c8eb02](https://gitlab.unige.ch///commit/5c8eb02))
* allow to use store on method dispatch parallel and sequential action ([90d74f1](https://gitlab.unige.ch///commit/90d74f1))

### [0.2.19](https://gitlab.unige.ch///compare/v0.2.18...v0.2.19) (2019-12-04)

### [0.2.18](https://gitlab.unige.ch///compare/v0.2.17...v0.2.18) (2019-12-02)


### Bug Fixes

* validation error backend with solidify error ([9268aa0](https://gitlab.unige.ch///commit/9268aa0))

### [0.2.17](https://gitlab.unige.ch///compare/v0.2.16...v0.2.17) (2019-11-28)


### Features

* add option on resource clean method ([7a8e189](https://gitlab.unige.ch///commit/7a8e189))
* translate backend validation message ([c5b5848](https://gitlab.unige.ch///commit/c5b5848))

### [0.2.16](https://gitlab.unige.ch///compare/v0.2.15...v0.2.16) (2019-11-25)


### Bug Fixes

* avoid error on memoized util when state retreive is null or undefined ([1f36026](https://gitlab.unige.ch///commit/1f36026))

### [0.2.15](https://gitlab.unige.ch///compare/v0.2.14...v0.2.15) (2019-11-25)


### Features

* add get by id on composition state ([109b328](https://gitlab.unige.ch///commit/109b328))
* add method on api service to get list ([488906e](https://gitlab.unige.ch///commit/488906e))

### [0.2.14](https://gitlab.unige.ch///compare/v0.2.13...v0.2.14) (2019-11-20)


### Bug Fixes

* change header validate application/hal+json to application/json ([38ec854](https://gitlab.unige.ch///commit/38ec854))


### Features

* add hate oas model ([b1c1450](https://gitlab.unige.ch///commit/b1c1450))
* add isArray pipe ([62f05d1](https://gitlab.unige.ch///commit/62f05d1))
* add method values into object utils ([6eebb2a](https://gitlab.unige.ch///commit/6eebb2a))

### [0.2.13](https://gitlab.unige.ch///compare/v0.2.12...v0.2.13) (2019-11-19)


### Bug Fixes

* avoid error if resource state action getByIdSuccess have no parent action provided ([704d479](https://gitlab.unige.ch///commit/704d479))
* unit test ([3548fae](https://gitlab.unige.ch///commit/3548fae))


### Features

* add memoized selector method to all state and into memoized util ([dde0498](https://gitlab.unige.ch///commit/dde0498))

### [0.2.12](https://gitlab.unige.ch///compare/v0.2.11...v0.2.12) (2019-11-13)


### Bug Fixes

* add missing change query parameter to resource readonly normal and no sql ([e55ba09](https://gitlab.unige.ch///commit/e55ba09))

### [0.2.11](https://gitlab.unige.ch///compare/v0.2.10...v0.2.11) (2019-11-13)


### Bug Fixes

* error wrapping ([7039c69](https://gitlab.unige.ch///commit/7039c69))


### Features

* add util to allow copy string to clipboard ([7394f21](https://gitlab.unige.ch///commit/7394f21))

### [0.2.10](https://gitlab.unige.ch///compare/v0.2.9...v0.2.10) (2019-11-12)


### Bug Fixes

* dispatchParallelActionAndWaitForSubActionsCompletion ¨when no subaction completion ([e26b512](https://gitlab.unige.ch///commit/e26b512))
* environment injection for aot error ([60ac813](https://gitlab.unige.ch///commit/60ac813))
* remove useless parameters in action of resource readonly ([a157e56](https://gitlab.unige.ch///commit/a157e56))


### Features

* add mapping object util ([1ef0f10](https://gitlab.unige.ch///commit/1ef0f10))
* allow to use object for search item on query parameters ([31a1e83](https://gitlab.unige.ch///commit/31a1e83))
* allow to use other notifier service or none ([8781564](https://gitlab.unige.ch///commit/8781564))
* create injection token for notifier service in global error handler ([a892f26](https://gitlab.unige.ch///commit/a892f26))
* wrap solidify state error ([be94851](https://gitlab.unige.ch///commit/be94851))

### [0.2.9](https://gitlab.unige.ch///compare/v0.2.8...v0.2.9) (2019-11-06)


### Bug Fixes

* dispatchParallelActionAndWaitForSubActionsCompletion to return observable when no action ([def2bfd](https://gitlab.unige.ch///commit/def2bfd))
* lint error missing fdescribe ([ae888b3](https://gitlab.unige.ch///commit/ae888b3))


### Features

* add array util with distinct method ([cb42c84](https://gitlab.unige.ch///commit/cb42c84))
* allow retreive only missing data on action get by list id for resource state ([586ee4b](https://gitlab.unige.ch///commit/586ee4b))

### [0.2.8](https://gitlab.unige.ch///compare/v0.2.7...v0.2.8) (2019-11-06)


### Features

* add resource action get by list id ([72bf7ea](https://gitlab.unige.ch///commit/72bf7ea))

### [0.2.7](https://gitlab.unige.ch///compare/v0.2.6...v0.2.7) (2019-11-06)

### [0.2.6](https://gitlab.unige.ch///compare/v0.2.5...v0.2.6) (2019-11-06)


### Bug Fixes

* rollback update to translate validation message ([8c6a829](https://gitlab.unige.ch///commit/8c6a829))

### [0.2.5](https://gitlab.unige.ch///compare/v0.2.4...v0.2.5) (2019-11-06)


### Bug Fixes

* missing coma in package json ([f0544b0](https://gitlab.unige.ch///commit/f0544b0))
* move as peer dependencies libs ([d89d565](https://gitlab.unige.ch///commit/d89d565))

### [0.2.4](https://gitlab.unige.ch///compare/v0.2.3...v0.2.4) (2019-11-05)


### Bug Fixes

* add environment variable to translate validation message ([3465bb0](https://gitlab.unige.ch///commit/3465bb0))

### [0.2.3](https://gitlab.unige.ch///compare/v0.2.2...v0.2.3) (2019-11-05)


### Bug Fixes

* move schematic to dev dependancy ([ed0cef9](https://gitlab.unige.ch///commit/ed0cef9))
* unit test on date util ([6e11737](https://gitlab.unige.ch///commit/6e11737))
* update date format ISO8601 to include ms ([f8bf41f](https://gitlab.unige.ch///commit/f8bf41f))

### [0.2.2](https://gitlab.unige.ch///compare/v0.2.1...v0.2.2) (2019-11-04)


### Features

* add usage of query parameters in composition state ([d6949c2](https://gitlab.unige.ch///commit/d6949c2))

### [0.2.1](https://gitlab.unige.ch///compare/v0.2.0...v0.2.1) (2019-10-31)

## [0.2.0](https://gitlab.unige.ch///compare/v0.1.21...v0.2.0) (2019-10-29)

### [0.1.21](https://gitlab.unige.ch///compare/v0.1.20...v0.1.21) (2019-10-25)


### Features

* add avaibility to use callback to redirect after create, update or delete success of resource ([02a88b6](https://gitlab.unige.ch///commit/02a88b6))

### [0.1.20](https://gitlab.unige.ch///compare/v0.1.19...v0.1.20) (2019-10-24)


### Features

* add api method to patch by id with custom url ([fd7a572](https://gitlab.unige.ch///commit/fd7a572))

### [0.1.19](https://gitlab.unige.ch///compare/v0.1.18...v0.1.19) (2019-10-23)

### [0.1.18](https://gitlab.unige.ch///compare/v0.1.17...v0.1.18) (2019-10-23)


### Bug Fixes

* update value and validity of field with only frontend error ([9bc1d37](https://gitlab.unige.ch///commit/9bc1d37))


### Features

* add validation directive ([cc3f7db](https://gitlab.unige.ch///commit/cc3f7db))

### [0.1.17](https://gitlab.unige.ch///compare/v0.1.16...v0.1.17) (2019-10-18)


### Features

* allow usage of component for notification snackbar ([294e369](https://gitlab.unige.ch///commit/294e369))

### [0.1.16](https://gitlab.unige.ch///compare/v0.1.15...v0.1.16) (2019-10-15)


### Bug Fixes

* allow status in error dto to be a number ([1a985b3](https://gitlab.unige.ch///commit/1a985b3))
* DLCM-822 override url at runtime not take into account on NGXS state ([465bce2](https://gitlab.unige.ch///commit/465bce2))

### [0.1.15](https://gitlab.unige.ch///compare/v0.1.14...v0.1.15) (2019-10-08)


### Features

* add relation 3 tiers get by id ([99072df](https://gitlab.unige.ch///commit/99072df))

### [0.1.14](https://gitlab.unige.ch///compare/v0.1.13...v0.1.14) (2019-10-04)


### Bug Fixes

* restore to default value before create ([f9913d4](https://gitlab.unige.ch///commit/f9913d4))

### [0.1.13](https://gitlab.unige.ch///compare/v0.1.12...v0.1.13) (2019-10-03)


### Bug Fixes

* clean selected value after update/delete success without refresh ([4620020](https://gitlab.unige.ch///commit/4620020))
* export base state ([02f0413](https://gitlab.unige.ch///commit/02f0413))

### [0.1.12](https://gitlab.unige.ch///compare/v0.1.11...v0.1.12) (2019-10-01)


### Bug Fixes

* make configuration reinit of state when get ([de3c3a3](https://gitlab.unige.ch///commit/de3c3a3))
* reset current and select value on get all and get by id ([eb2e29c](https://gitlab.unige.ch///commit/eb2e29c))
* set selected and current value to undefined when get ([39d78d8](https://gitlab.unige.ch///commit/39d78d8))

### [0.1.11](https://gitlab.unige.ch///compare/v0.1.10...v0.1.11) (2019-09-26)


### Bug Fixes

* lint error ([9a71912](https://gitlab.unige.ch///commit/9a71912))

### [0.1.10](https://gitlab.unige.ch///compare/v0.1.9...v0.1.10) (2019-09-25)


### Bug Fixes

* lint error ([4f6330e](https://gitlab.unige.ch///commit/4f6330e))
* relation 3 tiers to set as sequential ([4af82c3](https://gitlab.unige.ch///commit/4af82c3))
* ts lint error ([5f6a7d5](https://gitlab.unige.ch///commit/5f6a7d5))

### [0.1.9](https://gitlab.unige.ch///compare/v0.1.8...v0.1.9) (2019-09-23)


### Features

* add method in store util to force sequential call to backend ([52460d2](https://gitlab.unige.ch///commit/52460d2))

### [0.1.8](https://gitlab.unige.ch///compare/v0.1.7...v0.1.8) (2019-09-20)


### Bug Fixes

* relation 3 tiers ([46cd449](https://gitlab.unige.ch///commit/46cd449))

### [0.1.7](https://gitlab.unige.ch///compare/v0.1.6...v0.1.7) (2019-09-17)


### Features

* update resource state ([016d9f5](https://gitlab.unige.ch///commit/016d9f5))

### [0.1.6](https://gitlab.unige.ch///compare/v0.1.5...v0.1.6) (2019-09-16)


### Bug Fixes

* update action on resource state update the current value ([98b0824](https://gitlab.unige.ch///commit/98b0824))


### Features

* add enum util ([f305168](https://gitlab.unige.ch///commit/f305168))

### [0.1.5](https://gitlab.unige.ch///compare/v0.1.4...v0.1.5) (2019-09-13)


### Bug Fixes

* comment the memoized selector isLoading that doesn't work when there is several usage ([bd0cb44](https://gitlab.unige.ch///commit/bd0cb44))

### [0.1.4](https://gitlab.unige.ch///compare/v0.1.3...v0.1.4) (2019-09-12)


### Features

* Update state ([e32ed5b](https://gitlab.unige.ch///commit/e32ed5b))

### [0.1.3](https://gitlab.unige.ch///compare/v0.1.2...v0.1.3) (2019-09-10)


### Features

* add method in date util ([36934fc](https://gitlab.unige.ch///commit/36934fc))

### [0.1.2](https://gitlab.unige.ch///compare/v0.1.1...v0.1.2) (2019-09-04)


### Bug Fixes

* ignore ts lint error on ngxs selector ([583dc3a](https://gitlab.unige.ch///commit/583dc3a))


### Features

* add memoized selector on state ([5a71107](https://gitlab.unige.ch///commit/5a71107))
* **solidify-frontend:** new relation state ([fe26e36](https://gitlab.unige.ch///commit/fe26e36))

### [0.1.1](https://gitlab.unige.ch///compare/v0.1.0...v0.1.1) (2019-09-03)


### Features

* add readonly state ([6465d3c](https://gitlab.unige.ch///commit/6465d3c))

## [0.1.0](https://gitlab.unige.ch///compare/v0.0.8...v0.1.0) (2019-08-29)


### Bug Fixes

* don't log stacktrace error to avoid overload of useless data (JS info) ([c2e7657](https://gitlab.unige.ch///commit/c2e7657))

### [0.0.8](https://gitlab.unige.ch///compare/v0.0.7...v0.0.8) (2019-08-26)


### Features

* add option object on all state ([d0d43e3](https://gitlab.unige.ch///commit/d0d43e3))

### [0.0.7](https://gitlab.unige.ch///compare/v0.0.6...v0.0.7) (2019-08-26)


### Features

* translate all notifications ([565fc4e](https://gitlab.unige.ch///commit/565fc4e))

### [0.0.6](https://gitlab.unige.ch///compare/v0.0.5...v0.0.6) (2019-08-23)


### Features

* add delete list action for association remote ([3b3d329](https://gitlab.unige.ch///commit/3b3d329))

### [0.0.5](https://gitlab.unige.ch///compare/v0.0.3...v0.0.5) (2019-08-22)


### Bug Fixes

* async validation on input that doesn't have other validation ([3ce1bda](https://gitlab.unige.ch///commit/3ce1bda))
* date result when input is null, undefined or whitespace ([535d69f](https://gitlab.unige.ch///commit/535d69f))
* remove oauth information in url after authentification ([d94ecf9](https://gitlab.unige.ch///commit/d94ecf9))

### [0.0.3](https://gitlab.unige.ch///compare/v0.0.1...v0.0.3) (2019-08-20)


### Bug Fixes

* allow override of env setting on OAuth2Interceptor ([1909ad3](https://gitlab.unige.ch///commit/1909ad3))
* increase state ([675ebe7](https://gitlab.unige.ch///commit/675ebe7))
* unit test error with environment injection token import ([b1332b8](https://gitlab.unige.ch///commit/b1332b8))


### Features

* add on relation 2 tiers Update Relation and Delete By Id ([0bb3745](https://gitlab.unige.ch///commit/0bb3745))

### [0.0.2](https://gitlab.unige.ch///compare/v0.0.1...v0.0.2) (2019-08-09)

### 0.0.1 (2019-08-08)

### 0.0.1 (2019-07-26)

## 0.0.0 (2019-07-25)
