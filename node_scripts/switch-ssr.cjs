const utils = require("./utils.cjs");
const findValueInProcessArgv = utils.findValueInProcessArgv;
const findValuesInProcessArgv = utils.findValuesInProcessArgv;
const escapeRegExp = utils.escapeRegExp;
const mapFile = utils.mapFile;
const walkSequential = utils.walkSequential;

/*
 * Switch SSR
 *
 * Process arguments :
 *   directoryPaths: directory paths to process, separated by "," or ";" (no default)
 *   srr: ssr mode (default: false)
 *   silent: silent output without logs (default: false)
 *
 * */

const stringArraySeparator = /\s*[,;]\s*/g;
const startingSpacesRegExp = /^\s*/;
const baseSsrCommentPlaceholderComment = "ssr:comment:placeholder";
const baseSsrCommentStartComment = "ssr:comment:start";
const baseSsrCommentEndComment = "ssr:comment:end";
const baseSsrUncommentStartComment = "ssr:uncomment:start";
const baseSsrUncommentEndComment = "ssr:uncomment:end";

const directoryPathsProcessArgumentKey = "directoryPaths";
const silentProcessArgumentKey = "silent";
const ssrArgumentKey = "ssr";

const directoryPaths = findValuesInProcessArgv(directoryPathsProcessArgumentKey, stringArraySeparator, true, true);
const silent = findValueInProcessArgv(silentProcessArgumentKey) === "true" || false;
const ssr = findValueInProcessArgv(ssrArgumentKey) === "true" || false;

const log = silent ? new Function() : console.log;

function switchSsr() {
    if (directoryPaths.length) {
        directoryPaths.forEach(function (directoryPath) {
            walkSequential(
                directoryPath,
                function (err, result) {
                    result.forEach(function (filePath) {
                        if (!/\.(?:ts|js|scss|html)$/.test(filePath)) {
                            return;
                        }
                        mapFile(
                            filePath,
                            function (data) {
                                const lineSeparatorString = data.indexOf("\r\n") >= 0 ? "\r\n" : data.indexOf("\n") >= 0 ? "\n" : "\r";
                                const lines = data.split(lineSeparatorString);
                                handleSsrCommentWithDoubleSlashComments(ssr, lines, filePath);
                                handleSsrUncommentWithDoubleSlashComments(ssr, lines, filePath);
                                handleSsrCommentWithXmlComments(ssr, lines, filePath);
                                handleSsrUncommentWithXmlComments(ssr, lines, filePath);
                                return lines.join(lineSeparatorString);
                            },
                            function (effective) {
                                if (effective) {
                                    log("Success : Ssr update effective in file path '" + filePath + "'");
                                } else {
                                    //log("Error : Srr update not effective in file path '" + filePath + "'");
                                }
                            }
                        );
                    })
                }
            );
        });
    }
}

function onBlockInLines(lines, getIsBlockStartLine, getIsBlockEndLine, callback, options) {
    options = options || {};
    const fromIndex = options.fromIndex || 0;
    const toIndex = options.toIndex || lines.length;
    const isBlockStartLine = getIsBlockStartLine();
    let blockStartLineIndex = -1;
    for (let lineIndex = fromIndex; lineIndex < toIndex; lineIndex++) {
        const line = lines[lineIndex];
        const isBlockStartLineOrDelta = isBlockStartLine(line, lineIndex);
        if (isBlockStartLineOrDelta) {
            blockStartLineIndex = lineIndex + (typeof isBlockStartLineOrDelta === "number" ? isBlockStartLineOrDelta : 0);
            break;
        }
    }
    if (blockStartLineIndex >= 0 && blockStartLineIndex < lines.length) {
        const blockStartLine = lines[blockStartLineIndex];
        const blockStartLineStartingSpaces = startingSpacesRegExp.exec(blockStartLine)[0];
        const isBlockEndLine = getIsBlockEndLine(blockStartLine, blockStartLineIndex, blockStartLineStartingSpaces);
        let blockEndLineIndex = -1;
        const adjustedFromIndex = blockStartLineIndex + 1;
        for (let lineIndex = adjustedFromIndex; lineIndex < toIndex; lineIndex++) {
            const line = lines[lineIndex]
            const isBlockEndLineOrDelta = isBlockEndLine(line, lineIndex);
            if (isBlockEndLineOrDelta) {
                blockEndLineIndex = lineIndex + (typeof isBlockEndLineOrDelta === "number" ? isBlockEndLineOrDelta : 0);
                break;
            }
        }
        if (blockEndLineIndex >= 0 && blockEndLineIndex < lines.length) {
            const blockEndLine = lines[blockEndLineIndex];
            callback(blockStartLine, blockStartLineIndex, blockStartLineStartingSpaces, blockEndLine, blockEndLineIndex);
            options.fromIndex = blockEndLineIndex;
            onBlockInLines(lines, getIsBlockStartLine, getIsBlockEndLine, callback, options);
        } else {
            if (options.onBlockEndLineNotFound) {
                options.onBlockEndLineNotFound(fromIndex, toIndex, adjustedFromIndex);
            }
        }
    } else {
        if (options.onBlockStartLineNotFound) {
            options.onBlockStartLineNotFound(fromIndex, toIndex);
        }
    }
}

function handleSsrCommentsInLines(ssr, lines, getIsBlockStartLine, getIsBlockEndLine, formatLine, actionLabel, filePath) {
    onBlockInLines(
        lines,
        function () {
            return getIsBlockStartLine();
        },
        function (blockStartLine, blockStartLineIndex, blockStartLineStartingSpaces) {
            return getIsBlockEndLine(blockStartLine, blockStartLineIndex, blockStartLineStartingSpaces);
        },
        function (blockStartLine, blockStartLineIndex, blockStartLineStartingSpaces, blockEndLine, blockEndLineIndex) {
            const blockLength = blockEndLineIndex - blockStartLineIndex + 1;
            log("Info : " + actionLabel + " (" + blockLength + " lines from line " + blockStartLineIndex + " to line " + blockEndLineIndex + ") ready to " + actionLabel + " in file path '" + filePath + "'");
            for (let lineIndex = blockStartLineIndex + 1; lineIndex < blockEndLineIndex; lineIndex++) {
                lines[lineIndex] = formatLine(lines[lineIndex], blockStartLine, blockStartLineIndex, blockStartLineStartingSpaces);
            }
        },
        {
            fromIndex: undefined,
            toIndex: undefined,
            onBlockStartLineNotFound: function (fromIndex, toIndex) {
                // log("Warning : " + actionLabel + " not found in file path '" + filePath + "' (from line " + fromIndex + " to line " + toIndex + ")");
            },
            onBlockEndLineNotFound: function (fromIndex, toIndex, adjustedFromIndex) {
                log("Warning : " + actionLabel + " had no ending found in file path '" + filePath + "' (from line " + adjustedFromIndex + " to line " + toIndex + ")");
            }
        }
    );
}

function handleSsrCommentWithDoubleSlashComments(ssr, lines, filePath) {
    if (!/\.(?:ts|js|scss)$/.test(filePath)) {
        return;
    }
    const commentedPlaceholderComment = "// " + baseSsrCommentPlaceholderComment;
    return handleSsrCommentsInLines(
        ssr,
        lines,
        function () {
            const commentRegExp = new RegExp("^\\s*\\/\\/\\s*" + escapeRegExp(baseSsrCommentStartComment));
            return function (line, lineIndex) {
                return commentRegExp.test(line);
            }
        },
        function (blockStartLine, blockStartLineIndex, blockStartLineStartingSpaces) {
            const commentRegExp = new RegExp("^\\s*\\/\\/\\s*" + escapeRegExp(baseSsrCommentEndComment));
            return function (line, lineIndex) {
                return commentRegExp.test(line);
            }
        },
        function (line, blockStartLine, blockStartLineIndex, blockStartLineStartingSpaces) {
            if (ssr) {
                return line.indexOf(commentedPlaceholderComment) >= 0 ? line : commentedPlaceholderComment + line;
            } else {
                return line.replace(commentedPlaceholderComment, "");
            }
        },
        ssr ? "enable ssr comment with double slash comments" : "disable ssr comment with double slash comments",
        filePath
    );
}

function handleSsrUncommentWithDoubleSlashComments(ssr, lines, filePath) {
    if (!/\.(?:ts|js|scss)$/.test(filePath)) {
        return;
    }
    const placeholderComment = "/* " + baseSsrCommentPlaceholderComment + " */";
    return handleSsrCommentsInLines(
        ssr,
        lines,
        function () {
            const commentRegExp = new RegExp("^\\s*\\/\\/\\s*" + escapeRegExp(baseSsrUncommentStartComment));
            return function (line, lineIndex) {
                return commentRegExp.test(line);
            }
        },
        function (blockStartLine, blockStartLineIndex, blockStartLineStartingSpaces) {
            const commentRegExp = new RegExp("^\\s*\\/\\/\\s*" + escapeRegExp(baseSsrUncommentEndComment));
            return function (line, lineIndex) {
                return commentRegExp.test(line);
            }
        },
        function (line, blockStartLine, blockStartLineIndex, blockStartLineStartingSpaces) {
            if (ssr) {
                return line.replace("//", placeholderComment);
            } else {
                return line.replace(placeholderComment, "//");
            }
        },
        ssr ? "enable ssr uncomment with double slash comments" : "disable ssr uncomment with double slash comments",
        filePath
    );
}

function handleSsrCommentWithXmlComments(ssr, lines, filePath) {
    if (!/\.(?:html)$/.test(filePath)) {
        return;
    }
    const commentedPlaceholderComment = "<!-- " + baseSsrCommentPlaceholderComment;
    return handleSsrCommentsInLines(
        ssr,
        lines,
        function () {
            const commentRegExp = new RegExp("^\\s*" + escapeRegExp("<!--") + "\\s*" + escapeRegExp(baseSsrCommentStartComment) + "\\s*" + escapeRegExp("-->"));
            return function (line, lineIndex) {
                return commentRegExp.test(line);
            }
        },
        function (blockStartLine, blockStartLineIndex, blockStartLineStartingSpaces) {
            const commentRegExp = new RegExp("^\\s*" + escapeRegExp("<!--") + "\\s*" + escapeRegExp(baseSsrCommentEndComment) + "\\s*" + escapeRegExp("-->"));
            return function (line, lineIndex) {
                return commentRegExp.test(line);
            }
        },
        function (line, blockStartLine, blockStartLineIndex, blockStartLineStartingSpaces) {
            if (ssr) {
                return line.indexOf(commentedPlaceholderComment) >= 0 ? line : commentedPlaceholderComment + line + " -->";
            } else {
                return line.replace(commentedPlaceholderComment, "").replace(" -->", "");
            }
        },
        ssr ? "enable ssr comment with xml comments" : "disable ssr comment with xml comments",
        filePath
    );
}

function handleSsrUncommentWithXmlComments(ssr, lines, filePath) {
    if (!/\.(?:html)$/.test(filePath)) {
        return;
    }
    const placeholderComment = "<!-- " + baseSsrCommentPlaceholderComment + " -->";
    return handleSsrCommentsInLines(
        ssr,
        lines,
        function () {
            const commentRegExp = new RegExp("^\\s*" + escapeRegExp("<!--") + "\\s*" + escapeRegExp(baseSsrUncommentStartComment) + "\\s*" + escapeRegExp("-->"));
            return function (line, lineIndex) {
                return commentRegExp.test(line);
            }
        },
        function (blockStartLine, blockStartLineIndex, blockStartLineStartingSpaces) {
            const commentRegExp = new RegExp("^\\s*" + escapeRegExp("<!--") + "\\s*" + escapeRegExp(baseSsrUncommentEndComment) + "\\s*" + escapeRegExp("-->"));
            return function (line, lineIndex) {
                return commentRegExp.test(line);
            }
        },
        function (line, blockStartLine, blockStartLineIndex, blockStartLineStartingSpaces) {
            if (ssr) {
                return line.replace(" -->", "").replace("<!-- ", placeholderComment);
            } else {
                return line.indexOf(placeholderComment) >= 0 ? line.replace(placeholderComment, "<!-- ") + " -->" : line;
            }
        },
        ssr ? "enable ssr uncomment with xml comments" : "disable ssr uncomment with xml comments",
        filePath
    );
}

switchSsr();
