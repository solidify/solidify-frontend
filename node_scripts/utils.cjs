const fs = require("fs");
const path = require("path");
const zlib = require("zlib");

/*
 * Escape a string for being used in RegExp construction
 *
 * const regExpSpecialCharacters =
 *   ["!", "$", "(", ")", "*", "+", "-", ".", ":", "<", "=", ">", "?", "[", "]", "^", "{", "|", "}"];
 *
 * const regExpDelimiters =
 *   ["/"];
 *
 * const stringDelimiters =
 *   ["\"", "'"];
 *
 * Escapement :
 *   All characters from regExpSpecialCharacters, regExpDelimiters and stringDelimiters are escaped
 *
 * Note :
 *   Escaped characters have been ordered by charCodeAt(0)
 *
 * */
function escapeRegExp(str) {
    return str.replace(/[\!\"\$\'\(\)\*\+\-\.\/\:\<\=\>\?\[\]\^\{\|\}]/g, "\\$&");
}

function findValueInProcessArgv(flag) {
    if (typeof flag !== "string" || !(flag = flag.replace(/^[\s-]+|[\s=]+$/g, ""))) {
        return undefined;
    }
    const matchingRegExp = new RegExp("^-{0,2}" + escapeRegExp(flag) + "(?:=|$)", "gi");
    const matchingProcessArguments = process.argv.slice(2).filter(function (argv) {
            return ~argv.toString().search(matchingRegExp);
        }
    );
    const matchingProcessArgvValues = matchingProcessArguments.map(function (argv) {
        return argv.toString().replace(matchingRegExp, "") || "true";
    });
    return matchingProcessArgvValues.length && matchingProcessArgvValues[0] || undefined;
}

function findValuesInProcessArgv(flag, separator, trim, filterNonEmpty) {
    let values = (findValueInProcessArgv(flag) || "").split(separator);
    if (trim) {
        values = values.map(function (value) {
            return value.trim();
        });
    }
    if (filterNonEmpty) {
        values = values.filter(function (value) {
            return value.length > 0;
        });
    }
    return values;
}

function fileExists(filePath) {
    const _fs = fs || require("fs");
    return _fs.existsSync(filePath) && _fs.statSync(filePath).isFile();
}

function readFile(filePath, callback, encoding) {
    const _encoding = encoding && typeof encoding === "string" ? encoding : "utf-8";
    const _fs = fs || require("fs");
    _fs.readFile(filePath, _encoding, function (err, data) {
        if (err) {
            return console.error(err);
        }
        if (typeof callback === "function") {
            callback(data, filePath, _encoding);
        }
    });
}

function writeFile(filePath, content, callback, encoding) {
    const _encoding = encoding && typeof encoding === "string" ? encoding : "utf-8";
    const _fs = fs || require("fs");
    _fs.writeFile(filePath, content, _encoding, function (err) {
        if (err) {
            return console.error(err);
        }
        if (typeof callback === "function") {
            callback(filePath, content, _encoding);
        }
    });
}

function mapFile(filePath, map, callback, encoding) {
    const _encoding = encoding && typeof encoding === "string" ? encoding : "utf-8";
    const _fs = fs || require("fs");
    _fs.readFile(filePath, _encoding, function (err, data) {
        if (err) {
            return console.error(err);
        }
        const result = typeof map === "function" ? map(data, filePath, encoding) : map ? map.toString() : "";
        const effective = result !== data;
        _fs.writeFile(filePath, result, _encoding, function (err) {
            if (err) {
                return console.error(err);
            }
            if (typeof callback === "function") {
                callback(effective, result, data, filePath, _encoding);
            }
        });
    });
}

function replaceInFile(filePath, searchValue, replaceValue, callback, encoding) {
    return mapFile(
        filePath,
        function (data) {
            return data.replace(searchValue, replaceValue);
        },
        callback,
        encoding
    );
}

function walkParallel(dir, done, doer) {
    const _fs = fs || require("fs");
    const _path = path || require("path");
    const hasDoer = typeof doer === "function";
    let results = [];
    _fs.readdir(dir, function (err, list) {
        if (err) {
            return done(err);
        }
        let pending = list.length;
        if (!pending) {
            return done(null, results);
        }
        list.forEach(function (fileName) {
            const filePath = _path.resolve(dir, fileName);
            _fs.stat(filePath, function (statErr, stat) {
                if (stat && stat.isDirectory()) {
                    walkParallel(filePath, function (innerErr, res) {
                        results = results.concat(res);
                        if (!--pending) {
                            done(null, results);
                        }
                    }, doer);
                } else {
                    function doo() {
                        results.push(filePath);
                        if (!--pending) {
                            done(null, results);
                        }
                    }

                    if (hasDoer) {
                        doer(filePath, doo);
                    } else {
                        doo();
                    }
                }
            });
        });
    });
}

function walkSequential(dir, done, doer) {
    const _fs = fs || require("fs");
    const _path = path || require("path");
    const hasDoer = typeof doer === "function";
    let results = [];
    _fs.readdir(dir, function (err, list) {
        if (err) {
            return done(err);
        }
        let i = 0;
        (function next() {
            const fileName = list[i++];
            if (!fileName) {
                return done(null, results);
            }
            const filePath = _path.resolve(dir, fileName);
            _fs.stat(filePath, function (statErr, stat) {
                if (stat && stat.isDirectory()) {
                    walkSequential(filePath, function (innerErr, res) {
                        results = results.concat(res);
                        next();
                    }, doer);
                } else {
                    function doo() {
                        results.push(filePath);
                        next();
                    }

                    if (hasDoer) {
                        doer(filePath, doo);
                    } else {
                        doo();
                    }
                }
            });
        })();
    });
}

function wordsToCamelCase(words, upperFirstLetter) {
    const _words = Array.isArray(words) ? words : ("" + words).match(/\w+/g);
    const _filteredWords = Array.isArray(_words) ? _words.filter(function (word) {
        return word && typeof word === "string"
    }) : [];
    if (!_filteredWords.length) {
        return "";
    }
    const camelCaseResult = _filteredWords.map(function (word) {
        return word[0].toUpperCase() + word.substr(1).toLowerCase();
    }).join("");
    return upperFirstLetter ? camelCaseResult : camelCaseResult[0].toLowerCase() + camelCaseResult.substr(1);
}

function pipeIntoFileReadWriteStream(filePath, outputFilePath, pipe) {
    const readStream = fs.createReadStream(filePath);
    const writeStream = fs.createWriteStream(outputFilePath);
    return readStream.pipe(pipe).pipe(writeStream);
}

function gzip(filePath, outputFilePath) {
    return pipeIntoFileReadWriteStream(filePath, outputFilePath || filePath + ".gz", zlib.createGzip());
}

function getFileSizeInBytes(fileName) {
    return fs.statSync(fileName).size;
}

// Module exports

exports.escapeRegExp = escapeRegExp;
exports.findValueInProcessArgv = findValueInProcessArgv;
exports.findValuesInProcessArgv = findValuesInProcessArgv;
exports.fileExists = fileExists;
exports.writeFile = writeFile;
exports.readFile = readFile;
exports.mapFile = mapFile;
exports.replaceInFile = replaceInFile;
exports.walkParallel = walkParallel;
exports.walkSequential = walkSequential;
exports.wordsToCamelCase = wordsToCamelCase;
exports.gzip = gzip;
exports.getFileSizeInBytes = getFileSizeInBytes;
