module.exports = {
    root: true,
    ignorePatterns: [
        "projects/**/*",
        "node_modules/**/*"
    ],
    overrides: [
        {
            files: [
                "*.ts"
            ],
            parserOptions: {
                tsconfigRootDir: __dirname,
                project: [
                    "tsconfig.json",
                    "e2e/tsconfig.json"
                ],
                createDefaultProgram: true
            },
            extends: [
                "plugin:@angular-eslint/template/process-inline-templates"
            ],
            plugins: [
                "@angular-eslint",
                "@typescript-eslint",
                "eslint-plugin-import",
                "eslint-plugin-jsdoc",
                "eslint-plugin-prefer-arrow",
                "unused-imports",
                "solidify",
                "@stylistic",
            ],
            rules: {
                // ng-cli-compat rules
                "@typescript-eslint/interface-name-prefix": "off",
                "sort-keys": "off",
                "@angular-eslint/template/no-negated-async": "off",
                "@angular-eslint/contextual-lifecycle": "error",
                "@angular-eslint/no-conflicting-lifecycle": "error",
                "@angular-eslint/no-host-metadata-property": "error",
                "@angular-eslint/no-inputs-metadata-property": "error",
                "@angular-eslint/no-output-native": "error",
                "@angular-eslint/no-outputs-metadata-property": "error",
                "@angular-eslint/use-lifecycle-interface": "error",
                "@angular-eslint/use-pipe-transform-interface": "error",
                "@typescript-eslint/adjacent-overload-signatures": "error",
                "@typescript-eslint/consistent-type-assertions": "error",
                "@typescript-eslint/no-empty-function": "off",
                "@typescript-eslint/no-explicit-any": "off",
                "@typescript-eslint/no-misused-new": "error",
                "@typescript-eslint/no-non-null-assertion": "error",
                "@typescript-eslint/no-parameter-properties": "off",
                "@typescript-eslint/no-use-before-define": "off",
                "@typescript-eslint/no-var-requires": "off",
                "@typescript-eslint/prefer-for-of": "error",
                "@typescript-eslint/prefer-function-type": "error",
                "@typescript-eslint/prefer-namespace-keyword": "error",
                "@typescript-eslint/triple-slash-reference": [
                    "error",
                    {
                        "path": "always",
                        "types": "prefer-import",
                        "lib": "always"
                    }
                ],
                "@typescript-eslint/unified-signatures": "error",
                "constructor-super": "error",
                "eqeqeq": ["error", "smart"],
                "guard-for-in": "error",
                "id-blacklist": [
                    "error",
                    "any",
                    "Number",
                    "number",
                    "String",
                    "string",
                    "Boolean",
                    "boolean",
                    "Undefined",
                    "undefined"
                ],
                "id-match": "error",
                // "jsdoc/newline-after-description": "error",
                "max-classes-per-file": "off",
                "no-bitwise": "error",
                "no-caller": "error",
                "no-cond-assign": "error",
                "no-debugger": "error",
                "no-empty": "off",
                "no-eval": "error",
                "no-fallthrough": "error",
                "no-invalid-this": "off",
                "no-new-wrappers": "error",
                "@typescript-eslint/no-shadow": [
                    "error",
                    {
                        "hoist": "all"
                    }
                ],
                "no-throw-literal": "error",
                "no-unsafe-finally": "error",
                "no-unused-labels": "error",
                "no-var": "error",
                "one-var": ["error", "never"],
                "prefer-arrow/prefer-arrow-functions": "error",
                "prefer-const": "error",
                "radix": "error",
                "use-isnan": "error",
                "valid-typeof": "off",
                // ng-cli-compat--formatting-add-on rules
                "arrow-parens": "off",
                "curly": "error",
                "eol-last": "error",
                "jsdoc/check-alignment": "error",
                "new-parens": "error",
                "no-multiple-empty-lines": "off",
                "no-trailing-spaces": "error",
                "quote-props": ["error", "as-needed"],
                "space-before-function-paren": [
                    "error",
                    {
                        "anonymous": "never",
                        "asyncArrow": "always",
                        "named": "never"
                    }
                ],
                "quotes": "off",
                "@stylistic/type-annotation-spacing": "error",
                // solidify rules
                "solidify/copyright-header": ["error",
                    {
                        "file": "copyright-header.js",
                        "inceptionYear": "2017",
                    }
                ],
                "solidify/readonly-injectables": "error",
                "@angular-eslint/prefer-on-push-component-change-detection": "error",
                "@angular-eslint/component-class-suffix": [
                    "error",
                    {
                        "suffixes": [
                            "AppComponent",
                            "Routable",
                            "Presentational",
                            "Container",
                            "Dialog",
                            "Form"
                        ]
                    }
                ],
                "@angular-eslint/component-selector": [
                    "error",
                    {
                        "type": "element",
                        "style": "kebab-case"
                    }
                ],
                "@angular-eslint/directive-class-suffix": "off",
                "@angular-eslint/directive-selector": [
                    "error",
                    {
                        "type": "attribute",
                        "style": "camelCase"
                    }
                ],
                "@angular-eslint/no-input-rename": "off",
                "@angular-eslint/no-output-on-prefix": "off",
                "@angular-eslint/no-output-rename": "off",
                "@typescript-eslint/array-type": [
                    "error",
                    {
                        "default": "array"
                    }
                ],
                "@typescript-eslint/consistent-type-definitions": "error",
                "@typescript-eslint/dot-notation": "off",
                "@typescript-eslint/explicit-member-accessibility": [
                    "off",
                    {
                        "accessibility": "explicit"
                    }
                ],
                "@stylistic/member-delimiter-style": [
                    "error",
                    {
                        "multiline": {
                            "delimiter": "semi",
                            "requireLast": true
                        },
                        "singleline": {
                            "delimiter": "comma",
                            "requireLast": false
                        }
                    }
                ],
                "no-unused-vars": "off",
                "unused-imports/no-unused-imports": "error",
                "@typescript-eslint/member-ordering": "off",
                "@typescript-eslint/naming-convention": [
                    "error",
                    {
                        "selector": "variable",
                        "format": [
                            "camelCase",
                            "UPPER_CASE",
                            "snake_case",
                            "PascalCase"
                        ]
                    },
                    {
                        "selector": ["function", "variable"],
                        "modifiers": ["protected"],
                        "format": ["camelCase"],
                        "leadingUnderscore": "require"
                    },
                    {
                        "selector": ["function", "variable"],
                        "modifiers": ["private"],
                        "format": ["camelCase"],
                        "leadingUnderscore": "require"
                    },
                    {
                        "selector": ["property"],
                        "modifiers": ["protected"],
                        "format": ["camelCase", "UPPER_CASE"],
                        "leadingUnderscore": "require"
                    },
                    {
                        "selector": ["property"],
                        "modifiers": ["private"],
                        "format": ["camelCase", "UPPER_CASE"],
                        "leadingUnderscore": "require"
                    },
                    {
                        "selector": ["variable"],
                        "modifiers": ["private", "readonly"],
                        "format": ["UPPER_CASE", "camelCase"],
                        "leadingUnderscore": "require"
                    },
                    {
                        "selector": ["variable"],
                        "modifiers": ["protected", "readonly"],
                        "format": ["UPPER_CASE", "camelCase"],
                        "leadingUnderscore": "require"
                    },
                ],
                "@typescript-eslint/no-empty-interface": "off",
                "@typescript-eslint/no-namespace": [
                    "off",
                    {
                        "allowDeclarations": true,
                        "allowDefinitionFiles": true
                    }
                ],
                "@typescript-eslint/no-inferrable-types": [
                    "off",
                    {
                        "ignoreParameters": true
                    }
                ],
                "@stylistic/quotes": [
                    "error",
                    "double",
                    {
                        "allowTemplateLiterals": true
                    }
                ],
                "@stylistic/semi": [
                    "error"
                ],
                "@typescript-eslint/no-unused-expressions": [
                    "error"
                ],
                "@typescript-eslint/explicit-function-return-type": [
                    "error",
                    {
                        "allowExpressions": true,
                    }
                ],
                "arrow-body-style": [
                    "error",
                    "as-needed"
                ],
                "brace-style": [
                    "error",
                    "1tbs"
                ],
                "comma-dangle": [
                    "error",
                    {
                        "arrays": "always-multiline",
                        "objects": "always-multiline",
                        "imports": "always-multiline",
                        "exports": "always-multiline",
                        "functions": "always-multiline"
                    }
                ],
                "complexity": [
                    "error",
                    {
                        "max": 200
                    }
                ],
                "import/no-deprecated": "off",
                "jsdoc/no-types": "off",
                "max-len": "off",
                "no-console": [
                    "error",
                    {
                        "allow": [
                            "null"
                        ]
                    }
                ],
                "no-redeclare": "off",
                "no-undef-init": "off",
                "no-underscore-dangle": "off",
                "no-mixed-spaces-and-tabs": [
                    "error"
                ],
                "no-restricted-imports": [
                    "error",
                    {
                        "paths": [],
                        "patterns": [
                            "rxjs/Rx",
                            "@app/generated-api",
                            "**/generated-api"
                        ]
                    }
                ],
                "object-shorthand": "off"
            }
        },
        {
            files: [
                "*.html"
            ],
            extends: [
                "plugin:@angular-eslint/template/recommended"
            ],
            rules: {}
        }
    ]
}
