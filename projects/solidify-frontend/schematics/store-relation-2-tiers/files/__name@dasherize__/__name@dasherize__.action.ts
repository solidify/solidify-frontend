import {LocalStateEnum} from "@app/shared/enums/local-state.enum";
import {Relation2TiersAction, Relation2TiersNameSpace, TypeDefaultAction} from "solidify-frontend";

const state = LocalStateEnum.<%= camelize(name) %>;

export namespace <%= classify(name) %>Action {
    @TypeDefaultAction(state)
    export class GetAll extends Relation2TiersAction.GetAll {
    }

    @TypeDefaultAction(state)
    export class GetAllSuccess extends Relation2TiersAction.GetAllSuccess<<%= classify(name) %>> {
    }

    @TypeDefaultAction(state)
    export class GetAllFail extends Relation2TiersAction.GetAllFail {
    }

    @TypeDefaultAction(state)
    export class Update extends Relation2TiersAction.Update {
    }

    @TypeDefaultAction(state)
    export class UpdateSuccess extends Relation2TiersAction.UpdateSuccess {
    }

    @TypeDefaultAction(state)
    export class UpdateFail extends Relation2TiersAction.UpdateFail {
    }

    @TypeDefaultAction(state)
    export class Create extends Relation2TiersAction.Create {
    }

    @TypeDefaultAction(state)
    export class CreateSuccess extends Relation2TiersAction.CreateSuccess {
    }

    @TypeDefaultAction(state)
    export class CreateFail extends Relation2TiersAction.CreateFail {
    }

    @TypeDefaultAction(state)
    export class Delete extends Relation2TiersAction.Delete {
    }

    @TypeDefaultAction(state)
    export class DeleteSuccess extends Relation2TiersAction.DeleteSuccess {
    }

    @TypeDefaultAction(state)
    export class DeleteFail extends Relation2TiersAction.DeleteFail {
    }
}


export const <%= camelize(name) %>ActionNameSpace: Relation2TiersNameSpace = <%= classify(name) %>Action;
