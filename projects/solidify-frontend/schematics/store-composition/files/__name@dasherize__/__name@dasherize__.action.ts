import {LocalStateEnum} from "@app/shared/enums/local-state.enum";
import {
  CompositionAction,
  CompositionNameSpace,
  TypeDefaultAction,
} from "solidify-frontend";

const state = LocalStateEnum. < %= camelize(name) % >;

export namespace
< %= classify(name) % > Action;
{
  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends CompositionAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends CompositionAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends CompositionAction.GetAllSuccess

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class GetAllFail extends CompositionAction.GetAllFail {
  }

  @TypeDefaultAction(state)
  export class Update extends CompositionAction.Update

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class UpdateSuccess extends CompositionAction.UpdateSuccess

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class UpdateFail extends CompositionAction.UpdateFail

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class Create extends CompositionAction.Create

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class CreateSuccess extends CompositionAction.CreateSuccess

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class CreateFail extends CompositionAction.CreateFail

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class Delete extends CompositionAction.Delete

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class DeleteSuccess extends CompositionAction.DeleteSuccess

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class DeleteFail extends CompositionAction.DeleteFail

<<
  %= classify(name) % >> {};
}

export const <;
%= camelize(name) % > ActionNameSpace;
:
CompositionNameSpace = < %= classify(name) % > Action;
