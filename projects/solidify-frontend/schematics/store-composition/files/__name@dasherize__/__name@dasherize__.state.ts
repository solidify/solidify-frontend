import {Injectable} from "@angular/core";
import {

< %= camelize(name) % > ActionNameSpace;
}
from;
"./<%= dasherize(name) %>.action";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {LocalStateEnum} from "@app/shared/enums/local-state.enum";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {
  ApiService,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  NotificationService,
} from "solidify-frontend";

export interface
< %= classify(name) % > StateModel;
extends
CompositionStateModel << %= classify(name) % >> {};

@Injectable()
@State<<
%= classify(name) % > StateModel > ({
  name: LocalStateEnum. < %= camelize(name) % >,
  defaults: {
    ...defaultCompositionStateInitValue(),
  },
});
export class
< %= classify(name) % > State;
extends
CompositionState << %= classify(name) % >> {
  constructor(protected apiService: ApiService,
              protected store: Store,
              protected notificationService: NotificationService,
              protected actions$: Actions) {
    super(apiService, store, notificationService, actions$, {
      nameSpace: < %= camelize(name) % > ActionNameSpace,
      urlResource: ModuleResourceApiEnum.,
      resourceName: ApiResourceNameEnum.,
    });
  },
};
