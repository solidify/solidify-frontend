import {Component, ChangeDetectionStrategy} from "@angular/core";
import {Store} from "@ngxs/store";
import {CoreAbstractComponent} from "solidify-frontend";

@Component({
    selector: "<%= prefix %>-<%= dasherize(name) %>-container",
    templateUrl: "./<%= dasherize(name) %>.container.html",
    styleUrls: ["./<%= dasherize(name) %>.container.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class <%= classify(name) %>Container extends CoreAbstractComponent {

    constructor(private store: Store) {
        super();
    }
}
