export interface Schema {
    name: string;
    project?: string;
    prefix?: string
    path?: string
}
