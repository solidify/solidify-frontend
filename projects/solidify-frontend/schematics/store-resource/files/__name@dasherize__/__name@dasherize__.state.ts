import {Injectable} from "@angular/core";
import {LocalStateEnum} from "@app/shared/enums/local-state.enum";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {
  ApiService,
  defaultResourceStateInitValue,
  NotificationService,
  ResourceState,
  ResourceStateModel,
} from "solidify-frontend";
import {

< %= camelize(name) % > ActionNameSpace;
}
from;
"./<%= dasherize(name) %>.action";

export interface
< %= classify(name) % > StateModel;
extends
ResourceStateModel << %= classify(name) % >> {};

@Injectable()
@State<<
%= classify(name) % > StateModel > ({
  name: LocalStateEnum. < %= camelize(name) % >,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
});
export class
< %= classify(name) % > State;
extends
ResourceState << %= classify(name) % >> {

  constructor(protected apiService: ApiService,
              protected store: Store,
              protected notificationService: NotificationService,
              protected actions$: Actions) {
    super(apiService, store, notificationService, actions$, {
      nameSpace: < %= camelize(name) % > ActionNameSpace,
      urlResource: ModuleResourceApiEnum.,
      routeRedirectUrlAfterSuccessAction: RoutesEnum.,
    });
  },
};
