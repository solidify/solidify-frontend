import {LocalStateEnum} from "path-to/local-state.enum";
import {
  ResourceAction,
  ResourceNameSpace,
  TypeDefaultAction,
} from "solidify-frontend";

const state = LocalStateEnum. < %= camelize(name) % >;

export namespace
< %= classify(name) % > Action;
{
  @TypeDefaultAction(state)
  export class LoadResource extends ResourceAction.LoadResource {
  }

  @TypeDefaultAction(state)
  export class LoadResourceSuccess extends ResourceAction.LoadResourceSuccess {
  }

  @TypeDefaultAction(state)
  export class LoadResourceFail extends ResourceAction.LoadResourceFail {
  }

  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends ResourceAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends ResourceAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends ResourceAction.GetAllSuccess

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class GetAllFail extends ResourceAction.GetAllFail

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class GetById extends ResourceAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends ResourceAction.GetByIdSuccess

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class GetByIdFail extends ResourceAction.GetByIdFail

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class Create extends ResourceAction.Create

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class CreateSuccess extends ResourceAction.CreateSuccess

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class CreateFail extends ResourceAction.CreateFail

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class Update extends ResourceAction.Update

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class UpdateSuccess extends ResourceAction.UpdateSuccess

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class UpdateFail extends ResourceAction.UpdateFail

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class Delete extends ResourceAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends ResourceAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends ResourceAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class DeleteList extends ResourceAction.DeleteList {
  }

  @TypeDefaultAction(state)
  export class DeleteListSuccess extends ResourceAction.DeleteListSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteListFail extends ResourceAction.DeleteListFail {
  }

  @TypeDefaultAction(state)
  export class AddInList extends ResourceAction.AddInList

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class AddInListById extends ResourceAction.AddInListById {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdSuccess extends ResourceAction.AddInListByIdSuccess

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class AddInListByIdFail extends ResourceAction.AddInListByIdFail

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class RemoveInListById extends ResourceAction.RemoveInListById {
  }

  @TypeDefaultAction(state)
  export class RemoveInListByListId extends ResourceAction.RemoveInListByListId {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkList extends ResourceAction.LoadNextChunkList {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListSuccess extends ResourceAction.LoadNextChunkListSuccess

<<
  %= classify(name) % >> {};

  @TypeDefaultAction(state)
  export class LoadNextChunkListFail extends ResourceAction.LoadNextChunkListFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends ResourceAction.Clean {
  }
}

export const <;
%= camelize(name) % > ActionNameSpace;
:
ResourceNameSpace = < %= classify(name) % > Action;
