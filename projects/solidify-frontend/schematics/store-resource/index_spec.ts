// import {
//   SchematicTestRunner,
//   UnitTestTree,
// } from "@angular-devkit/schematics/testing";
// import {
//   Schema as ApplicationOptions,
//   Style,
// } from "@schematics/angular/application/schema";
// import {Schema as WorkspaceOptions} from "@schematics/angular/workspace/schema";
// import * as assert from "assert";
// import * as path from "path";
// import {Schema as ModelOptions} from "./schema";
//
// const workspaceOptions: WorkspaceOptions = {
//   name: "workspace",
//   newProjectRoot: "projects",
//   version: "6.0.0",
// };
//
// const appOptions: ApplicationOptions = {
//   name: "bar",
//   inlineStyle: false,
//   inlineTemplate: false,
//   routing: false,
//   style: Style.Scss,
//   skipTests: false,
//   skipPackageJson: false,
// };
//
// const defaultOptions: ModelOptions = {
//   name: "foo",
//   // spec: true,
//   // flat: false,
//   project: "bar",
// };
//
// const collectionPath = path.join(__dirname, "../collection.json");
// const runner = new SchematicTestRunner("schematics", collectionPath);
//
// let appTree: UnitTestTree;
//
// describe("Schematic: Store Resource", () => {
//   const schematicRunner = new SchematicTestRunner(
//     "@schematics/solidify",
//     require.resolve("../collection.json"),
//   );
//
//   beforeEach(async () => {
//     appTree = await runner.runExternalSchematicAsync(
//       "@schematics/angular",
//       "workspace",
//       workspaceOptions,
//     ).toPromise();
//     appTree = await runner.runExternalSchematicAsync(
//       "@schematics/angular",
//       "application",
//       appOptions,
//       appTree,
//     ).toPromise();
//   });
//
//   it("should create a store derived of resource", async () => {
//     const options = {...defaultOptions, name: "test"};
//
//     const tree = await runner.runSchematicAsync("store-resource", options, appTree).toPromise();
//     assert(tree.files.includes("/projects/bar/src/app/test/test.action.ts"));
//     assert(tree.files.includes("/projects/bar/src/app/test/test.state.ts"));
//   });
//
//   it("should handle upper case paths", async () => {
//     const pathOption = "projects/bar/src/app/SOME/UPPER/DIR";
//     const options = {...defaultOptions, path: pathOption};
//
//     const tree = await schematicRunner.runSchematicAsync("store-resource", options, appTree)
//       .toPromise();
//     let files = tree.files;
//     let root = `/${pathOption}/foo/foo`;
//     expect(files).toEqual(jasmine.arrayContaining([
//       `${root}.state.ts`,
//       `${root}.action.ts`,
//     ]));
//
//     const options2 = {...options, name: "BAR"};
//     const tree2 = await schematicRunner.runSchematicAsync("store-resource", options2, tree)
//       .toPromise();
//     files = tree2.files;
//     root = `/${pathOption}/bar/bar`;
//     expect(files).toEqual(jasmine.arrayContaining([
//       `${root}.state.ts`,
//       `${root}.action.ts`,
//     ]));
//   });
// });
