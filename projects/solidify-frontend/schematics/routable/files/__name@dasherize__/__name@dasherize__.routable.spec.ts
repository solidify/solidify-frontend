import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { <%= classify(name) %>Routable } from "./<%= dasherize(name) %>.routable";

describe("<%= classify(name) %>Routable", () => {
    let component: <%= classify(name) %>Routable;
    let fixture: ComponentFixture<<%= classify(name) %>Routable>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ <%= classify(name) %>Routable ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(<%= classify(name) %>Routable);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
