import {Component, ChangeDetectionStrategy} from "@angular/core";
import {Store} from "@ngxs/store";
import {CoreAbstractComponent} from "solidify-frontend";

@Component({
    selector: "<%= prefix %>-<%= dasherize(name) %>-routable",
    templateUrl: "./<%= dasherize(name) %>.routable.html",
    styleUrls: ["./<%= dasherize(name) %>.routable.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class <%= classify(name) %>Routable extends CoreAbstractComponent {
    // @Select((state: LocalStateModel) => state[LocalStateEnum.my_state][LocalStateEnum.my_sub_state].list) listObs: Observable<any[]>;
    
    constructor(private store: Store) {
        super();
    }
}
