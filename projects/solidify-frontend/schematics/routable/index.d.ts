import { Rule } from '@angular-devkit/schematics';
import { Schema } from "./schema";
export declare function routableSchematics(_options: Schema): Rule;
