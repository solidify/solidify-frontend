"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@angular-devkit/schematics/testing/index");
const schema_1 = require("@schematics/angular/application/schema");
const path = require("path");
const assert = require("assert");
const workspaceOptions = {
    name: "workspace",
    newProjectRoot: "projects",
    version: "6.0.0"
};
const appOptions = {
    name: "bar",
    inlineStyle: false,
    inlineTemplate: false,
    routing: false,
    style: schema_1.Style.Scss,
    skipTests: false,
    skipPackageJson: false
};
const defaultOptions = {
    name: "foo",
    // spec: true,
    // flat: false,
    project: "bar"
};
const collectionPath = path.join(__dirname, "../collection.json");
const runner = new testing_1.SchematicTestRunner("schematics", collectionPath);
let appTree;
describe("Schematic: Routable", () => {
    const schematicRunner = new testing_1.SchematicTestRunner("@schematics/solidify", require.resolve("../collection.json"));
    beforeEach(() => {
        appTree = runner.runExternalSchematic("@schematics/angular", "workspace", workspaceOptions);
        appTree = runner.runExternalSchematic("@schematics/angular", "application", appOptions, appTree);
    });
    it("should create a routable component", () => {
        const options = Object.assign({}, defaultOptions, { name: "test" });
        const tree = runner.runSchematic("routable", options, appTree);
        assert(tree.files.includes("/projects/bar/src/app/test/test.routable.ts"));
        assert(tree.files.includes("/projects/bar/src/app/test/test.routable.html"));
        assert(tree.files.includes("/projects/bar/src/app/test/test.routable.scss"));
        assert(tree.files.includes("/projects/bar/src/app/test/test.routable.spec.ts"));
    });
    it("should handle upper case paths", () => __awaiter(this, void 0, void 0, function* () {
        const pathOption = "projects/bar/src/app/SOME/UPPER/DIR";
        const options = Object.assign({}, defaultOptions, { path: pathOption });
        const tree = yield schematicRunner.runSchematicAsync("routable", options, appTree)
            .toPromise();
        let files = tree.files;
        let root = `/${pathOption}/foo/foo.routable`;
        expect(files).toEqual(jasmine.arrayContaining([
            `${root}.scss`,
            `${root}.html`,
            `${root}.spec.ts`,
            `${root}.ts`,
        ]));
        const options2 = Object.assign({}, options, { name: "BAR" });
        const tree2 = yield schematicRunner.runSchematicAsync("routable", options2, tree)
            .toPromise();
        files = tree2.files;
        root = `/${pathOption}/bar/bar.routable`;
        expect(files).toEqual(jasmine.arrayContaining([
            `${root}.scss`,
            `${root}.html`,
            `${root}.spec.ts`,
            `${root}.ts`,
        ]));
    }));
});
//# sourceMappingURL=index_spec.js.map
