"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schematics_1 = require("@angular-devkit/schematics/src/index");
const core_1 = require("@angular-devkit/core/src/index");
const project_1 = require("@schematics/angular/utility/project");
const parse_name_1 = require("@schematics/angular/utility/parse-name");
// You don't have to export the function as default. You can also have more than one rule factory
// per file.
function routableSchematics(_options) {
    return (tree, _context) => {
        const workspaceConfigBuffer = tree.read("angular.json");
        if (!workspaceConfigBuffer) {
            throw new schematics_1.SchematicsException("Not an Angular CLI workspace");
        }
        const workspaceConfig = JSON.parse(workspaceConfigBuffer.toString());
        const projectName = _options.project || workspaceConfig.defaultProject;
        const project = workspaceConfig.projects[projectName];
        _options.prefix = project.prefix;
        if (_options.path === undefined && project) {
            _options.path = project_1.buildDefaultPath(project);
        }
        const parsedPath = parse_name_1.parseName(_options.path, _options.name);
        const { name, path } = parsedPath;
        const sourceTemplates = schematics_1.url('./files');
        const sourceParametrizedTemplates = schematics_1.apply(sourceTemplates, [
            schematics_1.template(Object.assign({}, _options, core_1.strings, { name })),
            schematics_1.move(path)
        ]);
        return schematics_1.mergeWith(sourceParametrizedTemplates)(tree, _context);
        // tree.create('test.js', `console.log('${_options.name}!');`);
        // return tree;
    };
}
exports.routableSchematics = routableSchematics;
//# sourceMappingURL=index.js.map
