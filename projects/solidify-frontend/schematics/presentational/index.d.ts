import { Rule } from "@angular-devkit/schematics";
import { Schema } from "./schema";
export declare function presentationalSchematics(_options: Schema): Rule;
