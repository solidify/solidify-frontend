import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { <%= classify(name) %>Presentational } from "./<%= dasherize(name) %>.presentational";

describe("<%= classify(name) %>Presentational", () => {
    let component: <%= classify(name) %>Presentational;
    let fixture: ComponentFixture<<%= classify(name) %>Presentational>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ <%= classify(name) %>Presentational ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(<%= classify(name) %>Presentational);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
