import {Component, ChangeDetectionStrategy} from "@angular/core";
import {CoreAbstractComponent} from "solidify-frontend";

@Component({
    selector: "<%= prefix %>-<%= dasherize(name) %>-presentational",
    templateUrl: "./<%= dasherize(name) %>.presentational.html",
    styleUrls: ["./<%= dasherize(name) %>.presentational.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class <%= classify(name) %>Presentational extends CoreAbstractComponent {
}
