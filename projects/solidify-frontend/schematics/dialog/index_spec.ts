// import {SchematicTestRunner, UnitTestTree} from "@angular-devkit/schematics/testing";
// import {Schema as ApplicationOptions, Style} from "@schematics/angular/application/schema";
// import {Schema as WorkspaceOptions} from "@schematics/angular/workspace/schema";
// import {Schema as ModelOptions} from "./schema";
// import * as path from "path";
// import * as assert from "assert";
//
// const workspaceOptions: WorkspaceOptions = {
//     name: "workspace",
//     newProjectRoot: "projects",
//     version: "6.0.0"
// };
//
// const appOptions: ApplicationOptions = {
//     name: "bar",
//     inlineStyle: false,
//     inlineTemplate: false,
//     routing: false,
//     style: Style.Scss,
//     skipTests: false,
//     skipPackageJson: false
// };
//
// const defaultOptions: ModelOptions = {
//     name: "foo",
//     // spec: true,
//     // flat: false,
//     project: "bar"
// };
//
// const collectionPath = path.join(__dirname, "../collection.json");
// const runner = new SchematicTestRunner("schematics", collectionPath);
//
// let appTree: UnitTestTree;
//
// describe("Schematic: Dialog", () => {
//     const schematicRunner = new SchematicTestRunner(
//         "@schematics/solidify",
//         require.resolve("../collection.json"),
//     );
//
//   beforeEach(async () => {
//     appTree = await runner.runExternalSchematicAsync(
//       "@schematics/angular",
//       "workspace",
//       workspaceOptions
//     ).toPromise();
//     appTree = await runner.runExternalSchematicAsync(
//       "@schematics/angular",
//       "application",
//       appOptions,
//       appTree
//     ).toPromise();
//   });
//
//     it("should create a dialog component", async () => {
//         const options = {...defaultOptions, name: "test"};
//
//         const tree = await runner.runSchematicAsync("dialog", options, appTree).toPromise();
//         assert(tree.files.includes("/projects/bar/src/app/test/test.dialog.ts"));
//         assert(tree.files.includes("/projects/bar/src/app/test/test.dialog.html"));
//         assert(tree.files.includes("/projects/bar/src/app/test/test.dialog.scss"));
//         assert(tree.files.includes("/projects/bar/src/app/test/test.dialog.spec.ts"));
//     });
//
//     it("should handle upper case paths", async () => {
//         const pathOption = "projects/bar/src/app/SOME/UPPER/DIR";
//         const options = {...defaultOptions, path: pathOption};
//
//         const tree = await schematicRunner.runSchematicAsync("dialog", options, appTree)
//             .toPromise();
//         let files = tree.files;
//         let root = `/${pathOption}/foo/foo.dialog`;
//         expect(files).toEqual(jasmine.arrayContaining([
//             `${root}.scss`,
//             `${root}.html`,
//             `${root}.spec.ts`,
//             `${root}.ts`,
//         ]));
//
//         const options2 = {...options, name: "BAR"};
//         const tree2 = await schematicRunner.runSchematicAsync("dialog", options2, tree)
//             .toPromise();
//         files = tree2.files;
//         root = `/${pathOption}/bar/bar.dialog`;
//         expect(files).toEqual(jasmine.arrayContaining([
//             `${root}.scss`,
//             `${root}.html`,
//             `${root}.spec.ts`,
//             `${root}.ts`,
//         ]));
//     });
// });
