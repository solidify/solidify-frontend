import {Component, ChangeDetectionStrategy, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngxs/store";
import {CoreAbstractComponent} from "solidify-frontend";

@Component({
    selector: "<%= prefix %>-<%= dasherize(name) %>-dialog",
    templateUrl: "./<%= dasherize(name) %>.dialog.html",
    styleUrls: ["./<%= dasherize(name) %>.dialog.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class <%= classify(name) %>Dialog extends CoreAbstractComponent {

    constructor(private store: Store,
                private dialogRef: MatDialogRef<<%= classify(name) %>Dialog>,
                @Inject(MAT_DIALOG_DATA) public data: <%= classify(name) %>DialogModel) {
        super();
    }
}

export interface <%= classify(name) %>DialogModel {
    test: string;
}
