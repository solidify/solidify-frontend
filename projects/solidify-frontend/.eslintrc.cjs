module.exports = {
    extends: "../../.eslintrc.cjs",
    ignorePatterns: [
        "!**/*",
        "node_modules/**/*",
        "schematics/**/*"
    ],
    overrides: [
        {
            files: [
                "*.ts"
            ],
            parserOptions: {
                project: [
                    "projects/solidify-frontend/tsconfig.lib.json",
                    "projects/solidify-frontend/tsconfig.spec.json"
                ],
                createDefaultProgram: true
            },
            rules: {
                "@angular-eslint/component-selector": [
                    "error",
                    {
                        "type": "element",
                        "prefix": "solidify",
                        "style": "kebab-case"
                    }
                ],
                "@angular-eslint/directive-selector": [
                    "error",
                    {
                        "type": "attribute",
                        "prefix": "solidify",
                        "style": "camelCase"
                    }
                ],
            }
        },
        {
            files: [
                "*.html"
            ],
            rules: {}
        }
    ]
}
