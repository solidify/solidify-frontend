{
  "solidify": {
    "appStatus": {
      "updateVersionDialog": {
        "body": "Do you want to update the application to the latest version. The current page will be refreshed.",
        "title": "A new version of the application is available",
        "updateAfter": "Later",
        "updateNow": "Yes"
      }
    },
    "application": {
      "aboutInjectedName": "About {{name}}",
      "accessToken": "Access token",
      "alreadyUsed": "This value is already used",
      "appBackToOnline": "You are back online",
      "appOffline": "Your are currently offline",
      "ark": {
        "button": {
          "copyArk": "Copy ARK",
          "copyUrlArk": "Copy url with ARK",
          "navigateToTheArk": "Navigate to the ARK"
        },
        "notification": {
          "arkCopyToClipboard": "ARK copied to clipboard",
          "urlArkCopyToClipboard": "Url with ARK copied to clipboard"
        }
      },
      "backToAdmin": "Back to administration",
      "backToList": "Back to list",
      "changeTime": "Change time",
      "changelog": "Changelog",
      "create": "Create",
      "created": "Created",
      "createdBy": "Created by",
      "delete": "Delete",
      "description": "Description",
      "detail": "Detail",
      "doi": {
        "button": {
          "copyLongDoi": "Copy long DOI",
          "copyShortDoi": "Copy short DOI",
          "copyUrlLongDoi": "Copy url with long DOI",
          "copyUrlShortLongDoi": "Copy url with short DOI",
          "navigateToTheDoi": "Navigate to the DOI"
        },
        "notification": {
          "longDoiCopyToClipboard": "Long DOI copied to clipboard",
          "shortDoiCopyToClipboard": "Short DOI copied to clipboard",
          "urlLongDoiCopyToClipboard": "Url with long DOI copied to clipboard",
          "urlShortDoiCopyToClipboard": "Url with short DOI copied to clipboard"
        }
      },
      "edit": "Edit",
      "exitEditMode": "Exit edit mode",
      "expiration": "Expiration",
      "fileUploadTile": {
        "button": {
          "cancel": "Cancel",
          "retry": "Retry"
        }
      },
      "folderTree": {
        "button": {
          "collapseAll": "Collapse all",
          "deleteFolder": "Delete folder",
          "downloadFolder": "Download folder",
          "expandAll": "Expand all",
          "uploadData": "Upload data"
        }
      },
      "navigateToChangelog": "See changelog",
      "navigateToReleaseNotes": "See release notes",
      "pageNotFound": {
        "button": {
          "backToHome": "Back to home page"
        },
        "title": "The requested resource does not exist or no longer exists..."
      },
      "save": "Save",
      "status": "Status",
      "statusHistoryDialogTitle": "{{name}}: Status history",
      "tablePersonResourceRole": {
        "add": "Add",
        "delete": "Delete"
      },
      "token": {
        "type": "Type",
        "typeMfa": "Multi-factor authentication",
        "typeStandard": "Standard"
      },
      "tokenDialog": {
        "copiedToClipboardFail": "Unable to copy the token to the clipboard",
        "copiedToClipboardSuccess": "Token copied to the clipboard",
        "copyToClipboard": "Copy to clipboard",
        "logInMfa": "Reconnect with Multi-factor authentication",
        "title": "Access token"
      },
      "updated": "Updated",
      "updatedBy": "Updated by",
      "user": "User",
      "youAreInEditMode": "You are in edit mode"
    },
    "codeEditor": {
      "copy": "Copy",
      "notification": {
        "copiedToClipboard": "Copied to clipboard"
      },
      "seeErrorDetailsBelow": "See error details below",
      "syntaxInvalid": "Incorrect syntax",
      "syntaxValid": "Correct syntax"
    },
    "core": {
      "about": {
        "commitDate": "Commit date",
        "module": "Module",
        "url": "Url",
        "version": "Version"
      },
      "additionalInformation": "Additional information",
      "breadcrumbHome": "Home",
      "cancel": "Cancel",
      "changelog": "Changelog",
      "characters": "Characters",
      "citationCopyToClipboard": "Copy to clipboard",
      "citationStyles": "Citation styles",
      "close": "Close",
      "cookieConsent": {
        "allowTheCookie": "Allow",
        "banner": {
          "allowCookies": "Allow cookies",
          "decline": "Decline",
          "learnMore": "Learn more",
          "newCookieIsAvailable": "A new cookie is available.",
          "newCookiesAreAvailable": "{{number}} new cookies are available.",
          "personalize": "Customize",
          "thisWebsiteUsesCookiesToEnsureYouGetTheBestExperienceOnOurWebsite": "This website uses cookies to ensure you get the best experience on our website."
        },
        "notification": {
          "cookieNameCopiedToClipboard": "Cookie name copied to the clipboard",
          "cookieXEnabled": "Cookie \"{{label}}\" enabled",
          "featureDisabledBecauseCookieDeclined": "This feature is not available to users who refuse the \"{{label}}\" cookie",
          "unableToCopyCookieNameToClipboard": "Unable to copy name to clipboard"
        },
        "sidebar": {
          "allowAllCookies": "Allow all cookies",
          "cookiesAvailable": {
            "description": "Allow to let you know when a new cookie is available",
            "title": "New cookie available"
          },
          "darkMode": {
            "description": "Saves the last brightness mode",
            "title": "Brightness mode"
          },
          "downloadToken": {
            "description": "Allows you to download files",
            "title": "Download token"
          },
          "googleAnalytics": {
            "description": "Allows to collect usage information to improve our service",
            "title": "Google Analytics"
          },
          "ignoreGlobalBanner": {
            "description": "Allows you to stop displaying the global banner if you have requested its closure",
            "title": "Display global banner"
          },
          "language": {
            "description": "Allows you to save your favorite language to load the site in the right language for your next visit",
            "title": "Favorite language"
          },
          "personalizeCookie": {
            "description": "Allows you to keep your cookie preferences and not be asked each time you visit if you consent to cookies",
            "title": "Cookie preferences"
          },
          "privacyPolicyAndTermsOfUseAccepted": {
            "description": "Allows you to retain your consent to the \"Data Protection Policy\" and \"Terms of Use\" and not be asked for consent each time you visit our website",
            "title": "Terms and policy acceptance"
          },
          "title": "Customize cookies",
          "tooltipClose": "Close cookie personalization panel"
        }
      },
      "copyToClipboard": "Copy to clipboard",
      "coreAboutDashboard": "Dashboard",
      "deleteDialogConfirmDeleteAction": "Confirm delete action",
      "doNotSeparate": "Do not separate",
      "fileDownload": {
        "downloadedSuccessfully": "File downloaded successfully",
        "fail": "Error while downloading",
        "forbidden": "You do not have permission to download this file",
        "start": "File download ongoing"
      },
      "firstLogin": {
        "close": "Close",
        "content": "This is a tutorial to help you better familiarize yourself with the interface. If you decide to skip this tutorial, you can always find it in the top-right menu.",
        "next": "Next",
        "title": "Hello and welcome to {{themeName}} !"
      },
      "includesSeparatorCharacters": "includes separator characters.",
      "languageSelectorSelectLanguage": "Select language",
      "maintenanceInProgress": "Maintenance in progress",
      "noCitationsAreAvailable": "No citations are available",
      "notification": {
        "copiedToClipboard": "Copied to clipboard",
        "http": {
          "badRequestToTranslate": "Bad request",
          "forbiddenToTranslate": "Access to this resource is forbidden",
          "internalErrorToTranslate": "Internal server error",
          "invalidTokenToTranslate": "Unable to refresh your token, you need to log back in",
          "notFoundToTranslate": "The requested resource was not found",
          "offlineToTranslate": "You are currently offline",
          "unauthorizedToTranslate": "You do not have the required authorization to access this resource"
        },
        "idCopyToClipboard": "Identifier copied to clipboard",
        "unsavedChanges": "You have unsaved changes! If you leave, your changes will be lost.",
        "upload": {
          "cancelled": "The file upload has been cancelled",
          "fail": "A problem was encountered during the upload",
          "fileSizeLimitXExceeded": "The file size limit set at {{size}} has been exceeded",
          "inProgress": "You have a file upload in progress! If you leave, the upload may be interrupted.",
          "success": "Upload successful"
        }
      },
      "orcidAuthenticated": "ORCID Authenticated",
      "orcidNotAuthenticated": "ORCID not authenticated",
      "orcidRedirect": {
        "subtitle": "You will now be automatically directed back to the submission system website",
        "title": "Thank you for connecting your ORCID iD"
      },
      "orcidSignIdButton": {
        "hint": "Learn more on ORCID identifier",
        "text": "Create or connect your ORCID iD"
      },
      "paginator": {
        "firstPage": "First page",
        "itemPerPage": "Items per page",
        "lastPage": "Last page",
        "nextPage": "Next page",
        "ofLabel": "of",
        "previousPage": "Previous page"
      },
      "privacyPolicyTermsOfUseApproval": {
        "dialog": {
          "close": "Close",
          "confirm": "Confirm",
          "iAcceptPrivacyPolicy": "I have read and I accept Data Protection Policy",
          "iAcceptTermsOfUse": "I have read and I accept Terms of Use",
          "message": "In order to proceed, you need to accept Terms of Use and Data Protection Policy",
          "title": "Terms of Use and Data Protection Policy"
        }
      },
      "refresh": "Refresh",
      "releaseNotes": "Release notes",
      "reviews": "Reviews",
      "separate": "Separate",
      "separatorCharacterDetected": "Separator character detected",
      "serverOffline": "Server currently offline",
      "status": "Status",
      "theFollowingPastedValue": "The following pasted value",
      "themeSelector": "Theme selector",
      "unableToLoadApp": "Unable to load application",
      "unaccessibleTab": "Unable to access to this tab",
      "validationError": {
        "invalidToTranslate": "This field is invalid",
        "requiredToTranslate": "This field is required"
      },
      "wouldYouLikeToSeparateTheEnteredValueUsingTheFollowingSeparator": "Would you like to separate the entered value using the following separator:",
      "yes": "Yes",
      "yesImSure": "Yes, I'm sure!"
    },
    "dataTable": {
      "all": "All",
      "allElementSearchedSelected": "All {{numberElementSelected}} searched elements are selected.",
      "cleanAllFilter": "Remove all filters",
      "clearSelection": "Clear selection",
      "copyIdToClipboard": "Copy ID to clipboard",
      "currentPageElementSelected": "All {{numberElementSelected}} elements are on this page.",
      "dialog": {
        "looseSelection": {
          "body": "By performing a new search you will lose your current selection of {{numberSelected}} items.",
          "title": "You're going to lose your selection"
        }
      },
      "moreOptions": "More options",
      "noData": "No documents",
      "notificationOnlyTheMaximalAmountAreSelected": "Only {{maximalNumber}} elements can be selected simultaneously",
      "selectAllElement": "Select all {{numberAllElement}} elements of the search.",
      "showHistoryStatus": "Show history status"
    },
    "errorHandling": {
      "notification": {
        "backendUnavailable": "The server is unreachable at the moment. Please try again later"
      }
    },
    "filePreview": {
      "dialog": {
        "title": "Preview"
      },
      "notification": {
        "previewDownloadFail": "Something went wrong during the visualization process, the file can't be open",
        "previewFileContentTypeNotSupported": "The content type of the file is not supported to preview",
        "previewFileNotSupported": "This file's format is not supported for preview",
        "previewFileStatusDoesNotAllowIt": "The file status does not allow preview it",
        "previewFileTooBig": "The file is too large for the preview"
      },
      "visualizationDocxPreviewMessageBanner": "The preview is potentially incomplete, please download to see the real rendering",
      "zoomIn": "Zoom in",
      "zoomOut": "Zoom out",
      "zoomVal": "Zoom:"
    },
    "globalBanner": {
      "admin": {
        "breadcrumb": {
          "create": "Create",
          "edit": "Edit",
          "root": "Global banners"
        },
        "dialog": {
          "delete": {
            "message": "Are you sure you want to delete the following global banner: '{{name}}' ?"
          }
        },
        "form": {
          "description": "Description",
          "enabled": "Enabled",
          "endDate": "End date",
          "endHour": "End hour",
          "isWithDescription": "Has a description",
          "name": "Name",
          "startDate": "Start date",
          "startHour": "Start hour",
          "title": "Title",
          "type": "Type"
        },
        "list": {
          "created": "Created",
          "enabled": "Enabled",
          "endDate": "End date",
          "name": "Name",
          "startDate": "Start date",
          "type": "Type",
          "updated": "Updated"
        },
        "notification": {
          "resourceCreateSuccess": "Global banner created",
          "resourceDeleteSuccess": "Global banner deleted",
          "resourceUpdateSuccess": "Global banner updated"
        },
        "tile": {
          "subtitle": "List of global banners for advertising",
          "title": "Global banners"
        }
      },
      "clickForMoreInfo": "Click for more information",
      "close": "Close",
      "enum": {
        "type": {
          "critical": "Critical",
          "info": "Information",
          "warning": "Warning"
        }
      }
    },
    "image": {
      "uploadAvatar": {
        "delete": "Delete",
        "dialog": {
          "cancel": "Cancel",
          "confirm": "Confirm",
          "title": "Upload avatar"
        },
        "notSupported": "Unable to load this avatar"
      },
      "uploadImage": {
        "delete": "Delete",
        "dialog": {
          "cancel": "Cancel",
          "confirm": "Confirm",
          "title": "Upload logo"
        },
        "notSupported": "Unable to load this image"
      }
    },
    "input": {
      "comma": "Comma",
      "dash": "Dash",
      "fileUploadInput": {
        "button": {
          "delete": "Delete",
          "download": "Download",
          "preview": "Preview"
        },
        "file": "File",
        "fileToUpload": "File to upload",
        "noFileAvailable": "No file available"
      },
      "keywordInputLabel": "Keywords",
      "lineBreak": "Line break",
      "multiSelect": {
        "defaultValue": "Default value",
        "noDataToSelect": "There is no data to submit",
        "notificationExtraInfoCopyToClipboard": "Value copied to clipboard",
        "placeholder": "Click here to add a value"
      },
      "search": "Search",
      "semicolon": "Semicolon",
      "underscore": "Underscore",
      "urlInputLabel": "URL"
    },
    "oaiPmh": {
      "breadcrumb": {
        "create": "Create",
        "edit": "Edit"
      },
      "oaiMetadataPrefix": {
        "breadcrumb": {
          "root": "OAI Metadata Prefixes"
        },
        "dialog": {
          "delete": {
            "message": "Are you sure you want to delete the following OAI metadata prefix: '{{name}}' ?"
          }
        },
        "form": {
          "description": "Description",
          "enabled": "Enabled",
          "metadataSchema": "XML schema",
          "metadataXmlTransformation": "XML transformation",
          "name": "Name",
          "prefix": "Prefix",
          "reference": "Reference",
          "schemaNamespace": "XML schema namespace",
          "tooltip": {
            "schemaNamespace": "XML schema namespace"
          },
          "xmlClass": "XML class"
        },
        "list": {
          "created": "Created",
          "description": "Description",
          "enabled": "Enabled",
          "name": "Name",
          "prefix": "Prefix",
          "reference": "Reference",
          "updated": "Updated"
        },
        "notification": {
          "resourceCreateSuccess": "OAI metadata prefix created",
          "resourceDeleteSuccess": "OAI metadata prefix deleted",
          "resourceUpdateSuccess": "OAI metadata prefix updated"
        },
        "tile": {
          "subtitle": "Describes available OAI metadata prefixes",
          "title": "OAI Metadata Prefixes"
        }
      },
      "oaiSet": {
        "breadcrumb": {
          "root": "OAI Sets"
        },
        "dialog": {
          "delete": {
            "message": "Are you sure you want to delete the following OAI set: '{{name}}' ?"
          }
        },
        "form": {
          "description": "Description",
          "enabled": "Enabled",
          "name": "Name",
          "query": "Query",
          "spec": "Spec",
          "tooltip": {
            "query": "Query to search for the OAI set. Only works within this repository.",
            "spec": "The OAI set's identifier. Must be lower case."
          }
        },
        "list": {
          "created": "Created",
          "description": "Description",
          "enabled": "Enabled",
          "name": "Name",
          "spec": "Spec",
          "updated": "Updated"
        },
        "notification": {
          "resourceCreateSuccess": "OAI set created",
          "resourceDeleteSuccess": "OAI set deleted",
          "resourceUpdateSuccess": "OAI set updated"
        },
        "tile": {
          "subtitle": "Describes OAI sets",
          "title": "OAI Sets"
        }
      }
    },
    "oauth2": {
      "mfaIsRequiredToPerformThisAction": "Multi-factor authentication is required to perform this action.",
      "reConnect": "Reconnect"
    },
    "richTextEditor": {
      "characters": "Characters",
      "displayWithFormatting": "Display with formatting",
      "displayWithoutFormatting": "Display without formatting",
      "maximum": "maximum",
      "toolbar": {
        "bold": "Bold",
        "cleanStyle": "Clean style",
        "image": "Image",
        "italic": "Italic",
        "link": "Link",
        "strike": "Strike",
        "sub": "Sub",
        "super": "Super",
        "underline": "Underline",
        "video": "Video"
      },
      "words": "words"
    },
    "search": {
      "clearFilters": "Clear filters",
      "excludeFacet": "Exclude",
      "includeFacet": "Include",
      "indexFieldAlias": {
        "breadcrumb": {
          "create": "Create",
          "edit": "Edit",
          "root": "Index field alias"
        },
        "dialog": {
          "delete": {
            "message": "Are you sure you want to delete the following index field alias: '{{name}}' ?"
          }
        },
        "form": {
          "alias": "Alias",
          "facet": "Facet",
          "facetDefaultVisibleValues": "Default facet value occurrences",
          "facetLimit": "Maximum number of facet values",
          "facetMinCount": "Minimum facet value occurrences",
          "facetOrder": "Position of the facet",
          "field": "Field",
          "indexName": "Index name",
          "system": "System"
        },
        "list": {
          "alias": "Alias",
          "created": "Created",
          "facet": "Facet",
          "field": "Field",
          "indexName": "Index name",
          "sort": "Sort",
          "system": "System",
          "updated": "Updated"
        },
        "notification": {
          "resourceCreate": {
            "success": "Index field alias created"
          },
          "resourceDelete": {
            "fail": "Unable to remove this alias from the index field",
            "success": "Index field alias deleted"
          },
          "resourceUpdate": {
            "success": "Index field alias updated"
          }
        },
        "tile": {
          "subtitle": "Describes index field alias",
          "title": "Index field aliases"
        }
      },
      "refineSearch": "Refine search",
      "searchQuestionMarkTooltip": "Click for more information"
    },
    "select": {
      "lastSelection": "Previously selected",
      "mostCommonValues": "Most common values",
      "objectNotFound": "Object not found",
      "search": "Search"
    },
    "tour": {
      "close": "Close"
    }
  }
}