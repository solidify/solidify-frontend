/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - global-errors-handler.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpErrorResponse} from "@angular/common/http";
import {
  ErrorHandler,
  Injectable,
  Injector,
} from "@angular/core";
import {ApmService} from "@elastic/apm-rum-angular";
import {
  isInstanceOf,
  isNotNullNorUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
  isUndefined,
} from "../core-resources/tools/is/is.tool";

import {DefaultSolidifyEnvironment} from "../core/environments/environment.solidify-defaults";
import {SolidifyError} from "../core/errors/solidify.error";
import {ErrorHelper} from "../core/helpers/error.helper";
import {ENVIRONMENT} from "../core/injection-tokens/environment.injection-token";
import {SolidifyHttpErrorResponseModel} from "../core/models/errors/solidify-http-error-response.model";
import {NotifierService} from "../core/models/services/notifier-service.model";
import {AbstractBaseService} from "../core/services/abstract-base.service";
import {ErrorService} from "../core/services/error.service";
import {LoggingService} from "../core/services/logging.service";
import {NotificationService} from "../core/services/notification.service";

@Injectable()
export class GlobalErrorsHandlerService extends AbstractBaseService implements ErrorHandler {
  private readonly _ERROR_EXPRESSION_CHANGED: string = "NG0100: ExpressionChangedAfterItHasBeenCheckedError";

  private readonly _LIST_ERROR_FRONTEND_TO_IGNORE: string[] = [
    this._ERROR_EXPRESSION_CHANGED,
  ];

  private _environment: DefaultSolidifyEnvironment | undefined;

  // Error handling is important and needs to be loaded first.
  // Because of this we should manually inject the services with Injector.
  constructor(protected readonly _injector: Injector) {
    super();
  }

  get environment(): DefaultSolidifyEnvironment {
    if (isUndefined(this._environment)) {
      this._environment = this._injector.get(ENVIRONMENT);
    }
    return this._environment;
  }

  private _loggingService: LoggingService | undefined;

  get loggingService(): LoggingService {
    if (isUndefined(this._loggingService)) {
      this._loggingService = this._injector.get(LoggingService);
    }
    return this._loggingService;
  }

  private _errorService: ErrorService | undefined;

  get errorService(): ErrorService {
    if (isUndefined(this._errorService)) {
      this._errorService = this._injector.get(ErrorService);
    }
    return this._errorService;
  }

  private _notifierService: NotifierService | undefined;

  get notifierService(): NotifierService {
    if (isUndefined(this._notifierService)) {
      this._notifierService = this._injector.get(NotificationService);
    }
    return this._notifierService;
  }

  private _apmService: ApmService | undefined;

  get apmService(): ApmService {
    if (isUndefined(this._apmService)) {
      this._apmService = this._injector.get(ApmService);
    }
    return this._apmService;
  }

  handleError(error: Error | SolidifyHttpErrorResponseModel | SolidifyError<Error> | SolidifyError<SolidifyHttpErrorResponseModel>): void {
    if (error === this.environment.errorToSkipInErrorHandler) {
      return;
    }
    const httpErrorKeyToSkipInErrorHandler = this.environment.httpErrorKeyToSkipInErrorHandler;
    const notifierService = this.notifierService;
    const nativeError = isInstanceOf(error, SolidifyError) ? error.nativeError : error as Error;

    let message;
    let subMessage;
    let stackTrace;

    const isBackendError = nativeError instanceof HttpErrorResponse;
    if (isBackendError) {
      const isErrorToDisplay = ErrorHelper.isErrorToDisplay(nativeError, httpErrorKeyToSkipInErrorHandler);
      if (isErrorToDisplay) {
        // Server Error
        message = this.errorService.getServerMessage(nativeError);
        subMessage = this.errorService.getServerSubMessage(nativeError);
        stackTrace = this.errorService.getServerStack(nativeError);
        if (isNotNullNorUndefined(notifierService)) {
          notifierService.showError(message, undefined, undefined, subMessage, undefined, nativeError);
        }
      }
    } else {
      // Client Error
      message = this.errorService.getClientMessage(nativeError);
      subMessage = this.errorService.getClientSubMessage(nativeError);
      stackTrace = this.errorService.getClientStack(nativeError);
      if (isNotNullNorUndefined(notifierService)
        && (isNullOrUndefinedOrWhiteString(nativeError.message) || this._LIST_ERROR_FRONTEND_TO_IGNORE.findIndex(m => nativeError.message.startsWith(m)) === -1)) {
        notifierService.showError(message, undefined, undefined, subMessage, undefined, nativeError);
      }
    }

    // Always log errors
    this.loggingService.logError(message, subMessage, error, stackTrace);
    if (isTrue(this.environment.elasticApmSendErrorsErrors) && isNotNullNorUndefined(this.apmService?.apm) && isTrue(this.apmService.apm.isActive())) {
      this.apmService.apm.captureError(nativeError);
    }
  }
}
