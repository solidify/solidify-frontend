/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-frontend-error-handling.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  Inject,
  NgModule,
} from "@angular/core";
import {
  ApmModule,
  ApmService,
} from "@elastic/apm-rum-angular";

import {DefaultSolidifyEnvironment} from "../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../core/injection-tokens/environment.injection-token";
import {SolidifyFrontendCoreModule} from "../core/solidify-frontend-core.module";
import {
  isNotNullNorUndefinedNorWhiteString,
  isTrue,
} from "../core-resources/tools/is/is.tool";

const modules = [
  SolidifyFrontendCoreModule,
  ApmModule,
];

@NgModule({
  declarations: [],
  imports: [
    ...modules,
  ],
  exports: [
    ...modules,
  ],
  providers: [
    ApmService,
  ],
})
export class SolidifyFrontendErrorHandlingModule {

  constructor(private readonly _apmService: ApmService,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    if (isTrue(this._environment.elasticApmEnabled) && isNotNullNorUndefinedNorWhiteString(this._environment.elasticApmUrl)) {
      _apmService.init({
        serviceName: this._environment.appTitle,
        serverUrl: this._environment.elasticApmUrl,
        environment: this._environment.production ? "Production" : (isTrue(this._environment.ribbonEnabled) && isNotNullNorUndefinedNorWhiteString(this._environment.ribbonText) ? this._environment.ribbonText : "Development"),
      });
    }
  }

}
