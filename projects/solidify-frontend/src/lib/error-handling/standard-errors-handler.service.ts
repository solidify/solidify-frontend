/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - standard-errors-handler.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpErrorResponse,
  HttpStatusCode,
} from "@angular/common/http";
import {
  Injectable,
  Injector,
} from "@angular/core";
import {Store} from "@ngxs/store";
import {
  isInstanceOf,
  isNullOrUndefined,
} from "../core-resources/tools/is/is.tool";
import {SsrUtil} from "../core-resources/utils/ssr.util";

import {DefaultSolidifyEnvironment} from "../core/environments/environment.solidify-defaults";
import {SolidifyError} from "../core/errors/solidify.error";
import {ErrorHelper} from "../core/helpers/error.helper";
import {ENVIRONMENT} from "../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../core/injection-tokens/label-to-translate.injection-token";
import {SolidifyHttpErrorResponseModel} from "../core/models/errors/solidify-http-error-response.model";
import {ErrorsSkipperService} from "../core/services/errors-skipper.service";
import {NotificationService} from "../core/services/notification.service";
import {SolidifyAppAction} from "../core/stores/abstract/app/app.action";
import {LabelTranslateInterface} from "../label-translate-interface.model";
import {OAuth2Service} from "../oauth2/oauth2.service";
import {GlobalErrorsHandlerService} from "./global-errors-handler.service";

@Injectable()
export class StandardErrorsHandlerService extends GlobalErrorsHandlerService {
  // Error handling is important and needs to be loaded first.
  // Because of this we should manually inject the services with Injector.

  constructor(private readonly oAuth2Service: OAuth2Service,
              protected readonly _injector: Injector) {
    super(_injector);
  }

  handleError(error: Error | SolidifyHttpErrorResponseModel | SolidifyError<Error> | SolidifyError<SolidifyHttpErrorResponseModel>): void {
    const environment: DefaultSolidifyEnvironment = this._injector.get(ENVIRONMENT);
    const labelTranslate: LabelTranslateInterface = this._injector.get(LABEL_TRANSLATE);
    const errorSkipper: ErrorsSkipperService = this._injector.get(ErrorsSkipperService);
    const notificationService: NotificationService = this._injector.get(NotificationService);
    const nativeError = isInstanceOf(error, SolidifyError) ? error.nativeError : error as Error;
    if (nativeError === environment.errorToSkipInErrorHandler) {
      return;
    }
    const httpErrorKeyToSkipInErrorHandler = environment.httpErrorKeyToSkipInErrorHandler;

    if (nativeError instanceof HttpErrorResponse) {
      if (errorSkipper.shouldBeSkipped(nativeError.status)) {
        return;
      }
      if (nativeError.status === 0) {
        notificationService.showError(labelTranslate.errorHandlingNotificationBackendUnavailable);
      }
      if (ErrorHelper.isErrorToDisplay(nativeError, httpErrorKeyToSkipInErrorHandler)) {
        // Server Error
        if (nativeError.status === HttpStatusCode.Unauthorized) {
          if (SsrUtil.isBrowser && (isNullOrUndefined(nativeError.error) || nativeError.error.error === ErrorHelper.INVALID_TOKEN_ERROR)) {
            const store = this._injector.get(Store);
            store.dispatch(new SolidifyAppAction.Logout());
          }
        }
      }
    }

    super.handleError(error);
  }
}
