/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - file-upload-input.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostBinding,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
  ValidatorFn,
} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
  tap,
} from "rxjs";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
} from "../../../core-resources/tools/is/is.tool";
import {ObjectUtil} from "../../../core-resources/utils/object.util";
import {StringUtil} from "../../../core-resources/utils/string.util";
import {AbstractInternalContainer} from "../../../core/components/containers/abstract-internal/abstract-internal.container";
import {IconNamePartialEnum} from "../../../core/enums/partial/icon-name-partial.enum";
import {FileStatusEnum} from "../../../core/enums/upload/file-status.enum";
import {SolidifyFile} from "../../../core/models/dto/solidify-file.model";
import {DownloadService} from "../../../core/services/download.service";
import {ResourceFileNameSpace} from "../../../core/stores/abstract/resource-file/resource-file-namespace.model";
import {ExtendEnum} from "../../../core-resources/types/extend-enum.type";
import {DialogUtil} from "../../../core/utils/dialog.util";
import {ObservableUtil} from "../../../core/utils/observable.util";
import {StoreUtil} from "../../../core/utils/stores/store.util";
import {SolidifyForbiddenValuesValidator} from "../../../core/validators/solidify-forbidden-values.validator";
import {FilePreviewDialog} from "../../../file-preview/components/dialogs/file-preview/file-preview.dialog";
import {FileVisualizerHelper} from "../../../file-preview/helpers/file-visualizer.helper";
import {SolidifyDataFileModel} from "../../../file-preview/models/solidify-data-file.model";
import {FileVisualizerService} from "../../../file-preview/services/file-visualizer.service";

@Component({
  selector: "solidify-file-upload-input-container",
  templateUrl: "./file-upload-input.container.html",
  styleUrls: ["./file-upload-input.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: FileUploadInputContainer,
    },
  ],
})
export class FileUploadInputContainer extends AbstractInternalContainer implements OnInit, ControlValueAccessor {
  @ViewChild("fileInput")
  fileInput: ElementRef;

  private _previewAvailable: boolean = false;

  waitStatusReady: boolean = false;

  @Input()
  noFileAvailableToTranslate: string = this.labelTranslateInterface.inputFileUploadInputNoFileAvailable;

  @Input()
  uploadButtonLabelToTranslate: string = this.labelTranslateInterface.inputFileUploadInputFileToUpload;

  @Input()
  uploadButtonIcon: ExtendEnum<IconNamePartialEnum> = IconNamePartialEnum.uploadFile;

  @Input()
  allowUpload: boolean = true;

  @Input()
  allowDelete: boolean = true;

  @Input()
  allowDownload: boolean = true;

  @Input()
  titleToTranslate: string | undefined = this.labelTranslateInterface.inputFileUploadInputFile;

  @Input()
  @HostBinding("class.readonly")
  readonly: boolean = false;

  @Input()
  mimeTypeKey: string = "mimeType";

  @Input()
  puidKey: string = "puid";

  @Input()
  statusKey: string = "status";

  @Input()
  blobProvidedMode: SolidifyFileUploadBlobProvidedMode;

  @Input()
  resourceId: string | undefined;

  @Input()
  resourceFileNameSpace: ResourceFileNameSpace;

  @Input()
  blob: Blob | undefined;

  @Input()
  externalPreview: boolean = false;

  @Input()
  externalDownload: boolean = false;

  @Input()
  formControl: FormControl<SolidifyFileUploadType>;

  @Input()
  downloadUrl: string;

  @Input()
  isRequired: boolean;

  get isRequiredInternal(): boolean {
    return !this.readonly && (this.formControl?.hasValidator(SolidifyFileUploadInputRequiredValidator) || this.isRequired);
  }

  typeFile: SolidifyTypeFile = undefined;

  @Input()
  backendFileAdapter: (file: SolidifyFile) => SolidifyDataFileModel = file => ({
    fileName: file.fileName,
    fileSize: file.fileSize,
    status: ObjectUtil.getNestedValue(file, this.statusKey),
    puid: ObjectUtil.getNestedValue(file, this.puidKey),
    mimetype: ObjectUtil.getNestedValue(file, this.mimeTypeKey),
  });

  private _backendFile: SolidifyDataFileModel | undefined;

  @Input()
  set backendFile(value: SolidifyFile) {
    if (isNullOrUndefined(value)) {
      return;
    }
    this.typeFile = SolidifyTypeFile.BACKEND_FILE;
    if (isNullOrUndefined(this.backendFile) && isNotNullNorUndefined(this.formControl)) {
      this.formControl.setValue(SolidifyFileUploadStatus.UNCHANGED, {
        onlySelf: true,
        emitEvent: false,
        emitModelToViewChange: false,
        emitViewToModelChange: false,
      });
    }
    this._backendFile = this.backendFileAdapter(value);
    this.internalFile = {
      name: this._backendFile.fileName,
      size: this._backendFile.fileSize,
      type: this._backendFile.mimetype,
    } as File;
  }

  @Input()
  set browserFile(value: File) {
    this.typeFile = SolidifyTypeFile.INPUT_FILE;
    this.internalFile = value;
    this._updateNewFile(this.internalFile);
  }

  set internalFile(value: File) {
    this._internalFileBS.next(value);
    this._computePreviewAvailable();
  }

  get internalFile(): File {
    return this._internalFileBS.value;
  }

  private readonly _internalFileBS: BehaviorSubject<File | undefined> = new BehaviorSubject<File | undefined>(undefined);

  private readonly _fileBS: BehaviorSubject<SolidifyFileUploadType> = new BehaviorSubject<SolidifyFileUploadType>(undefined);
  @Output("fileChange")
  readonly fileObs: Observable<SolidifyFileUploadType> = ObservableUtil.asObservable(this._fileBS);

  private readonly _downloadBS: BehaviorSubject<File | undefined> = new BehaviorSubject<File | undefined>(undefined);
  @Output("downloadChange")
  readonly downloadObs: Observable<File | undefined> = ObservableUtil.asObservable(this._downloadBS);

  private readonly _previewBS: BehaviorSubject<File | undefined> = new BehaviorSubject<File | undefined>(undefined);
  @Output("previewChange")
  readonly previewObs: Observable<File | undefined> = ObservableUtil.asObservable(this._previewBS);

  private readonly _deleteBS: BehaviorSubject<SolidifyFileUploadTypeUnchangedOrDeleted> = new BehaviorSubject<SolidifyFileUploadTypeUnchangedOrDeleted>(undefined);
  @Output("deleteChange")
  readonly deleteObs: Observable<SolidifyFileUploadTypeUnchangedOrDeleted> = ObservableUtil.asObservable(this._deleteBS);

  get displayUploadButton(): boolean {
    return !this.readonly && this.allowUpload;
  }

  get displayDeleteButton(): boolean {
    return !this.readonly && isNotNullNorUndefined(this.internalFile) && this.allowDelete;
  }

  get displayDownloadButton(): boolean {
    return isNotNullNorUndefined(this.internalFile) && this.allowDownload && !this.waitStatusReady;
  }

  get displayPreviewButton(): boolean {
    return isNotNullNorUndefined(this.internalFile) && this._previewAvailable && !this.waitStatusReady;
  }

  constructor(protected readonly _injector: Injector,
              private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _dialog: MatDialog,
              private readonly _downloadService: DownloadService,
              private readonly _fileVisualizerService: FileVisualizerService) {
    super(_injector);
  }

  writeValue(obj: any): void {
  }

  propagateChange: (__: any) => void = (__: any): void => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState?(isDisabled: boolean): void {
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (isNullOrUndefined(this.blobProvidedMode)) {
      throw new Error("'blobProvidedMode' input should be defined");
    }

    if (this.blobProvidedMode === SolidifyFileUploadBlobProvidedMode.INPUT) {
      if (isNullOrUndefined(this.blob)) {
        throw new Error("'blob' input should be defined");
      }
    } else if (this.blobProvidedMode === SolidifyFileUploadBlobProvidedMode.RETRIEVE_BY_STORE && isNotNullNorUndefined(this.backendFile)) {
      if (isNullOrUndefinedOrWhiteString(this.resourceId)) {
        throw new Error("'resourceId' input should be defined");
      }
      if (isNullOrUndefined(this.resourceFileNameSpace)) {
        throw new Error("'resourceFileNameSpace' input should be defined");
      }
    } else if (this.blobProvidedMode === SolidifyFileUploadBlobProvidedMode.RETRIEVE_BY_URL && isNotNullNorUndefined(this.backendFile)) {
      if (isNullOrUndefined(this.downloadUrl)) {
        throw new Error("'downloadUrl' input should be defined");
      }
    }

    this._mapFormControlFileOnNewFile();
  }

  private _mapFormControlFileOnNewFile(): void {
    if (isNotNullNorUndefined(this.formControl)) {
      this.subscribe(this.fileObs.pipe(
        tap(file => {
          this.formControl.setValue(file);
          this.formControl.markAsDirty();
        }),
      ));
    }
  }

  private _updateNewFile(file: File | undefined): void {
    const fileUploadType = this._computeFileUploadType(file);
    this._fileBS.next(fileUploadType);
    if (isNullOrUndefined(fileUploadType) || fileUploadType === SolidifyFileUploadStatus.DELETED) {
      this._deleteBS.next(fileUploadType);
    }
  }

  private _computeFileUploadType(file: File | undefined): SolidifyFileUploadType {
    let fileUploadType: SolidifyFileUploadType = file;
    if (isNullOrUndefined(file)) {
      fileUploadType = isNullOrUndefined(this._backendFile) ? undefined : SolidifyFileUploadStatus.DELETED;
    }
    return fileUploadType;
  }

  onFileChangeByInput(event: any): void {
    if (event.target.files.length > 0) {
      const files: FileList = event.target.files;
      this.onFileChange(files);
    }
    if (isNotNullNorUndefined(this.fileInput)) {
      this.fileInput.nativeElement.value = null;
    }
  }

  onFileChange(files: FileList | File[]): void {
    this.browserFile = files[0];
  }

  deleteFile(): void {
    this.browserFile = null;
  }

  private _openPreviewDialog(dataFile: SolidifyDataFileModel, blob: Blob): void {
    DialogUtil.open(this._dialog, FilePreviewDialog, {
        fileInput: {
          dataFile: dataFile,
          blob: blob,
        },
        fileDownloadUrl: this.downloadUrl,
      },
      {
        width: "max-content",
        maxWidth: "90vw",
        height: "min-content",
      });
  }

  previewFile(): void {
    this._previewBS.next(this.internalFile);

    if (isTrue(this.externalPreview)) {
      return;
    }

    if (this.typeFile === SolidifyTypeFile.BACKEND_FILE) {
      const solidifyFile: SolidifyDataFileModel = ObjectUtil.clone(this._backendFile);
      if (isNullOrUndefined(solidifyFile.status)) {
        solidifyFile.status = FileStatusEnum.READY;
      }
      if (this.blobProvidedMode === SolidifyFileUploadBlobProvidedMode.RETRIEVE_BY_STORE) {
        this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
          new this.resourceFileNameSpace.GetFile(this.resourceId, false),
          this.resourceFileNameSpace.GetFileSuccess,
          result => {
            this._openPreviewDialog(solidifyFile, result.blob);
          }),
        );
      } else if (this.blobProvidedMode === SolidifyFileUploadBlobProvidedMode.INPUT) {
        this._openPreviewDialog(solidifyFile, this.blob);
      } else if (this.blobProvidedMode === SolidifyFileUploadBlobProvidedMode.RETRIEVE_BY_URL) {
        this._openPreviewDialog(solidifyFile, undefined);
      }
    } else {
      const dataFile = {
        fileName: this.internalFile.name,
        fileSize: this.internalFile.size,
        mimeType: this.internalFile.type,
        status: FileStatusEnum.READY,
      } as SolidifyDataFileModel;
      this._openPreviewDialog(dataFile, this.internalFile);
    }
  }

  downloadFile(): void {
    this._downloadBS.next(this.internalFile);

    if (isTrue(this.externalDownload)) {
      return;
    }

    const fileName = StringUtil.replaceNonIso8859_1Chars(this.internalFile.name);

    let blob;
    if (this.typeFile === SolidifyTypeFile.BACKEND_FILE) {
      if (this.blobProvidedMode === SolidifyFileUploadBlobProvidedMode.RETRIEVE_BY_STORE) {
        this._store.dispatch(new this.resourceFileNameSpace.GetFile(this.resourceId, true));
        return;
      } else if (this.blobProvidedMode === SolidifyFileUploadBlobProvidedMode.INPUT) {
        blob = this.blob;
      } else if (this.blobProvidedMode === SolidifyFileUploadBlobProvidedMode.RETRIEVE_BY_URL) {
        this._downloadInMemory(this.downloadUrl, fileName, this.internalFile.type);
        return;
      }
    } else {
      blob = this.internalFile;
    }

    const url = URL.createObjectURL(blob);
    this._downloadInMemory(url, fileName, this.internalFile.type);
  }

  private _downloadInMemory(url: string, fileName: string, mimetype: string): void {
    this.subscribe(this._downloadService.downloadInMemory(url, fileName, true, mimetype));
  }

  private _computePreviewAvailable(): void {
    if (isNullOrUndefined(this.internalFile)) {
      this.waitStatusReady = false;
      this._previewAvailable = false;
      return;
    }
    let dataFile: SolidifyDataFileModel = null;

    if (this.typeFile === SolidifyTypeFile.BACKEND_FILE) {
      dataFile = this._backendFile;
    } else if (this.typeFile === SolidifyTypeFile.INPUT_FILE) {
      dataFile = {
        fileName: this.internalFile.name,
        fileSize: this.internalFile.size,
        mimetype: this.internalFile.type,
      };
    } else {
      throw new Error(`Unmanaged typeFile '${this.typeFile}' for preview`);
    }

    if (isNullOrUndefined(dataFile.status)) {
      dataFile.status = FileStatusEnum.READY;
    }

    this.waitStatusReady = !FileVisualizerHelper.canHandleByStatus({dataFile: dataFile});
    this._previewAvailable = FileVisualizerHelper.canHandle(this._fileVisualizerService.listFileVisualizer, {
      dataFile: dataFile,
      fileExtension: FileVisualizerHelper.getFileExtension(this.internalFile.name),
    });
  }
}

export type SolidifyTypeFile =
  "INPUT_FILE"
  | "BACKEND_FILE";

export const SolidifyTypeFile = {
  INPUT_FILE: "INPUT_FILE" as SolidifyTypeFile,
  BACKEND_FILE: "BACKEND_FILE" as SolidifyTypeFile,
};

export type SolidifyFileUploadBlobProvidedMode =
  "RETRIEVE_BY_STORE"
  | "INPUT"
  | "RETRIEVE_BY_URL"
  | "NEVER";

export const SolidifyFileUploadBlobProvidedMode = {
  RETRIEVE_BY_STORE: "RETRIEVE_BY_STORE" as SolidifyFileUploadBlobProvidedMode,
  INPUT: "INPUT" as SolidifyFileUploadBlobProvidedMode,
  RETRIEVE_BY_URL: "RETRIEVE_BY_URL" as SolidifyFileUploadBlobProvidedMode,
  NEVER: "NEVER" as SolidifyFileUploadBlobProvidedMode,
};

export type SolidifyFileUploadStatus =
  "DELETED" |
  "UNCHANGED";

export const SolidifyFileUploadStatus = {
  DELETED: "DELETED" as SolidifyFileUploadStatus,
  UNCHANGED: "UNCHANGED" as SolidifyFileUploadStatus,
};

export type SolidifyFileUploadType = File | SolidifyFileUploadTypeUnchangedOrDeleted;
export type SolidifyFileUploadTypeUnchangedOrDeleted = SolidifyFileUploadStatus | undefined;

export const SolidifyFileUploadInputRequiredValidator: ValidatorFn = SolidifyForbiddenValuesValidator(undefined, [null, undefined, SolidifyFileUploadStatus.DELETED], "Required");
