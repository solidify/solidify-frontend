/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - keyword-input.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {
  MatChipEditedEvent,
  MatChipGrid,
  MatChipInput,
  MatChipInputEvent,
} from "@angular/material/chips";
import {MatDialog} from "@angular/material/dialog";
import {
  FloatLabelType,
  MatFormFieldAppearance,
  SubscriptSizing,
} from "@angular/material/form-field";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {SOLIDIFY_CONSTANTS} from "../../../core-resources/constants";
import {KeyValue} from "../../../core-resources/models/key-value.model";
import {
  isArray,
  isEmptyString,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
} from "../../../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../../../core-resources/types/extend-enum.type";
import {ArrayUtil} from "../../../core-resources/utils/array.util";
import {SsrUtil} from "../../../core-resources/utils/ssr.util";
import {StringUtil} from "../../../core-resources/utils/string.util";
import {KeywordPasteDialog} from "../../../core/components/dialogs/keyword-paste/keyword-paste.dialog";
import {AbstractInternalPresentational} from "../../../core/components/presentationals/abstract-internal/abstract-internal.presentational";
import {IconNamePartialEnum} from "../../../core/enums/partial/icon-name-partial.enum";
import {FormValidationHelper} from "../../../core/helpers/form-validation.helper";
import {LABEL_TRANSLATE} from "../../../core/injection-tokens/label-to-translate.injection-token";
import {DialogUtil} from "../../../core/utils/dialog.util";
import {ObservableUtil} from "../../../core/utils/observable.util";
import {LabelTranslateInterface} from "../../../label-translate-interface.model";

@Component({
  selector: "solidify-keyword-input",
  templateUrl: "./keyword-input.presentational.html",
  styleUrls: ["./keyword-input.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: KeywordInputPresentational,
    },
  ],
})

export class KeywordInputPresentational extends AbstractInternalPresentational implements OnInit, ControlValueAccessor {
  static readonly KEY_ENTER: string = "Enter";

  @ViewChild(MatChipInput)
  inputChip: MatChipInput;

  @ViewChild("input")
  input: ElementRef;

  @Input()
  separatorKeys: string[] = [KeywordInputPresentational.KEY_ENTER, SOLIDIFY_CONSTANTS.SPACE, SOLIDIFY_CONSTANTS.COMMA, SOLIDIFY_CONSTANTS.SEMICOLON, SOLIDIFY_CONSTANTS.SEPARATOR];

  @Input()
  placeholderToTranslate: string = this.labelTranslateInterface.inputKeywordInputLabel;

  @Input()
  readonly: boolean;

  @Input()
  formControl: FormControl;

  @Input()
  appearance: MatFormFieldAppearance = this.appearanceInputMaterial;

  @Input()
  positionLabel: FloatLabelType = this.positionLabelInputMaterial;

  @Input()
  subscriptSizing: SubscriptSizing = this.subscriptSizingInputMaterial;

  @Input()
  hintToTranslate: string;

  @Input()
  capitalize: boolean = false;

  @Input()
  chipEditable: boolean = true;

  @Input()
  separatorToControlOnPaste: KeyValue[];

  @Input()
  extraInfoImage: ExtendEnum<IconNamePartialEnum> | undefined;

  @Input()
  extraInfoImageLabel: string | undefined;

  @Input()
  extraInfoImageLabelCallback: (value: string) => string = (value: string) => this.extraInfoImageLabel;

  clickExtraInfoImage(value: string, $event: Event): void {
    if (isNotNullNorUndefined($event)) {
      $event.stopPropagation();
    }
    this._extraInfoImageCallbackBS.next(value);
  }

  _spaceDelimiter: boolean = true;

  @Input()
  set spaceDelimiter(value: boolean) {
    this._spaceDelimiter = value;
    this._addOrRemoveSeparator(this._spaceDelimiter, SOLIDIFY_CONSTANTS.SPACE);
  }

  _enterDelimiter: boolean = true;

  @Input()
  set enterDelimiter(value: boolean) {
    this._enterDelimiter = value;
    this._addOrRemoveSeparator(this._enterDelimiter, KeywordInputPresentational.KEY_ENTER);
  }

  _commaDelimiter: boolean = true;

  @Input()
  set commaDelimiter(value: boolean) {
    this._commaDelimiter = value;
    this._addOrRemoveSeparator(this._commaDelimiter, SOLIDIFY_CONSTANTS.COMMA);
  }

  _semicolonDelimiter: boolean = true;

  @Input()
  set semicolonDelimiter(value: boolean) {
    this._semicolonDelimiter = value;
    this._addOrRemoveSeparator(this._semicolonDelimiter, SOLIDIFY_CONSTANTS.SEMICOLON);
  }

  _separatorDelimiter: boolean = true;

  @Input()
  set separatorDelimiter(value: boolean) {
    this._separatorDelimiter = value;
    this._addOrRemoveSeparator(this._separatorDelimiter, SOLIDIFY_CONSTANTS.SEPARATOR);
  }

  @Input()
  showEmptyRequiredFieldInError: boolean = this.displayEmptyRequiredFieldInError;

  @Input()
  prefixIcon: ExtendEnum<IconNamePartialEnum>;

  @Input()
  chipWidth: string = "200px";

  private readonly _valueBS: BehaviorSubject<string[] | undefined> = new BehaviorSubject<string[] | undefined>(undefined);
  @Output("valueChange")
  readonly valueObs: Observable<string[] | undefined> = ObservableUtil.asObservable(this._valueBS);

  protected readonly _extraInfoImageCallbackBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("extraInfoImageCallback")
  readonly extraInfoImageCallbackObs: Observable<string | undefined> = ObservableUtil.asObservable(this._extraInfoImageCallbackBS);

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  @ViewChild("chipGrid")
  chipGrid: MatChipGrid;

  previousValue: string[] = [];
  isDirtyField: boolean = false;

  constructor(private readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector,
              protected readonly _dialog: MatDialog,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface) {
    super(_injector);
    this.separatorToControlOnPaste = [
      {
        key: SOLIDIFY_CONSTANTS.COMMA,
        value: this.labelTranslate.inputComma,
      },
      {
        key: SOLIDIFY_CONSTANTS.SEMICOLON,
        value: this.labelTranslate.inputSemicolon,
      },
      {
        key: SOLIDIFY_CONSTANTS.UNDERSCORE,
        value: this.labelTranslate.inputUnderscore,
      },
      {
        key: SOLIDIFY_CONSTANTS.LINE_BREAK,
        value: this.labelTranslate.inputLineBreak,
      },
      {
        key: SOLIDIFY_CONSTANTS.DASH,
        value: this.labelTranslate.inputDash,
      },
      {
        key: "–",
        value: this.labelTranslate.inputDash,
      },
      {
        key: "—",
        value: this.labelTranslate.inputDash,
      },
      {
        key: "―",
        value: this.labelTranslate.inputDash,
      },
      {
        key: "‒",
        value: this.labelTranslate.inputDash,
      },
    ];
  }

  ngOnInit(): void {
    if (isNotNullNorUndefined(this.formControl)) {
      this.subscribe(this.formControl.statusChanges.pipe(
        distinctUntilChanged(),
        tap(status => {
          this.chipGrid.errorState = status === SOLIDIFY_CONSTANTS.FORM_STATUS_INVALID;
        }),
      ));

      this.previousValue = [...this.formControl.value ?? []];
      this.subscribe(this.formControl.valueChanges.pipe(
        distinctUntilChanged(),
        tap((value: string[]) => {
          // Allow to manage case when (blur with no change with click or keyboard navigation)
          const diff = ArrayUtil.diff(this.previousValue, value ?? []).diff;
          if (isNonEmptyArray(diff)) {
            this.propagateChange(value);
            this._valueBS.next(value);
            this.isDirtyField = true;
          } else if (!this.isDirtyField) {
            this.formControl.markAsPristine();
          }
          this.previousValue = [...value];
        }),
      ));
    }
  }

  propagateChange: (__: any) => void = (__: any) => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(value: string[]): void {
  }

  remove(value: string): void {
    const list: string[] = this.formControl.value;
    const indexOf = list.indexOf(value);
    if (indexOf === -1) {
      return;
    }
    list.splice(indexOf, 1);
    this.formControl.markAsDirty();
    this.formControl.setValue(list);
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = this._transformValue(event.value || StringUtil.stringEmpty);
    const list: string[] = this.formControl.value;

    if (!isEmptyString(value) && !list.includes(value)) {
      list.push(value);
    }
    this.formControl.setValue(list);
    this.formControl.markAsDirty();

    if (input) {
      input.value = StringUtil.stringEmpty;
    }
  }

  getValue(): string[] {
    const value = this.formControl.value;
    return (!isNullOrUndefined(value) && isArray(value)) ? value.filter(v => v !== StringUtil.stringEmpty) : [];
  }

  private _addOrRemoveSeparator(withTheSeparator: boolean, separator: string): void {
    const indexOf = this.separatorKeys.indexOf(separator);
    if (withTheSeparator && indexOf === -1) {
      this.separatorKeys.push(separator);
    } else if (!withTheSeparator && indexOf !== -1) {
      this.separatorKeys.splice(indexOf, 1);
    }
  }

  paste($event: ClipboardEvent): void {
    const pastedString = ($event.clipboardData || SsrUtil.window?.clipboardData).getData("text");
    if (!isNonEmptyString(pastedString)) {
      return;
    }
    const listOfSeparatorFounded = [];
    this.separatorToControlOnPaste.forEach(separator => {
      if (pastedString.indexOf(separator.key) !== -1) {
        listOfSeparatorFounded.push(separator);
      }
    });

    let currentValue = this.formControl.value;
    if (!isArray(currentValue)) {
      currentValue = [];
    }

    if (listOfSeparatorFounded.length > 0) {
      this.subscribe(DialogUtil.open(this._dialog, KeywordPasteDialog, {
          stringPaste: pastedString,
          listSeparator: listOfSeparatorFounded,
        }, undefined, separator => {
          const listValue = pastedString.split(separator)
            .map((value: string) => this._transformValue(value))
            .filter(value => isNonEmptyString(value));

          this.formControl.setValue(ArrayUtil.distinct([...currentValue, ...listValue]));
          this.formControl.markAsDirty();
          $event.preventDefault();
          this._changeDetector.detectChanges();
        },
        () => {
          this.formControl.setValue(ArrayUtil.distinct([...currentValue, this._transformValue(pastedString)]));
          this.formControl.markAsDirty();
          this._changeDetector.detectChanges();
        }));
    } else {
      this.formControl.setValue(ArrayUtil.distinct([...currentValue, this._transformValue(pastedString)]));
      this.formControl.markAsDirty();
    }
    $event.preventDefault();
    $event.stopPropagation();
  }

  private _transformValue(value: string): string {
    value = value.trim();
    return this.capitalize ? StringUtil.capitalize(value) : value;
  }

  keydown($event: KeyboardEvent): void {
    if (this.separatorKeys.indexOf($event.key) !== -1) {
      this.inputChip.chipEnd.next({input: this.input.nativeElement, value: this.input.nativeElement.value} as MatChipInputEvent);
      $event.stopPropagation();
      $event.preventDefault();
    }
  }

  edit(oldValue: string, $event: MatChipEditedEvent): void {
    const newValue = $event.value;
    if (isNullOrUndefinedOrWhiteString(newValue)) {
      this.remove(oldValue);
      return;
    }

    let currentValue = this.formControl.value;
    if (!isArray(currentValue)) {
      currentValue = [];
    } else {
      currentValue = [...currentValue];
    }

    const index = currentValue.indexOf(oldValue);
    if (index >= 0) {
      const newValueTransformed = this._transformValue(newValue);
      if (newValueTransformed === oldValue) {
        return;
      }
      currentValue[index] = newValueTransformed;
      this.formControl.setValue(ArrayUtil.distinct(currentValue));
      this.formControl.markAsDirty();
    }
  }
}
