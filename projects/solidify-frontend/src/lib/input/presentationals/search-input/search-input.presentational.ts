/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - search-input.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
} from "@angular/forms";
import {
  FloatLabelType,
  MatFormFieldAppearance,
  SubscriptSizing,
} from "@angular/material/form-field";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {DataTestPartialEnum} from "../../../core-resources/enums/partial/data-test-partial.enum";
import {
  isNonEmptyString,
  isNullOrUndefined,
} from "../../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../../core-resources/utils/ssr.util";
import {StringUtil} from "../../../core-resources/utils/string.util";
import {AbstractInternalPresentational} from "../../../core/components/presentationals/abstract-internal/abstract-internal.presentational";
import {IconNamePartialEnum} from "../../../core/enums/partial/icon-name-partial.enum";
import {BaseFormDefinition} from "../../../core/models/forms/base-form-definition.model";
import {ExtendEnum} from "../../../core-resources/types/extend-enum.type";
import {ObservableUtil} from "../../../core/utils/observable.util";

@Component({
  selector: "solidify-search-input",
  templateUrl: "./search-input.presentational.html",
  styleUrls: ["./search-input.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchInputPresentational extends AbstractInternalPresentational implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  searchForm: FormGroup;

  readonly MODE_VALIDATION: string = "validation";
  readonly MODE_AUTO: string = "auto";

  private _value: string;

  @Input()
  appearance: MatFormFieldAppearance = this.appearanceInputMaterial;

  @Input()
  positionLabel: FloatLabelType = this.positionLabelInputMaterial;

  @Input()
  subscriptSizing: SubscriptSizing = this.subscriptSizingInputMaterial;

  @Input()
  searchAutoFocus: boolean = false;

  @Input()
  placeholder: string | undefined = undefined;

  @Input()
  allowEmptySearch: boolean = false;

  @Input()
  iconSearchName: ExtendEnum<IconNamePartialEnum> = IconNamePartialEnum.send;

  @Input()
  dataTest: ExtendEnum<DataTestPartialEnum>;

  @Input()
  set value(value: string) {
    this._initForm();
    this._value = value;
    this.formControl.setValue(this.value);
  }

  get value(): string {
    return isNullOrUndefined(this._value) ? StringUtil.stringEmpty : this._value;
  }

  get formControl(): FormControl {
    return this.searchForm.get(this.formDefinition.search) as FormControl;
  }

  @Input()
  mode: "auto" | "validation" = "auto";

  @Input()
  labelToTranslate: string = this.labelTranslateInterface.inputSearch;

  private readonly _valueBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("valueChange")
  readonly valueObs: Observable<string | undefined> = ObservableUtil.asObservable(this._valueBS);

  private readonly _blurBS: BehaviorSubject<HTMLInputElement | undefined> = new BehaviorSubject<HTMLInputElement | undefined>(undefined);
  @Output("blurChange")
  readonly blurObs: Observable<HTMLInputElement | undefined> = ObservableUtil.asObservable(this._blurBS);

  private _debounceTime: number = 600;

  constructor(protected readonly _fb: FormBuilder,
              protected readonly _injector: Injector) {
    super(_injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._initForm();
    this.formControl.setValue(this.value);

    if (this.mode === this.MODE_AUTO && SsrUtil.isBrowser) {
      this.subscribe(
        this.formControl.valueChanges
          .pipe(
            debounceTime(this._debounceTime),
            distinctUntilChanged(),
            tap((value: string) => {
              this._submitValue(value);
            }),
          ),
      );
    }
  }

  private _initForm(): void {
    if (!isNullOrUndefined(this.searchForm)) {
      return;
    }
    this.searchForm = this._fb.group({
      [this.formDefinition.search]: StringUtil.stringEmpty,
    });
  }

  private _submitValue(value: string): void {
    this._valueBS.next(value.trim());
  }

  isValuePresent(): boolean {
    return isNonEmptyString(this.formControl.value);
  }

  submit(): void {
    const value = this.formControl.value;
    this._submitValue(value);
  }

  clean(): void {
    this.formControl.setValue(StringUtil.stringEmpty);
  }

  blur(inputElement: HTMLInputElement): void {
    this._blurBS.next(inputElement);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  search: string = "search";
}
