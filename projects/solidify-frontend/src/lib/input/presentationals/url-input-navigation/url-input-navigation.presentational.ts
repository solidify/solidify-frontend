/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - url-input-navigation.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {
  FloatLabelType,
  MatFormFieldAppearance,
  SubscriptSizing,
} from "@angular/material/form-field";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  pairwise,
  tap,
} from "rxjs/operators";
import {
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
} from "../../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../../core-resources/utils/ssr.util";
import {StringUtil} from "../../../core-resources/utils/string.util";
import {AbstractInternalPresentational} from "../../../core/components/presentationals/abstract-internal/abstract-internal.presentational";
import {SOLIDIFY_CONSTANTS} from "../../../core-resources/constants";
import {IconNamePartialEnum} from "../../../core/enums/partial/icon-name-partial.enum";
import {FormValidationHelper} from "../../../core/helpers/form-validation.helper";
import {BaseFormDefinition} from "../../../core/models/forms/base-form-definition.model";
import {ExtendEnum} from "../../../core-resources/types/extend-enum.type";
import {ObservableUtil} from "../../../core/utils/observable.util";

@Component({
  selector: "solidify-url-input-navigator",
  templateUrl: "./url-input-navigation.presentational.html",
  styleUrls: ["./url-input-navigation.presentational.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: UrlInputNavigationPresentational,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class UrlInputNavigationPresentational extends AbstractInternalPresentational implements OnInit, ControlValueAccessor {
  @Input()
  placeholderToTranslate: string = this.labelTranslateInterface.inputUrlInputLabel;

  @Input()
  formControl: FormControl;

  @Input()
  readonly: boolean;

  @Input()
  disabled: boolean;

  @Input()
  appearance: MatFormFieldAppearance = this.appearanceInputMaterial;

  @Input()
  positionLabel: FloatLabelType = this.positionLabelInputMaterial;

  @Input()
  subscriptSizing: SubscriptSizing = this.subscriptSizingInputMaterial;

  @Input()
  prefixUrl: string;

  @Input()
  callbackUrl: (value: string) => string;

  @Input()
  externalLinkTooltipToTranslate: string;

  @Input()
  showEmptyRequiredFieldInError: boolean = this.displayEmptyRequiredFieldInError;

  @Input()
  prefixIcon: ExtendEnum<IconNamePartialEnum>;

  classInputIgnored: string = this.environment.classInputIgnored;

  cssClass: string = StringUtil.stringEmpty;

  private readonly _valueBS: BehaviorSubject<string[] | undefined> = new BehaviorSubject<string[] | undefined>(undefined);
  @Output("valueChange")
  readonly valueObs: Observable<string[] | undefined> = ObservableUtil.asObservable(this._valueBS);

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  constructor(private readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_injector);
  }

  ngOnInit(): void {
    if (this.disabled) {
      this.cssClass = this.classInputIgnored;
    }

    if (isNotNullNorUndefined(this.formControl)) {
      this.subscribe(this.formControl.statusChanges.pipe(
        distinctUntilChanged(),
        filter(status => status === SOLIDIFY_CONSTANTS.FORM_STATUS_INVALID),
        tap(status => {
          setTimeout(() => {
            this._changeDetector.detectChanges();
          });
        }),
      ));

      this.subscribe(this.formControl.valueChanges.pipe(
        distinctUntilChanged(),
        pairwise(),
        tap(([prev, next]) => {
          this.propagateChange(next);
          this._valueBS.next(next);
          if (prev === next) {
            this.formControl.markAsPristine();
          }
          this._changeDetector.detectChanges();
        }),
      ));
    }
  }

  propagateChange: (__: any) => void = (__: any) => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(value: string[]): void {
  }

  navigateToUrl(event: Event, fd: AbstractControl): void {
    if (isNotNullNorUndefined(event)) {
      event.stopPropagation();
      event.preventDefault();
    }
    SsrUtil.window?.open(this._generateUrl(fd.value), "_blank");
  }

  private _generateUrl(value): string {
    if (isNotNullNorUndefined(this.callbackUrl)) {
      return this.callbackUrl(value);
    }
    if (isNotNullNorUndefinedNorWhiteString(this.prefixUrl)) {
      return this.prefixUrl + value;
    }
    return value;
  }

}

class FormComponentFormDefinition extends BaseFormDefinition {
  url: string = "url";
}

