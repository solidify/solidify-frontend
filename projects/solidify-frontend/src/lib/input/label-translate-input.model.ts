/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - label-translate-input.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MARK_AS_TRANSLATABLE} from "../core-resources/utils/translate.util";

export interface LabelSolidifyInput {
  inputFileUploadInputNoFileAvailable?: string;
  inputFileUploadInputFileToUpload?: string;
  inputFileUploadInputFile?: string;
  inputFileUploadInputButtonPreview?: string;
  inputFileUploadInputButtonDownload?: string;
  inputFileUploadInputButtonDelete?: string;
  inputComma?: string;
  inputDash?: string;
  inputKeywordInputLabel?: string;
  inputLineBreak?: string;
  inputMultiSelectDefaultValue?: string;
  inputMultiSelectNoDataToSelect?: string;
  inputMultiSelectNotificationExtraInfoCopyToClipboard?: string;
  inputMultiSelectPlaceholder?: string;
  inputSearch?: string;
  inputSemicolon?: string;
  inputUnderscore?: string;
  inputUrlInputLabel?: string;
}

export const labelSolidifyInput: LabelSolidifyInput = {
  inputFileUploadInputNoFileAvailable: MARK_AS_TRANSLATABLE("solidify.input.fileUploadInput.noFileAvailable"),
  inputFileUploadInputFileToUpload: MARK_AS_TRANSLATABLE("solidify.input.fileUploadInput.fileToUpload"),
  inputFileUploadInputFile: MARK_AS_TRANSLATABLE("solidify.input.fileUploadInput.file"),
  inputFileUploadInputButtonPreview: MARK_AS_TRANSLATABLE("solidify.input.fileUploadInput.button.preview"),
  inputFileUploadInputButtonDownload: MARK_AS_TRANSLATABLE("solidify.input.fileUploadInput.button.download"),
  inputFileUploadInputButtonDelete: MARK_AS_TRANSLATABLE("solidify.input.fileUploadInput.button.delete"),
  inputComma: MARK_AS_TRANSLATABLE("solidify.input.comma"),
  inputDash: MARK_AS_TRANSLATABLE("solidify.input.dash"),
  inputKeywordInputLabel: MARK_AS_TRANSLATABLE("solidify.input.keywordInputLabel"),
  inputLineBreak: MARK_AS_TRANSLATABLE("solidify.input.lineBreak"),
  inputMultiSelectDefaultValue: MARK_AS_TRANSLATABLE("solidify.input.multiSelect.defaultValue"),
  inputMultiSelectNoDataToSelect: MARK_AS_TRANSLATABLE("solidify.input.multiSelect.noDataToSelect"),
  inputMultiSelectNotificationExtraInfoCopyToClipboard: MARK_AS_TRANSLATABLE("solidify.input.multiSelect.notificationExtraInfoCopyToClipboard"),
  inputMultiSelectPlaceholder: MARK_AS_TRANSLATABLE("solidify.input.multiSelect.placeholder"),
  inputSearch: MARK_AS_TRANSLATABLE("solidify.input.search"),
  inputSemicolon: MARK_AS_TRANSLATABLE("solidify.input.semicolon"),
  inputUnderscore: MARK_AS_TRANSLATABLE("solidify.input.underscore"),
  inputUrlInputLabel: MARK_AS_TRANSLATABLE("solidify.input.urlInputLabel"),
};
