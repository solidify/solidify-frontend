/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - twitter-wrapper.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ChangeDetectionStrategy,
  Component,
  Injector,
} from "@angular/core";
import {AbstractInternalPresentational} from "../../../../core/components/presentationals/abstract-internal/abstract-internal.presentational";
import {TwitterChromeEnum} from "../../../enums/twitter-chrome.enum";
import {TwitterSourceTypeEnum} from "../../../enums/twitter-source-type.enum";

@Component({
  selector: "solidify-twitter-wrapper-presentational",
  templateUrl: "./twitter-wrapper.presentational.html",
  styleUrls: ["./twitter-wrapper.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TwitterWrapperPresentational extends AbstractInternalPresentational {
  readonly sourceType: TwitterSourceTypeEnum = TwitterSourceTypeEnum.profile;
  readonly twitterEnabled: boolean = this.environment.twitterEnabled;
  readonly screenName: string = this.environment.twitterAccount;
  readonly tweetLimit: number = this.environment.twitterTweetToDisplay;
  readonly chrome: TwitterChromeEnum[] = [TwitterChromeEnum.noborders, TwitterChromeEnum.transparent];

  constructor(protected readonly _injector: Injector) {
    super(_injector);
  }
}
