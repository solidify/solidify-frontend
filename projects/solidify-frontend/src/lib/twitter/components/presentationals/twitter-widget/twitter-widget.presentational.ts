/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - twitter-widget.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  Injector,
  Input,
  Renderer2,
} from "@angular/core";
import {Store} from "@ngxs/store";
import {
  catchError,
  distinctUntilChanged,
  take,
  tap,
} from "rxjs/operators";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {AbstractInternalPresentational} from "../../../../core/components/presentationals/abstract-internal/abstract-internal.presentational";
import {DefaultSolidifyEnvironment} from "../../../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {LoggingService} from "../../../../core/services/logging.service";
import {MemoizedUtil} from "../../../../core/utils/stores/memoized.util";
import {TwitterChromeEnum} from "../../../enums/twitter-chrome.enum";
import {TwitterSourceTypeEnum} from "../../../enums/twitter-source-type.enum";
import {TwitterMetadata} from "../../../models/twitter-metadata.model";
import {
  DataSource,
  Options,
} from "../../../models/twitter-widget.model";
import {TwitterService} from "../../../services/twitter.service";

@Component({
  selector: "solidify-twitter-widget-presentational",
  templateUrl: "./twitter-widget.presentational.html",
  styleUrls: ["./twitter-widget.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TwitterWidgetPresentational extends AbstractInternalPresentational implements AfterViewInit {
  @Input()
  sourceType: TwitterSourceTypeEnum | undefined;

  @Input()
  screenName: string | undefined;

  @Input()
  userId: string | undefined;

  @Input()
  tweetLimit: number | undefined;

  @Input()
  borderColor: string | undefined;

  @Input()
  ariaPolite: string | undefined;

  @Input()
  height: number | undefined;

  @Input()
  chrome: TwitterChromeEnum[] = [TwitterChromeEnum.noborders, TwitterChromeEnum.transparent];

  constructor(protected readonly _injector: Injector,
              private readonly _renderer: Renderer2,
              private readonly _element: ElementRef,
              private readonly _twitterService: TwitterService,
              private readonly _loggingService: LoggingService,
              private readonly _store: Store,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super(_injector);
  }

  ngAfterViewInit(): void {
    // https://developer.twitter.com/en/docs/twitter-for-websites/javascript-api/guides/scripting-factory-functions
    this.subscribe(this._twitterService.loadScript(this._renderer)
      .pipe(
        take(1),
        tap((twitterMetadata: TwitterMetadata) => {
          if (isNullOrUndefined(twitterMetadata)) {
            this._loggingService.logWarning("Twitter : Unable to load twitter, probably blocked by an extension");
            return;
          }
          const nativeElement: Element = this._element.nativeElement;
          const dataSource: DataSource = {
            sourceType: this.sourceType,
            screenName: this.screenName,
            userId: this.userId,
          };
          const options: Options = {
            theme: MemoizedUtil.selectSnapshot(this._store, this.environment.appState, state => state.darkMode) ? "dark" : undefined,
            tweetLimit: this.tweetLimit,
            height: this.height,
            chrome: this.chrome,
            ariaPolite: this.ariaPolite,
            borderColor: this.borderColor,
          };

          this.subscribe(MemoizedUtil.select(this._store, this.environment.appState, state => state.darkMode).pipe(
            distinctUntilChanged(),
            tap(enabled => {
              const list: NodeList = this._element.nativeElement.querySelectorAll("iframe");
              list.forEach(node => {
                try {
                  this._element.nativeElement.removeChild(node.parentNode);
                } catch (e) {
                  // eslint-disable-next-line no-console
                  console.error("Unable to remove node in native element", node.parentNode, this._element.nativeElement);
                }
              });
              options.theme = enabled ? "dark" : undefined;
              twitterMetadata.widgets.createTimeline(dataSource, nativeElement, options)
                .then((iframe: any) => {})
                .catch((message: string) => {
                  this._loggingService.logWarning("Twitter : Could not create widget", message);
                  throw this._environment.errorToSkipInErrorHandler;
                });
            })));
        }),
        catchError((err: any) => {
          this._loggingService.logWarning("Twitter : Error loading twitter widget", err);
          throw this._environment.errorToSkipInErrorHandler;
        }),
      ));
  }
}
