/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-app-tour.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Inject} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {IMdStepOption} from "ngx-ui-tour-md-menu/lib/step-option.interface";
import {
  Observable,
  Subject,
} from "rxjs";
import {
  take,
  tap,
} from "rxjs/operators";
import {FirstLoginDialog} from "../core/components/dialogs/first-login/first-login.dialog";
import {StepTourSectionNamePartialEnum} from "../core/enums/partial/step-tour-section-name-partial.enum";
import {DefaultSolidifyEnvironment} from "../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../core/injection-tokens/label-to-translate.injection-token";
import {AbstractBaseService} from "../core/services/abstract-base.service";
import {SolidifyAppAction} from "../core/stores/abstract/app/app.action";
import {
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../core-resources/types/extend-enum.type";
import {DialogUtil} from "../core/utils/dialog.util";
import {LabelTranslateInterface} from "../label-translate-interface.model";

// @dynamic
export abstract class AbstractAppTourService extends AbstractBaseService {
  isInTour: boolean;

  defaultOptions: IStepTourOption;

  get fullListTourStepOptions(): IStepTourOption[] {
    const fullListOption: IStepTourOption[] = [];
    this._tourSteps.map(t => t.stepTour).forEach(s => {
      if (isNonEmptyArray(s)) {
        fullListOption.push(...s);
      }
    });
    return fullListOption;
  }

  constructor(protected readonly _store: Store,
              protected readonly _matDialog: MatDialog,
              protected readonly _tourService: TourServiceInterface,
              protected readonly _tourSteps: ITourSection[],
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslate: LabelTranslateInterface,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super();
    this.defaultOptions = {
      centerAnchorOnScroll: false,
      endBtnTitle: this._labelTranslate.tourClose,
      enableBackdrop: true,
    };
  }

  runFullTour(): void {
    setTimeout(() => {
      this.subscribe(DialogUtil.open(this._matDialog, FirstLoginDialog, undefined, {
        maxWidth: "400px",
      }, result => this.runTourFromListOption(this.fullListTourStepOptions, {}, this._environment.routeHomePage)));
    }, 1000);
  }

  runTour(stepTourSectionName: ExtendEnum<StepTourSectionNamePartialEnum>, extraOptions: IStepTourOption | undefined = undefined, navigationEnd: string | undefined = undefined): void {
    const section = this._tourSteps.find(s => s.key === stepTourSectionName);
    if (isNonEmptyArray(section?.stepTour)) {
      this.runTourFromListOption(section?.stepTour, extraOptions, navigationEnd);
    }
  }

  private runTourFromListOption(steps: IStepTourOption[], extraOptions: IStepTourOption | undefined = undefined, navigationEnd: string | undefined = undefined): void {
    this._initializedTour(steps, isNullOrUndefined(extraOptions) ? {} : extraOptions);
    this.subscribe(this._endTourObservable(navigationEnd));
    this._startTour();
  }

  private _initializedTour(steps: IStepTourOption[], extraOptions: IStepTourOption = {}): void {
    this._tourService.initialize(steps, {...this.defaultOptions, ...extraOptions});
  }

  private _endTourObservable(navigationEnd: string | undefined): Observable<void> {
    return this._tourService.end$.asObservable().pipe(
      take(1),
      tap(() => {
        this._store.dispatch(new SolidifyAppAction.SetIsInTourMode(false));
        if (isNotNullNorUndefined(navigationEnd)) {
          this._store.dispatch(new Navigate([navigationEnd]));
        }
      }),
    );
  }

  private _startTour(): void {
    this._store.dispatch(new SolidifyAppAction.SetIsInTourMode(true));
    this._tourService.start();
  }
}

export interface TourServiceInterface {
  start(): void;

  initialize(steps: IStepTourOption[], stepDefaults?: IStepTourOption): void;

  end$: Subject<any>;

  hasPrev(step: IStepTourOption): any;

  prev(): any;

  hasNext(step: IStepTourOption): any;

  next(): any;

  end(): any;
}

export interface IStepTourOption extends IMdStepOption {
}

export interface ITourSection {
  key: ExtendEnum<StepTourSectionNamePartialEnum>;
  stepTour: IStepTourOption[];
}
