/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - string.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {SOLIDIFY_CONSTANTS} from "../constants";
import {ChangeCase} from "../lib-wrapped/change-case.model";
import {KeyValue} from "../models/key-value.model";
import {
  isEmptyString,
  isNullOrUndefined,
} from "../tools/is/is.tool";

export class StringUtil {
  static readonly stringEmpty: string = SOLIDIFY_CONSTANTS.STRING_EMPTY;

  static format(str: string, ...val: string[]): string {
    for (let i = 0; i < val.length; i++) {
      str = this.replaceAll(str, `{${i}}`, val[i]);
    }
    return str;
  }

  static formatKeyValue(str: string, ...val: KeyValue[]): string {
    val.forEach(v => {
      str = this.replaceAll(str, `{${v.key}}`, v.value);
    });
    return str;
  }

  static capitalize(origin: string): string {
    if (isNullOrUndefined(origin) || isEmptyString(origin)) {
      return origin;
    }
    const firstCharUpper = origin.substring(0, 1).toUpperCase();
    if (origin.length === 1) {
      return firstCharUpper;
    }
    return firstCharUpper + origin.substring(1, origin.length);
  }

  static convertSpinalCaseToPascalCase(origin: string): string {
    origin = this.replaceAll(origin, "-", " ");
    return ChangeCase.pascalCase(origin);
  }

  static convertSpinalCaseToCamelCase(origin: string): string {
    origin = this.replaceAll(origin, "-", " ");
    return ChangeCase.camelCase(origin);
  }

  /**
   * twoWords
   * @param origin
   */
  static convertToCamelCase(origin: string): string {
    return ChangeCase.camelCase(origin);
  }

  /**
   * two-words
   * @param origin
   */
  static convertToSpinalCase(origin: string): string {
    origin = this.replaceAll(origin, " ", "-");
    return ChangeCase.kebabCase(origin);
  }

  /**
   * two_words
   * @param origin
   */
  static convertToSnakeCase(origin: string): string {
    origin = this.replaceAll(origin, " ", "_");
    return ChangeCase.snakeCase(origin);
  }

  /**
   * TwoWords
   * @param origin
   */
  static convertToPascalCase(origin: string): string {
    return ChangeCase.pascalCase(origin);
  }

  /**
   * Two words
   * @param value
   */
  static convertToSentence(value: string): string {
    value = StringUtil.convertToSnakeCase(value);
    value = StringUtil.capitalize(value);
    return this.replaceAll(value, "_", " ");
  }

  static replaceAll(str: string, oldChar: string, newChar: string): string {
    const strSplitter = str.split(oldChar);
    return strSplitter.join(newChar);
  }

  static replaceNonIso8859_1Chars(value: string): string {
    if (isNullOrUndefined(value)) {
      return value;
    }
    value = this.replaceAll(value, "’", "'");
    return value
      .normalize("NFD")  // Unicode normalization (decomposition of accented characters)
      .replace(/[\u0300-\u036f]/g, "")  // Remove diacritical marks
      .replace(/[^\u0000-\u00FF]/g, "_");  // Filter to keep only ISO-8859-1 characters
  }

  static replaceForbiddenFileNameChar(value: string, replacementChar: string): string {
    if (isNullOrUndefined(value)) {
      return value;
    }
    value = this.replaceAll(value, "\\", replacementChar);
    value = this.replaceAll(value, "/", replacementChar);
    value = this.replaceAll(value, ":", replacementChar);
    value = this.replaceAll(value, "*", replacementChar);
    value = this.replaceAll(value, "?", replacementChar);
    value = this.replaceAll(value, "\"", replacementChar);
    value = this.replaceAll(value, "<", replacementChar);
    value = this.replaceAll(value, ">", replacementChar);
    value = this.replaceAll(value, "|", replacementChar);
    return value;
  }
}
