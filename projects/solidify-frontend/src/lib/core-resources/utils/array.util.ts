/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - array.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import _ from "lodash";
import {
  isArray,
  isFunction,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isString,
} from "../tools/is/is.tool";
import {ObjectUtil} from "./object.util";

export class ArrayUtil {
  static distinct<T>(array: T[], key: string | undefined = undefined): T[] {
    const result = [];
    const map = new Map();
    for (const item of array) {
      let mapKey = item;
      if (!isNullOrUndefined(key)) {
        mapKey = item[key];
      }
      if (!map.has(mapKey)) {
        map.set(mapKey, item);
        result.push(item);
      }
    }
    return result;
  }

  /**
   * Allow to get diff between 2 array
   *
   * @param arrayA
   * @param arrayB
   */
  static diff<T>(arrayA: T[], arrayB: T[], comparator: (a: T, b: T) => boolean = undefined): ArrayDiffResult<T> {
    /* eslint-disable no-console */
    if (isNullOrUndefined(arrayA)) {
      console.warn("'arrayA' param should not be null");
      return undefined;
    }
    if (!isArray(arrayA)) {
      console.warn("'arrayA' param should be an array");
      return undefined;
    }
    if (isNullOrUndefined(arrayB)) {
      console.warn("'arrayB' param should not be null");
      return undefined;
    }
    if (!isArray(arrayB)) {
      console.warn("'arrayB' param should be an array");
      return undefined;
    }
    /* eslint-enable no-console */
    const arrayDiffResult = {
      arrayA: arrayA,
      arrayB: arrayB,
    } as ArrayDiffResult<T>;
    if (isFunction(comparator)) {
      arrayDiffResult.diff = _.xorWith(arrayA, arrayB, comparator as any);
      arrayDiffResult.common = _.intersectionWith(arrayA, arrayB, comparator as any);
      arrayDiffResult.presentOnlyInArrayA = _.intersectionWith(arrayA, arrayDiffResult.diff, comparator as any);
      arrayDiffResult.presentOnlyInArrayB = _.intersectionWith(arrayB, arrayDiffResult.diff, comparator as any);
    } else {
      arrayDiffResult.diff = _.xor(arrayA, arrayB);
      arrayDiffResult.common = _.intersection(arrayA, arrayB);
      arrayDiffResult.presentOnlyInArrayA = _.intersection(arrayA, arrayDiffResult.diff);
      arrayDiffResult.presentOnlyInArrayB = _.intersection(arrayB, arrayDiffResult.diff);
    }
    arrayDiffResult.missingInArrayA = arrayDiffResult.presentOnlyInArrayB;
    arrayDiffResult.missingInArrayB = arrayDiffResult.presentOnlyInArrayA;
    return arrayDiffResult;
  }

  /**
   * Extract items from an array depending of a matching function
   *
   * @param array
   * @param matchingFunc
   * @return an array with extracted items
   */
  static extract<T>(array: T[], matchingFunc: (item: T) => boolean): T[] {
    const indexToExtract = [];
    const extractedItems = [];
    array.forEach((item, index) => {
      if (matchingFunc(item)) {
        indexToExtract.push(index);
      }
    });
    indexToExtract.reverse().forEach(index => {
      extractedItems.push(...array.splice(index, 1));
    });
    return extractedItems;
  }

  static insert<T>(array: T[], index: number, ...items: T[]): T[] {
    /* eslint-disable no-console */
    if (isNullOrUndefined(array)) {
      console.warn("'array' param should not be null");
      return undefined;
    }
    if (!isArray(array)) {
      console.warn("'array' param should be an array");
      return undefined;
    }
    if (isNullOrUndefined(items)) {
      console.warn("'items' param should not be null");
      return undefined;
    }
    if (!isArray(items)) {
      console.warn("'items' param should be an array");
      return undefined;
    }
    if (index > array.length) {
      console.warn("'index' is out of bound");
      return undefined;
    }
    /* eslint-enable no-console */
    array = [...array];
    array.splice(index, 0, ...items);
    return array;
  }

  static sortAscendant<T>(list: T[], key?: string | undefined): T[] {
    return list.sort((a, b) => ArrayUtil.sortAscendantFunc(a, b, key));
  }

  static sortAscendantWithCallback<T>(list: T[], callback: (item: T) => any): T[] {
    return list.sort((a, b) => ArrayUtil.sortAscendantWithCallbackFunc(a, b, callback));
  }

  static sortDescendant<T>(list: T[], key?: string | undefined): T[] {
    return list.sort((a, b) => ArrayUtil.sortDescendantFunc(a, b, key));
  }

  static sortDescendantWithCallback<T>(list: T[], callback: (item: T) => any): T[] {
    return list.sort((a, b) => ArrayUtil.sortDescendantWithCallbackFunc(a, b, callback));
  }

  static sortAscendantFunc<T>(a: T, b: T, key?: string | undefined): number {
    let func = null;
    if (isNotNullNorUndefinedNorWhiteString(key)) {
      func = item => ObjectUtil.getNestedValue(item, key);
    }
    return this.sortAscendantWithCallbackFunc(a, b, func);
  }

  static sortAscendantWithCallbackFunc<T>(a: T, b: T, callback: (item: T) => any): number {
    let valueA: any = a;
    let valueB: any = b;
    if (isFunction(callback)) {
      valueA = callback(a);
      valueB = callback(b);
    }
    if (isString(valueA)) {
      valueA = valueA.toLowerCase();
    }
    if (isString(valueB)) {
      valueB = valueB.toLowerCase();
    }
    if (valueA < valueB) {
      return -1;
    }
    if (valueA > valueB) {
      return 1;
    }
    return 0;
  }

  static sortDescendantFunc<T>(a: T, b: T, key?: string | undefined): number {
    let func = null;
    if (isNotNullNorUndefinedNorWhiteString(key)) {
      func = item => ObjectUtil.getNestedValue(item, key);
    }
    return this.sortDescendantWithCallbackFunc(a, b, func);
  }

  static sortDescendantWithCallbackFunc<T>(a: T, b: T, callback: (item: T) => any): number {
    const sort = this.sortAscendantWithCallbackFunc(a, b, callback);
    if (sort === 1) {
      return -1;
    }
    if (sort === -1) {
      return 1;
    }
    return 0;
  }
}

export interface ArrayDiffResult<T> {
  arrayA: T[];
  arrayB: T[];
  diff: T[];
  common: T[];
  missingInArrayA: T[];
  missingInArrayB: T[];
  presentOnlyInArrayA: T[];
  presentOnlyInArrayB: T[];
}
