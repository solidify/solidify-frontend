/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - object.util.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ObjectUtil} from "./object.util";

describe("ObjectUtil", () => {
  describe("getNestedValue", () => {
    it("should get nested attribute value when 1 level", () => {
      const object = {
        attribut1: "test",
      };
      expect(ObjectUtil.getNestedValue(object, "attribut1")).toEqual("test");
    });

    it("should get nested attribute value when 2 level", () => {
      const object = {
        attribut1: {
          attribut2: "test",
        },
      };
      expect(ObjectUtil.getNestedValue(object, "attribut1.attribut2")).toEqual("test");
    });

    it("should get nested attribute value when 3 level", () => {
      const object = {
        attribut1: {
          attribut2: {
            attribut3: "test",
          },
        },
      };
      expect(ObjectUtil.getNestedValue(object, "attribut1.attribut2.attribut3")).toEqual("test");
    });

    it("should get default value when invalid key in second part", () => {
      const object = {
        attribut1: {
          attribut2: "test",
        },
      };
      expect(ObjectUtil.getNestedValue(object, "attribut1.wrongAttribut2")).toBeUndefined();
    });

    it("should get default value when invalid key in first part", () => {
      const object = {
        attribut1: {
          attribut2: "test",
        },
      };
      expect(ObjectUtil.getNestedValue(object, "wrongAttribut1.attribut2")).toBeUndefined();
    });

    it("should get default value when object is empty", () => {
      const object = {};
      expect(ObjectUtil.getNestedValue(object, "attribut1.attribut2")).toBeUndefined();
    });

    it("should get default value when object is null", () => {
      const object = null;
      expect(ObjectUtil.getNestedValue(object, "attribut1.attribut2")).toBeUndefined();
    });

    it("should get default value when object is undefined", () => {
      const object = null;
      expect(ObjectUtil.getNestedValue(object, "attribut1.attribut2")).toBeUndefined();
    });

    it("should get default value when key is undefined", () => {
      const object = {
        attribut1: {
          attribut2: "test",
        },
      };
      expect(ObjectUtil.getNestedValue(object, undefined)).toBeUndefined();
    });

    it("should get default value when key is null", () => {
      const object = {
        attribut1: {
          attribut2: "test",
        },
      };
      expect(ObjectUtil.getNestedValue(object, null)).toBeUndefined();
    });

    it("should get default value when key is empty", () => {
      const object = {
        attribut1: {
          attribut2: "test",
        },
      };
      expect(ObjectUtil.getNestedValue(object, "")).toBeUndefined();
    });

    it("should get default value when key is space", () => {
      const object = {
        attribut1: {
          attribut2: "test",
        },
      };
      expect(ObjectUtil.getNestedValue(object, " ")).toBeUndefined();
    });
  });
});
