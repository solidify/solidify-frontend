/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - clipboard.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {isTruthyObject} from "../tools/is/is.tool";
import {SsrUtil} from "./ssr.util";

export class ClipboardUtil {
  static copyStringToClipboard(value: string, doc: Document = SsrUtil.window?.document): boolean {
    if (isTruthyObject(doc)) {
      const textareaElement = doc.createElement("textarea");
      textareaElement.value = value;
      textareaElement.setAttribute("readonly", "");
      textareaElement.style.position = "absolute";
      textareaElement.style.opacity = "0";
      textareaElement.style.pointerEvents = "none";
      doc.body.appendChild(textareaElement);
      const selection = doc.getSelection();
      const selectedRange = selection.rangeCount > 0 ? selection.getRangeAt(0) : undefined;
      textareaElement.select();
      textareaElement.setSelectionRange(0, textareaElement.value.length); // For mobile devices
      doc.execCommand("copy");
      doc.body.removeChild(textareaElement);
      if (isTruthyObject(selectedRange)) {
        doc.getSelection().removeAllRanges();
        doc.getSelection().addRange(selectedRange);
      }
      return true;
    }
    return false;
  }
}
