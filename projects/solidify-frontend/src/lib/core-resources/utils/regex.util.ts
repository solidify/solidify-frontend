/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - regex.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

export class RegexUtil {
  // WARNING : FIREFOX DOESN'T SUPPORT GROUP ON REGEX !
  static url: RegExp = new RegExp("^(https?:\/\/)?([a-z0-9.-]+)(:[0-9]{1,5})?(\/[a-zA-Z0-9._~:/?#[\\]@!$&'()*+,;=%-]*)?$", "i");
  static uri: RegExp = new RegExp("^([a-z0-9+.-]+):\/\/.");
  static subDirectory: RegExp = new RegExp("^\/$|\/.*[^\/]$");
  static urlInComplexText: RegExp = new RegExp("(http|ftp|https):\\/\\/([\\w.,@?^=%&:\\/~+#-]*[\\w@?^=%&\\/~+#-])?", "gi");
  static htmlTag: RegExp = new RegExp("<[^>]*>", "gi");
  static isValidEmail: RegExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
}
