/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - string.util.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {KeyValue} from "../models/key-value.model";
import {StringUtil} from "./string.util";

describe("StringUtil", () => {
  describe("format", () => {
    it("should insert 2 values in string", () => {
      const originalString = "My string with {0} injected value and {1} value";
      const val1 = "val1";
      const val2 = "val2";
      const result = StringUtil.format(originalString, val1, val2);
      expect(result).toBe("My string with val1 injected value and val2 value");
    });

    it("should insert 1 string in string that have 2 unresolved value", () => {
      const originalString = "My string with {0} injected value and {1} value";
      const val1 = "val1";
      const result = StringUtil.format(originalString, val1);
      expect(result).toBe("My string with val1 injected value and {1} value");
    });
  });

  describe("formatKeyValue", () => {
    it("should insert 2 values in string by key", () => {
      const originalString = "My string with {key1} injected value and {key2} value";
      const keyValue1 = new KeyValue("key1", "val2");
      const keyValue2 = new KeyValue("key2", "val1");
      const result = StringUtil.formatKeyValue(originalString, keyValue2, keyValue1);
      expect(result).toBe("My string with val2 injected value and val1 value");
    });
  });

  describe("convertSpinalCaseToPascalCase", () => {
    it("should convert spinalCase to PascalCase", () => {
      expect(StringUtil.convertSpinalCaseToPascalCase("my-string")).toBe("MyString");
    });
  });

  describe("convertSpinalCaseToCamelCase", () => {
    it("should convert spinalCase to camelCase", () => {
      expect(StringUtil.convertSpinalCaseToCamelCase("my-string")).toBe("myString");
    });
  });

  describe("convertToPascalCase", () => {
    it("should convert string to PascalCase", () => {
      expect(StringUtil.convertToPascalCase("my string")).toBe("MyString");
    });
  });
});
