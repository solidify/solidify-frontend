/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - file.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  isNullOrUndefined,
  isNumber,
} from "../tools/is/is.tool";
import {StringUtil} from "./string.util";

export class FileUtil {
  static transformFileSize(size: number): string {
    if (isNullOrUndefined(size) || !isNumber(size)) {
      return StringUtil.stringEmpty;
    }
    let extension: string;
    let newSize: number;
    if (size < 1024) {
      newSize = size;
      extension = " B";
    } else if (size < (1024 * 1024)) {
      newSize = size / 1024;
      extension = " KB";
    } else if (size < (1024 * 1024 * 1024)) {
      newSize = size / (1024 * 1024);
      extension = " MB";
    } else if (size < (1024 * 1024 * 1024 * 1024)) {
      newSize = size / (1024 * 1024 * 1024);
      extension = " GB";
    } else {
      newSize = size / (1024 * 1024 * 1024 * 1024);
      extension = " TB";
    }
    return newSize.toFixed(1) + extension;
  }
}
