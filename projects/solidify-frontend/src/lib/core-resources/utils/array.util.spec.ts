/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - array.util.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {ArrayUtil} from "./array.util";

describe("ArrayUtil", () => {
  describe("distinct", () => {
    it("should remove duplicate 'a'", () => {
      const list = ["a", "b", "a"];
      expect(ArrayUtil.distinct(list)).toEqual(["a", "b"]);
    });

    it("should remove duplicate 2", () => {
      const list = [2, 1, 2, 3];
      expect(ArrayUtil.distinct(list)).toEqual([2, 1, 3]);
    });

    it("should remove duplicate 2", () => {
      const list = [
        {
          id: "test1",
          value: "val1",
        },
        {
          id: "test2",
          value: "val2",
        },
        {
          id: "test1",
          value: "val3",
        }];
      expect(ArrayUtil.distinct(list, "id")).toEqual([
        {
          id: "test1",
          value: "val1",
        },
        {
          id: "test2",
          value: "val2",
        }]);
    });
  });
});
