/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - label-translate-interface.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  labelSolidifyAppStatus,
  LabelSolidifyAppStatus,
} from "./app-status/label-translate-app-status.model";
import {
  labelSolidifyApplication,
  LabelSolidifyApplication,
} from "./application/label-translate-application.model";
import {
  LabelSolidifyCodeEditor,
  labelSolidifyCodeEditor,
} from "./code-editor/label-translate-code-editor.model";
import {
  labelSolidifyCore,
  LabelSolidifyCore,
} from "./core/label-translate-core.model";
import {
  labelSolidifyDataTable,
  LabelSolidifyDataTable,
} from "./data-table/label-translate-data-table.model";
import {
  labelSolidifyErrorHandling,
  LabelSolidifyErrorHandling,
} from "./error-handling/label-translate-error-handling.model";
import {
  labelSolidifyFilePreview,
  LabelSolidifyFilePreview,
} from "./file-preview/label-translate-file-preview.model";
import {
  labelSolidifyGlobalBanner,
  LabelSolidifyGlobalBanner,
} from "./global-banner/label-translate-global-banner.model";
import {
  labelSolidifyImage,
  LabelSolidifyImage,
} from "./image/label-translate-image.model";
import {
  labelSolidifyInput,
  LabelSolidifyInput,
} from "./input/label-translate-input.model";
import {
  labelSolidifyOaiPmh,
  LabelSolidifyOaiPmh,
} from "./oai-pmh/label-translate-oai-pmh.model";
import {
  labelSolidifyOauth2,
  LabelSolidifyOauth2,
} from "./oauth2/label-translate-oauth2.model";
import {
  labelSolidifyRichTextEditor,
  LabelSolidifyRichTextEditor,
} from "./rich-text-editor/label-translate-rich-text-editor.model";
import {
  labelSolidifySearch,
  LabelSolidifySearch,
} from "./search/label-translate-search.model";
import {
  labelSolidifySelect,
  LabelSolidifySelect,
} from "./select/label-translate-select.model";
import {
  labelSolidifyTour,
  LabelSolidifyTour,
} from "./tour/label-translate-tour.model";
import {
  labelSolidifyUser,
  LabelSolidifyUser,
} from "./user/label-translate-user.model";

export interface LabelTranslateInterface extends LabelSolidifyApplication,
  LabelSolidifyAppStatus,
  LabelSolidifyCore,
  LabelSolidifyCodeEditor,
  LabelSolidifyDataTable,
  LabelSolidifyErrorHandling,
  LabelSolidifyFilePreview,
  LabelSolidifyGlobalBanner,
  LabelSolidifyImage,
  LabelSolidifyInput,
  LabelSolidifyOauth2,
  LabelSolidifyRichTextEditor,
  LabelSolidifySearch,
  LabelSolidifySelect,
  LabelSolidifyTour,
  LabelSolidifyUser,
  LabelSolidifyOaiPmh {
}

export const labelTranslateInterface: LabelTranslateInterface = {
  ...labelSolidifyApplication,
  ...labelSolidifyAppStatus,
  ...labelSolidifyCodeEditor,
  ...labelSolidifyCore,
  ...labelSolidifyDataTable,
  ...labelSolidifyErrorHandling,
  ...labelSolidifyFilePreview,
  ...labelSolidifyGlobalBanner,
  ...labelSolidifyImage,
  ...labelSolidifyInput,
  ...labelSolidifyOauth2,
  ...labelSolidifyRichTextEditor,
  ...labelSolidifySearch,
  ...labelSolidifySelect,
  ...labelSolidifyTour,
  ...labelSolidifyUser,
  ...labelSolidifyOaiPmh,
};
