/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-async.pipe.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {AsyncPipe} from "@angular/common";
import {
  Pipe,
  PipeTransform,
} from "@angular/core";
import {
  map,
  tap,
} from "rxjs/operators";
import {ObservableOrPromiseOrValue} from "../models/observables/observable-or-promise-or-value.model";
import {isEquality} from "../../core-resources/tools/is/is.tool";
import {observablify} from "../tools/obsersablify.tool";

@Pipe({
  name: "solidifyAsync",
})
export class SolidifyAsyncPipe extends AsyncPipe implements PipeTransform {

  private _source: ObservableOrPromiseOrValue<any> | null | undefined;

  private _latestResult: any | null | undefined;

  private _gotValue: boolean = false;

  transform<T>(source: ObservableOrPromiseOrValue<T> | null | undefined,
               options?: SolidifyAsyncPipeOptions | null | undefined): T | null | undefined {
    const safeOptions = {
      ...this.getDefaultSolidifyAsyncPipeOptions(),
      ...options,
    };
    if (this._source !== source) {
      this._source = source;
      this._gotValue = false;
      this._latestResult = super.transform(observablify(source).pipe(
        map(value => safeOptions.preventFallbackValueConflict && isEquality(value, safeOptions.fallbackValue) ?
          SolidifyAsyncPipe._invertFallbackValue(value as null | undefined) : value),
        tap(() => this._gotValue = true)),
      );
    }
    if (!this._gotValue) {
      this._latestResult = safeOptions.fallbackValue;
    }
    return this._latestResult;
  }

  getDefaultSolidifyAsyncPipeOptions(): SolidifyAsyncPipeOptions {
    return {
      fallbackValue: null,
      preventFallbackValueConflict: true,
    };
  }

  private static _invertFallbackValue(value: null | undefined): null | undefined {
    return value === null ? undefined : null;
  }

}

export interface SolidifyAsyncPipeOptions {
  fallbackValue?: null | undefined;
  preventFallbackValueConflict?: boolean;
}
