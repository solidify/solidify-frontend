/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - datetime.pipe.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  Pipe,
  PipeTransform,
} from "@angular/core";
import {DateUtil} from "../utils/date.util";
import {CoreAbstractPipe} from "./core-abstract/core-abstract.pipe";

@Pipe({
  name: "datetime",
})
export class DatetimePipe extends CoreAbstractPipe implements PipeTransform {
  transform(date: Date, format: "datetime" | "date" | "time" = "datetime"): string {
    if (format === "date") {
      return DateUtil.convertDateToDateString(date);
    }
    if (format === "time") {
      return DateUtil.convertDateToTimeString(date);
    }
    return DateUtil.convertDateToDateTimeString(date);
  }
}
