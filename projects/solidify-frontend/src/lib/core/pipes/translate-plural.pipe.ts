/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - translate-plural.pipe.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  Pipe,
  PipeTransform,
} from "@angular/core";
import {isNonEmptyArray} from "../../core-resources/tools/is/is.tool";

/**
 * Usage in translation json files:
 * - en.json (key: value):
 * "found results": "=0 {No results} =1 {One result} >1 {{{count}} results}",
 * - fr.json (key: value):
 * "found results": "=0 {Pas de résultats} =1 {Un résultat} <5 {{{count}} résultats} else {{{count}} résultats trouvés}"
 *
 * Usage in component:
 * {{'found results' | translate:{count: cnt} | translatePlural:cnt}}
 *
 * Results:
 * If cnt = 0: No results (en) Pas de résultats (fr)
 * If cnt = 7: 7 results found (en) 7 résultats trouvés (fr)
 */
@Pipe({
  name: "translatePlural",
  pure: false,
})
export class TranslatePluralPipe implements PipeTransform {
  private static readonly _OPERATOR_EQUAL: string = "=";
  private static readonly _OPERATOR_LOWER: string = "<";
  private static readonly _OPERATOR_UPPER: string = ">";
  private static readonly _OPERATOR_ELSE: string = "e";
  private static readonly _TYPE_STRING: string = "string";

  transform(text: string, value: string | number): string {
    const match = text.match(/(([=<>][^}]+|else) ?{([^}]+))}/g);
    if (isNonEmptyArray(match)) {
      const ret = match
        .map(m => m.match(/([=<>e]) ?([^{]+) ?{([^}]+)}/))
        .find(f => TranslatePluralPipe._evalCondition(value, f[1], f[2].trim()));
      if (isNonEmptyArray(ret) && ret.length >= 4) {
        return ret[3];
      }
    }
    return text;
  }

  private static _evalCondition(left: number | string, operator: string, right: string): boolean {
    if (operator === this._OPERATOR_ELSE) {
      return true;
    }

    const strings = typeof left === this._TYPE_STRING;
    left = left.toString();
    const leftNumber: number = Number.parseInt(left, 10);
    const rightNumber = Number.parseInt(right, 10);

    if (strings && [this._OPERATOR_LOWER, this._OPERATOR_UPPER].includes(operator)) {
      return false;
    } else if (!strings && (isNaN(leftNumber) || isNaN(rightNumber))) {
      return false;
    }
    switch (operator) {
      case this._OPERATOR_EQUAL:
        return strings ? left === right : leftNumber === rightNumber;
      case this._OPERATOR_LOWER:
        return leftNumber < rightNumber;
      case this._OPERATOR_UPPER:
        return leftNumber > rightNumber;
    }
    return false;
  }
}
