/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - is-function.pipe.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {IsFunctionPipe} from "./is-function.pipe";

describe("IsFunctionPipe", () => {
  const add = (x: number, y: number): number => x + y;

  it("create an instance", () => {
    const pipe = new IsFunctionPipe();
    expect(pipe).toBeTruthy();
  });

  it("assert that value is a function", () => {
    const pipe = new IsFunctionPipe();

    expect(pipe.transform(add(2, 3))).toBe(false);
  });

  it("assert that value is not a function", () => {
    const pipe = new IsFunctionPipe();
    expect(pipe.transform(false)).toBe(false);
  });

});
