/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - is-pipes.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AssertIsNeverPipe} from "./assert-is-never.pipe";
import {IsArrayPipe} from "./is-array.pipe";
import {IsBooleanPipe} from "./is-boolean.pipe";
import {IsDefinedPipe} from "./is-defined.pipe";
import {IsEmptyArrayPipe} from "./is-empty-array.pipe";
import {IsEmptyObjectPipe} from "./is-empty-object.pipe";
import {IsEmptyStringPipe} from "./is-empty-string.pipe";
import {IsEqualityPipe} from "./is-equality.pipe";
import {IsErrorPipe} from "./is-error.pipe";
import {IsFalsePipe} from "./is-false.pipe";
import {IsFalsyPipe} from "./is-falsy.pipe";
import {IsFormArray} from "./is-form-array.pipe";
import {IsFormGroup} from "./is-form-group.pipe";
import {IsFunctionPipe} from "./is-function.pipe";
import {IsInstanceOfPipe} from "./is-instance-of.pipe";
import {IsIterablePipe} from "./is-iterable.pipe";
import {IsMapPipe} from "./is-map.pipe";
import {IsNonEmptyArrayPipe} from "./is-non-empty-array.pipe";
import {IsNonEmptyObjectPipe} from "./is-non-empty-object.pipe";
import {IsNonEmptyStringPipe} from "./is-non-empty-string.pipe";
import {IsNonWhiteStringPipe} from "./is-non-white-string.pipe";
import {IsNotNullNorUndefinedNorEmptyArrayPipe} from "./is-not-null-nor-undefined-nor-empty-array.pipe";
import {IsNotNullNorUndefinedNorEmptyObjectPipe} from "./is-not-null-nor-undefined-nor-empty-object.pipe";
import {IsNotNullNorUndefinedNorEmptyPipe} from "./is-not-null-nor-undefined-nor-empty.pipe";
import {IsNotNullNorUndefinedNorWhiteStringPipe} from "./is-not-null-nor-undefined-nor-white-string.pipe";
import {IsNotNullNorUndefinedPipe} from "./is-not-null-nor-undefined.pipe";
import {IsNotNullPipe} from "./is-not-null.pipe";
import {IsNullOrUndefinedOrEmptyArrayPipe} from "./is-null-or-undefined-or-empty-array.pipe";
import {IsNullOrUndefinedOrEmptyObjectPipe} from "./is-null-or-undefined-or-empty-object.pipe";
import {IsNullOrUndefinedOrEmptyPipe} from "./is-null-or-undefined-or-empty.pipe";
import {IsNullOrUndefinedOrWhiteStringPipe} from "./is-null-or-undefined-or-white-string.pipe";
import {IsNullOrUndefinedPipe} from "./is-null-or-undefined.pipe";
import {IsNullPipe} from "./is-null.pipe";
import {IsNumberFinitePipe} from "./is-number-finite.pipe";
import {IsNumberInfinitePipe} from "./is-number-infinite.pipe";
import {IsNumberInvalidPipe} from "./is-number-invalid.pipe";
import {IsNumberRealPipe} from "./is-number-real.pipe";
import {IsNumberUnrealPipe} from "./is-number-unreal.pipe";
import {IsNumberValidPipe} from "./is-number-valid.pipe";
import {IsNumberPipe} from "./is-number.pipe";
import {IsObjectPipe} from "./is-object.pipe";
import {IsObservablePipe} from "./is-observable.pipe";
import {IsPrimitivePipe} from "./is-primitive.pipe";
import {IsReferencePipe} from "./is-reference.pipe";
import {IsSetPipe} from "./is-set.pipe";
import {IsStringPipe} from "./is-string.pipe";
import {IsSymbolPipe} from "./is-symbol.pipe";
import {IsTruePipe} from "./is-true.pipe";
import {IsTruthyObjectPipe} from "./is-truthy-object.pipe";
import {IsTruthyPipe} from "./is-truthy.pipe";
import {IsUndefinedPipe} from "./is-undefined.pipe";
import {IsWhiteStringPipe} from "./is-white-string.pipe";

export const isPipes = [
  IsArrayPipe,
  IsBooleanPipe,
  IsFalsePipe,
  IsFunctionPipe,
  IsNonEmptyArrayPipe,
  IsNonEmptyObjectPipe,
  IsNonEmptyStringPipe,
  IsNullOrUndefinedPipe,
  IsNullOrUndefinedOrEmptyPipe,
  IsNullOrUndefinedOrEmptyArrayPipe,
  IsNullOrUndefinedOrEmptyObjectPipe,
  IsNullOrUndefinedOrWhiteStringPipe,
  IsNumberPipe,
  IsNumberFinitePipe,
  IsNumberInvalidPipe,
  IsNumberValidPipe,
  IsObservablePipe,
  IsTruePipe,
  IsTruthyPipe,
  IsTruthyObjectPipe,
  IsFalsyPipe,
  IsFormArray,
  IsFormGroup,
  IsNotNullPipe,
  IsNotNullNorUndefinedPipe,
  IsNotNullNorUndefinedNorEmptyPipe,
  IsNotNullNorUndefinedNorEmptyArrayPipe,
  IsNotNullNorUndefinedNorEmptyObjectPipe,
  IsNotNullNorUndefinedNorWhiteStringPipe,
  IsNumberInfinitePipe,
  IsNumberRealPipe,
  IsNumberUnrealPipe,
  IsObjectPipe,
  IsEmptyArrayPipe,
  IsIterablePipe,
  IsMapPipe,
  IsSetPipe,
  IsNullPipe,
  IsStringPipe,
  IsEmptyObjectPipe,
  IsEmptyStringPipe,
  IsWhiteStringPipe,
  IsNonWhiteStringPipe,
  IsSymbolPipe,
  IsUndefinedPipe,
  IsDefinedPipe,
  IsReferencePipe,
  IsPrimitivePipe,
  IsInstanceOfPipe,
  IsErrorPipe,
  IsEqualityPipe,
  AssertIsNeverPipe,
];
