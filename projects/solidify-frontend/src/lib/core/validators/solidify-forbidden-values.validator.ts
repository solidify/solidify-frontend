/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-forbidden-values.validator.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AbstractControl,
  ValidationErrors,
  ValidatorFn,
} from "@angular/forms";
import {TranslateService} from "@ngx-translate/core";
import {
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isString,
  isTrue,
} from "../../core-resources/tools/is/is.tool";
import {MappingObjectUtil} from "../../core-resources/utils/mapping-object.util";
import {ValidationDirective} from "../directives/validation/validation.directive";
import {FormValidationHelper} from "../helpers/form-validation.helper";

export const SolidifyForbiddenValuesValidator = (translateService: TranslateService, listForbidden: string[], labelToTranslate: string, caseInsensitive: boolean = false): ValidatorFn => (control: AbstractControl): ValidationErrors | null => {
  const KEY_NOT_THIS_VALUE: string = "notThisValue";
  const value = isTrue(caseInsensitive) && isString(control.value) ? control.value.toLowerCase() : control.value;

  let metadataErrors = FormValidationHelper.getMetadataErrors(control);
  if (isNullOrUndefined(metadataErrors)) {
    metadataErrors = FormValidationHelper.initMetadataErrors(control);
  }

  if (isNotNullNorUndefinedNorWhiteString(value)) {
    MappingObjectUtil.delete(metadataErrors, ValidationDirective.ERROR_REQUIRED);
  }

  if (listForbidden.filter(v => (isTrue(caseInsensitive) && isString(v) && v.toLowerCase() === value) || v === value).length > 0) {
    MappingObjectUtil.set(metadataErrors, KEY_NOT_THIS_VALUE, isNotNullNorUndefined(translateService) ? translateService.instant(labelToTranslate) : labelToTranslate);
    return {[KEY_NOT_THIS_VALUE]: true};
  } else {
    MappingObjectUtil.delete(metadataErrors, KEY_NOT_THIS_VALUE);
    return {};
  }
};
