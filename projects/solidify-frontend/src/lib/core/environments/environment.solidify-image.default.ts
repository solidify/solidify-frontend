/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-image.default.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {ImageDisplayModePartialEnum} from "../../image/enums/image-display-mode-partial.enum";
import {DefaultSolidifyImageEnvironment} from "./environment.solidify-image";

export const defaultSolidifyImageEnvironment: DefaultSolidifyImageEnvironment = {
  avatarSizeDefault: 300,
  avatarSizeMapping: {
    [ImageDisplayModePartialEnum.MAIN]: 300,
    [ImageDisplayModePartialEnum.UPLOAD]: 145,
    [ImageDisplayModePartialEnum.IN_TILE]: 145,
    [ImageDisplayModePartialEnum.IN_OVERLAY]: 125,
    [ImageDisplayModePartialEnum.IN_LIST]: 45,
    [ImageDisplayModePartialEnum.NEXT_FORM_FIELD]: 45,
  },
  imageSizeDefault: 400,
  imageSizeMapping: {
    [ImageDisplayModePartialEnum.MAIN]: 400,
    [ImageDisplayModePartialEnum.UPLOAD]: 200,
    [ImageDisplayModePartialEnum.IN_TILE]: 200,
    [ImageDisplayModePartialEnum.IN_OVERLAY]: 125,
    [ImageDisplayModePartialEnum.IN_LIST]: 50,
    [ImageDisplayModePartialEnum.NEXT_FORM_FIELD]: 50,
  },
  uploadImageDefaultAspectRatio: undefined,
  uploadImageDefaultResizeToHeight: 300 * 2,
  uploadImageDefaultResizeToWidth: 300 * 2,
  uploadAvatarDefaultAspectRatio: 1,
  uploadAvatarDefaultResizeToHeight: 145 * 2,
  uploadAvatarDefaultResizeToWidth: 145 * 2,
};
