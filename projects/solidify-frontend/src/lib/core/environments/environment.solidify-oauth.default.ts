/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-oauth.default.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {DefaultSolidifyOauthEnvironment} from "./environment.solidify-oauth";

export const defaultSolidifyOauthEnvironment: DefaultSolidifyOauthEnvironment = {
  // OAuth2 properties
  computeOAuthEndpoint: false,
  authorization: "http://localhost:4200",
  callExternalLogOut: true,
  authorizationExternalLogOutSuffixUrl: "/Shibboleth.sso/Logout",
  externalLogOutTimeoutCloseTab: 100,
  oidc: false,
  requestAccessToken: true,
  dummyClientSecret: "123abc",
  dummyClientMfaSecret: "123abc",
  clientId: "local-dev-angular",
  clientIdMfa: undefined,
  responseType: "token",
  requireHttps: false,
  allowedUrls: [],
  redirectUrl: SsrUtil.window?.location.origin,
  timeoutFactor: 0.75,
  postLogoutRedirectUri: "",
  logoutUrl: "",
  nonceStateSeparator: ";",
  disableNonceCheck: false,
  sendAccessToken: true,
  tokenInMemoryStorage: false,
};
