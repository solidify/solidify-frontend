/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-defaults.default.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {defaultSolidifyAppStatusEnvironment} from "./environment.solidify-app-status.default";
import {defaultSolidifyCodeEditorEnvironment} from "./environment.solidify-code-editor.default";
import {defaultSolidifyComponentEnvironment} from "./environment.solidify-component.default";
import {defaultSolidifyCoreAppEnvironment} from "./environment.solidify-core.default";
import {defaultSolidifyDataTableEnvironment} from "./environment.solidify-data-table.default";
import {DefaultSolidifyEnvironment} from "./environment.solidify-defaults";
import {defaultSolidifyDownloadEnvironment} from "./environment.solidify-download.default";
import {defaultSolidifyErrorHandlerEnvironment} from "./environment.solidify-error-handler.default";
import {defaultSolidifyFilePreviewEnvironment} from "./environment.solidify-file-preview.default";
import {defaultSolidifyGlobalBannerEnvironment} from "./environment.solidify-global-banner.default";
import {defaultSolidifyImageEnvironment} from "./environment.solidify-image.default";
import {defaultSolidifyNotificationEnvironment} from "./environment.solidify-notification.default";
import {defaultSolidifyOaiPmhEnvironment} from "./environment.solidify-oai-pmh.default";
import {defaultSolidifyOauthEnvironment} from "./environment.solidify-oauth.default";
import {defaultSolidifySearchEnvironment} from "./environment.solidify-search.default";
import {defaultSolidifySpinnerEnvironment} from "./environment.solidify-spinner.default";
import {defaultSolidifyTwitterEnvironment} from "./environment.solidify-twitter.default";
import {defaultSolidifyUploadEnvironment} from "./environment.solidify-upload.default";

export const defaultSolidifyEnvironment: DefaultSolidifyEnvironment = {
  ...defaultSolidifyAppStatusEnvironment,
  ...defaultSolidifyCoreAppEnvironment,
  ...defaultSolidifyCodeEditorEnvironment,
  ...defaultSolidifyOauthEnvironment,
  ...defaultSolidifyNotificationEnvironment,
  ...defaultSolidifyOaiPmhEnvironment,
  ...defaultSolidifyErrorHandlerEnvironment,
  ...defaultSolidifySpinnerEnvironment,
  ...defaultSolidifyComponentEnvironment,
  ...defaultSolidifyImageEnvironment,
  ...defaultSolidifyUploadEnvironment,
  ...defaultSolidifyDataTableEnvironment,
  ...defaultSolidifyDownloadEnvironment,
  ...defaultSolidifyFilePreviewEnvironment,
  ...defaultSolidifyGlobalBannerEnvironment,
  ...defaultSolidifyTwitterEnvironment,
  ...defaultSolidifySearchEnvironment,
};
