/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-search.default.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DefaultSolidifyApplicationEnvironment} from "../../application/environments/environment.solidify-application";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {ApiResourceNamePartialEnum} from "../enums/partial/api-resource-name-partial.enum";
import {AppRoutesPartialEnum} from "../enums/partial/app-routes-partial.enum";
import {DefaultSolidifySearchEnvironment} from "./environment.solidify-search";

const routeAdminIndexFieldAlias: string = `${AppRoutesPartialEnum.admin}/${AppRoutesPartialEnum.indexFieldAlias}`;
const routeAdminIndexFieldAliasDetail: string = `${routeAdminIndexFieldAlias}/${AppRoutesPartialEnum.indexFieldAliasDetail}`;

export const defaultSolidifySearchEnvironment: DefaultSolidifySearchEnvironment = {
  searchFacets: undefined,
  searchFacetValuesTranslations: undefined,
  indexFieldAliasRouteRedirectUrlAfterSuccessCreateAction: (resId: string) => `${routeAdminIndexFieldAliasDetail}/${resId}`,
  indexFieldAliasRouteRedirectUrlAfterSuccessUpdateAction: (resId: string) => `${routeAdminIndexFieldAliasDetail}/${resId}`,
  indexFieldAliasRouteRedirectUrlAfterSuccessDeleteAction: routeAdminIndexFieldAlias,
  routeAdminIndexFieldAlias: routeAdminIndexFieldAlias,
  routeAdminIndexFieldAliasDetail: routeAdminIndexFieldAliasDetail,
  routeAdminIndexFieldAliasCreate: `${routeAdminIndexFieldAlias}/${AppRoutesPartialEnum.indexFieldAliasCreate}`,
  apiSearchIndexFieldAlias: (environment: DefaultSolidifyApplicationEnvironment) => environment.index + SOLIDIFY_CONSTANTS.SEPARATOR + ApiResourceNamePartialEnum.INDEX_FIELD_ALIASES,
  index: undefined,
};
