/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-defaults.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DefaultSolidifyAppStatusEnvironment} from "./environment.solidify-app-status";
import {DefaultSolidifyCodeEditorEnvironment} from "./environment.solidify-code-editor";
import {DefaultSolidifyComponentEnvironment} from "./environment.solidify-component";
import {CoreSolidifyEnvironment} from "./environment.solidify-core";
import {DefaultSolidifyDataTableEnvironment} from "./environment.solidify-data-table";
import {DefaultSolidifyDownloadEnvironment} from "./environment.solidify-download";
import {DefaultSolidifyErrorHandlerEnvironment} from "./environment.solidify-error-handler";
import {DefaultSolidifyFilePreviewEnvironment} from "./environment.solidify-file-preview";
import {DefaultSolidifyGlobalBannerEnvironment} from "./environment.solidify-global-banner";
import {DefaultSolidifyImageEnvironment} from "./environment.solidify-image";
import {DefaultSolidifyNotificationEnvironment} from "./environment.solidify-notification";
import {DefaultSolidifyOaiPmhEnvironment} from "./environment.solidify-oai-pmh";
import {DefaultSolidifyOauthEnvironment} from "./environment.solidify-oauth";
import {DefaultSolidifySearchEnvironment} from "./environment.solidify-search";
import {DefaultSolidifySpinnerEnvironment} from "./environment.solidify-spinner";
import {DefaultSolidifyTwitterEnvironment} from "./environment.solidify-twitter";
import {DefaultSolidifyUploadEnvironment} from "./environment.solidify-upload";

export interface DefaultSolidifyEnvironment extends CoreSolidifyEnvironment,
  DefaultSolidifyAppStatusEnvironment,
  DefaultSolidifyOauthEnvironment,
  DefaultSolidifyCodeEditorEnvironment,
  DefaultSolidifyNotificationEnvironment,
  DefaultSolidifyOaiPmhEnvironment,
  DefaultSolidifyErrorHandlerEnvironment,
  DefaultSolidifySpinnerEnvironment,
  DefaultSolidifyComponentEnvironment,
  DefaultSolidifyImageEnvironment,
  DefaultSolidifyUploadEnvironment,
  DefaultSolidifyDataTableEnvironment,
  DefaultSolidifyDownloadEnvironment,
  DefaultSolidifyFilePreviewEnvironment,
  DefaultSolidifyGlobalBannerEnvironment,
  DefaultSolidifyTwitterEnvironment,
  DefaultSolidifySearchEnvironment {
}
