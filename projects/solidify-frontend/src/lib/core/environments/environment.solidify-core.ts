/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-core.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Type} from "@angular/core";
import {
  FloatLabelType,
  MatFormFieldAppearance,
  SubscriptSizing,
} from "@angular/material/form-field";
import {MetaDefinition} from "@angular/platform-browser";
import {MappingObject} from "../../core-resources/types/mapping-type.type";
import {ApiActionNamePartialEnum} from "../enums/partial/api-action-name-partial.enum";
import {LanguagePartialEnum} from "../enums/partial/language-partial.enum";
import {ThemePartialEnum} from "../enums/partial/theme-partial.enum";
import {KeyValue} from "../../core-resources/models/key-value.model";
import {SolidifyAppStateModel} from "../stores/abstract/app/app-state.model";
import {SolidifyAppState} from "../stores/abstract/app/app.state";
import {ExtendEnum} from "../../core-resources/types/extend-enum.type";

export interface CoreSolidifyEnvironment {
  production: boolean;
  isDemoMode: boolean;
  forceRuntimeOverrideSetting: boolean;
  intervalRefreshRuntimeOverrideSetting: number;
  showDebugInformation: boolean;
  forceServiceWorker: boolean;
  serviceWorkerFileName: string;
  baseHref: string;
  appLanguages: ExtendEnum<LanguagePartialEnum>[];
  defaultLanguage: ExtendEnum<LanguagePartialEnum>;
  appLanguagesTranslate: KeyValue[];
  appThemes: ExtendEnum<ThemePartialEnum>[];
  appThemesTranslate: KeyValue[];
  theme: ExtendEnum<ThemePartialEnum>;
  themeName: string;
  metaThemeColor: string;
  darkMode: boolean;
  highlightJsThemeLight: string;
  highlightJsThemeDark: string;
  reloadAfterLogout: boolean;
  appState: Type<SolidifyAppState<SolidifyAppStateModel>>;
  routeHomePage: string;
  routeSegmentEdit: string;
  defaultStickyDatatableHeight: number;
  maintenanceMode: boolean;
  appTitle: string;
  appDescription: string;
  imageOpenGraph: string;
  elasticApmEnabled: boolean;
  elasticApmUrl: string;
  elasticApmSendErrorsErrors: boolean;
  googleAnalyticsId: string;
  appearanceInputMaterial: MatFormFieldAppearance;
  positionLabelInputMaterial: FloatLabelType;
  subscriptSizingInputMaterial: SubscriptSizing;
  displayEmptyRequiredFieldInError: boolean;
  listTagsComponentsWhereClickAllowToEnterInEditMode: string[];
  ribbonEnabled: boolean;
  ribbonColor: string;
  ribbonText: string;
  minimalPageSizeForPageInfos: number;
  defaultEnumValuePageSizeOption: number;
  pollingMaxIntervalInSecond: number;
  modalWidth: string;
  actionToDoWhenUpdateAvailable: "do-nothing" | "prompt" | "refresh";
  orcidUrl: string;
  orcidQueryParam: string;
  orcidErrorQueryParam: string;
  orcidErrorDescriptionQueryParam: string;
  frontendVersionFile: string;
  overlayDelayInMs: number;
  loadBackendModulesInfo: boolean;
  refreshGlobalBannerIntervalInSecond: number;
  labelEnumConverter: ((currentLanguage: ExtendEnum<LanguagePartialEnum>) => string) | undefined;
  displayPrivacyPolicyAndTermsOfUseApprovalDialog: boolean;
  versionPrivacyPolicyAndTermsOfUse: string;
  privacyPolicyLink: MappingObject<ExtendEnum<LanguagePartialEnum>, string>;
  termsOfUseLink: MappingObject<ExtendEnum<LanguagePartialEnum>, string>;
  aboutModulesToIgnore: string[];
  resourceFileApiActionNameUploadLogo: ExtendEnum<ApiActionNamePartialEnum>;
  resourceFileApiActionNameDownloadLogo: ExtendEnum<ApiActionNamePartialEnum>;
  resourceFileApiActionNameDeleteLogo: ExtendEnum<ApiActionNamePartialEnum>;
  resourceFileApiActionNameUploadAvatar: ExtendEnum<ApiActionNamePartialEnum>;
  resourceFileApiActionNameDownloadAvatar: ExtendEnum<ApiActionNamePartialEnum>;
  resourceFileApiActionNameDeleteAvatar: ExtendEnum<ApiActionNamePartialEnum>;
  resourceFileApiActionNameUploadThumbnail: ExtendEnum<ApiActionNamePartialEnum>;
  resourceFileApiActionNameDownloadThumbnail: ExtendEnum<ApiActionNamePartialEnum>;
  resourceFileApiActionNameDeleteThumbnail: ExtendEnum<ApiActionNamePartialEnum>;
  ssrUrlModuleRewrite: MappingObject<string, string>;
  /**
   * Allows you to define the file extensions for which processing will take place to add the real extension to the downloaded file name.
   * Example ["dua", "thumbnail"]
   */
  fileExtensionToCompleteWithRealExtensionOnDownload: string[];
  /**
   * List of metas injected dynamically
   * ex: Usefull for google-site-verification
   */
  listMetas: MetaDefinition[];
  useTransferStateForTranslation: boolean;
}
