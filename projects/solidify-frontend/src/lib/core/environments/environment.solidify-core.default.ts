/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-core.default.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {ApiActionNamePartialEnum} from "../enums/partial/api-action-name-partial.enum";
import {LanguagePartialEnum} from "../enums/partial/language-partial.enum";
import {CoreSolidifyEnvironment} from "./environment.solidify-core";

export const defaultSolidifyCoreAppEnvironment: CoreSolidifyEnvironment = {
  production: false,
  isDemoMode: false,
  showDebugInformation: true,
  forceRuntimeOverrideSetting: false,
  intervalRefreshRuntimeOverrideSetting: 60000,
  forceServiceWorker: false,
  serviceWorkerFileName: "ngsw-worker.js",
  baseHref: "/",
  appLanguages: [LanguagePartialEnum.en],
  defaultLanguage: LanguagePartialEnum.en,
  appLanguagesTranslate: [],
  appThemes: [],
  appThemesTranslate: [],
  theme: undefined,
  themeName: undefined,
  metaThemeColor: "#000000",
  darkMode: false,
  highlightJsThemeLight: "./assets/highlightjs/light.css",
  highlightJsThemeDark: "./assets/highlightjs/dark.css",
  reloadAfterLogout: false,
  appState: undefined,
  routeHomePage: "home",
  routeSegmentEdit: "edit",
  defaultStickyDatatableHeight: 40,
  maintenanceMode: false,
  appTitle: "App build with Solidify",
  appDescription: "Angular app based on Solidify UNIGE Librairy",
  imageOpenGraph: undefined,
  elasticApmEnabled: false,
  elasticApmUrl: undefined,
  elasticApmSendErrorsErrors: true,
  googleAnalyticsId: undefined,
  appearanceInputMaterial: "outline",
  positionLabelInputMaterial: "auto",
  subscriptSizingInputMaterial: "fixed",
  displayEmptyRequiredFieldInError: true,
  listTagsComponentsWhereClickAllowToEnterInEditMode: [],
  ribbonEnabled: true,
  ribbonColor: "#e60000",
  ribbonText: "Dev",
  minimalPageSizeForPageInfos: 1,
  defaultEnumValuePageSizeOption: SOLIDIFY_CONSTANTS.DEFAULT_ENUM_VALUE_PAGE_SIZE_OPTION,
  pollingMaxIntervalInSecond: 60,
  modalWidth: "90%",
  actionToDoWhenUpdateAvailable: "prompt",
  orcidUrl: "http://orcid.org",
  orcidQueryParam: "orcid",
  orcidErrorQueryParam: "orcid_error",
  orcidErrorDescriptionQueryParam: "orcid_error_description",
  frontendVersionFile: "frontend-version.json",
  overlayDelayInMs: 500,
  loadBackendModulesInfo: true,
  refreshGlobalBannerIntervalInSecond: 60,
  labelEnumConverter: undefined,
  displayPrivacyPolicyAndTermsOfUseApprovalDialog: false,
  versionPrivacyPolicyAndTermsOfUse: undefined,
  privacyPolicyLink: undefined,
  termsOfUseLink: undefined,
  aboutModulesToIgnore: [],
  resourceFileApiActionNameUploadLogo: ApiActionNamePartialEnum.UPLOAD_LOGO,
  resourceFileApiActionNameDownloadLogo: ApiActionNamePartialEnum.DOWNLOAD_LOGO,
  resourceFileApiActionNameDeleteLogo: ApiActionNamePartialEnum.DELETE_LOGO,
  resourceFileApiActionNameUploadAvatar: ApiActionNamePartialEnum.UPLOAD_AVATAR,
  resourceFileApiActionNameDownloadAvatar: ApiActionNamePartialEnum.DOWNLOAD_AVATAR,
  resourceFileApiActionNameDeleteAvatar: ApiActionNamePartialEnum.DELETE_AVATAR,
  resourceFileApiActionNameUploadThumbnail: ApiActionNamePartialEnum.UPLOAD_THUMBNAIL,
  resourceFileApiActionNameDownloadThumbnail: ApiActionNamePartialEnum.DOWNLOAD_THUMBNAIL,
  resourceFileApiActionNameDeleteThumbnail: ApiActionNamePartialEnum.DELETE_THUMBNAIL,
  ssrUrlModuleRewrite: undefined,
  fileExtensionToCompleteWithRealExtensionOnDownload: [],
  listMetas: [],
  useTransferStateForTranslation: false,
};
