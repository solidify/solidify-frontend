/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-oai-pmh.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

export interface DefaultSolidifyOaiPmhEnvironment {
  /**
   * Automatically set by backend
   */
  oaiInfo: string;
  /**
   * Automatically set by backend
   */
  oaiPmh: string | undefined;
  /**
   * Allow to use an oaiPmh short url more user-friendly
   * (Example: https://yareta.unige.ch/oai, wll use this short url instead the following full url provided by backend https://access.yareta.unige.ch/accession/oai-info/oai-provider/oai)
   */
  oaiPmhShortcutUrl: string | undefined;
  oaiSetRouteRedirectUrlAfterSuccessCreateAction: (resId: string) => string;
  oaiSetRouteRedirectUrlAfterSuccessUpdateAction: (resId: string) => string;
  oaiSetRouteRedirectUrlAfterSuccessDeleteAction: string;
  oaiMetadataPrefixRouteRedirectUrlAfterSuccessCreateAction: (resId: string) => string;
  oaiMetadataPrefixRouteRedirectUrlAfterSuccessUpdateAction: (resId: string) => string;
  oaiMetadataPrefixRouteRedirectUrlAfterSuccessDeleteAction: string;
  apiOaiInfoOaiSets: (environment: DefaultSolidifyOaiPmhEnvironment) => string;
  apiOaiInfoOaiMetadataPrefixes: (environment: DefaultSolidifyOaiPmhEnvironment) => string;
  routeAdminOaiSet: string;
  routeAdminOaiSetDetail: string;
  routeAdminOaiSetCreate: string;
  routeAdminOaiMetadataPrefix: string;
  routeAdminOaiMetadataPrefixDetail: string;
  routeAdminOaiMetadataPrefixCreate: string;
  oaiProviderEndPath: string;
  /**
   * Suffix to add to 'oaiInfo' url to obtain 'oaiPmh' url
   */
  oaiInfoProviderSuffixPath: string;
  /**
   * Url param like smartView
   */
  oaiProviderCustomParam: string | undefined;
}
