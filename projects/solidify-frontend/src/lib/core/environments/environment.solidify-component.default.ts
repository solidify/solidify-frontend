/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-component.default.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {DefaultSolidifyComponentEnvironment} from "./environment.solidify-component";

export const defaultSolidifyComponentEnvironment: DefaultSolidifyComponentEnvironment = {
  classInputHide: "hidden-input",
  classInputIgnored: "ignored-input",
  classBypassEdit: "bypass-edit",
  defaultEnumValuePageSizeLazyLoad: 20,
  timeBeforeDisplayTooltip: 680,

  // DATATABLE
  pageSizeOptions: [5, 10, 25, 50, 100],
  cdkDropListIdPrefix: "cdkDropList-",
  maximalPageSizeToRetrievePaginationInfo: 2000,
  minimalPageSizeToRetrievePaginationInfo: 1,
  timeBeforeDisplayTooltipOnDataTable: 500,
  defaultPageSize: 10,

  // Breakpoints
  breakpointXs: "0px",
  breakpointSm: "576px",
  breakpointMd: "992px",
  breakpointLg: "1360px",
};
