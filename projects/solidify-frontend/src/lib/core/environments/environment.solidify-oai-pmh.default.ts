/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-oai-pmh.default.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {ApiResourceNamePartialEnum} from "../enums/partial/api-resource-name-partial.enum";
import {AppRoutesPartialEnum} from "../enums/partial/app-routes-partial.enum";
import {DefaultSolidifyOaiPmhEnvironment} from "./environment.solidify-oai-pmh";

const routeAdminOaiSet: string = `${AppRoutesPartialEnum.admin}/${AppRoutesPartialEnum.oaiSet}`;
const routeAdminOaiSetDetail: string = `${routeAdminOaiSet}/${AppRoutesPartialEnum.oaiSetDetail}`;

const routeAdminOaiMetadataPrefix: string = `${AppRoutesPartialEnum.admin}/${AppRoutesPartialEnum.oaiMetadataPrefix}`;
const routeAdminOaiMetadataPrefixDetail: string = `${routeAdminOaiMetadataPrefix}/${AppRoutesPartialEnum.oaiMetadataPrefixDetail}`;
export const defaultSolidifyOaiPmhEnvironment: DefaultSolidifyOaiPmhEnvironment = {
  oaiProviderEndPath: "?verb=Identify",
  oaiInfoProviderSuffixPath: "/oai-provider/oai",
  oaiProviderCustomParam: undefined,
  routeAdminOaiMetadataPrefix: routeAdminOaiMetadataPrefix,
  routeAdminOaiMetadataPrefixCreate: `${routeAdminOaiMetadataPrefix}/${AppRoutesPartialEnum.oaiMetadataPrefixCreate}`,
  routeAdminOaiMetadataPrefixDetail: routeAdminOaiMetadataPrefixDetail,
  routeAdminOaiSet: routeAdminOaiSet,
  routeAdminOaiSetCreate: `${routeAdminOaiSet}/${AppRoutesPartialEnum.oaiSetCreate}`,
  routeAdminOaiSetDetail: routeAdminOaiSetDetail,
  oaiInfo: "URL-TO-OAI-INFO",
  oaiPmh: undefined,
  oaiPmhShortcutUrl: undefined,
  apiOaiInfoOaiSets: (environment: DefaultSolidifyOaiPmhEnvironment) => environment.oaiInfo + SOLIDIFY_CONSTANTS.SEPARATOR + ApiResourceNamePartialEnum.OAI_SETS,
  apiOaiInfoOaiMetadataPrefixes: (environment: DefaultSolidifyOaiPmhEnvironment) => environment.oaiInfo + SOLIDIFY_CONSTANTS.SEPARATOR + ApiResourceNamePartialEnum.OAI_METADATA_PREFIXES,
  oaiSetRouteRedirectUrlAfterSuccessCreateAction: (resId: string) => `${routeAdminOaiSetDetail}/${resId}`,
  oaiSetRouteRedirectUrlAfterSuccessUpdateAction: (resId: string) => `${routeAdminOaiSetDetail}/${resId}`,
  oaiSetRouteRedirectUrlAfterSuccessDeleteAction: routeAdminOaiSet,
  oaiMetadataPrefixRouteRedirectUrlAfterSuccessCreateAction: (resId: string) => `${routeAdminOaiMetadataPrefixDetail}/${resId}`,
  oaiMetadataPrefixRouteRedirectUrlAfterSuccessUpdateAction: (resId: string) => `${routeAdminOaiMetadataPrefixDetail}/${resId}`,
  oaiMetadataPrefixRouteRedirectUrlAfterSuccessDeleteAction: routeAdminOaiMetadataPrefix,
};
