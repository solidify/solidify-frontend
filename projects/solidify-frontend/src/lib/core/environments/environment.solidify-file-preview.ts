/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-file-preview.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MappingObject} from "../../core-resources/types/mapping-type.type";
import {FileStatusEnum} from "../enums/upload/file-status.enum";

export interface DefaultSolidifyFilePreviewEnvironment {
  visualizationStatusThatAllowPreview: FileStatusEnum[];
  visualizationDefaultMaxFileSizeInMegabytes: number;

  // tiff constant
  visualizationTiffExtensions: string[];
  visualizationTiffContentType: string[];
  visualizationTiffMimeType: string[];
  visualizationTiffPronomId: string[];
  visualizationTiffMaxFileSizeInMegabytes: number | undefined;

  // image constant
  visualizationImageExtensions: string[];
  visualizationImageContentType: string[];
  visualizationImageMimeType: string[];
  visualizationImagePronomId: string[];
  visualizationImageMaxFileSizeInMegabytes: number | undefined;

  // movie constant for all movie
  visualizationMovieExtensions: string[];
  visualizationMovieContentType: string[];
  visualizationMovieMimeType: string[];
  visualizationMoviePronomId: string[];
  visualizationMovieMaxFileSizeInMegabytes: number | undefined;

  // movie constant
  visualizationClassicalMovieExtensions: string[];
  visualizationClassicalMovieContentType: string[];
  visualizationClassicalMovieMimeType: string[];
  visualizationClassicalMoviePronomId: string[];
  visualizationClassicalMovieMaxFileSizeInMegabytes: number | undefined;

  // pdf constant
  visualizationPdfExtensions: string[];
  visualizationPdfContentType: string[];
  visualizationPdfMimeType: string[];
  visualizationPdfPronomId: string[];
  visualizationPdfMaxFileSizeInMegabytes: number | undefined;

  // docx
  visualizationDocxPreviewExtensions: string[];
  visualizationDocxPreviewContentType: string[];
  visualizationDocxPreviewMimeType: string[];
  visualizationDocxPreviewPronomId: string[];
  visualizationDocxPreviewMaxFileSizeInMegabytes: number | undefined;

  // xlsx
  visualizationXlsxPreviewExtensions: string[];
  visualizationXlsxPreviewContentType: string[];
  visualizationXlsxPreviewMimeType: string[];
  visualizationXlsxPreviewPronomId: string[];
  visualizationXlsxPreviewMaxFileSizeInMegabytes: number | undefined;

  // xlsx
  visualizationCsvPreviewExtensions: string[];
  visualizationCsvPreviewContentType: string[];
  visualizationCsvPreviewMimeType: string[];
  visualizationCsvPreviewPronomId: string[];
  visualizationCsvPreviewMaxFileSizeInMegabytes: number | undefined;

  // sound constant
  visualizationSoundExtensions: string[];
  visualizationSoundContentType: string[];
  visualizationSoundMimeType: string[];
  visualizationSoundPronomId: string[];
  visualizationSoundMaxFileSizeInMegabytes: number | undefined;

  // sme constant
  visualizationSmeExtensions: string[];
  visualizationSmeContentType: string[];
  visualizationSmeMimeType: string[];
  visualizationSmePronomId: string[];
  visualizationSmeMaxFileSizeInMegabytes: number | undefined;

  // text constant
  visualizationTextFileExtensions: string[];
  visualizationTextFileContentType: string[];
  visualizationTextFileMimeType: string[];
  visualizationTextFilePronomId: string[];
  visualizationTextFileMaxFileSizeInMegabytes: number | undefined;
  visualizationTextFileHighlightLanguages: string[];
  visualizationTextFileHighlightMaxFileSizeInMegabytes: number | undefined;
  /**
   * Define for each extension the size limit for highlight
   * Value is size limit in Megabyte
   * If no value found, use visualizationTextFileHighlightMaxFileSizeInMegabytes as default value
   */
  visualizationTextFileHighlightMaxFileSizeInMegabytesByExtensions: MappingObject<string, number | undefined>;

  // markdown constant
  visualizationMarkdownExtensions: string[];
  visualizationMarkdownContentType: string[];
  visualizationMarkdownMimeType: string[];
  visualizationMarkdownPronomId: string[];
  visualizationMarkdownMaxFileSizeInMegabytes: number | undefined;
}
