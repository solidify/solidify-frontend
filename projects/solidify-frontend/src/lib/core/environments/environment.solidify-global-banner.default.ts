/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-global-banner.default.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {ApiResourceNamePartialEnum} from "../enums/partial/api-resource-name-partial.enum";
import {AppRoutesPartialEnum} from "../enums/partial/app-routes-partial.enum";
import {LanguagePartialEnum} from "../enums/partial/language-partial.enum";
import {LabelUtil} from "../utils/label.util";
import {DefaultSolidifyGlobalBannerEnvironment} from "./environment.solidify-global-banner";

const routeAdminGlobalBanner: string = `${AppRoutesPartialEnum.admin}/${AppRoutesPartialEnum.globalBanner}`;
const routeAdminGlobalBannerDetail: string = `${routeAdminGlobalBanner}/${AppRoutesPartialEnum.globalBannerDetail}`;

export const defaultSolidifyGlobalBannerEnvironment: DefaultSolidifyGlobalBannerEnvironment = {
  admin: undefined,
  routeAdminGlobalBanner: routeAdminGlobalBanner,
  routeAdminGlobalBannerCreate: `${routeAdminGlobalBanner}/${AppRoutesPartialEnum.globalBannerCreate}`,
  routeAdminGlobalBannerDetail: routeAdminGlobalBannerDetail,
  apiGlobalBanner: (environment: DefaultSolidifyGlobalBannerEnvironment) => environment.admin + SOLIDIFY_CONSTANTS.SEPARATOR + ApiResourceNamePartialEnum.GLOBAL_BANNERS,
  globalBannerRouteRedirectUrlAfterSuccessCreateAction: (resId: string) => `${routeAdminGlobalBannerDetail}/${resId}`,
  globalBannerRouteRedirectUrlAfterSuccessUpdateAction: (resId: string) => `${routeAdminGlobalBannerDetail}/${resId}`,
  globalBannerRouteRedirectUrlAfterSuccessDeleteAction: routeAdminGlobalBanner,
  globalBannerLanguageKeyFr: "fr" as LanguagePartialEnum,
  globalBannerLanguageKeyEn: "en" as LanguagePartialEnum,
  globalBannerGetTranslationFromListLabels: LabelUtil.getTranslationFromListLabels,
};
