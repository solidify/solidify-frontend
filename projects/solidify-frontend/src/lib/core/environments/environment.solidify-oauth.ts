/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-oauth.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


export interface DefaultSolidifyOauthEnvironment {
  /**
   * If true, the authorization endpoint will be computed from value retrieve on admin/modules backend endpoint
   * If false, the authorization endpoint will be the value of authorization property set manually
   */
  computeOAuthEndpoint: boolean;
  /**
   * The authorization endpoint
   */
  authorization?: string | undefined; // "http://localhost:4200" or "https://dlcm-serveur.unige.ch/authorization" (can be retrieve from backend if computeOAuthEndpoint is true)
  /**
   * If true, the external logout will be called when the user logout from the application or during MFA login process
   * If false, the external logout will not be called
   */
  callExternalLogOut?: boolean;
  /**
   * The suffix url to add to the authorization origin endpoint to generate the external logout url
   */
  authorizationExternalLogOutSuffixUrl?: string;
  /**
   * The timeout in milliseconds before closing the tab used to external logout
   */
  externalLogOutTimeoutCloseTab?: number;
  /**
   * If true, the OpenID Connect protocol will be used to request the access token
   * If false, the OAuth2 protocol will be used to request the access token
   */
  oidc?: boolean;
  /**
   * If true, the access token will be requested
   * If false, the access token will not be requested
   */
  requestAccessToken?: boolean;
  /**
   * The dummy client secret used to request the access token
   */
  dummyClientSecret?: string;
  /**
   * The dummy client secret used to request the access token during MFA login process
   */
  dummyClientMfaSecret?: string;
  /**
   * The client id used to request the access token
   */
  clientId?: string;
  /**
   * The client id used to request the access token during MFA login process
   */
  clientIdMfa?: string;
  /**
   * The response type used to request the access token
   */
  responseType?: string;
  /**
   * If true, the https protocol will be required to request the access token
   */
  requireHttps?: boolean;
  /**
   * The list of allowed urls that don't need access token header
   */
  allowedUrls?: string[];
  /**
   * The redirect url after login
   */
  redirectUrl?: string;
  timeoutFactor?: number;
  postLogoutRedirectUri?: string;
  /**
   * The logout url
   */
  logoutUrl?: string;
  /**
   * The separator char used to separate nonce, login mode and state
   */
  nonceStateSeparator?: string;
  /**
   * If true, the nonce check will be disabled
   * If false, the nonce check will not be disabled
   */
  disableNonceCheck?: boolean;
  /**
   * If true, the access token will be sent in the header of each request
   * If false, the access token will not be sent in the header of each request
   */
  sendAccessToken?: boolean;
  /**
   * If true, the access token will be stored in memory storage
   * If false, the access token will not be stored in session storage
   */
  tokenInMemoryStorage?: boolean;
}
