/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-file-preview.default.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {FileStatusEnum} from "../enums/upload/file-status.enum";
import {DefaultSolidifyFilePreviewEnvironment} from "./environment.solidify-file-preview";

export const defaultSolidifyFilePreviewEnvironment: DefaultSolidifyFilePreviewEnvironment = {
  visualizationStatusThatAllowPreview: [
    FileStatusEnum.READY,
    FileStatusEnum.CONFIRMED,
    FileStatusEnum.TO_BE_CONFIRMED,
    FileStatusEnum.COMPLETED,
  ],
  visualizationDefaultMaxFileSizeInMegabytes: 300,

  // tiff constant
  visualizationTiffExtensions: ["tiff"],
  visualizationTiffContentType: ["image/tiff"],
  visualizationTiffMimeType: ["image/tiff", "image/x-tiff"],
  visualizationTiffPronomId: ["fmt/353"],
  visualizationTiffMaxFileSizeInMegabytes: undefined,

  // image constant
  visualizationImageExtensions: ["png", "gif", "jpeg", "jpg", "svg"],
  visualizationImageContentType: ["image/gif", "image/png", "image/jpeg", "image/svg+xml"],
  visualizationImageMimeType: ["image/gif", "image/png", "image/jpeg", "image/svg+xml"],
  visualizationImagePronomId: ["fmt/3", "fmt/4", "fmt/11", "fmt/12", "fmt/13", "fmt/42", "fmt/43", "fmt/44", "fmt/353", "x-fmt/399", "x-fmt/388", "xfmt/387", "fmt/155", "fmt/154", "fmt/153", "fmt/91", "fmt/92", "fmt/413", "x-fmt/109", "fmt/579", "fmt/580", "fmt/581", "fmt/582", "fmt/702"],
  visualizationImageMaxFileSizeInMegabytes: undefined,

  // movie constant for all movie
  visualizationMovieExtensions: ["webm", "flv", "gif", "gifv", "avi", "mov", "qt", "mpg", "mp2", "mpeg", "mpe", "mpv"],
  visualizationMovieContentType: ["video/mp4", "video/dv", "video/mpeg", "video/mj2", "video/xmatroska", "audio/xmatroska", "video/ogg", "audio/ogg", "application/ogg"],
  visualizationMovieMimeType: ["video/mp4", "video/dv", "video/mpeg", "video/mj2", "video/xmatroska", "audio/xmatroska", "video/ogg", "audio/ogg", "application/ogg"],
  visualizationMoviePronomId: ["fmt/199", "x-fmt/152", "fmt/649", "fmt/640", "fmt/337", "fmt/569", "fmt/569", "fmt/203", "fmt/945"],
  visualizationMovieMaxFileSizeInMegabytes: undefined,

  // movie constant
  visualizationClassicalMovieExtensions: ["mp4", "ogg", "webm"],
  visualizationClassicalMovieContentType: [],
  visualizationClassicalMovieMimeType: ["video/mp4", "video/ogg", "audio/ogg", "video/webm"],
  visualizationClassicalMoviePronomId: ["fmt/199", "fmt/203", "fmt/944", "fmt/945", "fmt/946", "fmt/947", "fmt/948"],
  visualizationClassicalMovieMaxFileSizeInMegabytes: undefined,

  // pdf constant
  visualizationPdfExtensions: ["pdf"],
  visualizationPdfContentType: [],
  visualizationPdfMimeType: ["application/pdf"],
  visualizationPdfPronomId: ["fmt/95", "fmt/3", "fmt/476", "fmt/477", "fmt/479", "fmt/480", "fmt/18", "fmt/19", "fmt/20", "fmt/276"],
  visualizationPdfMaxFileSizeInMegabytes: undefined,

  // docx-preview
  visualizationDocxPreviewExtensions: ["docx"],
  visualizationDocxPreviewContentType: [],
  visualizationDocxPreviewMimeType: ["application/vnd.openxmlformats-officedocument.wordprocessingml.document"],
  visualizationDocxPreviewPronomId: ["fmt/189", "fmt/412"],
  visualizationDocxPreviewMaxFileSizeInMegabytes: undefined,

  // xlsx
  visualizationXlsxPreviewExtensions: ["xlsx"],
  visualizationXlsxPreviewContentType: [],
  visualizationXlsxPreviewMimeType: ["application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"],
  visualizationXlsxPreviewPronomId: ["fmt/189", "fmt/214"],
  visualizationXlsxPreviewMaxFileSizeInMegabytes: undefined,

  // xlsx
  visualizationCsvPreviewExtensions: ["csv", "tsv"],
  visualizationCsvPreviewContentType: [],
  visualizationCsvPreviewMimeType: ["text/csv", "text/tab-separated-values"],
  visualizationCsvPreviewPronomId: ["x-fmt/18", "x-fmt/13"],
  visualizationCsvPreviewMaxFileSizeInMegabytes: undefined,

  // sound constant
  visualizationSoundExtensions: ["mp3", "mp4", "flac", "ogg", "wav", "acc"],
  visualizationSoundContentType: ["audio/mpeg"],
  visualizationSoundMimeType: ["audio/aac ", "audio/aacp"],
  visualizationSoundPronomId: [],
  visualizationSoundMaxFileSizeInMegabytes: undefined,

  // sme constant
  visualizationSmeExtensions: ["mol"],
  visualizationSmeContentType: ["text/plain"],
  visualizationSmeMimeType: [],
  visualizationSmePronomId: [],
  visualizationSmeMaxFileSizeInMegabytes: undefined,

  // text constant
  visualizationTextFileExtensions: ["txt", "json", "xml", "java", "yml", "ts", "css", "scss", "ads", "adb", "ada", "bash", "sh", "h", "cpp", "hpp", "cpp", "html", "htm", "shtml", "shtm", "xhtml", "xhtm", "jsp", "mak", "php", "php3", "php4", "php5", "phpml", "phps", "phps", "sql", "yml", "yaml", "rdf", "rdfs", "ttl", "csv", "nt", "sparql", "adoc", "py", "js", "java"],
  visualizationTextFileContentType: [],
  visualizationTextFileMimeType: ["text/plain", "text/xml"],
  visualizationTextFilePronomId: ["x-fmt/111", "fmt/101"],
  visualizationTextFileMaxFileSizeInMegabytes: undefined,
  visualizationTextFileHighlightLanguages: ["xml", "json", "java"],
  visualizationTextFileHighlightMaxFileSizeInMegabytes: 1,
  visualizationTextFileHighlightMaxFileSizeInMegabytesByExtensions: {},

  // markdown constant
  visualizationMarkdownExtensions: ["md", "readme"],
  visualizationMarkdownContentType: [],
  visualizationMarkdownMimeType: ["text/plain", "text/markdown"],
  visualizationMarkdownPronomId: ["fmt/1149"],
  visualizationMarkdownMaxFileSizeInMegabytes: undefined,
};
