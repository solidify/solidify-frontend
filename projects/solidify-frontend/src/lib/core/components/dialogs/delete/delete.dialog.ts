/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - delete.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {Store} from "@ngxs/store";
import {
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {ButtonColorEnum} from "../../../enums/button-color.enum";
import {CompositionActionHelper} from "../../../stores/abstract/composition/composition-action.helper";
import {CompositionNameSpace} from "../../../stores/abstract/composition/composition-namespace.model";
import {ResourceActionHelper} from "../../../stores/abstract/resource/resource-action.helper";
import {ResourceNameSpace} from "../../../stores/abstract/resource/resource-namespace.model";
import {cleanHtml} from "../../../tools/clean-html.tool";
import {AbstractInternalDialog} from "../abstract-internal/abstract-internal.dialog";

@Component({
  selector: "solidify-delete-dialog",
  templateUrl: "./delete.dialog.html",
  styleUrls: ["./delete.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteDialog extends AbstractInternalDialog<DeleteDialogData, boolean> implements OnInit {
  paramMessage: { name: string } = {name: ""};

  public readonly KEY_TITLE: string = this.labelTranslateInterface.coreCeleteDialogConfirmDeleteAction;
  public readonly KEY_CONFIRM_BUTTON: string = this.labelTranslateInterface.coreYes;
  public readonly KEY_CANCEL_BUTTON: string = this.labelTranslateInterface.coreCancel;

  constructor(private readonly _store: Store,
              protected readonly _dialogRef: MatDialogRef<DeleteDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: DeleteDialogData,
              protected readonly _injector: Injector) {
    super(_injector, _dialogRef, data);
  }

  get colorConfirm(): ButtonColorEnum {
    return isNullOrUndefined(this.data?.colorConfirm) ? ButtonColorEnum.warn : this.data.colorConfirm;
  }

  get colorCancel(): ButtonColorEnum {
    return isNullOrUndefined(this.data?.colorCancel) ? ButtonColorEnum.primary : this.data.colorCancel;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.paramMessage.name = cleanHtml(this.data.name);
  }

  confirm(): void {
    if (isNotNullNorUndefined(this.data.resId)) {
      if (isNotNullNorUndefined(this.data.resourceNameSpace)) {
        this._store.dispatch(ResourceActionHelper.delete(this.data.resourceNameSpace, this.data.resId));
      } else if (isNotNullNorUndefined(this.data.compositionNameSpace) && isNotNullNorUndefinedNorWhiteString(this.data.parentId)) {
        this._store.dispatch(CompositionActionHelper.delete(this.data.compositionNameSpace, this.data.parentId, this.data.resId));
      }
    }
    this.submit(true);
  }
}

export interface DeleteDialogData {
  resId?: string;
  parentId?: string;
  name: string;
  message: string;
  resourceNameSpace?: ResourceNameSpace;
  compositionNameSpace?: CompositionNameSpace;
  colorConfirm?: ButtonColorEnum;
  colorCancel?: ButtonColorEnum;
}
