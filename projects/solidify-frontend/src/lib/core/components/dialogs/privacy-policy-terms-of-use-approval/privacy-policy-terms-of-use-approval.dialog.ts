/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - privacy-policy-terms-of-use-approval.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {Store} from "@ngxs/store";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {MappingObject} from "../../../../core-resources/types/mapping-type.type";
import {MappingObjectUtil} from "../../../../core-resources/utils/mapping-object.util";
import {LanguagePartialEnum} from "../../../enums/partial/language-partial.enum";
import {FormValidationHelper} from "../../../helpers/form-validation.helper";
import {BaseFormDefinition} from "../../../models/forms/base-form-definition.model";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {SolidifyValidator} from "../../../utils/validations/validation.util";
import {AbstractInternalDialog} from "../abstract-internal/abstract-internal.dialog";

@Component({
  selector: "solidify-privacy-policy-terms-of-use-approval-dialog",
  templateUrl: "./privacy-policy-terms-of-use-approval.dialog.html",
  styleUrls: ["./privacy-policy-terms-of-use-approval.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrivacyPolicyTermsOfUseApprovalDialog extends AbstractInternalDialog<PrivacyPolicyTermsOfUseApprovalDialogData, boolean> implements OnInit {
  isLoading: boolean = false;
  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  privacyPolicyLink: string;
  termsOfUseLink: string;

  private _computeLink(linkMap: MappingObject<ExtendEnum<LanguagePartialEnum>, string>): string | undefined {
    let link = MappingObjectUtil.get(linkMap, this.data.language);
    if (isNullOrUndefined(link) && MappingObjectUtil.size(linkMap) > 0) {
      link = MappingObjectUtil.values(linkMap)[0];
    }
    return link;
  }

  constructor(protected readonly _dialogRef: MatDialogRef<PrivacyPolicyTermsOfUseApprovalDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: PrivacyPolicyTermsOfUseApprovalDialogData,
              private readonly _fb: FormBuilder,
              private readonly _store: Store,
              protected readonly _injector: Injector) {
    super(_injector, _dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.form = this._fb.group({});
    if (isNotNullNorUndefined(this.data.privacyPolicyLink)) {
      this.form.addControl(this.formDefinition.acceptPrivacyPolicy, this._fb.control(false, [Validators.requiredTrue, SolidifyValidator]));
      this.privacyPolicyLink = this._computeLink(this.data.privacyPolicyLink);
    }
    if (isNotNullNorUndefined(this.data.termsOfUseLink)) {
      this.form.addControl(this.formDefinition.acceptTermsOfUse, this._fb.control(false, [Validators.requiredTrue, SolidifyValidator]));
      this.termsOfUseLink = this._computeLink(this.data.termsOfUseLink);
    }
  }

  onSubmit(): void {
    if (this.form.invalid) {
      return;
    }
    this.isLoading = true;
    this.submit(true);
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }
}

export interface PrivacyPolicyTermsOfUseApprovalDialogData {
  language: ExtendEnum<LanguagePartialEnum>;
  privacyPolicyLink: MappingObject<ExtendEnum<LanguagePartialEnum>, string>;
  termsOfUseLink: MappingObject<ExtendEnum<LanguagePartialEnum>, string>;
}

class FormComponentFormDefinition extends BaseFormDefinition {
  acceptPrivacyPolicy: string = "acceptPrivacyPolicy";
  acceptTermsOfUse: string = "acceptTermsOfUse";
}
