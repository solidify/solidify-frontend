/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - data-table-loose-selection.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {ButtonColorEnum} from "../../../enums/button-color.enum";
import {AbstractInternalDialog} from "../abstract-internal/abstract-internal.dialog";

@Component({
  selector: "solidify-data-table-loose-selection-dialog",
  templateUrl: "./data-table-loose-selection.dialog.html",
  styleUrls: ["./data-table-loose-selection.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataTableLooseSelectionDialog extends AbstractInternalDialog<DataTableLooseSelectionDialogData, boolean> implements OnInit {
  paramMessage: { numberSelected: number } = {numberSelected: 0};

  constructor(protected readonly _injector: Injector,
              protected readonly _dialogRef: MatDialogRef<DataTableLooseSelectionDialog>,
              @Inject(MAT_DIALOG_DATA) readonly data: DataTableLooseSelectionDialogData) {
    super(_injector, _dialogRef, data);
  }

  get colorConfirm(): ButtonColorEnum {
    return isNullOrUndefined(this.data?.colorConfirm) ? ButtonColorEnum.primary : this.data.colorConfirm;
  }

  get colorCancel(): ButtonColorEnum {
    return isNullOrUndefined(this.data?.colorCancel) ? ButtonColorEnum.primary : this.data.colorCancel;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.paramMessage.numberSelected = this.data.numberItemSelected;
  }

  confirm(): void {
    this.submit(true);
  }
}

export interface DataTableLooseSelectionDialogData {
  numberItemSelected: number;
  colorConfirm?: ButtonColorEnum;
  colorCancel?: ButtonColorEnum;
}
