/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - keyword-paste.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {FormControl} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  isNonEmptyString,
  isNullOrUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {LabelTranslateInterface} from "../../../../label-translate-interface.model";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {ButtonColorEnum} from "../../../enums/button-color.enum";
import {LABEL_TRANSLATE} from "../../../injection-tokens/label-to-translate.injection-token";
import {KeyValue} from "../../../../core-resources/models/key-value.model";
import {AbstractDialog} from "../abstract/abstract.dialog";

@Component({
  selector: "solidify-keyword-paste-dialog",
  templateUrl: "./keyword-paste.dialog.html",
  styleUrls: ["./keyword-paste.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KeywordPasteDialog extends AbstractDialog<KeywordPasteDialogData, string> implements OnInit {
  formControl: FormControl = new FormControl();

  stringPasteTransformed: string;

  constructor(protected readonly _dialogRef: MatDialogRef<KeywordPasteDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: KeywordPasteDialogData,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface,
  ) {
    super(_dialogRef, data);
    this.stringPasteTransformed = data.stringPaste;
  }

  get solidifyConstants(): typeof SOLIDIFY_CONSTANTS {
    return SOLIDIFY_CONSTANTS;
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(this.formControl.valueChanges.pipe(
      distinctUntilChanged(),
      tap(separator => this.computeHighlightText(this.data.stringPaste, separator)),
    ));
    this.formControl.setValue(this.data.listSeparator[0]?.key);
  }

  confirm(): void {
    this.submit(this.formControl.value);
  }

  computeHighlightText(valueElement: string, separator: string): void {
    const fc = this.formControl;
    if (isNullOrUndefined(fc) || !isNonEmptyString(separator)) {
      this.stringPasteTransformed = valueElement;
    }
    this.stringPasteTransformed = valueElement.replace(
      new RegExp(separator, "gi"),
      match => `<span class="highlight-text ${separator === SOLIDIFY_CONSTANTS.LINE_BREAK ? "is-line-break" : ""}">${match}</span>`);
  }

  get colorConfirm(): ButtonColorEnum {
    return isNullOrUndefined(this.data?.colorConfirm) ? ButtonColorEnum.primary : this.data.colorConfirm;
  }

  get colorCancel(): ButtonColorEnum {
    return isNullOrUndefined(this.data?.colorCancel) ? ButtonColorEnum.primary : this.data.colorCancel;
  }
}

export interface KeywordPasteDialogData {
  stringPaste: string;
  listSeparator: KeyValue[];
  colorConfirm?: ButtonColorEnum;
  colorCancel?: ButtonColorEnum;
}
