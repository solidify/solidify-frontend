/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - first-login.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {ButtonColorEnum} from "../../../enums/button-color.enum";
import {EnumUtil} from "../../../../core-resources/utils/enum.util";
import {AbstractInternalDialog} from "../abstract-internal/abstract-internal.dialog";

@Component({
  selector: "solidify-first-login",
  templateUrl: "./first-login.dialog.html",
  styleUrls: ["./first-login.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FirstLoginDialog extends AbstractInternalDialog<FirstLoginDialogData, boolean> {
  constructor(protected readonly _dialogRef: MatDialogRef<FirstLoginDialog>,
              protected readonly _injector: Injector,
              @Inject(MAT_DIALOG_DATA) public readonly data: FirstLoginDialogData,
  ) {
    super(_injector, _dialogRef, data);
  }

  get currentThemeName(): string {
    return EnumUtil.getLabel(this.environment.appThemesTranslate, String(this.environment.theme));
  }

  startTour(): void {
    this.submit(true);
  }

  get colorConfirm(): ButtonColorEnum {
    return isNullOrUndefined(this.data?.colorConfirm) ? ButtonColorEnum.primary : this.data.colorConfirm;
  }

  get colorCancel(): ButtonColorEnum {
    return isNullOrUndefined(this.data?.colorCancel) ? ButtonColorEnum.primary : this.data.colorCancel;
  }
}

export interface FirstLoginDialogData {
  colorConfirm?: ButtonColorEnum;
  colorCancel?: ButtonColorEnum;
}
