/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - orcid-redirect.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {timer} from "rxjs";
import {tap} from "rxjs/operators";
import {AppRoutesPartialEnum} from "../../../enums/partial/app-routes-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {UrlQueryParamHelper} from "../../../helpers/url-query-param.helper";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {AbstractInternalRoutable} from "../abstract-internal/abstract-internal.routable";

@Component({
  selector: "solidify-orcid-redirect-routable",
  templateUrl: "./orcid-redirect.routable.html",
  styleUrls: ["./orcid-redirect.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrcidRedirectRoutable extends AbstractInternalRoutable implements OnInit {
  success: boolean = false;

  constructor(protected readonly _store: Store,
              protected readonly _injector: Injector,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_injector);
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (UrlQueryParamHelper.currentUrlContainsQueryParam(this._environment.orcidQueryParam) && UrlQueryParamHelper.currentUrlContainsQueryParameterValue(this._environment.orcidQueryParam, "true")) {
      this.success = true;
      this.subscribe(timer(3000).pipe(
        tap(() => {
          this._redirect();
        })));
    } else {
      this._redirect();
    }
  }

  private _redirect(): void {
    this._store.dispatch(new Navigate([AppRoutesPartialEnum.home], UrlQueryParamHelper.getQueryParamMappingObjectFromCurrentUrl()));
  }
}
