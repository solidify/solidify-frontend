/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-full-page-mode.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  Injector,
  OnInit,
} from "@angular/core";
import {SsrUtil} from "../../../../core-resources/utils/ssr.util";
import {AbstractInternalRoutable} from "../abstract-internal/abstract-internal.routable";

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractFullPageModeRoutable extends AbstractInternalRoutable implements OnInit {
  logo: string;

  constructor(protected readonly _injector: Injector) {
    super(_injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.logo = `assets/themes/${this.environment.theme}/toolbar-header-image.png`;
  }

  refresh(): void {
    SsrUtil.window?.open(".", "_self");
  }
}
