/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - maintenance-mode.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {Meta} from "@angular/platform-browser";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {isFalse} from "../../../../core-resources/tools/is/is.tool";

import {AppConfigService} from "../../../config/app-config.service";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {SolidifyAppAction} from "../../../stores/abstract/app/app.action";
import {MemoizedUtil} from "../../../utils/stores/memoized.util";
import {AbstractFullPageModeRoutable} from "../abstract-full-page-mode/abstract-full-page-mode.routable";

@Component({
  selector: "solidify-maintenance-mode-routable",
  templateUrl: "./maintenance-mode.routable.html",
  styleUrls: ["./maintenance-mode.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MaintenanceModeRoutable extends AbstractFullPageModeRoutable implements OnInit {

  isLoggedIsObs: Observable<boolean>;

  constructor(protected readonly _store: Store,
              protected readonly _injector: Injector,
              protected readonly _appConfigService: AppConfigService,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
              protected readonly _meta: Meta) {
    super(_injector);
    this.isLoggedIsObs = MemoizedUtil.select(this._store, this.environment.appState, state => state.isLoggedIn);
    this._meta.updateTag({name: SOLIDIFY_CONSTANTS.ONLINE, content: SOLIDIFY_CONSTANTS.STRING_FALSE});
  }

  // Override if necessary
  protected _isLoggedInRoot(): boolean {
    return false;
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (isFalse(this._environment.maintenanceMode) || this._isLoggedInRoot()) {
      this._redirectToHomePageIfApplicable();
    }

    this.subscribe(this._appConfigService.environmentUpdatedObs.pipe(
      tap(environment => {
        this._redirectToHomePageIfApplicable();
      })),
    );
  }

  login(): void {
    this._store.dispatch(new SolidifyAppAction.InitiateNewLogin());
  }

  protected _canRedirectToHome(): boolean {
    return isFalse(this._environment.maintenanceMode) || this._isLoggedInRoot();
  }

  // Override if necessary
  protected _onRedirectToHome(): void {
    return;
  }

  protected _redirectToHomePageIfApplicable(): void {
    if (this._canRedirectToHome()) {
      this._store.dispatch(new Navigate([""]));
      this._onRedirectToHome();
    }
  }

}
