/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - icon-app-summary.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {ICONS_LIST} from "../../../injection-tokens/icon-list.injection-token";
import {IconInfos} from "../../../models/icon/icon-infos.model";
import {AbstractInternalRoutable} from "../abstract-internal/abstract-internal.routable";

@Component({
  selector: "solidify-icon-app-summary-routable",
  templateUrl: "./icon-app-summary.routable.html",
  styleUrls: ["./icon-app-summary.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconAppSummaryRoutable extends AbstractInternalRoutable implements OnInit {
  constructor(@Inject(ICONS_LIST) public readonly iconsList: IconInfos[],
              protected readonly _injector: Injector) {
    super(_injector);
  }
}
