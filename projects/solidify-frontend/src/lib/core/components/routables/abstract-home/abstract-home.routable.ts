/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-home.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  Injector,
  OnInit,
} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {DataTestPartialEnum} from "../../../../core-resources/enums/partial/data-test-partial.enum";
import {isEmptyString} from "../../../../core-resources/tools/is/is.tool";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {RoutesPartialEnum} from "../../../enums/partial/routes-partial.enum";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {AbstractInternalRoutable} from "../abstract-internal/abstract-internal.routable";

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractHomeRoutable extends AbstractInternalRoutable implements OnInit {
  searchValueInUrl: string = SOLIDIFY_CONSTANTS.STRING_EMPTY;

  protected abstract _tiles: HomeTileModel[];

  protected constructor(protected readonly _store: Store,
                        protected readonly _translate: TranslateService,
                        protected readonly _injector: Injector) {
    super(_injector);
  }

  navigate(path: ExtendEnum<RoutesPartialEnum>): void {
    this._store.dispatch(new Navigate([path]));
  }

  ngOnInit(): void {
    this._computeTilesToDisplay();
  }

  visibleTiles: HomeTileModel[];

  private _getVisibleTiles(): HomeTileModel[] {
    return this._tiles.filter(tile => this._displayTile(tile) && this._tilesMatchSearchValue(tile));
  }

  protected abstract _displayTile(tile: HomeTileModel): boolean;

  private _tilesMatchSearchValue(tile: HomeTileModel): boolean {
    if (isEmptyString(this.searchValueInUrl)) {
      return true;
    }
    const terms = this.searchValueInUrl.split(SOLIDIFY_CONSTANTS.SPACE);
    const titleTranslated = this._translate.instant(tile.titleToTranslate).toLowerCase().trim();
    const subtitleTranslated = this._translate.instant(tile.subtitleToTranslate).toLowerCase().trim();
    return terms.every(term => titleTranslated.includes(term) || subtitleTranslated.includes(term));
  }

  search(searchTerm: string): void {
    this.searchValueInUrl = searchTerm.toLowerCase().trim();
    this._computeTilesToDisplay();
    return;
  }

  private _computeTilesToDisplay(): void {
    this.visibleTiles = this._getVisibleTiles();
  }
}

export interface HomeTileModel {
  avatarIcon: string;
  titleToTranslate: string;
  subtitleToTranslate?: string;
  path: ExtendEnum<RoutesPartialEnum>;
  permission: string;
  dataTest?: ExtendEnum<DataTestPartialEnum>;
}
