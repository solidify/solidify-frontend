/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - paginator.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  Injector,
  Input,
  Output,
  Renderer2,
  ViewChild,
} from "@angular/core";
import {
  MatPaginator,
  PageEvent,
} from "@angular/material/paginator";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {isNotNullNorUndefined} from "../../../../core-resources/tools/is/is.tool";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {Paging} from "../../../../core-resources/models/query-parameters/paging.model";
import {ObservableUtil} from "../../../utils/observable.util";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-paginator",
  templateUrl: "./paginator.presentational.html",
  styleUrls: ["./paginator.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaginatorPresentational extends AbstractInternalPresentational implements AfterViewInit {
  @Input()
  pageSizeOptions: number[] = this.environment.pageSizeOptions;

  @Input()
  pagingModel: Paging;

  @Input()
  hidePageSize: boolean = false;

  private readonly _pageBS: BehaviorSubject<Paging | undefined> = new BehaviorSubject<Paging | undefined>(undefined);

  @Output("pageChange")
  readonly pageObs: Observable<Paging | undefined> = ObservableUtil.asObservable(this._pageBS);

  @ViewChild("paginator")
  paginator: MatPaginator;

  constructor(protected readonly _injector: Injector,
              private readonly _elementRef: ElementRef,
              private readonly _renderer: Renderer2,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment,
  ) {
    super(_injector);
  }

  onPageEvent(pageEventMaterial: PageEvent): void {
    this._pageBS.next(this.adaptMaterialPageEventToPagingModel(pageEventMaterial));
  }

  adaptMaterialPageEventToPagingModel(pageEventMaterial: PageEvent): Paging {
    return {
      length: pageEventMaterial.length,
      pageIndex: pageEventMaterial.pageIndex,
      pageSize: pageEventMaterial.pageSize,
    } as Paging;
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
    const matForField = this._elementRef.nativeElement.querySelector("mat-form-field");
    if (isNotNullNorUndefined(matForField)) {
      this._renderer.addClass(matForField, this._environment.classBypassEdit);
      this._renderer.addClass(matForField, this._environment.classInputIgnored);
    }
  }
}
