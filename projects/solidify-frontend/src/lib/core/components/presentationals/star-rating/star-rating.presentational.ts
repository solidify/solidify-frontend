/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - star-rating.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Injector,
  Input,
  Output,
} from "@angular/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {ObservableUtil} from "../../../utils/observable.util";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-star-rating",
  templateUrl: "./star-rating.presentational.html",
  styleUrls: ["./star-rating.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class StarRatingPresentational extends AbstractInternalPresentational {
  @Input()
  noValue: boolean = false;

  @HostBinding("class.is-editable")
  @Input()
  isEditable: boolean = false;

  @Input()
  numberReview: number | undefined = undefined;

  @Input()
  rating: number;

  @Input()
  maxStarsNumber: number = 3;

  @HostBinding("class.is-label")
  @Input()
  label: string | undefined;

  @Input()
  tooltipToTranslate: string | undefined;

  @HostBinding("class.is-disabled")
  @Input()
  disabled: boolean = false;

  @HostBinding("class.is-center")
  @Input()
  center: boolean = false;

  private readonly _rateBS: BehaviorSubject<number> = new BehaviorSubject<number>(undefined);
  @Output("rateChange")
  readonly rateObs: Observable<number> = ObservableUtil.asObservable(this._rateBS);

  constructor(protected readonly _injector: Injector) {
    super(_injector);
  }

  starsArray(): number[] {
    return Array(this.maxStarsNumber);
  }

  setRating(index: number): void {
    if (!this.isEditable) {
      return;
    }
    this.rating = index + 1;
    this._rateBS.next(this.rating);
  }

  tempRating: number;

  cleanTempRating(): void {
    if (!this.isEditable) {
      return;
    }
    this.tempRating = undefined;
  }

  setTempRating(index: number): void {
    if (!this.isEditable) {
      return;
    }
    this.tempRating = index + 1;
  }
}
