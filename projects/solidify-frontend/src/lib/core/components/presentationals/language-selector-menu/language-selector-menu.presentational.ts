/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - language-selector-menu.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  Output,
  ViewChild,
} from "@angular/core";
import {MatMenu} from "@angular/material/menu";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {EnumUtil} from "../../../../core-resources/utils/enum.util";
import {StringUtil} from "../../../../core-resources/utils/string.util";
import {LanguagePartialEnum} from "../../../enums/partial/language-partial.enum";
import {KeyValue} from "../../../../core-resources/models/key-value.model";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {ObservableUtil} from "../../../utils/observable.util";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-language-selector-menu",
  templateUrl: "./language-selector-menu.presentational.html",
  styleUrls: ["./language-selector-menu.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  exportAs: "menuLanguage",
})
export class LanguageSelectorMenuPresentational extends AbstractInternalPresentational {
  @ViewChild(MatMenu, {static: true}) menu: MatMenu;

  @Input()
  useImage: boolean = false;

  @Input()
  displaySelected: boolean = false;

  languageEnumTranslate: KeyValue[] = this.environment.appLanguagesTranslate;
  listLanguages: ExtendEnum<LanguagePartialEnum>[] = this.environment.appLanguages;

  private readonly _languageBS: BehaviorSubject<ExtendEnum<LanguagePartialEnum> | undefined> = new BehaviorSubject<ExtendEnum<LanguagePartialEnum> | undefined>(undefined);
  @Output("languageChange")
  readonly languageObs: Observable<ExtendEnum<LanguagePartialEnum> | undefined> = ObservableUtil.asObservable(this._languageBS);

  private get _language(): ExtendEnum<LanguagePartialEnum> | undefined {
    return this._languageBS.getValue();
  }

  private set _language(value: ExtendEnum<LanguagePartialEnum> | undefined) {
    if (this._languageBS.value !== value) {
      this._languageBS.next(value);
    }
  }

  get language(): ExtendEnum<LanguagePartialEnum> | undefined {
    return this._language;
  }

  @Input()
  set language(language: ExtendEnum<LanguagePartialEnum> | undefined) {
    this._language = language;
  }

  get stringUtil(): typeof StringUtil {
    return StringUtil;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  get listLanguageFiltered(): ExtendEnum<LanguagePartialEnum>[] {
    return this.listLanguages.filter(l => this.language !== l);
  }

  constructor(protected readonly _injector: Injector) {
    super(_injector);
  }

  selectLanguage(language: ExtendEnum<LanguagePartialEnum>): void {
    this._languageBS.next(language);
  }

  getPathToImage(language: ExtendEnum<LanguagePartialEnum>): string | undefined {
    if (isNullOrUndefined(language)) {
      return undefined;
    }
    return "assets/language-icons/" + language + ".svg";
  }
}
