/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-can-deactivate.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  HostListener,
  Injector,
} from "@angular/core";
import {Store} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  take,
  tap,
} from "rxjs/operators";
import {isFalse} from "../../../../core-resources/tools/is/is.tool";
import {ICanDeactivate} from "../../../models/can-deactivate.model";
import {MemoizedUtil} from "../../../utils/stores/memoized.util";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

// @dynamic
@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractCanDeactivatePresentational extends AbstractInternalPresentational implements ICanDeactivate {
  private _ignorePreventLeavePopupObs: Observable<boolean>;

  abstract canDeactivate(): boolean;

  @HostListener("window:beforeunload", ["$event"])
  leaveWithoutSaveNotification($event: Event): void {
    this.subscribe(this._ignorePreventLeavePopupObs.pipe(
        take(1),
        tap(ignorePreventLeavePopup => {
          if (isFalse(ignorePreventLeavePopup) && !this.canDeactivate()) {
            $event.returnValue = true;
          }
        }),
      ),
    );
  }

  constructor(protected readonly _injector: Injector) {
    super(_injector);
    const store = _injector.get(Store);
    this._ignorePreventLeavePopupObs = MemoizedUtil.select(store, this.environment.appState, state => state.ignorePreventLeavePopup);
  }
}
