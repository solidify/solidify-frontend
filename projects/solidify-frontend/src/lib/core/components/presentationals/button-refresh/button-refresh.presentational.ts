/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - button-refresh.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  Output,
} from "@angular/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {ObservableUtil} from "../../../utils/observable.util";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-button-refresh",
  templateUrl: "./button-refresh.presentational.html",
  styleUrls: ["./button-refresh.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class ButtonRefreshPresentational extends AbstractInternalPresentational {
  @Input("isLoading")
  isLoading: boolean;

  private readonly _refreshBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("refresh")
  readonly refreshObs: Observable<void> = ObservableUtil.asObservable(this._refreshBS);

  constructor(protected readonly _injector: Injector) {
    super(_injector);
  }

  refresh(): void {
    this._refreshBS.next();
  }
}
