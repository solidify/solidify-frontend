/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - table-module-version.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
  Output,
} from "@angular/core";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  isNonEmptyArray,
  isNotNullNorUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {MappingObject} from "../../../../core-resources/types/mapping-type.type";
import {ArrayUtil} from "../../../../core-resources/utils/array.util";
import {ClipboardUtil} from "../../../../core-resources/utils/clipboard.util";
import {MappingObjectUtil} from "../../../../core-resources/utils/mapping-object.util";
import {SsrUtil} from "../../../../core-resources/utils/ssr.util";
import {LabelTranslateInterface} from "../../../../label-translate-interface.model";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../../injection-tokens/label-to-translate.injection-token";
import {BackendModuleVersion} from "../../../models/backend-modules/backend-module-version.model";
import {FrontendVersion} from "../../../models/frontend-version.model";
import {NotificationService} from "../../../services/notification.service";
import {DateUtil} from "../../../utils/date.util";
import {ObservableUtil} from "../../../utils/observable.util";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-table-module-version",
  templateUrl: "./table-module-version.presentational.html",
  styleUrls: ["./table-module-version.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: TableModuleVersionPresentational,
    },
  ],
})
export class TableModuleVersionPresentational extends AbstractInternalPresentational {

  private _mapBackendModulesVersion: MappingObject<string, BackendModuleVersion>;

  @Input()
  set mapBackendModulesVersion(value: MappingObject<string, BackendModuleVersion>) {
    this._mapBackendModulesVersion = value;
    this.backendModulesVersion = MappingObjectUtil.values(this._mapBackendModulesVersion);
    this._computeModuleVersion();
  }

  get mapBackendModulesVersion(): MappingObject<string, BackendModuleVersion> {
    return this._mapBackendModulesVersion;
  }

  backendModulesVersion: BackendModuleVersion[];

  private _frontendVersion: FrontendVersion;

  @Input()
  set frontendVersion(value: FrontendVersion) {
    this._frontendVersion = value;
    this._computeModuleVersion();
  }

  get frontendVersion(): FrontendVersion {
    return this._frontendVersion;
  }

  listModulesVersion: ModuleVersion[];

  protected readonly _navigateBS: BehaviorSubject<string[]> = new BehaviorSubject<string[]>(undefined);
  @Output("navigate")
  readonly navigateObs: Observable<string[]> = ObservableUtil.asObservable(this._navigateBS);

  constructor(protected readonly _injector: Injector,
              @Inject(LABEL_TRANSLATE) readonly labelTranslateInterface: LabelTranslateInterface,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment,
              private readonly notificationService: NotificationService) {
    super(_injector);
  }

  private _computeModuleVersion(): void {
    this.listModulesVersion = [];
    if (isNotNullNorUndefined(this.frontendVersion)) {
      this.listModulesVersion.push({
        name: "Portal",
        version: this.frontendVersion.version,
        date: DateUtil.convertSwissDateTimeLongStringToDate(this.frontendVersion.date + ":00"),
        url: SsrUtil.window?.location.origin,
        urlInfo: "./assets/" + this._environment.frontendVersionFile,
      });
    }
    if (isNonEmptyArray(this.backendModulesVersion)) {
      this.backendModulesVersion.forEach(m => {
        this.listModulesVersion.push({
          name: m.name,
          version: m.application?.version,
          date: new Date(m.git?.commit?.time),
          url: m.url,
          urlInfo: m.urlInfo,
          urlDashboard: m.urlDashboard,
        });
      });
    }
    this.listModulesVersion = ArrayUtil.sortAscendant(this.listModulesVersion, "name");
  }

  trackByFn(index: number, moduleVersion: ModuleVersion): string {
    return moduleVersion.name;
  }

  openTab(url: string): void {
    SsrUtil.window?.open(url, "_blank");
  }

  copy(url: string): void {
    if (ClipboardUtil.copyStringToClipboard(url)) {
      this.notificationService.showInformation(this.labelTranslateInterface.coreNotificationCopiedToClipboard);
    }
  }
}

interface ModuleVersion {
  name: string;
  url: string;
  urlInfo: string;
  urlDashboard?: string;
  version: string;
  date: Date;
}
