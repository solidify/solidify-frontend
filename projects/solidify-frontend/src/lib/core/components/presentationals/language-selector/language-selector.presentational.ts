/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - language-selector.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  Output,
} from "@angular/core";
import {TooltipPosition} from "@angular/material/tooltip";
import {Store} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {EnumUtil} from "../../../../core-resources/utils/enum.util";
import {StringUtil} from "../../../../core-resources/utils/string.util";
import {LanguagePartialEnum} from "../../../enums/partial/language-partial.enum";
import {KeyValue} from "../../../../core-resources/models/key-value.model";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {ObservableUtil} from "../../../utils/observable.util";
import {MemoizedUtil} from "../../../utils/stores/memoized.util";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-language-selector",
  templateUrl: "./language-selector.presentational.html",
  styleUrls: ["./language-selector.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LanguageSelectorPresentational extends AbstractInternalPresentational {
  darkModeObs: Observable<boolean>;

  private readonly _languageBS: BehaviorSubject<ExtendEnum<LanguagePartialEnum> | undefined> = new BehaviorSubject<ExtendEnum<LanguagePartialEnum> | undefined>(undefined);

  languageEnumTranslate: KeyValue[];

  @Input()
  useImage: boolean = false;

  @Input()
  tooltipPosition: TooltipPosition = "below";

  @Input()
  displaySelected: boolean = false;

  @Output("languageChange")
  readonly languageObs: Observable<ExtendEnum<LanguagePartialEnum> | undefined> = ObservableUtil.asObservable(this._languageBS);

  get language(): ExtendEnum<LanguagePartialEnum> | undefined {
    return this._languageBS.getValue();
  }

  @Input()
  set language(value: ExtendEnum<LanguagePartialEnum> | undefined) {
    if (this._languageBS.value !== value) {
      this._languageBS.next(value);
    }
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  get stringUtil(): typeof StringUtil {
    return StringUtil;
  }

  constructor(protected readonly _injector: Injector,
              private readonly _store: Store) {
    super(_injector);
    this.darkModeObs = MemoizedUtil.select(this._store, this.environment.appState, state => state.darkMode);
    this.languageEnumTranslate = this.environment.appLanguagesTranslate;
  }

  getPathToImage(language: ExtendEnum<LanguagePartialEnum>): string | undefined {
    if (isNullOrUndefined(language)) {
      return undefined;
    }
    return "assets/language-icons/" + language + ".svg"; // TODO CHANGE TO USE ICON HELPER
  }
}
