/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - breadcrumb.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
  Output,
} from "@angular/core";
import {
  ActivatedRoute,
  NavigationEnd,
  Router,
} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  map,
} from "rxjs/operators";
import {
  isFunction,
  isNullOrUndefined,
  isTrue,
  isUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {StringUtil} from "../../../../core-resources/utils/string.util";
import {Breadcrumb} from "../../../models/route/breadcrumb.model";
import {SolidifyRouteData} from "../../../models/route/route-data.model";
import {ObservableUtil} from "../../../utils/observable.util";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-breadcrumb",
  templateUrl: "./breadcrumb.presentational.html",
  styleUrls: ["./breadcrumb.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BreadcrumbPresentational extends AbstractInternalPresentational implements OnInit {
  breadcrumbsObs: Observable<Breadcrumb[]>;

  private readonly _navigateBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);

  @Output("navigateChange")
  readonly navigateObs: Observable<string | undefined> = ObservableUtil.asObservable(this._navigateBS);

  private readonly _BREADCRUMB_TRANSLATE_HOME: string = this.labelTranslateInterface.coreBreadcrumbHome;

  constructor(private readonly translate: TranslateService,
              private readonly router: Router,
              private readonly store: Store,
              private readonly activatedRoute: ActivatedRoute,
              protected readonly _injector: Injector) {
    super(_injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.breadcrumbsObs = this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        distinctUntilChanged(),
        map(event => this.buildBreadCrumb(this.activatedRoute.root)),
      );
  }

  buildBreadCrumb(route: ActivatedRoute,
                  url: string = "",
                  breadcrumbs: Breadcrumb[] = []): Breadcrumb[] {
    const label: string = this.getLabel(route);
    const labelObs = this.getBreadcrumbMemoizedSelector(route);
    const path = this.getPath(route);
    url = `${url}${path}/`;

    if (!isUndefined(label) || !isUndefined(labelObs)) {

      const breadcrumbElement: Breadcrumb = {
        label,
        labelObs: labelObs,
        url: url,
        noLink: this.getNoLink(route),
      } as Breadcrumb;

      breadcrumbs = [...breadcrumbs, breadcrumbElement];
    }
    if (route.firstChild) {
      return this.buildBreadCrumb(route.firstChild, url, breadcrumbs);
    }
    return breadcrumbs;
  }

  private getLabel(route: ActivatedRoute): string | undefined {
    if (route.routeConfig) {
      const routeConfig = route.routeConfig;
      const data: SolidifyRouteData = routeConfig.data;
      if (isNullOrUndefined(data) || isNullOrUndefined(data.breadcrumb)) {
        // console.warn(`Please define the data '${this._BREADCRUMB_KEY}', for the following route ${route.toString()}`);
        return undefined;
      }
      if (isFunction(data.breadcrumb)) {
        return data.breadcrumb(route.snapshot.params);
      }
      return data.breadcrumb;
    }

    return this._BREADCRUMB_TRANSLATE_HOME;
  }

  private getBreadcrumbMemoizedSelector(route: ActivatedRoute): Observable<string> | undefined {
    if (isNullOrUndefined(route.routeConfig) || isNullOrUndefined(route.routeConfig.data)) {
      return undefined;
    }
    const breadcrumbMemoizedSelector = (route.routeConfig.data as SolidifyRouteData).breadcrumbMemoizedSelector;
    if (!isNullOrUndefined(breadcrumbMemoizedSelector)) {
      return this.store.select(breadcrumbMemoizedSelector);
    }
    return undefined;
  }

  private getNoLink(route: ActivatedRoute): boolean {
    if (isNullOrUndefined(route.routeConfig) || isNullOrUndefined(route.routeConfig.data)) {
      return false;
    }
    const noBreadcrumbLink = (route.routeConfig.data as SolidifyRouteData).noBreadcrumbLink;
    if (isTrue(noBreadcrumbLink)) {
      return true;
    }
    return false;
  }

  private getPath(route: ActivatedRoute): string {
    if (route.routeConfig) {
      let path = route.routeConfig.path;
      path = this.insertParametersInPath(route, path);
      return path;
    }
    return StringUtil.stringEmpty;
  }

  private insertParametersInPath(route: ActivatedRoute, path: string): string {
    const params = route.snapshot.params;
    // eslint-disable-next-line guard-for-in
    for (const key in params) {
      path = path.replace(`:${key}`, params[key]);
    }
    return path;
  }

  navigate(breadcrumb: Breadcrumb): void {
    if (this.haveNoLink(breadcrumb)) {
      return;
    }
    this._navigateBS.next(breadcrumb.url);
  }

  haveNoLink(breadcrumb: Breadcrumb): boolean {
    return this.router.url + "/" === breadcrumb.url || breadcrumb.noLink;
  }
}
