/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - button-orcid.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Inject,
  Injector,
  Input,
} from "@angular/core";
import {
  isFalse,
  isNotNullNorUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
} from "../../../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../../../core-resources/utils/ssr.util";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {NotificationService} from "../../../services/notification.service";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-button-orcid",
  templateUrl: "./button-orcid.presentational.html",
  styleUrls: ["./button-orcid.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonOrcidPresentational extends AbstractInternalPresentational {
  @Input()
  orcid: string | undefined;

  @Input()
  isVerifiedOrcid: boolean | undefined = undefined;

  @Input()
  isImageClickable: boolean = false;

  @Input()
  isButton: boolean = false;

  @Input()
  preventClick: boolean = false;

  @Input()
  isPristineForm: boolean | undefined = undefined;

  @HostBinding("class.hide")
  get hide(): boolean {
    return isNullOrUndefinedOrWhiteString(this.orcid) || (isNotNullNorUndefined(this.isPristineForm) && isFalse(this.isPristineForm));
  }

  constructor(protected readonly _injector: Injector,
              private readonly _notificationService: NotificationService,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super(_injector);
  }

  navigateToOrcid($event?: MouseEvent, isImage: boolean = false): void {
    if (this.hide) {
      return;
    }
    if (isTrue(isImage) && isFalse(this.isImageClickable)) {
      return;
    }
    if (isTrue(this.preventClick) && isNotNullNorUndefined($event)) {
      $event.stopPropagation();
    }
    SsrUtil.window?.open(this._environment.orcidUrl + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this.orcid, "_blank");
  }
}
