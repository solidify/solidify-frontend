/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - orcid-sign-in-button.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  HostListener,
  Inject,
  Injector,
  Output,
} from "@angular/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {ObservableUtil} from "../../../utils/observable.util";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-orcid-sign-in-button",
  templateUrl: "./orcid-sign-in-button.presentational.html",
  styleUrls: ["./orcid-sign-in-button.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrcidSignInButtonPresentational extends AbstractInternalPresentational {
  private readonly _clickBS: BehaviorSubject<void | undefined> = new BehaviorSubject<void | undefined>(undefined);
  @Output("clickChange")
  readonly clickObs: Observable<void | undefined> = ObservableUtil.asObservable(this._clickBS);

  @HostListener("click", ["$event"]) click(mouseEvent: MouseEvent): void {
    this._clickBS.next();
  }

  constructor(protected readonly _injector: Injector,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super(_injector);
  }
}
