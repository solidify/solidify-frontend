/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - citations.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  OnInit,
} from "@angular/core";
import {FormControl} from "@angular/forms";
import {tap} from "rxjs/operators";
import {
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
} from "../../../../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {ClipboardUtil} from "../../../../core-resources/utils/clipboard.util";
import {LabelTranslateInterface} from "../../../../label-translate-interface.model";
import {CookieType} from "../../../cookie-consent/enums/cookie-type.enum";
import {CookieConsentUtil} from "../../../cookie-consent/utils/cookie-consent.util";
import {LocalStoragePartialEnum} from "../../../enums/partial/local-storage-partial.enum";
import {LocalStorageHelper} from "../../../helpers/local-storage.helper";
import {LABEL_TRANSLATE} from "../../../injection-tokens/label-to-translate.injection-token";
import {Citation} from "../../../models/dto/citation.model";
import {NotificationService} from "../../../services/notification.service";
import {AbstractPresentational} from "../abstract/abstract.presentational";

@Component({
  selector: "solidify-citations",
  templateUrl: "./citations.presentational.html",
  styleUrls: ["./citations.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CitationsPresentational extends AbstractPresentational implements OnInit {
  formControl = new FormControl();

  selectedCitation: Citation = undefined;

  @Input()
  lastCitationLocalStorageKey: ExtendEnum<LocalStoragePartialEnum> | undefined = undefined;

  private _listCitations: Citation[];

  @Input()
  set listCitations(value: Citation[]) {
    this._listCitations = value;

    if (isNonEmptyArray(this.listCitations)) {
      this.selectedCitation = this.listCitations[0];
    } else {
      this.selectedCitation = undefined;
    }
    this.formControl.setValue(this.selectedCitation);
  }

  get listCitations(): Citation[] {
    return this._listCitations;
  }

  trackByCitation(index: number, citation: Citation): string {
    return getCitationKey(citation);
  }

  constructor(private readonly _notificationService: NotificationService,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();

    const lastCitationKey = LocalStorageHelper.getItem(this.lastCitationLocalStorageKey);
    if (isNotNullNorUndefinedNorWhiteString(lastCitationKey)) {
      const lastCitation = this.listCitations.find(c => getCitationKey(c) === lastCitationKey);
      if (isNotNullNorUndefined(lastCitation)) {
        this.formControl.setValue(lastCitation);
      }
    }

    this.subscribe(this.formControl.valueChanges.pipe(
      tap(value => {
        this.selectedCitation = value;

        if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, this.lastCitationLocalStorageKey)) {
          LocalStorageHelper.setItem(this.lastCitationLocalStorageKey, getCitationKey(this.selectedCitation));
        }
      }),
    ));
  }

  copyToClipboard(): void {
    if (ClipboardUtil.copyStringToClipboard(this.selectedCitation.text)) {
      this._notificationService.showInformation(this.labelTranslate.coreNotificationCopiedToClipboard);
    }
  }
}

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
function getCitationKey(citation: Citation): string {
  return `${citation.style}|${citation.outputFormat}[${citation.language}]`;
}
