/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - ribbon.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Inject,
} from "@angular/core";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {AbstractPresentational} from "../abstract/abstract.presentational";

@Component({
  selector: "solidify-ribbon",
  templateUrl: "./ribbon.presentational.html",
  styleUrls: ["./ribbon.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RibbonPresentational extends AbstractPresentational {
  @HostBinding("class.is-visible")
  isVisible: boolean;

  text: string;

  @HostBinding("style.background-color")
  color: string;

  constructor(@Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super();
    this.isVisible = this._environment.ribbonEnabled;
    this.text = this._environment.ribbonText;
    this.color = this._environment.ribbonColor;
  }
}
