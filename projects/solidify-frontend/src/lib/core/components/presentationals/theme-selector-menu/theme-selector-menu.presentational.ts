/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - theme-selector-menu.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  Output,
  ViewChild,
} from "@angular/core";
import {MatMenu} from "@angular/material/menu";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {StringUtil} from "../../../../core-resources/utils/string.util";
import {ThemePartialEnum} from "../../../enums/partial/theme-partial.enum";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {ObservableUtil} from "../../../utils/observable.util";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-theme-selector-menu",
  templateUrl: "./theme-selector-menu.presentational.html",
  styleUrls: ["./theme-selector-menu.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  exportAs: "menuTheme",
})
export class ThemeSelectorMenuPresentational extends AbstractInternalPresentational {
  @ViewChild(MatMenu, {static: true}) menu: MatMenu;

  listThemes: ExtendEnum<ThemePartialEnum>[] = this.environment.appThemes;

  private readonly _themeBS: BehaviorSubject<ExtendEnum<ThemePartialEnum> | undefined> = new BehaviorSubject<ExtendEnum<ThemePartialEnum> | undefined>(undefined);
  @Output("themeChange")
  readonly themeObs: Observable<ExtendEnum<ThemePartialEnum> | undefined> = ObservableUtil.asObservable(this._themeBS);

  private get _theme(): ExtendEnum<ThemePartialEnum> | undefined {
    return this._themeBS.getValue();
  }

  private set _theme(value: ExtendEnum<ThemePartialEnum> | undefined) {
    if (this._themeBS.value !== value) {
      this._themeBS.next(value);
    }
  }

  get theme(): ExtendEnum<ThemePartialEnum> | undefined {
    return this._theme;
  }

  @Input()
  set theme(theme: ExtendEnum<ThemePartialEnum> | undefined) {
    this._theme = theme;
  }

  get stringUtil(): typeof StringUtil {
    return StringUtil;
  }

  constructor(protected readonly _injector: Injector) {
    super(_injector);
  }

  getPathToImage(theme: ExtendEnum<ThemePartialEnum>): string {
    if (isNullOrUndefined(theme)) {
      return undefined;
    }
    return "assets/themes/" + theme + "/logo.svg";
  }

  selectTheme(theme: ExtendEnum<ThemePartialEnum>): void {
    this._themeBS.next(theme);
  }
}
