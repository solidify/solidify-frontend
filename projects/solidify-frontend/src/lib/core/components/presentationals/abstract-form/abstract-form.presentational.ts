/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectorRef,
  Directive,
  ElementRef,
  HostBinding,
  HostListener,
  Injector,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChildren,
} from "@angular/core";
import {
  AbstractControl,
  FormArray,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from "@angular/forms";
import {Navigate} from "@ngxs/router-plugin";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {ObjectUtil} from "../../../../core-resources/utils/object.util";
import {SsrUtil} from "../../../../core-resources/utils/ssr.util";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {DomHelper} from "../../../helpers/dom.helper";
import {FormValidationHelper} from "../../../helpers/form-validation.helper";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {FormControlKey} from "../../../models/forms/form-control-key.model";
import {ModelFormControlEvent} from "../../../models/forms/model-form-control-event.model";
import {ObservableUtil} from "../../../utils/observable.util";
import {AbstractCanDeactivatePresentational} from "../abstract-can-deactivate/abstract-can-deactivate.presentational";
import {PanelExpandablePresentational} from "../panel-expandable/panel-expandable.presentational";

// @dynamic
@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractFormPresentational<TResource extends BaseResourceType> extends AbstractCanDeactivatePresentational implements OnInit {
  @ViewChildren(PanelExpandablePresentational) listPanelExpandablePresentational: QueryList<PanelExpandablePresentational>;

  form: FormGroup;

  @HostBinding("class.edit-available")
  @Input()
  editAvailable: boolean;

  private _model: TResource;

  @Input()
  set model(value: TResource) {
    const oldValue = ObjectUtil.clone(this._model);
    this._model = value;
    this._modelUpdated(oldValue, value);
  }

  get model(): TResource {
    return this._model;
  }

  @HostBinding("class.readonly")
  protected _readonly: boolean;

  protected _listFieldNameToDisplayErrorInToast: string[] = [];

  @Input()
  set readonly(value: boolean) {
    this._readonly = value;
    this._updateFormReadonly();
  }

  get readonly(): boolean {
    return this._readonly;
  }

  classInputIgnored: string = this.environment.classInputIgnored;
  classBypassEdit: string = this.environment.classBypassEdit;
  isValidWhenDisable: boolean;

  private readonly _timeoutMsBeforeFocus: number = 100;
  private readonly _TAG_MAT_FORM_FIELD: string = "mat-form-field";
  private readonly _TAG_MAT_CHECKBOX: string = "mat-checkbox";
  private readonly _TAG_TEXTAREA: string = "textarea";
  private readonly _TAG_MAT_SELECT: string = "mat-select";
  private readonly _TAG_INPUT: string = "input";

  @HostListener("keydown.F2", ["$event"])
  onEnterInEdition(keyboardEvent: KeyboardEvent): void {
    if (keyboardEvent && !keyboardEvent.defaultPrevented) {
      keyboardEvent.preventDefault();
      if (!this.readonly || !this.editAvailable) {
        return;
      }
      this.enterInEditMode();
    }
  }

  @HostListener("click", ["$event"]) click(mouseEvent: MouseEvent): void {
    if (!this.readonly || !this.editAvailable) {
      return;
    }
    setTimeout(() => {
      if (isNotNullNorUndefinedNorWhiteString(this._getHighlightedText())) {
        return;
      }
      const parentElement = DomHelper.getParentWithTag(mouseEvent.target as Element, [this._TAG_MAT_FORM_FIELD, this._TAG_MAT_CHECKBOX, ...this.environment.listTagsComponentsWhereClickAllowToEnterInEditMode], this._elementRef.nativeElement, this.classBypassEdit);
      if (isNotNullNorUndefined(parentElement) && !parentElement.classList.contains(this.classInputIgnored)) {
        this.enterInEditMode();
        setTimeout(() => {
          const elementToFocus = this._getElementToFocus(parentElement);
          AbstractFormPresentational._focus(elementToFocus);
        }, this._timeoutMsBeforeFocus);
      }
    });
  }

  private _getHighlightedText(): string {
    if (isNotNullNorUndefined(SsrUtil.window?.getSelection)) {
      return SsrUtil.window?.getSelection().toString();
    } else if (isNotNullNorUndefined(typeof SsrUtil.window?.document?.["selection"]) && document?.["selection"]?.type === "Text") {
      return SsrUtil.window?.document["selection"].createRange().text;
    }
    return SOLIDIFY_CONSTANTS.STRING_EMPTY;
  }

  private _getElementToFocus(element: Element): Element {
    if (element.tagName.toLowerCase() === this._TAG_MAT_FORM_FIELD) {
      const input = DomHelper.getParentChildrenWithTag(element, [`${this._TAG_INPUT}:not(.${this.environment.classInputIgnored})`, this._TAG_TEXTAREA, this._TAG_MAT_SELECT]);
      if (!isNullOrUndefined(input)) {
        return input;
      }
    }
    if (element.tagName.toLowerCase() === this._TAG_MAT_CHECKBOX) {
      return element;
    }
    return element;
  }

  private static _focus(element: Element): void {
    element["focus"]();
  }

  protected readonly _submitBS: BehaviorSubject<ModelFormControlEvent<TResource> | undefined> = new BehaviorSubject<ModelFormControlEvent<TResource> | undefined>(undefined);
  @Output("submitChange")
  readonly submitObs: Observable<ModelFormControlEvent<TResource> | undefined> = ObservableUtil.asObservable(this._submitBS);

  protected readonly _dirtyBS: BehaviorSubject<boolean | undefined> = new BehaviorSubject<boolean | undefined>(undefined);
  @Output("dirtyChange")
  readonly dirtyObs: Observable<boolean | undefined> = ObservableUtil.asObservable(this._dirtyBS);

  private readonly _checkAvailableBS: BehaviorSubject<FormControlKey> = new BehaviorSubject<FormControlKey>(undefined);
  @Output("checkAvailableChange")
  readonly checkAvailableObs: Observable<FormControlKey> = ObservableUtil.asObservable(this._checkAvailableBS);

  protected readonly _navigateBS: BehaviorSubject<string[]> = new BehaviorSubject<string[]>(undefined);
  @Output("navigate")
  readonly navigateObs: Observable<string[]> = ObservableUtil.asObservable(this._navigateBS);

  protected readonly _navigateWithQueryParamBS: BehaviorSubject<Navigate> = new BehaviorSubject<Navigate>(undefined);
  @Output("navigateWithQueryParam")
  readonly navigateWithQueryParamObs: Observable<Navigate> = ObservableUtil.asObservable(this._navigateWithQueryParamBS);

  protected readonly _editBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("editChange")
  readonly editObs: Observable<void> = ObservableUtil.asObservable(this._editBS);

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  required: ValidationErrors = Validators.required;

  protected constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
                        protected readonly _elementRef: ElementRef,
                        protected readonly _injector: Injector) {
    super(_injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.resetFormToInitialValue();
    this._updateFormReadonly();

    this.subscribe(this.form.statusChanges, () => {
      if (this._dirtyBS.value !== this.form.dirty) {
        this._dirtyBS.next(this.form.dirty);
      }
    });
  }

  navigate(value: string[]): void {
    this._navigateBS.next(value);
  }

  resetFormToInitialValue(): void {
    if (this.model) {
      this._bindFormTo(this.model);
    } else {
      this._initNewForm();
    }
  }

  protected _modelUpdated(oldValue: TResource | undefined, newValue: TResource): void {
  }

  private _updateFormReadonly(): void {
    if (isNullOrUndefined(this.form)) {
      return;
    }
    if (this.readonly) {
      this.form.disable({emitEvent: false});
    } else {
      this.form.enable({emitEvent: false});
      this._disableSpecificField();
    }
  }

  protected _disableSpecificField(): void {
    // OVERRIDE THIS METHOD IF NECESSARY
  }

  protected abstract _initNewForm(): void;

  protected abstract _bindFormTo(model: TResource): void;

  onSubmit(): void {
    let model = this._treatmentBeforeSubmit(this.form.value as TResource);
    model = this._internalTreatmentBeforeSubmit(model);
    this._addResId(model);
    this._submitBS.next({
      model: model,
      formControl: this.form,
      changeDetectorRef: this._changeDetectorRef,
      listFieldNameToDisplayErrorInToast: this._listFieldNameToDisplayErrorInToast,
      listPanelExpandablePresentational: this.listPanelExpandablePresentational.toArray(),
    });
  }

  protected abstract _treatmentBeforeSubmit(model: TResource): TResource;

  protected _internalTreatmentBeforeSubmit(model: TResource): TResource {
    return model;
  }

  protected _addResId(model: TResource): void {
    if (isNotNullNorUndefined(this.model)) {
      model.resId = this.model.resId;
    }
  }

  canDeactivate(): boolean {
    return !this.form.dirty;
  }

  getAbstractControl(key: string): AbstractControl {
    return FormValidationHelper.getAbstractControl(this.form, key);
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  getFormGroup(key: string): FormGroup {
    return FormValidationHelper.getFormGroup(this.form, key);
  }

  getFormArray(key: string): FormArray {
    return FormValidationHelper.getFormArray(this.form, key);
  }

  isRequired(key: string): boolean {
    const errors = this.getFormControl(key).errors;
    return isNullOrUndefined(errors) ? false : errors.required;
  }

  checkAvailable(key: string, formControl: FormControl): void {
    this._checkAvailableBS.next({key: key, formControl: formControl, changeDetector: this._changeDetectorRef});
  }

  checkAvailableMultiple(...keys: string[]): void {
    this._checkAvailableBS.next({keys: keys, form: this.form, changeDetector: this._changeDetectorRef});
  }

  enterInEditMode(): void {
    this._editBS.next();
  }
}
