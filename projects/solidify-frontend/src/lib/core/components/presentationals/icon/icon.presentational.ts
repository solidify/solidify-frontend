/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - icon.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  Input,
  Renderer2,
} from "@angular/core";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {IconLibEnum} from "../../../enums/icon-lib.enum";
import {IconNamePartialEnum} from "../../../enums/partial/icon-name-partial.enum";
import {IconHelper} from "../../../helpers/icon.helper";
import {ICONS_LIST} from "../../../injection-tokens/icon-list.injection-token";
import {IconInfos} from "../../../models/icon/icon-infos.model";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {CoreAbstractComponent} from "../../core-abstract/core-abstract.component";

@Component({
  selector: "solidify-icon",
  templateUrl: "./icon.presentational.html",
  styleUrls: ["./icon.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconPresentational extends CoreAbstractComponent {
  private readonly _CLASS_FONT_AWESOME: string = "fa-icon";
  private readonly _CLASS_MATERIAL: string = "mat-icon";
  private readonly _CLASS_IMAGE: string = "image";
  readonly PATH_TO_IMAGE: string = "assets/images/";

  iconInfo: IconInfos;

  protected _iconName: ExtendEnum<IconNamePartialEnum>;

  @Input()
  set iconName(value: ExtendEnum<IconNamePartialEnum>) {
    this._iconName = value;
    this.iconInfo = IconHelper.getIcon(this._listIconInfos, value);

    if (isNullOrUndefined(this.iconInfo)) {
      return;
    }

    this._updateClass();
  }

  get iconLib(): typeof IconLibEnum {
    return IconLibEnum;
  }

  constructor(private readonly _renderer: Renderer2,
              private readonly _el: ElementRef,
              @Inject(ICONS_LIST) private readonly _listIconInfos: IconInfos[]) {
    super();
  }

  private _updateClass(): void {
    this._renderer.removeClass(this._el.nativeElement, this._CLASS_MATERIAL);
    this._renderer.removeClass(this._el.nativeElement, this._CLASS_FONT_AWESOME);
    this._renderer.removeClass(this._el.nativeElement, this._CLASS_IMAGE);

    if (this.iconInfo.lib === IconLibEnum.materialIcon) {
      this._renderer.addClass(this._el.nativeElement, this._CLASS_MATERIAL);
    } else if ([IconLibEnum.fontAwesomeSolid, IconLibEnum.fontAwesomeBrand, IconLibEnum.fontAwesomeRegular].includes(this.iconInfo.lib)) {
      this._renderer.addClass(this._el.nativeElement, this._CLASS_FONT_AWESOME);
    } else if (this.iconInfo.lib === IconLibEnum.image) {
      this._renderer.addClass(this._el.nativeElement, this._CLASS_IMAGE);
    }
  }
}

