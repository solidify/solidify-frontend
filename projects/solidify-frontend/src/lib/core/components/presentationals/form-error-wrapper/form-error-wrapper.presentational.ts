/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - form-error-wrapper.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Injector,
  Input,
  OnInit,
} from "@angular/core";
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {
  filter,
  takeWhile,
  tap,
} from "rxjs/operators";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-form-error-wrapper",
  templateUrl: "./form-error-wrapper.presentational.html",
  styleUrls: ["./form-error-wrapper.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: FormErrorWrapperPresentational,
    },
  ],
})
export class FormErrorWrapperPresentational extends AbstractInternalPresentational implements OnInit, ControlValueAccessor {
  @Input()
  formControl: AbstractControl & FormControl;

  @Input()
  @HostBinding("class.is-under-input")
  isUnderInput: boolean = true;

  @HostBinding("class.hide")
  hide: boolean = true;

  constructor(protected readonly _injector: Injector,
              private readonly _changeDetector: ChangeDetectorRef) {
    super(_injector);
  }

  registerOnChange(fn: any): void {
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(obj: any): void {
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(this.formControl.statusChanges.pipe(
      takeWhile(() => this.hide),
      filter(status => status === SOLIDIFY_CONSTANTS.FORM_STATUS_INVALID && this.formControl.dirty),
      tap(() => {
        this.hide = false;
        this._changeDetector.detectChanges();
      }),
    ));
  }
}
