/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - detail-presentation.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  TemplateRef,
} from "@angular/core";
import {IconNamePartialEnum} from "../../../enums/partial/icon-name-partial.enum";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-detail-presentation",
  templateUrl: "./detail-presentation.presentational.html",
  styleUrls: ["./detail-presentation.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailPresentationPresentational extends AbstractInternalPresentational {
  @Input() leftTemplate: TemplateRef<any>;
  @Input() centerTemplate: TemplateRef<any>;
  @Input() rightTemplate: TemplateRef<any>;

  @Input() iconName: ExtendEnum<IconNamePartialEnum>;
}
