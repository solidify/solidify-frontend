/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - button-id.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from "@angular/core";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {NotificationService} from "../../../services/notification.service";
import {ClipboardUtil} from "../../../../core-resources/utils/clipboard.util";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-button-id",
  templateUrl: "./button-id.presentational.html",
  styleUrls: ["./button-id.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonIdPresentational extends AbstractInternalPresentational {
  @Input()
  resource: BaseResourceType;

  constructor(private readonly _notificationService: NotificationService,
              protected readonly _injector: Injector) {
    super(_injector);
  }

  copyToClipboard(): void {
    ClipboardUtil.copyStringToClipboard(this.resource.resId);
    this._notificationService.showInformation(this.labelTranslateInterface.coreNotificationIdCopyToClipboard);
  }
}
