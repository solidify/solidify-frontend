/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-content.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterViewInit,
  Directive,
  HostListener,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  isArray,
  isEmptyString,
  isNullOrUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {ObservableUtil} from "../../../utils/observable.util";
import {CoreAbstractComponent} from "../../core-abstract/core-abstract.component";

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractContentPresentational<TResource extends BaseResourceType> extends CoreAbstractComponent implements OnInit, AfterViewInit {
  @Input()
  abstract host: any;

  private readonly _closeBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("closeChange")
  readonly closeObs: Observable<void> = ObservableUtil.asObservable(this._closeBS);

  private readonly _closeByTabBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("closeByTabChange")
  readonly closeByTabObs: Observable<void> = ObservableUtil.asObservable(this._closeByTabBS);

  private readonly _closeByShiftTabBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("closeByShiftTabChange")
  readonly closeByShiftTabObs: Observable<void> = ObservableUtil.asObservable(this._closeByShiftTabBS);

  indexSelected: number;

  constructor() {
    super();
  }

  closeByTab(keyEvent: Event): void {
    if (!keyEvent.defaultPrevented) {
      keyEvent.preventDefault();
      this._closeByTabBS.next();
    }
  }

  closeByShiftTab(keyEvent: Event): void {
    if (!keyEvent.defaultPrevented) {
      keyEvent.preventDefault();
      this._closeByShiftTabBS.next();
    }
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  abstract select(value: TResource, force: boolean): void;

  close(): void {
    this._closeBS.next();
  }

  isActive(value: TResource): boolean {
    const formValue = this.host.formControl.value;
    if (isNullOrUndefined(value) || isNullOrUndefined(formValue) || isEmptyString(formValue)) {
      return false;
    }
    const currentValueId = value[this.host.valueKey];

    if (isArray(formValue)) {
      return formValue.includes(currentValueId);
    } else {
      return formValue === currentValueId;
    }
  }

  selectNextResource(): void {
    const val = this.host.list;
    let activeValIndex = -1;
    for (let index = 0; index < val.length; index++) {
      if (this.indexSelected === index) {
        activeValIndex = index;
        break;
      }
    }
    if (activeValIndex !== val.length - 1) {
      this.indexSelected = activeValIndex + 1;
    } else {
      this.indexSelected = activeValIndex;
    }
  }

  selectPreviousResource(): void {
    const val = this.host.list;
    let activeValIndex = -1;
    for (let index = 0; index < val.length; index++) {
      if (this.indexSelected === index) {
        activeValIndex = index;
        break;
      }
    }
    if (activeValIndex > 0) {
      this.indexSelected = activeValIndex - 1;
    } else {
      this.indexSelected = activeValIndex;
    }
  }

  @HostListener("keydown.arrowDown", ["$event"])
  onArrowDown(keyboardEvent: KeyboardEvent): void {
    if (keyboardEvent && !keyboardEvent.defaultPrevented) {
      keyboardEvent.preventDefault();
      this.selectNextResource();
    }
  }

  @HostListener("keydown.arrowUp", ["$event"])
  onArrowUp(keyboardEvent: KeyboardEvent): void {
    if (keyboardEvent && !keyboardEvent.defaultPrevented) {
      keyboardEvent.preventDefault();
      this.selectPreviousResource();
    }
  }
}



