/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - panel-expandable.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Injector,
  Input,
  Output,
} from "@angular/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  isFalse,
  isTrue,
} from "../../../../core-resources/tools/is/is.tool";
import {IconNamePartialEnum} from "../../../enums/partial/icon-name-partial.enum";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {ObservableUtil} from "../../../utils/observable.util";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-panel-expandable",
  templateUrl: "./panel-expandable.presentational.html",
  styleUrls: ["./panel-expandable.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger("buttonAnimation", [
      state("void", style({transform: "rotate(180deg)"})),
      transition(":enter", animate("600ms ease")),
      transition(":leave", animate("300ms ease")),
    ]),
    trigger("contentAnimation", [
      state("void", style({transform: "translateY(-100%)"})),
      transition(":enter", animate("600ms ease")),
      transition(":leave", animate("300ms ease")),
    ]),
  ],
})
export class PanelExpandablePresentational extends AbstractInternalPresentational {
  @Input()
  titleToTranslate: string;

  @Input()
  suffixToTranslate: string;

  @Input()
  isOpen: boolean = false;

  @HostBinding("class.is-closed")
  get isClosed(): boolean {
    return !this.isOpen;
  }

  @Input()
  @HostBinding("class.is-outline")
  isOutline: boolean = false;

  @Input()
  isRequired: boolean;

  @Input()
  tooltip: string | undefined;

  @Input()
  @HostBinding("class.is-panel-collapsable")
  isPanelCollapsable: boolean = true;

  @Input()
  isTitleClickable: boolean = false;

  @Input()
  icon: ExtendEnum<IconNamePartialEnum>;

  @Input()
  @HostBinding("class.is-extra-icon-button")
  extraIconButton: ExtendEnum<IconNamePartialEnum>;

  @Input()
  extraIconTooltipToTranslate: string;

  private readonly _clickOnTitleBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("clickOnTitle")
  readonly clickOnTitleObs: Observable<void> = ObservableUtil.asObservable(this._clickOnTitleBS);

  private readonly _clickOnExtraIconBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("clickOnExtraIcon")
  readonly clickOnExtraIconObs: Observable<void> = ObservableUtil.asObservable(this._clickOnExtraIconBS);

  private readonly _openBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("openChange")
  readonly openObs: Observable<void> = ObservableUtil.asObservable(this._openBS);

  private readonly _closeBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("closeChange")
  readonly closeObs: Observable<void> = ObservableUtil.asObservable(this._closeBS);

  constructor(protected readonly _injector: Injector) {
    super(_injector);
  }

  toggle(): void {
    if (isTrue(this.isOutline) || isFalse(this.isPanelCollapsable)) {
      return;
    }
    this.isOpen = !this.isOpen;

    if (this.isOpen) {
      this._openBS.next();
    } else {
      this._closeBS.next();
    }
  }

  titleClicked($event: MouseEvent): void {
    if (this.isTitleClickable) {
      this._clickOnTitleBS.next();
      $event.stopPropagation();
    }
  }

  clickOnExtraIcon($event: MouseEvent): void {
    $event.stopPropagation();
    this._clickOnExtraIconBS.next();
  }
}
