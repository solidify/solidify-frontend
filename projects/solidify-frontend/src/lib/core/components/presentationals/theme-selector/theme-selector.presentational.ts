/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - theme-selector.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  Output,
} from "@angular/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {ThemePartialEnum} from "../../../enums/partial/theme-partial.enum";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {ObservableUtil} from "../../../utils/observable.util";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-theme-selector",
  templateUrl: "./theme-selector.presentational.html",
  styleUrls: ["./theme-selector.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThemeSelectorPresentational extends AbstractInternalPresentational {
  private readonly _themeBS: BehaviorSubject<ExtendEnum<ThemePartialEnum> | undefined> = new BehaviorSubject<ExtendEnum<ThemePartialEnum> | undefined>(undefined);
  @Output("themeChange")
  readonly themeObs: Observable<ExtendEnum<ThemePartialEnum> | undefined> = ObservableUtil.asObservable(this._themeBS);

  private get _theme(): ExtendEnum<ThemePartialEnum> | undefined {
    return this._themeBS.getValue();
  }

  private set _theme(value: ExtendEnum<ThemePartialEnum> | undefined) {
    if (this._themeBS.value !== value) {
      this._themeBS.next(value);
    }
  }

  get theme(): ExtendEnum<ThemePartialEnum> | undefined {
    return this._theme;
  }

  @Input()
  set theme(theme: ExtendEnum<ThemePartialEnum> | undefined) {
    this._theme = theme;
  }

  getPathToImage(theme: ExtendEnum<ThemePartialEnum>): string {
    if (isNullOrUndefined(theme)) {
      return undefined;
    }
    return "assets/themes/" + theme + "/logo.svg";
  }
}
