/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - snackbar.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  NgZone,
} from "@angular/core";
import {
  MAT_SNACK_BAR_DATA,
  MatSnackBarRef,
} from "@angular/material/snack-bar";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {LabelTranslateInterface} from "../../../../label-translate-interface.model";
import {NotificationTypeEnum} from "../../../enums/notification-type.enum";
import {IconNamePartialEnum} from "../../../enums/partial/icon-name-partial.enum";
import {LABEL_TRANSLATE} from "../../../injection-tokens/label-to-translate.injection-token";
import {SnackbarData} from "../../../models/snackbar/snack-bar.model";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {ClipboardUtil} from "../../../../core-resources/utils/clipboard.util";
import {AbstractSnackbarPresentational} from "../abstract-snackbar/abstract-snackbar.presentational";

@Component({
  selector: "solidify-snackbar",
  templateUrl: "./snackbar.presentational.html",
  styleUrls: ["./snackbar.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SnackbarPresentational extends AbstractSnackbarPresentational {
  get notificationTypeEnum(): typeof NotificationTypeEnum {
    return NotificationTypeEnum;
  }

  constructor(public readonly snackbarRef: MatSnackBarRef<SnackbarPresentational>,
              @Inject(MAT_SNACK_BAR_DATA) public readonly data: SharedSnackbarOption,
              @Inject(LABEL_TRANSLATE) public readonly labelTranslateInterface: LabelTranslateInterface,
              private readonly _ngZone: NgZone) {
    super();
  }

  getIcon(): ExtendEnum<IconNamePartialEnum> | undefined {
    if (!isNullOrUndefined(this.data.icon)) {
      return this.data.icon;
    }

    switch (this.data.category) {
      case NotificationTypeEnum.success:
        return IconNamePartialEnum.success;
      case NotificationTypeEnum.information:
        return IconNamePartialEnum.information;
      case NotificationTypeEnum.warning:
        return IconNamePartialEnum.warning;
      case NotificationTypeEnum.error:
        return IconNamePartialEnum.error;
    }

    return undefined;
  }

  close($event?: MouseEvent): void {
    this._ngZone.run(() => this.snackbarRef.dismiss());
  }

  actionButton($event: MouseEvent): void {
    this.close($event);
    this.data?.action.callback();
  }

  copyToClipboardError(): void {
    ClipboardUtil.copyStringToClipboard(JSON.stringify(this.data.error));
    this.close();
  }
}

export interface SharedSnackbarOption extends SnackbarData {
  icon?: IconNamePartialEnum;
}
