/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-overlay.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {OverlayRef} from "@angular/cdk/overlay";
import {
  Directive,
  ElementRef,
  HostListener,
} from "@angular/core";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../../../core-resources/utils/ssr.util";
import {DomHelper} from "../../../helpers/dom.helper";
import {OverlayData} from "../../../models/overlay-data.model";
import {CoreAbstractComponent} from "../../core-abstract/core-abstract.component";

// @dynamic
@Directive()
export abstract class AbstractOverlayPresentational<TData extends OverlayData, UExtra = any> extends CoreAbstractComponent {
  protected _data: TData;

  set data(value: TData) {
    this._data = value;
    this._postUpdateData();
  }

  get data(): TData {
    return this._data;
  }

  extra: UExtra;

  overlayRef: OverlayRef;

  @HostListener("mouseleave", ["$event"])
  hide($event: MouseEvent): void {
    const tagName = this._elementRef.nativeElement.tagName.toLowerCase();
    const parentElement = DomHelper.getParentWithTag($event.relatedTarget as Element, [tagName], SsrUtil.window?.document.parentElement);
    if (isNullOrUndefined(parentElement)) {
      this.overlayRef.detach();
    }
  }

  constructor(protected readonly _elementRef: ElementRef) {
    super();
  }

  protected _postUpdateData(): void {
  }
}
