/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - status.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Injector,
  Input,
} from "@angular/core";
import {StatusModel} from "../../../../core-resources/models/status.model";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-status",
  templateUrl: "./status.presentational.html",
  styleUrls: ["./status.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatusPresentational extends AbstractInternalPresentational {
  @Input()
  statusModel: StatusModel;

  @HostBinding("class.is-label")
  @Input()
  label: string | undefined = this.labelTranslateInterface.coreStatus;

  @HostBinding("class.is-label-inline")
  @Input()
  isInline: boolean = false;

  @HostBinding("class.take-all-width")
  @Input()
  takeAllWidth: boolean = false;

  constructor(protected readonly _injector: Injector) {
    super(_injector);
  }
}
