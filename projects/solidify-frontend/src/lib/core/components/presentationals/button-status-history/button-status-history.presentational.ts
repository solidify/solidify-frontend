/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - button-status-history.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Output,
} from "@angular/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {ObservableUtil} from "../../../utils/observable.util";
import {AbstractInternalPresentational} from "../abstract-internal/abstract-internal.presentational";

@Component({
  selector: "solidify-button-status-history",
  templateUrl: "./button-status-history.presentational.html",
  styleUrls: ["./button-status-history.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonStatusHistoryPresentational extends AbstractInternalPresentational {
  private readonly _showHistoryBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("showHistory")
  readonly showHistoryObs: Observable<void> = ObservableUtil.asObservable(this._showHistoryBS);

  constructor(protected readonly _injector: Injector) {
    super(_injector);
  }

  showHistory(): void {
    this._showHistoryBS.next();
  }
}
