/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - markdown-viewer.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import md from "markdown-it";
import {
  take,
  tap,
} from "rxjs/operators";
import {isNullOrUndefinedOrWhiteString} from "../../../../core-resources/tools/is/is.tool";
import {AbstractContainer} from "../abstract/abstract.container";

@Component({
  selector: "solidify-markdown-viewer-container",
  templateUrl: "./markdown-viewer.container.html",
  styleUrls: ["./markdown-viewer.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MarkdownViewerContainer extends AbstractContainer {
  private _markdownFilePath: string;

  @Input()
  set markdownFilePath(value: string) {
    this._markdownFilePath = value;
    if (isNullOrUndefinedOrWhiteString(this.markdownFilePath)) {
      return;
    }
    this.subscribe(this._httpClient.get(this.markdownFilePath, {responseType: "text"}).pipe(
      take(1),
      tap(fileContent => this.markdownText = fileContent),
    ));
  }

  get markdownFilePath(): string {
    return this._markdownFilePath;
  }

  @Input()
  set markdownText(value: string) {
    if (isNullOrUndefinedOrWhiteString(value)) {
      return;
    }
    const markdownIt = md();
    try {
      this.markdownRendered = markdownIt.render(value);
    } catch (e) {

    }
    this._changeDetector.detectChanges();
  }

  markdownRendered: string;

  constructor(private readonly _httpClient: HttpClient,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }
}
