/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - resource-name.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
  Type,
} from "@angular/core";
import {Store} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
} from "rxjs/operators";
import {
  isFunction,
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {StringUtil} from "../../../../core-resources/utils/string.util";
import {BaseResource} from "../../../../core-resources/models/dto/base-resource.model";
import {Label} from "../../../models/label.model";
import {ResourceNameSpace} from "../../../stores/abstract/resource/resource-namespace.model";
import {ResourceState} from "../../../stores/abstract/resource/resource.state";
import {MemoizedUtil} from "../../../utils/stores/memoized.util";
import {AbstractContainer} from "../abstract/abstract.container";

@Component({
  selector: "solidify-resource-name-container",
  templateUrl: "./resource-name.container.html",
  styleUrls: ["./resource-name.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceNameContainer<TResource extends BaseResource> extends AbstractContainer implements OnChanges {
  label: string | Label[];
  listObs: Observable<TResource[]>;
  isLoading: boolean = false;

  private _currentResource: TResource;

  set currentResource(value: TResource) {
    this._currentResource = value;
    this.computeLabel();
  }

  get currentResource(): TResource {
    return this._currentResource;
  }

  @Input()
  labelKey: string;

  @Input()
  labelCallback: (resource: TResource) => string;

  @Input()
  backTranslate: boolean = false;

  @Input()
  state: Type<ResourceState<any, any>>;

  @Input()
  action: ResourceNameSpace;

  @Input()
  id: string;

  @Input()
  inline: boolean = false;

  @Input()
  callbackOnRetrieve: ((elementRef: ElementRef, resource: TResource) => void) | undefined;

  @Input()
  callbackOnClick: ((resource: TResource) => boolean) | undefined;

  constructor(private readonly _store: Store,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _elementRef: ElementRef) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);

    if (isNullOrUndefined(this.id) || isNullOrUndefined(this.action) || isNullOrUndefined(this.state) || (isNullOrUndefined(this.labelKey) && isNullOrUndefined(this.labelCallback))) {
      return;
    }

    if (isNullOrUndefined(this.listObs)) {
      this.listObs = MemoizedUtil.list(this._store, this.state);
    }

    if (isNullOrUndefined(this.id)) {
      this.label = StringUtil.stringEmpty;
      return;
    }
    const listSnapshot = MemoizedUtil.listSnapshot(this._store, this.state);

    this.computeCurrentResource(listSnapshot);

    if (isNullOrUndefined(this.currentResource)) {
      this.subscribe(this.listObs.pipe(
        distinctUntilChanged(),
        filter(list => {
          this.computeCurrentResource(list);
          return isNotNullNorUndefined(this.currentResource);
        }),
        take(1),
      ), list => {
        this.computeCurrentResource(list);
        this.isLoading = false;
        this._changeDetector.detectChanges();
      });
      if (isFunction(this.action.AddInList)) {
        this._store.dispatch(new this.action.AddInListById(this.id, true));
      } else {
        // eslint-disable-next-line no-console
        console.warn("action provide doesn't contains AddInListById method");
      }
      this.isLoading = true;
    } else {
      this.computeLabel();
    }
  }

  private computeLabel(): void {
    if (isNullOrUndefined(this.currentResource)) {
      this.label = StringUtil.stringEmpty;
      return;
    }
    if (isFunction(this.callbackOnRetrieve)) {
      this.callbackOnRetrieve(this._elementRef, this.currentResource);
      this._changeDetector.detectChanges();
    }
    if (isFunction(this.labelCallback)) {
      this.label = this.labelCallback(this.currentResource);
      return;
    }
    this.label = this.currentResource?.[this.labelKey];
  }

  private computeCurrentResource(list: TResource[]): void {
    if (isNullOrUndefined(list)) {
      return undefined;
    }
    const resource = list.find(o => o.resId === this.id);
    if (isNullOrUndefined(resource)) {
      return undefined;
    }
    this.currentResource = resource;
  }

  callCallbackOnClick(): boolean {
    return isFunction(this.callbackOnClick) ? this.callbackOnClick(this.currentResource) : false;
  }
}

