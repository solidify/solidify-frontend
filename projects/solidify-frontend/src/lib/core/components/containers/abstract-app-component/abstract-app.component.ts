/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-app.component.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterViewInit,
  ChangeDetectorRef,
  Directive,
  ElementRef,
  HostListener,
  Injector,
  makeStateKey,
  OnInit,
  Optional,
  Renderer2,
  ViewChild,
} from "@angular/core";
import {
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  fromEvent,
  Observable,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  map,
  skip,
  take,
  tap,
} from "rxjs/operators";
import {
  isFalse,
  isInstanceOf,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isString,
  isTrue,
  isTruthyObject,
} from "../../../../core-resources/tools/is/is.tool";
import {SolidifyObject} from "../../../../core-resources/types/solidify-object.type";
import {SsrUtil} from "../../../../core-resources/utils/ssr.util";
import {AppConfigService} from "../../../config/app-config.service";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {CookieConsentService} from "../../../cookie-consent/services/cookie-consent.service";
import {LanguagePartialEnum} from "../../../enums/partial/language-partial.enum";
import {ThemePartialEnum} from "../../../enums/partial/theme-partial.enum";
import {LocalStorageHelper} from "../../../helpers/local-storage.helper";
import {SessionStorageHelper} from "../../../helpers/session-storage.helper";
import {FrontendVersion} from "../../../models/frontend-version.model";
import {StateModel} from "../../../models/stores/state.model";
import {BreakpointService} from "../../../services/breakpoint.service";
import {GoogleAnalyticsService} from "../../../services/google-analytics.service";
import {LoggingService} from "../../../services/logging.service";
import {MetaService} from "../../../services/meta.service";
import {NotificationService} from "../../../services/notification.service";
import {ScrollService} from "../../../services/scroll.service";
import {TransferStateService} from "../../../services/transfer-state.service";
import {SolidifyAppAction} from "../../../stores/abstract/app/app.action";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {MemoizedUtil} from "../../../utils/stores/memoized.util";
import {ofSolidifyActionCompleted} from "../../../utils/stores/store.tool";
import {AbstractInternalContainer} from "../abstract-internal/abstract-internal.container";

declare let gtag: (name: string, id: string, option: SolidifyObject) => void;

// @dynamic
@Directive()
export abstract class AbstractAppComponent extends AbstractInternalContainer implements OnInit, AfterViewInit {

  protected readonly _ATTRIBUTE_SRC_DIFFER_KEY: string = "src-differ";
  protected readonly _ATTRIBUTE_SRC: string = "src";

  private static readonly _THEME_ATTRIBUTE_NAME: string = "theme";
  private static readonly _DARK_MODE_ATTRIBUTE_NAME: string = "dark-mode";
  private static readonly _UI_ATTRIBUTE_NAME: string = "ui";
  private static readonly _BOOTSTRAP_FROM_ATTRIBUTE_NAME: string = "bootstrap-from";
  private static readonly _LANGUAGE_ATTRIBUTE_NAME: string = "lang";
  logo: string;

  urlStateObs: Observable<RouterStateSnapshot>;
  appLanguageObs: Observable<ExtendEnum<LanguagePartialEnum>>;
  isLoggedObs: Observable<boolean>;
  isServerOfflineObs: Observable<boolean>;
  isUnableToLoadAppObs: Observable<boolean>;
  themeObs: Observable<ExtendEnum<ThemePartialEnum>>;
  darkModeObs: Observable<boolean>;
  isTouchInterfaceObs: Observable<boolean>;

  frontendVersionObs: Observable<FrontendVersion>;

  ignoreGrid: boolean = false;
  displayCart: boolean = false;
  isMaintenanceModeActive: boolean = false;
  isBootstrappedFromServer: boolean = false;

  @ViewChild("main")
  mainElementRef: ElementRef;

  @HostListener("window:beforeunload", ["$event"])
  leaveWithoutSaveNotification($event: Event): void {
    const preventExit = MemoizedUtil.selectSnapshot(this._store, this.environment.appState, state => state.preventExit);
    const ignorePreventExit = MemoizedUtil.selectSnapshot(this._store, this.environment.appState, state => state.ignorePreventLeavePopup);
    if (isFalse(ignorePreventExit) && isTrue(preventExit)) {
      $event.returnValue = true;
    }
  }

  get sessionStorageHelper(): typeof SessionStorageHelper {
    return SessionStorageHelper;
  }

  get localStorageHelper(): typeof LocalStorageHelper {
    return LocalStorageHelper;
  }

  get currentAppLanguage(): ExtendEnum<ThemePartialEnum> {
    return MemoizedUtil.selectSnapshot(this._store, this.environment.appState, state => state.appLanguage);
  }

  get currentTheme(): ExtendEnum<ThemePartialEnum> {
    return MemoizedUtil.selectSnapshot(this._store, this.environment.appState, state => state.theme);
  }

  get currentDarkMode(): boolean {
    return MemoizedUtil.selectSnapshot(this._store, this.environment.appState, state => state.darkMode);
  }

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _router: Router,
              protected readonly _translate: TranslateService,
              protected readonly _renderer: Renderer2,
              protected readonly _injector: Injector,
              protected readonly _notificationService: NotificationService,
              protected readonly _loggingService: LoggingService,
              public readonly breakpointService: BreakpointService,
              protected readonly _scrollService: ScrollService,
              protected readonly _googleAnalyticsService: GoogleAnalyticsService,
              protected readonly _cookieConsentService: CookieConsentService,
              protected readonly _appConfigService: AppConfigService,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _metaService: MetaService,
              @Optional() protected readonly _transferState: TransferStateService,
  ) {
    super(_injector);
    this._metaService.meta.updateTag({name: SOLIDIFY_CONSTANTS.ONLINE, content: SOLIDIFY_CONSTANTS.STRING_TRUE});
    this._removeGlobalSpinner();

    this.urlStateObs = this._store.select((state: StateModel) => state.router.state);
    this.frontendVersionObs = MemoizedUtil.select(this._store, this.environment.appState, state => state.frontendVersion);
    this.appLanguageObs = MemoizedUtil.select(this._store, this.environment.appState, state => state.appLanguage);
    this.isLoggedObs = MemoizedUtil.select(this._store, this.environment.appState, state => state.isLoggedIn);
    this.isServerOfflineObs = MemoizedUtil.select(this._store, this.environment.appState, state => state.isServerOffline);
    this.isUnableToLoadAppObs = MemoizedUtil.select(this._store, this.environment.appState, state => state.isUnableToLoadApp);
    this.themeObs = MemoizedUtil.select(this._store, this.environment.appState, state => state.theme);
    this.darkModeObs = MemoizedUtil.select(this._store, this.environment.appState, state => state.darkMode);
    this.isTouchInterfaceObs = MemoizedUtil.select(this._store, this.environment.appState, state => state.isTouchInterface);

    this._setOverrideCssStyleSheet();
    this._doThemeChange(this.currentTheme);
    this._doDarkMode(this.currentDarkMode);
    this._googleAnalyticsService.init(this._renderer);
    this._loadStaticExtraMetas();
    this._setFromWhereAppIsBootstrapped();
  }

  // Override if necessary
  protected _isLoggedInRoot(): boolean {
    return false;
  }

  ngOnInit(): void {
    super.ngOnInit();

    this._findMetaToUpdate();

    if (SsrUtil.isBrowser) {
      this.subscribe(this._listenLanguageChange());
      this.subscribe(this._listenLanguageReload());
      this.subscribe(this._observeDarkModeChange());
      this.subscribe(this._observeThemeChange());
      this.subscribe(this._observeToUpdateMaintenanceMode());
      this.subscribe(this._observeIsTouchInterfaceChange());
      this.subscribe(this._observeTouchEvent());
      this.subscribe(this._observeCurrentUrlForMetaUpdate());
      this._cookieConsentService.openBannerIfNeverAnswerOrNewerCookie();
    } else {
      this._waitAppIsReadyBeforeRenderingInServerSide();
    }
  }

  private _removeGlobalSpinner(): void {
    const body = SsrUtil.document.body;
    const globalSpinner = body.querySelector(".global-spinner-wrapper");
    if (isNotNullNorUndefined(globalSpinner)) {
      this._renderer.removeChild(body, globalSpinner);
    }
  }

  /**
   * Allow to wait end of application load before render app in SSR
   *
   * @param count
   * @private
   */
  private _waitAppIsReadyBeforeRenderingInServerSide(count: number = 100): void {
    if (count <= 0) {
      // eslint-disable-next-line no-console
      console.error("Expiration of delay to render ssr page");
      return;
    }
    count--;
    const isApplicationInitialized = MemoizedUtil.selectSnapshot(this._store, this.environment.appState, state => state.isApplicationInitialized);
    const isServerOffline = MemoizedUtil.selectSnapshot(this._store, this.environment.appState, state => state.isServerOffline);
    const isUnableToLoadApp = MemoizedUtil.selectSnapshot(this._store, this.environment.appState, state => state.isUnableToLoadApp);
    if (isApplicationInitialized || isServerOffline || isUnableToLoadApp) {
      return;
    }
    setTimeout(() => this._waitAppIsReadyBeforeRenderingInServerSide(count), 50);
  }

  private _loadStaticExtraMetas(): void {
    this.environment.listMetas?.forEach(meta => {
      this._metaService.meta.addTag(meta);
    });
  }

  private _setFromWhereAppIsBootstrapped(): void {
    const STATE_KEY = makeStateKey<boolean>("AppComponentIsBootstrapedFromServer");
    if (this._transferState?.hasKey(STATE_KEY)) {
      this.isBootstrappedFromServer = this._transferState.get(STATE_KEY, false);
      this._transferState.remove(STATE_KEY);
    }
    if (SsrUtil.isServer) {
      this.isBootstrappedFromServer = true;
      this._transferState?.set(STATE_KEY, this.isBootstrappedFromServer);
    }
    this._setBootstrapFromAttribute(this.isBootstrappedFromServer);
  }

  private _observeCurrentUrlForMetaUpdate(): Observable<string> {
    return this.urlStateObs
      .pipe(
        filter(urlState => isNotNullNorUndefined(urlState)),
        map(urlState => urlState.url),
        distinctUntilChanged(),
        skip(1),
        tap(urlRequested => {
          this._findMetaToUpdate(urlRequested);
        }),
      );
  }

  private _findMetaToUpdate(urlRequested: string = SsrUtil.window.location.pathname): void {
    this._metaService.findMetaToUpdate(urlRequested);
  }

  private _observeToUpdateMaintenanceMode(): Observable<any> {
    this.isMaintenanceModeActive = isTrue(this.environment.maintenanceMode);
    return this._appConfigService.environmentUpdatedObs
      .pipe(
        map(environment => {
          this.isMaintenanceModeActive = isTrue(this.environment.maintenanceMode);
          this._changeDetector.detectChanges();
          return this.isMaintenanceModeActive;
        }),
      );
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
    if (isTruthyObject(this.mainElementRef)) {
      this._scrollService.init(this.mainElementRef);
    }
  }

  private _observeTouchEvent(): Observable<Event> {
    return fromEvent(SsrUtil.window?.document, "touchstart")
      .pipe(
        take(1),
        tap(event => this._store.dispatch(new SolidifyAppAction.SetIsTouchInterface(true))),
      );
  }

  private _observeIsTouchInterfaceChange(): Observable<boolean> {
    return this.isTouchInterfaceObs.pipe(
      distinctUntilChanged(),
      skip(1),
      tap((isTouchInterface: boolean) => {
        this._setTouchInterfaceAttribute(isTouchInterface);
      }),
    );
  }

  navigate(route: string | Navigate): void {
    if (isString(route)) {
      this._store.dispatch(new Navigate([route]));
    } else if (isInstanceOf(route, Navigate)) {
      this._store.dispatch(route);
    }
  }

  private _setThemeAttribute(theme: ExtendEnum<ThemePartialEnum>): void {
    this._renderer.setAttribute(SsrUtil.document.body, AbstractAppComponent._THEME_ATTRIBUTE_NAME, String(theme));
  }

  private _setDarkModeAttribute(enabled: boolean): void {
    this._renderer.setAttribute(SsrUtil.document.body, AbstractAppComponent._DARK_MODE_ATTRIBUTE_NAME, enabled ? SOLIDIFY_CONSTANTS.STRING_TRUE : SOLIDIFY_CONSTANTS.STRING_FALSE);
  }

  private _setTouchInterfaceAttribute(isTouchInterface: boolean): void {
    this._renderer.setAttribute(SsrUtil.document.body, AbstractAppComponent._UI_ATTRIBUTE_NAME, isTouchInterface ? "touch" : "standard");
  }

  private _setLanguageAttribute(language: ExtendEnum<LanguagePartialEnum>): void {
    this._renderer.setAttribute(SsrUtil.document.body.parentElement, AbstractAppComponent._LANGUAGE_ATTRIBUTE_NAME, language);
  }

  private _setBootstrapFromAttribute(isBootstrapFromSsr: boolean): void {
    this._renderer.setAttribute(SsrUtil.document.body, AbstractAppComponent._BOOTSTRAP_FROM_ATTRIBUTE_NAME, isBootstrapFromSsr ? "ssr" : "browser");
  }

  private _setFavicon(theme: ExtendEnum<ThemePartialEnum>): void {
    const header = SsrUtil.document.querySelector("head");
    const oldFavicon = SsrUtil.document.querySelector("head link[type='image/x-abstract-icon']");
    let link = SsrUtil.document.createElement("link");
    if (isNotNullNorUndefined(oldFavicon)) {
      link = oldFavicon as any;
    }
    this._renderer.setAttribute(link, "type", "image/x-icon");
    this._renderer.setAttribute(link, "rel", "icon");
    this._renderer.setAttribute(link, "href", `./assets/themes/${theme}/favicon.ico`);
    this._renderer.appendChild(header, link);
  }

  private _setWebManifest(theme: ExtendEnum<ThemePartialEnum>): void {
    const header = SsrUtil.document.querySelector("head");
    const oldManifest = SsrUtil.document.querySelector("head link[rel='manifest']");
    let link = SsrUtil.document.createElement("link");
    if (isNotNullNorUndefined(oldManifest)) {
      link = oldManifest as any;
    }
    this._renderer.setAttribute(link, "rel", "manifest");
    this._renderer.setAttribute(link, "href", `./assets/themes/${theme}/manifest.webmanifest.json`);
    this._renderer.appendChild(header, link);
  }

  private _setMetaColor(): void {
    const header = SsrUtil.document.querySelector("head");
    const oldMetaColor = SsrUtil.document.querySelector("head meta[name='theme-color']");
    let meta = SsrUtil.document.createElement("meta");
    if (isNotNullNorUndefined(oldMetaColor)) {
      meta = oldMetaColor as any;
    }
    this._renderer.setAttribute(meta, "name", "theme-color");
    this._renderer.setAttribute(meta, "content-color", this.environment.metaThemeColor);
    this._renderer.appendChild(header, meta);
  }

  private _setImageToolbar(theme: ExtendEnum<ThemePartialEnum>): void {
    this.logo = `./assets/themes/${theme}/toolbar-header-image.png`;
  }

  logout(): void {
    this._store.dispatch(new SolidifyAppAction.Logout());
  }

  languageChange(language: ExtendEnum<LanguagePartialEnum>): void {
    if (language !== this.currentAppLanguage) {
      this._store.dispatch(new SolidifyAppAction.ChangeAppLanguage(language));
    }
  }

  darkThemeChange(enabled: boolean): void {
    if (enabled !== this.currentDarkMode) {
      this._store.dispatch(new SolidifyAppAction.ChangeDarkMode(enabled));
    }
  }

  themeChange(theme: ExtendEnum<ThemePartialEnum>): void {
    if (theme !== this.currentTheme) {
      this._store.dispatch(new SolidifyAppAction.ChangeAppTheme(theme));
    }
  }

  navigateToHome(): void {
    this.navigate(this.environment.routeHomePage);
  }

  private _observeThemeChange(): Observable<ExtendEnum<ThemePartialEnum>> {
    return this.themeObs.pipe(
      distinctUntilChanged(),
      skip(1),
      tap((theme: ExtendEnum<ThemePartialEnum>) => {
        this._doThemeChange(theme);
        this._store.dispatch(new SolidifyAppAction.ReloadAppLanguage());
      }),
    );
  }

  private _doThemeChange(theme: ExtendEnum<ThemePartialEnum>): void {
    this._setThemeAttribute(theme);
    this._setFavicon(theme);
    this._setWebManifest(theme);
    this._setImageToolbar(theme);
    this._setMetaColor();
  }

  private _observeDarkModeChange(): Observable<boolean> {
    return this.darkModeObs.pipe(
      distinctUntilChanged(),
      skip(1),
      tap((enabled: boolean) => this._doDarkMode(enabled)),
    );
  }

  private _doDarkMode(enabled: boolean): void {
    this._setDarkModeAttribute(enabled);
  }

  private _listenLanguageChange(): Observable<ExtendEnum<LanguagePartialEnum>> {
    return this._actions$.pipe(
      ofSolidifyActionCompleted(SolidifyAppAction.ChangeAppLanguageSuccess),
      map(actionCompleted => {
        const language = actionCompleted.action.parentAction.language;
        this._setLanguageAttribute(language);
        this._findMetaToUpdate();
        return language;
      }),
    );
  }

  private _listenLanguageReload(): Observable<void> {
    return this._actions$.pipe(
      ofSolidifyActionCompleted(SolidifyAppAction.ReloadAppLanguageSuccess),
      map(actionCompleted => {
        this._findMetaToUpdate();
      }),
    );
  }

  private _setOverrideCssStyleSheet(): void {
    const header = SsrUtil.document.querySelector("head");
    const link = SsrUtil.document.createElement("link");
    this._renderer.setAttribute(link, "type", "text/css");
    this._renderer.setAttribute(link, "rel", "stylesheet");
    this._renderer.setAttribute(link, "href", `./assets/styles/override.css`);
    this._renderer.appendChild(header, link);
  }

  protected _getValueScriptDiffer(script: Element): string | undefined {
    if (isNullOrUndefined(script) || isNullOrUndefined(script.attributes)) {
      return undefined;
    }
    const attribute = script.attributes[this._ATTRIBUTE_SRC_DIFFER_KEY];
    return isNullOrUndefined(attribute) ? undefined : attribute.value;
  }

  protected _getScriptWithValueEndWith(listScript: Element[], threeDimensionalLib: string): Element | undefined {
    return listScript.find(script => {
      const value = this._getValueScriptDiffer(script);
      return isNotNullNorUndefined(value) && value.endsWith(threeDimensionalLib);
    });
  }

  scrollHandler($event: Event): void {
    this._scrollService.triggerScrollEventMain($event);
  }
}
