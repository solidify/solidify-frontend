/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - tabs.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  ActivatedRoute,
  NavigationCancel,
  NavigationEnd,
  Router,
} from "@angular/router";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
  of,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  tap,
} from "rxjs/operators";
import {
  isArray,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isObservable,
} from "../../../../core-resources/tools/is/is.tool";
import {Tab} from "../../../models/tab.model";
import {ObservableUtil} from "../../../utils/observable.util";
import {AbstractInternalContainer} from "../abstract-internal/abstract-internal.container";

@Component({
  selector: "solidify-tabs-container",
  templateUrl: "./tabs.container.html",
  styleUrls: ["./tabs.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TabsContainer extends AbstractInternalContainer implements OnInit {
  @Input()
  tabs: Tab[];

  @Input()
  initialTabActiveId: string | undefined;

  @Input()
  isLoading: boolean;

  @Input()
  suffixUrlMatcher: (route: ActivatedRoute) => string = (route: ActivatedRoute) => {
    const children = route.snapshot.children;
    if (children.length > 0 && children[0].url.length > 0) {
      const mode = children[0].url[0].path;
      return mode;
    }
    return undefined;
    // eslint-disable-next-line @stylistic/semi, @stylistic/member-delimiter-style
  };

  private readonly _tabBS: BehaviorSubject<Tab> = new BehaviorSubject<Tab>(undefined);
  @Output("tabChange")
  readonly tabObs: Observable<Tab> = ObservableUtil.asObservable(this._tabBS);

  currentTab: Tab;

  @Input()
  tabTourAnchor: string;

  constructor(private readonly _store: Store,
              private readonly _router: Router,
              private readonly _route: ActivatedRoute,
              protected readonly _injector: Injector) {
    super(_injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    if (isNotNullNorUndefined(this.initialTabActiveId)) {
      this.currentTab = this.tabs.find(t => t.id === this.initialTabActiveId);
      this._tabBS.next(this.currentTab);
    }
    this._computeCurrentTabFromRoute();
    this.subscribe(this._router.events
      .pipe(
        filter(event => event instanceof NavigationCancel || event instanceof NavigationEnd),
        distinctUntilChanged(),
        tap(event => {
          this._computeCurrentTabFromRoute();
        }),
      ),
    );
  }

  trackByFn(index: number, tab: Tab): string {
    return tab.id;
  }

  private _computeCurrentTabFromRoute(): Tab | undefined {
    const tabRouteSelected = this.suffixUrlMatcher(this._route);
    const tabIndex = this.tabs.findIndex(t => t.suffixUrl === tabRouteSelected);
    if (tabIndex === -1) {
      return undefined;
    }
    this.currentTab = this.tabs[tabIndex];
    this._tabBS.next(this.currentTab);
    return this.currentTab;
  }

  private dispatchActionNavigateToTab(selectedTab: Tab): Observable<any> {
    const path = selectedTab.route();
    return this._store.dispatch(new Navigate(isArray(path) ? path : [path]));
  }

  isDisplayed(tab: Tab): Observable<boolean> {
    if (isNullOrUndefined(tab.conditionDisplay)) {
      return of(true);
    }
    if (isObservable(tab.conditionDisplay())) {
      return tab.conditionDisplay() as Observable<boolean>;
    } else {
      return of(tab.conditionDisplay() as boolean);
    }
  }

  navigateToTab(tab: Tab): void {
    if (this.currentTab === tab) {
      return;
    }
    if (isNullOrUndefined(tab.route)) {
      this.currentTab = tab;
      return;
    }
    this.dispatchActionNavigateToTab(tab);
  }
}
