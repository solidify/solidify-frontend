/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - public_api.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

// !!! Order matter here : If wrong order you should have an error like : "Super expression must either be null or a function"
export * from "./core-abstract/core-abstract.component";

// CONTAINERS
export * from "./containers/abstract/abstract.container";
export * from "./containers/abstract-app-component/abstract-app.component";
export * from "./containers/empty-container/empty.container";
export * from "./containers/markdown-viewer/markdown-viewer.container";
export * from "./containers/resource-name/resource-name.container";
export * from "./containers/tabs/tabs.container";
export * from "./containers/resource-name/resource-name.container";

// DIALOGS
export * from "./dialogs/abstract/abstract-dialog-interface.model";
export * from "./dialogs/abstract/abstract.dialog";
export * from "./dialogs/base-action/base-action.dialog";
export * from "./dialogs/base-info/base-info.dialog";
export * from "./dialogs/confirm/confirm.dialog";
export * from "./dialogs/data-table-loose-selection/data-table-loose-selection.dialog";
export * from "./dialogs/delete/delete.dialog";
export * from "./dialogs/first-login/first-login.dialog";
export * from "./dialogs/keyword-paste/keyword-paste.dialog";
export * from "./dialogs/privacy-policy-terms-of-use-approval/privacy-policy-terms-of-use-approval.dialog";

// PRESENTATIONALS
export * from "./presentationals/abstract/abstract.presentational";
export * from "./presentationals/abstract-can-deactivate/abstract-can-deactivate.presentational";
export * from "./presentationals/abstract-content/abstract-content.presentational";
export * from "./presentationals/abstract-form/abstract-form.presentational";
export * from "./presentationals/abstract-overlay/abstract-overlay.presentational";
export * from "./presentationals/abstract-snackbar/abstract-snackbar.presentational";
export * from "./presentationals/banner/banner.presentational";
export * from "./presentationals/breadcrumb/breadcrumb.presentational";
export * from "./presentationals/button-id/button-id.presentational";
export * from "./presentationals/button-orcid/button-orcid.presentational";
export * from "./presentationals/button-refresh/button-refresh.presentational";
export * from "./presentationals/button-status-history/button-status-history.presentational";
export * from "./presentationals/citations/citations.presentational";
export * from "./presentationals/detail-presentation/detail-presentation.presentational";
export * from "./presentationals/empty-form-control-wrapper/empty-form-control-wrapper.presentational";
export * from "./presentationals/form-error-wrapper/form-error-wrapper.presentational";
export * from "./presentationals/icon/icon.presentational";
export * from "./presentationals/label/label.presentational";
export * from "./presentationals/language-selector/language-selector.presentational";
export * from "./presentationals/language-selector-menu/language-selector-menu.presentational";
export * from "./presentationals/orcid-sign-in-button/orcid-sign-in-button.presentational";

export * from "./presentationals/paginator/paginator.presentational";
export * from "./presentationals/panel-expandable/panel-expandable.presentational";
export * from "./presentationals/ribbon/ribbon.presentational";

export * from "./presentationals/snackbar/snackbar.presentational";
export * from "./presentationals/star-rating/star-rating.presentational";
export * from "./presentationals/status/status.presentational";
export * from "./presentationals/table-module-version/table-module-version.presentational";
export * from "./presentationals/theme-selector/theme-selector.presentational";
export * from "./presentationals/theme-selector-menu/theme-selector-menu.presentational";

// ROUTABLES
export * from "./routables/abstract/abstract.routable";
export * from "./routables/abstract-can-deactivate/abstract-can-deactivate.routable";
export * from "./routables/abstract-full-page-mode/abstract-full-page-mode.routable";
export * from "./routables/abstract-home/abstract-home.routable";
export * from "./routables/changelog/changelog.routable";
export * from "./routables/component-app-summary/component-app-summary.routable";
export * from "./routables/empty/empty.routable";
export * from "./routables/icon-app-summary/icon-app-summary.routable";
export * from "./routables/maintenance-mode/maintenance-mode.routable";
export * from "./routables/orcid-redirect/orcid-redirect.routable";
export * from "./routables/release-notes/release-notes.routable";
export * from "./routables/server-offline-mode/server-offline-mode.routable";
export * from "./routables/unable-to-load-app/unable-to-load-app.routable";
