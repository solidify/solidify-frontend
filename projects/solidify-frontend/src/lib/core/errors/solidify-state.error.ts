/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-state.error.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {isTruthyObject} from "../../core-resources/tools/is/is.tool";
import {SolidifyHttpErrorResponseModel} from "../models/errors/solidify-http-error-response.model";
import {BaseStateModel} from "../models/stores/base-state.model";
import {BasicState} from "../stores/abstract/base/basic.state";
import {SolidifyError} from "./solidify.error";

export class SolidifyStateError<TState extends BasicState<TStateModel>, TStateModel extends BaseStateModel> extends SolidifyError {

  constructor(public state: TState,
              nativeError?: SolidifyHttpErrorResponseModel) {
    super("Solidify State Error", nativeError);
  }

  getStateName(): string | undefined {
    return isTruthyObject(this.state) ? this.state.stateName : undefined;
  }
}
