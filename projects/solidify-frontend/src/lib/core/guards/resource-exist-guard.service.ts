/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - resource-exist-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
} from "@angular/router";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  Observable,
  of,
} from "rxjs";
import {map} from "rxjs/operators";
import {isNullOrUndefined} from "../../core-resources/tools/is/is.tool";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {ApiService} from "../http/api.service";
import {SolidifyRouteData} from "../models/route/route-data.model";
import {AbstractBaseService} from "../services/abstract-base.service";
import {ResourceActionHelper} from "../stores/abstract/resource/resource-action.helper";
import {ResourceNameSpace} from "../stores/abstract/resource/resource-namespace.model";
import {RoutingUtil} from "../utils/routing.util";
import {MemoizedUtil} from "../utils/stores/memoized.util";
import {ofSolidifyActionCompleted} from "../utils/stores/store.tool";
import {StoreUtil} from "../utils/stores/store.util";

@Injectable({
  providedIn: "root",
})
export class ResourceExistGuardService extends AbstractBaseService implements CanActivate {
  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              public readonly router: Router,
              public readonly apiService: ApiService) {
    super();
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const fullUrl = RoutingUtil.generateFullUrlStringFromActivatedRouteSnapshot(route);
    const data = route.data as SolidifyRouteData;
    const nameSpace: ResourceNameSpace = data.nameSpace;

    if (isNullOrUndefined(nameSpace)) {
      // eslint-disable-next-line no-console
      console.error("Missing routing data 'nameSpace' needed for ResourceExistGuardService on route " + fullUrl);
      return of(true);
    }

    const resourceState = data.resourceState;

    if (isNullOrUndefined(resourceState)) {
      // eslint-disable-next-line no-console
      console.error("Missing routing data 'resourceState' needed for ResourceExistGuardService on route " + fullUrl);
      return of(true);
    }

    const resourceId: string = route.params[isNullOrUndefined(data.paramId) ? SOLIDIFY_CONSTANTS.ID : data.paramId];
    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(this._store, [
      {
        action: ResourceActionHelper.getById(nameSpace, resourceId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(nameSpace.GetByIdSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(nameSpace.GetByIdFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          const resource = MemoizedUtil.currentSnapshot(this._store, resourceState);

          if (isNullOrUndefined(resource) || resource.resId !== resourceId) {
            return this._redirectToPageNotFound(resourceId, data.routeNotFound);
          }
          return true;
        } else {
          return this._redirectToPageNotFound(resourceId, data.routeNotFound);
        }
      }),
    );
  }

  private _redirectToPageNotFound(id: string, routeNotFound: string | undefined): false {
    this._store.dispatch(new Navigate([isNullOrUndefined(routeNotFound) ? "not-found" : routeNotFound, id]));
    return false;
  }
}
