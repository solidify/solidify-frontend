/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - combined-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
  Injector,
  Type,
} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
  UrlTree,
} from "@angular/router";
import {lastValueFrom} from "rxjs";
import {
  isEmptyArray,
  isFalse,
  isObservable,
  isPromise,
} from "../../core-resources/tools/is/is.tool";
import {CoreSolidifyEnvironment} from "../environments/environment.solidify-core";
import {ENVIRONMENT} from "../injection-tokens/environment.injection-token";
import {SolidifyRouteData} from "../models/route/route-data.model";
import {AbstractBaseService} from "../services/abstract-base.service";
import {LoggingService} from "../services/logging.service";

@Injectable({
  providedIn: "root",
})
export class CombinedGuardService extends AbstractBaseService implements CanActivate {
  constructor(@Inject(ENVIRONMENT) private readonly _environment: CoreSolidifyEnvironment,
              private readonly _injector: Injector,
              private readonly _loggingService: LoggingService) {
    super();
  }

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean | UrlTree> {
    const guards = (route.data as SolidifyRouteData).guards || [];
    if (this._shouldLog && isEmptyArray(guards)) {
      this._loggingService.logWarning(`No guard defined for route ${state.url} in property 'guards' as expected`);
    }
    for (const guard of guards) {
      const instance: CanActivate = this._injector.get(guard);
      let result = instance.canActivate(route, state);

      if (isPromise(result)) {
        if (this._shouldLog) {
          this._loggingService.logInfo(`${this._getGuardIdentifierString(guard, state)}: wait promise response`);
        }
        result = await result;
      } else if (isObservable(result)) {
        if (this._shouldLog) {
          this._loggingService.logInfo(`${this._getGuardIdentifierString(guard, state)}: wait observable response`);
        }
        result = await lastValueFrom(result); // if subscription of guard is not closed, the await is never resolved. Consider using take(1) in guards.
      }

      if (isFalse(result) || result instanceof UrlTree) {
        if (this._shouldLog) {
          this._loggingService.logWarning(`${this._getGuardIdentifierString(guard, state)}: ` + (isFalse(result) ? `reject navigation` : `redirect to path ${result.toString()}`));
        }
        return result;
      } else if (this._shouldLog) {
        this._loggingService.logInfo(`${this._getGuardIdentifierString(guard, state)}: accept navigation`);
      }
    }
    return true;
  }

  private get _shouldLog(): boolean {
    return isFalse(this._environment.production) || this._environment.showDebugInformation;
  }

  private _getGuardIdentifierString(guard: Type<CanActivate>, state: RouterStateSnapshot): string {
    return `guard [${guard.name}] on route '${state.url}`;
  }
}
