/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - composition-exist-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
} from "@angular/router";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  Observable,
  of,
} from "rxjs";
import {map} from "rxjs/operators";
import {isNullOrUndefined} from "../../core-resources/tools/is/is.tool";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {AppRoutesPartialEnum} from "../enums/partial/app-routes-partial.enum";
import {ApiService} from "../http/api.service";
import {BaseResource} from "../../core-resources/models/dto/base-resource.model";
import {SolidifyRouteData} from "../models/route/route-data.model";
import {AbstractBaseService} from "../services/abstract-base.service";
import {CompositionActionHelper} from "../stores/abstract/composition/composition-action.helper";
import {CompositionNameSpace} from "../stores/abstract/composition/composition-namespace.model";
import {RoutingUtil} from "../utils/routing.util";
import {MemoizedUtil} from "../utils/stores/memoized.util";
import {ofSolidifyActionCompleted} from "../utils/stores/store.tool";
import {StoreUtil} from "../utils/stores/store.util";

@Injectable({
  providedIn: "root",
})
export class CompositionExistGuardService extends AbstractBaseService implements CanActivate {
  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              public readonly router: Router,
              public readonly apiService: ApiService) {
    super();
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const fullUrl = RoutingUtil.generateFullUrlStringFromActivatedRouteSnapshot(route);
    const data = route.data as SolidifyRouteData;
    const compositionNameSpace: CompositionNameSpace = data.compositionNameSpace;

    if (isNullOrUndefined(compositionNameSpace)) {
      // eslint-disable-next-line no-console
      console.error("Missing routing data 'compositionNameSpace' needed for ResourceExistGuardService on route " + fullUrl);
      return of(true);
    }

    const compositionState = data.compositionState;

    if (isNullOrUndefined(compositionState)) {
      // eslint-disable-next-line no-console
      console.error("Missing routing data 'compositionState' needed for ResourceExistGuardService on route " + fullUrl);
      return of(true);
    }

    const parentCompositionId: string = route.params[isNullOrUndefined(data.paramIdCompositionParent) ? AppRoutesPartialEnum.paramIdParentCompositionWithoutPrefixParam : data.paramIdCompositionParent];
    const resourceId: string = route.params[isNullOrUndefined(data.paramId) ? SOLIDIFY_CONSTANTS.ID : data.paramId];
    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(this._store, [
      {
        action: CompositionActionHelper.getById(compositionNameSpace, parentCompositionId, resourceId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(compositionNameSpace.GetByIdSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(compositionNameSpace.GetByIdFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          const resource: BaseResource = MemoizedUtil.currentSnapshot(this._store, compositionState);

          if (isNullOrUndefined(resource) || resource.resId !== resourceId) {
            return this._redirectToPageNotFound(resourceId, data.routeNotFound);
          }
          return true;
        } else {
          return this._redirectToPageNotFound(resourceId, data.routeNotFound);
        }
      }),
    );
  }

  private _redirectToPageNotFound(id: string, routeNotFound: string | undefined): false {
    this._store.dispatch(new Navigate([isNullOrUndefined(routeNotFound) ? "not-found" : routeNotFound, id]));
    return false;
  }
}
