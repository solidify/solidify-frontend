/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - prevent-leave-maintenance-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {CanDeactivate} from "@angular/router";
import {isFalse} from "../../core-resources/tools/is/is.tool";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../injection-tokens/environment.injection-token";
import {ICanDeactivate} from "../models/can-deactivate.model";
import {AbstractBaseService} from "../services/abstract-base.service";

@Injectable({
  providedIn: "root",
})
export class PreventLeaveMaintenanceGuardService extends AbstractBaseService implements CanDeactivate<ICanDeactivate> {
  constructor(@Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super();
  }

  // Override if necessary
  protected _isLoggedInRoot(): boolean {
    return false;
  }

  canDeactivate(component: ICanDeactivate): boolean {
    return isFalse(this._environment.maintenanceMode) || this._isLoggedInRoot();
  }
}
