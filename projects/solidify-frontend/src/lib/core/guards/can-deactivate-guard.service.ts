/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - can-deactivate-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanDeactivate,
  RouterStateSnapshot,
} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {
  isFunction,
  isNullOrUndefined,
} from "../../core-resources/tools/is/is.tool";
import {LabelTranslateInterface} from "../../label-translate-interface.model";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../injection-tokens/label-to-translate.injection-token";
import {ICanDeactivate} from "../models/can-deactivate.model";
import {AbstractBaseService} from "../services/abstract-base.service";
import {SolidifyAppAction} from "../stores/abstract/app/app.action";
import {MemoizedUtil} from "../utils/stores/memoized.util";

@Injectable({
  providedIn: "root",
})
export class CanDeactivateGuard extends AbstractBaseService implements CanDeactivate<ICanDeactivate> {
  constructor(private readonly _store: Store,
              private readonly _translate: TranslateService,
              @Inject(LABEL_TRANSLATE) private readonly _labelTranslate: LabelTranslateInterface,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super();
  }

  canDeactivate(component: ICanDeactivate,
                currentRoute: ActivatedRouteSnapshot,
                currentState: RouterStateSnapshot,
                nextState: RouterStateSnapshot,
  ): boolean {
    const globalUnsavedChange = MemoizedUtil.selectSnapshot(this._store, this._environment.appState, state => state.preventExit);
    const ignorePreventLeavePopup = MemoizedUtil.selectSnapshot(this._store, this._environment.appState, state => state.ignorePreventLeavePopup);
    if (!ignorePreventLeavePopup && (globalUnsavedChange || (!isNullOrUndefined(component) && isFunction(component.canDeactivate) && !component.canDeactivate()))) {
      if (confirm(this.getMessage(globalUnsavedChange))) {
        this._store.dispatch(new SolidifyAppAction.CancelPreventExit());
        return true;
      } else {
        return false;
      }
    }
    return true;
  }

  private getMessage(globalUnsavedChange: boolean): string {
    let message = this._translate.instant(this._labelTranslate.coreNotificationUnsavedChanges);
    if (globalUnsavedChange) {
      const globalMessage = MemoizedUtil.selectSnapshot(this._store, this._environment.appState, state => state.preventExitReasonToTranslate);
      if (!isNullOrUndefined(globalMessage)) {
        message = this._translate.instant(globalMessage);
      }
    }
    return message;
  }
}
