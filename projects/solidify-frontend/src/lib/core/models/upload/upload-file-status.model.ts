/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - upload-file-status.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Guid} from "../../../core-resources/models/guid.model";
import {FileUploadStatusEnum} from "../../enums/upload/file-upload-status.enum";
import {FileUploadWrapper} from "./file-upload-wrapper.model";
import {SolidifyFileUpload} from "./solidify-file-upload.model";

export interface UploadFileStatus<TResource extends SolidifyFileUpload, UFileUploadWrapper extends FileUploadWrapper = FileUploadWrapper> {
  guid: Guid;
  fileUploadWrapper: UFileUploadWrapper;
  status: FileUploadStatusEnum;
  errorMessageToTranslate: string;
  progressPercentage?: number;
  message?: string;
  result?: TResource;
  isArchive: boolean;
}
