/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - data-table-component-input.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {ValueType} from "../../enums/datatable/data-table-component-input-value-type.enum";
import {BaseResourceType} from "../../../core-resources/models/dto/base-resource.model";
import {DataTableColumns} from "./data-table-columns.model";

export interface DataTableComponentInput<TResource extends BaseResourceType = any> {
  /**
   * The input name to bind with the component instanciate
   */
  key: string;
  /**
   * The type of input binding
   */
  valueType: ValueType;
  /**
   * A static value to provide. Only used if valueType = isStatic
   */
  staticValue?: any;
  /**
   * A computed value. Only used if valueType = isCallback
   *
   * @param rowData data for this row
   * @param col column definition
   */
  callbackValue?: (rowData: TResource, col: DataTableColumns<TResource>) => any;
  /**
   * An attribute to retrieve into DataTableColumns. Only used if valueType = isAttributeOnCol
   */
  attributeOnCol?: string;
  /**
   * An attribute to retrieve into row data.Only used if valueType = isOtherColData
   */
  dataCol?: string;
}
