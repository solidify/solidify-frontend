/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - data-table-columns.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DataTableFieldTypeEnum} from "../../enums/datatable/data-table-field-type.enum";
import {OrderEnum} from "../../../core-resources/enums/order.enum";
import {DataTestPartialEnum} from "../../../core-resources/enums/partial/data-test-partial.enum";
import {ResourceNameSpace} from "../../stores/abstract/resource/resource-namespace.model";
import {ResourceState} from "../../stores/abstract/resource/resource.state";
import {ExtendEnum} from "../../../core-resources/types/extend-enum.type";
import {MappingObject} from "../../../core-resources/types/mapping-type.type";
import {BaseResourceWithLabels} from "../dto/base-resource-with-labels.model";
import {BaseResourceType} from "../../../core-resources/models/dto/base-resource.model";
import {KeyValue} from "../../../core-resources/models/key-value.model";
import {Sort} from "../../../core-resources/models/query-parameters/sort.model";
import {DataTableActions} from "./data-table-actions.model";
import {DataTableComponent} from "./data-table-component.model";

export interface DataTableColumns<TResource extends (BaseResourceType | any) = any> {
  /**
   * The field to retrieve in row data, if undefined all row data is used
   */
  field: keyof TResource | undefined;
  /**
   * Label in header to translate
   */
  header: string;
  /**
   * Callback to display tooltip
   *
   * @deprecated not used
   * @param value
   */
  tooltip?: (value: string) => string | undefined;
  /**
   * Define how to handle the data, and what type of filter to use
   */
  type?: DataTableFieldTypeEnum;
  /**
   * The default sort to apply
   */
  order: OrderEnum;
  /**
   * List of enum to use to translate data. Also used in filter single select input. Only used if type = singleSelect
   */
  filterEnum?: KeyValue[] | BaseResourceWithLabels[];
  /**
   * Add data-test tag in the cell
   */
  dataTest?: ExtendEnum<DataTestPartialEnum>;
  /**
   * Translate cell data. Only used if type = singleSelect or string
   */
  translate?: boolean;
  /**
   * Component to inject. Use DataTableComponentHelper.get() static method
   */
  component?: DataTableComponent;
  /**
   * With of column
   */
  width?: string | undefined;
  /**
   * Min with of column
   */
  minWidth?: string | undefined;
  /**
   * Max with of column
   */
  maxWidth?: string | undefined;
  /**
   * Alignment of cell data
   */
  alignment?: "left" | "center" | "right";
  /**
   * Break word in cell data
   */
  breakWord?: boolean | undefined;
  /**
   * Apply text align center
   *
   * @deprecated use alignment
   */
  centerContent?: boolean;
  /**
   * Callback to add css class `is-highlighting-cell`
   */
  isHighlightCell?: ((data: TResource) => boolean) | undefined;

  /**
   * Define if the column is sortable
   */
  isSortable?: boolean;
  /**
   * Define if the column is filterable
   */
  isFilterable?: boolean;
  /**
   * Field used to filter
   * By default, filterable field used is `field`.
   * If you want to override, define here
   */
  filterableField?: keyof TResource;
  /**
   * Field used to sort
   * By default, sortable field used is `field`.
   * If you want to override, define here
   */
  sortableField?: keyof TResource;
  /**
   * Callback used to apply treatment in list item. Only used if type = list or listToJoinString
   */
  listCallback?: (value: any) => string;
  /**
   * Limit of item to display. Only used if type = list or listToJoinString
   */
  maxListItem?: number | undefined;
  /**
   * Callback to display conditionally cell
   */
  conditionallyDisplay?: ((data: TResource) => boolean) | undefined;
  /**
   * DataTableActions to display in cell. Only used if type = actionButton
   */
  action?: DataTableActions<TResource>;

  /**
   * Allow to use searchable single select. Only used if type = searchableSingleSelect
   */
  resourceNameSpace?: ResourceNameSpace;
  /**
   * Allow to use searchable single select. Only used if type = searchableSingleSelect
   */
  resourceState?: ResourceState<any, any>;
  /**
   * Allow to define sort criteria in searchable single select. Only used if type = searchableSingleSelect
   */
  searchableSingleSelectSort?: Sort;
  /**
   * Allow to define value in searchable single select. Only used if type = searchableSingleSelect
   */
  resourceValueKey?: string;
  /**
   * Allow to define label to use in searchable single select. Only used if type = searchableSingleSelect
   */
  resourceLabelKey?: string;
  /**
   * Allow to define if `resourceLabelCallback` should be used in searchable single select. Only used if type = searchableSingleSelect
   *
   * @deprecated not used
   */
  useResourceLabelCallback?: boolean | undefined;
  /**
   * Allow to use callback to generate label in searchable single select. Only used if type = searchableSingleSelect
   */
  resourceLabelCallback?: (value: any) => string;
  /**
   * Allow to use a callback to display a second line in searchable single select. Only used if type = searchableSingleSelect
   */
  resourceExtraLabelSecondLineCallback?: (value: any) => string;
  /**
   * Allow to define extra query param in request throw by searchable single select. Only used if type = searchableSingleSelect
   */
  extraSearchQueryParam?: MappingObject<string, string> | undefined;

  /**
   * Invert value to show in the column but used original value for filtering and sorting
   */
  invertedBooleanValue?: boolean | undefined;

  /**
   * @deprecated not used
   */
  isUser?: boolean;
  /**
   * A computed value
   *
   * @param rowData data for this row
   * @param col column definition
   */
  callbackValue?: (rowData: TResource, col: DataTableColumns<TResource>) => any;
  /**
   * Allow to define Decimal representation options used in number DecimalPipe.
   * Specified by a string in the following format: {minIntegerDigits}.{minFractionDigits}-{maxFractionDigits}.
   * If undefined (not specified), do not display decimal
   * Only used if type = number
   */
  numberPipeDigitsInfo?: string | null;
  /**
   * Allow to define a code for the locale format rules used in number DecimalPipe.
   * If undefined (not specified), use LOCAL_ID defined in AppModule
   * Only used if type = number
   */
  numberPipeLocale?: string | null;
}
