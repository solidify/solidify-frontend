/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - extra-button-toolbar.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ElementRef} from "@angular/core";
import {Navigate} from "@ngxs/router-plugin";
import {ButtonColorEnum} from "../../enums/button-color.enum";
import {ButtonThemeEnum} from "../../enums/button-theme.enum";
import {DataTestPartialEnum} from "../../../core-resources/enums/partial/data-test-partial.enum";
import {IconNamePartialEnum} from "../../enums/partial/icon-name-partial.enum";
import {ExtendEnum} from "../../../core-resources/types/extend-enum.type";
import {BaseResource} from "../../../core-resources/models/dto/base-resource.model";
import {ObservableOrPromiseOrValue} from "../observables/observable-or-promise-or-value.model";
import {SolidifyObject} from "../../../core-resources/types/solidify-object.type";

export interface ExtraButtonToolbar<TResourceModel extends BaseResource> {
  /**
   * Represents the color of a button. The value should be an instance of ButtonColorEnum,
   * which defines the allowable colors for button components.
   */
  color: ButtonColorEnum;
  /**
   * Represents a class name that can be used to apply styling in HTML elements.
   *
   * The variable can store a string representing a CSS class or undefined.
   *
   * @type {string | undefined}
   */
  class?: string | undefined;
  /**
   * Represents an icon used in the application. The icon is an extended enumeration
   * type that includes a partial set of icon names defined in `IconNamePartialEnum`.
   *
   * This variable is intended to be used wherever icons are required to standardize
   * their representation and ensure consistency across the application.
   *
   * @type {ExtendEnum<IconNamePartialEnum>}
   */
  icon: ExtendEnum<IconNamePartialEnum>;
  /**
   * The typeButton variable is an optional property that represents the theme of a button.
   * Its value is derived from the ButtonThemeEnum enumeration, which dictates the stylistic
   * appearance and behavior of the button.
   *
   * Possible values can include different themes such as primary, secondary, etc., as defined
   * by the ButtonThemeEnum.
   */
  typeButton?: ButtonThemeEnum;
  /**
   * A function that takes an optional current resource model and returns a string that is either
   * directly available or wrapped within an Observable or Promise.
   *
   * @param {TResourceModel} [current] - An optional parameter representing the current resource model.
   * @return {ObservableOrPromiseOrValue<string>} The string that is either directly available or
   *                                             wrapped within an Observable or Promise.
   */
  labelToTranslate: (current?: TResourceModel | undefined) => ObservableOrPromiseOrValue<string>;
  /**
   * labelToTranslate parameters to inject parameter in a translated value
   */
  labelParams?: SolidifyObject;
  /**
   * A string representing the tooltip text to be translated.
   * This variable is optional and may be undefined.
   */
  tooltipToTranslate?: string | undefined;
  /**
   * Function to navigate to a different resource.
   *
   * @param {TResourceModel} [current] - The current resource model. If not provided, it can be undefined.
   * @returns {Navigate} - The navigation handler to perform the navigation.
   */
  navigate?: (current?: TResourceModel | undefined) => Navigate | string;
  /**
   * Specifies the target context in which to open the navigation link.
   *
   * The possible values are:
   * - "_blank": Opens the link in a new tab or window.
   * - "_parent": Opens the link in the parent frame.
   * - "_self": Opens the link in the same frame as it was clicked (default behavior).
   * - "_top": Opens the link in the full body of the window, breaking out of any frames.
   *
   * This property is optional.
   */
  navigateTarget?: "_blank" | "_parent" | "_self" | "top";
  /**
   * Optional callback function that can be executed with various parameters.
   *
   * @param {TResourceModel} [current] - The current resource model, if provided.
   * @param {ElementRef} [buttonElementRef] - A reference to the button element, if provided.
   * @param {boolean} [checked] - The checked state, if provided.
   */
  callback?: (current?: TResourceModel | undefined, buttonElementRef?: ElementRef | undefined, checked?: boolean) => void;
  /**
   * Represents the order or sequence of an item, potentially used for sorting or prioritizing purposes.
   *
   * The `order` property is optional and can be a number that indicates the position or rank
   * of the associated item within a list or collection.
   */
  order?: number;
  /**
   * The `dataTest` property is an optional field that allows to define attribut used by e2e test to handle element
   *
   * @type {ExtendEnum<DataTestPartialEnum>}
   */
  dataTest?: ExtendEnum<DataTestPartialEnum>;
  /**
   * A callback function that determines whether a certain condition is met for display button.
   * This function can return an Observable, a Promise, or a boolean value directly.
   *
   * @param {TResourceModel | undefined} [current] - The current resource model to evaluate the condition against.
   * @returns {ObservableOrPromiseOrValue<boolean>} A boolean value, a Promise that resolves to a boolean, or
   * an Observable emitting a boolean indicating whether the condition is met.
   */
  displayCondition?: (current?: TResourceModel | undefined) => ObservableOrPromiseOrValue<boolean>;
  /**
   * An optional function that determines the condition for disabling a button.
   * The function takes an optional parameter `current` of type `TResourceModel` or `undefined`.
   * It returns a value that can be an Observable, a Promise, or a boolean, indicating whether the resource should be disabled.
   *
   * @param {TResourceModel | undefined} [current] - The current resource model or undefined.
   * @returns {ObservableOrPromiseOrValue<boolean>} - A boolean, Observable, or Promise indicating the disable condition.
   */
  disableCondition?: (current?: TResourceModel | undefined) => ObservableOrPromiseOrValue<boolean>;
  /**
   * Indicates whether the component is a toggle button.
   *
   * If set to true, the button can switch between two states (e.g., on and off).
   * If set to false or undefined, the button does not have toggle behavior.
   */
  isToggleButton?: boolean;
  /**
   * Indicates whether a toggle control is currently checked.
   *
   * This variable represents the state of a toggle switch or checkbox,
   * specifying if it is in an "on" (true) or "off" (false) position. It
   * may be undefined if the initial state is not explicitly set.
   */
  isToggleChecked?: boolean;
}
