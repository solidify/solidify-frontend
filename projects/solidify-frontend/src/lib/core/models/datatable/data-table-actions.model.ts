/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - data-table-actions.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {IconNamePartialEnum} from "../../enums/partial/icon-name-partial.enum";
import {ExtendEnum} from "../../../core-resources/types/extend-enum.type";
import {BaseResource} from "../../../core-resources/models/dto/base-resource.model";
import {ObservableOrPromiseOrValue} from "../observables/observable-or-promise-or-value.model";

export interface DataTableActions<TResource extends BaseResource = any> {
  logo: ExtendEnum<IconNamePartialEnum>;
  callback: (current: TResource) => void;
  mouseoverCallback?: ($event: MouseEvent, current: TResource) => void;
  placeholder: (current: TResource) => ObservableOrPromiseOrValue<string>;
  displayOnCondition?: (current: TResource) => boolean | undefined;
  disableCondition?: (current: TResource) => ObservableOrPromiseOrValue<boolean> | undefined;
  isVisible?: boolean | undefined;
  isWrapped?: boolean;
  order?: number | undefined;
  displayNumberItems?: (current: TResource) => ObservableOrPromiseOrValue<number> | undefined;
}
