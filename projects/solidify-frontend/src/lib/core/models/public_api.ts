/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - public_api.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

export * from "./angular/core-abstract-angular-element.model";

export * from "./backend-modules/backend-application.model";
export * from "./backend-modules/backend-manifest.model";
export * from "./backend-modules/backend-module-version.model";
export * from "./backend-modules/commit.model";
export * from "./backend-modules/git.model";

export * from "./datatable/data-table-actions.model";
export * from "./datatable/data-table-bulk-actions.model";
export * from "./datatable/data-table-columns.model";
export * from "./datatable/data-table-component.model";
export * from "./datatable/data-table-component-input.model";
export * from "./datatable/extra-button-toolbar.model";

export * from "./dto/application-role.model";
export * from "./dto/base-relation-resource.model";
export * from "./dto/base-resource-avatar.model";
export * from "./dto/base-resource-logo.model";
export * from "./dto/base-resource-thumbnail.model";
export * from "./dto/base-resource-with-labels.model";
export * from "./dto/base-resource-with-logo.model";
export * from "./dto/base-resource-with-relation.model";
export * from "./dto/citation.model";
export * from "./dto/collection-typed.model";
export * from "./dto/facet.model";
export * from "./dto/facet-value.model";
export * from "./dto/hate-oas.model";
export * from "./dto/solidify-file.model";
export * from "./dto/page.model";
export * from "./dto/result.model";

export * from "./errors/error-dto.model";
export * from "./errors/error-dto-message.model";
export * from "./errors/form-error.model";
export * from "./errors/solidify-http-error-response.model";

export * from "./forms/base-form-definition.model";
export * from "./forms/form-control-key.model";
export * from "./forms/form-state.model";
export * from "./forms/model-form-control-event.model";

export * from "./http/upload-event.model";

export * from "./icon/icon-infos.model";

export * from "./managers/subscription-manager.model";

export * from "./observables/counter-subject.model";
export * from "./observables/observable-or-promise-or-value.model";

export * from "./route/breadcrumb.model";
export * from "./route/route.model";
export * from "./route/route-data.model";

export * from "./snackbar/snack-bar.model";

export * from "./stores/base-resource-state.model";
export * from "./stores/base-resource-store-namespace.model";
export * from "./stores/base-store-namespace.model";
export * from "./stores/base.action";
export * from "./stores/base-state.model";
export * from "./stores/state-action.model";
export * from "./stores/state.model";
export * from "./stores/state-context.model";
export * from "./stores/sub-resource-update.model";

export * from "./upload/file-upload-wrapper.model";
export * from "./upload/solidify-file-upload.model";
export * from "./upload/upload-file-status.model";

export * from "./frontend-version.model";
export * from "./global-banner.model";
export * from "./can-deactivate.model";
export * from "./download-token.model";
export * from "./label.model";
export * from "../../label-translate-interface.model";
export * from "./overlay-data.model";
export * from "./status-history.model";
export * from "./status-history-dialog-data.model";
export * from "./tab.model";
export * from "./token.model";
