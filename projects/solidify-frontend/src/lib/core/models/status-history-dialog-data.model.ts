/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - status-history-dialog-data.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Observable} from "rxjs";
import {StatusHistoryNamespace} from "../stores/abstract/status-history/status-history-namespace.model";
import {QueryParameters} from "../../core-resources/models/query-parameters/query-parameters.model";
import {StatusHistory} from "./status-history.model";
import {StatusModel} from "../../core-resources/models/status.model";

export interface StatusHistoryDialogData {
  parentId: string;
  resourceResId: string;
  name: string;
  statusHistory: Observable<StatusHistory[]>;
  isLoading: Observable<boolean>;
  queryParametersObs: Observable<QueryParameters>;
  state: StatusHistoryNamespace;
  statusEnums: StatusModel[];
}
