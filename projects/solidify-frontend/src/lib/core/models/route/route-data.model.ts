/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - route-data.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {Type} from "@angular/core";
import {
  CanActivate,
  Data,
  Params,
} from "@angular/router";
import {CompositionNameSpace} from "../../stores/abstract/composition/composition-namespace.model";
import {CompositionState} from "../../stores/abstract/composition/composition.state";
import {ResourceNameSpace} from "../../stores/abstract/resource/resource-namespace.model";
import {ResourceState} from "../../stores/abstract/resource/resource.state";
import {BaseStateModel} from "../stores/base-state.model";

export class SolidifyRouteData implements Data {
  breadcrumb?: string | ((params: Params) => string);
  breadcrumbMemoizedSelector?: (state: BaseStateModel) => string;
  permission?: string;
  noBreadcrumbLink?: boolean;
  nameSpace?: ResourceNameSpace;
  resourceState?: Type<ResourceState<any, any>>;
  compositionNameSpace?: CompositionNameSpace;
  compositionState?: Type<CompositionState<any, any>>;
  routeNotFound?: string;
  paramId?: string;
  paramIdCompositionParent?: string;
  guards?: Type<CanActivate>[];
}
