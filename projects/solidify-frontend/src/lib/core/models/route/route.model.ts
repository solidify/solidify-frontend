/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - route.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {Type} from "@angular/core";
import {
  LoadChildren,
  ResolveData,
  Route,
  RunGuardsAndResolvers,
  UrlMatcher,
} from "@angular/router";
import {SolidifyRouteData} from "./route-data.model";

export declare type SolidifyRoutes = SolidifyRoute[];

export class SolidifyRoute implements Route {
  canActivate?: any[];
  canActivateChild?: any[];
  canDeactivate?: any[];
  canLoad?: any[];
  children?: SolidifyRoutes;
  component?: Type<any>;
  data?: SolidifyRouteData | undefined;
  loadChildren?: LoadChildren;
  matcher?: UrlMatcher;
  outlet?: string;
  path?: string;
  pathMatch?: "prefix" | "full";
  redirectTo?: string;
  resolve?: ResolveData;
  runGuardsAndResolvers?: RunGuardsAndResolvers;
}
