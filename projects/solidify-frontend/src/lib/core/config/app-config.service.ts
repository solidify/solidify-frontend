/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - app-config.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DOCUMENT} from "@angular/common";
import {
  HttpBackend,
  HttpClient,
} from "@angular/common/http";
import {
  Injectable,
  Injector,
  makeStateKey,
  Type,
} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
  of,
  timer,
} from "rxjs";
import {
  catchError,
  map,
  switchMap,
  take,
} from "rxjs/operators";
import {DefaultSolidifyApplicationEnvironment} from "../../application/environments/environment.solidify-application";
import {SolidifyApplicationAppStateModel} from "../../application/stores/abstract/app/solidify-application-app-state.model";
import {
  isNullOrUndefined,
  isTrue,
} from "../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {FileVisualizerHelper} from "../../file-preview/helpers/file-visualizer.helper";
import {SolidifyAppUserStateModel} from "../../user/stores/abstract/app-user/app-user-state.model";
import {SolidifyAppUserState} from "../../user/stores/abstract/app-user/app-user.state";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {AbstractBaseService} from "../services/abstract-base.service";
import {TransferStateService} from "../services/transfer-state.service";
import {SolidifyAppAction} from "../stores/abstract/app/app.action";
import {SolidifyAppState} from "../stores/abstract/app/app.state";
import {CacheBustingUtil} from "../utils/cache-busting.util";
import {DateUtil} from "../utils/date.util";
import {ObservableUtil} from "../utils/observable.util";
import {ofSolidifyActionCompleted} from "../utils/stores/store.tool";
import {StoreUtil} from "../utils/stores/store.util";

@Injectable({
  providedIn: "root",
})
export class AppConfigService extends AbstractBaseService {

  private readonly _CONSOLE_WARN_NO_ENVIRONMENT_RUNTIME_AVAILABLE: string = "No runtime config available, using the compiled environment config";
  private readonly _CONSOLE_ERROR_LOADING_RUNTIME_CONFIG: string = "Error loading runtime config. An empty file named \"environment.runtime.json\" " +
    "must be available in the application assets/configurations/ folder";
  private readonly _CONFIG_FILE_URL: string = "./assets/configurations/environment.runtime.json";

  private readonly _environmentUpdatedBS: BehaviorSubject<DefaultSolidifyEnvironment> = new BehaviorSubject<DefaultSolidifyEnvironment>(undefined);
  readonly environmentUpdatedObs: Observable<DefaultSolidifyEnvironment> = ObservableUtil.asObservable(this._environmentUpdatedBS);

  constructor(private readonly _injector: Injector) {
    super();
    const document = _injector.get(DOCUMENT);
    SsrUtil[SsrUtil.DOCUMENT_PROPERTY_KEY] = document;
  }

  mergeConfigAndInitApplication(environment: DefaultSolidifyEnvironment,
                                appState: Type<SolidifyAppState<SolidifyApplicationAppStateModel>>,
                                appUserState?: Type<SolidifyAppUserState<SolidifyAppUserStateModel>>): Observable<boolean> {
    return this.mergeConfig(environment).pipe(
      take(1),
      switchMap(success => this.initApplication(environment, appState, appUserState).pipe(take(1))),
    );
  }

  mergeConfig(environment: DefaultSolidifyEnvironment): Observable<boolean> {
    if (environment.production === true || environment.forceRuntimeOverrideSetting === true) {
      if (SsrUtil.isBrowser) {
        this.subscribe(this._scheduleEnvironmentAutoUpdate(environment));
      }
      return this._getRuntimeEnvironment(environment).pipe(
        take(1),
        map(res => true),
      );
    } else {
      if (isTrue(environment.showDebugInformation) && SsrUtil.isBrowser) {
        // eslint-disable-next-line no-console
        console.info("Environment config used", environment);
      }
      return of(true);
    }
  }

  initApplication(environment: DefaultSolidifyEnvironment,
                  appState: Type<SolidifyAppState<SolidifyApplicationAppStateModel>>,
                  appUserState?: Type<SolidifyAppUserState<SolidifyAppUserStateModel>>): Observable<boolean> {
    const store = this._injector.get(Store);
    const actions$ = this._injector.get(Actions);
    new DateUtil(); // Allow to initiate moment in DateUtil
    new FileVisualizerHelper(environment.visualizationStatusThatAllowPreview); // Allow to provide status that allow preview
    environment.appState = appState;
    (environment as DefaultSolidifyApplicationEnvironment).appUserState = appUserState;
    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(store, [{
      action: new SolidifyAppAction.InitApplication(),
      subActionCompletions: [
        actions$.pipe(ofSolidifyActionCompleted(SolidifyAppAction.InitApplicationSuccess)),
        actions$.pipe(ofSolidifyActionCompleted(SolidifyAppAction.InitApplicationFail)),
      ],
    }]).pipe(
      take(1),
      map(result => result.success),
    );
  }

  private _getRuntimeEnvironment(environment: DefaultSolidifyEnvironment): Observable<DefaultSolidifyEnvironment> {
    const transferState = this._injector.get(TransferStateService);

    const STATE_KEY = makeStateKey<Partial<DefaultSolidifyEnvironment>>(`AppConfig`);
    if (transferState.hasKey(STATE_KEY)) {
      const runtimeConfig = transferState.get(STATE_KEY, {});
      transferState.remove(STATE_KEY);
      if (isNullOrUndefined(runtimeConfig)) {
        return of(environment);
      }
      return of(this._assignRuntimeConfigToEnvironment(runtimeConfig, environment));
    }

    const handler = this._injector.get(HttpBackend);
    const http = new HttpClient(handler);
    let url = this._CONFIG_FILE_URL + CacheBustingUtil.generateCacheBustingQueryParam();
    if (SsrUtil.isServer) {
      url = `http://localhost:${SsrUtil.port}/` + url;
    }
    return http.get<DefaultSolidifyEnvironment>(url)
      .pipe(
        take(1),
        map(runtimeConfig => {
          transferState.set(STATE_KEY, runtimeConfig);
          return this._assignRuntimeConfigToEnvironment(runtimeConfig, environment);
        }),
        catchError(err => {
          transferState.set(STATE_KEY, undefined);
          if (err.status === 404) {
            // eslint-disable-next-line no-console
            console.warn(err, this._CONSOLE_WARN_NO_ENVIRONMENT_RUNTIME_AVAILABLE);
          } else {
            // eslint-disable-next-line no-console
            console.error(err, this._CONSOLE_ERROR_LOADING_RUNTIME_CONFIG);
          }
          return of(environment);
        }),
      );
  }

  private _assignRuntimeConfigToEnvironment(runtimeConfig: Partial<DefaultSolidifyEnvironment>, environment: DefaultSolidifyEnvironment): DefaultSolidifyEnvironment {
    // Merge runtime environment config into compiled environment.
    Object.assign(environment, runtimeConfig);
    if (isTrue(environment.showDebugInformation) && SsrUtil.isBrowser) {
      // eslint-disable-next-line no-console
      console.info("Environment config used", environment);
    }
    this._environmentUpdatedBS.next(environment);
    return environment;
  }

  private _scheduleEnvironmentAutoUpdate(environment: DefaultSolidifyEnvironment): Observable<DefaultSolidifyEnvironment> {
    const interval = environment.intervalRefreshRuntimeOverrideSetting;
    return timer(interval, interval).pipe(switchMap(() => this._getRuntimeEnvironment(environment)));
  }
}
