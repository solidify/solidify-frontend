/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - app-config.service.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  HttpClientTestingModule,
  HttpTestingController,
} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../injection-tokens/environment.injection-token";
import {AppConfigService} from "./app-config.service";

describe("AppConfigService", () => {
  let httpMock: HttpTestingController;
  let service: AppConfigService;
  const environment = {} as DefaultSolidifyEnvironment;
  const CONFIG_FILE_PATH = "./assets/configurations/environment.runtime.json?solidify-cache-bust=";

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: ENVIRONMENT,
          useValue: environment,
        },
      ],
    });
    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(AppConfigService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should override compile environment config with runtime configuration", done => {
    const compiledEnvironmentConfig: any = {production: true, envConfig: "initial"};
    const runtimeEnvironmentConfig: any = {production: true, envConfig: "updated"};

    service.mergeConfig(compiledEnvironmentConfig).subscribe(() => {
      expect(compiledEnvironmentConfig).toEqual(runtimeEnvironmentConfig);
      done();
    });
    const request = httpMock.expectOne(res => res.url.startsWith(CONFIG_FILE_PATH));
    request.flush(runtimeEnvironmentConfig);
  });

  it("should add additional runtime configuration to the compile environment config", done => {
    const compiledEnvironmentConfig: any = {production: true, envConfig: "a_value"};
    const runtimeEnvironmentConfig: any = {newConfig: "a_value"};
    const expectedMergedConfig: any = {production: true, envConfig: "a_value", newConfig: "a_value"};

    service.mergeConfig(compiledEnvironmentConfig).subscribe(() => {
      expect(compiledEnvironmentConfig).toEqual(expectedMergedConfig);
      done();
    });
    const request = httpMock.expectOne(res => res.url.startsWith(CONFIG_FILE_PATH));
    request.flush(runtimeEnvironmentConfig);
  });

  it("should throw a console.error when environment runtime config is not a valid JSON format", done => {
    const compiledEnvironmentConfig: any = {production: true, envConfig: "initial"};

    spyOn(console, "error");

    service.mergeConfig(compiledEnvironmentConfig).subscribe(() => {
      // eslint-disable-next-line no-console
      expect(console.error).toHaveBeenCalled();
      done();
    });

    httpMock.expectOne(res => res.url.startsWith(CONFIG_FILE_PATH)).error(new ErrorEvent("error"));
  });

  it("should not load runtime environment config if the compile environment is dev", done => {
    const compiledEnvironmentConfig: any = {production: false, envConfig: "initial"};

    spyOn(console, "error");

    service.mergeConfig(compiledEnvironmentConfig).subscribe(() => {
      // eslint-disable-next-line no-console
      expect(console.error).not.toHaveBeenCalled();
      done();
    });
  });

  it("should keep compiled environment config when runtime environment config is empty", done => {
    const compiledEnvironmentConfig: any = {production: true, envConfig: "initial"};
    const runtimeEnvironmentConfig = null;

    spyOn(console, "error");

    service.mergeConfig(compiledEnvironmentConfig).subscribe(() => {
      // eslint-disable-next-line no-console
      expect(console.error).not.toHaveBeenCalled();
      done();
    });

    const request = httpMock.expectOne(res => res.url.startsWith(CONFIG_FILE_PATH));
    request.flush(runtimeEnvironmentConfig);
  });
});
