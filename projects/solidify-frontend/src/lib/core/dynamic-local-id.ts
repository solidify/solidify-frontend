/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - dynamic-local-id.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  LOCALE_ID,
  StaticClassProvider,
} from "@angular/core";
import {MAT_DATE_LOCALE} from "@angular/material/core";
import {TranslateService} from "@ngx-translate/core";
import {isNotNullNorUndefinedNorWhiteString} from "../core-resources/tools/is/is.tool";
import {SsrUtil} from "../core-resources/utils/ssr.util";
import {LanguagePartialEnum} from "./enums/partial/language-partial.enum";
import {DefaultSolidifyEnvironment} from "./environments/environment.solidify-defaults";
import {ENVIRONMENT} from "./injection-tokens/environment.injection-token";

export class DynamicLocaleId extends String {
  constructor(protected readonly _translateService: TranslateService,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super();
  }

  toString(): string {
    const navigatorLanguageCulture = SsrUtil.window?.navigator.language;
    const appLanguageIso2 = this._translateService.currentLang;
    if (isNotNullNorUndefinedNorWhiteString(navigatorLanguageCulture)) {
      const indexOfCultureSeparator = navigatorLanguageCulture.indexOf("-");
      const hasCulture = navigatorLanguageCulture.indexOf("-") >= 0;
      const navigatorLanguageIso2 = hasCulture ? navigatorLanguageCulture.substring(0, indexOfCultureSeparator) : navigatorLanguageCulture;
      const isNavigatorLanguageSupportedByApp = this._environment.appLanguages.includes(navigatorLanguageIso2);
      if (isNavigatorLanguageSupportedByApp) {
        return hasCulture ? navigatorLanguageCulture : this._addCultureToLanguage(navigatorLanguageIso2);
      }
    }

    return this._addCultureToLanguage(appLanguageIso2);
  }

  protected _addCultureToLanguage(language: string): string {
    const languageLowerCase = language.toLowerCase();
    switch (languageLowerCase) {
      case LanguagePartialEnum.fr:
        return "fr-CH";
      case LanguagePartialEnum.en:
        return "en-GB";
      case LanguagePartialEnum.de:
        return "de-CH";
    }
    return "en-GB";
  }
}

export const LocaleIdProvider: StaticClassProvider = {
  provide: LOCALE_ID,
  useClass: DynamicLocaleId,
  deps: [TranslateService, ENVIRONMENT],
};

export const MatDateLocaleProvider: StaticClassProvider = {
  provide: MAT_DATE_LOCALE,
  useClass: DynamicLocaleId,
  deps: [TranslateService, ENVIRONMENT],
};
