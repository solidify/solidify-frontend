/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - force-error-deserialization-interceptor.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {catchError} from "rxjs/operators";
import {isString} from "../../core-resources/tools/is/is.tool";
import {SolidifyHttpErrorResponseModel} from "../models/errors/solidify-http-error-response.model";
import {AbstractBaseService} from "../services/abstract-base.service";

@Injectable({
  providedIn: "root",
})
export class ForceErrorDeserializationInterceptor extends AbstractBaseService implements HttpInterceptor {
  constructor() {
    super();
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next
      .handle(req)
      .pipe(
        catchError((err: SolidifyHttpErrorResponseModel) => {
          this._deserializeErrorIfNot(err);
          throw err;
        }),
      );
  }

  /**
   * Manage case where responseType is text and not json.
   * In this case, error is in text and not in json.
   * This cause problem in error treatment (error handler, oauth MFA interceptor...)
   * @param error
   */
  private _deserializeErrorIfNot(error: SolidifyHttpErrorResponseModel | HttpErrorResponse): void {
    if (isString(error.error)) {
      try {
        Object.assign(error, {error: JSON.parse(error.error)});
      } catch (e) {
        // eslint-disable-next-line no-console
        console.warn(`Unable to parse JSON in error : ${error.error}`);
      }
    }
  }
}
