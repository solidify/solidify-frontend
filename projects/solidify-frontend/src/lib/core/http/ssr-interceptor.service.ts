/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - ssr-interceptor.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

/* eslint-disable no-console */

import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from "@angular/common/http";
import {
  Inject,
  Injectable,
} from "@angular/core";
import {Observable} from "rxjs";
import {
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isObject,
} from "../../core-resources/tools/is/is.tool";
import {MappingObject} from "../../core-resources/types/mapping-type.type";
import {MappingObjectUtil} from "../../core-resources/utils/mapping-object.util";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../injection-tokens/environment.injection-token";
import {AbstractBaseService} from "../services/abstract-base.service";

@Injectable({
  providedIn: "root",
})
export class SsrInterceptorService extends AbstractBaseService implements HttpInterceptor {
  constructor(@Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super();
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (SsrUtil.isServer) {
      const url = req.url;
      let modifiedReq = undefined;
      if (url.startsWith("./") || url.startsWith("/") || url.startsWith("assets/")) {
        modifiedReq = req.clone({
          url: `http://localhost:${SsrUtil.port}/` + url,
        });
      } else {
        const newUrl = SsrInterceptorService._adaptExternalUrlToInternalUrlForSsr(this._environment.ssrUrlModuleRewrite, url);
        if (isNotNullNorUndefinedNorWhiteString(newUrl)) {
          modifiedReq = req.clone({
            url: newUrl,
          });
        }
      }

      if (isNotNullNorUndefined(modifiedReq)) {
        this._log(`[${req.method}] ${url}: Intercept request updated into '${modifiedReq.url}'`);
        return next.handle(modifiedReq);
      }
      this._log(`[${req.method}] ${url}: Intercept request not updated`);
    }
    return next.handle(req);
  }

  private _log(message: string): void {
    if (this._environment.showDebugInformation) {
      console.log(message);
    }
  }

  /**
   * Return new url if mapping found, undefined in other case
   *
   * @param ssrUrlModuleRewrite
   * @param url
   */
  private static _adaptExternalUrlToInternalUrlForSsr(ssrUrlModuleRewrite: MappingObject<string, string>, url: string): string | undefined {
    if (SsrUtil.isBrowser) {
      return;
    }
    if (isObject(ssrUrlModuleRewrite) && (url.startsWith("http://") || url.startsWith("https://"))) {
      const from = MappingObjectUtil.keys(ssrUrlModuleRewrite).find(k => url.startsWith(k));
      if (isNotNullNorUndefined(from)) {
        const dest = MappingObjectUtil.get(ssrUrlModuleRewrite, from);
        return url.replace(from, dest);
      }
    }
  }
}
