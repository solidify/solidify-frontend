/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - http-translate-loader-extra.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {
  Inject,
  makeStateKey,
  Optional,
} from "@angular/core";
import {TranslateLoader} from "@ngx-translate/core";
import {
  combineLatest,
  Observable,
  of,
} from "rxjs";
import {map} from "rxjs/operators";
import {SolidifyObject} from "../../core-resources/types/solidify-object.type";
import {ObjectUtil} from "../../core-resources/utils/object.util";
import {StringUtil} from "../../core-resources/utils/string.util";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../injection-tokens/environment.injection-token";
import {TransferStateService} from "../services/transfer-state.service";

export class HttpTranslateLoaderExtra implements TranslateLoader {
  private readonly _prefix: string = "./assets/";
  private readonly _prefixBase: string = "i18n/";
  private readonly _prefixThemes: string = "themes/";
  private readonly _prefixRuntime: string = "i18n/";
  private readonly _solidifySuffix: string = "_solidify";
  private readonly _runtimeSuffix: string = "_runtime";
  private readonly _suffix: string = ".json";
  private readonly _langKey: string = "lang";
  private readonly _langKeyToReplace: string = "{lang}";
  private readonly _themeKey: string = "theme";
  private readonly _themeKeyToReplace: string = "{theme}";

  private readonly _defaultOptions: HttpTranslateLoaderExtraOption = {
    suffix: this._suffix,
    // urlSolidifyFile => ./assets/i18n/{lang}_solidify.json
    urlSolidifyFile: `${this._prefix}${this._prefixBase}${this._langKeyToReplace}${this._solidifySuffix}`,
    // urlDefaultFile => ./assets/i18n/{lang}.json
    urlDefaultFile: `${this._prefix}${this._prefixBase}${this._langKeyToReplace}`,
    // urlThemeFile => ./assets/themes/{theme}/i18n/{lang}_{theme}.json
    urlThemeFile: `${this._prefix}${this._prefixThemes}${this._themeKeyToReplace}${SOLIDIFY_CONSTANTS.URL_SEPARATOR}${this._prefixBase}${this._langKeyToReplace}_${this._themeKeyToReplace}`,
    // urlRuntimeFile => ./assets/i18n/{lang}_runtime.json
    urlRuntimeFile: `${this._prefix}${this._prefixRuntime}${this._langKeyToReplace}${this._runtimeSuffix}`,
  };
  private _optionsToUse: HttpTranslateLoaderExtraOption = ObjectUtil.clone(this._defaultOptions);

  constructor(private readonly _http: HttpClient,
              private readonly _options: HttpTranslateLoaderExtraOption,
              @Optional() private readonly _transferState: TransferStateService,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    ObjectUtil.merge(this._optionsToUse, this._options);
  }

  /**
   * Gets the translations from the server
   */
  public getTranslation(lang: string): Observable<SolidifyObject> {
    const STATE_KEY = makeStateKey<SolidifyObject>(`HttpTranslateLoaderTranslation-${lang}`);
    if (this._environment.useTransferStateForTranslation && this._transferState?.hasKey(STATE_KEY)) {
      const translations = this._transferState.get(STATE_KEY, {});
      this._transferState.remove(STATE_KEY);
      return of(translations);
    }
    // create translate resource with inheritance using the following order :
    // - "./assets/i18n/en_solidify.json" => default solidify file used
    // - "./assets/i18n/en.json" => default file used
    // - "./assets/themes/myTheme/i18n/en_myTheme.json" => for override default file by theme
    // - "./assets/i18n/en_runtime.json" => for override at runtime
    return combineLatest([
      this._http.get(StringUtil.formatKeyValue(this._optionsToUse.urlSolidifyFile, {key: this._langKey, value: lang}) + this._optionsToUse.suffix),
      this._http.get(StringUtil.formatKeyValue(this._optionsToUse.urlDefaultFile, {key: this._langKey, value: lang}) + this._optionsToUse.suffix),
      this._http.get(StringUtil.formatKeyValue(this._optionsToUse.urlThemeFile,
        {key: this._langKey, value: lang},
        {key: this._themeKey, value: this._environment.theme},
        {key: this._themeKey, value: this._environment.theme},
      ) + this._optionsToUse.suffix),
      this._http.get(StringUtil.formatKeyValue(this._optionsToUse.urlRuntimeFile, {key: this._langKey, value: lang}) + this._optionsToUse.suffix),
    ]).pipe(
      map(([solidify, base, theme, extra]) => {
        const translations = ObjectUtil.mergeDeep(solidify, base, theme, extra);
        if (this._environment.useTransferStateForTranslation) {
          this._transferState?.set(STATE_KEY, translations);
        }
        return translations;
      }),
    );
  }
}

export interface HttpTranslateLoaderExtraOption {
  suffix?: string;
  urlSolidifyFile?: string;
  urlDefaultFile?: string;
  urlThemeFile?: string;
  urlRuntimeFile?: string;
}

