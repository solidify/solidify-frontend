/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - language-interceptor.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from "@angular/common/http";
import {
  Inject,
  Injectable,
} from "@angular/core";
import {Store} from "@ngxs/store";
import {Observable} from "rxjs";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../injection-tokens/environment.injection-token";
import {AbstractBaseService} from "../services/abstract-base.service";
import {isNotNullNorUndefined} from "../../core-resources/tools/is/is.tool";
import {MemoizedUtil} from "../utils/stores/memoized.util";

@Injectable({
  providedIn: "root",
})
export class LanguageInterceptor extends AbstractBaseService implements HttpInterceptor {

  constructor(private readonly _store: Store,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super();
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler, noRetry: boolean = false): Observable<HttpEvent<any>> {
    const currentLanguage = MemoizedUtil.selectSnapshot(this._store, this._environment.appState, state => state.appLanguage);
    if (isNotNullNorUndefined(currentLanguage)) {
      const modifiedReq = req.clone({
        headers: req.headers.set("Accept-Language", currentLanguage),
      });
      return next.handle(modifiedReq);
    } else {
      return next.handle(req);
    }
  }
}
