/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - store.decorator.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {ActionOptions} from "@ngxs/store";
import {
  SOLIDIFY_META_KEY,
  SolidifyMetadataUtil,
} from "../utils/stores/solidify-metadata.util";
import {
  OverrideDefaultAction,
  RegisterDefaultAction,
} from "./store.decorator";

describe("Property Name Decorator", () => {
  describe("@RegisterDefaultAction decorator", () => {
    it("should register in default action the action 'Get All'", () => {
      const baseConstructor = {};
      const actionToRegister = "Get All";
      const options = {test: true} as ActionOptions;
      const callback: () => null = () => null;
      const meta = SolidifyMetadataUtil.ensureStoreSolidifyMetadata(baseConstructor.constructor as any);
      const registerDefaultAction = RegisterDefaultAction(callback, options);
      registerDefaultAction.call(null, baseConstructor, actionToRegister, null);
      expect(meta.defaultActions.length).toBe(1);
      expect(meta.defaultActions[0].fn).toBe(actionToRegister);
      expect(meta.defaultActions[0].callback).toBe(callback);
      expect(meta.defaultActions[0].options).toBe(options);
      baseConstructor.constructor[SOLIDIFY_META_KEY] = undefined;
    });
  });

  describe("@OverrideDefaultAction decorator", () => {
    it("should register in excluded default action the action 'Get All'", () => {
      const childConstructor = {};
      const actionToIgnore = "Get All";
      const meta = SolidifyMetadataUtil.ensureStoreSolidifyMetadata(childConstructor.constructor as any);
      const overrideDefaultAction = OverrideDefaultAction();
      overrideDefaultAction.call(null, childConstructor, actionToIgnore, null);
      expect(meta.excludedRegisteredDefaultActionFns.length).toBe(1);
      expect(meta.excludedRegisteredDefaultActionFns[0]).toBe(actionToIgnore);
      childConstructor.constructor[SOLIDIFY_META_KEY] = undefined;
    });
  });
});
