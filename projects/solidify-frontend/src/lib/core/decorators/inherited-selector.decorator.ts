/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - inherited-selector.decorator.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {createSelector} from "@ngxs/store";
import {ɵPlainObjectOf} from "@ngxs/store/internals";

/**
 * Decorator for memoizing a state selector.
 */
// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export function InheritedSelector(selectors?: any[]): MethodDecorator {
  return <T>(
    target: any,
    key: string | symbol,
    descriptor: TypedPropertyDescriptor<T>,
  ): TypedPropertyDescriptor<T> | void => {
    const isNotMethod = !(descriptor && descriptor.value !== null);

    if (isNotMethod) {
      throw new Error(CONFIG_MESSAGES[VALIDATION_CODE.SELECTOR_DECORATOR]());
    }

    const originalFn = descriptor.value;
    let memoizedFn: any = null;
    const newDescriptor = {
      configurable: true,
      get() {
        // Selector initialisation deferred to here so that it is at runtime, not decorator parse time
        memoizedFn =
          memoizedFn ||
          createSelector(
            selectors,
            originalFn as any,
            {
              containerClass: this, // <<< Modification of NGXS just here by replacing "target" by "this" to target the child state and not the abstract parent state
              selectorName: key.toString(),
              // eslint-disable-next-line
              getSelectorOptions() {
                return {};
              },
            },
          );
        return memoizedFn;
      },
    };
    // Add hidden property to descriptor
    newDescriptor["originalFn"] = originalFn;
    return newDescriptor;
  };
}

enum VALIDATION_CODE {
  SELECTOR_DECORATOR = "SELECTOR_DECORATOR"
}

const CONFIG_MESSAGES: ɵPlainObjectOf<Function> = {
  [VALIDATION_CODE.SELECTOR_DECORATOR]: () => "Selectors only work on methods",
};
