/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - action-ngxs.decorator.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ActionOptions} from "@ngxs/store";
import {ɵensureStoreMetadata} from "@ngxs/store/internals";

// UNUSED:
// It's a copy of the code source of NGXS @Action
// Just here to understand the logic of @Action NGXS decorator (
// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export function CopyOfNGXSAction(actions: any | any[], options?: ActionOptions): MethodDecorator {
  return (target: any, name: string, descriptor: TypedPropertyDescriptor<any>) => {
    const meta = ɵensureStoreMetadata(target.constructor);

    if (!Array.isArray(actions)) {
      actions = [actions];
    }

    for (const action of actions) {
      const type = action.type;

      if (!action.type) {
        throw new Error(`Action ${action.name} is missing a static "type" property`);
      }

      if (!meta.actions[type]) {
        meta.actions[type] = [];
      }

      meta.actions[type].push({
        fn: name,
        options: options || {},
        type,
      });
    }
  };
}
