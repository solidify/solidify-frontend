/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - dialog.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Type} from "@angular/core";
import {
  MatDialog,
  MatDialogConfig,
} from "@angular/material/dialog";
import {Observable} from "rxjs";
import {
  take,
  tap,
} from "rxjs/operators";
import {
  isArray,
  isFunction,
  isNonWhiteString,
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "../../core-resources/tools/is/is.tool";
import {ObjectUtil} from "../../core-resources/utils/object.util";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {AbstractDialogInterface} from "../components/dialogs/abstract/abstract-dialog-interface.model";
import {ObservableUtil} from "./observable.util";

// @dynamic
export class DialogUtil {
  private static readonly _SOLIDIFY_DIALOGS_CLASS: string = "solidify-dialogs";

  static open<TData, UResult>(matDialog: MatDialog,
                              dialogComponent: Type<AbstractDialogInterface<TData, UResult>>,
                              data?: TData,
                              options?: MatDialogConfigExtended,
                              callbackSuccess?: (result: UResult) => void,
                              callbackFail?: () => void,
  ): Observable<UResult> {
    const elementFocusedBeforeDialogOpened = SsrUtil.document?.activeElement;
    if (isNullOrUndefined(options)) {
      options = {};
    }
    if (isNotNullNorUndefined(options.panelClass) && isNonWhiteString(options.panelClass)) {
      options.panelClass = [options.panelClass as string];
    } else if (!isArray(options.panelClass)) {
      options.panelClass = [];
    }
    options.panelClass.push(this._SOLIDIFY_DIALOGS_CLASS);
    if (isNullOrUndefined(data)) {
      data = {} as TData;
    }
    const matOptions: MatDialogConfig<TData> = ObjectUtil.clone(options);
    matOptions.data = data;
    let observable = matDialog.open(dialogComponent, matOptions).afterClosed();

    // Restore position of focus after dialog is closed, even if there is no subscription on the result of this method
    ObservableUtil.takeOneThenUnsubscribe(observable, () => elementFocusedBeforeDialogOpened?.["focus"]());

    if (options.takeOne) {
      observable = observable.pipe(take(1));
    }

    observable = observable.pipe(
      tap((result: UResult | undefined) => {
        if (isNullOrUndefined(result)) {
          if (isFunction(callbackFail)) {
            callbackFail();
          }
          return;
        }
        if (isFunction(callbackSuccess)) {
          callbackSuccess(result);
        }
      }),
    );
    return observable;
  }
}

export interface MatDialogConfigExtended extends Omit<MatDialogConfig, "data"> {
  takeOne?: boolean;
}
