/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - meta.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {Meta} from "@angular/platform-browser";
import {
  isEmptyArray,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
} from "../../core-resources/tools/is/is.tool";

export class MetaUtil {
  static setMetaTagName(meta: Meta, name: string, content: string, updateTag: boolean = true): void {
    if (isNullOrUndefinedOrWhiteString(content)) {
      if (updateTag) {
        this.cleanMetaByName(meta, name);
      }
      return;
    }
    if (updateTag) {
      meta.updateTag({
        name: name,
        content: content,
      });
    } else {
      meta.addTag({
        name: name,
        content: content,
      });
    }
  }

  static setMetaTagNameForEachArray(meta: Meta, name: string, contentArray: string[]): void {
    if (isNullOrUndefined(contentArray) || isEmptyArray(contentArray)) {
      this.cleanMetaByName(meta, name);
      return;
    }
    contentArray.forEach(content => {
      this.setMetaTagName(meta, name, content, false);
    });
  }

  static setMetaTagNameArrayWithSeparator(meta: Meta, name: string, contentArray: string[], separator: string = ", "): void {
    if (isNullOrUndefined(contentArray) || isEmptyArray(contentArray) || isNullOrUndefined(separator)) {
      return;
    }
    this.setMetaTagName(meta, name, contentArray.join(separator), true);
  }

  static cleanMetaByName(meta: Meta, name: string): void {
    meta.removeTag(`name='${name}'`);
  }

  static cleanListMetaByName(meta: Meta, listMetaName: string[]): void {
    listMetaName.forEach(name => {
      this.cleanMetaByName(meta, name);
    });
  }

  static cleanMatchingMetaByName(meta: Meta, regexp: RegExp): void {
    const listMeta = meta.getTags("name");
    const listMatchingMeta = listMeta.filter(m => regexp.test(m.name));
    listMatchingMeta.forEach(t => {
      meta.removeTagElement(t);
    });
  }

  static setMetaTagProperty(meta: Meta, property: string, content: string): void {
    if (isNullOrUndefinedOrWhiteString(content)) {
      this.cleanMetaByProperty(meta, property);
      return;
    }
    meta.updateTag({
      property: property,
      content: content,
    });
  }

  static setMetaTagPropertyForEachArray(meta: Meta, property: string, contentArray: string[]): void {
    if (isNullOrUndefined(contentArray) || isEmptyArray(contentArray)) {
      return;
    }
    contentArray.forEach((content, index) => {
      this.setMetaTagProperty(meta, property + "_" + (index + 1), content);
    });
  }

  static setMetaTagPropertyArrayWithSeparator(meta: Meta, property: string, contentArray: string[], separator: string = ", "): void {
    if (isNullOrUndefined(contentArray) || isEmptyArray(contentArray) || isNullOrUndefined(separator)) {
      return;
    }
    this.setMetaTagProperty(meta, property, contentArray.join(separator));
  }

  static cleanMetaByProperty(meta: Meta, property: string): void {
    meta.removeTag(`property='${property}'`);
  }

  static cleanListMetaByProperty(meta: Meta, listMetaProperty: string[]): void {
    listMetaProperty.forEach(property => {
      this.cleanMetaByProperty(meta, property);
    });
  }
}

