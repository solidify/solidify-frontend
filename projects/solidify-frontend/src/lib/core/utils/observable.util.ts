/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - observable.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  BehaviorSubject,
  Observable,
  Subject,
  Subscription,
} from "rxjs";
import {
  map,
  skip,
  take,
  takeUntil,
} from "rxjs/operators";
import {isTrue} from "../../core-resources/tools/is/is.tool";

export class ObservableUtil {
  static asObservable<T>(BS: BehaviorSubject<T>): Observable<T> {
    return BS.pipe(skip(1));
  }

  /**
   * Subscribe to an observable to execute only once time the callback then unsubscribe
   * Use this method instead of take(1) that not unsubscribe, causing memory leak issue
   * @param observable
   * @param callback
   */
  static takeOneThenUnsubscribe<T>(observable: Observable<any>, callback: (...params: any) => void): Subscription {
    return this.subscribeToObservableThenUnsubscribeIfTrue(observable.pipe(
      take(1),
      map((...params: any) => {
        callback(...params);
        return true;
      }),
    ));
  }

  /**
   * Subscribe to an observable until true append in observable
   * Use this method to force unsubscribe, avoiding memory leak issue
   * @param observable
   */
  static subscribeToObservableThenUnsubscribeIfTrue(observable: Observable<boolean>): Subscription {
    const unsubscribeSubject: Subject<boolean> = new Subject<boolean>();
    return observable.pipe(takeUntil(unsubscribeSubject)).subscribe(unsubscribe => {
      if (isTrue(unsubscribe)) {
        unsubscribeSubject.next(true);
      }
    });
  }
}
