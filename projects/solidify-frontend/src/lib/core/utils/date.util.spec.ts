/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - date.util.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {DateUtil} from "./date.util";

describe("DateUtil", () => {
  beforeAll(() => {
    new DateUtil();
  });

  describe("convertToLocalDateDateSimple", () => {
    it("should convert date when Date object", () => {
      const date = new Date("2019-08-30");
      expect(DateUtil.convertToLocalDateDateSimple(date)).toBe("2019-08-30");
    });

    it("should convert date when Date object", () => {
      const date = new Date("2019-08-30 18:20:30");
      expect(DateUtil.convertToLocalDateDateSimple(date)).toBe("2019-08-30");
    });

    it("should convert date when Date is string", () => {
      const date = "2019-08-30";
      expect(DateUtil.convertToLocalDateDateSimple(date)).toBe("2019-08-30");
    });

    it("should convert date when Date is string with time", () => {
      const date = "2019-08-30 18:20:30";
      expect(DateUtil.convertToLocalDateDateSimple(date)).toBe("2019-08-30");
    });

    it("should return null when space", () => {
      const date = " ";
      expect(DateUtil.convertToLocalDateDateSimple(date)).toBeNull();
    });

    it("should return null when empty", () => {
      const date = "";
      expect(DateUtil.convertToLocalDateDateSimple(date)).toBeNull();
    });

    it("should return null when null", () => {
      const date = null;
      expect(DateUtil.convertToLocalDateDateSimple(date)).toBeNull();
    });

    it("should return null when undefined", () => {
      const date = undefined;
      expect(DateUtil.convertToLocalDateDateSimple(date)).toBeNull();
    });

    it("should return null when invalid", () => {
      const date = "test invalid";
      expect(DateUtil.convertToLocalDateDateSimple(date)).toBeNull();
    });
  });

  describe("convertToOffsetDateTimeIso8601", () => {
    it("should convert date when Date object", () => {
      const date = new Date("2019-08-30");
      expect(DateUtil.convertToOffsetDateTimeIso8601(date)).toBe("2019-08-30T02:00:00.000+0200");
    });

    it("should convert date when Date object", () => {
      const date = new Date("2019-08-30 18:20:30");
      expect(DateUtil.convertToOffsetDateTimeIso8601(date)).toBe("2019-08-30T18:20:30.000+0200");
    });

    it("should convert date when Date is string", () => {
      const date = "2019-08-30";
      expect(DateUtil.convertToOffsetDateTimeIso8601(date)).toBe("2019-08-30T00:00:00.000+0200");
    });

    it("should convert date when Date is string with time", () => {
      const date = "2019-08-30 18:20:30";
      expect(DateUtil.convertToOffsetDateTimeIso8601(date)).toBe("2019-08-30T18:20:30.000+0200");
    });

    it("should return null when space", () => {
      const date = " ";
      expect(DateUtil.convertToOffsetDateTimeIso8601(date)).toBeNull();
    });

    it("should return null when empty", () => {
      const date = "";
      expect(DateUtil.convertToOffsetDateTimeIso8601(date)).toBeNull();
    });

    it("should return null when null", () => {
      const date = null;
      expect(DateUtil.convertToOffsetDateTimeIso8601(date)).toBeNull();
    });

    it("should return null when undefined", () => {
      const date = undefined;
      expect(DateUtil.convertToOffsetDateTimeIso8601(date)).toBeNull();
    });

    it("should return null when invalid", () => {
      const date = "test invalid";
      expect(DateUtil.convertToOffsetDateTimeIso8601(date)).toBeNull();
    });
  });

  describe("convertOffsetDateTimeIso8601ToDate", () => {
    it("should convert date ISO 8601 in Date", () => {
      const date = "2019-08-30T18:20:30.012+0200";
      expect(DateUtil.convertOffsetDateTimeIso8601ToDate(date)).toEqual(new Date("2019-08-30 18:20:30.012"));
    });

    it("should return null when space", () => {
      const date = " ";
      expect(DateUtil.convertOffsetDateTimeIso8601ToDate(date)).toBeNull();
    });

    it("should return null when empty", () => {
      const date = "";
      expect(DateUtil.convertOffsetDateTimeIso8601ToDate(date)).toBeNull();
    });

    it("should return null when null", () => {
      const date = null;
      expect(DateUtil.convertOffsetDateTimeIso8601ToDate(date)).toBeNull();
    });

    it("should return null when undefined", () => {
      const date = undefined;
      expect(DateUtil.convertOffsetDateTimeIso8601ToDate(date)).toBeNull();
    });

    it("should return null when invalid", () => {
      const date = "test invalid";
      expect(DateUtil.convertOffsetDateTimeIso8601ToDate(date)).toBeNull();
    });
  });
});
