/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - user-preferences.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {CookieType} from "../cookie-consent/enums/cookie-type.enum";
import {CookieConsentUtil} from "../cookie-consent/utils/cookie-consent.util";
import {CookiePartialEnum} from "../enums/partial/cookie-partial.enum";
import {LanguagePartialEnum} from "../enums/partial/language-partial.enum";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {CookieHelper} from "../helpers/cookie.helper";
import {ExtendEnum} from "../../core-resources/types/extend-enum.type";

export class UserPreferencesUtil {
  static getPreferredDarkMode(environment: DefaultSolidifyEnvironment): boolean {
    const darkModeInStorage = CookieHelper.getItem(CookiePartialEnum.darkMode);
    let darkMode = environment.darkMode;
    if (darkModeInStorage === SOLIDIFY_CONSTANTS.STRING_TRUE) {
      darkMode = true;
    } else if (darkModeInStorage === SOLIDIFY_CONSTANTS.STRING_FALSE) {
      darkMode = false;
    }
    return darkMode;
  }

  static getPreferredLanguage(environment: DefaultSolidifyEnvironment): ExtendEnum<LanguagePartialEnum> {
    const savedLanguage = CookieHelper.getItem(CookiePartialEnum.language) as ExtendEnum<LanguagePartialEnum>;
    if (isNullOrUndefined(savedLanguage) || environment.appLanguages.indexOf(savedLanguage) === -1) {
      return this._determineDefaultLanguageAndStoreIntoCookie(environment);
    }
    return savedLanguage;
  }

  private static _determineDefaultLanguageAndStoreIntoCookie(environment: DefaultSolidifyEnvironment): ExtendEnum<LanguagePartialEnum> {
    const defaultLanguageDetermined = UserPreferencesUtil._determineDefaultLanguage(environment);
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.cookie, CookiePartialEnum.language)) {
      CookieHelper.setItem(CookiePartialEnum.language, defaultLanguageDetermined);
    }
    return defaultLanguageDetermined;
  }

  private static _determineDefaultLanguage(environment: DefaultSolidifyEnvironment): ExtendEnum<LanguagePartialEnum> {
    const languageFromLocalBrowser = UserPreferencesUtil._findCorrespondingLanguageToLocal(environment);
    if (languageFromLocalBrowser === null) {
      return environment.defaultLanguage;
    }
    return languageFromLocalBrowser;
  }

  private static _findCorrespondingLanguageToLocal(environment: DefaultSolidifyEnvironment): ExtendEnum<LanguagePartialEnum> {
    const browserLanguage: string = SsrUtil.window?.navigator.language;

    const foundLanguage: ExtendEnum<LanguagePartialEnum> = environment.appLanguages.find(l => browserLanguage.startsWith(l));
    if (isNotNullNorUndefined(foundLanguage)) {
      return foundLanguage;
    }

    return null;
  }
}
