/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - url.util.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {UrlUtil} from "./url.util";

const protocol = "http";
const domain = "localhost:4200";
const path = "test/titi/toto";
const queryParam = "param1=1&param2=2&param3=3";

const origin = `${protocol}://${domain}`;
const originWithPath = `${protocol}://${domain}/${path}`;
const originWithPathAndParam = `${protocol}://${domain}/${path}?${queryParam}`;

describe("UrlUtil", () => {
  describe("addUrlSeparatorAtEndIfMissing", () => {
    it("should add / if missing", () => {
      expect(UrlUtil.addUrlSeparatorAtEndIfMissing(origin)).toEqual(origin + "/");
    });

    it("should do nothing if present", () => {
      expect(UrlUtil.addUrlSeparatorAtEndIfMissing(origin + "/")).toEqual(origin + "/");
    });

    it("should do nothing in nothing", () => {
      expect(UrlUtil.addUrlSeparatorAtEndIfMissing("")).toEqual("");
    });

    it("should do nothing in undefined", () => {
      expect(UrlUtil.addUrlSeparatorAtEndIfMissing(undefined)).toEqual("");
    });

    it("should do nothing in null", () => {
      expect(UrlUtil.addUrlSeparatorAtEndIfMissing(null)).toEqual("");
    });
  });

  describe("removeUrlSeparatorAtEndIfMissing", () => {
    it("should do nothing", () => {
      expect(UrlUtil.removeUrlSeparatorAtEndIfPresent(origin)).toEqual(origin);
    });

    it("should remove /", () => {
      expect(UrlUtil.removeUrlSeparatorAtEndIfPresent(origin + "/")).toEqual(origin);
    });

    it("should do nothing in nothing", () => {
      expect(UrlUtil.removeUrlSeparatorAtEndIfPresent("")).toEqual("");
    });

    it("should do nothing in undefined", () => {
      expect(UrlUtil.removeUrlSeparatorAtEndIfPresent(undefined)).toEqual("");
    });

    it("should do nothing in null", () => {
      expect(UrlUtil.removeUrlSeparatorAtEndIfPresent(null)).toEqual("");
    });
  });

  describe("removeParametersFromUrl", () => {
    it("should remove param", () => {
      expect(UrlUtil.removeParametersFromUrl(originWithPathAndParam)).toEqual(originWithPath);
    });

    it("should do nothing with orgin with path", () => {
      expect(UrlUtil.removeParametersFromUrl(originWithPath)).toEqual(originWithPath);
    });

    it("should do nothing with orgin", () => {
      expect(UrlUtil.removeParametersFromUrl(origin)).toEqual(origin);
    });

    it("should do nothing with domain", () => {
      expect(UrlUtil.removeParametersFromUrl(domain)).toEqual(domain);
    });

    it("should do nothing in nothing", () => {
      expect(UrlUtil.removeParametersFromUrl("")).toEqual("");
    });

    it("should do nothing in undefined", () => {
      expect(UrlUtil.removeParametersFromUrl(undefined)).toEqual("");
    });

    it("should do nothing in null", () => {
      expect(UrlUtil.removeParametersFromUrl(null)).toEqual("");
    });
  });

  describe("getPath", () => {
    it("should retrieve path in origin with path and param", () => {
      expect(UrlUtil.getPath(originWithPathAndParam)).toEqual(path);
    });

    it("should retrieve path in origin in path", () => {
      expect(UrlUtil.getPath(originWithPath)).toEqual(path);
    });

    it("should retrieve nothing in origin", () => {
      expect(UrlUtil.getPath(origin)).toEqual("");
    });

    it("should retrieve nothing in nothing", () => {
      expect(UrlUtil.getPath("")).toEqual("");
    });

    it("should retrieve nothing in undefined", () => {
      expect(UrlUtil.getPath(undefined)).toEqual("");
    });

    it("should retrieve nothing in null", () => {
      expect(UrlUtil.getPath(null)).toEqual("");
    });
  });

  describe("getDomain", () => {
    it("should retrieve domain in origin with path and param", () => {
      expect(UrlUtil.getDomain(originWithPathAndParam)).toEqual(domain);
    });

    it("should retrieve domain in origin in path", () => {
      expect(UrlUtil.getDomain(originWithPath)).toEqual(domain);
    });

    it("should retrieve domain in origin", () => {
      expect(UrlUtil.getDomain(origin)).toEqual(domain);
    });

    it("should retrieve domain in domain", () => {
      expect(UrlUtil.getDomain(domain)).toEqual(domain);
    });

    it("should retrieve nothing in nothing", () => {
      expect(UrlUtil.getDomain("")).toEqual("");
    });

    it("should retrieve nothing in undefined", () => {
      expect(UrlUtil.getDomain(undefined)).toEqual("");
    });

    it("should retrieve nothing in null", () => {
      expect(UrlUtil.getDomain(null)).toEqual("");
    });
  });

  describe("getOrigin", () => {
    it("should retrieve origin in origin with path and param", () => {
      expect(UrlUtil.getOrigin(originWithPathAndParam)).toEqual(origin);
    });

    it("should retrieve origin in origin in path", () => {
      expect(UrlUtil.getOrigin(originWithPath)).toEqual(origin);
    });

    it("should retrieve domain in origin", () => {
      expect(UrlUtil.getOrigin(origin)).toEqual(origin);
    });

    it("should retrieve domain in domain", () => {
      expect(UrlUtil.getOrigin(domain)).toEqual(domain);
    });

    it("should retrieve nothing in nothing", () => {
      expect(UrlUtil.getOrigin("")).toEqual("");
    });

    it("should retrieve nothing in undefined", () => {
      expect(UrlUtil.getOrigin(undefined)).toEqual("");
    });

    it("should retrieve nothing in null", () => {
      expect(UrlUtil.getOrigin(null)).toEqual("");
    });
  });

  describe("getProtocol", () => {
    it("should retrieve protocol in origin with path and param", () => {
      expect(UrlUtil.getProtocol(originWithPathAndParam)).toEqual(protocol);
    });

    it("should retrieve protocol in origin in path", () => {
      expect(UrlUtil.getProtocol(originWithPath)).toEqual(protocol);
    });

    it("should retrieve protocol in origin", () => {
      expect(UrlUtil.getProtocol(origin)).toEqual(protocol);
    });

    it("should retrieve nothing in domain", () => {
      expect(UrlUtil.getProtocol(domain)).toEqual("");
    });

    it("should retrieve nothing in nothing", () => {
      expect(UrlUtil.getProtocol("")).toEqual("");
    });

    it("should retrieve nothing in undefined", () => {
      expect(UrlUtil.getProtocol(undefined)).toEqual("");
    });

    it("should retrieve nothing in null", () => {
      expect(UrlUtil.getProtocol(null)).toEqual("");
    });
  });
});
