/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - label.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Store} from "@ngxs/store";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {LanguagePartialEnum} from "../enums/partial/language-partial.enum";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {Label} from "../models/label.model";
import {
  isFunction,
  isNonEmptyArray,
  isNotNullNorUndefined,
} from "../../core-resources/tools/is/is.tool";
import {MemoizedUtil} from "./stores/memoized.util";

export class LabelUtil {
  static getTranslationFromListLabelsWithStore(store: Store, environment: DefaultSolidifyEnvironment, listLabels: Label[], languageEnumConverter?: (currentLanguage: LanguagePartialEnum) => string): string {
    const currentLanguage = MemoizedUtil.selectSnapshot(store, environment.appState, state => state.appLanguage);
    return LabelUtil.getTranslationFromListLabels(currentLanguage, listLabels, environment.labelEnumConverter);
  }

  static getTranslationFromListLabels(currentLanguage: LanguagePartialEnum, listLabels: Label[], languageEnumConverter?: (currentLanguage: LanguagePartialEnum) => string): string {
    if (isNotNullNorUndefined(listLabels) && isNonEmptyArray(listLabels)) {
      for (const label of listLabels) {
        if (LabelUtil.isSameLanguage(currentLanguage, label, languageEnumConverter)) {
          return label.text;
        }
      }
    }
    return SOLIDIFY_CONSTANTS.STRING_EMPTY;
  }

  static isSameLanguage: (currentLanguage: LanguagePartialEnum, label: Label, languageEnumConverter?: (currentLanguage: LanguagePartialEnum) => string) => boolean = (currentLanguage, label, languageEnumConverter) =>
    (isFunction(languageEnumConverter) && (label.languageCode === languageEnumConverter(currentLanguage) || label.language?.code === languageEnumConverter(currentLanguage)))
    ||
    (!isFunction(languageEnumConverter) && (label.languageCode === currentLanguage || label.language?.code === currentLanguage));
}
