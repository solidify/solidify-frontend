/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-metadata.util.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {ResourceNameSpace} from "../../stores/abstract/resource/resource-namespace.model";
import {
  SOLIDIFY_META_KEY,
  SolidifyMetaDataModel,
  SolidifyMetadataUtil,
  SolidifyStateClass,
} from "./solidify-metadata.util";

describe("SolidifyMetadataUtil", () => {
  describe("ensureStoreSolidifyMetadata", () => {
    it("should return metadata when no meta defined", () => {
      const target = {};
      const result = SolidifyMetadataUtil.ensureStoreSolidifyMetadata<ResourceNameSpace>(target as SolidifyStateClass<ResourceNameSpace>);
      expect(result).not.toBeNull();
      expect(result).not.toBeUndefined();
      expect(target[SOLIDIFY_META_KEY]).toBe(result);
      expect(result.excludedRegisteredDefaultActionFns).toBeUndefined();
      expect(result.defaultActions).toBeUndefined();
    });

    it("should return metadata when meta defined", () => {
      const target = {
        [SOLIDIFY_META_KEY]: {
          defaultActions: [],
          excludedRegisteredDefaultActionFns: [],
        } as SolidifyMetaDataModel<ResourceNameSpace>,
      };
      const result = SolidifyMetadataUtil.ensureStoreSolidifyMetadata<ResourceNameSpace>(target as SolidifyStateClass<ResourceNameSpace>);
      expect(result).not.toBeNull();
      expect(result).not.toBeUndefined();
      expect(target[SOLIDIFY_META_KEY]).toBe(result);
      expect(result.excludedRegisteredDefaultActionFns.length).toBe(0);
      expect(result.defaultActions.length).toBe(0);
    });
  });

});
