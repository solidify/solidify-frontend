/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-metadata.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {RegisteredDefaultAction} from "../../decorators/store.decorator";
import {BaseStoreNameSpace} from "../../models/stores/base-store-namespace.model";
import {isTruthyObject} from "../../../core-resources/tools/is/is.tool";
import defineProperty = Reflect.defineProperty;

export const SOLIDIFY_META_KEY = "SOLIDIFY_META";

export interface SolidifyStateClass<T extends BaseStoreNameSpace> {
  [SOLIDIFY_META_KEY]?: SolidifyMetaDataModel<T>;

  new(...args: any[]): any;
}

export interface SolidifyMetaDataModel<T extends BaseStoreNameSpace> {
  defaultActions?: RegisteredDefaultAction<T>[] | undefined;
  excludedRegisteredDefaultActionFns?: string[] | undefined;
}

export class SolidifyMetadataUtil {
  /**
   * Ensures metadata is attached to the class and returns it.
   */
  static ensureStoreSolidifyMetadata<T extends BaseStoreNameSpace>(target: SolidifyStateClass<T>): SolidifyMetaDataModel<T> {
    if (!target.hasOwnProperty(SOLIDIFY_META_KEY) || !isTruthyObject(target[SOLIDIFY_META_KEY])) {
      defineProperty(target, SOLIDIFY_META_KEY, {value: {}, enumerable: true, writable: true});
    }
    return target[SOLIDIFY_META_KEY];
  }
}
