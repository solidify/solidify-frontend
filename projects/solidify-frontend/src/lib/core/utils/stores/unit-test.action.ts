/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - unit-test.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
} from "../../models/stores/base.action";

export const unitTestStateName = "unitTest";

export namespace UnitTestAction {
  export class Test extends BaseAction {
    static readonly type: string = `[${unitTestStateName}] Test`;

    constructor(public resultExpected: boolean, public throwError: boolean = false) {
      super();
    }
  }

  export class TestSuccess extends BaseSubActionSuccess<Test> {
    static readonly type: string = `[${unitTestStateName}] Test Success`;
  }

  export class TestFail extends BaseSubActionFail<Test> {
    static readonly type: string = `[${unitTestStateName}] Test Fail`;
  }
}
