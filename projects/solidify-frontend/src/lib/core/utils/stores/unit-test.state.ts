/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - unit-test.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  Action,
  State,
} from "@ngxs/store";
import {
  Observable,
  timer,
} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {SolidifyStateError} from "../../errors/solidify-state.error";
import {BaseStateModel} from "../../models/stores/base-state.model";
import {SolidifyStateContext} from "../../models/stores/state-context.model";
import {BasicState} from "../../stores/abstract/base/basic.state";
import {
  UnitTestAction,
  unitTestStateName,
} from "./unit-test.action";

export interface UnitTestStateModel extends BaseStateModel {
  numberSuccess: number;
  numberSuccessPending: number;
  numberFail: number;
  numberFailPending: number;
}

@Injectable()
@State<UnitTestStateModel>({
  name: unitTestStateName,
  defaults: {
    isLoadingCounter: 0,
    numberSuccess: 0,
    numberSuccessPending: 0,
    numberFail: 0,
    numberFailPending: 0,
  },
})
export class UnitTestState extends BasicState<UnitTestStateModel> {
  constructor() {
    super();
  }

  @Action(UnitTestAction.Test)
  test(ctx: SolidifyStateContext<UnitTestStateModel>, action: UnitTestAction.Test): Observable<any> {
    const state = {
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    } as UnitTestStateModel;
    let actionDispatched;
    if (action.resultExpected) {
      actionDispatched = new UnitTestAction.TestSuccess(action);
      state.numberSuccessPending = ctx.getState().numberSuccessPending + 1;
    } else {
      actionDispatched = new UnitTestAction.TestFail(action);
      state.numberFailPending = ctx.getState().numberFailPending + 1;
    }
    ctx.patchState(state);

    return timer(100).pipe(
      tap(() => {
        if (action.throwError) {
          throw new Error();
        }
        ctx.dispatch(actionDispatched);
      }),
      catchError(error => {
        ctx.dispatch(new UnitTestAction.TestFail(action));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(UnitTestAction.TestSuccess)
  testSuccess(ctx: SolidifyStateContext<UnitTestStateModel>, action: UnitTestAction.TestSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      numberSuccess: ctx.getState().numberSuccess + 1,
      numberSuccessPending: ctx.getState().numberSuccessPending - 1,
    });
  }

  @Action(UnitTestAction.TestFail)
  testFail(ctx: SolidifyStateContext<UnitTestStateModel>, action: UnitTestAction.TestFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      numberFail: ctx.getState().numberFail + 1,
      numberFailPending: ctx.getState().numberFailPending - 1,
    });
  }
}

