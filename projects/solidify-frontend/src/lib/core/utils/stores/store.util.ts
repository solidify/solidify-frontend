/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - store.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpErrorResponse} from "@angular/common/http";
import {
  ElementRef,
  Type,
} from "@angular/core";
import {AbstractControl} from "@angular/forms";
import {Navigate} from "@ngxs/router-plugin";
import {
  ActionCompletion,
  ActionOptions,
  Actions,
  Store,
} from "@ngxs/store";
import {ɵensureStoreMetadata} from "@ngxs/store/internals";
import _ from "lodash";
import {
  defer,
  from,
  merge,
  MonoTypeOperatorFunction,
  Observable,
  of,
  pipe,
  timeout,
  zip,
} from "rxjs";
import {
  catchError,
  concatMap,
  filter,
  map,
  take,
  takeUntil,
  tap,
  toArray,
} from "rxjs/operators";
import {
  isEmptyArray,
  isFalse,
  isFunction,
  isInstanceOf,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isTrue,
  isTruthyObject,
} from "../../../core-resources/tools/is/is.tool";
import {SolidifyObject} from "../../../core-resources/types/solidify-object.type";
import {ObjectUtil} from "../../../core-resources/utils/object.util";
import {SsrUtil} from "../../../core-resources/utils/ssr.util";
import {FORM_CONTROL_ELEMENT_REF} from "../../directives/form-control-element-ref/form-control-element-ref.directive";
import {AppRoutesPartialEnum} from "../../enums/partial/app-routes-partial.enum";
import {RoutesPartialEnum} from "../../enums/partial/routes-partial.enum";
import {SolidifyError} from "../../errors/solidify.error";
import {ErrorHelper} from "../../helpers/error.helper";
import {
  BaseResource,
  BaseResourceType,
} from "../../../core-resources/models/dto/base-resource.model";
import {CollectionTyped} from "../../models/dto/collection-typed.model";
import {ValidationErrorDto} from "../../models/errors/error-dto.model";
import {FormError} from "../../models/errors/form-error.model";
import {SolidifyHttpErrorResponseModel} from "../../models/errors/solidify-http-error-response.model";
import {ModelFormControlEvent} from "../../models/forms/model-form-control-event.model";
import {QueryParameters} from "../../../core-resources/models/query-parameters/query-parameters.model";
import {NotifierService} from "../../models/services/notifier-service.model";
import {BaseResourceStateModel} from "../../models/stores/base-resource-state.model";
import {BaseStateModel} from "../../models/stores/base-state.model";
import {
  ActionSubActionCompletionsWrapper,
  BaseAction,
  BaseSubAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  MultiActionsCompletionsResult,
} from "../../models/stores/base.action";
import {StoreActionClass} from "../../models/stores/state-action.model";
import {SolidifyStateContext} from "../../models/stores/state-context.model";
import {SubResourceUpdateModel} from "../../models/stores/sub-resource-update.model";
import {AssociationNoSqlReadOnlyStateModel} from "../../stores/abstract/association-no-sql-read-only/association-no-sql-read-only-state.model";
import {BasicState} from "../../stores/abstract/base/basic.state";
import {CompositionStateModel} from "../../stores/abstract/composition/composition-state.model";
import {ResourceStateModel} from "../../stores/abstract/resource/resource-state.model";
import {ExtendEnum} from "../../../core-resources/types/extend-enum.type";
import {
  SOLIDIFY_ERRORS,
  SOLIDIFY_VALIDATION_ERROR_ALREADY_DISPLAYED,
} from "../validations/validation.util";
import {SolidifyMetadataUtil} from "./solidify-metadata.util";
import {
  ofSolidifyActionCompleted,
  ofSolidifyActionDispatched,
} from "./store.tool";

// @dynamic
export class StoreUtil {
  private static readonly _BULK_DISPATCH_TIMEOUT_FOR_NON_DISPATCHED_SUB_ACTION_BROWSER: number = 20000;
  private static readonly _BULK_DISPATCH_TIMEOUT_FOR_NON_DISPATCHED_SUB_ACTION_SERVER: number = 2000;
  static readonly BULK_DISPATCH_TIMEOUT_FOR_NON_DISPATCHED_SUB_ACTION: number = SsrUtil.isServer ? this._BULK_DISPATCH_TIMEOUT_FOR_NON_DISPATCHED_SUB_ACTION_SERVER : this._BULK_DISPATCH_TIMEOUT_FOR_NON_DISPATCHED_SUB_ACTION_BROWSER;
  private static readonly _ACTION_TYPE_ATTRIBUTE_NAME: string = "type";
  private static readonly _ATTRIBUTE_STATE_NAME: string = "name";
  private static readonly _NGXS_META_OPTIONS_KEY: string = "NGXS_OPTIONS_META";

  // TODO : Enhance typing ?
  static initState<T>(baseConstructor: Function, constructor: Function, nameSpace: T): void {
    if (SsrUtil.isServer) {
      const stateName: string = constructor[this._NGXS_META_OPTIONS_KEY].name;
      if (SsrUtil.listSolidifyStateRegistered.indexOf(stateName) >= 0) {
        // Allow to avoid SSR multiple call on states methods with annotation @RegisterDefaultAction
        return;
      }
      SsrUtil.listSolidifyStateRegistered.push(stateName);
    }
    const baseMeta = SolidifyMetadataUtil.ensureStoreSolidifyMetadata<T>(baseConstructor as any);
    const defaultActions = baseMeta.defaultActions;
    if (Array.isArray(defaultActions)) {
      const meta = SolidifyMetadataUtil.ensureStoreSolidifyMetadata<T>(constructor as any);
      const excludedRegisteredDefaultActionFns = meta.excludedRegisteredDefaultActionFns;
      const safeExcludedFns = Array.isArray(excludedRegisteredDefaultActionFns) ? excludedRegisteredDefaultActionFns : [];
      defaultActions.forEach(({fn, callback, options}) => {
        if (safeExcludedFns.indexOf(fn) < 0) {
          this.initDefaultAction(constructor, fn, callback(nameSpace), options);
        }
      });
    }
  }

  static getDefaultData<TResource extends BaseResourceType>(constructor: Function): ResourceStateModel<TResource> {
    const meta = ɵensureStoreMetadata(constructor as any);
    return meta.defaults;
  }

  static initDefaultAction(constructor: Function, fn: string, storeActionClass: StoreActionClass, options?: ActionOptions): void {
    if (storeActionClass === undefined) {
      return;
    }
    const meta = ɵensureStoreMetadata(constructor as any);
    const type = storeActionClass.type;
    if (!type) {
      throw new Error(`Action ${storeActionClass.name} is missing a static "type" property`);
    }
    if (!meta.actions[type]) {
      meta.actions[type] = [];
    }
    meta.actions[type].push({
      fn,
      options: options || {},
      type,
    });
  }

  static getQueryParametersToApply(queryParameters: QueryParameters, ctx: SolidifyStateContext<BaseResourceStateModel>): QueryParameters {
    return queryParameters == null ? ctx.getState().queryParameters : queryParameters;
  }

  static updateQueryParameters<TResource>(ctx: StateContextOnBaseResource<TResource>, list: CollectionTyped<TResource> | null | undefined): QueryParameters {
    const queryParameters = ObjectUtil.clone(ctx.getState().queryParameters);
    const paging = ObjectUtil.clone(queryParameters.paging);
    paging.length = list === null || list === undefined ? 0 : list._page.totalItems;
    queryParameters.paging = paging;
    return queryParameters;
  }

  static dispatchActionAndWaitForSubActionCompletion<TResource extends BaseResource, UBaseAction extends BaseAction, VBaseSubAction extends BaseSubAction<UBaseAction>, WBaseSubAction extends BaseSubAction<UBaseAction>>(
    ctx: SolidifyStateContext<ResourceStateModel<TResource>> | Store,
    actions$: Actions,
    action: UBaseAction,
    subActionSuccess: Type<VBaseSubAction>,
    callbackSuccess: (res: VBaseSubAction) => void,
    subActionFail?: Type<WBaseSubAction>,
    callbackFail?: (res: WBaseSubAction) => void,
  ): Observable<VBaseSubAction | WBaseSubAction> {
    const listActionSuccess = [subActionSuccess] as any;
    if (isNotNullNorUndefined(subActionFail)) {
      listActionSuccess.push(subActionFail);
    }
    const observable = actions$.pipe(
      ofSolidifyActionCompleted(...listActionSuccess),
      filter(result => result.action.parentAction === action && result.result.successful),
      take(1),
      map(result => {
        if (isInstanceOf(result.action, subActionSuccess)) {
          callbackSuccess(result.action);
          return result.action;
        } else if (isNotNullNorUndefined(subActionFail) && isInstanceOf(result.action, subActionFail) && isFunction(callbackFail)) {
          callbackFail(result.action);
          return result.action;
        }
        return;
      }),
    );

    ctx.dispatch(action);
    return observable;
  }

  static waitForSubActionCompletion<TResource extends BaseResource, UBaseAction extends BaseAction, VBaseSubAction extends BaseSubAction<UBaseAction>>(
    actions$: Actions,
    action: UBaseAction,
    subActionSuccess: Type<VBaseSubAction>,
    callback: (res: VBaseSubAction) => void,
  ): Observable<VBaseSubAction> {
    const observable = actions$.pipe(
      ofSolidifyActionCompleted(subActionSuccess),
      filter(result => result.action.parentAction === action && result.result.successful),
      take(1),
      map(result => {
        callback(result.action);
        return result.action;
      }),
    );
    return observable;
  }

  static dispatchParallelActionAndWaitForSubActionsCompletion<TResource>(ctx: SolidifyStateContext<TResource> | Store,
                                                                         actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[],
                                                                         timeoutForNonDispatchedSubAction: number | undefined = this.BULK_DISPATCH_TIMEOUT_FOR_NON_DISPATCHED_SUB_ACTION): Observable<MultiActionsCompletionsResult> {
    if (actionSubActionCompletionsWrappers.length === 0) {
      return of({
        success: true,
        listActionFail: [],
        listActionSuccess: [],
        listActionGoalUndefined: [],
        listActionWithoutSubAction: [],
        listActionInError: [],
        rawResult: [],
      } as MultiActionsCompletionsResult);
    }

    const actions = new Array(actionSubActionCompletionsWrappers.length);
    const subActionCompletionObservables = [];
    actionSubActionCompletionsWrappers.forEach(
      (actionSubActionCompletionsWrapper, i) => {
        actions[i] = actionSubActionCompletionsWrapper.action;
        if (isNonEmptyArray(actionSubActionCompletionsWrapper.subActionCompletions)) {
          subActionCompletionObservables.push(
            merge(...actionSubActionCompletionsWrapper.subActionCompletions)
              .pipe(
                filter(actionCompletion => this.hasParentAction(actionCompletion.action, actionSubActionCompletionsWrapper.action)),
              ),
          );
        }
      },
    );
    if (subActionCompletionObservables.length === 0) {
      ctx.dispatch(actions).pipe(catchError(err => of({error: err})));
      return of(this._formatBulkActionExecutionResult([], actionSubActionCompletionsWrappers));
    }
    return zip(
      zip(...subActionCompletionObservables),
      defer(() => ctx.dispatch(actions).pipe(catchError(err => of({error: err})))),
    ).pipe(
      timeoutForNonDispatchedSubAction > 0 ? timeout(timeoutForNonDispatchedSubAction) : pipe(),
      map(values => {
        const listSubActionResult = values[0] as ActionCompletion<BaseSubAction<BaseAction>, Error>[];
        return this._formatBulkActionExecutionResult(listSubActionResult, actionSubActionCompletionsWrappers);
      }),
      catchError(err => {
        if (err.name === "TimeoutError") {
          // eslint-disable-next-line no-console
          console.error("Timeout on parallel dispatch, no expected sub action dispatched for at least one action : ", actions);
          throw new SolidifyError("Timeout on parallel dispatch, no expected sub action dispatched for at least one action", err);
        }
        throw err;
      }),
      catchError(err => of({
        success: false,
        error: err,
      } as MultiActionsCompletionsResult)),
    );
  }

  static dispatchSequentialActionAndWaitForSubActionsCompletion<TResource>(ctx: SolidifyStateContext<TResource> | Store,
                                                                           actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[],
                                                                           timeoutForNonDispatchedSubAction: number | undefined = this.BULK_DISPATCH_TIMEOUT_FOR_NON_DISPATCHED_SUB_ACTION,
                                                                           failFastOnError: boolean = false): Observable<MultiActionsCompletionsResult> {
    const obs = from(actionSubActionCompletionsWrappers)
      .pipe(
        concatMap((action) => {
          let subActionCompletionObs: Observable<ActionCompletion<BaseSubAction<BaseAction>, Error>> = of(undefined);

          if (isNonEmptyArray(action.subActionCompletions)) {
            subActionCompletionObs = merge(...action.subActionCompletions)
              .pipe(
                filter(actionCompletion => this.hasParentAction(actionCompletion.action, action.action)),
              );
          }

          return zip(
            zip(subActionCompletionObs),
            defer(() => ctx.dispatch(action.action).pipe(
              catchError(err => {
                if (failFastOnError) {
                  return err;
                }
                return of({error: err});
              }),
            )),
          ).pipe(
            timeoutForNonDispatchedSubAction > 0 ? timeout(timeoutForNonDispatchedSubAction) : pipe(),
            map(values => {
              const listSubActionResult = values[0] as ActionCompletion<BaseSubAction<BaseAction>, Error>[];
              return listSubActionResult[0];
            }),
            catchError(err => {
              if (err.name === "TimeoutError") {
                // eslint-disable-next-line no-console
                console.error("Timeout on sequential dispatch, no expected sub action dispatched for action", action.action);
                throw new SolidifyError("Timeout on sequential dispatch, no expected sub action dispatched for action : " + action.action.constructor[this._ACTION_TYPE_ATTRIBUTE_NAME], err);
              }
              if (failFastOnError) {
                return err;
              }
              return of({error: err});
            }),
          );
        }),
      );

    return obs.pipe(
      toArray(),
      map((listSubActionResult: ActionCompletion<BaseSubAction<BaseAction>, Error>[]) => {
        listSubActionResult = listSubActionResult.filter(s => isNotNullNorUndefined(s));
        return this._formatBulkActionExecutionResult(listSubActionResult, actionSubActionCompletionsWrappers);
      }),
      catchError(err => of({
        success: false,
        error: err,
      } as MultiActionsCompletionsResult)),
    );
  }

  private static _formatBulkActionExecutionResult(listSubActionResult: ActionCompletion<BaseSubAction<BaseAction>, Error>[],
                                                  actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[]): MultiActionsCompletionsResult {
    const result = {
      success: true,
      listActionSuccess: [],
      listActionFail: [],
      listActionInError: [],
      listActionGoalUndefined: [],
      listActionWithoutSubAction: [],
      rawResult: listSubActionResult,
    } as MultiActionsCompletionsResult;

    listSubActionResult.forEach(s => {
      if (isNotNullNorUndefined(s.result.error)) {
        throw s.result.error;
      }
      if (s.result.successful) {
        if (s.action instanceof BaseSubActionSuccess) {
          result.listActionSuccess.push(s.action);
        } else if (s.action instanceof BaseSubActionFail) {
          result.listActionFail.push(s.action);
        } else {
          result.listActionGoalUndefined.push(s.action);
        }
      } else {
        result.listActionInError.push(s.action);
      }
    });

    const listActionWithoutSubAction = actionSubActionCompletionsWrappers.map(s => s.action);
    actionSubActionCompletionsWrappers.forEach(s => {
      const subActionDispatched: BaseSubAction<any> = listSubActionResult.find(r => r.action.parentAction === s.action)?.action;
      if (isNotNullNorUndefined(subActionDispatched)) {

        const indexOfActionWithoutSubAction = listActionWithoutSubAction.indexOf(s.action);
        if (indexOfActionWithoutSubAction !== -1) {
          listActionWithoutSubAction.splice(indexOfActionWithoutSubAction, 1);
        }

        if (subActionDispatched instanceof BaseSubActionFail) {
          result.success = false;
          return; // = continue
        }
      }
      if (isFalse(result.success)) {
        return; // = continue
      }
      const needSubAction = isNonEmptyArray(s.subActionCompletions);
      if (!needSubAction) {
        return; // = continue
      }
      result.success = isNotNullNorUndefined(subActionDispatched);
    });

    result.listActionWithoutSubAction = listActionWithoutSubAction;
    return result;
  }

  static hasParentAction(action: BaseAction, parentAction: BaseAction): boolean {
    if (!isTruthyObject(action) || !isTruthyObject(parentAction)) {
      return false;
    }
    const parents: BaseAction[] = [];
    let currentAction = action;
    while (isTruthyObject(currentAction.parentAction)) {
      const currentParentAction = currentAction.parentAction;
      if (currentParentAction === parentAction) {
        return true;
      }
      if (currentParentAction === action || parents.includes(currentParentAction)) {
        return false;
      }
      parents.push(currentParentAction);
      currentAction = currentParentAction;
    }
    return false;
  }

  static isLoadingState(baseState: BaseStateModel): boolean {
    return baseState.isLoadingCounter > 0;
  }

  static cancelUncompleted<TResource, TResult = TResource>(currentAction: BaseAction, ctx: SolidifyStateContext<BaseStateModel> | StateContextOnBaseResource<TResource>, actions$: Actions, allowedTypes: any[], noLoadingCounterDecrement?: boolean): MonoTypeOperatorFunction<TResult> {
    return pipe(
      takeUntil(
        actions$.pipe(
          ofSolidifyActionDispatched(...allowedTypes),
          filter(dispatchedAction => {
            // if navigate action that go to detail, do not cancel the action
            if (dispatchedAction.constructor["type"] === Navigate.type) {
              const listPath = (dispatchedAction as Navigate).path;
              if (isEmptyArray(listPath)) {
                return false;
              }
              const path: string = listPath[listPath.length - 1];
              return !(path.endsWith("/" + AppRoutesPartialEnum.edit) || path === AppRoutesPartialEnum.edit);
            }

            // if same action as current, do not cancel the action
            return !(currentAction.constructor["type"] === dispatchedAction.constructor["type"] && currentAction === dispatchedAction);
          }),
          tap(() => {
              if (!noLoadingCounterDecrement) {
                ctx.patchState({
                  isLoadingCounter: ctx.getState().isLoadingCounter - 1,
                });
              }
            },
          ),
        ),
      ),
    );
  }

  static determineSubResourceChange(oldList: string[], newList: string[], updateAvailable: boolean = false): SubResourceUpdateModel {
    const subResourceUpdate: SubResourceUpdateModel = new SubResourceUpdateModel();
    const diff: string[] = _.xor(oldList, newList);
    diff.forEach(d => {
      if (_.includes(oldList, d)) {
        subResourceUpdate.resourceToRemoved.push(d);
      } else {
        subResourceUpdate.resourceToAdd.push(d);
      }
    });
    if (updateAvailable && isNonEmptyArray(subResourceUpdate.resourceToRemoved) && isNonEmptyArray(subResourceUpdate.resourceToAdd)) {
      subResourceUpdate.resourceToRemoved = [];
      subResourceUpdate.resourceToUpdate = subResourceUpdate.resourceToAdd;
      subResourceUpdate.resourceToAdd = [];
    }
    return subResourceUpdate;
  }

  static catchValidationErrors<T>(ctx: SolidifyStateContext<ResourceStateModel<T>>, modelFormControlEvent: ModelFormControlEvent<T>, notifierService: NotifierService, autoScrollToFirstValidationError: boolean): MonoTypeOperatorFunction<T> {
    return catchError((error: SolidifyHttpErrorResponseModel | SolidifyError | Error | HttpErrorResponse) => {
      let errorToTreat = error;
      if (error instanceof SolidifyError) {
        errorToTreat = error.nativeError;
      }
      const validationErrors = ErrorHelper.extractValidationErrorsNotAlreadyDisplayedFromError(errorToTreat);
      if (isNonEmptyArray(validationErrors) && isTruthyObject(modelFormControlEvent.formControl)) {
        const unbindValidationError = this._applyValidationErrorsOnFormControl(modelFormControlEvent, validationErrors, autoScrollToFirstValidationError);
      }
      throw error;
    });
  }

  private static _applyValidationErrorsOnFormControl<T>(modelFormControlEvent: ModelFormControlEvent<T>, validationErrors: ValidationErrorDto[], autoScrollToFirstValidationError: boolean): ValidationErrorDto[] {
    Object.keys(modelFormControlEvent.formControl.value).forEach(fieldName => {
      const formControl = modelFormControlEvent.formControl.get(fieldName);
      if (!isTruthyObject(formControl)) {
        return;
      }
      const hasToUpdateValueAndValidity = (isTruthyObject(formControl[SOLIDIFY_ERRORS]) && (formControl[SOLIDIFY_ERRORS] as FormError).errorsFromBackend !== null) || !isNullOrUndefined(formControl.errors);
      delete formControl[SOLIDIFY_ERRORS];
      if (hasToUpdateValueAndValidity) {
        formControl.updateValueAndValidity();
      }
    });
    return this.iterateOverValidationErrorToBindIntoFormControlIfFoundIt(modelFormControlEvent, validationErrors, autoScrollToFirstValidationError);
  }

  static iterateOverValidationErrorToBindIntoFormControlIfFoundIt<T>(modelFormControlEvent: ModelFormControlEvent<T>, validationErrors: ValidationErrorDto[], autoScrollToFirstValidationError: boolean): ValidationErrorDto[] {
    const unbindValidationErrors = [];
    let hasFormControlWithError = false;
    let firstValidationErrorFc: AbstractControl;
    validationErrors.forEach(error => {
      if (isNotNullNorUndefined(modelFormControlEvent.listFieldNameToDisplayErrorInToast)
        && modelFormControlEvent.listFieldNameToDisplayErrorInToast.indexOf(error.fieldName) !== -1) {
        error[SOLIDIFY_VALIDATION_ERROR_ALREADY_DISPLAYED] = false;
        unbindValidationErrors.push(error);
        return;
      }
      const formControl = modelFormControlEvent.formControl.get(error.fieldName);
      if (isNullOrUndefined(formControl)) {
        error[SOLIDIFY_VALIDATION_ERROR_ALREADY_DISPLAYED] = false;
        unbindValidationErrors.push(error);
        // eslint-disable-next-line no-console
        console.warn(`Unable to bind error for field name "${error.fieldName}". No FormControl exist in this form with this name.`);
        return;
      }
      hasFormControlWithError = true;

      if (isTrue(autoScrollToFirstValidationError) && isNullOrUndefined(firstValidationErrorFc)) {
        const elementRef = this.getElementRefOnCurrentAbstractControlOrOnParents(formControl);
        if (isNotNullNorUndefined(elementRef?.nativeElement)) {
          firstValidationErrorFc = formControl;
          elementRef.nativeElement.scrollIntoView({behavior: "smooth", block: "center"});
        }
      }
      let errors = formControl.errors;
      if (isNullOrUndefined(errors)) {
        errors = {};
      }
      formControl[SOLIDIFY_ERRORS] = {errorsFromBackend: error.errorMessages} as FormError;
      Object.assign(errors, {errorsFromBackend: error.errorMessages});
      formControl.setErrors(errors);
      formControl.markAsTouched();
      error[SOLIDIFY_VALIDATION_ERROR_ALREADY_DISPLAYED] = true;
      // formControl.updateValueAndValidity(); ==> Remove error set with formControl.setErrors(errors);...
    });
    if (hasFormControlWithError) {
      this._openAllExpandableSections(modelFormControlEvent);
    }
    modelFormControlEvent?.changeDetectorRef?.detectChanges();
    return unbindValidationErrors;
  }

  private static _openAllExpandableSections<T>(modelFormControlEvent: ModelFormControlEvent<T>): void {
    modelFormControlEvent.listPanelExpandablePresentational?.forEach(panelExpandablePresentational => {
      if (!panelExpandablePresentational.isOpen) {
        panelExpandablePresentational.toggle();
        panelExpandablePresentational.externalDetectChanges();
      }
    });
  }

  static getElementRefOnCurrentAbstractControlOrOnParents(abstractControl: AbstractControl): ElementRef | undefined {
    const elementRef = abstractControl[FORM_CONTROL_ELEMENT_REF] as ElementRef;
    if (isNotNullNorUndefined(elementRef)) {
      return elementRef;
    }
    if (isNullOrUndefined(abstractControl.parent)) {
      return undefined;
    }
    return this.getElementRefOnCurrentAbstractControlOrOnParents(abstractControl.parent);
  }

  static notifySuccess(notifierService: NotifierService, textToTranslate: string, messageParam: SolidifyObject | undefined = undefined): boolean {
    if (isTruthyObject(notifierService)) {
      if (isNonEmptyString(textToTranslate)) {
        notifierService.showSuccess(textToTranslate, messageParam);
        return true;
      }
    }
    return false;
  }

  static notifyError(notifierService: NotifierService, textToTranslate: string, messageParam: SolidifyObject | undefined = undefined): boolean {
    if (isTruthyObject(notifierService)) {
      if (isNonEmptyString(textToTranslate)) {
        notifierService.showError(textToTranslate, messageParam);
        return true;
      }
    }
    return false;
  }

  static navigateIfDefined(ctx: SolidifyStateContext<any>, route: ExtendEnum<RoutesPartialEnum> | undefined | ((resId: string) => string), resId: string | undefined, replaceUrl: boolean = false): void {
    if (!isNullOrUndefined(route)) { // TODO MANAGE CASE DELETE WITHOUT RESID
      if (isFunction(route)) {
        route = route(resId);
      }
      ctx.dispatch(new Navigate([route], {}, {replaceUrl: replaceUrl}));
    }
  }

  static navigateCompositionIfDefined(ctx: SolidifyStateContext<any>, route: ExtendEnum<RoutesPartialEnum> | undefined | ((parentCompositionId: string, resId: string) => string), parentCompositionId: string | undefined, resId: string | undefined, replaceUrl: boolean = false): void {
    if (!isNullOrUndefined(route)) { // TODO MANAGE CASE DELETE WITHOUT RESID
      if (isFunction(route)) {
        route = route(parentCompositionId, resId);
      }
      ctx.dispatch(new Navigate([route], {}, {replaceUrl: replaceUrl}));
    }
  }

  static getStateNameFromInstance<TStateModel extends BaseStateModel = BaseStateModel>(store: BasicState<TStateModel>): string {
    return this.getStateNameFromClass(store.constructor as Type<BasicState<BaseStateModel>>);
  }

  static getStateNameFromClass<TStateModel extends BaseStateModel = BaseStateModel>(ctor: Type<BasicState<TStateModel>>): string {
    return this.getStateFromClass(ctor)[this._ATTRIBUTE_STATE_NAME];
  }

  static getStateFromInstance<T = any, TStateModel extends BaseStateModel = BaseStateModel>(store: BasicState<TStateModel>): T {
    return this.getStateFromClass(store.constructor as Type<BasicState<BaseStateModel>>);
  }

  static getStateFromClass<T = any, TStateModel extends BaseStateModel = BaseStateModel>(ctor: Type<BasicState<TStateModel>>): T {
    return ctor[this._NGXS_META_OPTIONS_KEY];
  }

  static isNextChunkAvailable(queryParameters: QueryParameters): boolean {
    let numberOfPages = queryParameters.paging.length / queryParameters.paging.pageSize;
    if (numberOfPages < 1) {
      numberOfPages = 1;
    }
    if (queryParameters.paging.pageIndex + 1 >= numberOfPages) {
      return false;
    }
    return true;
  }
}

type StateContextOnBaseResource<TResource extends BaseResource>
  = SolidifyStateContext<ResourceStateModel<TResource>> |
  SolidifyStateContext<CompositionStateModel<TResource>> |
  SolidifyStateContext<AssociationNoSqlReadOnlyStateModel<TResource>>;
