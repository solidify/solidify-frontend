/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - store.util.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {TestBed} from "@angular/core/testing";
import {
  Actions,
  NgxsModule,
  Store,
} from "@ngxs/store";
import {ɵensureStoreMetadata} from "@ngxs/store/internals";
import {combineLatest} from "rxjs";
import {RegisteredDefaultAction} from "../../decorators/store.decorator";
import {BaseAction} from "../../models/stores/base.action";
import {StoreActionClass} from "../../models/stores/state-action.model";
import {BaseState} from "../../stores/abstract/base/base.state";
import {ResourceNameSpace} from "../../stores/abstract/resource/resource-namespace.model";
import {StubActionBis} from "../../tests/stubs/stub-action";
import {StubEmptyClass} from "../../tests/stubs/stub-empty-class";
import {MemoizedUtil} from "./memoized.util";
import {
  SOLIDIFY_META_KEY,
  SolidifyMetadataUtil,
} from "./solidify-metadata.util";
import {ofSolidifyActionCompleted} from "./store.tool";
import {StoreUtil} from "./store.util";
import {UnitTestAction} from "./unit-test.action";
import {UnitTestState} from "./unit-test.state";

fdescribe("StoreUtil", () => {
  describe("initState", () => {
    it("should register 0 action when no action in solidify meta", () => {
      const baseConstructor = {};
      const childConstructor = {};

      const functionName = "GetAll";
      const action = new StubActionBis();
      action.type = "[MyState] Get All";

      const spy = spyOn(StoreUtil, "initDefaultAction");
      StoreUtil.initState(baseConstructor.constructor, childConstructor.constructor, action as StoreActionClass | any);
      expect(spy).toHaveBeenCalledTimes(0);
      baseConstructor.constructor[SOLIDIFY_META_KEY] = undefined;
      childConstructor.constructor[SOLIDIFY_META_KEY] = undefined;
    });

    it("should register 1 action when 1 action in solidify meta of base class", () => {
      const baseConstructor = {};
      const childConstructor = {};
      const functionName = "GetAll";
      const action = new StubActionBis();
      action.type = "[MyState] Get All";

      const baseMeta = SolidifyMetadataUtil.ensureStoreSolidifyMetadata<ResourceNameSpace>(baseConstructor.constructor as any);
      baseMeta.defaultActions = [{
        fn: functionName,
        callback: () => null,
        options: {},
      }];
      const spy = spyOn(StoreUtil, "initDefaultAction");
      StoreUtil.initState(baseConstructor.constructor, childConstructor.constructor, action as StoreActionClass | any);
      expect(spy).toHaveBeenCalledTimes(1);
      baseConstructor.constructor[SOLIDIFY_META_KEY] = undefined;
      childConstructor.constructor[SOLIDIFY_META_KEY] = undefined;
    });

    it("should register 2 action when 2 action in solidify meta of base class", () => {
      const baseConstructor = {};
      const childConstructor = {};
      const functionName = "GetAll";
      const action = new StubActionBis();
      action.type = "[MyState] Get All";

      const baseMeta = SolidifyMetadataUtil.ensureStoreSolidifyMetadata<ResourceNameSpace>(baseConstructor.constructor as any);
      baseMeta.defaultActions = [
        {
          fn: functionName + 1,
          callback: () => null,
          options: {},
        },
        {
          fn: functionName + 2,
          callback: () => null,
          options: {},
        },
      ];
      const spy = spyOn(StoreUtil, "initDefaultAction");
      StoreUtil.initState(baseConstructor.constructor, childConstructor.constructor, action as StoreActionClass | any);
      expect(spy).toHaveBeenCalledTimes(2);
      baseConstructor.constructor[SOLIDIFY_META_KEY] = undefined;
      childConstructor.constructor[SOLIDIFY_META_KEY] = undefined;
    });

    it("should register 1 action when 2 action in solidify meta of base class and 1 excluded in state class", () => {
      const baseConstructor = {};
      const childConstructor = {};
      const functionName = "GetAll";
      const action = new StubActionBis();
      action.type = "[MyState] Get All";

      const baseMeta = SolidifyMetadataUtil.ensureStoreSolidifyMetadata<ResourceNameSpace>(baseConstructor.constructor as any);
      baseMeta.defaultActions = [
        {
          fn: functionName + 1,
          callback: () => null,
          options: {},
        },
        {
          fn: functionName + 2,
          callback: () => null,
          options: {},
        },
      ];

      const meta = SolidifyMetadataUtil.ensureStoreSolidifyMetadata<ResourceNameSpace>(childConstructor.constructor as any);
      meta.excludedRegisteredDefaultActionFns = [
        functionName + 2,
      ];

      const spy = spyOn(StoreUtil, "initDefaultAction");
      StoreUtil.initState(baseConstructor.constructor, childConstructor.constructor, action as StoreActionClass | any);
      expect(spy).toHaveBeenCalledTimes(1);
      baseConstructor.constructor[SOLIDIFY_META_KEY] = undefined;
      childConstructor.constructor[SOLIDIFY_META_KEY] = undefined;
    });
  });

  describe("initDefaultAction", () => {
    it("should register action on NGXS metadata", () => {
      const constructor = {};
      const functionName = "GetAll";
      const action = new StubActionBis();
      action.type = "[MyState] Get All";
      const metaNgxs = ɵensureStoreMetadata(constructor.constructor as any);
      expect(metaNgxs).not.toBeNull();
      expect(metaNgxs).not.toBeUndefined();
      let actionsRegisteredInNgXs = Object.getOwnPropertyNames(metaNgxs.actions);
      expect(actionsRegisteredInNgXs.length).toBe(0);

      StoreUtil.initDefaultAction(constructor.constructor, functionName, action as StoreActionClass | any);

      actionsRegisteredInNgXs = Object.getOwnPropertyNames(metaNgxs.actions);
      expect(actionsRegisteredInNgXs.length).toBe(1);
      expect(actionsRegisteredInNgXs[0]).toBe(action.type);
      const actionInNgxs: RegisteredDefaultAction<ResourceNameSpace> | any = metaNgxs.actions[actionsRegisteredInNgXs[0]];
      expect(actionInNgxs.length).toBe(1);
      expect(actionInNgxs[0].fn).toBe(functionName);
      expect(Object.getOwnPropertyNames(actionInNgxs[0].options).length).toBe(0);
      expect(actionInNgxs[0].type).toBe(action.type);

      constructor.constructor[SOLIDIFY_META_KEY] = undefined;
    });
  });

  describe("hasParentAction", () => {
    it("should return false when it is not parent", () => {
      const parentAction = {} as BaseAction;
      const action = {} as BaseAction;
      expect(StoreUtil.hasParentAction(action, parentAction)).toBeFalsy();
    });

    it("should return true when it is parent directly", () => {
      const parentAction = {} as BaseAction;
      const action = {parentAction: parentAction} as BaseAction;
      expect(StoreUtil.hasParentAction(action, parentAction)).toBeTruthy();
    });

    it("should return true when it is parent indirectly directly", () => {
      const parentAction = {} as BaseAction;
      const intermediateAction = {parentAction: parentAction} as BaseAction;
      const action = {parentAction: intermediateAction} as BaseAction;
      expect(StoreUtil.hasParentAction(action, parentAction)).toBeTruthy();
    });
  });

  describe("isLoadingState", () => {
    it("should return true when isLoadingCounter > 0", () => {
      expect(StoreUtil.isLoadingState({isLoadingCounter: 1})).toBeTruthy();
    });

    it("should return true when isLoadingCounter = 0", () => {
      expect(StoreUtil.isLoadingState({isLoadingCounter: 0})).toBeFalsy();
    });

    it("should return true when isLoadingCounter = -1", () => {
      expect(StoreUtil.isLoadingState({isLoadingCounter: -1})).toBeFalsy();
    });
  });

  describe("determineSubResourceChange", () => {
    it("should return (0 add / 0 remove) when list empty", () => {
      const oldList = [];
      const newList = [];
      const result = StoreUtil.determineSubResourceChange(oldList, newList);
      expect(result.resourceToRemoved.length).toBe(0);
      expect(result.resourceToAdd.length).toBe(0);
    });

    it("should return (1 add / 0 remove) when list (1 new / 0 old)", () => {
      const oldList = [];
      const newList = ["1"];
      const result = StoreUtil.determineSubResourceChange(oldList, newList);
      expect(result.resourceToRemoved.length).toBe(0);
      expect(result.resourceToAdd.length).toBe(1);
    });

    it("should return (0 add / 1 remove) when list (0 new / 1 old)", () => {
      const oldList = ["1"];
      const newList = [];
      const result = StoreUtil.determineSubResourceChange(oldList, newList);
      expect(result.resourceToRemoved.length).toBe(1);
      expect(result.resourceToAdd.length).toBe(0);
    });

    it("should return (0 add / 0 remove) when list (1 new / 1 old) same", () => {
      const oldList = ["1"];
      const newList = ["1"];
      const result = StoreUtil.determineSubResourceChange(oldList, newList);
      expect(result.resourceToRemoved.length).toBe(0);
      expect(result.resourceToAdd.length).toBe(0);
    });

    it("should return (1 add / 1 remove) when list (2 new / 2 old) with one element same", () => {
      const oldList = ["1", "2"];
      const newList = ["1", "3"];
      const result = StoreUtil.determineSubResourceChange(oldList, newList);
      expect(result.resourceToRemoved.length).toBe(1);
      expect(result.resourceToAdd.length).toBe(1);
    });

    it("should return (1 add / 1 remove) when list (1 new / 1 old) with different element", () => {
      const oldList = ["2"];
      const newList = ["3"];
      const result = StoreUtil.determineSubResourceChange(oldList, newList);
      expect(result.resourceToRemoved.length).toBe(1);
      expect(result.resourceToAdd.length).toBe(1);
    });
  });

  describe("getState", () => {
    it("should return name of the state", () => {
      const state = new StubEmptyClass();
      state.constructor["NGXS_OPTIONS_META"] = {name: "stateName"};
      expect(StoreUtil.getStateNameFromInstance(state as BaseState<any>)).toBe("stateName");
      state.constructor["NGXS_OPTIONS_META"] = undefined;
    });
  });

  describe("NGXS", () => {
    let store: Store;
    let actions$: Actions;
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    const getNumberSuccess = () => MemoizedUtil.selectSnapshot(store, UnitTestState, state => state.numberSuccess);
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    const getNumberFail = () => MemoizedUtil.selectSnapshot(store, UnitTestState, state => state.numberFail);
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    const getNumberPending = () => MemoizedUtil.selectSnapshot(store, UnitTestState, state => state.isLoadingCounter);
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    const getIsLoading = () => MemoizedUtil.isLoadingSnapshot(store, UnitTestState);

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [NgxsModule.forRoot([UnitTestState])],
      });

      store = TestBed.inject(Store);
      actions$ = TestBed.inject(Actions);
    });

    describe("basic", () => {
      it("should be success result", (done: DoneFn) => {
        store.dispatch(new UnitTestAction.Test(true)).subscribe(() => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberSuccess()).toBe(1);
          done();
        });
      });

      it("should be fail result", (done: DoneFn) => {
        store.dispatch(new UnitTestAction.Test(false)).subscribe(() => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberFail()).toBe(1);
          done();
        });
      });
    });

    describe("dispatchActionAndWaitForSubActionCompletion", () => {
      it("should call success callback", (done: DoneFn) => {
        StoreUtil.dispatchActionAndWaitForSubActionCompletion(store, actions$,
          new UnitTestAction.Test(true),
          UnitTestAction.TestSuccess,
          result => {
            expect(getIsLoading()).toBe(false);
            expect(getNumberSuccess()).toBe(1);
            done();
          }).subscribe();
      });

      it("should call fail callback", (done: DoneFn) => {
        StoreUtil.dispatchActionAndWaitForSubActionCompletion(store, actions$,
          new UnitTestAction.Test(false),
          UnitTestAction.TestSuccess,
          result => {
          },
          UnitTestAction.TestFail,
          result => {
            expect(getIsLoading()).toBe(false);
            expect(getNumberFail()).toBe(1);
            done();
          }).subscribe();
      });
    });

    describe("dispatchParallelActionAndWaitForSubActionsCompletion", () => {
      it("should 1 action finish to success", (done: DoneFn) => {
        StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
        ]).subscribe(result => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberSuccess()).toBe(1);
          expect(result.listActionSuccess.length).toBe(1);
          expect(result.listActionFail.length).toBe(0);
          expect(result.listActionInError.length).toBe(0);
          expect(result.listActionWithoutSubAction.length).toBe(0);
          expect(result.listActionGoalUndefined.length).toBe(0);
          expect(result.success).toBe(true);
          expect(result.error).toBe(undefined);
          done();
        });
      });

      it("should 1 action finish to fail", (done: DoneFn) => {
        StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(false),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
        ]).subscribe(result => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberFail()).toBe(1);
          expect(result.listActionSuccess.length).toBe(0);
          expect(result.listActionFail.length).toBe(1);
          expect(result.listActionInError.length).toBe(0);
          expect(result.listActionWithoutSubAction.length).toBe(0);
          expect(result.listActionGoalUndefined.length).toBe(0);
          expect(result.success).toBe(false);
          expect(result.error).toBe(undefined);
          done();
        });
      });

      it("should 2 action finish success", (done: DoneFn) => {
        StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
          {
            action: new UnitTestAction.Test(true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
        ]).subscribe(result => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberFail()).toBe(0);
          expect(getNumberSuccess()).toBe(2);
          expect(result.listActionSuccess.length).toBe(2);
          expect(result.listActionFail.length).toBe(0);
          expect(result.listActionInError.length).toBe(0);
          expect(result.listActionWithoutSubAction.length).toBe(0);
          expect(result.listActionGoalUndefined.length).toBe(0);
          expect(result.success).toBe(true);
          expect(result.error).toBe(undefined);
          done();
        });
      });

      it("should 2 action finish partially", (done: DoneFn) => {
        StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(false),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
          {
            action: new UnitTestAction.Test(true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
        ]).subscribe(result => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberFail()).toBe(1);
          expect(getNumberSuccess()).toBe(1);
          expect(result.listActionSuccess.length).toBe(1);
          expect(result.listActionFail.length).toBe(1);
          expect(result.listActionInError.length).toBe(0);
          expect(result.listActionWithoutSubAction.length).toBe(0);
          expect(result.listActionGoalUndefined.length).toBe(0);
          expect(result.success).toBe(false);
          expect(result.error).toBe(undefined);
          done();
        });
      });

      it("should 2 action finish partially when error", (done: DoneFn) => {
        StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(false, true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
          {
            action: new UnitTestAction.Test(true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
        ]).subscribe(result => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberFail()).toBe(1);
          expect(getNumberSuccess()).toBe(1);
          expect(result.listActionSuccess.length).toBe(1);
          expect(result.listActionFail.length).toBe(1);
          expect(result.listActionInError.length).toBe(0);
          expect(result.listActionWithoutSubAction.length).toBe(0);
          expect(result.listActionGoalUndefined.length).toBe(0);
          expect(result.success).toBe(false);
          expect(result.error).toBe(undefined);
          done();
        });
      });

      it("should 1 action finish in error", (done: DoneFn) => {
        const actionCompleted = actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail));
        const parallel = StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(false, true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
        ], 1000);
        combineLatest(parallel, actionCompleted).subscribe(([parallelResult, actionCompletedResult]) => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberFail()).toBe(1);
          expect(parallelResult.success).toBe(false);
          expect(parallelResult.error).toBe(undefined);
          done();
        });
      });

      it("should 1 action finish to without sub action", (done: DoneFn) => {
        const actionCompleted = actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess));
        const parallel = StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(true),
          },
        ]);
        combineLatest(parallel, actionCompleted).subscribe(([parallelResult, actionCompletedResult]) => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberSuccess()).toBe(1);
          expect(parallelResult.listActionSuccess.length).toBe(0);
          expect(parallelResult.listActionFail.length).toBe(0);
          expect(parallelResult.listActionInError.length).toBe(0);
          expect(parallelResult.listActionWithoutSubAction.length).toBe(1);
          expect(parallelResult.listActionGoalUndefined.length).toBe(0);
          expect(parallelResult.success).toBe(true);
          expect(parallelResult.error).toBe(undefined);
          done();
        });
      });

      it("should 1 action finish to goal undefined", (done: DoneFn) => {
        const actionCompleted = actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail));
        const parallel = StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(false),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
            ],
          },
        ], 1000);
        combineLatest(parallel, actionCompleted).subscribe(([parallelResult, actionCompletedResult]) => {
          // TODO MANAGE CORRECLTY IMPLEMENTAITON FOR THIS CASE TO HAVE REPORT WHEN TIMEOUT DUE TO UNREACH SUB ACTION
          expect(getIsLoading()).toBe(false);
          expect(getNumberFail()).toBe(1);
          expect(parallelResult.success).toBe(false);
          expect(parallelResult.error).not.toBe(undefined);
          done();
        });
      });
    });

    describe("dispatchSequentialActionAndWaitForSubActionsCompletion", () => {
      it("should 1 action finish to success", (done: DoneFn) => {
        StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
        ]).subscribe(result => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberSuccess()).toBe(1);
          expect(result.listActionSuccess.length).toBe(1);
          expect(result.listActionFail.length).toBe(0);
          expect(result.listActionInError.length).toBe(0);
          expect(result.listActionWithoutSubAction.length).toBe(0);
          expect(result.listActionGoalUndefined.length).toBe(0);
          expect(result.success).toBe(true);
          expect(result.error).toBe(undefined);
          done();
        });
      });

      it("should 1 action finish to fail", (done: DoneFn) => {
        StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(false),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
        ]).subscribe(result => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberFail()).toBe(1);
          expect(result.listActionSuccess.length).toBe(0);
          expect(result.listActionFail.length).toBe(1);
          expect(result.listActionInError.length).toBe(0);
          expect(result.listActionWithoutSubAction.length).toBe(0);
          expect(result.listActionGoalUndefined.length).toBe(0);
          expect(result.success).toBe(false);
          expect(result.error).toBe(undefined);
          done();
        });
      });

      it("should 2 action finish success", (done: DoneFn) => {
        StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
          {
            action: new UnitTestAction.Test(true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
        ]).subscribe(result => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberFail()).toBe(0);
          expect(getNumberSuccess()).toBe(2);
          expect(result.listActionSuccess.length).toBe(2);
          expect(result.listActionFail.length).toBe(0);
          expect(result.listActionInError.length).toBe(0);
          expect(result.listActionWithoutSubAction.length).toBe(0);
          expect(result.listActionGoalUndefined.length).toBe(0);
          expect(result.success).toBe(true);
          expect(result.error).toBe(undefined);
          done();
        });
      });

      it("should 2 action finish partially", (done: DoneFn) => {
        StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(false),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
          {
            action: new UnitTestAction.Test(true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
        ]).subscribe(result => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberFail()).toBe(1);
          expect(getNumberSuccess()).toBe(1);
          expect(result.listActionSuccess.length).toBe(1);
          expect(result.listActionFail.length).toBe(1);
          expect(result.listActionInError.length).toBe(0);
          expect(result.listActionWithoutSubAction.length).toBe(0);
          expect(result.listActionGoalUndefined.length).toBe(0);
          expect(result.success).toBe(false);
          expect(result.error).toBe(undefined);
          done();
        });
      });

      it("should 2 action finish partially when fail fast", (done: DoneFn) => {
        StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(false),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
          {
            action: new UnitTestAction.Test(true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
        ], StoreUtil.BULK_DISPATCH_TIMEOUT_FOR_NON_DISPATCHED_SUB_ACTION, true).subscribe(result => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberFail()).toBe(1);
          expect(getNumberSuccess()).toBe(1);
          expect(result.listActionSuccess.length).toBe(1);
          expect(result.listActionFail.length).toBe(1);
          expect(result.listActionInError.length).toBe(0);
          expect(result.listActionWithoutSubAction.length).toBe(0);
          expect(result.listActionGoalUndefined.length).toBe(0);
          expect(result.success).toBe(false);
          expect(result.error).toBe(undefined);
          done();
        });
      });

      it("should 2 action finish partially when error", (done: DoneFn) => {
        StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(false, true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
          {
            action: new UnitTestAction.Test(true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
        ]).subscribe(result => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberFail()).toBe(1);
          expect(getNumberSuccess()).toBe(1);
          expect(result.listActionSuccess.length).toBe(1);
          expect(result.listActionFail.length).toBe(1);
          expect(result.listActionInError.length).toBe(0);
          expect(result.listActionWithoutSubAction.length).toBe(0);
          expect(result.listActionGoalUndefined.length).toBe(0);
          expect(result.success).toBe(false);
          expect(result.error).toBe(undefined);
          done();
        });
      });

      it("should 2 action finish partially when error in fail fast", (done: DoneFn) => {
        StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(false, true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
          {
            action: new UnitTestAction.Test(true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
        ], StoreUtil.BULK_DISPATCH_TIMEOUT_FOR_NON_DISPATCHED_SUB_ACTION, true).subscribe(result => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberFail()).toBe(1);
          expect(getNumberSuccess()).toBe(0);
          expect(result.success).toBe(false);
          expect(result.error).not.toBe(undefined);
          done();
        });
      });

      it("should 1 action finish in error", (done: DoneFn) => {
        const actionCompleted = actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail));
        const parallel = StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(false, true),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail)),
            ],
          },
        ], 1000);
        combineLatest(parallel, actionCompleted).subscribe(([parallelResult, actionCompletedResult]) => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberFail()).toBe(1);
          expect(parallelResult.success).toBe(false);
          expect(parallelResult.error).toBe(undefined);
          done();
        });
      });

      it("should 1 action finish to without sub action", (done: DoneFn) => {
        const actionCompleted = actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess));
        const parallel = StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(true),
          },
        ]);
        combineLatest(parallel, actionCompleted).subscribe(([parallelResult, actionCompletedResult]) => {
          expect(getIsLoading()).toBe(false);
          expect(getNumberSuccess()).toBe(1);
          expect(parallelResult.listActionSuccess.length).toBe(0);
          expect(parallelResult.listActionFail.length).toBe(0);
          expect(parallelResult.listActionInError.length).toBe(0);
          expect(parallelResult.listActionWithoutSubAction.length).toBe(1);
          expect(parallelResult.listActionGoalUndefined.length).toBe(0);
          expect(parallelResult.success).toBe(true);
          expect(parallelResult.error).toBe(undefined);
          done();
        });
      });

      it("should 1 action finish to goal undefined", (done: DoneFn) => {
        const actionCompleted = actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestFail));
        const parallel = StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(store, [
          {
            action: new UnitTestAction.Test(false),
            subActionCompletions: [
              actions$.pipe(ofSolidifyActionCompleted(UnitTestAction.TestSuccess)),
            ],
          },
        ], 1000);
        combineLatest(parallel, actionCompleted).subscribe(([parallelResult, actionCompletedResult]) => {
          // TODO MANAGE CORRECLTY IMPLEMENTAITON FOR THIS CASE TO HAVE REPORT WHEN TIMEOUT DUE TO UNREACH SUB ACTION
          expect(getIsLoading()).toBe(false);
          expect(getNumberFail()).toBe(1);
          expect(parallelResult.success).toBe(false);
          expect(parallelResult.error).not.toBe(undefined);
          done();
        });
      });
    });
  });
});
