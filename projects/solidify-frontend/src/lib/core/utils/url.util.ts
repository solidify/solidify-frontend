/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - url.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {isNullOrUndefinedOrWhiteString} from "../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {UrlQueryParamHelper} from "../helpers/url-query-param.helper";
import {StateModel} from "../models/stores/state.model";

export class UrlUtil {
  private static readonly _PROTOCOL_SEPARATOR: string = "://";
  private static readonly _PARAMETERS_START: string = "?";
  private static readonly _PARAMETERS_SEPARATOR: string = "&";
  private static readonly _PARAMETERS_EQUAL: string = "=";

  /***
   * Add url separator at end of url if missing
   * @param url plain format url (ex: "https://unige.ch/test/url")
   * return the url wit separator at the end if missing (ex: "https://unige.ch/test/url/")
   */
  static addUrlSeparatorAtEndIfMissing(url: string): string {
    if (isNullOrUndefinedOrWhiteString(url)) {
      return "";
    }
    if (url.endsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR)) {
      return url;
    }
    return url + SOLIDIFY_CONSTANTS.URL_SEPARATOR;
  }

  /***
   * Remove url separator at end of url if present
   * @param url plain format url (ex: "https://unige.ch/test/url/")
   * return the url without separator at the end if present (ex: "https://unige.ch/test/url")
   */
  static removeUrlSeparatorAtEndIfPresent(url: string): string {
    if (isNullOrUndefinedOrWhiteString(url)) {
      return "";
    }
    if (!url.endsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR)) {
      return url;
    }
    return this.removeUrlSeparatorAtEndIfPresent(url.substring(0, url.length - 1));
  }

  /***
   * Remove parameters from an url
   * @param url plain format url (ex: "https://unige.ch/test/url?param=true")
   * return the url without query param (ex: "https://unige.ch/test/url")
   */
  static removeParametersFromUrl(url: string): string {
    if (isNullOrUndefinedOrWhiteString(url)) {
      return "";
    }
    const indexOfParam = url.indexOf(this._PARAMETERS_START);
    if (indexOfParam !== -1) {
      url = url.substring(0, indexOfParam);
    }
    return url;
  }

  /***
   * Get the path used in an url
   * @param url plain format url (ex: "https://unige.ch/test/url")
   * return the path used (ex: "test/url")
   */
  static getPath(url: string): string {
    if (isNullOrUndefinedOrWhiteString(url)) {
      return "";
    }
    const urlWithoutProtocol = this._removeProtocol(url);
    const urlWithoutProtocolAmdParameters = this.removeParametersFromUrl(urlWithoutProtocol);
    const indexOfFirstSlash = urlWithoutProtocolAmdParameters.indexOf(SOLIDIFY_CONSTANTS.URL_SEPARATOR);
    if (indexOfFirstSlash < 0) {
      return "";
    }
    return urlWithoutProtocolAmdParameters.substring(indexOfFirstSlash + 1, url.length);
  }

  /***
   * Get the domain used in an url
   * @param url plain format url (ex: "https://unige.ch/test/url")
   * return the domain used (ex: "unige.ch")
   */
  static getDomain(url: string): string {
    if (isNullOrUndefinedOrWhiteString(url)) {
      return "";
    }
    const urlWithoutProtocol = this._removeProtocol(url);
    const indexOfFirstSlash = urlWithoutProtocol.indexOf(SOLIDIFY_CONSTANTS.URL_SEPARATOR);
    if (indexOfFirstSlash < 0) {
      return urlWithoutProtocol;
    }
    return urlWithoutProtocol.substring(0, indexOfFirstSlash);
  }

  /***
   * Get the origin in an url
   * @param url plain format url (ex: "https://unige.ch/test/url")
   * return the domain used (ex: "https://unige.ch")
   */
  static getOrigin(url: string): string {
    if (isNullOrUndefinedOrWhiteString(url)) {
      return "";
    }
    const protocol = this.getProtocol(url);
    const domain = this.getDomain(url);
    if (isNullOrUndefinedOrWhiteString(protocol)) {
      return domain;
    }
    return protocol + this._PROTOCOL_SEPARATOR + domain;
  }

  /***
   * Get the protocol used in an url
   * @param url plain format url (ex: "https://unige.ch/test-url")
   * return the protocol used (ex: "http" or "https")
   */
  static getProtocol(url: string): string {
    if (isNullOrUndefinedOrWhiteString(url)) {
      return "";
    }
    const indexOfProtocol = url.indexOf(this._PROTOCOL_SEPARATOR);
    if (indexOfProtocol < 0) {
      return "";
    }
    return url.substring(0, indexOfProtocol);
  }

  /***
   * Convert navigate action to url
   * @param navigate NGXS Navigate action
   * @param environment app environment
   * return url
   */
  static convertNavigateActionToUrl(navigate: Navigate, environment: DefaultSolidifyEnvironment): string {
    if (SsrUtil.window) {
      return SsrUtil.window.location.origin + this.convertNavigateActionToPath(navigate, environment);
    }
    return "";
  }

  /***
   * Convert navigate action to url
   * @param navigate NGXS Navigate action
   * @param environment app environment
   * return url
   */
  static convertNavigateActionToPath(navigate: Navigate, environment: DefaultSolidifyEnvironment): string {
    return environment.baseHref + navigate.path.join(SOLIDIFY_CONSTANTS.URL_SEPARATOR) + UrlQueryParamHelper.getQueryParamStringFromQueryParamMappingObject(navigate.queryParams, true);
  }

  private static _removeProtocol(url: string): string {
    const indexOfProtocol = url.indexOf(this._PROTOCOL_SEPARATOR);
    let urlWithoutProtocol = url;
    if (indexOfProtocol >= 0) {
      urlWithoutProtocol = url.substring(indexOfProtocol + this._PROTOCOL_SEPARATOR.length, url.length);
    }
    return urlWithoutProtocol;
  }

  /**
   * Return url form Store Router state
   * @param store
   * return for exemple "/admin/license/xxxxx"
   */
  static getCurrentUrlFromStoreRouterState(store: Store): string {
    return store.selectSnapshot((s: StateModel) => s.router.state.url);
  }
}
