/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - error.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpErrorResponse,
  HttpStatusCode,
} from "@angular/common/http";
import {
  Inject,
  Injectable,
} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
} from "../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {LabelTranslateInterface} from "../../label-translate-interface.model";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {ErrorHelper} from "../helpers/error.helper";
import {ENVIRONMENT} from "../injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../injection-tokens/label-to-translate.injection-token";
import {AbstractBaseService} from "./abstract-base.service";

@Injectable({
  providedIn: "root",
})
export class ErrorService extends AbstractBaseService {
  constructor(private readonly _translate: TranslateService,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface) {
    super();
  }

  getClientMessage(error: Error): string {
    if (!SsrUtil.window?.navigator.onLine) {
      return this._translate.instant(this.labelTranslate.coreNotificationHttpOfflineToTranslate);
    }
    const errorMessage = error.message ? error.message : error.toString();
    const errorName = error.name ? error.name : error.toString();
    if (SsrUtil.isServer) {
      const errorStack = error.stack ? error.stack : error.toString();
      // eslint-disable-next-line no-console
      console.error("Client Error " + errorName, errorMessage, errorStack);
    }
    return errorName;
  }

  getClientSubMessage(error: Error): string {
    if (!SsrUtil.window?.navigator.onLine) {
      return this._translate.instant(this.labelTranslate.coreNotificationHttpOfflineToTranslate);
    }
    return error.message ? error.message : error.toString();
  }

  getClientStack(error: Error): string {
    return error.stack;
  }

  getServerMessage(error: HttpErrorResponse): string {
    if (error.status === HttpStatusCode.Forbidden) {
      return this._translate.instant(this.labelTranslate.coreNotificationHttpForbiddenToTranslate);
    }
    if (error.status === HttpStatusCode.Unauthorized) {
      if (isNullOrUndefined(error.error) || error.error.error === ErrorHelper.INVALID_TOKEN_ERROR) {
        return this._translate.instant(this.labelTranslate.coreNotificationHttpInvalidTokenToTranslate);
      } else {
        return this._translate.instant(this.labelTranslate.coreNotificationHttpUnauthorizedToTranslate);
      }
    }
    if (error.status === HttpStatusCode.BadRequest) {
      return this._translate.instant(this.labelTranslate.coreNotificationHttpBadRequestToTranslate);
    }
    if (error.status === HttpStatusCode.NotFound) {
      return this._translate.instant(this.labelTranslate.coreNotificationHttpNotFoundToTranslate);
    }
    if (error.status === HttpStatusCode.InternalServerError) {
      return this._translate.instant(this.labelTranslate.coreNotificationHttpInternalErrorToTranslate);
    }
    return error.message;
  }

  getServerSubMessage(error: HttpErrorResponse): string {
    if (error.status === HttpStatusCode.BadRequest) {
      const listValidationErrors = ErrorHelper.extractValidationErrorsNotAlreadyDisplayedFromError(error);
      const validationErrorsMessage = ErrorHelper.mergeValidationErrorsMessages(listValidationErrors);
      if (isNotNullNorUndefinedNorWhiteString(validationErrorsMessage)) {
        return validationErrorsMessage;
      }
    }
    return error.error?.message;
  }

  getServerStack(error: HttpErrorResponse): string {
    return error.message;
  }
}
