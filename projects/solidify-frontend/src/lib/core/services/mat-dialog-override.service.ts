/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - mat-dialog-override.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ComponentType,
  Overlay,
  OverlayContainer,
} from "@angular/cdk/overlay";
import {Location} from "@angular/common";
import {
  ElementRef,
  Inject,
  Injectable,
  Injector,
  NgZone,
  OnDestroy,
  Optional,
  SkipSelf,
  TemplateRef,
} from "@angular/core";
import {
  MAT_DIALOG_DEFAULT_OPTIONS,
  MAT_DIALOG_SCROLL_STRATEGY,
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from "@angular/material/dialog";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {SubscriptionManager} from "../models/managers/subscription-manager.model";
import {TabulationService} from "./tabulation.service";

@Injectable()
export class MatDialogOverrideService extends MatDialog implements OnDestroy {

  private readonly _subscriptionManager: SubscriptionManager = new SubscriptionManager();
  lasActiveElement: Element;

  constructor(_overlay: Overlay,
              _injector: Injector,
              @Optional() _location: Location,
              private readonly _ngZone: NgZone,
              @Optional() @Inject(MAT_DIALOG_DEFAULT_OPTIONS) _defaultOptions: MatDialogConfig,
              @Inject(MAT_DIALOG_SCROLL_STRATEGY) scrollStrategy: any,
              @Optional() @SkipSelf() _parentDialog: MatDialog,
              _overlayContainer: OverlayContainer,
              @Optional() private readonly _elementRef: ElementRef,
              private readonly _tabulationService: TabulationService) {
    super(_overlay,
      _injector,
      _location,
      _defaultOptions,
      scrollStrategy,
      _parentDialog,
      _overlayContainer);
    this._ngZone.runOutsideAngular(() => SsrUtil.window?.document.addEventListener("focusin", focusEvent => this.lasActiveElement = SsrUtil.window?.document.activeElement));
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._subscriptionManager.clearAndUnsubscribeAll();
  }

  open<T, D = any, R = any>(componentOrTemplateRef: ComponentType<T> | TemplateRef<T>, config?: MatDialogConfig<D>): MatDialogRef<T, R> {
    const lastActiveElement: Element = this.lasActiveElement;
    const matDialogRef = super.open(componentOrTemplateRef, config);
    this._subscriptionManager.subscribe(
      matDialogRef.afterClosed(),
      () => {
        this._tabulationService.focusPrev(lastActiveElement);
      },
    );
    return matDialogRef;
  }
}
