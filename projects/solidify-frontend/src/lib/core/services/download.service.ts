/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - download.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpClient,
  HttpHeaders,
  HttpStatusCode,
} from "@angular/common/http";
import {
  Inject,
  Injectable,
} from "@angular/core";
import {saveAs} from "file-saver";
import {Observable} from "rxjs";
import {
  catchError,
  take,
  tap,
} from "rxjs/operators";
import {
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
} from "../../core-resources/tools/is/is.tool";
import {MimetypeUtil} from "../../core-resources/utils/mimetype.util";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {LabelTranslateInterface} from "../../label-translate-interface.model";
import {OauthHelper} from "../../oauth2/oauth.helper";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {CookieType} from "../cookie-consent/enums/cookie-type.enum";
import {CookieConsentService} from "../cookie-consent/services/cookie-consent.service";
import {CookieConsentUtil} from "../cookie-consent/utils/cookie-consent.util";
import {ApiActionNamePartialEnum} from "../enums/partial/api-action-name-partial.enum";
import {CookiePartialEnum} from "../enums/partial/cookie-partial.enum";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {SolidifyError} from "../errors/solidify.error";
import {ApiService} from "../http/api.service";
import {ENVIRONMENT} from "../injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../injection-tokens/label-to-translate.injection-token";
import {DownloadToken} from "../models/download-token.model";
import {SolidifyHttpErrorResponseModel} from "../models/errors/solidify-http-error-response.model";
import {UrlUtil} from "../utils/url.util";
import {AbstractBaseService} from "./abstract-base.service";
import {StringUtil} from "../../core-resources/utils/string.util";
import {NotificationService} from "./notification.service";

@Injectable({
  providedIn: "root",
})
export class DownloadService extends AbstractBaseService {
  constructor(private readonly _httpClient: HttpClient,
              private readonly _apiService: ApiService,
              private readonly _notificationService: NotificationService,
              private readonly _cookieConsentService: CookieConsentService,
              @Inject(LABEL_TRANSLATE) private readonly _labelTranslate: LabelTranslateInterface,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super();
  }

  /**
   * Download using download token method if useDownloadToken is set to true. Otherwise download depending of isPublic attribute
   *
   * @param url
   * @param fileName
   * @param size
   * @param isPublic if true use download browser native. If false download in memory using token infos. Ignored if useDownloadToken is true
   */
  download(url: string, fileName: string, size?: number, isPublic: boolean = true, mimetype?: string): void {
    this._notificationService.showInformation(this._labelTranslate.coreFileDownloadStart);
    if (isTrue(this._environment.useDownloadToken)) {
      if (CookieConsentUtil.isFeatureAuthorized(CookieType.cookie, CookiePartialEnum.downloadToken)) {
        this._getAndStoreDownloadTokenThenDownload(url, fileName, mimetype);
      } else {
        this._cookieConsentService.notifyFeatureDisabledBecauseCookieDeclined(CookieType.cookie, CookiePartialEnum.downloadToken);
        return;
      }
    } else {
      if (isPublic) {
        this.downloadBrowserNative(url, fileName, mimetype);
      } else {
        this.subscribe(this.downloadInMemory(url, fileName, true, mimetype));
      }
    }
  }

  downloadBrowserNative(url: string, fileName?: string, mimetype?: string): void {
    const link = SsrUtil.document.createElement("a");
    link.href = url;
    if (isNotNullNorUndefinedNorWhiteString(fileName) && isNotNullNorUndefinedNorWhiteString(mimetype)) {
      fileName = this.addExtensionFromMimetypeInFileName(fileName, mimetype);
      link.download = fileName;
    } else {
      link.download = "";
      link.target = "_blank";
    }
    link.click();
  }

  private _getAndStoreDownloadTokenThenDownload(url: string, fileName?: string, mimetype?: string): void {
    const downloadTokenUrl = this._generateDownloadTokenUrl(url);
    this.subscribe(this.getAndStoreDownloadToken(downloadTokenUrl, fileName, mimetype).pipe(
      tap(result => {
        this.downloadBrowserNative(url, fileName, mimetype);
      }),
    ));
  }

  getAndStoreDownloadToken(downloadTokenUrl: string, fileName?: string, mimetype?: string): Observable<DownloadToken> {
    return this._httpClient.get<DownloadToken>(downloadTokenUrl, {
      withCredentials: true,
    }).pipe(
      take(1),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        if (error.status !== HttpStatusCode.Unauthorized || error.error?.error !== OauthHelper.MFA_NEEDED) {
          this._notificationService.showError(this._labelTranslate.coreFileDownloadForbidden);
        }
        throw error;
      }),
    );
  }

  private _generateDownloadTokenUrl(url: string): string {
    const endOfUrlExpected = SOLIDIFY_CONSTANTS.URL_SEPARATOR + ApiActionNamePartialEnum.DOWNLOAD;
    url = UrlUtil.removeParametersFromUrl(url);
    if (url.endsWith(endOfUrlExpected)) {
      return url.substring(0, url.length - endOfUrlExpected.length) + SOLIDIFY_CONSTANTS.URL_SEPARATOR + ApiActionNamePartialEnum.DOWNLOAD_TOKEN;
    }
    throw new SolidifyError(`Incompatible download url format for working with download token system. Disable useDownloadToken variable or change the format of download url to end by "${SOLIDIFY_CONSTANTS.URL_SEPARATOR + ApiActionNamePartialEnum.DOWNLOAD}"`);
  }

  // Prefer use download api to stream directly on disk
  downloadInMemory(url: string, fileName: string, withSaveAs: boolean = true, mimetype?: string, isPost?: boolean, postBody?: any): Observable<Blob> {
    let headers = new HttpHeaders();
    fileName = this.addExtensionFromMimetypeInFileName(fileName, mimetype);
    fileName = StringUtil.replaceNonIso8859_1Chars(fileName); // headers support only ISO-8859-1 chars
    headers = headers.set("Content-Disposition", "attachment; filename=" + fileName);
    if (withSaveAs) {
      this._notificationService.showInformation(this._labelTranslate.coreFileDownloadStart);
    }
    const options = {
      headers,
      responseType: "blob",
    };
    let obs;
    if (isPost) {
      obs = this._httpClient.post(url, postBody, options as any);
    } else {
      obs = this._httpClient.get(url, options as any);
    }
    return obs.pipe(
      tap((blobContent: Blob) => {
        if (withSaveAs) {
          this._notificationService.showSuccess(this._labelTranslate.coreFileDownloadDownloadedSuccessfully);
          saveAs(blobContent, fileName);
        }
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        if (withSaveAs) {
          this._notificationService.showError(this._labelTranslate.coreFileDownloadFail);
          if (error.status === HttpStatusCode.Forbidden) {
            this._notificationService.showError(this._labelTranslate.coreFileDownloadForbidden);
          } else {
            this._notificationService.showError(this._labelTranslate.coreFileDownloadFail);
          }
        }
        throw error;
      }),
    );
  }

  addExtensionFromMimetypeInFileName(fileName: string, mimetype: string): string {
    if (isNullOrUndefinedOrWhiteString(mimetype)) {
      return fileName;
    }
    if (isNullOrUndefined(this._environment.fileExtensionToCompleteWithRealExtensionOnDownload) ||
      this._environment.fileExtensionToCompleteWithRealExtensionOnDownload.findIndex(e => fileName.endsWith("." + e)) < 0) {
      return fileName;
    }
    const extension = MimetypeUtil.getFirstMatchingExtension(mimetype);
    return isNullOrUndefined(extension) ? fileName : `${fileName}.${extension}`;
  }
}
