/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-store-dialog.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Inject} from "@angular/core";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "../../core-resources/tools/is/is.tool";
import {StringUtil} from "../../core-resources/utils/string.util";
import {adminGlobalBannerActionNameSpace} from "../../global-banner/admin/stores/admin-global-banner.action";
import {LabelTranslateInterface} from "../../label-translate-interface.model";
import {adminOaiMetadataPrefixActionNameSpace} from "../../oai-pmh/oai-metadata-prefix/stores/admin-oai-metadata-prefix.action";
import {adminOaiSetActionNameSpace} from "../../oai-pmh/oai-set/stores/admin-oai-set.action";
import {adminIndexFieldAliasActionNameSpace} from "../../search/index-field-alias/stores/admin-index-field-alias.action";
import {DeleteDialogData} from "../components/dialogs/delete/delete.dialog";
import {ButtonColorEnum} from "../enums/button-color.enum";
import {StatePartialEnum} from "../enums/partial/state-partial.enum";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../injection-tokens/label-to-translate.injection-token";
import {ExtendEnum} from "../../core-resources/types/extend-enum.type";
import {AbstractBaseService} from "./abstract-base.service";

export abstract class AbstractStoreDialogService extends AbstractBaseService {
  private readonly _messageNotFound: string = "There is no dialog for action '{0}' declared for the state '{1}'. Please add 'if' statement into StoreDialogService class.";

  constructor(@Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslate: LabelTranslateInterface) {
    super();
  }

  protected abstract _deleteDataInternal(state: ExtendEnum<StatePartialEnum>): DeleteDialogData | undefined;

  deleteData(state: ExtendEnum<StatePartialEnum>): DeleteDialogData {
    let deleteDialogData = this._deleteDataSolidify(state);

    if (isNullOrUndefined(deleteDialogData)) {
      deleteDialogData = this._deleteDataInternal(state);
    }

    if (isNullOrUndefined(deleteDialogData)) {
      this._promptError("Delete", state);
    }
    deleteDialogData.colorConfirm = ButtonColorEnum.warn;
    deleteDialogData.colorCancel = ButtonColorEnum.primary;
    return deleteDialogData;
  }

  private _deleteDataSolidify(state: ExtendEnum<StatePartialEnum>): DeleteDialogData {
    const sharedDeleteDialogData = {} as DeleteDialogData;
    if (state === StatePartialEnum.admin_oaiSet) {
      sharedDeleteDialogData.message = this._labelTranslate.oaiPmhOaiSetDialogDeleteMessage;
      sharedDeleteDialogData.resourceNameSpace = adminOaiSetActionNameSpace;
    } else if (state === StatePartialEnum.admin_oaiMetadataPrefix) {
      sharedDeleteDialogData.message = this._labelTranslate.oaiPmhOaiMetadataPrefixDialogDeleteMessage;
      sharedDeleteDialogData.resourceNameSpace = adminOaiMetadataPrefixActionNameSpace;
    } else if (state === StatePartialEnum.admin_indexFieldAlias) {
      sharedDeleteDialogData.message = this._labelTranslate.searchIndexFieldAliasDialogDeleteMessage;
      sharedDeleteDialogData.resourceNameSpace = adminIndexFieldAliasActionNameSpace;
    } else if (state === StatePartialEnum.admin_globalBanner) {
      sharedDeleteDialogData.message = this._labelTranslate.globalBannerAdminDialogDeleteMessage;
      sharedDeleteDialogData.resourceNameSpace = adminGlobalBannerActionNameSpace;
    }
    if (isNotNullNorUndefined(sharedDeleteDialogData.message)) {
      return sharedDeleteDialogData;
    }
    return undefined;
  }

  private _promptError(typeRoute: string, state: ExtendEnum<StatePartialEnum>): void {
    throw Error(StringUtil.format(this._messageNotFound, typeRoute, state));
  }
}
