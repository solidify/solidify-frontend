/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - tabulation.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {InteractivityChecker} from "@angular/cdk/a11y";
import {Platform} from "@angular/cdk/platform";
import {Injectable} from "@angular/core";
import {
  isElement,
  isFunction,
  isHtmlElement,
  isTruthyObject,
} from "../../core-resources/tools/is/is.tool";
import {NumberUtil} from "../../core-resources/utils/number.util";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {AbstractBaseService} from "./abstract-base.service";

// @dynamic
@Injectable({providedIn: "root"})
export class TabulationService extends AbstractBaseService {

  constructor(private readonly _interactivityChecker: InteractivityChecker,
              private readonly _platform: Platform) {
    super();
  }

  getBodyElement(): HTMLElement | undefined {
    return SsrUtil.window?.document ? SsrUtil.window?.document.body : undefined;
  }

  getActiveElement(): Element | undefined {
    return SsrUtil.window?.document ? SsrUtil.window?.document.activeElement : undefined;
  }

  getAllElementsArray(parentElement: Element = isTruthyObject(SsrUtil.window?.document) ? SsrUtil.window?.document.documentElement : undefined): Element[] {
    return isElement(parentElement) ? Array.from(parentElement.querySelectorAll("*")) : [];
  }

  focusByDelta(delta: number, fromElement: Element = this.getActiveElement(), inclusive?: boolean): HTMLElement | null {
    const parentElement = isTruthyObject(SsrUtil.window?.document) ? SsrUtil.window?.document.documentElement : undefined;
    if (!isElement(parentElement) || !parentElement.contains(fromElement)) {
      return null;
    }
    const targetElement = this.traverseByDelta(
      delta,
      (htmlElement: HTMLElement) => this.isHtmlElementFocusableAndTabbable(htmlElement),
      (htmlElement: HTMLElement) => htmlElement === fromElement,
      fromElement,
      inclusive,
    ) as HTMLElement;
    if (this._platform.isBrowser && isElement(targetElement)) {
      targetElement.focus();
    }
    return targetElement;
  }

  getByDelta(delta: number, fromElement: Element = this.getActiveElement(), inclusive?: boolean): HTMLElement | null {
    return this.traverseByDelta(
      delta,
      (htmlElement: HTMLElement) => this.isHtmlElementFocusableAndTabbable(htmlElement),
      (htmlElement: HTMLElement) => htmlElement === fromElement,
      fromElement,
      inclusive,
    ) as HTMLElement;
  }

  getPrev(fromElement: Element = this.getActiveElement(), inclusive?: boolean): HTMLElement | null {
    return this.getByDelta(-1, fromElement, inclusive);
  }

  getNext(fromElement: Element = this.getActiveElement(), inclusive?: boolean): HTMLElement | null {
    return this.getByDelta(1, fromElement, inclusive);
  }

  focusPrev(fromElement: Element = this.getActiveElement(), inclusive?: boolean): HTMLElement | null {
    return this.focusByDelta(-1, fromElement, inclusive);
  }

  focusNext(fromElement: Element = this.getActiveElement(), inclusive?: boolean): HTMLElement | null {
    return this.focusByDelta(1, fromElement, inclusive);
  }

  private _getByDelta(delta: number, fromElement: Element, noLoop?: boolean): Element | null {
    // TODO : Review implementation
    const allElementsArray = this.getAllElementsArray();
    const allElementsCount = allElementsArray.length;
    if (allElementsCount) {
      const fromElementIndex = allElementsArray.indexOf(fromElement);
      if (fromElementIndex >= 0) {
        if (!noLoop || NumberUtil.isBetween(fromElementIndex + delta, 0, allElementsCount - 1, true)) {
          return allElementsArray[(fromElementIndex + delta + allElementsCount) % allElementsCount];
        }
      }
    }
    return null;
  }

  _getPrev(fromElement: Element, noLoop?: boolean): Element | null {
    return this._getByDelta(-1, fromElement, noLoop);
  }

  _getNext(fromElement: Element, noLoop?: boolean): Element | null {
    return this._getByDelta(1, fromElement, noLoop);
  }

  private _traverseByDelta(delta: number, findCallback: TraverseCallback, stopCallback: TraverseCallback, fromElement: Element, traversedElements: Element[]): Element | null {
    if (!isElement(fromElement)) {
      return null;
    }
    traversedElements.push(fromElement);
    if (findCallback(fromElement, traversedElements.length - 1, traversedElements)) {
      return fromElement;
    } else if (stopCallback(fromElement, traversedElements.length - 1, traversedElements)) {
      return null;
    } else {
      return this._traverseByDelta(delta, findCallback, stopCallback, this._getByDelta(delta, fromElement), traversedElements);
    }
  }

  private _traversePrev(findCallback: TraverseCallback, stopCallback: TraverseCallback, fromElement: Element, traversedElements: Element[]): Element | null {
    return this._traverseByDelta(-1, findCallback, stopCallback, fromElement, traversedElements);
  }

  private _traverseNext(findCallback: TraverseCallback, stopCallback: TraverseCallback, fromElement: Element, traversedElements: Element[]): Element | null {
    return this._traverseByDelta(1, findCallback, stopCallback, fromElement, traversedElements);
  }

  traverseByDelta(delta: number, findCallback: TraverseCallback, stopCallback: TraverseCallback, fromElement: Element = this.getActiveElement(), inclusive?: boolean, noLoop?: boolean): Element | null {
    if (!isElement(fromElement) || !isFunction(stopCallback)) {
      return null;
    }
    const traversedElements = [];
    if (inclusive) {
      traversedElements.push(fromElement);
      if (findCallback(fromElement, traversedElements.length - 1, traversedElements)) {
        return fromElement;
      }
      if (stopCallback(fromElement, traversedElements.length - 1, traversedElements)) {
        return null;
      }
    }
    const realStopCallback = noLoop ?
      (e: Element, i: number, t: Element[]) => e === this.getBodyElement() || stopCallback(e, i, t)
      : (e: Element, i: number, t: Element[]) => e === fromElement || stopCallback(e, i, t);
    return this._traverseByDelta(delta, findCallback, realStopCallback, this._getByDelta(delta, fromElement, noLoop), traversedElements);
  }

  traversePrev(findCallback: TraverseCallback, stopCallback: TraverseCallback, fromElement: Element = this.getActiveElement(), inclusive?: boolean, noLoop?: boolean): Element | null {
    return this.traverseByDelta(-1, findCallback, stopCallback, fromElement, inclusive, noLoop);
  }

  traverseNext(findCallback: TraverseCallback, stopCallback: TraverseCallback, fromElement: Element = this.getActiveElement(), inclusive?: boolean, noLoop?: boolean): Element | null {
    return this.traverseByDelta(1, findCallback, stopCallback, fromElement, inclusive, noLoop);
  }

  private _getElementChildIndex(element: Element): number {
    let i = -1;
    let previousSibling: Element | null = null;
    do {
      previousSibling = element.previousElementSibling;
      i++;
    } while (previousSibling);
    return i;
  }

  getElementChildIndex(element: Element): number | undefined {
    return isElement(element) ? this._getElementChildIndex(element) : undefined;
  }

  isHtmlElementFocusableAndTabbable(htmlElement: HTMLElement): boolean {
    return isHtmlElement(htmlElement) && this._interactivityChecker.isFocusable(htmlElement) && this._interactivityChecker.isTabbable(htmlElement);
  }

}

export type TraverseCallback<T = any, TElement extends Element = Element> = (element?: Element, index?: number, traversedElements?: Element[]) => T;
