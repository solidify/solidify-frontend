/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - router-extension.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  NavigationExtras,
  NavigationStart,
  ResolveEnd,
  Router,
} from "@angular/router";
import {tap} from "rxjs/operators";
import {isNullOrUndefined} from "../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {AppRoutesPartialEnum} from "../enums/partial/app-routes-partial.enum";
import {UrlUtil} from "../utils/url.util";
import {AbstractBaseService} from "./abstract-base.service";

@Injectable({
  providedIn: "root",
})
export class RouterExtensionService extends AbstractBaseService {
  private _previousUrl: string = undefined;
  private _currentUrl: string = undefined;

  constructor(private readonly _router: Router) {
    super();
    this._currentUrl = this._cleanUrl(this._router.url);
    this.subscribe(_router.events.pipe(
      tap(event => {
        if (event instanceof NavigationStart) {
          if (event.navigationTrigger === "popstate" && event.url === "/" + AppRoutesPartialEnum.redirect) {
            SsrUtil.window?.history.back();
          }
        }
        if (event instanceof ResolveEnd) {
          this._previousUrl = this._currentUrl;
          this._currentUrl = this._cleanUrl(event.url);
        }
      }),
    ));
  }

  private _cleanUrl(url: string): string {
    return UrlUtil.removeParametersFromUrl(url);
  }

  getPreviousUrl(): string {
    return this._previousUrl;
  }

  getCurrentUrl(): string {
    return this._currentUrl;
  }

  previousRouteIsChild(): boolean {
    if (isNullOrUndefined(this._previousUrl)) {
      return false;
    }
    return this._previousUrl.includes(this._currentUrl);
  }

  navigate(commands: any[], extras?: NavigationExtras, forceReloadOnNavigate: boolean = false): Promise<boolean> {
    if (forceReloadOnNavigate) {
      return this._forceReloadOnNavigate(commands, extras);
    }
    return this._router.navigate(commands, extras);
  }

  /**
   * Force to reload current component and apply guard if new route is same as current route.
   * @param commands
   * @param extras
   */
  private _forceReloadOnNavigate(commands: any[], extras?: NavigationExtras): Promise<boolean> {
    return this._router.navigateByUrl(AppRoutesPartialEnum.redirect, {skipLocationChange: false}).then(() => this._router.navigate(commands, extras));
  }
}
