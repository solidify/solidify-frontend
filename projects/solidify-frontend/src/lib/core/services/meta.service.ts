/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - meta.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DOCUMENT} from "@angular/common";
import {
  Inject,
  Injectable,
  Optional,
} from "@angular/core";
import {Meta} from "@angular/platform-browser";
import {TranslateService} from "@ngx-translate/core";
import {
  isFunction,
  isNonEmptyArray,
  isNotNullNorUndefinedNorWhiteString,
} from "../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../injection-tokens/environment.injection-token";
import {META_INFO_LIST} from "../injection-tokens/meta-info-list.injection-token";
import {AbstractBaseService} from "./abstract-base.service";

@Injectable({providedIn: "root"})
export class MetaService<T = any> extends AbstractBaseService {
  get origin(): string {
    return SsrUtil.window?.location.origin;
  }

  get pathName(): string {
    return SsrUtil.window?.location.pathname;
  }

  constructor(private readonly translate: TranslateService,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment,
              @Optional() @Inject(META_INFO_LIST) private readonly _listMetaInfo: MetaInfo<T>[],
              @Inject(DOCUMENT) private readonly _document: Document,
              public readonly meta: Meta) {
    super();
  }

  findMetaToUpdate(urlRequested: string): void {
    let found = false;

    if (isNonEmptyArray(this._listMetaInfo) && isNotNullNorUndefinedNorWhiteString(urlRequested)) {
      found = this._listMetaInfo.some(metaInfo => {
        if (metaInfo.regex.test(urlRequested)) {
          if (metaInfo.bypassAutomaticMeta) {
            return true;
          }
          this.setMetaFromInfo(metaInfo);
          return true;
        }
        return false;
      });
    }

    if (!found) {
      this._setDefaultMeta();
    }
  }

  setMetaFromInfo(metaInfo?: MetaInfo<T>, data?: T): void {
    this.setMeta(
      metaInfo.titleToTranslateCallback(this.translate, data),
      metaInfo.descriptionToTranslateCallback(this.translate, data),
      metaInfo.imageOpenGraph,
      metaInfo.extraMetaTreatmentCallback,
      data,
    );
  }

  setMeta(title: string, description: string, imageOpenGraph: string = this._environment.imageOpenGraph, extraMetaTreatmentCallback?: ExtraMetaTreatmentCallback<T>, data?: T): void {
    const urlRequested = this.pathName;
    this._document.title = title;
    this.meta.updateTag({
      name: "description",
      content: description,
    });

    this.meta.updateTag({
      property: "og:title",
      content: title,
    });
    this.meta.updateTag({
      property: "og:description",
      content: description,
    });

    this.meta.updateTag({
      property: "og:url",
      content: this.origin + urlRequested,
    });

    this._document.querySelector("link[rel=\"canonical\"]")?.setAttribute("href", this.origin + urlRequested);

    if (isNotNullNorUndefinedNorWhiteString(imageOpenGraph)) {
      this.meta.updateTag({
        property: "og:image",
        content: this.origin + "/assets/" + imageOpenGraph,
      });
    }

    if (isFunction(extraMetaTreatmentCallback)) {
      extraMetaTreatmentCallback(this._document, this.meta, this.translate, data);
    }
  }

  resetMeta(): void {
    this._setDefaultMeta();
  }

  private _setDefaultMeta(): void {
    this.setMeta(this._environment.appTitle, this.translate.instant(this._environment.appDescription));
  }
}

export type ExtraMetaTreatmentCallback<T> = (document: Document, meta: Meta, translateService: TranslateService, data?: T) => void;

export interface MetaInfo<T = any> {
  regex: RegExp;
  titleToTranslateCallback: (translate: TranslateService, t?: T) => string;
  descriptionToTranslateCallback: (translate: TranslateService, t?: T) => string;
  extraMetaTreatmentCallback?: ExtraMetaTreatmentCallback<T>;
  imageOpenGraph?: string;
  bypassAutomaticMeta?: boolean;
}
