/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - errors-skipper.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {Injectable} from "@angular/core";
import {isNullOrUndefined} from "../../core-resources/tools/is/is.tool";
import {AbstractBaseService} from "./abstract-base.service";

@Injectable({
  providedIn: "root",
})
export class ErrorsSkipperService extends AbstractBaseService {
  private _errorHttpCodeToSkipTemporary: number[];

  constructor() {
    super();
    this.init();
  }

  init(): void {
    if (isNullOrUndefined(this._errorHttpCodeToSkipTemporary)) {
      this._errorHttpCodeToSkipTemporary = [];
    }
  }

  addErrorSoSkip(error: number): void {
    if (!this._errorHttpCodeToSkipTemporary.includes(error)) {
      this._errorHttpCodeToSkipTemporary.push(error);
    }
  }

  removeErrorSoSkip(error: number): void {
    if (!this._errorHttpCodeToSkipTemporary.includes(error)) {
      return;
    }
    const index = this._errorHttpCodeToSkipTemporary.indexOf(error);
    if (index === -1) {
      return;
    }
    this._errorHttpCodeToSkipTemporary.splice(index, 1);
  }

  shouldBeSkipped(error: number): boolean {
    if (this._errorHttpCodeToSkipTemporary.includes(error)) {
      this.removeErrorSoSkip(error);
      return true;
    }
    return false;
  }
}
