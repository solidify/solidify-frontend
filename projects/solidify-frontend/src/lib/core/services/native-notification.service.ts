/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - native-notification.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {SwPush} from "@angular/service-worker";
import {TranslateService} from "@ngx-translate/core";
import {
  from,
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
  switchMap,
  take,
} from "rxjs/operators";
import {
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
} from "../../core-resources/tools/is/is.tool";
import {SolidifyObject} from "../../core-resources/types/solidify-object.type";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {ApiService} from "../http/api.service";
import {ENVIRONMENT} from "../injection-tokens/environment.injection-token";
import {AbstractBaseService} from "./abstract-base.service";
import {LoggingService} from "./logging.service";

@Injectable({
  providedIn: "root",
})
export class NativeNotificationService extends AbstractBaseService {
  private readonly _VAPID_PUBLIC_KEY: string;

  private readonly _PERMISSION_GRANTED: string = "granted";
  private readonly _PERMISSION_DENIED: string = "denied";

  constructor(private readonly _swPush: SwPush,
              private readonly _translate: TranslateService,
              private readonly _apiService: ApiService,
              private readonly _loggingService: LoggingService,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super();
    this._VAPID_PUBLIC_KEY = this._environment.notificationWebPushPublicKey;
  }

  requestNotification(): Observable<boolean> {
    if (isNotNullNorUndefinedNorWhiteString(this._VAPID_PUBLIC_KEY)) {
      return this._requestNotificationWithServiceWorker();
    }
    return this._requestNotificationWithNotificationApi();
  }

  private _requestNotificationWithNotificationApi(): Observable<boolean> {
    if (isNullOrUndefined(SsrUtil.window?.Notification)) {
      this._loggingService.logWarning("Notification API not defined on this browser");
      return of(false);
    }
    if (Notification.permission === this._PERMISSION_DENIED) {
      this._loggingService.logInfo("Notification feature declined previously by the user");
      return of(false);
    }
    return from(Notification.requestPermission()).pipe(
      map(status => {
        if (status === this._PERMISSION_GRANTED) {
          return true;
        }
        this._loggingService.logInfo("Notification feature declined by the user");
        return false;
      }),
    );
  }

  private _requestNotificationWithServiceWorker(): Observable<boolean> {
    const promise = this._swPush.requestSubscription({
      serverPublicKey: this._VAPID_PUBLIC_KEY,
    });
    return from(promise).pipe(
      take(1),
      switchMap((pushSubscription: PushSubscription) => {
        if (isNullOrUndefined(pushSubscription)) {
          this._loggingService.logInfo("Unable to retrieve pushSubscription information.");
          return of(false);
        }
        if (isNullOrUndefinedOrWhiteString(this._environment.notificationWebPushUrl)) {
          this._loggingService.logInfo("No backend endpoint defined in environment to save pushSubscription information.");
          return of(true);
        }
        return this._apiService.post(this._environment.notificationWebPushUrl, pushSubscription).pipe(
          map(() => true),
          catchError(error => {
            this._loggingService.logError("Error when provide pushSubscription information to backend on endpoint '" + this._environment.notificationWebPushUrl + "'.", error);
            return of(false);
          }),
        );
      }),
      catchError(err => {
        this._loggingService.logError(err);
        return of(false);
      }),
    );
  }

  sendNotificationTranslated(title: string, titleParam: SolidifyObject | undefined, option: NotificationOptions = {}): boolean {
    return this.sendNotification(this._translate.instant(title, titleParam), option);
  }

  sendNotification(title: string, option: NotificationOptions = {}): boolean {
    option.body = title;

    if (isNullOrUndefinedOrWhiteString(option.icon)) {
      option.icon = `assets/themes/${this._environment.theme}/icons/icon-512x512.png`;
    }

    const notification = new Notification(this._environment.appTitle, option);

    if (isNullOrUndefined(window?.Notification) || Notification.permission !== this._PERMISSION_GRANTED) {
      if (Notification.permission === this._PERMISSION_DENIED) {
        this._loggingService.logInfo("WebWorker notification ignored by denial");
        return false;
      }
      this._loggingService.logInfo("WebWorker Notification ignored as not granted, request to the user");
      this.subscribe(this.requestNotification().pipe(take(1)));
      return false;
    }

    notification.onclick = (e: MouseEvent) => {
      notification.close();
    };

    return true;
  }

  unsubscribe(): Observable<boolean> {
    return from(this._swPush.unsubscribe()).pipe(
      map(() => {
        this._loggingService.logInfo("Successfully unsubscribe from native notification");
        return true;
      }),
      catchError(error => {
        this._loggingService.logWarning("Unable to unsubscribe from native notification", error);
        return of(false);
      }),
    );
  }
}
