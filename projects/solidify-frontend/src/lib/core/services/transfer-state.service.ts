/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - transfer-state.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Injectable,
  StateKey,
  TransferState,
} from "@angular/core";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {AbstractBaseService} from "./abstract-base.service";

@Injectable({providedIn: "root"})
export class TransferStateService extends AbstractBaseService {
  constructor(private readonly _transferState: TransferState) {
    super();
  }

  /**
   * Get the value corresponding to a key. Return `defaultValue` if key is not found.
   */
  get<T>(key: StateKey<T>, defaultValue: T): T {
    if (SsrUtil.isServer) {
      return undefined;
    }
    return this._transferState.get(key, defaultValue);
  }

  /**
   * Set the value corresponding to a key.
   */
  set<T>(key: StateKey<T>, value: T): void {
    if (SsrUtil.isBrowser) {
      return;
    }
    return this._transferState.set(key, value);
  }

  /**
   * Remove a key from the store.
   */
  remove<T>(key: StateKey<T>): void {
    if (SsrUtil.isServer) {
      return;
    }
    return this._transferState.remove(key);
  }

  /**
   * Test whether a key exists in the store.
   */
  hasKey<T>(key: StateKey<T>): boolean {
    if (SsrUtil.isServer) {
      return false;
    }
    return this._transferState.hasKey(key);
  }

  /**
   * Indicates whether the state is empty.
   */
  get isEmpty(): boolean {
    return this._transferState.isEmpty;
  }

  /**
   * Register a callback to provide the value for a key when `toJson` is called.
   */
  onSerialize<T>(key: StateKey<T>, callback: () => T): void {
    return this._transferState.onSerialize(key, callback);
  }

  /**
   * Serialize the current state of the store to JSON.
   */
  toJson(): string {
    return this._transferState.toJson();
  }
}
