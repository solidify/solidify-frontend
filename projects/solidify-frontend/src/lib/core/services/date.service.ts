/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - date.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  Inject,
  Injectable,
} from "@angular/core";
import {
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  MatDateFormats,
} from "@angular/material/core";
import {DateUtil} from "../utils/date.util";
import {AbstractBaseService} from "./abstract-base.service";

@Injectable({providedIn: "root"})
export class DateService extends AbstractBaseService {
  constructor(@Inject(MAT_DATE_FORMATS) private readonly _matDateFormat: MatDateFormats,
              @Inject(MAT_DATE_LOCALE) private readonly _matDateLocal: string) {
    super();
  }

  getInputDateFormatExpected(): string {
    const formatToDisplay = this._matDateFormat.display.dateInput;
    const momentLocal = DateUtil.momentDate();
    return momentLocal.locale(momentLocal).localeData().longDateFormat(formatToDisplay);
  }
}
