/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - google-analytics.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
  Renderer2,
} from "@angular/core";
import {
  NavigationEnd,
  Router,
} from "@angular/router";
import {Store} from "@ngxs/store";
import {tap} from "rxjs/operators";
import {isNullOrUndefined} from "../../core-resources/tools/is/is.tool";
import {SolidifyObject} from "../../core-resources/types/solidify-object.type";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {CookieType} from "../cookie-consent/enums/cookie-type.enum";
import {CookieConsentService} from "../cookie-consent/services/cookie-consent.service";
import {CookieConsentUtil} from "../cookie-consent/utils/cookie-consent.util";
import {CookiePartialEnum} from "../enums/partial/cookie-partial.enum";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {CookieHelper} from "../helpers/cookie.helper";
import {ENVIRONMENT} from "../injection-tokens/environment.injection-token";
import {AbstractBaseService} from "./abstract-base.service";

declare let gtag: (name: string, id: string, option: SolidifyObject) => void;

// @dynamic
@Injectable({
  providedIn: "root",
})
export class GoogleAnalyticsService extends AbstractBaseService {
  private static readonly _KEY_CONFIG: string = "config";
  private readonly _ATTRIBUTE_SRC: string = "src";
  private readonly _ATTRIBUTE_SRC_GA_DIFFER_KEY: string = "src-ga-differ";

  constructor(private readonly _store: Store,
              private readonly _cookieConsentService: CookieConsentService,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
              private readonly _router: Router) {
    super();
  }

  init(renderer: Renderer2): void {
    const idGoogleAnalytics = this._environment.googleAnalyticsId;
    if (isNullOrUndefined(idGoogleAnalytics)) {
      return;
    }

    this.subscribe(this._cookieConsentService.watchCookiePreferenceUpdateObs(CookieType.cookie, CookiePartialEnum.googleAnalytics).pipe(
      tap(p => {
        this._manageCookieDependingOfFeatureAuthorization();
      }),
    ));
    this._manageCookieDependingOfFeatureAuthorization();

    gtag(GoogleAnalyticsService._KEY_CONFIG, idGoogleAnalytics, {anonymize_ip: true});
    const script = SsrUtil.window?.document.querySelectorAll(`script[${this._ATTRIBUTE_SRC_GA_DIFFER_KEY}]`)[0];
    const src = script.attributes[this._ATTRIBUTE_SRC_GA_DIFFER_KEY].textContent;
    renderer.setAttribute(script, this._ATTRIBUTE_SRC, src + idGoogleAnalytics);
    renderer.removeAttribute(script, this._ATTRIBUTE_SRC_GA_DIFFER_KEY);

    this.subscribe(this._router.events.pipe(
      tap(event => {
        if (event instanceof NavigationEnd) {
          gtag(GoogleAnalyticsService._KEY_CONFIG, idGoogleAnalytics,
            {
              page_path: event.urlAfterRedirects,
              anonymize_ip: true,
            },
          );
        }
      }),
    ));
  }

  private _manageCookieDependingOfFeatureAuthorization(): void {
    const isFeatureEnabled = CookieConsentUtil.isFeatureAuthorized(CookieType.cookie, CookiePartialEnum.googleAnalytics);
    if (isFeatureEnabled) {
      this.enableCookie();
    } else {
      this.disableCookie();
    }
  }

  disableCookie(): void {
    this._storeCookie(false);
  }

  enableCookie(): void {
    this._storeCookie(true);
  }

  private _storeCookie(storeCookie: boolean): void {
    this._setConfigGoogleAnalyticsStoreCookie(storeCookie);

    if (!storeCookie) {
      CookieHelper.removeItem("_ga");
      CookieHelper.removeItem("_gid");
      CookieHelper.removeItem("_gat");
      CookieHelper.removeItem("AMP_TOKEN");
    }
  }

  private _setConfigGoogleAnalyticsStoreCookie(store: boolean): void {
    gtag(GoogleAnalyticsService._KEY_CONFIG, this._environment.googleAnalyticsId, {
      client_storage: store ? "storage" : "none",
    });
  }
}
