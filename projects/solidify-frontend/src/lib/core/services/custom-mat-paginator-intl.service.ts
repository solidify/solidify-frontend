/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - custom-mat-paginator-intl.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
  OnDestroy,
} from "@angular/core";
import {MatPaginatorIntl} from "@angular/material/paginator";
import {TranslateService} from "@ngx-translate/core";
import {
  Subject,
  Subscription,
} from "rxjs";
import {takeUntil} from "rxjs/operators";
import {LABEL_TRANSLATE} from "../injection-tokens/label-to-translate.injection-token";
import {LabelTranslateInterface} from "../../label-translate-interface.model";

@Injectable({
  providedIn: "root",
})
export class CustomMatPaginatorIntlService extends MatPaginatorIntl implements OnDestroy {

  subscription: Subscription;
  unsubscribeSubject: Subject<void> = new Subject<void>();
  ofLabel: string;

  constructor(private readonly _translate: TranslateService,
              @Inject(LABEL_TRANSLATE) private readonly _labelTranslate: LabelTranslateInterface) {
    super();

    this._translate.onLangChange.pipe(takeUntil(this.unsubscribeSubject)).subscribe(() => {
      this.getAndInitTranslations();
    });

    this.getAndInitTranslations();
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject.next();
    this.unsubscribeSubject.complete();

    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

  getAndInitTranslations(): void {
    this.subscription = this._translate.get([
      this._labelTranslate.corePaginatorItemPerPage,
      this._labelTranslate.corePaginatorNextPage,
      this._labelTranslate.corePaginatorPreviousPage,
      this._labelTranslate.corePaginatorLastPage,
      this._labelTranslate.corePaginatorFirstPage,
      this._labelTranslate.corePaginatorOfLabel,
    ]).pipe(takeUntil(this.unsubscribeSubject))
      .subscribe(translation => {
        this.itemsPerPageLabel = translation[this._labelTranslate.corePaginatorItemPerPage];
        this.nextPageLabel = translation[this._labelTranslate.corePaginatorNextPage];
        this.previousPageLabel = translation[this._labelTranslate.corePaginatorPreviousPage];
        this.previousPageLabel = translation[this._labelTranslate.corePaginatorLastPage];
        this.previousPageLabel = translation[this._labelTranslate.corePaginatorFirstPage];
        this.ofLabel = translation[this._labelTranslate.corePaginatorOfLabel];
        this.changes.next();
      });
  }

  getRangeLabel: (page: number, pageSize: number, length: number) => string = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return `0 ${this.ofLabel} ${length}`;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return `${startIndex + 1} - ${endIndex} ${this.ofLabel} ${length}`;
    // eslint-disable-next-line @stylistic/semi, @stylistic/member-delimiter-style
  };
}
