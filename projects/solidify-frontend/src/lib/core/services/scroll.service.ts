/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - scroll.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ElementRef,
  Injectable,
} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {isNullOrUndefined} from "../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {ObservableUtil} from "../utils/observable.util";
import {AbstractBaseService} from "./abstract-base.service";

// @dynamic
@Injectable({
  providedIn: "root",
})
export class ScrollService extends AbstractBaseService {
  private _mainElementRef: ElementRef;

  private readonly _scrollEventMainBS: BehaviorSubject<any>;
  readonly scrollEventMainObs: Observable<Event | undefined>;

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions) {
    super();
    this._scrollEventMainBS = new BehaviorSubject<Event>(undefined);
    this.scrollEventMainObs = ObservableUtil.asObservable(this._scrollEventMainBS);
  }

  triggerScrollEventMain($event: Event): void {
    this._scrollEventMainBS.next($event);
  }

  init(mainElementRef: ElementRef): void {
    if (isNullOrUndefined(mainElementRef)) {
      return;
    }
    this._mainElementRef = mainElementRef;
  }

  scrollToTop(withTimeout: boolean = false): void {
    if (!this._checkIsInit()) {
      return;
    }
    if (withTimeout) {
      setTimeout(() => {
        this._scrollToTop();
      }, 0);
    } else {
      this._scrollToTop();
    }
  }

  scrollToBottom(withTimeout: boolean = false): void {
    if (!this._checkIsInit()) {
      return;
    }
    if (withTimeout) {
      setTimeout(() => {
        this._scrollToBottom();
      }, 0);
    } else {
      this._scrollToBottom();
    }
  }

  private _scrollToTop(): void {
    if (SsrUtil.isServer) {
      return;
    }
    this._mainElementRef?.nativeElement?.scroll(0, 0);
  }

  private _scrollToBottom(): void {
    if (SsrUtil.isServer) {
      return;
    }
    this._mainElementRef?.nativeElement?.scroll(0, this._mainElementRef.nativeElement.scrollHeight);
  }

  private _checkIsInit(): boolean {
    if (isNullOrUndefined(this._mainElementRef)) {
      // eslint-disable-next-line no-console
      console.warn("Scroll service is not initialized");
      return false;
    }

    return true;
  }
}
