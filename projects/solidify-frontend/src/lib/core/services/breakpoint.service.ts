/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - breakpoint.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {BreakpointObserver} from "@angular/cdk/layout";
import {
  Inject,
  Injectable,
} from "@angular/core";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../injection-tokens/environment.injection-token";
import {AbstractBaseService} from "./abstract-base.service";

@Injectable({
  providedIn: "root",
})
export class BreakpointService extends AbstractBaseService {
  readonly regex: RegExp = new RegExp("[A-Za-z]", "i");

  constructor(private readonly _breakpointObserver: BreakpointObserver,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super();
  }

  isSmallerOrEqualThanMdObs(): Observable<boolean> {
    return this._breakpointObserver.observe(`(max-width: ${this.decrement(this._environment.breakpointMd)})`).pipe(
      map(res => res.matches),
    );
  }

  isBiggerThanMdObs(): Observable<boolean> {
    return this._breakpointObserver.observe(`(max-width: ${this.decrement(this._environment.breakpointMd)})`).pipe(
      map(res => !res.matches),
    );
  }

  isSmallerOrEqualThanSmObs(): Observable<boolean> {
    return this._breakpointObserver.observe(`(max-width: ${this.decrement(this._environment.breakpointSm)})`).pipe(
      map(res => res.matches),
    );
  }

  isBiggerThanSmObs(): Observable<boolean> {
    return this._breakpointObserver.observe(`(max-width: ${this.decrement(this._environment.breakpointSm)})`).pipe(
      map(res => !res.matches),
    );
  }

  isSmallerThanMd(): boolean {
    return this._breakpointObserver.isMatched(`(max-width: ${this.decrement(this._environment.breakpointMd)})`);
  }

  isSmallerThanSm(): boolean {
    return this._breakpointObserver.isMatched(`(max-width: ${this.decrement(this._environment.breakpointSm)})`);
  }

  /***
   * Decrement max-width variable to avoid overlap with inclusive interval
   * @param size
   */
  private decrement(size: string): string {
    let numberInString: string = size;
    let suffix: string = "";
    const index = size.search(this.regex);
    if (index !== -1) {
      numberInString = size.substring(0, index);
      suffix = size.substring(index);
    }
    const numberDecremented: number = +numberInString - 1;
    return numberDecremented + suffix;
  }
}
