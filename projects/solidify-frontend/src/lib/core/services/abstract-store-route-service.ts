/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-store-route-service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Inject} from "@angular/core";
import {isNullOrUndefined} from "../../core-resources/tools/is/is.tool";
import {StringUtil} from "../../core-resources/utils/string.util";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {RoutesPartialEnum} from "../enums/partial/routes-partial.enum";
import {StatePartialEnum} from "../enums/partial/state-partial.enum";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../injection-tokens/environment.injection-token";
import {ExtendEnum} from "../../core-resources/types/extend-enum.type";
import {AbstractBaseService} from "./abstract-base.service";

export abstract class AbstractStoreRouteService extends AbstractBaseService {
  private readonly _messageNotFound: string = "There is no route declared for '{0}' for the state '{1}'. Please add if statement into StoreRouteService class to manage this state.";

  constructor(@Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super();
  }

  protected abstract _getDetailRouteInternal(state: ExtendEnum<StatePartialEnum>): RoutesPartialEnum | undefined;

  protected abstract _getCreateRouteInternal(state: ExtendEnum<StatePartialEnum>): RoutesPartialEnum | undefined;

  protected abstract _getRootRouteInternal(state: ExtendEnum<StatePartialEnum>): RoutesPartialEnum | undefined;

  getDetailRoute(state: ExtendEnum<StatePartialEnum>): RoutesPartialEnum {
    if (state === StatePartialEnum.admin_oaiSet) {
      return this._environment.routeAdminOaiSetDetail;
    }
    if (state === StatePartialEnum.admin_oaiMetadataPrefix) {
      return this._environment.routeAdminOaiMetadataPrefixDetail;
    }
    if (state === StatePartialEnum.admin_indexFieldAlias) {
      return this._environment.routeAdminIndexFieldAliasDetail;
    }
    if (state === StatePartialEnum.admin_globalBanner) {
      return this._environment.routeAdminGlobalBannerDetail;
    }
    const route = this._getDetailRouteInternal(state);
    if (isNullOrUndefined(route)) {
      this._promptError("Detail", state);
    }
    return route;
  }

  getCreateRoute(state: ExtendEnum<StatePartialEnum>): RoutesPartialEnum {
    if (state === StatePartialEnum.admin_oaiSet) {
      return this._environment.routeAdminOaiSetCreate;
    }
    if (state === StatePartialEnum.admin_oaiMetadataPrefix) {
      return this._environment.routeAdminOaiMetadataPrefixCreate;
    }
    if (state === StatePartialEnum.admin_indexFieldAlias) {
      return this._environment.routeAdminIndexFieldAliasCreate;
    }
    if (state === StatePartialEnum.admin_globalBanner) {
      return this._environment.routeAdminGlobalBannerCreate;
    }
    const route = this._getCreateRouteInternal(state);
    if (isNullOrUndefined(route)) {
      this._promptError("Create", state);
    }
    return route;
  }

  getRootRoute(state: ExtendEnum<StatePartialEnum>): RoutesPartialEnum {
    if (state === StatePartialEnum.admin_oaiSet) {
      return this._environment.routeAdminOaiSet;
    }
    if (state === StatePartialEnum.admin_oaiMetadataPrefix) {
      return this._environment.routeAdminOaiMetadataPrefix;
    }
    if (state === StatePartialEnum.admin_indexFieldAlias) {
      return this._environment.routeAdminIndexFieldAlias;
    }
    if (state === StatePartialEnum.admin_globalBanner) {
      return this._environment.routeAdminGlobalBanner;
    }
    const route = this._getRootRouteInternal(state);
    if (isNullOrUndefined(route)) {
      this._promptError("Root", state);
    }
    return route;
  }

  getEditRoute(state: ExtendEnum<StatePartialEnum>, id: string): RoutesPartialEnum {
    const detailRoute = this.getDetailRoute(state);
    return detailRoute + SOLIDIFY_CONSTANTS.URL_SEPARATOR + id + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this._environment.routeSegmentEdit;
  }

  private _promptError(typeRoute: string, state: ExtendEnum<StatePartialEnum>): void {
    throw Error(StringUtil.format(this._messageNotFound, typeRoute, state));
  }
}
