/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - error.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpErrorResponse,
  HttpStatusCode,
} from "@angular/common/http";
import {ValidationErrors} from "@angular/forms";
import {
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrEmptyArray,
} from "../../core-resources/tools/is/is.tool";
import {StringUtil} from "../../core-resources/utils/string.util";
import {SolidifyError} from "../errors/solidify.error";
import {
  ErrorDto,
  ValidationErrorDto,
} from "../models/errors/error-dto.model";
import {SolidifyHttpErrorResponseModel} from "../models/errors/solidify-http-error-response.model";
import {SOLIDIFY_VALIDATION_ERROR_ALREADY_DISPLAYED} from "../utils/validations/validation.util";

export class ErrorHelper {
  static readonly INVALID_TOKEN_ERROR: string = "invalid_token";

  static extractUserErrors(error: SolidifyHttpErrorResponseModel | SolidifyError | Error | HttpErrorResponse): string | undefined {
    if (!this.isValidationErrorsToDisplay((error as SolidifyHttpErrorResponseModel)?.error)) {
      return undefined;
    }
    const listValidationError = this.extractValidationErrorsNotAlreadyDisplayedFromError(error);
    return this.convertValidationErrorsInString(listValidationError);
  }

  /**
   * return true if there is at least one validation error to display
   * @param value
   */
  static isValidationErrorsToDisplay(value: any): value is ErrorDto {
    if (!this.isValidationErrors(value)) {
      return false;
    }
    const listValidationErrorNotDisplay = this._getOnlyValidationErrorsNotDisplayed(value);
    return isNonEmptyArray(listValidationErrorNotDisplay);
  }

  /**
   * return true if is validation errors
   * @param value
   */
  static isValidationErrors(value: any): value is ErrorDto {
    const errorDto = value as ErrorDto;
    if (isNullOrUndefined(errorDto)) {
      return false;
    }
    if (errorDto.statusCode !== HttpStatusCode.BadRequest) {
      return false;
    }
    if (isNullOrUndefinedOrEmptyArray(this._getAllValidationErrors(errorDto))) {
      return false;
    }
    return true;
  }

  private static _getAllValidationErrors(errorDto: ErrorDto): ValidationErrorDto[] {
    return errorDto.validationErrors;
  }

  private static _getOnlyValidationErrorsNotDisplayed(errorDto: ErrorDto): ValidationErrorDto[] {
    return this._getAllValidationErrors(errorDto).filter(v => v[SOLIDIFY_VALIDATION_ERROR_ALREADY_DISPLAYED] !== true);
  }

  static extractValidationErrorsNotAlreadyDisplayedFromError(error: SolidifyHttpErrorResponseModel | SolidifyError | Error | HttpErrorResponse): ValidationErrorDto[] {
    if (!(error instanceof HttpErrorResponse)) {
      return [];
    }
    if (!ErrorHelper.isValidationErrorsToDisplay(error.error)) {
      return [];
    }
    return this._getOnlyValidationErrorsNotDisplayed(error.error);
  }

  static extractAllValidationErrorsFromError(error: SolidifyHttpErrorResponseModel | SolidifyError | Error | HttpErrorResponse): ValidationErrorDto[] {
    if (!(error instanceof HttpErrorResponse)) {
      return [];
    }
    if (!ErrorHelper.isValidationErrorsToDisplay(error.error)) {
      return [];
    }
    return this._getAllValidationErrors(error.error);
  }

  static convertValidationErrorsInString(listValidationError: ValidationErrorDto[]): string {
    let errorsMessage = StringUtil.stringEmpty;
    if (isNotNullNorUndefined(listValidationError)) {
      const errorMessageSeparator = ". ";
      listValidationError.forEach(validationError => {
        if (isNonEmptyString(errorsMessage)) {
          errorsMessage = errorMessageSeparator;
        }
        if (isNotNullNorUndefined(validationError.errorMessages)) {
          errorsMessage = errorsMessage + validationError.errorMessages.join(errorMessageSeparator);
        }
      });
    }
    return errorsMessage;
  }

  static isErrorToDisplay(error: SolidifyHttpErrorResponseModel, httpErrorKeyToSkipInErrorHandler: string[]): boolean {
    if (error.status === HttpStatusCode.Unauthorized) {
      return true;
    }
    if (error.status === HttpStatusCode.BadRequest) {
      if (ErrorHelper.isValidationErrors(error.error)) {
        if (ErrorHelper.isValidationErrorsToDisplay(error.error)) {
          return true;
        }
        return false;
      } else {
        return true;
      }
    }
    if (![
      HttpStatusCode.NotFound,
      HttpStatusCode.InternalServerError,
      HttpStatusCode.Forbidden,
    ].includes(error.status)) {
      return false;
    }
    const errorDto = error.error as ErrorDto;
    if (isNullOrUndefined(errorDto)) {
      return false;
    }
    if (isNonEmptyArray(httpErrorKeyToSkipInErrorHandler) && httpErrorKeyToSkipInErrorHandler.includes(errorDto.message)) {
      return false;
    }
    return true;
  }

  static mergeValidationErrorsMessages(listValidationError: ValidationErrors[]): string | undefined {
    if (isNullOrUndefinedOrEmptyArray(listValidationError)) {
      return undefined;
    }
    let errorMessages = StringUtil.stringEmpty;
    const errorMessageSeparator = "\n";
    listValidationError.forEach(validationError => {
      if (isNonEmptyString(errorMessages)) {
        errorMessages = errorMessages + errorMessageSeparator;
      }
      if (isNotNullNorUndefined(validationError.errorMessages)) {
        errorMessages = errorMessages + validationError.errorMessages.join(errorMessageSeparator);
      }
    });
    return errorMessages;
  }
}
