/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - url-query-param.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  isEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
} from "../../core-resources/tools/is/is.tool";
import {MappingObject} from "../../core-resources/types/mapping-type.type";
import {MappingObjectUtil} from "../../core-resources/utils/mapping-object.util";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {DefaultSolidifyEnvironment} from "../environments/environment.solidify-defaults";
import {StringUtil} from "../../core-resources/utils/string.util";
import {UrlUtil} from "../utils/url.util";

// @dynamic
export class UrlQueryParamHelper {
  static readonly QUERY_PARAM_OAUTH_CODE: string = "code";
  static readonly QUERY_PARAM_OAUTH_STATE: string = "state";

  private static readonly _QUERY_PARAM_VALUE_AFFECTION_CHAR: string = "=";
  private static readonly _QUERY_PARAM_START_CHAR: string = "?";
  private static readonly _QUERY_PARAM_SEPARATOR_CHAR: string = "&";
  private static readonly _QUERY_PARAM_VALUES_SEPARATOR: string = ";";

  /***
   * Generate query param mapping object from current url
   */
  static getQueryParamMappingObjectFromCurrentUrl(decodeParamValue: boolean = false): MappingObject<string, string | undefined> {
    const queryParametersString = this._getQueryParametersFromCurrentUrl();
    return this.getQueryParamMappingObjectFromQueryParamString(queryParametersString, decodeParamValue);
  }

  /**
   * Generate query param mapping object from url
   *
   * @param url
   * @param decodeParamValue if true, decode param value
   */
  static getQueryParamMappingObjectFromUrl(url: string, decodeParamValue: boolean = false): MappingObject<string, string | undefined> {
    const queryParametersString = this._getQueryParametersFromUrl(url);
    return this.getQueryParamMappingObjectFromQueryParamString(queryParametersString, decodeParamValue);
  }

  /**
   * Generate query param
   *
   * @param queryParametersString query param string without prefix ? (ex: "roleId=MANAGER&personId=64344f4e-5e87-463d-a213-48fa4ab3be85&orgUnitName=Test%20creation%20org%20unit")
   * @param decodeParamValue if true, decode param value
   */
  static getQueryParamMappingObjectFromQueryParamString(queryParametersString: string, decodeParamValue: boolean = false): MappingObject<string, string | undefined> {
    const mappingObject = {} as MappingObject<string, string | undefined>;
    if (isNullOrUndefined(queryParametersString)) {
      return mappingObject;
    }
    queryParametersString = this._removeQueryParamPrefixCharIfPresent(queryParametersString);
    const listQueryParam = this._splitQueryParameters(queryParametersString);
    if (listQueryParam.length === 0) {
      return mappingObject;
    }
    listQueryParam.forEach(queryParam => {
      const queryParamSplitted = queryParam.split(this._QUERY_PARAM_VALUE_AFFECTION_CHAR);
      let queryParamValue = undefined;
      if (queryParamSplitted.length > 1) {
        queryParamValue = isTrue(decodeParamValue) ? SsrUtil.window?.decodeURIComponent(queryParamSplitted[1]) : queryParamSplitted[1];
      }
      MappingObjectUtil.set(mappingObject, queryParamSplitted[0], queryParamValue);
    });

    return mappingObject;
  }

  /**
   * Generate query param string
   */
  static getQueryParamStringFromQueryParamMappingObject(queryParametersMappingObject: MappingObject<string, string | undefined>, encodeParamValue: boolean = false): string {
    if (isNullOrUndefined(queryParametersMappingObject) || MappingObjectUtil.size(queryParametersMappingObject) === 0) {
      return SOLIDIFY_CONSTANTS.STRING_EMPTY;
    }
    let url = this._QUERY_PARAM_START_CHAR;

    MappingObjectUtil.forEach(queryParametersMappingObject, (value: string, key, mappingObject) => {
      const index = MappingObjectUtil.keys(mappingObject).indexOf(key);
      if (index > 0) {
        url = url + this._QUERY_PARAM_SEPARATOR_CHAR;
      }
      url = url + key + this._QUERY_PARAM_VALUE_AFFECTION_CHAR + (encodeParamValue ? SsrUtil.window?.encodeURIComponent(value) : value);
    });

    return url;
  }

  /**
   * Get oauth2 query parameters string from url
   *
   * @return "?code=qtl6us&state=cWW00SlvZaRXghd3Pl7J7QqactitCCjwZMV2H2lu"
   */
  static getQueryParamOAuth2(): string {
    // NEEDED TO PRESERVE QUERY PARAM WHEN ROOTING IS INITIALIZED
    // NECESSARY FOR OAUTH 2 PROCESS (SHOULD PRESERVED CODE AND STATE IN QUERY PARAM)
    const queryParamString = this._getQueryParametersFromCurrentUrl();
    if (isNullOrUndefined(queryParamString)) {
      return StringUtil.stringEmpty;
    }
    let listQueryParam = this._splitQueryParameters(queryParamString);
    listQueryParam = this._preserveOnlyQueryParametersOAuth2(listQueryParam);

    if (listQueryParam.length === 0) {
      return StringUtil.stringEmpty;
    }
    return this._generateQueryParametersStringFromList(listQueryParam);
  }

  /**
   * Get redirect path from path
   *
   * @param path that contains state query param with return param  (ex: "/home?code=qtl6us&state=cWW00SlvZaRXghd3Pl7J7QqactitCCjwZMV2H2lu;%2Fadmin%2Forganizational-unit%2Fcreate%3FroleId%3DMANAGER%26personId%3D64344f4e-5e87-463d-a213-48fa4ab3be85%26orgUnitName%3DTest%2520creation%2520org%2520unit")
   * @return redirect path decoded (ex: "/admin/organizational-unit/create?roleId=MANAGER&personId=64344f4e-5e87-463d-a213-48fa4ab3be85&orgUnitName=Test%20creation%20org%20unit")
   */
  static getRedirectPathFromPath(path: string, environment: DefaultSolidifyEnvironment): string | undefined {
    const splitted = path.split(this._QUERY_PARAM_START_CHAR);
    const queryParamString = splitted.length > 1 ? splitted[1] : undefined;
    if (isNullOrUndefined(queryParamString)) {
      return StringUtil.stringEmpty;
    }

    const listQueryParam = this._splitQueryParameters(queryParamString);
    const stateQueryParam: string = this._getStateFromListQueryParameter(listQueryParam);

    if (isNullOrUndefined(stateQueryParam)) {
      return StringUtil.stringEmpty;
    }
    return UrlQueryParamHelper._getRedirectPathFromStateQueryParam(stateQueryParam, environment);
  }

  /**
   * Remove queryParameters string from path
   *
   * @param redirectPath path with query parameters of not
   * @return path without query parameters
   */
  static removeParametersFromPath(redirectPath: string): string {
    return UrlUtil.removeParametersFromUrl(redirectPath);
  }

  /**
   * Extract redirect path from state query parameter
   * Warning: The redirect path could contain nonceStateSeparator char
   *
   * @param stateQueryParam state query param with nounce and redirect path (ex: "state=SIUl003VrsdcLgtnSdx5vQyxcJA28xPHPZ2IRo6A;MFA;%2Fadmin%2Forganizational-unit%2Fcreate%3FroleId%3DMANAGER%26personId%3D64344f4e-5e87-463d-a213-48fa4ab3be85%26orgUnitName%3DTest%2520creation%2520org%2520unit")
   * @private
   * @return redirect path decoded "/admin/organizational-unit/create?roleId=MANAGER&personId=64344f4e-5e87-463d-a213-48fa4ab3be85&orgUnitName=Test%20creation%20org%20unit"
   */
  private static _getRedirectPathFromStateQueryParam(stateQueryParam: string, environment: DefaultSolidifyEnvironment): string {
    // the state is format like that : "state=" + nonce + nonceStateSeparator + loginMode + nonceStateSeparator + redirectPath
    let stateValues = stateQueryParam.replace(this.QUERY_PARAM_OAUTH_STATE + this._QUERY_PARAM_VALUE_AFFECTION_CHAR, SOLIDIFY_CONSTANTS.STRING_EMPTY);
    stateValues = this._removeNonceFromStateQueryParam(stateValues, environment);
    stateValues = this._removeLoginModeFromStateQueryParam(stateValues, environment);
    return isNullOrUndefinedOrWhiteString(stateValues) ? undefined : SsrUtil.window?.decodeURIComponent(stateValues);
  }

  private static _removeNonceFromStateQueryParam(stateValues: string, environment: DefaultSolidifyEnvironment): string {
    if (isTrue(environment.disableNonceCheck)) {
      return stateValues;
    }
    return stateValues.substring(stateValues.indexOf(environment.nonceStateSeparator) + 1);
  }

  /**
   * Remove login mode from state query parameter (MFA or STANDARD)
   *
   * @param stateValues
   * @param environment
   * @private
   */
  private static _removeLoginModeFromStateQueryParam(stateValues: string, environment: DefaultSolidifyEnvironment): string {
    const indexOfNonceSeparator = stateValues.indexOf(environment.nonceStateSeparator);
    if (indexOfNonceSeparator < 0) {
      return "";
    }
    return stateValues.substring(indexOfNonceSeparator + 1);
  }

  /**
   * Check that query parameter provided is contained in current url
   *
   * @param queryParamName
   * @return true if query parameter provided is present
   */
  static currentUrlContainsQueryParam(queryParamName: string): boolean {
    if (isNullOrUndefined(queryParamName) || isEmptyString(queryParamName)) {
      return false;
    }
    const queryParamString = this._getQueryParametersFromCurrentUrl();
    if (isNullOrUndefined(queryParamString)) {
      return false;
    }
    const listQueryParam = this._splitQueryParameters(queryParamString);
    return isNotNullNorUndefined(listQueryParam.find(queryParam => queryParam.startsWith(queryParamName + this._QUERY_PARAM_VALUE_AFFECTION_CHAR)));
  }

  static currentUrlContainsQueryParameterValue(queryParamName: string, queryParamValue: string): boolean {
    if (!this.currentUrlContainsQueryParam(queryParamName)) {
      return false;
    }
    const queryParamString = this._getQueryParametersFromCurrentUrl();
    if (isNullOrUndefined(queryParamString)) {
      return false;
    }
    const listQueryParam = this._splitQueryParameters(queryParamString);
    const queryParamKeyValue = listQueryParam.find(queryParam => queryParam.startsWith(queryParamName + this._QUERY_PARAM_VALUE_AFFECTION_CHAR));
    if (isNotNullNorUndefined(queryParamKeyValue)) {
      const queryParamValueFromUrl = queryParamKeyValue.substring((queryParamName + this._QUERY_PARAM_VALUE_AFFECTION_CHAR).length);
      return queryParamValueFromUrl === queryParamValue;
    }
    return false;
  }

  private static _preserveOnlyQueryParametersOAuth2(listQueryParam: string[]): string[] {
    return listQueryParam.filter(query => query.startsWith(this.QUERY_PARAM_OAUTH_CODE + this._QUERY_PARAM_VALUE_AFFECTION_CHAR)
      || query.startsWith(this.QUERY_PARAM_OAUTH_STATE + this._QUERY_PARAM_VALUE_AFFECTION_CHAR));
  }

  private static _getQueryParametersFromCurrentUrl(): string | undefined {
    return this._getQueryParametersFromUrl(SsrUtil.window?.location.href);
  }

  private static _getQueryParametersFromUrl(url: string): string | undefined {
    const indexQueryParams = url?.indexOf(this._QUERY_PARAM_START_CHAR);
    return indexQueryParams >= 0 ? url.substring(indexQueryParams + 1) : undefined;
  }

  private static _removeQueryParamPrefixCharIfPresent(queryParamString: string): string {
    if (queryParamString.startsWith(this._QUERY_PARAM_START_CHAR)) {
      return queryParamString.substring(1);
    }
    return queryParamString;
  }

  private static _splitQueryParameters(queryParamString: string): string[] {
    return queryParamString.split(this._QUERY_PARAM_SEPARATOR_CHAR);
  }

  private static _generateQueryParametersStringFromList(listQueryParam: string[]): string {
    return this._QUERY_PARAM_START_CHAR + listQueryParam.join(this._QUERY_PARAM_SEPARATOR_CHAR);
  }

  private static _getStateFromListQueryParameter(listQueryParam: string[]): string {
    return listQueryParam.find(query => query.includes(this.QUERY_PARAM_OAUTH_STATE + this._QUERY_PARAM_VALUE_AFFECTION_CHAR));
  }

}

