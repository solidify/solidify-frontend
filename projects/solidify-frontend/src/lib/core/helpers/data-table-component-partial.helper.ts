/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - data-table-component-partial.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MappingObject} from "../../core-resources/types/mapping-type.type";
import {MappingObjectUtil} from "../../core-resources/utils/mapping-object.util";
import {ObjectUtil} from "../../core-resources/utils/object.util";
import {DataTableComponentPartialEnum} from "../enums/datatable/partial/data-table-component-partial.enum";
import {DataTableColumns} from "../models/datatable/data-table-columns.model";
import {DataTableComponent} from "../models/datatable/data-table-component.model";
import {BaseResource} from "../../core-resources/models/dto/base-resource.model";

// @dynamic
export abstract class DataTableComponentPartialHelper {
  static getInternal(listComponent: MappingObject<DataTableComponentPartialEnum, DataTableComponent<BaseResource>>, componentPartialEnumKey: DataTableComponentPartialEnum): DataTableComponent {
    return MappingObjectUtil.get(listComponent, componentPartialEnumKey);
  }

  private static readonly _SEPARATOR: string = ".";

  static getData<TResource extends BaseResource>(row: TResource, column: DataTableColumns): any {
    const field = column.field;
    if ((field as string).includes(this._SEPARATOR)) {
      return ObjectUtil.getNestedValue(row as any, column.field as string);
    }
    return row[field];
  }
}
