/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - polling.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

/* eslint-disable no-console */
import {
  asyncScheduler,
  EMPTY,
  expand,
  fromEvent,
  Observable,
  of,
  SchedulerLike,
  timer,
} from "rxjs";
import {
  delay,
  filter,
  map,
  mapTo,
  sampleTime,
  startWith,
  switchMap,
  takeWhile,
  tap,
} from "rxjs/operators";
import {
  isFalse,
  isNumberFinite,
  isNumberValid,
  isTrue,
  isUndefined,
} from "../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../core-resources/utils/ssr.util";

export class PollingHelper<Type> {
  private static readonly _SECOND_TO_MILLISECOND: number = 1000;
  private static readonly _POLLING_MAX_INTERVAL_IN_SECOND: number = 60;
  private static readonly _LOG_INFOS: boolean = false;

  static startPollingObs<Type>(config: PollingConfigInfo<Type>): Observable<Type> {
    if (SsrUtil.isServer) {
      return EMPTY;
    }
    if (isNumberValid(config.waitingTimeBeforeStartInSecond) && config.waitingTimeBeforeStartInSecond > 0) {
      return of(undefined).pipe(
        delay(config.waitingTimeBeforeStartInSecond * 1000),
        switchMap(() => this._startPollingObs(config)),
      );
    }
    return this._startPollingObs(config);
  }

  private static _startPollingObs<Type>(config: PollingConfigInfo<Type>): Observable<Type> {
    const stopAfterMaxIntervalReached = isTrue(config.incrementInterval) && isTrue(config.stopRefreshAfterMaximumIntervalReached);
    let maxIntervalReached = false;
    const maxIntervalInSecond = isNumberFinite(config.maximumIntervalRefreshInSecond) ? config.maximumIntervalRefreshInSecond : this._POLLING_MAX_INTERVAL_IN_SECOND;

    const intervalInMillisecond = config.initialIntervalRefreshInSecond * this._SECOND_TO_MILLISECOND;
    const optionIntervalBackoff = {
      initialInterval: intervalInMillisecond,
      maxInterval: maxIntervalInSecond * this._SECOND_TO_MILLISECOND,
      backoffDelay: (iteration, initialInterval) => {
        if (isTrue(config.incrementInterval)) {
          const interval = exponentialBackoffDelay(iteration, initialInterval);
          if (isTrue(stopAfterMaxIntervalReached) && isFalse(maxIntervalReached)) {
            maxIntervalReached = interval >= maxIntervalInSecond * this._SECOND_TO_MILLISECOND;
            if (this._LOG_INFOS && maxIntervalReached) {
              console.info("Max interval rechead");
            }
          }
          return interval;
        } else {
          return initialInterval;
        }
      },
    } as IntervalBackoffConfig;

    const observable = intervalBackoff(optionIntervalBackoff).pipe(
      filter(() => {
        const shouldContinue = isFalse(stopAfterMaxIntervalReached) || isFalse(maxIntervalReached);
        if (this._LOG_INFOS && isTrue(stopAfterMaxIntervalReached) && isFalse(shouldContinue)) {
          console.info("Stop action (Max interval rechead)");
        }
        return shouldContinue;
      }),
      takeWhile(() => isUndefined(config.continueUntil) || config.continueUntil()),
      tap(() => isUndefined(config.doBeforeFilter) || config.doBeforeFilter()),
      filter(() => isUndefined(config.filter) || config.filter()),
      map(() => config.actionToDo()),
    );

    if (isTrue(config.incrementInterval) && isTrue(config.resetIntervalWhenUserMouseEvent)) {
      return fromEvent(SsrUtil.window?.document, "mousemove").pipe(
        sampleTime(intervalInMillisecond),
        tap(() => {
          if (this._LOG_INFOS) {
            console.info("Reset interval due to user mouse event");
          }
          maxIntervalReached = false;
        }),
        startWith(null),
        switchMap(() => observable),
      );
    } else {
      return observable;
    }
  }
}

export interface PollingConfigInfo<Type> {
  waitingTimeBeforeStartInSecond?: number;
  initialIntervalRefreshInSecond: number;
  maximumIntervalRefreshInSecond?: number | undefined;
  doBeforeFilter?: () => void | undefined;
  continueUntil?: () => boolean | undefined;
  filter?: () => boolean | undefined;
  actionToDo: () => Type;
  incrementInterval?: boolean | undefined;
  stopRefreshAfterMaximumIntervalReached?: boolean | undefined;
  resetIntervalWhenUserMouseEvent?: boolean | undefined;
}

interface IntervalBackoffConfig {
  initialInterval: number;
  maxInterval?: number;
  backoffDelay?: (iteration: number, initialInterval: number) => number;
}

// Extracted from backoff-rxjs
// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
function intervalBackoff(
  config: number | IntervalBackoffConfig,
  scheduler: SchedulerLike = asyncScheduler,
): Observable<number> {
  let {
    initialInterval,
    // eslint-disable-next-line prefer-const
    maxInterval = Infinity,
    // eslint-disable-next-line prefer-const
    backoffDelay = exponentialBackoffDelay,
  } = typeof config === "number" ? {initialInterval: config} : config;
  initialInterval = initialInterval < 0 ? 0 : initialInterval;
  return of(0, scheduler).pipe(
    // Expend starts with number 1 and then recursively
    // projects each value to new Observable and puts it back in.
    expand(iteration =>
      timer(getDelay(backoffDelay(iteration, initialInterval), maxInterval))
        // Once timer is complete, iteration is increased
        .pipe(mapTo(iteration + 1)),
    ),
  );
}

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
function getDelay(backoffDelay: number, maxInterval: number): number {
  return Math.min(backoffDelay, maxInterval);
}

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
function exponentialBackoffDelay(
  iteration: number,
  initialInterval: number,
): number {
  return Math.pow(2, iteration) * initialInterval;
}
