/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - file-upload.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Guid} from "../../core-resources/models/guid.model";
import {isNullOrUndefined} from "../../core-resources/tools/is/is.tool";
import {FileUploadStatusEnum} from "../enums/upload/file-upload-status.enum";
import {UploadEventModel} from "../models/http/upload-event.model";
import {SolidifyStateContext} from "../models/stores/state-context.model";
import {FileUploadWrapper} from "../models/upload/file-upload-wrapper.model";
import {SolidifyFileUpload} from "../models/upload/solidify-file-upload.model";
import {UploadFileStatus} from "../models/upload/upload-file-status.model";
import {UploadStateModel} from "../stores/abstract/upload/upload-state.model";

export class FileUploadHelper<TResource extends SolidifyFileUpload> {
  static addToUploadStatus<TResource>(ctx: SolidifyStateContext<UploadStateModel<TResource>>, fileUploadWrapper: FileUploadWrapper, isArchive: boolean): UploadFileStatus<TResource> {
    const uploadFileStatus = this._createNewUploadFileStatus<TResource>(fileUploadWrapper, isArchive);
    ctx.patchState({
      uploadStatus: [
        ...ctx.getState().uploadStatus,
        uploadFileStatus,
      ],
    });
    return uploadFileStatus;
  }

  private static _createNewUploadFileStatus<TResource>(fileUploadWrapper: FileUploadWrapper, isArchive: boolean): UploadFileStatus<TResource> {
    return {
      guid: Guid.MakeNew(),
      fileUploadWrapper: fileUploadWrapper,
      status: FileUploadStatusEnum.started,
      progressPercentage: 0,
      isArchive: isArchive,
    } as UploadFileStatus<TResource>;
  }

  static updateInProgressUploadFileStatus<TResource>(ctx: SolidifyStateContext<UploadStateModel<TResource>>, uploadFileStatus: UploadFileStatus<TResource>, event: UploadEventModel): void {
    uploadFileStatus.status = FileUploadStatusEnum.inProgress;
    uploadFileStatus.progressPercentage = this.getUploadProgress(event, uploadFileStatus?.fileUploadWrapper?.file?.size);
    this.updateUploadFileStatus(ctx, uploadFileStatus);
  }

  static getUploadProgress(event: UploadEventModel, total: number): number {
    const loaded = event.loaded;
    if (isNullOrUndefined(total)) {
      total = event.total;
    }
    if (loaded > total) {
      total = loaded;
    }
    return Math.round(100 * loaded / total);
  }

  static updateCompletedUploadFileStatus<TResource>(ctx: SolidifyStateContext<UploadStateModel<TResource>>, uploadFileStatus: UploadFileStatus<TResource>, solidifyFile: TResource): void {
    uploadFileStatus.progressPercentage = 100;
    uploadFileStatus.status = FileUploadStatusEnum.completed;
    uploadFileStatus.result = solidifyFile;
    this.updateUploadFileStatus(ctx, uploadFileStatus);
    setTimeout(() => {
      this.removeToUploadStatus(ctx, uploadFileStatus);
    }, 5000);
  }

  static updateErrorUploadFileStatus<TResource>(ctx: SolidifyStateContext<UploadStateModel<TResource>>, uploadFileStatus: UploadFileStatus<TResource>, messageToTranslate: string): void {
    uploadFileStatus.status = FileUploadStatusEnum.inError;
    uploadFileStatus.errorMessageToTranslate = messageToTranslate;
    this.updateUploadFileStatus(ctx, uploadFileStatus);
  }

  static updateCancelUploadFileStatus<TResource>(ctx: SolidifyStateContext<UploadStateModel<TResource>>, uploadFileStatus: UploadFileStatus<TResource>): void {
    uploadFileStatus.status = FileUploadStatusEnum.canceled;
    this.updateUploadFileStatus(ctx, uploadFileStatus);
  }

  static updateUploadFileStatus<TResource>(ctx: SolidifyStateContext<UploadStateModel<TResource>>, uploadFileStatus: UploadFileStatus<TResource>): void {
    const uploadStatus = [...ctx.getState().uploadStatus];
    const index = this._getIndexCurrentUploadFileStatus(ctx, uploadFileStatus.guid);
    if (index !== -1 && index < uploadStatus.length) {
      uploadStatus[index] = uploadFileStatus;
      ctx.patchState({
        uploadStatus,
      });
    }
  }

  static removeToUploadStatus<TResource>(ctx: SolidifyStateContext<UploadStateModel<TResource>>, uploadFileStatus: UploadFileStatus<TResource>): void {
    const uploadStatus = [...ctx.getState().uploadStatus];
    const index = this._getIndexCurrentUploadFileStatus(ctx, uploadFileStatus.guid);
    if (index !== -1 && index < uploadStatus.length) {
      uploadStatus.splice(index, 1);
      ctx.patchState({
        uploadStatus,
      });
    }
  }

  static getCurrentUploadFileStatus<TResource>(ctx: SolidifyStateContext<UploadStateModel<TResource>>, guid: Guid): UploadFileStatus<TResource> | null {
    const index = this._getIndexCurrentUploadFileStatus(ctx, guid);
    if (index !== -1) {
      return Object.assign({}, ctx.getState().uploadStatus[index]);
    }
    return null;
  }

  private static _getIndexCurrentUploadFileStatus<TResource>(ctx: SolidifyStateContext<UploadStateModel<TResource>>, guid: Guid): number {
    const uploadStatus = ctx.getState().uploadStatus;
    return uploadStatus.findIndex(u => u.guid === guid);
  }
}
