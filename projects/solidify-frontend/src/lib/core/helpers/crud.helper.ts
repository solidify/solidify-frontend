/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - crud.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  map,
  take,
} from "rxjs/operators";
import {isNullOrUndefined} from "../../core-resources/tools/is/is.tool";
import {MappingObjectUtil} from "../../core-resources/utils/mapping-object.util";
import {StringUtil} from "../../core-resources/utils/string.util";
import {BaseResource} from "../../core-resources/models/dto/base-resource.model";
import {FormControlKey} from "../models/forms/form-control-key.model";
import {QueryParameters} from "../../core-resources/models/query-parameters/query-parameters.model";
import {ResourceActionHelper} from "../stores/abstract/resource/resource-action.helper";
import {ResourceNameSpace} from "../stores/abstract/resource/resource-namespace.model";
import {ofSolidifyActionCompleted} from "../utils/stores/store.tool";

// @dynamic
export class CrudHelper {
  private static _checkSingleConstraintAvailable(formControlKey: FormControlKey, store: Store, actions$: Actions, resourceNameSpace: ResourceNameSpace, resId: string | undefined, errorToTranslate: string): Observable<boolean> {
    const queryParameters = new QueryParameters();
    MappingObjectUtil.set(queryParameters.search.searchItems, formControlKey.key, formControlKey.formControl.value);
    store.dispatch(ResourceActionHelper.getAll(resourceNameSpace, queryParameters));
    return actions$.pipe(ofSolidifyActionCompleted(resourceNameSpace.GetAllSuccess))
      .pipe(
        take(1),
        map(result => {
          if (result.result.successful && result.action.list._data.length > 0) {
            if (result.action.list._data.filter((f: BaseResource) =>
              (StringUtil.stringEmpty + f[formControlKey.key]).toLowerCase() === (StringUtil.stringEmpty + formControlKey.formControl.value).toLowerCase()
              && (isNullOrUndefined(resId) || f.resId !== resId)).length === 0) {
              return true;
            }
            formControlKey.formControl.setErrors({
              errorsFromBackend: [errorToTranslate],
            });
            formControlKey.formControl.markAsTouched();
            formControlKey.changeDetector.detectChanges();
            return false;
          }
        }),
      );
  }

  private static _checkMultipleConstraintAvailable(formControlKey: FormControlKey, store: Store, actions$: Actions, resourceNameSpace: ResourceNameSpace, resId: string | undefined, errorToTranslate: string): Observable<boolean> {
    const queryParameters = new QueryParameters();
    const keys = formControlKey.keys;
    keys.forEach(k => {
      MappingObjectUtil.set(queryParameters.search.searchItems, k, formControlKey.form.get(k).value);
    });
    store.dispatch(ResourceActionHelper.getAll(resourceNameSpace, queryParameters));
    return actions$.pipe(ofSolidifyActionCompleted(resourceNameSpace.GetAllSuccess))
      .pipe(
        take(1),
        map(result => {
          if (result.result.successful && result.action.list._data.length > 0) {
            let list = result.action.list._data.filter((f: BaseResource) => isNullOrUndefined(resId) || f.resId !== resId);
            keys.forEach(k => {
              const value = formControlKey.form.get(k).value;
              list = list.filter(t => (StringUtil.stringEmpty + t[k]).toLowerCase() === (StringUtil.stringEmpty + value).toLowerCase());
            });
            if (list.length === 0) {
              return true;
            }

            keys.forEach(k => {
              const formControl = formControlKey.form.get(k);
              formControl.setErrors({
                errorsFromBackend: [errorToTranslate],
              });
            });
            formControlKey.form.markAsTouched();
            formControlKey.changeDetector.detectChanges();
            return false;
          } else {
            keys.forEach(k => {
              const formControl = formControlKey.form.get(k);
              formControl.updateValueAndValidity();
            });
            formControlKey.form.markAsTouched();
            formControlKey.changeDetector.detectChanges();
          }
        }),
      );
  }

  static checkAvailable(formControlKey: FormControlKey, store: Store, actions$: Actions, resourceNameSpace: ResourceNameSpace, resId: string | undefined, errorToTranslate: string): Observable<boolean> {
    if (!isNullOrUndefined(formControlKey.keys) && !isNullOrUndefined(formControlKey.form)) {
      return this._checkMultipleConstraintAvailable(formControlKey, store, actions$, resourceNameSpace, resId, errorToTranslate);
    } else if (!isNullOrUndefined(formControlKey.key) && !isNullOrUndefined(formControlKey.formControl)) {
      return this._checkSingleConstraintAvailable(formControlKey, store, actions$, resourceNameSpace, resId, errorToTranslate);
    }
    throw new Error("Should provide key and formControl for single constraint or key and form for multiple constraint");
  }
}
