/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - in-memory.storage.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {isNullOrUndefined} from "../../core-resources/tools/is/is.tool";

export class InMemoryStorage implements Storage {
  private readonly _storage: Map<string, string> = new Map<string, string>();

  constructor(init: Map<string, string> = new Map<string, string>()) {
    this._storage = init;
  }

  readonly length: number = isNullOrUndefined(this._storage) ? 0 : this._storage.size;

  clear(): void {
    this._storage.clear();
  }

  getItem(key: string): string | null {
    return this._storage.get(key);
  }

  key(index: number): string | null {
    return this._storage[index];
  }

  removeItem(key: string): void {
    this._storage.delete(key);
  }

  setItem(key: string, value: string): void {
    this._storage.set(key, value);
  }
}
