/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - jwt-token.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {Token} from "../models/token.model";
import {StringUtil} from "../../core-resources/utils/string.util";

export class JwtTokenHelper {
  private static _urlBase64Decode(str: string): any {
    let output = str.replace(/-/g, "+").replace(/_/g, "/");
    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += "==";
        break;
      case 3:
        output += "=";
        break;
      default:
        // eslint-disable-next-line no-throw-literal
        throw "Illegal base64url string!";
    }
    return SsrUtil.window?.decodeURIComponent(SsrUtil.window?.escape(window.atob(output)));
  }

  public static decodeToken(token: string = StringUtil.stringEmpty): Token {
    if (token === null || token === StringUtil.stringEmpty) {
      return {} as Token;
    }
    const parts = token.split(".");
    if (parts.length !== 3) {

      throw new Error("JWT must have 3 parts");
    }
    const decoded = this._urlBase64Decode(parts[1]);
    if (!decoded) {
      throw new Error("Cannot decode the token");
    }
    return JSON.parse(decoded);
  }
}
