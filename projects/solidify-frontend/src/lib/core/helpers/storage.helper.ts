/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - storage.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  isFalse,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNumberReal,
  isWhiteString,
} from "../../core-resources/tools/is/is.tool";
import {InMemoryStorage} from "./in-memory.storage";

// @dynamic
export abstract class StorageHelper<StorageKeyEnum> {
  protected static get _storage(): Storage {
    return new InMemoryStorage();
  }

  private static readonly _LIST_SEPARATOR: string = ";";

  static getItem<StorageKeyEnum>(key: StorageKeyEnum): string {
    return this._storage.getItem(key.toString());
  }

  static setItem<StorageKeyEnum>(key: StorageKeyEnum, value: string): void {
    this._storage.setItem(key.toString(), value);
  }

  static removeItem<StorageKeyEnum>(key: StorageKeyEnum): void {
    this._storage.removeItem(key.toString());
  }

  static getListItem<StorageKeyEnum>(key: StorageKeyEnum): string[] {
    let list = [];
    const listSerialized = this._storage.getItem(key.toString());
    if (isNotNullNorUndefined(listSerialized) && isNonEmptyString(listSerialized)) {
      list = listSerialized.split(this._LIST_SEPARATOR);
    }
    return list;
  }

  static initItemInList<StorageKeyEnum>(key: StorageKeyEnum, list: string[]): string[] {
    if (isNullOrUndefined(list)) {
      list = [];
    }
    this._storage.setItem(key.toString(), list.join(this._LIST_SEPARATOR));
    return list;
  }

  static addItemInList<StorageKeyEnum>(key: StorageKeyEnum, item: string, maxItems: number | undefined = undefined): string[] {
    let list = [];
    const listSerialized = this._storage.getItem(key.toString());
    if (isNonEmptyString(listSerialized)) {
      list = listSerialized.split(this._LIST_SEPARATOR);
    }
    if (!list.includes(item)) {
      list.push(item);
      if (isNumberReal(maxItems) && list.length > maxItems) {
        list = list.splice(list.length - maxItems, list.length);
      }
      this._storage.setItem(key.toString(), list.join(this._LIST_SEPARATOR));
    }

    return list;
  }

  static removeItemInList<StorageKeyEnum>(key: StorageKeyEnum, item: string, preserveKeyIfEmptyList: boolean = false): string[] {
    let list = [];
    const listSerialized = this._storage.getItem(key.toString());
    if (!isNullOrUndefined(listSerialized)) {
      if (isWhiteString(listSerialized)) {
        list = [];
      } else {
        list = listSerialized.split(this._LIST_SEPARATOR);
      }
    }
    const indexOfItem = list.indexOf(item);
    if (indexOfItem !== -1) {
      list.splice(indexOfItem, 1);
      if (list.length === 0 && isFalse(preserveKeyIfEmptyList)) {
        this._storage.removeItem(key.toString());
      } else {
        this._storage.setItem(key.toString(), list.join(this._LIST_SEPARATOR));
      }
    }
    return list;
  }

  static hasItemInList<StorageKeyEnum>(key: StorageKeyEnum, item: string): boolean {
    let list = [];
    const listSerialized = this._storage.getItem(key.toString());
    if (isNotNullNorUndefined(listSerialized) && isNonEmptyString(listSerialized)) {
      list = listSerialized.split(this._LIST_SEPARATOR);
    }
    return list.indexOf(item) !== -1;
  }
}
