/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - cookie.storage.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
} from "../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";

export class CookieStorage implements Storage {
  get _storage(): Map<string, string> {
    const cookieString = SsrUtil.isServer ? SsrUtil.window?.document?.[SsrUtil.COOKIE_SSR_PROPERTY_KEY] : SsrUtil.window?.document?.cookie;
    const map = new Map<string, string>();
    if (isNotNullNorUndefinedNorWhiteString(cookieString)) {
      const list = cookieString.split(SOLIDIFY_CONSTANTS.SEMICOLON + SOLIDIFY_CONSTANTS.SPACE);
      list.forEach(item => {
        const itemSplit = item.split(SOLIDIFY_CONSTANTS.EQUAL);
        const key = itemSplit[0];
        const value = itemSplit[1];
        map.set(key, value);
      });
    }
    return map;
  }

  get length(): number {
    return isNullOrUndefined(this._storage) ? 0 : this._storage.size;
  }

  clear(): void {
    this._storage.forEach((key, value) => {
      this.removeItem(key);
    });
  }

  getItem(key: string): string | null {
    return this._storage.get(key);
  }

  key(index: number): string | null {
    return this._storage[index];
  }

  removeItem(key: string): void {
    SsrUtil.window.document.cookie = key + "=; Max-Age=0";
  }

  setItem(key: string, value: string): void {
    SsrUtil.window.document.cookie = this._generateKeyValue(key, value);
  }

  private _generateKeyValue(key: string, value: string): string {
    return `${key}=${value};path=${this._getBaseHref()};`;
  }

  private _getBaseHref(): string {
    const baseHref = SsrUtil.window?.document?.getElementsByTagName("base")?.[0]?.getAttribute("href");
    if (isNullOrUndefinedOrWhiteString(baseHref)) {
      return SOLIDIFY_CONSTANTS.URL_SEPARATOR;
    }
    return baseHref;
  }
}
