/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - validation-error.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {AbstractControl} from "@angular/forms";
import {isNotNullNorUndefined} from "../../core-resources/tools/is/is.tool";
import {MappingObjectUtil} from "../../core-resources/utils/mapping-object.util";

export class ValidationErrorHelper {
  static addError(fc: AbstractControl, errorKey: string): boolean {
    let errorAdded;
    let newError = {};
    if (isNotNullNorUndefined(fc.errors)) {
      newError = Object.assign({}, fc.errors);
      errorAdded = !MappingObjectUtil.has(newError, errorKey) || MappingObjectUtil.get(newError, errorKey) === false;
    } else {
      errorAdded = true;
    }
    MappingObjectUtil.set(newError, errorKey, true);
    fc.setErrors(newError);
    return errorAdded;
  }

  static removeError(fc: AbstractControl, errorKey: string): boolean {
    if (!fc.hasError(errorKey)) {
      return false;
    }
    let newError = undefined;
    if (MappingObjectUtil.size(fc.errors) > 1) {
      newError = Object.assign({}, fc.errors);
      MappingObjectUtil.delete(newError, errorKey);
    }
    fc.setErrors(newError);
    return true;
  }

  static removeAllErrors(fc: AbstractControl): boolean {
    if (MappingObjectUtil.size(fc.errors) === 0) {
      return false;
    }
    fc.setErrors(undefined);
    return true;
  }
}
