/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - dom.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
} from "../../core-resources/tools/is/is.tool";

export class DomHelper {
  static getParentWithTag(target: Element, possibleParentTags: string[], stopElement: Element, stopClass?: string): undefined | Element {
    if (isNullOrUndefined(target) || target === stopElement || (isNotNullNorUndefinedNorWhiteString(stopClass) && target.classList.contains(stopClass))) {
      return undefined;
    }
    if (possibleParentTags.includes(target.tagName.toLowerCase())) {
      return target;
    }
    return this.getParentWithTag(target.parentElement, possibleParentTags, stopElement, stopClass);
  }

  static getParentChildrenWithTag(target: Element, possibleChildrenTags: string[]): undefined | Element {
    const result = target.querySelector(possibleChildrenTags.join(","));
    if (isNullOrUndefined(result)) {
      return undefined;
    }
    return result;
  }
}
