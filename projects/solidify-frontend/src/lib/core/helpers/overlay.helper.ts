/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - overlay.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ConnectedPosition,
  FlexibleConnectedPositionStrategy,
  Overlay,
} from "@angular/cdk/overlay";
import {ElementRef} from "@angular/core";
import {OverlayPositionEnum} from "../enums/overlay-position.enum";

export class OverlayHelper {
  static getFlexibleConnectedPositionStrategy(overlay: Overlay, elementToConnectTo: ElementRef, overlayPosition: OverlayPositionEnum): FlexibleConnectedPositionStrategy {
    return overlay.position()
      .flexibleConnectedTo(elementToConnectTo)
      .withFlexibleDimensions(false)
      .withPositions(this.getPosition(overlayPosition));
  }

  static getPosition(overlayPosition: OverlayPositionEnum): ConnectedPosition[] {
    switch (overlayPosition) {
      case OverlayPositionEnum.top:
        return [{
          originX: "start",
          originY: "top",
          overlayX: "start",
          overlayY: "bottom",
          offsetY: -34,
        }];
      case OverlayPositionEnum.above:
        return [{
          originX: "start",
          originY: "top",
          overlayX: "start",
          overlayY: "top",
        }];
      case OverlayPositionEnum.bottom:
      default:
        return [{
          originX: "start",
          originY: "bottom",
          overlayX: "start",
          overlayY: "top",
          offsetY: -20,
        }];
    }
  }
}
