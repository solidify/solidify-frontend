/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - cookie-consent.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  isEmptyArray,
  isFalse,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isTrue,
} from "../../../core-resources/tools/is/is.tool";
import {LabelTranslateInterface} from "../../../label-translate-interface.model";
import {CookiePartialEnum} from "../../enums/partial/cookie-partial.enum";
import {LocalStoragePartialEnum} from "../../enums/partial/local-storage-partial.enum";
import {DefaultSolidifyEnvironment} from "../../environments/environment.solidify-defaults";
import {CookieHelper} from "../../helpers/cookie.helper";
import {LocalStorageHelper} from "../../helpers/local-storage.helper";
import {SessionStorageHelper} from "../../helpers/session-storage.helper";
import {COOKIE_CONSENT_PREFERENCES} from "../../injection-tokens/cookie-consent-preferences.injection-token";
import {ENVIRONMENT} from "../../injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../injection-tokens/label-to-translate.injection-token";
import {AbstractBaseService} from "../../services/abstract-base.service";
import {NotificationService} from "../../services/notification.service";
import {SolidifyAppAction} from "../../stores/abstract/app/app.action";
import {ArrayUtil} from "../../../core-resources/utils/array.util";
import {ObservableUtil} from "../../utils/observable.util";
import {CookieType} from "../enums/cookie-type.enum";
import {CookiePreference} from "../models/cookie-preference.model";
import {CookieConsentUtil} from "../utils/cookie-consent.util";

@Injectable({
  providedIn: "root",
})
export class CookieConsentService extends AbstractBaseService {
  private readonly _cookiePreferenceUpdatedBS: BehaviorSubject<CookiePreference> = new BehaviorSubject<CookiePreference>(undefined);
  readonly cookiePreferenceUpdatedObs: Observable<CookiePreference> = ObservableUtil.asObservable(this._cookiePreferenceUpdatedBS);

  cookiePreferencePersonalizeCookie: CookiePreference;
  cookiePreferenceCookiesAvailable: CookiePreference;
  cookiePreferenceGoogleAnalytics: CookiePreference;
  cookieDownloadToken: CookiePreference;
  cookiePreferenceLanguage: CookiePreference;
  cookiePreferenceDarkMode: CookiePreference;
  cookiePreferenceGlobalBanner: CookiePreference;
  cookiePreferencePrivacyPolicyAndTermsOfUseAccepted: CookiePreference;

  listCookiePreferences: CookiePreference[];

  constructor(private readonly _store: Store,
              private readonly _notificationService: NotificationService,
              private readonly _translate: TranslateService,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment,
              @Inject(COOKIE_CONSENT_PREFERENCES) private readonly _listCookiePreferencesInjected: CookiePreference[],
  ) {
    super();

    this.cookiePreferencePersonalizeCookie = {
      type: CookieType.localStorage,
      isEnabled: true,
      isTechnical: true,
      name: LocalStoragePartialEnum.personalizeCookie,
      labelToTranslate: this.labelTranslate.coreCookieConsentSidebarPersonalizeCookieTitle,
      descriptionToTranslate: this.labelTranslate.coreCookieConsentSidebarPersonalizeCookieDescription,
      visible: () => true,
    };

    this.cookiePreferenceCookiesAvailable = {
      type: CookieType.localStorage,
      isEnabled: true,
      isTechnical: true,
      name: LocalStoragePartialEnum.cookiesAvailable,
      labelToTranslate: this.labelTranslate.coreCookieConsentSidebarCookiesAvailableTitle,
      descriptionToTranslate: this.labelTranslate.coreCookieConsentSidebarCookiesAvailableDescription,
      visible: () => true,
    };

    this.cookiePreferenceGoogleAnalytics = {
      type: CookieType.cookie,
      isEnabled: false,
      name: CookiePartialEnum.googleAnalytics,
      labelToTranslate: this.labelTranslate.coreCookieConsentSidebarGoogleAnalyticsTitle,
      descriptionToTranslate: this.labelTranslate.coreCookieConsentSidebarGoogleAnalyticsDescription,
      visible: () => isNotNullNorUndefinedNorWhiteString(this._environment.googleAnalyticsId),
    };

    this.cookieDownloadToken = {
      type: CookieType.cookie,
      isEnabled: false,
      name: CookiePartialEnum.downloadToken,
      labelToTranslate: this.labelTranslate.coreCookieConsentSidebarDownloadTokenTitle,
      descriptionToTranslate: this.labelTranslate.coreCookieConsentSidebarDownloadTokenDescription,
      visible: () => this._environment.useDownloadToken,
    };

    this.cookiePreferenceLanguage = {
      type: CookieType.cookie,
      isEnabled: false,
      name: CookiePartialEnum.language,
      labelToTranslate: this.labelTranslate.coreCookieConsentSidebarLanguageTitle,
      descriptionToTranslate: this.labelTranslate.coreCookieConsentSidebarLanguageDescription,
      visible: () => true,
    };

    this.cookiePreferenceDarkMode = {
      type: CookieType.cookie,
      isEnabled: false,
      name: CookiePartialEnum.darkMode,
      labelToTranslate: this.labelTranslate.coreCookieConsentSidebarDarkModeTitle,
      descriptionToTranslate: this.labelTranslate.coreCookieConsentSidebarDarkModeDescription,
      visible: () => true,
    };

    this.cookiePreferenceGlobalBanner = {
      type: CookieType.localStorage,
      isEnabled: false,
      name: LocalStoragePartialEnum.ignoreGlobalBanner,
      labelToTranslate: this.labelTranslate.coreCookieConsentSidebarIgnoreGlobalBannerTitle,
      descriptionToTranslate: this.labelTranslate.coreCookieConsentSidebarIgnoreGlobalBannerDescription,
      visible: () => true,
    };

    this.cookiePreferencePrivacyPolicyAndTermsOfUseAccepted = {
      type: CookieType.localStorage,
      isEnabled: false,
      name: LocalStoragePartialEnum.privacyPolicyAndTermsOfUseAccepted,
      labelToTranslate: this.labelTranslate.coreCookieConsentSidebarPrivacyPolicyAndTermsOfUseAcceptedTitle,
      descriptionToTranslate: this.labelTranslate.coreCookieConsentSidebarPrivacyPolicyAndTermsOfUseAcceptedDescription,
      visible: () => isTrue(this._environment.displayPrivacyPolicyAndTermsOfUseApprovalDialog),
    };

    this.listCookiePreferences = [
      this.cookiePreferencePersonalizeCookie,
      this.cookiePreferenceCookiesAvailable,
      this.cookieDownloadToken,
      this.cookiePreferenceGoogleAnalytics,
      this.cookiePreferenceLanguage,
      this.cookiePreferenceDarkMode,
      this.cookiePreferenceGlobalBanner,
      this.cookiePreferencePrivacyPolicyAndTermsOfUseAccepted,
      ...this._listCookiePreferencesInjected,
    ];

    this.initListCookiePreferencesFromLocalStorage();
    this.subscribe(this.watchCookiePreferenceUpdateObs(CookieType.localStorage, LocalStoragePartialEnum.cookiesAvailable).pipe(
      take(1),
      tap(cookiePreference => {
        this._setCookiesAvailable();
      }),
    ));
  }

  private _setCookiesAvailable(): void {
    if (!CookieConsentUtil.isFeatureAuthorizedFromCookiePreference(this.cookiePreferenceCookiesAvailable)) {
      return;
    }
    const listKey = [];
    this.listCookiePreferences.forEach(cookiePreference => {
      const key = CookieConsentUtil.generateCookiePreferenceIdFromCookiePreference(cookiePreference);
      listKey.push(key);
    });
    LocalStorageHelper.initItemInList(LocalStoragePartialEnum.cookiesAvailable, listKey);
  }

  get numberCookie(): number {
    return this.listCookiePreferences.length;
  }

  get cookieEnabled(): CookiePreference[] {
    return this.listCookiePreferences.filter(p => isTrue(p.isEnabled) && !isTrue(p.isTechnical));
  }

  get numberCookieEnabled(): number {
    return this.cookieEnabled.length;
  }

  get isAllCookiePreferencesAccepted(): boolean {
    return this.listCookiePreferences.findIndex(p => isFalse(p.isEnabled)) === -1;
  }

  watchCookiePreferenceUpdateObs(type: CookieType, name: string): Observable<CookiePreference> {
    return this.cookiePreferenceUpdatedObs.pipe(filter(p => p.type === type && p.name === name));
  }

  watchCookiePreferenceUpdateFromIdObs(id: string): Observable<CookiePreference> {
    return this.cookiePreferenceUpdatedObs.pipe(filter(p => CookieConsentUtil.generateCookiePreferenceId(p.type, p.name) === id));
  }

  isFeatureAuthorized(type: CookieType, name: string): boolean {
    return this.listCookiePreferences.findIndex(p => p.type === type && p.name === name && isTrue(p.isEnabled)) !== -1;
  }

  changeCookieSetting(cookiePreference: CookiePreference, isEnabled: boolean): void {
    const key = CookieConsentUtil.generateCookiePreferenceIdFromCookiePreference(cookiePreference);
    if (isTrue(isEnabled) || isTrue(cookiePreference.isTechnical)) {
      cookiePreference.isEnabled = true;
      LocalStorageHelper.addItemInList(LocalStoragePartialEnum.personalizeCookie, key);
    } else {
      cookiePreference.isEnabled = false;
      LocalStorageHelper.removeItemInList(LocalStoragePartialEnum.personalizeCookie, key);

      if (cookiePreference.type === CookieType.cookie) {
        CookieHelper.removeItem(cookiePreference.name);
      } else if (cookiePreference.type === CookieType.localStorage) {
        LocalStorageHelper.removeItem(cookiePreference.name);
      } else if (cookiePreference.type === CookieType.sessionStorage) {
        SessionStorageHelper.removeItem(cookiePreference.name);
      }
    }
    this._cookiePreferenceUpdatedBS.next(cookiePreference);
  }

  initListCookiePreferencesFromLocalStorage(): void {
    this.listCookiePreferences.forEach(p => {
      if (p.isTechnical) {
        p.isEnabled = true;
      } else {
        p.isEnabled = CookieConsentUtil.isFeatureAuthorizedFromCookiePreference(p);
      }
    });
  }

  acceptAll(): void {
    this.listCookiePreferences.forEach(p => {
      this.changeCookieSetting(p, true);
    });
    this.hideBanner();
  }

  declineAll(): void {
    this.listCookiePreferences.forEach(p => {
      this.changeCookieSetting(p, false);
    });
    this.hideBanner();
  }

  openPersonalizationSidebar(enableAllIfNoCookieEnabled: boolean = false): void {
    if (enableAllIfNoCookieEnabled && this.numberCookieEnabled === 0) {
      this.acceptAll();
    }
    this._store.dispatch(new SolidifyAppAction.ChangeDisplaySidebarCookieConsent(true));
    this.hideBanner();
  }

  closePersonalizationPanel(): void {
    this._store.dispatch(new SolidifyAppAction.ChangeDisplaySidebarCookieConsent(false));
  }

  openBannerIfNeverAnswerOrNewerCookie(): void {
    const personalizeCookie = LocalStorageHelper.getListItem(LocalStoragePartialEnum.personalizeCookie);
    if (isEmptyArray(personalizeCookie)) {
      this.openBanner();
    } else {
      if (isNullOrUndefined(LocalStorageHelper.getItem(LocalStoragePartialEnum.cookiesAvailable))) {
        this._setCookiesAvailable();
        return;
      }
      const previousCookiesAvailableKeys = LocalStorageHelper.getListItem(LocalStoragePartialEnum.cookiesAvailable);
      const currentCookiesAvailableKeys = this.listCookiePreferences.map(cookiePreference => CookieConsentUtil.generateCookiePreferenceIdFromCookiePreference(cookiePreference));
      const diffCookiesAvailable = ArrayUtil.diff(previousCookiesAvailableKeys, currentCookiesAvailableKeys);
      if (diffCookiesAvailable.presentOnlyInArrayB.length > 0) {
        this._setCookiesAvailable();
        this.openBanner(diffCookiesAvailable.presentOnlyInArrayB.length);
      }
    }
  }

  openBanner(newCookiesAvailable: number = undefined): void {
    this._store.dispatch(new SolidifyAppAction.ChangeDisplayBannerCookieConsent(true, newCookiesAvailable));
  }

  hideBanner(): void {
    this._store.dispatch(new SolidifyAppAction.ChangeDisplayBannerCookieConsent(false));
  }

  notifyFeatureDisabledBecauseCookieDeclined(type: CookieType, name: string): void {
    const cookiePreference = this.listCookiePreferences.find(p => p.type === type && p.name === name);
    const label = this._translate.instant(cookiePreference.labelToTranslate);
    this._notificationService.showInformation(this.labelTranslate.coreCookieConsentNotificationFeatureDisabledBecauseCookieDeclined
      , {label: label}, {
        text: this.labelTranslate.coreCookieConsentAllowTheCookie,
        callback: () => {
          this.changeCookieSetting(cookiePreference, true);
          this._notificationService.showSuccess(this.labelTranslate.coreCookieConsentNotificationCookieXEnabled, {label: label});
        },
      });
  }
}
