/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - cookie-consent-sidebar.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Renderer2,
} from "@angular/core";
import {MatSlideToggleChange} from "@angular/material/slide-toggle";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {isTrue} from "../../../../../core-resources/tools/is/is.tool";
import {AbstractInternalContainer} from "../../../../components/containers/abstract-internal/abstract-internal.container";
import {NotificationService} from "../../../../services/notification.service";
import {SolidifyAppAction} from "../../../../stores/abstract/app/app.action";
import {ClipboardUtil} from "../../../../../core-resources/utils/clipboard.util";
import {MemoizedUtil} from "../../../../utils/stores/memoized.util";
import {CookiePreference} from "../../../models/cookie-preference.model";
import {CookieConsentService} from "../../../services/cookie-consent.service";

@Component({
  selector: "solidify-cookie-consent-sidebar-container",
  templateUrl: "./cookie-consent-sidebar.container.html",
  styleUrls: ["./cookie-consent-sidebar.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger("sidebarAnimation", [
      state("void", style({opacity: "0" /*right: "-400px"*/})),
      transition(":enter", animate("300ms ease")),
      transition(":leave", animate("300ms ease")),
    ]),
    trigger("backdropAnimation", [
      state("void", style({opacity: "0"})),
      transition(":enter", animate("300ms ease")),
      transition(":leave", animate("300ms ease")),
    ]),
  ],
})
export class CookieConsentSidebarContainer extends AbstractInternalContainer {
  isOpenedSidebarCookieConsentObs: Observable<boolean>;

  constructor(protected readonly _injector: Injector,
              private readonly _store: Store,
              private readonly _renderer: Renderer2,
              private readonly _actions$: Actions,
              private readonly _notificationService: NotificationService,
              readonly cookieConsentService: CookieConsentService,
  ) {
    super(_injector);
    this.isOpenedSidebarCookieConsentObs = MemoizedUtil.select(this._store, this.environment.appState, s => s.isOpenedSidebarCookieConsent);
  }

  closeCookieConsentSidebar(): void {
    this._store.dispatch(new SolidifyAppAction.ChangeDisplaySidebarCookieConsent(false));
  }

  preferenceChanged($event: MatSlideToggleChange, cookiePreference: CookiePreference): void {
    this.cookieConsentService.changeCookieSetting(cookiePreference, $event.checked);
  }

  generalPreferencesChanged($event: MatSlideToggleChange): void {
    if (isTrue($event.checked)) {
      this.cookieConsentService.acceptAll();
    } else {
      this.cookieConsentService.declineAll();
    }
  }

  copyToClipboardCookieName(name: string): void {
    if (ClipboardUtil.copyStringToClipboard(name)) {
      this._notificationService.showSuccess(this.labelTranslateInterface.coreCookieConsentNotificationCookieNameCopiedToClipboard);
    } else {
      this._notificationService.showError(this.labelTranslateInterface.coreCookieConsentNotificationUnableToCopyCookieNameToClipboard);
    }
  }
}
