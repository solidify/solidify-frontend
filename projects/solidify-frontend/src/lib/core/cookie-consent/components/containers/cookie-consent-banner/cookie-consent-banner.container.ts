/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - cookie-consent-banner.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Injector,
} from "@angular/core";
import {Store} from "@ngxs/store";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {AbstractInternalContainer} from "../../../../components/containers/abstract-internal/abstract-internal.container";
import {MemoizedUtil} from "../../../../utils/stores/memoized.util";
import {CookieConsentService} from "../../../services/cookie-consent.service";

@Component({
  selector: "solidify-cookie-consent-banner-container",
  templateUrl: "./cookie-consent-banner.container.html",
  styleUrls: ["./cookie-consent-banner.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CookieConsentBannerContainer extends AbstractInternalContainer {
  isOpenedBannerCookieConsentObs: Observable<boolean>;
  newCookiesAvailableObs: Observable<number | undefined>;

  @HostBinding("class.is-visible")
  isVisible: boolean;

  constructor(protected readonly _store: Store,
              protected readonly _injector: Injector,
              private readonly _cookieConsentService: CookieConsentService) {
    super(_injector);
    this.isOpenedBannerCookieConsentObs = MemoizedUtil.select(this._store, this.environment.appState, state => state.isOpenedBannerCookieConsent);
    this.newCookiesAvailableObs = MemoizedUtil.select(this._store, this.environment.appState, state => state.newCookiesAvailable);

    this.subscribe(this.isOpenedBannerCookieConsentObs.pipe(
      tap(isOpenedBannerCookieConsent => {
        this.isVisible = isOpenedBannerCookieConsent;
      }),
    ));
  }

  declineAll(): void {
    this._cookieConsentService.declineAll();
  }

  acceptAll(): void {
    this._cookieConsentService.acceptAll();
  }

  personalize(): void {
    const isNewCookieAvailable = MemoizedUtil.selectSnapshot(this._store, this.environment.appState, state => state.newCookiesAvailable) > 0;
    this._cookieConsentService.openPersonalizationSidebar(!isNewCookieAvailable);
  }
}
