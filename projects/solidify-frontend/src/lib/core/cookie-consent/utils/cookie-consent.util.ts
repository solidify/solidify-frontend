/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - cookie-consent.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {LocalStoragePartialEnum} from "../../enums/partial/local-storage-partial.enum";
import {LocalStorageHelper} from "../../helpers/local-storage.helper";
import {CookieType} from "../enums/cookie-type.enum";
import {CookiePreference} from "../models/cookie-preference.model";

export class CookieConsentUtil {
  static generateCookiePreferenceIdFromCookiePreference(cookiePreference: CookiePreference): string {
    return CookieConsentUtil.generateCookiePreferenceId(cookiePreference.type, cookiePreference.name);
  }

  static generateCookiePreferenceId(type: CookieType, name: string): string {
    return `[${type}]_` + name;
  }

  static isFeatureAuthorizedFromCookiePreference(cookiePreference: CookiePreference): boolean {
    const item = CookieConsentUtil.generateCookiePreferenceIdFromCookiePreference(cookiePreference);
    return LocalStorageHelper.hasItemInList(LocalStoragePartialEnum.personalizeCookie, item);
  }

  static isFeatureAuthorized(type: CookieType, name: string): boolean {
    const id = CookieConsentUtil.generateCookiePreferenceId(type, name);
    return LocalStorageHelper.hasItemInList(LocalStoragePartialEnum.personalizeCookie, id);
  }

  static isFeatureAuthorizedFromId(id: string): boolean {
    return LocalStorageHelper.hasItemInList(LocalStoragePartialEnum.personalizeCookie, id);
  }
}
