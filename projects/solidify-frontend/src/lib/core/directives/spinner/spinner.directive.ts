/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - spinner.directive.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ComponentFactoryResolver,
  Directive,
  ElementRef,
  Inject,
  Input,
  OnInit,
  Renderer2,
  ViewContainerRef,
} from "@angular/core";
import {
  isNullOrUndefined,
  isTrue,
} from "../../../core-resources/tools/is/is.tool";
import {DefaultSolidifyEnvironment} from "../../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../injection-tokens/environment.injection-token";
import {AbstractSpinnerDirective} from "./abstract-spinner.directive";

@Directive({
  selector: "[solidifySpinner]",
})
export class SpinnerDirective extends AbstractSpinnerDirective implements OnInit {

  @Input("solidifySpinner")
  set show(value: boolean) {
    if (isNullOrUndefined(value)) {
      // eslint-disable-next-line no-console
      console.warn("solidifySpinner directive received a null or undefined value");
      value = true;
    }
    this._show = value;
    if (!this._isReady) {
      return;
    }

    if (isTrue(this._show)) {
      this._renderer.appendChild(this._elementRef.nativeElement, this._spinnerWrapper);
    } else {
      this._renderer.removeChild(this._elementRef.nativeElement, this._spinnerWrapper);
    }
  }

  @Input("solidifySpinnerStrokeWidth")
  strokeWidth: number;

  @Input("solidifySpinnerDiameter")
  diameter: number;

  @Input("solidifySpinnerWrapperClass")
  spinnerWrapperAdditionalClass: string | undefined = undefined;

  @Input("solidifySpinnerTop")
  set spinnerTop(value: number | undefined) {
    if (isNullOrUndefined(value) || value === 0) {
      this._renderer.removeStyle(this._spinnerWrapper, "top");
      return;
    }
    this._renderer.setStyle(this._spinnerWrapper, "top", value + "px");
  }

  @Input("solidifySpinnerHeight")
  set spinnerHeight(value: number | undefined) {
    if (isNullOrUndefined(value) || value === 0) {
      this._renderer.removeStyle(this._spinnerWrapper, "height");
      return;
    }
    this._renderer.setStyle(this._spinnerWrapper, "height", value + "px");
  }

  @Input("solidifySpinnerMinHeight")
  set spinnerMinHeight(value: number | undefined) {
    if (isNullOrUndefined(value) || value === 0) {
      this._renderer.removeStyle(this._spinnerWrapper, "min-height");
      return;
    }
    this._renderer.setStyle(this._spinnerWrapper, "min-height", value + "px");
  }

  constructor(@Inject(ENVIRONMENT) private readonly environment: DefaultSolidifyEnvironment,
              protected readonly _elementRef: ElementRef,
              protected readonly _renderer: Renderer2,
              protected readonly _viewContainerRef: ViewContainerRef,
              protected readonly _componentFactoryResolver: ComponentFactoryResolver) {
    super(_elementRef, _renderer, _viewContainerRef, _componentFactoryResolver, "spinner-background");
    this.strokeWidth = this.environment.defaultSpinnerStrokeWidth;
    this.diameter = this.environment.defaultSpinnerDiameter;
  }

  ngOnInit(): void {
    super.ngOnInit();
  }
}
