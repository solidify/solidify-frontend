/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-spinner.directive.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ComponentFactoryResolver,
  Directive,
  ElementRef,
  OnInit,
  Renderer2,
  ViewContainerRef,
} from "@angular/core";
import {MatProgressSpinner} from "@angular/material/progress-spinner";
import {
  isNullOrUndefined,
  isTrue,
} from "../../../core-resources/tools/is/is.tool";

@Directive()
export abstract class AbstractSpinnerDirective implements OnInit {
  protected _isReady: boolean;
  protected _show: boolean;
  spinnerWrapperAdditionalClass: string | undefined;

  abstract set show(value: boolean);

  abstract strokeWidth: number;

  abstract diameter: number;

  protected readonly _spinnerWrapper: HTMLDivElement;

  constructor(protected readonly _elementRef: ElementRef,
              protected readonly _renderer: Renderer2,
              protected readonly _viewContainerRef: ViewContainerRef,
              protected readonly _componentFactoryResolver: ComponentFactoryResolver,
              protected readonly _spinnerWrapperClass: string) {
    this._spinnerWrapper = this.createSpinnerWrapper();
  }

  ngOnInit(): void {
    this._renderer.setStyle(this._elementRef.nativeElement, "position", "relative");

    const spinner = this.createSpinnerComponent();
    this._renderer.appendChild(this._spinnerWrapper, spinner._elementRef.nativeElement);
    this.addAdditionalClass();
    this._isReady = true;
    if (isTrue(this._show)) {
      this.show = this._show;
    }
  }

  protected addAdditionalClass(): void {
    if (!isNullOrUndefined(this.spinnerWrapperAdditionalClass)) {
      this._renderer.addClass(this._spinnerWrapper, this.spinnerWrapperAdditionalClass);
    }
  }

  protected createSpinnerWrapper(): HTMLDivElement {
    const spinnerWrapper = this._renderer.createElement("div");
    this._renderer.setStyle(spinnerWrapper, "display", "flex");
    this._renderer.setStyle(spinnerWrapper, "align-items", "center");
    this._renderer.setStyle(spinnerWrapper, "justify-content", "center");
    this._renderer.setStyle(spinnerWrapper, "position", "absolute");
    this._renderer.setStyle(spinnerWrapper, "left", "0");
    this._renderer.setStyle(spinnerWrapper, "right", "0");
    this._renderer.setStyle(spinnerWrapper, "top", "0");
    this._renderer.setStyle(spinnerWrapper, "width", "100%");
    this._renderer.setStyle(spinnerWrapper, "bottom", "0");
    this._renderer.setStyle(spinnerWrapper, "min-height", "0");
    this._renderer.addClass(spinnerWrapper, this._spinnerWrapperClass);
    return spinnerWrapper;
  }

  protected createSpinnerComponent(): MatProgressSpinner {
    const factory = this._componentFactoryResolver.resolveComponentFactory(MatProgressSpinner);
    const componentRef = this._viewContainerRef.createComponent(factory);
    const spinner = componentRef.instance;
    spinner.strokeWidth = this.strokeWidth;
    spinner.diameter = this.diameter;
    spinner.mode = "indeterminate";
    return spinner;
  }
}
