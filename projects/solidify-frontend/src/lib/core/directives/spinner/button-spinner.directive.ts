/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - button-spinner.directive.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ComponentFactoryResolver,
  Directive,
  ElementRef,
  Inject,
  Input,
  OnInit,
  Renderer2,
  ViewContainerRef,
} from "@angular/core";
import {
  isNullOrUndefined,
  isTrue,
} from "../../../core-resources/tools/is/is.tool";
import {DefaultSolidifyEnvironment} from "../../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../injection-tokens/environment.injection-token";
import {AbstractSpinnerDirective} from "./abstract-spinner.directive";

@Directive({
  selector: "[solidifyButtonSpinner]",
})
export class ButtonSpinnerDirective extends AbstractSpinnerDirective implements OnInit {

  @Input("solidifyButtonSpinner")
  set show(value: boolean) {
    if (isNullOrUndefined(value)) {
      // eslint-disable-next-line no-console
      console.warn("solidifyButtonSpinner directive received a null or undefined value");
      value = true;
    }
    this._show = value;
    if (!this._isReady) {
      return;
    }

    if (isTrue(this._show)) {
      this._renderer.setStyle(this._elementRef.nativeElement, "pointer-events", "none");
      this._renderer.appendChild(this._elementRef.nativeElement, this._spinnerWrapper);
    } else {
      this._renderer.removeChild(this._elementRef.nativeElement, this._spinnerWrapper);
      this._renderer.setStyle(this._elementRef.nativeElement, "pointer-events", "initial");
    }

    if (isTrue(this.disableButton)) {
      this._elementRef.nativeElement.disabled = this._show;
    }
  }

  @Input("solidifyButtonSpinnerDisable")
  disableButton: boolean = false;

  @Input("solidifyButtonSpinnerStrokeWidth")
  strokeWidth: number;

  @Input("solidifyButtonSpinnerDiameter")
  diameter: number;

  constructor(@Inject(ENVIRONMENT) private readonly environment: DefaultSolidifyEnvironment,
              protected readonly _elementRef: ElementRef,
              protected readonly _renderer: Renderer2,
              protected readonly _viewContainerRef: ViewContainerRef,
              protected readonly _componentFactoryResolver: ComponentFactoryResolver) {
    super(_elementRef, _renderer, _viewContainerRef, _componentFactoryResolver, "button-spinner-background");

    this.strokeWidth = this.environment.defaultButtonSpinnerStrokeWidth;
    this.diameter = this.environment.defaultButtonSpinnerDiameter;
  }
}
