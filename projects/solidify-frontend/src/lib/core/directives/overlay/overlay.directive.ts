/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - overlay.directive.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ConnectedPosition,
  Overlay,
  OverlayPositionBuilder,
  OverlayRef,
} from "@angular/cdk/overlay";
import {ComponentPortal} from "@angular/cdk/portal";
import {
  ComponentRef,
  Directive,
  ElementRef,
  HostListener,
  Inject,
  Input,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  debounceTime,
  filter,
  tap,
} from "rxjs/operators";
import {
  isFalse,
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "../../../core-resources/tools/is/is.tool";
import {AbstractOverlayPresentational} from "../../components/presentationals/abstract-overlay/abstract-overlay.presentational";
import {DefaultSolidifyEnvironment} from "../../environments/environment.solidify-defaults";
import {DomHelper} from "../../helpers/dom.helper";
import {ENVIRONMENT} from "../../injection-tokens/environment.injection-token";
import {OverlayData} from "../../models/overlay-data.model";
import {ObservableUtil} from "../../utils/observable.util";
import {CoreAbstractDirective} from "../core-abstract/core-abstract.directive";

/**
 * Directive that allow to open an overlay when hover an element with that directive.
 */
@Directive({
  selector: "[solidifyOverlay]",
})
export class OverlayDirective<TData extends OverlayData, UExtra = any> extends CoreAbstractDirective implements OnInit, OnDestroy {
  @Input("solidifyOverlayData")
  data: TData;

  @Input("solidifyOverlayComponent")
  component: AbstractOverlayPresentational<TData>;

  @Input("solidifyOverlayExtra")
  extra: UExtra;

  @Input("solidifyOverlayPosition")
  position: ConnectedPosition = {
    originX: "center",
    originY: "top",
    overlayX: "center",
    overlayY: "bottom",
    offsetY: 8,
  };

  @Input("solidifyOverlayDelay")
  delayInMs: number;

  get isDisabled(): boolean {
    return isNullOrUndefined(this.data) || isNullOrUndefined(this.component);
  }

  private _overlayRef: OverlayRef;

  private readonly _cursorHoverBS: BehaviorSubject<boolean | undefined> = new BehaviorSubject<boolean | undefined>(undefined);
  private readonly _cursorHoverObs: Observable<boolean | undefined> = ObservableUtil.asObservable(this._cursorHoverBS);

  @HostListener("mouseenter")
  show(): void {
    if (this.isDisabled) {
      return;
    }
    if (isNotNullNorUndefined(this._overlayRef.overlayElement)) {
      this._overlayRef.detach();
    }

    this._cursorHoverBS.next(true);
  }

  @HostListener("mouseleave", ["$event"])
  hide($event: MouseEvent): void {
    if (this.isDisabled || isNullOrUndefined(this._overlayRef)) {
      return;
    }

    const overlayElement = this._overlayRef?.hostElement?.firstChild?.firstChild as Element;
    if (isNullOrUndefined(overlayElement)) {
      this._cursorHoverBS.next(false);
      return;
    }
    const overlayTagName = overlayElement.tagName.toLowerCase();
    const targetElement = $event.relatedTarget as Element;
    const element = DomHelper.getParentWithTag(targetElement, [overlayTagName], overlayElement.parentElement);

    if (isNullOrUndefined(element)) {
      this._cursorHoverBS.next(false);
    }
  }

  constructor(private readonly _overlayPositionBuilder: OverlayPositionBuilder,
              private readonly _elementRef: ElementRef,
              private readonly _overlay: Overlay,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super();
    this.delayInMs = this._environment.overlayDelayInMs;
  }

  ngOnInit(): void {
    super.ngOnInit();

    const positionStrategy = this._overlayPositionBuilder
      .flexibleConnectedTo(this._elementRef)
      .withPositions([this.position]);

    this._overlayRef = this._overlay.create({positionStrategy});

    this.subscribe(this._cursorHoverObs.pipe(
      tap(display => {
        if (isFalse(display)) {
          this._overlayRef.detach();
        }
      }),
      filter(display => display),
      debounceTime(this.delayInMs),
      tap(() => {
        if (isFalse(this._cursorHoverBS.value)) {
          return;
        }
        const overlayRef: ComponentRef<AbstractOverlayPresentational<TData>> = this._overlayRef.attach(new ComponentPortal(this.component as any));
        overlayRef.instance.data = this.data;
        overlayRef.instance.extra = this.extra;
        overlayRef.instance.overlayRef = this._overlayRef;
      }),
    ));
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._overlayRef.detach();
  }
}
