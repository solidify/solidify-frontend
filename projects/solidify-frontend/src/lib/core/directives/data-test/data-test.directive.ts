/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - data-test.directive.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  ElementRef,
  Input,
  Renderer2,
} from "@angular/core";
import {DataTestPartialEnum} from "../../../core-resources/enums/partial/data-test-partial.enum";
import {isNullOrUndefined} from "../../../core-resources/tools/is/is.tool";
import {SOLIDIFY_CONSTANTS} from "../../../core-resources/constants";
import {ExtendEnum} from "../../../core-resources/types/extend-enum.type";

@Directive({
  selector: "[solidifyDataTest]",
})
export class DataTestDirective {
  private _dataTest: ExtendEnum<DataTestPartialEnum>;

  @Input("solidifyDataTest")
  set dataTest(value: ExtendEnum<DataTestPartialEnum>) {
    this._dataTest = value;
    this._onDataTestChange();
  }

  get dataTest(): ExtendEnum<DataTestPartialEnum> {
    return this._dataTest;
  }

  constructor(protected readonly _elementRef: ElementRef,
              protected readonly _renderer: Renderer2) {
  }

  private _onDataTestChange(): void {
    if (isNullOrUndefined(this._dataTest)) {
      this._renderer.removeAttribute(this._elementRef.nativeElement, SOLIDIFY_CONSTANTS.DATA_TEST_ATTRIBUTE_NAME);
    } else {
      this._renderer.setAttribute(this._elementRef.nativeElement, SOLIDIFY_CONSTANTS.DATA_TEST_ATTRIBUTE_NAME, String(this._dataTest));
    }
  }

}
