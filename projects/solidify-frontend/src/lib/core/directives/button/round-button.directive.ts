/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - round-button.directive.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  ElementRef,
  HostListener,
  Input,
  OnInit,
  Renderer2,
} from "@angular/core";
import {MatRipple} from "@angular/material/core";
import {isNullOrUndefined} from "../../../core-resources/tools/is/is.tool";
import {ButtonColorEnum} from "../../enums/button-color.enum";

@Directive({
  selector: "[solidifyRoundButton]",
  providers: [MatRipple],
})
export class RoundButtonDirective implements OnInit {
  @Input("solidifyRoundButtonColor")
  private _color: ButtonColorEnum = ButtonColorEnum.primary;

  @Input("solidifyRoundButtonDisableRipple")
  disableRipple: boolean = false;

  constructor(protected readonly _elementRef: ElementRef,
              protected readonly _renderer: Renderer2,
              protected readonly _ripple: MatRipple) {
  }

  ngOnInit(): void {
    if (!isNullOrUndefined(this._color)) {
      this._renderer.addClass(this._elementRef.nativeElement, this._color);
    }
    this._renderer.setAttribute(this._elementRef.nativeElement, "type", "button");
    this._renderer.setStyle(this._elementRef.nativeElement, "position", "relative");
    if (!this.disableRipple) {
      this._renderer.setStyle(this._elementRef.nativeElement, "overflow", "hidden");
    }
    this._renderer.setStyle(this._elementRef.nativeElement, "border-radius", "50px");
    this._renderer.setStyle(this._elementRef.nativeElement, "padding", "10px");
    this._renderer.setStyle(this._elementRef.nativeElement, "border", "none");
    this._renderer.setStyle(this._elementRef.nativeElement, "cursor", "pointer");
    this._renderer.setStyle(this._elementRef.nativeElement, "box-shadow", "0px 2px 20px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12)");
    this._renderer.setStyle(this._elementRef.nativeElement, "display", "flex");
    this._renderer.setStyle(this._elementRef.nativeElement, "align-items", "center");
    this._renderer.setStyle(this._elementRef.nativeElement, "justify-content", "center");
    this._renderer.setStyle(this._elementRef.nativeElement, "margin", "5px 0");
  }

  @HostListener("mousedown", ["$event"]) onmousedown(mouseEvent: MouseEvent): void {
    if (!this.disableRipple) {
      this._ripple.launch(mouseEvent.x, mouseEvent.y);
    }
  }
}
