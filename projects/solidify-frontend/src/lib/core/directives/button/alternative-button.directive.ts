/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - alternative-button.directive.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  ElementRef,
  HostListener,
  Input,
  OnInit,
  Renderer2,
} from "@angular/core";
import {MatRipple} from "@angular/material/core";
import {isNullOrUndefined} from "../../../core-resources/tools/is/is.tool";
import {ButtonColorEnum} from "../../enums/button-color.enum";

@Directive({
  selector: "[solidifyAlternativeButton]",
  providers: [MatRipple],
})
export class AlternativeButtonDirective implements OnInit {
  private _color: ButtonColorEnum = ButtonColorEnum.primary;

  @Input("solidifyAlternativeButtonColor")
  set color(value: ButtonColorEnum) {
    if (isNullOrUndefined(this._color)) {
      this._renderer.removeClass(this._elementRef.nativeElement, this._color);
    }
    this._color = value;
    if (value) {
      this._renderer.addClass(this._elementRef.nativeElement, value);
    }
  }

  get color(): ButtonColorEnum {
    return this._color;
  }

  private _icon: boolean = false;

  @Input("solidifyAlternativeButtonIcon")
  set icon(value: boolean) {
    this._icon = value;

    if (value) {
      this._renderer.addClass(this._elementRef.nativeElement, "with-icon");
    } else {
      this._renderer.removeClass(this._elementRef.nativeElement, "with-icon");
    }
  }

  get icon(): boolean {
    return this._icon;
  }

  private _disableRipple: boolean = false;

  @Input("solidifyAlternativeButtonDisableRipple")
  set disableRipple(value: boolean) {
    this._disableRipple = value;

    if (value) {
      this._renderer.addClass(this._elementRef.nativeElement, "disable-ripple");
    } else {
      this._renderer.removeClass(this._elementRef.nativeElement, "disable-ripple");
    }
  }

  get disableRipple(): boolean {
    return this._disableRipple;
  }

  private _disabled: boolean = false;

  @Input("solidifyAlternativeButtonDisabled")
  set disabled(value: boolean) {
    this._disabled = value;

    if (value) {
      this._renderer.addClass(this._elementRef.nativeElement, "disabled");
    } else {
      this._renderer.removeClass(this._elementRef.nativeElement, "disabled");
    }
  }

  get disabled(): boolean {
    return this._disabled;
  }

  constructor(protected readonly _elementRef: ElementRef,
              protected readonly _renderer: Renderer2,
              protected readonly _ripple: MatRipple) {
  }

  ngOnInit(): void {
    this._renderer.addClass(this._elementRef.nativeElement, "solidify-alternative-button");
    this._renderer.setAttribute(this._elementRef.nativeElement, "type", "button");
    if (this.color) {
      this._renderer.addClass(this._elementRef.nativeElement, this.color);
    }
    if (this.disableRipple) {
      this._renderer.addClass(this._elementRef.nativeElement, "disable-ripple");
    }
  }

  @HostListener("mousedown", ["$event"]) onmousedown(mouseEvent: MouseEvent): void {
    if (!this.disableRipple && !this.disabled) {
      this._ripple.launch(mouseEvent.x, mouseEvent.y);
    }
  }
}
