/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - file-drop-zone.directive.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output,
  Renderer2,
} from "@angular/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  isNotNullNorUndefinedNorEmptyArray,
  isNotNullNorUndefinedNorWhiteString,
} from "../../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../../core-resources/utils/ssr.util";
import {SOLIDIFY_CONSTANTS} from "../../../core-resources/constants";
import {ObservableUtil} from "../../utils/observable.util";
import {CoreAbstractDirective} from "../core-abstract/core-abstract.directive";

@Directive({
  selector: "[solidifyFileDropZone]",
})
export class FileDropZoneDirective extends CoreAbstractDirective implements OnInit {
  private readonly _DIRECTIVE_CLASS: string = "file-drop-zone";
  private readonly _ATTRIBUTE_IS_FILE_DRAG: string = "is-file-drag";

  private _windowDragFileCounter: number = 0;
  private _elementDragFileCounter: number = 0;

  @Input("solidifyFileDropZoneDisabled")
  dragDisabled: boolean = false;

  @Input("solidifyFileDropZoneExtensions")
  extensions: string[] | undefined;

  private readonly _fileDroppedBS: BehaviorSubject<File[] | undefined> = new BehaviorSubject<File[] | undefined>(undefined);
  @Output("fileDropped")
  readonly fileDroppedObs: Observable<File[] | undefined> = ObservableUtil.asObservable(this._fileDroppedBS);

  private readonly _fileOverBS: BehaviorSubject<boolean | undefined> = new BehaviorSubject<boolean | undefined>(undefined);
  @Output("fileOver")
  readonly fileOverObs: Observable<boolean | undefined> = ObservableUtil.asObservable(this._fileOverBS);

  constructor(private readonly _renderer: Renderer2,
              private readonly _elementRef: ElementRef) {
    super();
    _renderer.addClass(_elementRef.nativeElement, this._DIRECTIVE_CLASS);
  }

  @HostBinding("class.file-over")
  get isFileOver(): boolean {
    return this._fileOverBS.value;
  }

  @HostListener("window:dragenter", ["$event"])
  onDragEnterWindow($event: DragEvent): void {
    if (this.dragDisabled === true) {
      return;
    }
    this._windowDragFileCounter++;
    this._computeWindowDragFileClass();
  }

  @HostListener("window:dragover", ["$event"])
  onDragOverWindow($event: Event): void {
    if (this.dragDisabled === true) {
      return;
    }
    $event.preventDefault();
  }

  @HostListener("window:dragleave")
  onDragLeaveWindow(): void {
    if (this.dragDisabled === true) {
      return;
    }
    this._windowDragFileCounter--;
    this._computeWindowDragFileClass();
  }

  @HostListener("window:drop", ["$event"])
  onDropWindow($event: DragEvent): void {
    if (this.dragDisabled === true) {
      return;
    }
    $event.preventDefault(); // avoid to open file dragged in elemet in new tab
    this._windowDragFileCounter = 0;
    this._computeWindowDragFileClass();
  }

  @HostListener("dragenter")
  onDragEnterElement(): void {
    if (this.dragDisabled === true) {
      return;
    }
    this._elementDragFileCounter++;
    this._computeElementDragFile();
  }

  @HostListener("dragover", ["$event"])
  onDragOverElement($event: Event): void {
    if (this.dragDisabled === true) {
      return;
    }
    $event.preventDefault();
  }

  @HostListener("dragleave")
  onDragLeaveElement(): void {
    if (this.dragDisabled === true) {
      return;
    }
    this._elementDragFileCounter--;
    this._computeElementDragFile();
  }

  @HostListener("drop", ["$event"])
  onDropElement($event: DragEvent): void {
    if (this.dragDisabled === true) {
      return;
    }
    $event.preventDefault(); // avoid to open file dragged in element in new tab
    this._elementDragFileCounter = 0;
    this._computeElementDragFile();
    let files = this._convertFileListToListOfFiles($event.dataTransfer.files);
    files = this._filterDirectory(files);
    files = this._filterExtensions(files);
    if (files.length > 0) {
      this._fileDroppedBS.next(files);
    }
  }

  private _convertFileListToListOfFiles(fileList: FileList): File[] {
    return Array.from(fileList);
  }

  private _filterDirectory(files: File[]): File[] {
    const filteredFileList = files.filter(s => isNotNullNorUndefinedNorWhiteString(s.type) || s.size > 0);
    return filteredFileList as any;
  }

  private _filterExtensions(files: File[]): File[] {
    if (isNotNullNorUndefinedNorEmptyArray(this.extensions)) {
      return files.filter(s => this.extensions.includes(s.name.split(".").pop()));
    }
    return files;
  }

  private _computeWindowDragFileClass(): void {
    if (this._windowDragFileCounter > 0) {
      this._renderer.setAttribute(SsrUtil.document.body, this._ATTRIBUTE_IS_FILE_DRAG, SOLIDIFY_CONSTANTS.STRING_TRUE);
    } else {
      this._renderer.removeAttribute(SsrUtil.document.body, this._ATTRIBUTE_IS_FILE_DRAG);
    }
  }

  private _computeElementDragFile(): void {
    if (this._elementDragFileCounter > 0 && this.isFileOver !== true) {
      this._fileOverBS.next(true);
    } else if (this._elementDragFileCounter === 0 && this.isFileOver === true) {
      this._fileOverBS.next(false);
    }
  }
}
