/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - auto-scroll-into-view.directive.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  ElementRef,
  Input,
  Renderer2,
} from "@angular/core";
import {
  isFunction,
  isTruthy,
  isTruthyObject,
} from "../../../core-resources/tools/is/is.tool";

@Directive({
  selector: "[solidifyAutoScrollIntoView]",
})

export class AutoScrollIntoViewDirective {

  private _autoScrollIntoView: boolean = false;

  @Input("solidifyAutoScrollIntoView")
  set autoScrollIntoView(value: boolean) {
    const valueBoolean = isTruthy(value);
    if (this._autoScrollIntoView !== valueBoolean) {
      this._autoScrollIntoView = valueBoolean;
      this._onAutoScrollIntoViewChange();
    }
  }

  constructor(private readonly _elementRef: ElementRef<HTMLElement>,
              private readonly _renderer: Renderer2) {
  }

  private _onAutoScrollIntoViewChange(): void {
    if (this._autoScrollIntoView) {
      const nativeElement = this._elementRef.nativeElement;
      if (isTruthyObject(nativeElement) && isFunction(nativeElement.scrollIntoView)) {
        nativeElement.scrollIntoView({behavior: "smooth", block: "nearest"});
      }
    }
  }

}
