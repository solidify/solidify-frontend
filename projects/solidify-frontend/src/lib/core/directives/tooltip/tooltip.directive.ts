/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - tooltip.directive.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AriaDescriber,
  FocusMonitor,
} from "@angular/cdk/a11y";
import {Directionality} from "@angular/cdk/bidi";
import {
  Overlay,
  ScrollDispatcher,
} from "@angular/cdk/overlay";
import {Platform} from "@angular/cdk/platform";
import {
  Directive,
  ElementRef,
  Inject,
  Input,
  NgZone,
  Optional,
  Renderer2,
  ViewContainerRef,
} from "@angular/core";
import {
  MAT_TOOLTIP_DEFAULT_OPTIONS,
  MAT_TOOLTIP_SCROLL_STRATEGY,
  MatTooltip,
  MatTooltipDefaultOptions,
} from "@angular/material/tooltip";
import {TranslateService} from "@ngx-translate/core";
import {isNonEmptyString} from "../../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../../core-resources/utils/ssr.util";
import {DefaultSolidifyEnvironment} from "../../environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../injection-tokens/environment.injection-token";

@Directive({
  selector: "[solidifyTooltip]",
  exportAs: "solidifyTooltip",
})
export class TooltipDirective extends MatTooltip {

  get solidifyTooltip(): string {
    return this.message;
  }

  @Input()
  set solidifyTooltip(value: string) {
    if (isNonEmptyString(value)) {
      this._renderer.setAttribute(this.el.nativeElement, "data.tooltip.key", value);
      this.message = this._translate.instant(value);
    } else {
      this._renderer.removeAttribute(this.el.nativeElement, "data.tooltip.key");
      this.message = undefined;
    }
  }

  @Input("showDelay")
  set showDelay(value: number) {
    super.showDelay = value;
  }

  constructor(_overlay: Overlay,
              _elementRef: ElementRef,
              _scrollDispatcher: ScrollDispatcher,
              _viewContainerRef: ViewContainerRef,
              _ngZone: NgZone,
              _platform: Platform,
              _ariaDescriber: AriaDescriber,
              _focusMonitor: FocusMonitor,
              @Inject(MAT_TOOLTIP_SCROLL_STRATEGY) _scrollStrategy: any,
              @Optional() _dir: Directionality,
              @Optional() @Inject(MAT_TOOLTIP_DEFAULT_OPTIONS) _defaultOptions: MatTooltipDefaultOptions,
              private readonly _translate: TranslateService,
              private readonly _renderer: Renderer2,
              @Inject(ENVIRONMENT) readonly environment: DefaultSolidifyEnvironment,
              public readonly el: ElementRef) {
    super(_overlay,
      _elementRef,
      _scrollDispatcher,
      _viewContainerRef,
      _ngZone,
      _platform,
      _ariaDescriber,
      _focusMonitor,
      _scrollStrategy,
      _dir,
      _defaultOptions,
      SsrUtil.window?.document,
    );
    this.showDelay = environment.timeBeforeDisplayTooltip;
  }
}
