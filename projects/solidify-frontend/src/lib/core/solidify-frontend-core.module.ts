/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-frontend-core.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {
  FormsModule,
  ReactiveFormsModule,
} from "@angular/forms";
import {
  MAT_MENU_DEFAULT_OPTIONS,
  MatMenuDefaultOptions,
} from "@angular/material/menu";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

import {EmptyContainer} from "./components/containers/empty-container/empty.container";
import {MarkdownViewerContainer} from "./components/containers/markdown-viewer/markdown-viewer.container";
import {ResourceNameContainer} from "./components/containers/resource-name/resource-name.container";
import {TabsContainer} from "./components/containers/tabs/tabs.container";
import {BaseActionDialog} from "./components/dialogs/base-action/base-action.dialog";
import {BaseInfoDialog} from "./components/dialogs/base-info/base-info.dialog";
import {ConfirmDialog} from "./components/dialogs/confirm/confirm.dialog";
import {DataTableLooseSelectionDialog} from "./components/dialogs/data-table-loose-selection/data-table-loose-selection.dialog";
import {DeleteDialog} from "./components/dialogs/delete/delete.dialog";
import {FirstLoginDialog} from "./components/dialogs/first-login/first-login.dialog";
import {KeywordPasteDialog} from "./components/dialogs/keyword-paste/keyword-paste.dialog";
import {PrivacyPolicyTermsOfUseApprovalDialog} from "./components/dialogs/privacy-policy-terms-of-use-approval/privacy-policy-terms-of-use-approval.dialog";
import {BannerPresentational} from "./components/presentationals/banner/banner.presentational";
import {BreadcrumbPresentational} from "./components/presentationals/breadcrumb/breadcrumb.presentational";
import {ButtonIdPresentational} from "./components/presentationals/button-id/button-id.presentational";
import {ButtonOrcidPresentational} from "./components/presentationals/button-orcid/button-orcid.presentational";
import {ButtonRefreshPresentational} from "./components/presentationals/button-refresh/button-refresh.presentational";
import {ButtonStatusHistoryPresentational} from "./components/presentationals/button-status-history/button-status-history.presentational";
import {CitationsPresentational} from "./components/presentationals/citations/citations.presentational";
import {DetailPresentationPresentational} from "./components/presentationals/detail-presentation/detail-presentation.presentational";
import {EmptyFormControlWrapperPresentational} from "./components/presentationals/empty-form-control-wrapper/empty-form-control-wrapper.presentational";
import {FormErrorWrapperPresentational} from "./components/presentationals/form-error-wrapper/form-error-wrapper.presentational";
import {IconPresentational} from "./components/presentationals/icon/icon.presentational";
import {LabelPresentational} from "./components/presentationals/label/label.presentational";
import {LanguageSelectorMenuPresentational} from "./components/presentationals/language-selector-menu/language-selector-menu.presentational";
import {LanguageSelectorPresentational} from "./components/presentationals/language-selector/language-selector.presentational";
import {OrcidSignInButtonPresentational} from "./components/presentationals/orcid-sign-in-button/orcid-sign-in-button.presentational";
import {PaginatorPresentational} from "./components/presentationals/paginator/paginator.presentational";
import {PanelExpandablePresentational} from "./components/presentationals/panel-expandable/panel-expandable.presentational";
import {RibbonPresentational} from "./components/presentationals/ribbon/ribbon.presentational";
import {SnackbarPresentational} from "./components/presentationals/snackbar/snackbar.presentational";
import {StarRatingPresentational} from "./components/presentationals/star-rating/star-rating.presentational";
import {StatusPresentational} from "./components/presentationals/status/status.presentational";
import {TableModuleVersionPresentational} from "./components/presentationals/table-module-version/table-module-version.presentational";
import {ThemeSelectorMenuPresentational} from "./components/presentationals/theme-selector-menu/theme-selector-menu.presentational";
import {ThemeSelectorPresentational} from "./components/presentationals/theme-selector/theme-selector.presentational";
import {ChangelogRoutable} from "./components/routables/changelog/changelog.routable";
import {ComponentAppSummaryRoutable} from "./components/routables/component-app-summary/component-app-summary.routable";
import {EmptyRoutable} from "./components/routables/empty/empty.routable";
import {IconAppSummaryRoutable} from "./components/routables/icon-app-summary/icon-app-summary.routable";
import {MaintenanceModeRoutable} from "./components/routables/maintenance-mode/maintenance-mode.routable";
import {OrcidRedirectRoutable} from "./components/routables/orcid-redirect/orcid-redirect.routable";
import {ReleaseNotesRoutable} from "./components/routables/release-notes/release-notes.routable";
import {ServerOfflineModeRoutable} from "./components/routables/server-offline-mode/server-offline-mode.routable";
import {UnableToLoadAppRoutable} from "./components/routables/unable-to-load-app/unable-to-load-app.routable";
import {CookieConsentBannerContainer} from "./cookie-consent/components/containers/cookie-consent-banner/cookie-consent-banner.container";
import {CookieConsentSidebarContainer} from "./cookie-consent/components/containers/cookie-consent-sidebar/cookie-consent-sidebar.container";
import {AutoScrollIntoViewDirective} from "./directives/auto-scroll-into-view/auto-scroll-into-view.directive";
import {AutoFocusDirective} from "./directives/autofocus/auto-focus.directive";
import {FocusFirstElementDirective} from "./directives/autofocus/focus-first-element.directive";
import {AlternativeButtonDirective} from "./directives/button/alternative-button.directive";
import {MatButtonThemeDirective} from "./directives/button/mat-button-theme.directive";
import {RoundButtonDirective} from "./directives/button/round-button.directive";
import {DataTableAnchorDynamicInjectionDirective} from "./directives/data-table-anchor-dynamic-injection/data-table-anchor-dynamic-injection.directive";
import {DataTestDirective} from "./directives/data-test/data-test.directive";
import {FileDropZoneDirective} from "./directives/file-drop-zone/file-drop-zone.directive";
import {FormControlElementRefDirective} from "./directives/form-control-element-ref/form-control-element-ref.directive";
import {OverlayDirective} from "./directives/overlay/overlay.directive";
import {ShortcutDirective} from "./directives/shortcut/shortcut.directive";
import {ButtonSpinnerDirective} from "./directives/spinner/button-spinner.directive";
import {SpinnerDirective} from "./directives/spinner/spinner.directive";
import {TooltipOnEllipsisDirective} from "./directives/tooltip-on-ellipsis/tooltip-on-ellipsis.directive";
import {TooltipDirective} from "./directives/tooltip/tooltip.directive";
import {ValidationDirective} from "./directives/validation/validation.directive";
import {pipes as otherPipes} from "./pipes/pipes.model";
import {isPipes} from "./pipes/shared-is-pipes/is-pipes.model";
import {SolidifyFrontendMaterialModule} from "./solidify-frontend-material.module";

// Allow to fix ERROR: node_modules/ngx-tour-core/lib/tour.module.d.ts:4:23 - error TS2314: Generic type 'ModuleWithProviders<T>' requires 1 type argument(s) static forRoot(): ModuleWithProviders;
// declare module "@angular/core" {
//   interface ModuleWithProviders<T = any> {
//     ngModule: Type<T>;
//     providers?: Provider[];
//   }
// }

const routables = [
  ReleaseNotesRoutable,
  ChangelogRoutable,
  ComponentAppSummaryRoutable,
  IconAppSummaryRoutable,
  MaintenanceModeRoutable,
  ServerOfflineModeRoutable,
  OrcidRedirectRoutable,
  UnableToLoadAppRoutable,
  EmptyRoutable,
];
const containers = [
  CookieConsentBannerContainer,
  CookieConsentSidebarContainer,
  EmptyContainer,
  MarkdownViewerContainer,
  ResourceNameContainer,
  TabsContainer,
];
const presentationals = [
  BannerPresentational,
  BreadcrumbPresentational,
  ButtonIdPresentational,
  ButtonRefreshPresentational,
  ButtonOrcidPresentational,
  ButtonStatusHistoryPresentational,
  DetailPresentationPresentational,
  IconPresentational,
  EmptyFormControlWrapperPresentational,
  FormErrorWrapperPresentational,
  LanguageSelectorPresentational,
  LanguageSelectorMenuPresentational,
  OrcidSignInButtonPresentational,
  PaginatorPresentational,
  LabelPresentational,
  PanelExpandablePresentational,
  RibbonPresentational,
  SnackbarPresentational,
  StarRatingPresentational,
  StatusPresentational,
  ThemeSelectorPresentational,
  ThemeSelectorMenuPresentational,
  CitationsPresentational,
  TableModuleVersionPresentational,
];
const dialogs = [
  BaseActionDialog,
  BaseInfoDialog,
  ConfirmDialog,
  DataTableLooseSelectionDialog,
  DeleteDialog,
  FirstLoginDialog,
  PrivacyPolicyTermsOfUseApprovalDialog,
  KeywordPasteDialog,
];
const directives = [
  AutoScrollIntoViewDirective,
  AutoFocusDirective,
  FocusFirstElementDirective,
  AlternativeButtonDirective,
  MatButtonThemeDirective,
  RoundButtonDirective,
  DataTestDirective,
  ValidationDirective,
  ShortcutDirective,
  ButtonSpinnerDirective,
  TooltipOnEllipsisDirective,
  TooltipDirective,
  SpinnerDirective,
  FileDropZoneDirective,
  FormControlElementRefDirective,
  DataTableAnchorDynamicInjectionDirective,
  OverlayDirective,
];
const pipes = [
  ...isPipes,
  ...otherPipes,
];

@NgModule({
  declarations: [
    ...routables,
    ...directives,
    ...containers,
    ...presentationals,
    ...dialogs,
    ...pipes,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    SolidifyFrontendMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild({}),
    // TourMatMenuModule.forRoot(),
  ],
  exports: [
    ...routables,
    ...directives,
    ...containers,
    ...presentationals,
    ...dialogs,
    ...pipes,
    CommonModule,
    FontAwesomeModule,
    SolidifyFrontendMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgxsModule,
    TranslateModule,
    // TourMatMenuModule
  ],
  providers: [
    {
      provide: MAT_MENU_DEFAULT_OPTIONS,
      useValue: {
        overlayPanelClass: "solidify-menu",
      } as MatMenuDefaultOptions,
    },
  ],
})
export class SolidifyFrontendCoreModule {
}
