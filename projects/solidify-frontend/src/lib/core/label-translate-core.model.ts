/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - label-translate-core.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MARK_AS_TRANSLATABLE} from "../core-resources/utils/translate.util";

export interface LabelSolidifyCore {
  coreAboutCommitDate?: string;
  coreAboutModule?: string;
  coreAboutUrl?: string;
  coreAboutVersion?: string;
  coreAdditionalInformation?: string;
  coreAboutDashboard?: string;
  coreBreadcrumbHome?: string;
  coreCancel?: string;
  coreChangelog?: string;
  coreCharacters?: string;
  coreCitationCopyToClipboard?: string;
  coreCitationStyles?: string;
  coreClose?: string;
  coreCookieConsentAllowTheCookie?: string;
  coreCookieConsentBannerAllowCookies?: string;
  coreCookieConsentBannerDecline?: string;
  coreCookieConsentBannerLearnMore?: string;
  coreCookieConsentBannerPersonalize?: string;
  coreCookieConsentBannerThisWebsiteUsesCookiesToEnsureYouGetTheBestExperienceOnOurWebsite?: string;
  coreCookieConsentBannerNewCookiesAreAvailable?: string;
  coreCookieConsentBannerNewCookieIsAvailable?: string;
  coreCookieConsentSidebarDownloadTokenDescription?: string;
  coreCookieConsentSidebarDownloadTokenTitle?: string;
  coreCookieConsentSidebarAllowAllCookies?: string;
  coreCookieConsentSidebarDarkModeDescription?: string;
  coreCookieConsentSidebarDarkModeTitle?: string;
  coreCookieConsentSidebarGoogleAnalyticsDescription?: string;
  coreCookieConsentSidebarGoogleAnalyticsTitle?: string;
  coreCookieConsentSidebarLanguageDescription?: string;
  coreCookieConsentSidebarLanguageTitle?: string;
  coreCookieConsentSidebarPersonalizeCookieDescription?: string;
  coreCookieConsentSidebarPersonalizeCookieTitle?: string;
  coreCookieConsentSidebarCookiesAvailableDescription?: string;
  coreCookieConsentSidebarCookiesAvailableTitle?: string;
  coreCookieConsentSidebarPrivacyPolicyAndTermsOfUseAcceptedDescription?: string;
  coreCookieConsentSidebarPrivacyPolicyAndTermsOfUseAcceptedTitle?: string;
  coreCookieConsentSidebarTitle?: string;
  coreCookieConsentSidebarTooltipClose?: string;
  coreCookieConsentSidebarIgnoreGlobalBannerDescription?: string;
  coreCookieConsentSidebarIgnoreGlobalBannerTitle?: string;
  coreCookieConsentNotificationCookieNameCopiedToClipboard?: string;
  coreCookieConsentNotificationCookieXEnabled?: string;
  coreCookieConsentNotificationFeatureDisabledBecauseCookieDeclined?: string;
  coreCookieConsentNotificationUnableToCopyCookieNameToClipboard?: string;
  coreCopyToClipboard?: string;
  coreCeleteDialogConfirmDeleteAction?: string;
  coreDoNotSeparate?: string;
  coreFileDownloadFail?: string;
  coreFileDownloadForbidden?: string;
  coreFileDownloadStart?: string;
  coreFileDownloadDownloadedSuccessfully?: string;
  coreFirstLoginClose?: string;
  coreFirstLoginContent?: string;
  coreFirstLoginNext?: string;
  coreFirstLoginTitle?: string;
  coreIncludesSeparatorCharacters?: string;
  coreLanguageSelectorSelectLanguage?: string;
  coreMaintenanceInProgress?: string;
  coreNoCitationsAreAvailable?: string;
  coreNotificationCopiedToClipboard?: string;
  coreNotificationHttpBadRequestToTranslate?: string;
  coreNotificationHttpForbiddenToTranslate?: string;
  coreNotificationHttpInternalErrorToTranslate?: string;
  coreNotificationHttpInvalidTokenToTranslate?: string;
  coreNotificationHttpNotFoundToTranslate?: string;
  coreNotificationHttpOfflineToTranslate?: string;
  coreNotificationHttpUnauthorizedToTranslate?: string;
  coreNotificationIdCopyToClipboard?: string;
  coreNotificationUnsavedChanges?: string;
  coreNotificationUploadCancelled?: string;
  coreNotificationUploadFail?: string;
  coreNotificationUploadFileSizeLimitXExceeded?: string;
  coreNotificationUploadInProgress?: string;
  coreNotificationUploadSuccess?: string;
  coreOrcidAuthenticated?: string;
  coreOrcidNotAuthenticated?: string;
  coreOrcidRedirectTitle?: string;
  coreOrcidRedirectSubtitle?: string;
  coreOrcidSignIdButtonText?: string;
  coreOrcidSignIdButtonHint?: string;
  corePaginatorFirstPage?: string;
  corePaginatorItemPerPage?: string;
  corePaginatorLastPage?: string;
  corePaginatorNextPage?: string;
  corePaginatorOfLabel?: string;
  corePaginatorPreviousPage?: string;
  corePrivacyPolicyTermsOfUseApprovalDialogClose?: string;
  corePrivacyPolicyTermsOfUseApprovalDialogConfirm?: string;
  corePrivacyPolicyTermsOfUseApprovalDialogIAcceptPrivacyPolicy?: string;
  corePrivacyPolicyTermsOfUseApprovalDialogIAcceptTermsOfUse?: string;
  corePrivacyPolicyTermsOfUseApprovalDialogMessage?: string;
  corePrivacyPolicyTermsOfUseApprovalDialogTitle?: string;
  coreRefresh?: string;
  coreReleaseNotes?: string;
  coreReviews?: string;
  coreSeparate?: string;
  coreSeparatorCharacterDetected?: string;
  coreServerOffline?: string;
  coreStatus?: string;
  coreTheFollowingPastedValue?: string;
  coreThemeSelector?: string;
  coreUnableToLoadApp?: string;
  coreUnaccessibleTab?: string;
  coreValidationErrorInvalidToTranslate?: string;
  coreValidationErrorRequiredToTranslate?: string;
  coreWouldYouLikeToSeparateTheEnteredValueUsingTheFollowingSeparator?: string;
  coreYes?: string;
  coreYesImSure?: string;
}

export const labelSolidifyCore: LabelSolidifyCore = {
  coreAboutCommitDate: MARK_AS_TRANSLATABLE("solidify.core.about.commitDate"),
  coreAboutModule: MARK_AS_TRANSLATABLE("solidify.core.about.module"),
  coreAboutUrl: MARK_AS_TRANSLATABLE("solidify.core.about.url"),
  coreAboutVersion: MARK_AS_TRANSLATABLE("solidify.core.about.version"),
  coreAdditionalInformation: MARK_AS_TRANSLATABLE("solidify.core.additionalInformation"),
  coreAboutDashboard: MARK_AS_TRANSLATABLE("solidify.core.coreAboutDashboard"),
  coreBreadcrumbHome: MARK_AS_TRANSLATABLE("solidify.core.breadcrumbHome"),
  coreCancel: MARK_AS_TRANSLATABLE("solidify.core.cancel"),
  coreChangelog: MARK_AS_TRANSLATABLE("solidify.core.changelog"),
  coreCharacters: MARK_AS_TRANSLATABLE("solidify.core.characters"),
  coreCitationCopyToClipboard: MARK_AS_TRANSLATABLE("solidify.core.citationCopyToClipboard"),
  coreCitationStyles: MARK_AS_TRANSLATABLE("solidify.core.citationStyles"),
  coreClose: MARK_AS_TRANSLATABLE("solidify.core.close"),
  coreCookieConsentAllowTheCookie: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.allowTheCookie"),
  coreCookieConsentBannerAllowCookies: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.banner.allowCookies"),
  coreCookieConsentBannerDecline: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.banner.decline"),
  coreCookieConsentBannerLearnMore: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.banner.learnMore"),
  coreCookieConsentBannerPersonalize: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.banner.personalize"),
  coreCookieConsentBannerThisWebsiteUsesCookiesToEnsureYouGetTheBestExperienceOnOurWebsite: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.banner.thisWebsiteUsesCookiesToEnsureYouGetTheBestExperienceOnOurWebsite"),
  coreCookieConsentBannerNewCookiesAreAvailable: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.banner.newCookiesAreAvailable"),
  coreCookieConsentBannerNewCookieIsAvailable: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.banner.newCookieIsAvailable"),
  coreCookieConsentSidebarDownloadTokenDescription: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.downloadToken.description"),
  coreCookieConsentSidebarDownloadTokenTitle: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.downloadToken.title"),
  coreCookieConsentSidebarAllowAllCookies: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.allowAllCookies"),
  coreCookieConsentSidebarDarkModeDescription: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.darkMode.description"),
  coreCookieConsentSidebarDarkModeTitle: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.darkMode.title"),
  coreCookieConsentSidebarGoogleAnalyticsDescription: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.googleAnalytics.description"),
  coreCookieConsentSidebarGoogleAnalyticsTitle: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.googleAnalytics.title"),
  coreCookieConsentSidebarLanguageDescription: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.language.description"),
  coreCookieConsentSidebarLanguageTitle: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.language.title"),
  coreCookieConsentSidebarPersonalizeCookieDescription: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.personalizeCookie.description"),
  coreCookieConsentSidebarPersonalizeCookieTitle: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.personalizeCookie.title"),
  coreCookieConsentSidebarCookiesAvailableDescription: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.cookiesAvailable.description"),
  coreCookieConsentSidebarCookiesAvailableTitle: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.cookiesAvailable.title"),
  coreCookieConsentSidebarPrivacyPolicyAndTermsOfUseAcceptedDescription: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.privacyPolicyAndTermsOfUseAccepted.description"),
  coreCookieConsentSidebarPrivacyPolicyAndTermsOfUseAcceptedTitle: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.privacyPolicyAndTermsOfUseAccepted.title"),
  coreCookieConsentSidebarTitle: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.title"),
  coreCookieConsentSidebarTooltipClose: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.tooltipClose"),
  coreCookieConsentSidebarIgnoreGlobalBannerDescription: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.ignoreGlobalBanner.description"),
  coreCookieConsentSidebarIgnoreGlobalBannerTitle: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.sidebar.ignoreGlobalBanner.title"),
  coreCookieConsentNotificationCookieNameCopiedToClipboard: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.notification.cookieNameCopiedToClipboard"),
  coreCookieConsentNotificationCookieXEnabled: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.notification.cookieXEnabled"),
  coreCookieConsentNotificationFeatureDisabledBecauseCookieDeclined: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.notification.featureDisabledBecauseCookieDeclined"),
  coreCookieConsentNotificationUnableToCopyCookieNameToClipboard: MARK_AS_TRANSLATABLE("solidify.core.cookieConsent.notification.unableToCopyCookieNameToClipboard"),
  coreCopyToClipboard: MARK_AS_TRANSLATABLE("solidify.core.copyToClipboard"),
  coreCeleteDialogConfirmDeleteAction: MARK_AS_TRANSLATABLE("solidify.core.deleteDialogConfirmDeleteAction"),
  coreDoNotSeparate: MARK_AS_TRANSLATABLE("solidify.core.doNotSeparate"),
  coreFileDownloadFail: MARK_AS_TRANSLATABLE("solidify.core.fileDownload.fail"),
  coreFileDownloadForbidden: MARK_AS_TRANSLATABLE("solidify.core.fileDownload.forbidden"),
  coreFileDownloadStart: MARK_AS_TRANSLATABLE("solidify.core.fileDownload.start"),
  coreFileDownloadDownloadedSuccessfully: MARK_AS_TRANSLATABLE("solidify.core.fileDownload.downloadedSuccessfully"),
  coreFirstLoginClose: MARK_AS_TRANSLATABLE("solidify.core.firstLogin.close"),
  coreFirstLoginContent: MARK_AS_TRANSLATABLE("solidify.core.firstLogin.content"),
  coreFirstLoginNext: MARK_AS_TRANSLATABLE("solidify.core.firstLogin.next"),
  coreFirstLoginTitle: MARK_AS_TRANSLATABLE("solidify.core.firstLogin.title"),
  coreIncludesSeparatorCharacters: MARK_AS_TRANSLATABLE("solidify.core.includesSeparatorCharacters"),
  coreLanguageSelectorSelectLanguage: MARK_AS_TRANSLATABLE("solidify.core.languageSelectorSelectLanguage"),
  coreMaintenanceInProgress: MARK_AS_TRANSLATABLE("solidify.core.maintenanceInProgress"),
  coreNoCitationsAreAvailable: MARK_AS_TRANSLATABLE("solidify.core.noCitationsAreAvailable"),
  coreNotificationCopiedToClipboard: MARK_AS_TRANSLATABLE("solidify.core.notification.copiedToClipboard"),
  coreNotificationHttpBadRequestToTranslate: MARK_AS_TRANSLATABLE("solidify.core.notification.http.badRequestToTranslate"),
  coreNotificationHttpForbiddenToTranslate: MARK_AS_TRANSLATABLE("solidify.core.notification.http.forbiddenToTranslate"),
  coreNotificationHttpInternalErrorToTranslate: MARK_AS_TRANSLATABLE("solidify.core.notification.http.internalErrorToTranslate"),
  coreNotificationHttpInvalidTokenToTranslate: MARK_AS_TRANSLATABLE("solidify.core.notification.http.invalidTokenToTranslate"),
  coreNotificationHttpNotFoundToTranslate: MARK_AS_TRANSLATABLE("solidify.core.notification.http.notFoundToTranslate"),
  coreNotificationHttpOfflineToTranslate: MARK_AS_TRANSLATABLE("solidify.core.notification.http.offlineToTranslate"),
  coreNotificationHttpUnauthorizedToTranslate: MARK_AS_TRANSLATABLE("solidify.core.notification.http.unauthorizedToTranslate"),
  coreNotificationIdCopyToClipboard: MARK_AS_TRANSLATABLE("solidify.core.notification.idCopyToClipboard"),
  coreNotificationUnsavedChanges: MARK_AS_TRANSLATABLE("solidify.core.notification.unsavedChanges"),
  coreNotificationUploadCancelled: MARK_AS_TRANSLATABLE("solidify.core.notification.upload.cancelled"),
  coreNotificationUploadFail: MARK_AS_TRANSLATABLE("solidify.core.notification.upload.fail"),
  coreNotificationUploadFileSizeLimitXExceeded: MARK_AS_TRANSLATABLE("solidify.core.notification.upload.fileSizeLimitXExceeded"),
  coreNotificationUploadInProgress: MARK_AS_TRANSLATABLE("solidify.core.notification.upload.inProgress"),
  coreNotificationUploadSuccess: MARK_AS_TRANSLATABLE("solidify.core.notification.upload.success"),
  coreOrcidAuthenticated: MARK_AS_TRANSLATABLE("solidify.core.orcidAuthenticated"),
  coreOrcidNotAuthenticated: MARK_AS_TRANSLATABLE("solidify.core.orcidNotAuthenticated"),
  coreOrcidRedirectTitle: MARK_AS_TRANSLATABLE("solidify.core.orcidRedirect.title"),
  coreOrcidRedirectSubtitle: MARK_AS_TRANSLATABLE("solidify.core.orcidRedirect.subtitle"),
  coreOrcidSignIdButtonText: MARK_AS_TRANSLATABLE("solidify.core.orcidSignIdButton.text"),
  coreOrcidSignIdButtonHint: MARK_AS_TRANSLATABLE("solidify.core.orcidSignIdButton.hint"),
  corePaginatorFirstPage: MARK_AS_TRANSLATABLE("solidify.core.paginator.firstPage"),
  corePaginatorItemPerPage: MARK_AS_TRANSLATABLE("solidify.core.paginator.itemPerPage"),
  corePaginatorLastPage: MARK_AS_TRANSLATABLE("solidify.core.paginator.lastPage"),
  corePaginatorNextPage: MARK_AS_TRANSLATABLE("solidify.core.paginator.nextPage"),
  corePaginatorOfLabel: MARK_AS_TRANSLATABLE("solidify.core.paginator.ofLabel"),
  corePaginatorPreviousPage: MARK_AS_TRANSLATABLE("solidify.core.paginator.previousPage"),
  corePrivacyPolicyTermsOfUseApprovalDialogClose: MARK_AS_TRANSLATABLE("solidify.core.privacyPolicyTermsOfUseApproval.dialog.close"),
  corePrivacyPolicyTermsOfUseApprovalDialogConfirm: MARK_AS_TRANSLATABLE("solidify.core.privacyPolicyTermsOfUseApproval.dialog.confirm"),
  corePrivacyPolicyTermsOfUseApprovalDialogIAcceptPrivacyPolicy: MARK_AS_TRANSLATABLE("solidify.core.privacyPolicyTermsOfUseApproval.dialog.iAcceptPrivacyPolicy"),
  corePrivacyPolicyTermsOfUseApprovalDialogIAcceptTermsOfUse: MARK_AS_TRANSLATABLE("solidify.core.privacyPolicyTermsOfUseApproval.dialog.iAcceptTermsOfUse"),
  corePrivacyPolicyTermsOfUseApprovalDialogMessage: MARK_AS_TRANSLATABLE("solidify.core.privacyPolicyTermsOfUseApproval.dialog.message"),
  corePrivacyPolicyTermsOfUseApprovalDialogTitle: MARK_AS_TRANSLATABLE("solidify.core.privacyPolicyTermsOfUseApproval.dialog.title"),
  coreRefresh: MARK_AS_TRANSLATABLE("solidify.core.refresh"),
  coreReleaseNotes: MARK_AS_TRANSLATABLE("solidify.core.releaseNotes"),
  coreReviews: MARK_AS_TRANSLATABLE("solidify.core.reviews"),
  coreSeparate: MARK_AS_TRANSLATABLE("solidify.core.separate"),
  coreSeparatorCharacterDetected: MARK_AS_TRANSLATABLE("solidify.core.separatorCharacterDetected"),
  coreServerOffline: MARK_AS_TRANSLATABLE("solidify.core.serverOffline"),
  coreStatus: MARK_AS_TRANSLATABLE("solidify.core.status"),
  coreTheFollowingPastedValue: MARK_AS_TRANSLATABLE("solidify.core.theFollowingPastedValue"),
  coreThemeSelector: MARK_AS_TRANSLATABLE("solidify.core.themeSelector"),
  coreUnableToLoadApp: MARK_AS_TRANSLATABLE("solidify.core.unableToLoadApp"),
  coreUnaccessibleTab: MARK_AS_TRANSLATABLE("solidify.core.unaccessibleTab"),
  coreValidationErrorInvalidToTranslate: MARK_AS_TRANSLATABLE("solidify.core.validationError.invalidToTranslate"),
  coreValidationErrorRequiredToTranslate: MARK_AS_TRANSLATABLE("solidify.core.validationError.requiredToTranslate"),
  coreWouldYouLikeToSeparateTheEnteredValueUsingTheFollowingSeparator: MARK_AS_TRANSLATABLE("solidify.core.wouldYouLikeToSeparateTheEnteredValueUsingTheFollowingSeparator"),
  coreYes: MARK_AS_TRANSLATABLE("solidify.core.yes"),
  coreYesImSure: MARK_AS_TRANSLATABLE("solidify.core.yesImSure"),
};
