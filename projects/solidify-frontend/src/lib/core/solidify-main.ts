/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-main.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {enableProdMode} from "@angular/core";
import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";

import {DefaultSolidifyEnvironment} from "./environments/environment.solidify-defaults";
import {SolidifyFrontendAbstractAppModule} from "./solidify-frontend-abstract-app.module";
import {Type} from "../core-resources/tools/is/is.tool";

declare const readyToBootstrap: Promise<string>;

export const bootstrapSolidifyApp: (appModule: Type<SolidifyFrontendAbstractAppModule>, environment: DefaultSolidifyEnvironment) => void
  = (appModule, environment) => {
  if (environment.production) {
    enableProdMode();
  }

  readyToBootstrap.then(baseHref => {
    // eslint-disable-next-line no-console
    console.log("Bootstrap app with base href '" + baseHref + "'");
    environment.baseHref = baseHref;
    platformBrowserDynamic().bootstrapModule(appModule)
      // eslint-disable-next-line no-console
      .catch(err => console.error(err));
  });
};
