/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - user-application-role.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


export type UserApplicationRoleEnum =
  "ROOT"
  | "ADMIN"
  | "USER"
  | "TRUSTED_CLIENT"
  | "GUEST";
// eslint-disable-next-line @typescript-eslint/naming-convention, no-underscore-dangle, id-blacklist, id-match
export const UserApplicationRoleEnum = {
  root: "ROOT" as UserApplicationRoleEnum,
  admin: "ADMIN" as UserApplicationRoleEnum,
  user: "USER" as UserApplicationRoleEnum,
  trusted_client: "TRUSTED_CLIENT" as UserApplicationRoleEnum,
  guest: "GUEST" as UserApplicationRoleEnum,
};
