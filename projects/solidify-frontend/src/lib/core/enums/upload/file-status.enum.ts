/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - file-status.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

export type FileStatusEnum =
  "RECEIVED"
  | "TO_PROCESS"
  | "PROCESSED"
  | "READY"
  | "CLEANING"
  | "IN_ERROR"
  | "TO_BE_CONFIRMED"
  | "CONFIRMED"
  | "COMPLETED";
// eslint-disable-next-line @typescript-eslint/naming-convention, no-underscore-dangle, id-blacklist, id-match
export const FileStatusEnum = {
  RECEIVED: "RECEIVED" as FileStatusEnum,
  TO_PROCESS: "TO_PROCESS" as FileStatusEnum,
  PROCESSED: "PROCESSED" as FileStatusEnum,
  READY: "READY" as FileStatusEnum,
  CLEANING: "CLEANING" as FileStatusEnum,
  IN_ERROR: "IN_ERROR" as FileStatusEnum,
  TO_BE_CONFIRMED: "TO_BE_CONFIRMED" as FileStatusEnum,
  CONFIRMED: "CONFIRMED" as FileStatusEnum,
  COMPLETED: "COMPLETED" as FileStatusEnum,
};
