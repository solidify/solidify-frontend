/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - data-table-component-input-value-type.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

export enum ValueType {
  /**
   * Use row data
   */
  isRowData = 1,
  /**
   * Use attribute in row data
   *
   * @deprecated use isColData instead
   */
  isOtherColData,
  /**
   * Use DataTableColumns definition
   */
  isCol,
  /**
   * Use `field` attribute value in DataTableColumns
   */
  isField,
  /**
   * Use attribute in DataTableColumns
   */
  isAttributeOnCol,
  /**
   * Use `field` attribute in DataTableColumns to get row data (support nested attribute ex: field='user.resId')
   */
  isColData,
  /**
   * Use `resId` attribute in row data
   */
  isResId,
  /**
   * Use static value defined in 'staticValue' in DataTableComponentInput
   */
  isStatic,
  /**
   * Use callback defined in 'callbackValue' in DataTableComponentInput
   */
  isCallback,
  /**
   * Use callback defined in 'callbackValue' in DataTableColumn
   */
  isColCallback,
}
