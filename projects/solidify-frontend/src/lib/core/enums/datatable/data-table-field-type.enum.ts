/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - data-table-field-type.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


/* eslint-disable id-blacklist */
export enum DataTableFieldTypeEnum {
  /**
   * Display the data in string format
   * Filter input is available as free text input
   */
  string = 0,
  /**
   * Display the data in date format by using DateUtil.convertDateToDateString (ex: 01.01.2023)
   * Filter input is not available
   */
  date,
  /**
   * Display the data in date time format by using DateUtil.convertDateToDateTimeString (ex: 01.01.2023 15:24:03)
   * Filter input is not available
   */
  datetime,
  /**
   * Display in numeric format to the right of the cell
   * Filter input is available as number input
   */
  number,
  /**
   * Display data retrieve with single searchable select component
   * Filter input is available as searchable single select
   */
  searchableSingleSelect,
  /**
   * Data can be translated using "filterEnum"
   * Filter input is available and as single select using "filterEnum" list values
   */
  singleSelect,
  /**
   * Apply pipe filesize to the data
   * Display to the right of the cell
   */
  size,
  /**
   * Display icon trueValue or falseValue if data is true of false
   * Filter input is available as single select
   */
  boolean,
  /**
   * Display list item one next the other using string join with separator ', '
   */
  listToJoinString,
  /**
   * Display list item one below the other using ul li
   */
  list,
  /**
   * Apply innerHTML to data
   * Filter input is available as free text input
   */
  innerHtml,
  /**
   * Display an action button
   * Filter input is available as free text input
   */
  actionButton,
}
