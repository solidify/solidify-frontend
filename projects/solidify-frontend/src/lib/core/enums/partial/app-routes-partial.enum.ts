/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - app-routes-partial.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

export enum AppRoutesPartialEnum {
  root = "",
  index = "index",
  icons = "icons",
  redirect = "redirect",
  components = "components",
  colorCheck = "color-check",
  maintenance = "maintenance",
  about = "about",
  releaseNotes = "release-notes",
  changelog = "changelog",
  serverOffline = "server-offline",
  orcidRedirect = "orcid-redirect",
  unableToLoadApp = "unable-to-load-application",
  login = "login",
  home = "home",
  separator = "/",
  paramId = ":id",
  paramIdWithoutPrefixParam = "id",
  paramIdParentComposition = ":parent-composition-id",
  paramIdParentCompositionWithoutPrefixParam = "parent-composition-id",
  edit = "edit",

  admin = "admin",

  oaiMetadataPrefix = "oai-metadata-prefix",
  oaiMetadataPrefixDetail = "detail",
  oaiMetadataPrefixCreate = "create",
  oaiMetadataPrefixEdit = "edit",

  oaiSet = "oai-set",
  oaiSetDetail = "detail",
  oaiSetCreate = "create",
  oaiSetBulkCreate = "bulk-create",
  oaiSetEdit = "edit",

  indexFieldAlias = "index-field-alias",
  indexFieldAliasDetail = "detail",
  indexFieldAliasCreate = "create",
  indexFieldAliasEdit = "edit",

  globalBanner = "global-banner",
  globalBannerDetail = "detail",
  globalBannerCreate = "create",
  globalBannerEdit = "edit",
}
