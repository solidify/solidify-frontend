/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - api-resource-name-partial.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

export enum ApiResourceNamePartialEnum {
  HISTORY = "history",
  OAI = "oai",
  OAI_PROVIDER = "oai-provider",
  OAI_SETS = "oai-sets",
  OAI_METADATA_PREFIXES = "oai-metadata-prefixes",
  ORCID = "orcid",
  SYSTEM_PROPERTY = "system-properties",
  INDEX_FIELD_ALIASES = "index-field-aliases",
  LANGUAGE = "languages",
  GLOBAL_BANNERS = "global-banners",
}
