/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - icon-name-partial.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

// @dynamic
export abstract class IconNamePartialEnum {
  static readonly internalLink: string = "internalLink";
  static readonly closeChip: string = "closeChip";
  static readonly close: string = "close";
  static readonly redo: string = "redo";
  static readonly defaultValue: string = "defaultValue";
  static readonly externalLink: string = "externalLink";
  static readonly clear: string = "clear";
  static readonly sortUndefined: string = "sortUndefined";
  static readonly sortAscending: string = "sortAscending";
  static readonly sortDescending: string = "sortDescending";
  static readonly trueValue: string = "trueValue";
  static readonly falseValue: string = "falseValue";
  static readonly refresh: string = "refresh";
  static readonly resId: string = "resId";
  static readonly more: string = "more";
  static readonly copyToClipboard: string = "copyToClipboard";
  static readonly offline: string = "offline";
  static readonly maintenance: string = "maintenance";
  static readonly down: string = "down";
  static readonly send: string = "send";
  static readonly search: string = "search";
  static readonly star: string = "star";
  static readonly starEmpty: string = "starEmpty";
  static readonly success: string = "success";
  static readonly information: string = "information";
  static readonly dashboard: string = "dashboard";
  static readonly warning: string = "warning";
  static readonly error: string = "error";
  static readonly history: string = "history";
  static readonly back: string = "back";
  static readonly create: string = "create";
  static readonly delete: string = "delete";
  static readonly save: string = "save";
  static readonly edit: string = "edit";
  static readonly downArrow: string = "downArrow";
  static readonly zoomOut: string = "zoomOut";
  static readonly zoomIn: string = "zoomIn";
  static readonly unableToLoadApp: string = "unableToLoadApp";
  static readonly orcid: string = "orcid";
  static readonly orcidText: string = "orcidText";
  static readonly dragToSort: string = "dragToSort";
  static readonly oaiSets: string = "oaiSets";
  static readonly oaiMetadataPrefixes: string = "oaiMetadataPrefixes";
  static readonly uploadImage: string = "uploadImage";
  static readonly fingerprint: string = "fingerprint";
  static readonly http: string = "http";
  static readonly excludedFacet: string = "excludedFacet";
  static readonly uploadFile: string = "uploadFile";
  static readonly reset: string = "reset";
  static readonly preview: string = "preview";
  static readonly files: string = "files";
  static readonly download: string = "download";
  static readonly notFound: string = "notFound";
  static readonly home: string = "home";
  static readonly logInMfa: string = "logInMfa";
  static readonly collapseAll: string = "collapseAll";
  static readonly expandAll: string = "expandAll";
  static readonly folderEmpty: string = "folderEmpty";
  static readonly folderOpened: string = "folderOpened";
  static readonly folderClosed: string = "folderClosed";
  static readonly indexFieldAliases: string = "indexFieldAliases";
  static readonly add: string = "add";
  static readonly globalBanners: string = "globalBanners";
  static readonly correct: string = "correct";
  static readonly incorrect: string = "incorrect";
  static readonly help: string = "help";
}
