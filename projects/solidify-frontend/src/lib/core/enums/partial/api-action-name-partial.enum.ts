/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - api-action-name-partial.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


export enum ApiActionNamePartialEnum {
  ALL = "All",
  CLOSE = "close",
  COUNT = "count",
  CREATE = "new",
  CREATION = "creation.when",
  DASHBOARD = "dashboard",
  DELETE = "delete",
  DELETE_CACHE = "delete-cache",
  DOWNLOAD = "download",
  DOWNLOAD_TOKEN = "download-token",
  ERROR = "error",
  FILE = "file",
  HISTORY = "history",
  IMPORT = "import",
  IMPORTED = "imported",
  LAST_CREATED = "lastCreated",
  LAST_UPDATED = "lastUpdated",
  LIST = "list",
  MODULE = "module",
  INFO = "info",
  NEXT = "next",
  PAGE = "page",
  PARENT = "parent",
  PREVIOUS = "previous",
  RANGE = "searchRange",
  READ = "get",
  RESOURCES = "resources",
  RESUME = "resume",
  SAVE = "save",
  SEARCH = "search",
  SELF = "self",
  SIZE = "size",
  SORT = "sort",
  START = "start",
  STATUS = "status",
  UPLOAD = "upload",
  UPLOAD_ARCHIVE = "upload-archive",
  UPDATE = "edit",
  UPDATED = "lastUpdate.when",
  VALUES = "values",
  VIEW = "view",
  UPLOAD_LOGO = "upload-logo",
  DOWNLOAD_LOGO = "download-logo",
  DELETE_LOGO = "delete-logo",
  UPLOAD_AVATAR = "upload-avatar",
  DOWNLOAD_AVATAR = "download-avatar",
  DELETE_AVATAR = "delete-avatar",
  UPLOAD_THUMBNAIL = "upload-thumbnail",
  DOWNLOAD_THUMBNAIL = "download-thumbnail",
  DELETE_THUMBNAIL = "delete-thumbnail",
  UPLOAD_DUA = "upload-dua",
  DOWNLOAD_DUA = "download-dua",
  DELETE_DUA = "delete-dua"
}
