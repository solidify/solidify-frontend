/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - resource-namespace.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {BaseResourceStoreNameSpace} from "../../../models/stores/base-resource-store-namespace.model";
import {StoreActionClass} from "../../../models/stores/state-action.model";

export interface ResourceNameSpace extends BaseResourceStoreNameSpace {
  LoadResource: StoreActionClass;
  LoadResourceSuccess: StoreActionClass;
  LoadResourceFail: StoreActionClass;
  ChangeQueryParameters: StoreActionClass;
  GetAll: StoreActionClass;
  GetAllSuccess: StoreActionClass;
  GetAllFail: StoreActionClass;
  GetByListId: StoreActionClass;
  GetByListIdSuccess: StoreActionClass;
  GetByListIdFail: StoreActionClass;
  GetById: StoreActionClass;
  GetByIdSuccess: StoreActionClass;
  GetByIdFail: StoreActionClass;
  Create: StoreActionClass;
  CreateSuccess: StoreActionClass;
  CreateFail: StoreActionClass;
  Update: StoreActionClass;
  UpdateSuccess: StoreActionClass;
  UpdateFail: StoreActionClass;
  Delete: StoreActionClass;
  DeleteSuccess: StoreActionClass;
  DeleteFail: StoreActionClass;
  DeleteList: StoreActionClass;
  DeleteListSuccess: StoreActionClass;
  DeleteListFail: StoreActionClass;
  AddInList: StoreActionClass;
  AddInListById: StoreActionClass;
  AddInListByIdSuccess: StoreActionClass;
  AddInListByIdFail: StoreActionClass;
  RemoveInListById: StoreActionClass;
  RemoveInListByListId: StoreActionClass;
  LoadNextChunkList: StoreActionClass;
  LoadNextChunkListSuccess: StoreActionClass;
  LoadNextChunkListFail: StoreActionClass;
  Clean: StoreActionClass;
}
