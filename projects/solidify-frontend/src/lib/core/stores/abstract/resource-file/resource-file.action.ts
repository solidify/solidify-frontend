/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - resource-file.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {SolidifyHttpErrorResponseModel} from "../../../models/errors/solidify-http-error-response.model";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
} from "../../../models/stores/base.action";
import {FileUploadWrapper} from "../../../models/upload/file-upload-wrapper.model";
import {ResourceFileNameSpace} from "./resource-file-namespace.model";

export namespace ResourceFileAction {
  export class GetFile extends BaseAction {
    static readonly type: string = `[{0}] Get File`;

    constructor(public resId: string, public downloadInMemory: boolean = false) {
      super();
    }
  }

  export class GetFileSuccess extends BaseSubActionSuccess<GetFile> {
    static readonly type: string = `[{0}] Get File Success`;

    constructor(public parentAction: GetFile, public blob: Blob) {
      super(parentAction);
    }
  }

  export class GetFileFail extends BaseSubActionFail<GetFile> {
    static readonly type: string = `[{0}] Get File Fail`;
  }

  export class GetFileByResId extends BaseAction {
    static readonly type: string = `[{0}] Get File By ResId`;

    constructor(public resId: string, public downloadInMemory: boolean = false) {
      super();
    }
  }

  export class GetFileByResIdSuccess extends BaseSubActionSuccess<GetFileByResId> {
    static readonly type: string = `[{0}] Get File By ResId Success`;

    constructor(public parentAction: GetFileByResId, public blob: Blob) {
      super(parentAction);
    }
  }

  export class GetFileByResIdFail extends BaseSubActionFail<GetFileByResId> {
    static readonly type: string = `[{0}] Get File By ResId Fail`;

    constructor(public parentAction: GetFileByResId) {
      super(parentAction);
    }
  }

  export class UploadFile extends BaseAction {
    static readonly type: string = `[{0}] Upload File`;

    constructor(public resId: string, public fileUploadWrapper: FileUploadWrapper) {
      super();
    }
  }

  export class UploadFileSuccess extends BaseSubActionSuccess<UploadFile> {
    static readonly type: string = `[{0}] Upload File Success`;

    constructor(public parentAction: UploadFile, public uploadBody?: any) {
      super(parentAction);
    }
  }

  export class UploadFileFail extends BaseSubActionFail<UploadFile> {
    static readonly type: string = `[{0}] Upload File Fail`;

    constructor(public parentAction: UploadFile, public error?: SolidifyHttpErrorResponseModel) {
      super(parentAction);
    }
  }

  export class DeleteFile extends BaseAction {
    static readonly type: string = `[{0}] Delete File`;

    constructor(public resId: string, public file: File) {
      super();
    }
  }

  export class DeleteFileSuccess extends BaseSubActionSuccess<DeleteFile> {
    static readonly type: string = `[{0}] Delete File Success`;

    constructor(public parentAction: DeleteFile) {
      super(parentAction);
    }
  }

  export class DeleteFileFail extends BaseSubActionFail<DeleteFile> {
    static readonly type: string = `[{0}] Delete File Fail`;
  }
}

export const resourceFileActionNameSpace: ResourceFileNameSpace | any = ResourceFileAction;
