/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - resource-file.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpEventType} from "@angular/common/http";
import {Inject} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  isFunction,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {MappingObjectUtil} from "../../../../core-resources/utils/mapping-object.util";
import {ObjectUtil} from "../../../../core-resources/utils/object.util";
import {StringUtil} from "../../../../core-resources/utils/string.util";
import {
  OverrideDefaultAction,
  RegisterDefaultAction,
} from "../../../decorators/store.decorator";
import {ApiActionNamePartialEnum} from "../../../enums/partial/api-action-name-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {ApiService} from "../../../http/api.service";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {BaseResourceAvatar} from "../../../models/dto/base-resource-avatar.model";
import {BaseResourceLogo} from "../../../models/dto/base-resource-logo.model";
import {BaseResourceThumbnail} from "../../../models/dto/base-resource-thumbnail.model";
import {BaseResourceWithFile} from "../../../models/dto/base-resource-with-logo.model";
import {BaseResource} from "../../../../core-resources/models/dto/base-resource.model";
import {SolidifyFile} from "../../../models/dto/solidify-file.model";
import {SolidifyHttpErrorResponseModel} from "../../../models/errors/solidify-http-error-response.model";
import {UploadEventModel} from "../../../models/http/upload-event.model";
import {BaseResourceStateModel} from "../../../models/stores/base-resource-state.model";
import {ActionSubActionCompletionsWrapper} from "../../../models/stores/base.action";
import {SolidifyStateContext} from "../../../models/stores/state-context.model";
import {DownloadService} from "../../../services/download.service";
import {NotificationService} from "../../../services/notification.service";
import {ofSolidifyActionCompleted} from "../../../utils/stores/store.tool";
import {ResourceAction} from "../resource/resource.action";
import {
  defaultResourceStateInitValue,
  ResourceState,
} from "../resource/resource.state";
import {ResourceFileActionHelper} from "./resource-file-action.helper";
import {ResourceFileNameSpace} from "./resource-file-namespace.model";
import {ResourceFileOptions} from "./resource-file-options.model";
import {ResourceFileStateModel} from "./resource-file-state.model";
import {ResourceFileAction} from "./resource-file.action";

export const defaultResourceFileStateInitValue: <TResourceType extends BaseResourceWithFile> () => ResourceFileStateModel<TResourceType> = () =>
  ({
    ...defaultResourceStateInitValue(),
    file: undefined,
    isLoadingFile: false,
    listFile: {},
    isLoadingAllFile: false,
  });

export abstract class ResourceFileState<TStateModel extends BaseResourceStateModel, TResource extends BaseResourceWithFile> extends ResourceState<TStateModel, TResource> {
  protected readonly _FILE_KEY: string = "file";

  protected declare readonly _nameSpace: ResourceFileNameSpace;
  protected declare readonly _optionsState: ResourceFileOptions;

  protected constructor(protected _apiService: ApiService,
                        protected _store: Store,
                        protected _notificationService: NotificationService,
                        protected _actions$: Actions,
                        protected _options: ResourceFileOptions,
                        protected _downloadService: DownloadService,
                        protected _mode: ResourceFileStateModeEnum,
                        @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_apiService, _store, _notificationService, _actions$, _environment, _options);
    this._optionsState = Object.assign(ResourceFileState._getDefaultOptions(), _options);
  }

  protected static _getDefaultOptions(): ResourceFileOptions | any {
    let defaultOptions: ResourceFileOptions | any = {
      downloadInMemory: false,
      fileNameAttribute: "fileName",
    } as ResourceFileOptions;
    defaultOptions = Object.assign(ResourceState._getDefaultOptions(), defaultOptions);
    return defaultOptions;
  }

  protected _getDownloadAction(): ApiActionNamePartialEnum {
    if (isNotNullNorUndefinedNorWhiteString(this._optionsState.resourceFileApiActionNameDownloadCustom)) {
      return this._optionsState.resourceFileApiActionNameDownloadCustom;
    }
    if (this._mode === ResourceFileStateModeEnum.logo) {
      return this._environment.resourceFileApiActionNameDownloadLogo;
    }
    if (this._mode === ResourceFileStateModeEnum.avatar) {
      return this._environment.resourceFileApiActionNameDownloadAvatar;
    }
    if (this._mode === ResourceFileStateModeEnum.thumbnail) {
      return this._environment.resourceFileApiActionNameDownloadThumbnail;
    }
    if (this._mode === ResourceFileStateModeEnum.custom) {
      throw new Error(`resourceFileApiActionNameDownloadCustom option is not defined for state ${this.stateName}`);
    }
  }

  protected _getUploadAction(): ApiActionNamePartialEnum {
    if (isNotNullNorUndefinedNorWhiteString(this._optionsState.resourceFileApiActionNameUploadCustom)) {
      return this._optionsState.resourceFileApiActionNameUploadCustom;
    }
    if (this._mode === ResourceFileStateModeEnum.logo) {
      return this._environment.resourceFileApiActionNameUploadLogo;
    }
    if (this._mode === ResourceFileStateModeEnum.avatar) {
      return this._environment.resourceFileApiActionNameUploadAvatar;
    }
    if (this._mode === ResourceFileStateModeEnum.thumbnail) {
      return this._environment.resourceFileApiActionNameUploadThumbnail;
    }
    if (this._mode === ResourceFileStateModeEnum.custom) {
      if (isNullOrUndefined(this._optionsState.customUploadUrlSuffix)) {
        throw new Error(`resourceFileApiActionNameUploadCustom option is not defined for state ${this.stateName}`);
      }
    }
  }

  protected _getDeleteAction(): ApiActionNamePartialEnum {
    if (isNotNullNorUndefinedNorWhiteString(this._optionsState.resourceFileApiActionNameDeleteCustom)) {
      return this._optionsState.resourceFileApiActionNameDeleteCustom;
    }
    if (this._mode === ResourceFileStateModeEnum.logo) {
      return this._environment.resourceFileApiActionNameDeleteLogo;
    }
    if (this._mode === ResourceFileStateModeEnum.avatar) {
      return this._environment.resourceFileApiActionNameDeleteAvatar;
    }
    if (this._mode === ResourceFileStateModeEnum.thumbnail) {
      return this._environment.resourceFileApiActionNameDeleteThumbnail;
    }
    if (this._mode === ResourceFileStateModeEnum.custom) {
      throw new Error(`resourceFileApiActionNameDeleteCustom option is not defined for state ${this.stateName}`);
    }
  }

  protected _getFile(model: TResource): SolidifyFile {
    if (isNotNullNorUndefinedNorWhiteString(this._optionsState.customFileAttribute)) {
      return model[this._optionsState.customFileAttribute];
    }
    if (this._mode === ResourceFileStateModeEnum.logo) {
      return (model as BaseResourceLogo)?.logo;
    }
    if (this._mode === ResourceFileStateModeEnum.avatar) {
      return (model as BaseResourceAvatar)?.avatar;
    }
    if (this._mode === ResourceFileStateModeEnum.thumbnail) {
      return (model as BaseResourceThumbnail)?.thumbnail;
    }
    if (this._mode === ResourceFileStateModeEnum.custom) {
      throw new Error(`customFileAttribute option is not defined for state ${this.stateName}`);
    }
  }

  protected abstract get _urlFileResource(): string;

  @RegisterDefaultAction((resourceFileNameSpace: ResourceFileNameSpace) => resourceFileNameSpace.GetFile, {}, ResourceState)
  getFile(ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>, action: ResourceFileAction.GetFile): Observable<Blob> {
    this._isLoadingFile(ctx, true);
    return this._downloadService.downloadInMemory(`${this._urlFileResource}/${action.resId}/${this._getDownloadAction()}`, this._getFileName(ctx), this._optionsState.downloadInMemory || action.downloadInMemory)
      .pipe(
        tap((data: Blob) => {
          ctx.dispatch(ResourceFileActionHelper.getFileSuccess(this._nameSpace, action, data));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(ResourceFileActionHelper.getFileFail(this._nameSpace, action));
          throw error;
        }),
      );
  }

  @RegisterDefaultAction((resourceFileNameSpace: ResourceFileNameSpace) => resourceFileNameSpace.GetFileSuccess, {}, ResourceState)
  getFileSuccess(ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>, action: ResourceFileAction.GetFileSuccess): void {
    ctx.patchState({
      isLoadingFile: false,
      file: isNullOrUndefined(action.blob) ? undefined : URL.createObjectURL(action.blob),
    });
  }

  @RegisterDefaultAction((resourceFileNameSpace: ResourceFileNameSpace) => resourceFileNameSpace.GetFileFail, {}, ResourceState)
  getFileFail(ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>, action: ResourceFileAction.GetFileFail): void {
    ctx.patchState({
      isLoadingFile: false,
      file: undefined,
    });
  }

  @RegisterDefaultAction((resourceFileNameSpace: ResourceFileNameSpace) => resourceFileNameSpace.GetFileByResId, {}, ResourceState)
  getFileByResId(ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>, action: ResourceFileAction.GetFileByResId): Observable<any> {
    ctx.patchState({
      isLoadingAllFile: true,
    });
    return this._downloadService.downloadInMemory(`${this._urlFileResource}/${action.resId}/${this._getDownloadAction()}`, this._getFileName(ctx), this._optionsState.downloadInMemory || action.downloadInMemory)
      .pipe(
        tap((data: Blob) => {
          ctx.dispatch(ResourceFileActionHelper.getFileByResIdSuccess(this._nameSpace, action, data));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(ResourceFileActionHelper.getFileByResIdFail(this._nameSpace, action));
          throw error;
        }),
      );
  }

  @RegisterDefaultAction((resourceFileNameSpace: ResourceFileNameSpace) => resourceFileNameSpace.GetFileByResIdSuccess, {}, ResourceState)
  getFileByResIdSuccess(ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>, action: ResourceFileAction.GetFileByResIdSuccess): void {
    const fileCopy = ObjectUtil.clone(ctx.getState().listFile);
    if (isNullOrUndefined(action.blob)) {
      MappingObjectUtil.set(fileCopy, action.parentAction.resId, StringUtil.stringEmpty);
    } else {
      MappingObjectUtil.set(fileCopy, action.parentAction.resId, URL.createObjectURL(action.blob));
    }
    ctx.patchState({
      isLoadingAllFile: false,
      listFile: fileCopy,
    });
  }

  @RegisterDefaultAction((resourceFileNameSpace: ResourceFileNameSpace) => resourceFileNameSpace.GetFileByResIdFail, {}, ResourceState)
  getFileByResIdFail(ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>, action: ResourceFileAction.GetFileByResIdFail): void {
    const fileCopy = ObjectUtil.clone(ctx.getState().listFile);
    MappingObjectUtil.set(fileCopy, action.parentAction.resId, StringUtil.stringEmpty);

    ctx.patchState({
      isLoadingAllFile: false,
      listFile: fileCopy,
    });
  }

  @RegisterDefaultAction((resourceFileNameSpace: ResourceFileNameSpace) => resourceFileNameSpace.UploadFile, {}, ResourceState)
  uploadFile(ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>, action: ResourceFileAction.UploadFile): Observable<any> {
    this._isLoadingFile(ctx, true);
    const formData = new FormData();
    formData.append(this._FILE_KEY, action.fileUploadWrapper.file, action.fileUploadWrapper.file.name);
    if (isFunction(this._optionsState.addExtraFormDataDuringUpload)) {
      this._optionsState.addExtraFormDataDuringUpload(formData, action);
    }

    let url = `${this._urlFileResource}/${action.resId}/${this._getUploadAction()}`;
    if (isFunction(this._optionsState.customUploadUrlSuffix)) {
      const suffix = this._optionsState.customUploadUrlSuffix(action);
      if (isNotNullNorUndefinedNorWhiteString(suffix)) {
        url = `${this._urlFileResource}/${suffix}`;
      }
    }

    return this._apiService.upload(url, formData)
      .pipe(
        map((event: UploadEventModel) => {
          switch (event.type) {
            case HttpEventType.DownloadProgress:
            case HttpEventType.UploadProgress:
              return;
            case HttpEventType.Response:
              if (isNotNullNorUndefined(event.body) &&
                (
                  (this._mode === ResourceFileStateModeEnum.avatar && isNotNullNorUndefined((event.body as BaseResourceAvatar).avatar)) ||
                  (this._mode === ResourceFileStateModeEnum.logo && isNotNullNorUndefined((event.body as BaseResourceLogo).logo)) ||
                  (this._mode === ResourceFileStateModeEnum.thumbnail && isNotNullNorUndefined((event.body as BaseResourceThumbnail).thumbnail)) ||
                  (this._mode === ResourceFileStateModeEnum.custom && isNotNullNorUndefined((event.body as BaseResource)[this._optionsState.customFileAttribute]))
                )
              ) {
                ctx.dispatch(ResourceFileActionHelper.uploadFileSuccess(this._nameSpace, action, event.body));
              } else {
                ctx.dispatch(ResourceFileActionHelper.uploadFileFail(this._nameSpace, action));
              }
              return;
            default:
              return;
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(ResourceFileActionHelper.uploadFileFail(this._nameSpace, action, error));
          throw error;
        }),
      );
  }

  @RegisterDefaultAction((resourceFileNameSpace: ResourceFileNameSpace) => resourceFileNameSpace.UploadFileSuccess, {}, ResourceState)
  uploadFileSuccess(ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>, action: ResourceFileAction.UploadFileSuccess): void {
    this._isLoadingFile(ctx, false);
  }

  @RegisterDefaultAction((resourceFileNameSpace: ResourceFileNameSpace) => resourceFileNameSpace.UploadFileFail, {}, ResourceState)
  uploadFileFail(ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>, action: ResourceFileAction.UploadFileFail): void {
    this._isLoadingFile(ctx, false);
  }

  protected _isLoadingFile(ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>, isLoading: boolean): void {
    ctx.patchState({
      isLoadingFile: isLoading,
    });
  }

  @RegisterDefaultAction((resourceFileNameSpace: ResourceFileNameSpace) => resourceFileNameSpace.DeleteFile, {}, ResourceState)
  deleteFile(ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>, action: ResourceFileAction.DeleteFile): Observable<any> {
    this._isLoadingFile(ctx, true);

    return this._apiService.delete(`${this._urlFileResource}/${action.resId}/${this._getDeleteAction()}`)
      .pipe(
        tap((result: any) => {
          ctx.dispatch(ResourceFileActionHelper.deleteFileSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(ResourceFileActionHelper.deleteFileSuccess(this._nameSpace, action));
          throw error;
        }),
      );
  }

  @RegisterDefaultAction((resourceFileNameSpace: ResourceFileNameSpace) => resourceFileNameSpace.DeleteFileSuccess, {}, ResourceState)
  deleteFileSuccess(ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>, action: ResourceFileAction.DeleteFileSuccess): void {
    ctx.patchState({
      isLoadingFile: false,
      file: undefined,
    });
  }

  @RegisterDefaultAction((resourceFileNameSpace: ResourceFileNameSpace) => resourceFileNameSpace.DeleteFileFail, {}, ResourceState)
  deleteFileFail(ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>, action: ResourceFileAction.DeleteFileFail): void {
    this._isLoadingFile(ctx, false);
  }

  @OverrideDefaultAction()
  @RegisterDefaultAction((resourceNameSpace: ResourceFileNameSpace) => resourceNameSpace.Clean)
  clean(ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>, action: ResourceAction.Clean): void {
    super.clean(ctx, action);
    ctx.patchState({
      file: undefined,
      isLoadingFile: false,
      listFile: {},
      isLoadingAllFile: false,
    });
  }

  protected override _getListActionsUpdateSubResource(model: TResource, action: ResourceAction.Create<TResource> | ResourceAction.Update<TResource>, ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>): ActionSubActionCompletionsWrapper[] {
    const actions = super._getListActionsUpdateSubResource(model, action, ctx);
    actions.push(...this._getActionUpdateFile(model, action, ctx));
    return actions;
  }

  protected _getActionUpdateFile(model: TResource, action: ResourceAction.Create<TResource> | ResourceAction.Update<TResource>, ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>): ActionSubActionCompletionsWrapper[] {
    return ResourceFileState.getActionUpdateFile(model.resId, action.modelFormControlEvent.model.newPendingFile, this._getFile(model), this._nameSpace, this._actions$);
  }

  static getActionUpdateFile(resourceId: string, newPendingFile: Blob | null | undefined, oldFile: SolidifyFile | null | undefined, nameSpace: ResourceFileNameSpace, _actions$: Actions): ActionSubActionCompletionsWrapper[] {
    const actions = [];

    const isFileToUpload = isNotNullNorUndefined(newPendingFile);
    const isFileToDelete = newPendingFile === null;
    if (isFileToUpload) {
      actions.push({
        action: ResourceFileActionHelper.uploadFile(nameSpace, resourceId, {
          file: newPendingFile as File,
        }),
        subActionCompletions: [
          _actions$.pipe(ofSolidifyActionCompleted(nameSpace.UploadFileSuccess)),
          _actions$.pipe(ofSolidifyActionCompleted(nameSpace.UploadFileFail)),
        ],
      });
    } else if (isFileToDelete) {
      if (isNotNullNorUndefined(oldFile)) {
        actions.push({
          action: ResourceFileActionHelper.deleteFile(nameSpace, resourceId, null),
          subActionCompletions: [
            _actions$.pipe(ofSolidifyActionCompleted(nameSpace.DeleteFileSuccess)),
            _actions$.pipe(ofSolidifyActionCompleted(nameSpace.DeleteFileFail)),
          ],
        });
      }
    }
    return actions;
  }

  protected _getFileName(ctx: SolidifyStateContext<ResourceFileStateModel<TResource>>): string {
    return ctx.getState().current?.[this._optionsState.customFileAttribute]?.[this._optionsState.fileNameAttribute];
  }
}

export enum ResourceFileStateModeEnum {
  avatar = "avatar",
  logo = "logo",
  thumbnail = "thumbnail",
  custom = "custom",
}
