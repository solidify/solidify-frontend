/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - resource-file-action.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {ResourceActionHelper} from "../resource/resource-action.helper";
import {ResourceFileNameSpace} from "./resource-file-namespace.model";
import {ResourceFileAction} from "./resource-file.action";

export class ResourceFileActionHelper extends ResourceActionHelper {
  static getFile(resourceFileNameSpace: ResourceFileNameSpace, ...args: ConstructorParameters<typeof ResourceFileAction.GetFile>): ResourceFileAction.GetFile {
    return new resourceFileNameSpace.GetFile(...args);
  }

  static getFileSuccess(resourceFileNameSpace: ResourceFileNameSpace, ...args: ConstructorParameters<typeof ResourceFileAction.GetFileSuccess>): ResourceFileAction.GetFileSuccess {
    return new resourceFileNameSpace.GetFileSuccess(...args);
  }

  static getFileFail(resourceFileNameSpace: ResourceFileNameSpace, ...args: ConstructorParameters<typeof ResourceFileAction.GetFileFail>): ResourceFileAction.GetFileFail {
    return new resourceFileNameSpace.GetFileFail(...args);
  }

  static getFileByResId(resourceFileNameSpace: ResourceFileNameSpace, ...args: ConstructorParameters<typeof ResourceFileAction.GetFileByResId>): ResourceFileAction.GetFileByResId {
    return new resourceFileNameSpace.GetFileByResId(...args);
  }

  static getFileByResIdSuccess(resourceFileNameSpace: ResourceFileNameSpace, ...args: ConstructorParameters<typeof ResourceFileAction.GetFileByResIdSuccess>): ResourceFileAction.GetFileByResIdSuccess {
    return new resourceFileNameSpace.GetFileByResIdSuccess(...args);
  }

  static getFileByResIdFail(resourceFileNameSpace: ResourceFileNameSpace, ...args: ConstructorParameters<typeof ResourceFileAction.GetFileByResIdFail>): ResourceFileAction.GetFileByResIdFail {
    return new resourceFileNameSpace.GetFileByResIdFail(...args);
  }

  static deleteFile(resourceFileNameSpace: ResourceFileNameSpace, ...args: ConstructorParameters<typeof ResourceFileAction.DeleteFile>): ResourceFileAction.DeleteFile {
    return new resourceFileNameSpace.DeleteFile(...args);
  }

  static deleteFileSuccess(resourceFileNameSpace: ResourceFileNameSpace, ...args: ConstructorParameters<typeof ResourceFileAction.DeleteFileSuccess>): ResourceFileAction.DeleteFileSuccess {
    return new resourceFileNameSpace.DeleteFileSuccess(...args);
  }

  static deleteFileFail(resourceFileNameSpace: ResourceFileNameSpace, ...args: ConstructorParameters<typeof ResourceFileAction.DeleteFileFail>): ResourceFileAction.DeleteFileFail {
    return new resourceFileNameSpace.DeleteFileFail(...args);
  }

  static uploadFile(resourceFileNameSpace: ResourceFileNameSpace, ...args: ConstructorParameters<typeof ResourceFileAction.UploadFile>): ResourceFileAction.UploadFile {
    return new resourceFileNameSpace.UploadFile(...args);
  }

  static uploadFileSuccess(resourceFileNameSpace: ResourceFileNameSpace, ...args: ConstructorParameters<typeof ResourceFileAction.UploadFileSuccess>): ResourceFileAction.UploadFileSuccess {
    return new resourceFileNameSpace.UploadFileSuccess(...args);
  }

  static uploadFileFail(resourceFileNameSpace: ResourceFileNameSpace, ...args: ConstructorParameters<typeof ResourceFileAction.UploadFileFail>): ResourceFileAction.UploadFileFail {
    return new resourceFileNameSpace.UploadFileFail(...args);
  }
}
