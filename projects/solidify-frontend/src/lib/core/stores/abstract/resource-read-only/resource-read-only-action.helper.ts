/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - resource-read-only-action.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {ResourceReadOnlyNameSpace} from "./resource-read-only-namespace.model";
import {ResourceReadOnlyAction} from "./resource-read-only.action";

export class ResourceReadOnlyActionHelper {
  static changeQueryParameters(resourceReadOnlyNameSpace: ResourceReadOnlyNameSpace, ...args: ConstructorParameters<typeof ResourceReadOnlyAction.ChangeQueryParameters>): ResourceReadOnlyAction.ChangeQueryParameters {
    return new resourceReadOnlyNameSpace.ChangeQueryParameters(...args);
  }

  static getAll(resourceReadOnlyNameSpace: ResourceReadOnlyNameSpace, ...args: ConstructorParameters<typeof ResourceReadOnlyAction.GetAll>): ResourceReadOnlyAction.GetAll {
    return new resourceReadOnlyNameSpace.GetAll(...args);
  }

  static getAllSuccess<TResource extends BaseResourceType>(resourceReadOnlyNameSpace: ResourceReadOnlyNameSpace, ...args: ConstructorParameters<typeof ResourceReadOnlyAction.GetAllSuccess>): ResourceReadOnlyAction.GetAllSuccess<TResource> {
    return new resourceReadOnlyNameSpace.GetAllSuccess(...args);
  }

  static getAllFail(resourceReadOnlyNameSpace: ResourceReadOnlyNameSpace, ...args: ConstructorParameters<typeof ResourceReadOnlyAction.GetAllFail>): ResourceReadOnlyAction.GetAllFail {
    return new resourceReadOnlyNameSpace.GetAllFail(...args);
  }

  static getById(resourceReadOnlyNameSpace: ResourceReadOnlyNameSpace, ...args: ConstructorParameters<typeof ResourceReadOnlyAction.GetById>): ResourceReadOnlyAction.GetById {
    return new resourceReadOnlyNameSpace.GetById(...args);
  }

  static getByIdSuccess<TResource extends BaseResourceType>(resourceReadOnlyNameSpace: ResourceReadOnlyNameSpace, ...args: ConstructorParameters<typeof ResourceReadOnlyAction.GetByIdSuccess>): ResourceReadOnlyAction.GetByIdSuccess<TResource> {
    return new resourceReadOnlyNameSpace.GetByIdSuccess(...args);
  }

  static getByIdFail(resourceReadOnlyNameSpace: ResourceReadOnlyNameSpace, ...args: ConstructorParameters<typeof ResourceReadOnlyAction.GetByIdFail>): ResourceReadOnlyAction.GetByIdFail {
    return new resourceReadOnlyNameSpace.GetByIdFail(...args);
  }
}
