/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - resource-read-only.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Type,
} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {RegisterDefaultAction} from "../../../decorators/store.decorator";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {SolidifyStateError} from "../../../errors/solidify-state.error";
import {ApiService} from "../../../http/api.service";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {CollectionTyped} from "../../../models/dto/collection-typed.model";
import {SolidifyHttpErrorResponseModel} from "../../../models/errors/solidify-http-error-response.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {NotifierService} from "../../../models/services/notifier-service.model";
import {BaseResourceStateModel} from "../../../models/stores/base-resource-state.model";
import {SolidifyStateContext} from "../../../models/stores/state-context.model";
import {MemoizedUtil} from "../../../utils/stores/memoized.util";
import {StoreUtil} from "../../../utils/stores/store.util";
import {
  BaseResourceState,
  defaultBaseResourceStateInitValue,
} from "../base/base-resource.state";
import {ResourceReadOnlyActionHelper} from "./resource-read-only-action.helper";
import {ResourceReadOnlyNameSpace} from "./resource-read-only-namespace.model";
import {ResourceReadOnlyOptions} from "./resource-read-only-options.model";
import {ResourceReadOnlyStateModel} from "./resource-read-only-state.model";
import {ResourceReadOnlyAction} from "./resource-read-only.action";

export const defaultResourceReadOnlyStateInitValue: <TResourceType extends BaseResourceType> () => ResourceReadOnlyStateModel<TResourceType> = () =>
  ({
    ...defaultBaseResourceStateInitValue(),
    list: undefined,
    current: undefined,
  });

// @dynamic
export abstract class ResourceReadOnlyState<TStateModel extends BaseResourceStateModel, TResource extends BaseResourceType> extends BaseResourceState<TStateModel> {
  protected declare readonly _nameSpace: ResourceReadOnlyNameSpace;
  protected declare readonly _optionsState: ResourceReadOnlyOptions;

  protected constructor(protected readonly _apiService: ApiService,
                        protected readonly _store: Store,
                        protected readonly _notificationService: NotifierService,
                        protected readonly _actions$: Actions,
                        @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
                        protected readonly _options: ResourceReadOnlyOptions) {
    super(_apiService, _store, _notificationService, _actions$, _environment, _options, ResourceReadOnlyState);
  }

  // TODO remove this statics method when MemoizedUtilStateType typing error fixed for this state
  protected static _getDefaultOptions(): ResourceReadOnlyOptions | any {
    let defaultOptions: ResourceReadOnlyOptions | any = {} as ResourceReadOnlyOptions;
    defaultOptions = Object.assign(BaseResourceState._getDefaultOptions(), defaultOptions);
    return defaultOptions;
  }

  protected static override _checkOptions(stateName: string, options: ResourceReadOnlyOptions): void {
    BaseResourceState._checkOptions(stateName, options);
  }

  static list<TStateModel extends ResourceReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceReadOnlyState<TStateModel, TResource>>): Observable<TResource[]> {
    return MemoizedUtil.select(store, ctor, state => state.list, true);
  }

  static listSnapshot<TStateModel extends ResourceReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceReadOnlyState<TStateModel, TResource>>): TResource[] {
    return MemoizedUtil.selectSnapshot(store, ctor, state => state.list);
  }

  static total<TStateModel extends ResourceReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceReadOnlyState<TStateModel, TResource>>): Observable<number> {
    return MemoizedUtil.select(store, ctor, state => state.total, true);
  }

  static totalSnapshot<TStateModel extends ResourceReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceReadOnlyState<TStateModel, TResource>>): number {
    return MemoizedUtil.selectSnapshot(store, ctor, state => state.total);
  }

  static current<TStateModel extends ResourceReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceReadOnlyState<TStateModel, TResource>>): Observable<TResource> {
    return MemoizedUtil.select(store, ctor, state => state.current, true);
  }

  static currentSnapshot<TStateModel extends ResourceReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceReadOnlyState<TStateModel, TResource>>): TResource {
    return MemoizedUtil.selectSnapshot(store, ctor, state => state.current);
  }

  static queryParameters<TStateModel extends ResourceReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceReadOnlyState<TStateModel, TResource>>): Observable<QueryParameters> {
    return MemoizedUtil.select(store, ctor, state => state.queryParameters, true);
  }

  static queryParametersSnapshot<TStateModel extends ResourceReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceReadOnlyState<TStateModel, TResource>>): QueryParameters {
    return MemoizedUtil.selectSnapshot(store, ctor, state => state.queryParameters);
  }

  @RegisterDefaultAction((resourceReadOnlyNameSpace: ResourceReadOnlyNameSpace) => resourceReadOnlyNameSpace.ChangeQueryParameters)
  changeQueryParameters(ctx: SolidifyStateContext<ResourceReadOnlyStateModel<TResource>>, action: ResourceReadOnlyAction.ChangeQueryParameters): void {
    ctx.patchState({
      queryParameters: action.queryParameters,
    });
    ctx.dispatch(ResourceReadOnlyActionHelper.getAll(this._nameSpace, undefined, action.keepCurrentContext));
  }

  @RegisterDefaultAction((resourceReadOnlyNameSpace: ResourceReadOnlyNameSpace) => resourceReadOnlyNameSpace.GetAll)
  getAll(ctx: SolidifyStateContext<ResourceReadOnlyStateModel<TResource>>, action: ResourceReadOnlyAction.GetAll): Observable<CollectionTyped<TResource>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        total: 0,
        list: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    return this._getAll<TResource>(ctx, action)
      .pipe(
        tap((collection: CollectionTyped<TResource>) => {
          ctx.dispatch(ResourceReadOnlyActionHelper.getAllSuccess<TResource>(this._nameSpace, action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(ResourceReadOnlyActionHelper.getAllFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  protected override _callGetAllEndpoint<U>(ctx: SolidifyStateContext<ResourceReadOnlyStateModel<TResource>>, action: ResourceReadOnlyAction.GetAll, queryParameters: QueryParameters): Observable<CollectionTyped<U>> {
    const url = this._urlResource;
    return this._apiService.getCollection<U>(url, queryParameters);
  }

  @RegisterDefaultAction((resourceReadOnlyNameSpace: ResourceReadOnlyNameSpace) => resourceReadOnlyNameSpace.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<ResourceReadOnlyStateModel<TResource>>, action: ResourceReadOnlyAction.GetAllSuccess<TResource>): void {
    ctx.patchState({
      total: action.list._page?.totalItems,
      list: action.list._data,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((resourceReadOnlyNameSpace: ResourceReadOnlyNameSpace) => resourceReadOnlyNameSpace.GetAllFail)
  getAllFail(ctx: SolidifyStateContext<ResourceReadOnlyStateModel<TResource>>, action: ResourceReadOnlyAction.GetAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((resourceReadOnlyNameSpace: ResourceReadOnlyNameSpace) => resourceReadOnlyNameSpace.GetById)
  getById(ctx: SolidifyStateContext<ResourceReadOnlyStateModel<TResource>>, action: ResourceReadOnlyAction.GetById): Observable<TResource> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        current: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });

    return this._apiService.getById<TResource>(this._urlResource, action.id)
      .pipe(
        tap((model: TResource) => {
          ctx.dispatch(ResourceReadOnlyActionHelper.getByIdSuccess(this._nameSpace, action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(ResourceReadOnlyActionHelper.getByIdFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((resourceReadOnlyNameSpace: ResourceReadOnlyNameSpace) => resourceReadOnlyNameSpace.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<ResourceReadOnlyStateModel<TResource>>, action: ResourceReadOnlyAction.GetByIdSuccess<TResource>): void {
    ctx.patchState({
      current: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((resourceReadOnlyNameSpace: ResourceReadOnlyNameSpace) => resourceReadOnlyNameSpace.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<ResourceReadOnlyStateModel<TResource>>, action: ResourceReadOnlyAction.GetByIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
