/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - association-no-sql-read-only.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Inject} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {RegisterDefaultAction} from "../../../decorators/store.decorator";
import {ApiResourceNamePartialEnum} from "../../../enums/partial/api-resource-name-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {SolidifyStateError} from "../../../errors/solidify-state.error";
import {ApiService} from "../../../http/api.service";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {CollectionTyped} from "../../../models/dto/collection-typed.model";
import {SolidifyHttpErrorResponseModel} from "../../../models/errors/solidify-http-error-response.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {NotifierService} from "../../../models/services/notifier-service.model";
import {BaseResourceStateModel} from "../../../models/stores/base-resource-state.model";
import {SolidifyStateContext} from "../../../models/stores/state-context.model";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {StoreUtil} from "../../../utils/stores/store.util";
import {
  BaseResourceState,
  defaultBaseResourceStateInitValue,
} from "../base/base-resource.state";
import {AssociationNoSqlReadOnlyActionHelper} from "./association-no-sql-read-only-action.helper";
import {AssociationNoSqlReadOnlyNameSpace} from "./association-no-sql-read-only-namespace.model";
import {AssociationNoSqlReadOnlyOptions} from "./association-no-sql-read-only-options.model";
import {AssociationNoSqlReadOnlyStateModel} from "./association-no-sql-read-only-state.model";
import {AssociationNoSqlReadOnlyAction} from "./association-no-sql-read-only.action";

export const defaultAssociationNoSqlReadOnlyStateInitValue: <TResourceType extends BaseResourceType> () => AssociationNoSqlReadOnlyStateModel<TResourceType> = () =>
  ({
    ...defaultBaseResourceStateInitValue(),
    selected: undefined,
    current: undefined,
  });

// @dynamic
export abstract class AssociationNoSqlReadOnlyState<TStateModel extends BaseResourceStateModel, TResource extends BaseResourceType> extends BaseResourceState<TStateModel> {
  protected readonly _resourceName: ExtendEnum<ApiResourceNamePartialEnum>;
  protected declare readonly _nameSpace: AssociationNoSqlReadOnlyNameSpace;
  protected declare readonly _optionsState: AssociationNoSqlReadOnlyOptions;

  protected constructor(protected readonly _apiService: ApiService,
                        protected readonly _store: Store,
                        protected readonly _notificationService: NotifierService,
                        protected readonly _actions$: Actions,
                        @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
                        protected readonly _options: AssociationNoSqlReadOnlyOptions) {
    super(_apiService, _store, _notificationService, _actions$, _environment, _options, AssociationNoSqlReadOnlyState);
    this._resourceName = this._optionsState.resourceName;
  }

  private _evaluateSubResourceUrl(parentId: string): string {
    return this._urlResource + SOLIDIFY_CONSTANTS.URL_SEPARATOR + parentId + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this._resourceName;
  }

  @RegisterDefaultAction((associationNoSqlReadOnlyNameSpace: AssociationNoSqlReadOnlyNameSpace) => associationNoSqlReadOnlyNameSpace.GetAll)
  getAll<U>(ctx: SolidifyStateContext<AssociationNoSqlReadOnlyStateModel<TResource>>, action: AssociationNoSqlReadOnlyAction.GetAll): Observable<CollectionTyped<U>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        total: 0,
        selected: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    return this._getAll<U>(ctx, action)
      .pipe(
        tap((collection: CollectionTyped<U>) => {
          ctx.dispatch(AssociationNoSqlReadOnlyActionHelper.getAllSuccess<U>(this._nameSpace, action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(AssociationNoSqlReadOnlyActionHelper.getAllFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  protected override _callGetAllEndpoint<U>(ctx: SolidifyStateContext<AssociationNoSqlReadOnlyStateModel<TResource>>, action: AssociationNoSqlReadOnlyAction.GetAll, queryParameters: QueryParameters): Observable<CollectionTyped<U>> {
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.getCollection<U>(url, queryParameters);
  }

  @RegisterDefaultAction((associationNoSqlReadOnlyNameSpace: AssociationNoSqlReadOnlyNameSpace) => associationNoSqlReadOnlyNameSpace.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<AssociationNoSqlReadOnlyStateModel<TResource>>, action: AssociationNoSqlReadOnlyAction.GetAllSuccess<TResource>): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.list);

    ctx.patchState({
      total: action.list._page.totalItems,
      selected: action.list._data,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters,
    });
  }

  @RegisterDefaultAction((associationNoSqlReadOnlyNameSpace: AssociationNoSqlReadOnlyNameSpace) => associationNoSqlReadOnlyNameSpace.GetAllFail)
  getAllFail(ctx: SolidifyStateContext<AssociationNoSqlReadOnlyStateModel<TResource>>, action: AssociationNoSqlReadOnlyAction.GetAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((associationNoSqlReadOnlyNameSpace: AssociationNoSqlReadOnlyNameSpace) => associationNoSqlReadOnlyNameSpace.GetById)
  getById<U>(ctx: SolidifyStateContext<AssociationNoSqlReadOnlyStateModel<TResource>>, action: AssociationNoSqlReadOnlyAction.GetById): Observable<U> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        current: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.getById<U>(url, action.resId)
      .pipe(
        tap((model: U) => {
          ctx.dispatch(AssociationNoSqlReadOnlyActionHelper.getByIdSuccess<U>(this._nameSpace, action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(AssociationNoSqlReadOnlyActionHelper.getByIdFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((associationNoSqlReadOnlyNameSpace: AssociationNoSqlReadOnlyNameSpace) => associationNoSqlReadOnlyNameSpace.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<AssociationNoSqlReadOnlyStateModel<TResource>>, action: AssociationNoSqlReadOnlyAction.GetByIdSuccess<TResource>): void {
    ctx.patchState({
      current: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((associationNoSqlReadOnlyNameSpace: AssociationNoSqlReadOnlyNameSpace) => associationNoSqlReadOnlyNameSpace.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<AssociationNoSqlReadOnlyStateModel<TResource>>, action: AssociationNoSqlReadOnlyAction.GetByIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
