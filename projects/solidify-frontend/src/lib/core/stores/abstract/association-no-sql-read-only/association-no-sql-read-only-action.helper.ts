/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - association-no-sql-read-only-action.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {AssociationNoSqlReadOnlyNameSpace} from "./association-no-sql-read-only-namespace.model";
import {AssociationNoSqlReadOnlyAction} from "./association-no-sql-read-only.action";

export class AssociationNoSqlReadOnlyActionHelper {
  static getAll(associationNoSqlReadOnlyNameSpace: AssociationNoSqlReadOnlyNameSpace, ...args: ConstructorParameters<typeof AssociationNoSqlReadOnlyAction.GetAll>): AssociationNoSqlReadOnlyAction.GetAll {
    return new associationNoSqlReadOnlyNameSpace.GetAll(...args);
  }

  static getAllSuccess<TResource extends BaseResourceType>(associationNoSqlReadOnlyNameSpace: AssociationNoSqlReadOnlyNameSpace, ...args: ConstructorParameters<typeof AssociationNoSqlReadOnlyAction.GetAllSuccess>): AssociationNoSqlReadOnlyAction.GetAllSuccess<TResource> {
    return new associationNoSqlReadOnlyNameSpace.GetAllSuccess(...args);
  }

  static getAllFail(associationNoSqlReadOnlyNameSpace: AssociationNoSqlReadOnlyNameSpace, ...args: ConstructorParameters<typeof AssociationNoSqlReadOnlyAction.GetAllFail>): AssociationNoSqlReadOnlyAction.GetAllFail {
    return new associationNoSqlReadOnlyNameSpace.GetAllFail(...args);
  }

  static getById(associationNoSqlReadOnlyNameSpace: AssociationNoSqlReadOnlyNameSpace, ...args: ConstructorParameters<typeof AssociationNoSqlReadOnlyAction.GetById>): AssociationNoSqlReadOnlyAction.GetById {
    return new associationNoSqlReadOnlyNameSpace.GetById(...args);
  }

  static getByIdSuccess<TResource extends BaseResourceType>(associationNoSqlReadOnlyNameSpace: AssociationNoSqlReadOnlyNameSpace, ...args: ConstructorParameters<typeof AssociationNoSqlReadOnlyAction.GetByIdSuccess>): AssociationNoSqlReadOnlyAction.GetByIdSuccess<TResource> {
    return new associationNoSqlReadOnlyNameSpace.GetByIdSuccess(...args);
  }

  static getByIdFail(associationNoSqlReadOnlyNameSpace: AssociationNoSqlReadOnlyNameSpace, ...args: ConstructorParameters<typeof AssociationNoSqlReadOnlyAction.GetByIdFail>): AssociationNoSqlReadOnlyAction.GetByIdFail {
    return new associationNoSqlReadOnlyNameSpace.GetByIdFail(...args);
  }
}
