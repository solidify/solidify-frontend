/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - base-resource.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  Observable,
  of,
} from "rxjs";
import {switchMap} from "rxjs/operators";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {QueryParametersUtil} from "../../../../core-resources/utils/query-parameters.util";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {ApiService} from "../../../http/api.service";
import {CollectionTyped} from "../../../models/dto/collection-typed.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {NotifierService} from "../../../models/services/notifier-service.model";
import {BaseResourceStateModel} from "../../../models/stores/base-resource-state.model";
import {BaseResourceStoreNameSpace} from "../../../models/stores/base-resource-store-namespace.model";
import {BaseAction} from "../../../models/stores/base.action";
import {SolidifyStateContext} from "../../../models/stores/state-context.model";
import {BaseResourceOptions} from "./base-resource-options.model";
import {
  BaseState,
  defaultBaseStateInitValue,
} from "./base.state";

export const defaultBaseResourceStateInitValue: () => BaseResourceStateModel = () =>
  ({
    ...defaultBaseStateInitValue(),
    total: 0,
    queryParameters: new QueryParameters(),
  });

export abstract class BaseResourceState<TStateModel extends BaseResourceStateModel> extends BaseState<TStateModel> {
  protected declare readonly _nameSpace: BaseResourceStoreNameSpace;
  protected declare readonly _optionsState: BaseResourceOptions;

  protected constructor(protected _apiService: ApiService,
                        protected _store: Store,
                        protected _notificationService: NotifierService,
                        protected _actions$: Actions,
                        protected _environment: DefaultSolidifyEnvironment,
                        protected _options: BaseResourceOptions,
                        protected _baseStateConstructor: typeof BaseResourceState | any) {
    super(_apiService, _store, _notificationService, _actions$, _options, _baseStateConstructor);
  }

  protected static _getDefaultOptions(): BaseResourceOptions | any {
    let defaultOptions: BaseResourceOptions | any = {} as BaseResourceOptions;
    defaultOptions = Object.assign(BaseState._getDefaultOptions(), defaultOptions);
    return defaultOptions;
  }

  protected static _checkOptions(stateName: string, options: BaseResourceOptions): void {
    BaseState._checkOptions(stateName, options);
  }

  protected _getAll<U>(ctx: SolidifyStateContext<any>,
                       actionGetAll: BaseAction,
                       queryParameters: QueryParameters = undefined,
                       collection: CollectionTyped<U> = undefined): Observable<CollectionTyped<U>> {
    queryParameters = QueryParametersUtil.clone(isNotNullNorUndefined(queryParameters) ? queryParameters : ctx.getState().queryParameters);
    return this._callGetAllEndpoint<U>(ctx, actionGetAll, queryParameters)
      .pipe(
        switchMap((nextChunkCollection: CollectionTyped<U>) => {
          if (isNullOrUndefined(collection)) {
            collection = nextChunkCollection;
          } else {
            collection._data.push(...nextChunkCollection._data);
            collection._page.currentPage = nextChunkCollection._page.currentPage;
          }
          if (this._haveMoreDataToRetrieve(collection)) {
            queryParameters.paging.pageIndex++;
            return this._getAll(ctx, actionGetAll, queryParameters, collection);
          }
          return of(collection);
        }),
      );
  }

  protected _haveMoreDataToRetrieve<U>(collection: CollectionTyped<U>): boolean {
    return collection._page.sizePage === this._environment.maximalPageSizeToRetrievePaginationInfo
      && collection._page.sizePage === collection._data.length
      && collection._page.currentPage < collection._page.totalPages;
  }

  protected _callGetAllEndpoint<U>(ctx: SolidifyStateContext<BaseResourceStateModel>, action: BaseAction, queryParameters: QueryParameters): Observable<CollectionTyped<U>> {
    return this._apiService.getCollection<U>(this._urlResource, queryParameters);
  }
}
