/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - relation-3-tiers.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Inject} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {RegisterDefaultAction} from "../../../decorators/store.decorator";
import {ApiResourceNamePartialEnum} from "../../../enums/partial/api-resource-name-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {SolidifyStateError} from "../../../errors/solidify-state.error";
import {ApiService} from "../../../http/api.service";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {BaseRelationResourceType} from "../../../models/dto/base-relation-resource.model";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {CollectionTyped} from "../../../models/dto/collection-typed.model";
import {SolidifyHttpErrorResponseModel} from "../../../models/errors/solidify-http-error-response.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {NotifierService} from "../../../models/services/notifier-service.model";
import {BaseResourceStateModel} from "../../../models/stores/base-resource-state.model";
import {ActionSubActionCompletionsWrapper} from "../../../models/stores/base.action";
import {SolidifyStateContext} from "../../../models/stores/state-context.model";
import {SubResourceUpdateModel} from "../../../models/stores/sub-resource-update.model";
import {
  isEmptyArray,
  isNonEmptyArray,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {ofSolidifyActionCompleted} from "../../../utils/stores/store.tool";
import {StoreUtil} from "../../../utils/stores/store.util";
import {
  BaseResourceState,
  defaultBaseResourceStateInitValue,
} from "../base/base-resource.state";
import {Relation3TiersActionHelper} from "./relation-3-tiers-action.helper";
import {Relation3TiersForm} from "./relation-3-tiers-form.model";
import {Relation3TiersNameSpace} from "./relation-3-tiers-namespace.model";
import {Relation3TiersOptions} from "./relation-3-tiers-options.model";
import {Relation3TiersStateModel} from "./relation-3-tiers-state.model";
import {Relation3TiersAction} from "./relation-3-tiers.action";

export const defaultRelation3TiersStateInitValue: <TResourceType extends BaseResourceType> () => Relation3TiersStateModel<TResourceType> = () =>
  ({
    ...defaultBaseResourceStateInitValue(),
    current: undefined,
    selected: undefined,
  });

// @dynamic
export abstract class Relation3TiersState<TStateModel extends BaseResourceStateModel, TResource extends BaseResourceType, TRelation extends BaseRelationResourceType> extends BaseResourceState<TStateModel> {
  protected readonly _resourceName: ExtendEnum<ApiResourceNamePartialEnum>;
  protected declare readonly _nameSpace: Relation3TiersNameSpace;
  protected declare readonly _optionsState: Relation3TiersOptions;

  protected constructor(protected readonly _apiService: ApiService,
                        protected readonly _store: Store,
                        protected readonly _notificationService: NotifierService,
                        protected readonly _actions$: Actions,
                        @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
                        protected readonly _options: Relation3TiersOptions) {
    super(_apiService, _store, _notificationService, _actions$, _environment, _options, Relation3TiersState);
    this._resourceName = this._optionsState.resourceName;
  }

  protected static _getDefaultOptions(): Relation3TiersOptions | any {
    let defaultOptions: Relation3TiersOptions | any = {} as Relation3TiersOptions;
    defaultOptions = Object.assign(BaseResourceState._getDefaultOptions(), defaultOptions);
    return defaultOptions;
  }

  private _evaluateSubResourceUrl(parentId: string, id: string | undefined = undefined): string {
    const url = this._urlResource + SOLIDIFY_CONSTANTS.URL_SEPARATOR + parentId + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this._resourceName;
    if (isUndefined(id)) {
      return url;
    }
    return url + SOLIDIFY_CONSTANTS.URL_SEPARATOR + id;
  }

  protected abstract _convertResourceInForm(resource: TResource): Relation3TiersForm;

  private _convertSelectedToForm(list: TResource[]): Relation3TiersForm[] {
    const form = [];
    if (isNullOrUndefined(list)) {
      return [];
    }
    list.forEach(r => {
      form.push(this._convertResourceInForm(r));
    });
    return form;
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.GetAll)
  getAll<U>(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.GetAll): Observable<CollectionTyped<U>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        total: 0,
        selected: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    return this._getAll<U>(ctx, action)
      .pipe(
        tap((collection: CollectionTyped<U>) => {
          ctx.dispatch(Relation3TiersActionHelper.getAllSuccess<U>(this._nameSpace, action, collection));
          return of(collection);
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(Relation3TiersActionHelper.getAllFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  protected override _callGetAllEndpoint<U>(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.GetAll, queryParameters: QueryParameters): Observable<CollectionTyped<U>> {
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.getCollection<U>(url, queryParameters);
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.GetAllSuccess<TResource>): void {
    ctx.patchState({
      selected: action.list._data,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.GetAllFail)
  getAllFail(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.GetAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.GetById)
  getById<U>(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.GetById): Observable<U> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        current: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    const url = this._evaluateSubResourceUrl(action.parentId, action.id);
    return this._apiService.getCollection<U>(url, undefined)
      .pipe(
        tap((model: U) => {
          ctx.dispatch(Relation3TiersActionHelper.getByIdSuccess<U>(this._nameSpace, action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(Relation3TiersActionHelper.getByIdFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.GetByIdSuccess<TResource>): void {
    ctx.patchState({
      current: action.current,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.GetByIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.Create)
  create(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.Create): Observable<string[]> {
    if (action.listResId.length === 0) {
      return;
    }
    const url = this._evaluateSubResourceUrl(action.parentId, action.id);
    return this._apiService.post<string[]>(url, action.listResId)
      .pipe(
        tap(() => {
          ctx.dispatch(Relation3TiersActionHelper.createSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(Relation3TiersActionHelper.createFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.CreateResource)
  createResource(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.CreateResource): Observable<string[]> {
    if (action.listResId.length === 0) {
      return;
    }
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.post<string[]>(url, action.listResId)
      .pipe(
        tap(() => {
          ctx.dispatch(Relation3TiersActionHelper.createResourceSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(Relation3TiersActionHelper.createResourceFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.Delete)
  delete(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.Delete): Observable<string[]> {
    const url = this._evaluateSubResourceUrl(action.parentId, action.id);
    return this._apiService.delete<string[]>(url, action.listResId)
      .pipe(
        tap(() => {
          ctx.dispatch(Relation3TiersActionHelper.deleteSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(Relation3TiersActionHelper.deleteFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.UpdateItem)
  updateItem(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.UpdateItem): Observable<any> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const url = this._evaluateSubResourceUrl(action.parentId, action.id);
    return this._apiService.patch<string[]>(url, action.object)
      .pipe(
        tap(() => {
          ctx.dispatch(Relation3TiersActionHelper.updateItemSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(Relation3TiersActionHelper.updateItemFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.UpdateItemSuccess)
  updateItemSuccess(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.UpdateItemSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.UpdateItemFail)
  updateItemFail(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.UpdateItemFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.Update)
  update(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.Update): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const listOldForm = this._convertSelectedToForm(ctx.getState().selected);
    const listNewForm = action.newForm;

    const subResourceUpdate: SubResourceUpdateModel = StoreUtil.determineSubResourceChange(listOldForm.map(o => o.id), listNewForm.map(o => o.id));
    const createAndUpdateActions = this._computeResourceUpdatesActionsToDispatch(subResourceUpdate, action.parentId, action.addCreateDefaultAction);

    listNewForm.forEach(newForm => {
      const oldForm: Relation3TiersForm = listOldForm.find(old => old.id === newForm.id);
      let relationActionSubActionCompletionsWrapper: ActionSubActionCompletionsWrapper[];
      if (isNullOrUndefined(oldForm)) {
        relationActionSubActionCompletionsWrapper = this._computeRelationUpdatesActionsToDispatch({resourceToAdd: newForm.listId} as SubResourceUpdateModel, action.parentId, newForm.id);
      } else {
        const subResourceListUpdate: SubResourceUpdateModel = StoreUtil.determineSubResourceChange(oldForm.listId, newForm.listId, isNotNullNorUndefinedNorWhiteString(this._optionsState.updateGrandChildListActionName));
        relationActionSubActionCompletionsWrapper = this._computeRelationUpdatesActionsToDispatch(subResourceListUpdate, action.parentId, newForm.id);
      }
      createAndUpdateActions.push(...relationActionSubActionCompletionsWrapper);
    });

    // console.debug("Dispatch sequential", createAndUpdateActions);
    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(ctx, createAndUpdateActions)
      .pipe(
        map(result => {
          if (result.success) {
            ctx.dispatch(Relation3TiersActionHelper.updateSuccess(this._nameSpace, action));
          } else {
            ctx.dispatch(Relation3TiersActionHelper.updateFail(this._nameSpace, action));
          }
          return result.success;
        }),
      );
  }

  protected _computeResourceUpdatesActionsToDispatch(subResourceUpdate: SubResourceUpdateModel, parentId: string, addCreateDefaultAction: boolean): ActionSubActionCompletionsWrapper[] {
    const actions = [];
    if (!isNullOrUndefined(subResourceUpdate.resourceToRemoved)) {
      subResourceUpdate.resourceToRemoved.forEach(id => {
        const deleteAction = Relation3TiersActionHelper.delete(this._nameSpace, parentId, id, null);
        actions.push({
          action: deleteAction,
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.DeleteSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.DeleteFail)),
          ],
        });
      });
    }
    if (!isNullOrUndefined(subResourceUpdate.resourceToAdd) && !isEmptyArray(subResourceUpdate.resourceToAdd) && addCreateDefaultAction) {
      const createAction = Relation3TiersActionHelper.createResource(this._nameSpace, parentId, subResourceUpdate.resourceToAdd);
      actions.push({
        action: createAction,
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.CreateResourceSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.CreateResourceFail)),
        ],
      });
    }
    return actions;
  }

  private _computeRelationUpdatesActionsToDispatch(subResourceUpdate: SubResourceUpdateModel, parentId: string, id: string): ActionSubActionCompletionsWrapper[] {
    const actions = [];
    if (isNonEmptyArray(subResourceUpdate.resourceToAdd)) {
      const createAction = Relation3TiersActionHelper.create(this._nameSpace, parentId, id, subResourceUpdate.resourceToAdd);
      actions.push({
        action: createAction,
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.CreateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.CreateFail)),
        ],
      });
    }
    if (isNonEmptyArray(subResourceUpdate.resourceToRemoved)) {
      const deleteAction = Relation3TiersActionHelper.delete(this._nameSpace, parentId, id, subResourceUpdate.resourceToRemoved);
      actions.push({
        action: deleteAction,
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.DeleteSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.DeleteFail)),
        ],
      });
    }
    if (isNonEmptyArray(subResourceUpdate.resourceToUpdate)) {
      const updateItemAction = Relation3TiersActionHelper.setGrandChildList(this._nameSpace, parentId, id, subResourceUpdate.resourceToUpdate);
      actions.push({
        action: updateItemAction,
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.SetGrandChildListSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.SetGrandChildListFail)),
        ],
      });
    }
    return actions;
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.UpdateSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      selected: undefined,
    });
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.UpdateFail)
  updateFail(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.UpdateFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(Relation3TiersActionHelper.getAll(this._nameSpace, action.parentAction.parentId));
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.SetGrandChildList)
  setGrandChildList(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.SetGrandChildList): Observable<void> {
    if (isNullOrUndefinedOrWhiteString(this._optionsState.updateGrandChildListActionName)) {
      throw new Error(`Need to define option "updateGrandChildListActionName" in state ${this._stateName}`);
    }

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const url = this._evaluateSubResourceUrl(action.parentId, action.id);
    return this._apiService.post<string[], void>(`${url}/${this._optionsState.updateGrandChildListActionName}`, action.listResId)
      .pipe(
        tap(() => {
          ctx.dispatch(Relation3TiersActionHelper.setGrandChildListSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(Relation3TiersActionHelper.setGrandChildListFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.SetGrandChildListSuccess)
  setGrandChildListSuccess(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.SetGrandChildListSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((relation3TiersNameSpace: Relation3TiersNameSpace) => relation3TiersNameSpace.SetGrandChildListFail)
  setGrandChildListFail(ctx: SolidifyStateContext<Relation3TiersStateModel<TResource>>, action: Relation3TiersAction.SetGrandChildListFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
