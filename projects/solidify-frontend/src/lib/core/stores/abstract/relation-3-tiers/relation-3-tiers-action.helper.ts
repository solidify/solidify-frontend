/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - relation-3-tiers-action.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {Relation3TiersNameSpace} from "./relation-3-tiers-namespace.model";
import {Relation3TiersAction} from "./relation-3-tiers.action";

export class Relation3TiersActionHelper {
  static getAll(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.GetAll>): Relation3TiersAction.GetAll {
    return new relation3TiersNameSpace.GetAll(...args);
  }

  static getAllSuccess<TResource extends BaseResourceType>(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.GetAllSuccess>): Relation3TiersAction.GetAllSuccess<TResource> {
    return new relation3TiersNameSpace.GetAllSuccess(...args);
  }

  static getAllFail(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.GetAllFail>): Relation3TiersAction.GetAllFail {
    return new relation3TiersNameSpace.GetAllFail(...args);
  }

  static getById(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.GetById>): Relation3TiersAction.GetById {
    return new relation3TiersNameSpace.GetById(...args);
  }

  static getByIdSuccess<TResource extends BaseResourceType>(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.GetByIdSuccess>): Relation3TiersAction.GetByIdSuccess<TResource> {
    return new relation3TiersNameSpace.GetByIdSuccess(...args);
  }

  static getByIdFail(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.GetByIdFail>): Relation3TiersAction.GetByIdFail {
    return new relation3TiersNameSpace.GetByIdFail(...args);
  }

  static create(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.Create>): Relation3TiersAction.Create {
    return new relation3TiersNameSpace.Create(...args);
  }

  static createSuccess(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.CreateSuccess>): Relation3TiersAction.CreateSuccess {
    return new relation3TiersNameSpace.CreateSuccess(...args);
  }

  static createFail(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.CreateFail>): Relation3TiersAction.CreateFail {
    return new relation3TiersNameSpace.CreateFail(...args);
  }

  static createResource(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.CreateResource>): Relation3TiersAction.CreateResource {
    return new relation3TiersNameSpace.CreateResource(...args);
  }

  static createResourceSuccess(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.CreateResourceSuccess>): Relation3TiersAction.CreateResourceSuccess {
    return new relation3TiersNameSpace.CreateResourceSuccess(...args);
  }

  static createResourceFail(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.CreateResourceFail>): Relation3TiersAction.CreateResourceFail {
    return new relation3TiersNameSpace.CreateResourceFail(...args);
  }

  static delete(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.Delete>): Relation3TiersAction.Delete {
    return new relation3TiersNameSpace.Delete(...args);
  }

  static deleteSuccess(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.DeleteSuccess>): Relation3TiersAction.DeleteSuccess {
    return new relation3TiersNameSpace.DeleteSuccess(...args);
  }

  static deleteFail(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.DeleteFail>): Relation3TiersAction.DeleteFail {
    return new relation3TiersNameSpace.DeleteFail(...args);
  }

  static update(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.Update>): Relation3TiersAction.Update {
    return new relation3TiersNameSpace.Update(...args);
  }

  static updateSuccess(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.UpdateSuccess>): Relation3TiersAction.UpdateSuccess {
    return new relation3TiersNameSpace.UpdateSuccess(...args);
  }

  static updateFail(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.UpdateFail>): Relation3TiersAction.UpdateFail {
    return new relation3TiersNameSpace.UpdateFail(...args);
  }

  static updateItem(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.UpdateItem>): Relation3TiersAction.UpdateItem {
    return new relation3TiersNameSpace.UpdateItem(...args);
  }

  static updateItemSuccess(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.UpdateItemSuccess>): Relation3TiersAction.UpdateItemSuccess {
    return new relation3TiersNameSpace.UpdateItemSuccess(...args);
  }

  static updateItemFail(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.UpdateItemFail>): Relation3TiersAction.UpdateItemFail {
    return new relation3TiersNameSpace.UpdateItemFail(...args);
  }

  static setGrandChildList(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.SetGrandChildList>): Relation3TiersAction.SetGrandChildList {
    return new relation3TiersNameSpace.SetGrandChildList(...args);
  }

  static setGrandChildListSuccess(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.SetGrandChildListSuccess>): Relation3TiersAction.SetGrandChildListSuccess {
    return new relation3TiersNameSpace.SetGrandChildListSuccess(...args);
  }

  static setGrandChildListFail(relation3TiersNameSpace: Relation3TiersNameSpace, ...args: ConstructorParameters<typeof Relation3TiersAction.SetGrandChildListFail>): Relation3TiersAction.SetGrandChildListFail {
    return new relation3TiersNameSpace.SetGrandChildListFail(...args);
  }
}
