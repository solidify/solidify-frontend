/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - app.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpClient,
  HttpHeaders,
} from "@angular/common/http";
import {
  Inject,
  LOCALE_ID,
  makeStateKey,
  Optional,
} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import * as moment from "moment";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {isFalse} from "../../../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../../../core-resources/utils/ssr.util";
import {AppConfigService} from "../../../config/app-config.service";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {CookieType} from "../../../cookie-consent/enums/cookie-type.enum";
import {CookieConsentUtil} from "../../../cookie-consent/utils/cookie-consent.util";
import {RegisterDefaultAction} from "../../../decorators/store.decorator";
import {ApiActionNamePartialEnum} from "../../../enums/partial/api-action-name-partial.enum";
import {CookiePartialEnum} from "../../../enums/partial/cookie-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {CookieHelper} from "../../../helpers/cookie.helper";
import {ApiService} from "../../../http/api.service";
import {APP_OPTIONS} from "../../../injection-tokens/app-options.injection-token";
import {SolidifyHttpErrorResponseModel} from "../../../models/errors/solidify-http-error-response.model";
import {FrontendVersion} from "../../../models/frontend-version.model";
import {ActionSubActionCompletionsWrapper} from "../../../models/stores/base.action";
import {SolidifyStateContext} from "../../../models/stores/state-context.model";
import {NativeNotificationService} from "../../../services/native-notification.service";
import {NotificationService} from "../../../services/notification.service";
import {TransferStateService} from "../../../services/transfer-state.service";
import {CacheBustingUtil} from "../../../utils/cache-busting.util";
import {
  ofSolidifyActionCompleted,
  ofSolidifyActionErrored,
} from "../../../utils/stores/store.tool";
import {StoreUtil} from "../../../utils/stores/store.util";
import {UserPreferencesUtil} from "../../../utils/user-preferences.util";
import {defaultBaseStateInitValue} from "../base/base.state";
import {BasicState} from "../base/basic.state";
import {SolidifyAppNameSpace} from "./app-namespace.model";
import {SolidifyAppOptions} from "./app-options.model";
import {SolidifyAppStateModel} from "./app-state.model";
import {SolidifyAppAction} from "./app.action";

export const defaultSolidifyAppStateInitValue: () => SolidifyAppStateModel = () =>
  ({
    ...defaultBaseStateInitValue(),
    isTouchInterface: false,
    isApplicationInitialized: false,
    isServerOffline: false,
    isUnableToLoadApp: false,
    isLoggedIn: false,
    isInTourMode: false,
    ignorePreventLeavePopup: false,
    isOpenedSidebarUserGuide: false,
    isOpenedSidebarCookieConsent: false,
    isOpenedBannerCookieConsent: false,
    newCookiesAvailable: undefined,
    appLanguage: undefined,
    theme: undefined,
    darkMode: false,
    preventExit: false,
    preventExitReasonToTranslate: undefined,
    token: undefined,
    frontendVersion: undefined,
  });

// @dynamic
export abstract class SolidifyAppState<TStateModel extends SolidifyAppStateModel> extends BasicState<TStateModel> {
  protected static readonly _AUTHORIZATION_MODULE_NAME: string = "authorization";

  protected declare readonly _state: TStateModel;
  protected declare readonly _stateName: string;
  protected readonly _nameSpace: SolidifyAppNameSpace;

  constructor(@Inject(LOCALE_ID) protected readonly _locale: string,
              protected readonly _store: Store,
              protected readonly _translate: TranslateService,
              protected readonly _actions$: Actions,
              protected readonly _apiService: ApiService,
              protected readonly _httpClient: HttpClient,
              protected readonly _notificationService: NotificationService,
              @Optional() @Inject(APP_OPTIONS) protected readonly _optionsState: SolidifyAppOptions,
              protected readonly _environment: DefaultSolidifyEnvironment,
              protected readonly _nativeNotificationService: NativeNotificationService,
              protected readonly _appConfigService: AppConfigService,
              @Optional() protected readonly _transferState: TransferStateService,
  ) {
    super();
    this._state = StoreUtil.getStateFromInstance(this);
    this._stateName = StoreUtil.getStateNameFromInstance(this);
    this._nameSpace = _optionsState.nameSpace;
    StoreUtil.initState(SolidifyAppState, this.constructor, this._nameSpace);
    // _environment.appState = _options.baseAppSate;
    // _environment.appNameSpace = _options.nameSpace as SolidifyAppNameSpace;
  }

  // Override if necessary
  protected _isLoggedInRoot(): boolean {
    return false;
  }

  // Override if necessary
  protected _loadModule(): ActionSubActionCompletionsWrapper[] {
    return [];
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.InitApplication)
  initApplication(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.InitApplication): Observable<boolean> {
    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(
      ctx,
      [
        {action: new SolidifyAppAction.ChangeDarkMode(UserPreferencesUtil.getPreferredDarkMode(this._environment))},
        {action: new SolidifyAppAction.ChangeAppTheme(this._environment.theme)},
        {action: new SolidifyAppAction.DefineFallbackLanguage()},
        {
          action: new SolidifyAppAction.ChangeAppLanguage(UserPreferencesUtil.getPreferredLanguage(this._environment)),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SolidifyAppAction.ChangeAppLanguageSuccess)),
          ],
        },
        ...this._loadModule(),
        {
          action: new SolidifyAppAction.Login(),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SolidifyAppAction.LoginSuccess)),
            this._actions$.pipe(ofSolidifyActionErrored(SolidifyAppAction.LoginSuccess)) as any,
            this._actions$.pipe(ofSolidifyActionCompleted(SolidifyAppAction.LoginFail)),
          ],
        },
        {
          action: new SolidifyAppAction.InitApplicationParallel(),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SolidifyAppAction.InitApplicationParallelSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SolidifyAppAction.InitApplicationParallelFail)),
          ],
        },
      ],
    ).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SolidifyAppAction.InitApplicationSuccess(action, result));
        } else {
          ctx.dispatch(new SolidifyAppAction.InitApplicationFail(action, result));
        }
        return result.success;
      }),
    );
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.InitApplicationParallel)
  initApplicationParallel(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.InitApplicationParallel): Observable<boolean> {
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, this._optionsState.initApplicationParallel).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SolidifyAppAction.InitApplicationParallelSuccess(action));
        } else {
          ctx.dispatch(new SolidifyAppAction.InitApplicationParallelFail(action, result));
        }
        return result.success;
      }),
    );
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.InitApplicationParallelSuccess)
  initApplicationParallelSuccess(ctx: SolidifyStateContext<SolidifyAppStateModel>): void {
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.InitApplicationParallelFail)
  initApplicationParallelFail(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.InitApplicationParallelFail): void {
    // eslint-disable-next-line no-console
    console.error("Error during initialization of application parallel", action.result);
  }

  // Override if necessary
  protected _updateEnvironmentAllowedUrlsIfNecessary(): void {
    return;
  }

  // Override if necessary
  protected _computeHttpHeadersForLoadBackendModuleVersion(): HttpHeaders {
    return new HttpHeaders();
  }

  static getModuleInfoUrl(name: string, url: string): string {
    return this.getModuleUrl(name, url) + SOLIDIFY_CONSTANTS.URL_SEPARATOR + ApiActionNamePartialEnum.INFO;
  }

  static getModuleSwaggerUrl(name: string, url: string, swaggerHtmlFile: string = "swagger-ui.html"): string {
    return this.getModuleUrl(name, url) + SOLIDIFY_CONSTANTS.URL_SEPARATOR + swaggerHtmlFile;
  }

  static getModuleInfoDashboard(name: string, url: string): string {
    return this.getModuleUrl(name, url) + SOLIDIFY_CONSTANTS.URL_SEPARATOR + ApiActionNamePartialEnum.DASHBOARD;
  }

  static getModuleUrl(name: string, url: string): string {
    if (name !== this._AUTHORIZATION_MODULE_NAME && (url.match(/\//g) || []).length > 2) {
      url = url.substring(0, url.lastIndexOf(SOLIDIFY_CONSTANTS.URL_SEPARATOR));
    }
    return url;
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.InitApplicationSuccess)
  initApplicationSuccess(ctx: SolidifyStateContext<SolidifyAppStateModel>): void {
    ctx.patchState({
      isApplicationInitialized: true,
    });
    this.subscribe(this._appConfigService.environmentUpdatedObs.pipe(
      tap(environment => {
        this._navigateToMaintenanceIfApplicable();
      }),
    ));
    this._navigateToMaintenanceIfApplicable();
  }

  protected _navigateToMaintenanceIfApplicable(): boolean {
    if (this._environment.maintenanceMode && !this._isLoggedInRoot()) {
      this._store.dispatch(new Navigate([this._optionsState.routeMaintenance]));
      return true;
    }
    return false;
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.InitApplicationFail)
  initApplicationFail(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.InitApplicationFail): void {
    // eslint-disable-next-line no-console
    console.error("Error during initialization of application", action.result);
    if (this._navigateToMaintenanceIfApplicable()) {
      return;
    }
    if (isFalse(ctx.getState().isServerOffline)) {
      ctx.patchState({
        isUnableToLoadApp: true,
      });
      ctx.dispatch(new Navigate([this._optionsState.routeUnableToLoadApp]));
    }
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.DefineFallbackLanguage)
  defineFallbackLanguage(ctx: SolidifyStateContext<SolidifyAppStateModel>): void {
    // this language will be used as a fallback when a translation isn't found in the current language
    this._translate.setDefaultLang(this._environment.defaultLanguage);
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.SetMomentLocal)
  setMomentLocal(ctx: SolidifyStateContext<SolidifyAppStateModel>): void {
    moment.locale(this._locale);
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.ChangeAppLanguage)
  changeAppLanguage(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.ChangeAppLanguage): Observable<any> {
    return this._translate.use(action.language).pipe(
      tap(() => {
        ctx.dispatch(new SolidifyAppAction.ChangeAppLanguageSuccess(action));
      }),
    );
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.ChangeAppLanguageSuccess)
  changeAppLanguageSuccess(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.ChangeAppLanguageSuccess): void {
    ctx.patchState({
      appLanguage: action.parentAction.language,
    });
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.cookie, CookiePartialEnum.language)) {
      CookieHelper.setItem(CookiePartialEnum.language, action.parentAction.language);
    }
    ctx.dispatch(new SolidifyAppAction.SetMomentLocal());
    return this.doChangeAppLanguageSuccess(ctx, action);
  }

  // Override if necessary and type in subclass
  doChangeAppLanguageSuccess(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.ChangeAppLanguageSuccess): any {
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.ReloadAppLanguage)
  reloadAppLanguage(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.ReloadAppLanguage): Observable<any> {
    return this._translate.reloadLang(ctx.getState().appLanguage).pipe(
      tap(() => {
        ctx.dispatch(new SolidifyAppAction.ReloadAppLanguageSuccess(action));
      }),
    );
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.ReloadAppLanguageSuccess)
  reloadAppLanguageSuccess(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.ReloadAppLanguageSuccess): void {
    return this.doReloadAppLanguageSuccess(ctx, action);
  }

  // Override if necessary and type in subclass
  doReloadAppLanguageSuccess(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.ReloadAppLanguageSuccess): any {
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.ChangeAppTheme)
  changeAppTheme(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.ChangeAppTheme): Observable<boolean> {
    return this.doChangeAppTheme(ctx, action);
  }

  // Override if necessary and type in subclass
  doChangeAppTheme(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.ChangeAppTheme): Observable<boolean> {
    this._environment.theme = action.theme;
    ctx.patchState({
      theme: action.theme,
    });
    return of(true);
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.ChangeDarkMode)
  changeDarkMode(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.ChangeDarkMode): void {
    ctx.patchState({
      darkMode: action.enabled,
    });
    this._applyDarkMode(ctx, action);
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.cookie, CookiePartialEnum.darkMode)) {
      CookieHelper.setItem(CookiePartialEnum.darkMode, action.enabled ? SOLIDIFY_CONSTANTS.STRING_TRUE : SOLIDIFY_CONSTANTS.STRING_FALSE);
    }
  }

  // Override if necessary
  protected _applyDarkMode(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.ChangeDarkMode): void {
    return;
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.ChangeDisplaySidebarUserGuide)
  changeDisplaySidebarUserGuide(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.ChangeDisplaySidebarUserGuide): void {
    ctx.patchState({
      isOpenedSidebarUserGuide: action.isOpen,
    });
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.ChangeDisplayBannerCookieConsent)
  displayCookieConsent(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.ChangeDisplayBannerCookieConsent): void {
    ctx.patchState({
      isOpenedBannerCookieConsent: action.isOpen,
      newCookiesAvailable: action.newCookiesAvailable,
    });
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.ChangeDisplaySidebarCookieConsent)
  changeDisplaySidebarCookieConsent(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.ChangeDisplaySidebarCookieConsent): void {
    ctx.patchState({
      isOpenedSidebarCookieConsent: action.isOpen,
    });
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.Login)
  login(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.Login): ReturnType<this["doLogin"]> {
    if (SsrUtil.isServer) {
      ctx.dispatch(new SolidifyAppAction.LoginFail(action));
      return;
    }
    return this.doLogin(ctx, action);
  }

  // Override if necessary and type in subclass
  doLogin(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.Login): any {
    return;
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.LoginSuccess)
  loginSuccess(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.LoginSuccess): ReturnType<this["doLoginSuccess"]> {
    return this.doLoginSuccess(ctx, action);
  }

  // Override if necessary and type in subclass
  doLoginSuccess(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.LoginSuccess): any {
    return;
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.LoginFail)
  loginFail(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.LoginFail): ReturnType<this["doLoginFail"]> {
    return this.doLoginFail(ctx, action);
  }

  // Override if necessary and type in subclass
  doLoginFail(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.LoginFail): any {
    return;
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.GetAppVersion)
  getAppVersion(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.GetAppVersion): Observable<FrontendVersion> {
    const STATE_KEY = makeStateKey<FrontendVersion>(`AppStateAppVersion`);
    if (this._transferState?.hasKey(STATE_KEY)) {
      const frontendVersion = this._transferState.get(STATE_KEY, {});
      ctx.dispatch(new SolidifyAppAction.GetAppVersionSuccess(action, frontendVersion));
      this._transferState.remove(STATE_KEY);
      return of(frontendVersion);
    }

    return this._httpClient.get<FrontendVersion>("./assets/" + this._environment.frontendVersionFile + CacheBustingUtil.generateCacheBustingQueryParam())
      .pipe(
        tap(frontendVersion => {
          this._transferState?.set(STATE_KEY, frontendVersion);
          ctx.dispatch(new SolidifyAppAction.GetAppVersionSuccess(action, frontendVersion));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SolidifyAppAction.GetAppVersionFail(action));
          throw error;
        }),
      );
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.GetAppVersionSuccess)
  getAppVersionSuccess(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.GetAppVersionSuccess): void {
    ctx.patchState({
      frontendVersion: action.appVersion,
    });
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.GetAppVersionFail)
  getAppVersionFail(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.GetAppVersionFail): void {
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.Logout)
  logout(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.Logout): ReturnType<this["doLogout"]> {
    return this.doLogout(ctx, action);
  }

  // Override if necessary and type in subclass
  doLogout(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.Logout): any {
    return;
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.CancelPreventExit)
  cancelPreventExit(ctx: SolidifyStateContext<SolidifyAppStateModel>): void {
    ctx.patchState({
      preventExit: false,
      preventExitReasonToTranslate: undefined,
    });
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.PreventExit)
  preventExit(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.PreventExit): void {
    ctx.patchState({
      preventExit: true,
      preventExitReasonToTranslate: action.reasonToTranslate,
    });
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.SetIsInTourMode)
  setIsInTourMode(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.SetIsInTourMode): void {
    ctx.patchState({
      isInTourMode: action.isInTourMode,
    });
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.SetIsTouchInterface)
  setIsTouchInterface(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.SetIsTouchInterface): void {
    ctx.patchState({
      isTouchInterface: action.isTouchInterface,
    });
  }

  @RegisterDefaultAction((solidifyAppNameSpace: SolidifyAppNameSpace) => solidifyAppNameSpace.InitiateNewLogin)
  initiateNewLogin(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.InitiateNewLogin): ReturnType<this["doInitiateNewLogin"]> {
    return this.doInitiateNewLogin(ctx, action);
  }

  // Override if necessary and type in subclass
  doInitiateNewLogin(ctx: SolidifyStateContext<SolidifyAppStateModel>, action: SolidifyAppAction.InitiateNewLogin): any {
    return;
  }

}
