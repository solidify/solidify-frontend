/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - app-options.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {RoutesPartialEnum} from "../../../enums/partial/routes-partial.enum";
import {ActionSubActionCompletionsWrapper} from "../../../models/stores/base.action";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {BaseOptions} from "../base/base-options.model";
import {SolidifyAppNameSpace} from "./app-namespace.model";
import {SolidifyAppState} from "./app.state";

export interface SolidifyAppOptions extends BaseOptions {
  nameSpace: SolidifyAppNameSpace;
  routeMaintenance: ExtendEnum<RoutesPartialEnum>;
  routeUnableToLoadApp: ExtendEnum<RoutesPartialEnum>;
  urlActiveGlobalBanner: () => string;
  baseAppSate: typeof SolidifyAppState | any;
  initApplicationParallel: ActionSubActionCompletionsWrapper[];
  forceLogin?: boolean;
}
