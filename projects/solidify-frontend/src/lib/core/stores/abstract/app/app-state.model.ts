/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - app-state.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {LanguagePartialEnum} from "../../../enums/partial/language-partial.enum";
import {ThemePartialEnum} from "../../../enums/partial/theme-partial.enum";
import {FrontendVersion} from "../../../models/frontend-version.model";
import {BaseStateModel} from "../../../models/stores/base-state.model";
import {Token} from "../../../models/token.model";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";

export interface SolidifyAppStateModel extends BaseStateModel {
  isTouchInterface: boolean;
  isApplicationInitialized: boolean;
  isServerOffline: boolean;
  isUnableToLoadApp: boolean;
  isLoggedIn: boolean;
  isInTourMode: boolean;
  ignorePreventLeavePopup: boolean;
  isOpenedSidebarUserGuide: boolean;
  isOpenedSidebarCookieConsent: boolean;
  isOpenedBannerCookieConsent: boolean;
  newCookiesAvailable: number | undefined;
  appLanguage: ExtendEnum<LanguagePartialEnum>;
  theme: ExtendEnum<ThemePartialEnum>;
  darkMode: boolean;
  preventExit: boolean;
  preventExitReasonToTranslate: string | undefined;
  token: Token | undefined;
  frontendVersion: FrontendVersion | undefined;
}
