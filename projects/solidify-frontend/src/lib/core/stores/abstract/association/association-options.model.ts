/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - association-options.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ApiResourceNamePartialEnum} from "../../../enums/partial/api-resource-name-partial.enum";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {BaseResourceOptions} from "../base/base-resource-options.model";
import {AssociationNameSpace} from "./association-namespace.model";

export interface AssociationOptions extends BaseResourceOptions {
  nameSpace: AssociationNameSpace;
  resourceName: ExtendEnum<ApiResourceNamePartialEnum>;
  apiPathGetAll?: ((parentId: string) => string) | undefined;
  apiPathCreate?: ((parentId: string) => string) | undefined;
  apiPathGetById?: ((parentId: string, resId: string) => string) | undefined;
  apiPathDelete?: ((parentId: string, resId: string) => string) | undefined;
  apiPathDeleteList?: ((parentId: string) => string) | undefined;
  apiCustomParamGetAll?: ((parentId: string) => { [key: string]: string }) | undefined;
  keepCurrentStateAfterCreate?: boolean;
  keepCurrentStateAfterUpdate?: boolean;
  keepCurrentStateAfterDelete?: boolean;
  refreshAfterCreateSuccess?: boolean;
  refreshAfterCreateFail?: boolean;
  refreshAfterUpdateSuccess?: boolean;
  refreshAfterUpdateFail?: boolean;
}
