/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - association-action.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {AssociationNameSpace} from "./association-namespace.model";
import {AssociationAction} from "./association.action";

export class AssociationActionHelper {
  static getAll(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.GetAll>): AssociationAction.GetAll {
    return new associationNameSpace.GetAll(...args);
  }

  static getAllSuccess<TResource extends BaseResourceType>(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.GetAllSuccess>): AssociationAction.GetAllSuccess<TResource> {
    return new associationNameSpace.GetAllSuccess(...args);
  }

  static getAllFail(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.GetAllFail>): AssociationAction.GetAllFail {
    return new associationNameSpace.GetAllFail(...args);
  }

  static getById(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.GetById>): AssociationAction.GetById {
    return new associationNameSpace.GetById(...args);
  }

  static getByIdSuccess<TResource extends BaseResourceType>(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.GetByIdSuccess>): AssociationAction.GetByIdSuccess<TResource> {
    return new associationNameSpace.GetByIdSuccess(...args);
  }

  static getByIdFail(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.GetByIdFail>): AssociationAction.GetByIdFail {
    return new associationNameSpace.GetByIdFail(...args);
  }

  static create(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.Create>): AssociationAction.Create {
    return new associationNameSpace.Create(...args);
  }

  static createSuccess(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.CreateSuccess>): AssociationAction.CreateSuccess {
    return new associationNameSpace.CreateSuccess(...args);
  }

  static createFail(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.CreateFail>): AssociationAction.CreateFail {
    return new associationNameSpace.CreateFail(...args);
  }

  static delete(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.Delete>): AssociationAction.Delete {
    return new associationNameSpace.Delete(...args);
  }

  static deleteSuccess(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.DeleteSuccess>): AssociationAction.DeleteSuccess {
    return new associationNameSpace.DeleteSuccess(...args);
  }

  static deleteFail(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.DeleteFail>): AssociationAction.DeleteFail {
    return new associationNameSpace.DeleteFail(...args);
  }

  static deleteList(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.DeleteList>): AssociationAction.DeleteList {
    return new associationNameSpace.DeleteList(...args);
  }

  static deleteListSuccess(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.DeleteListSuccess>): AssociationAction.DeleteListSuccess {
    return new associationNameSpace.DeleteListSuccess(...args);
  }

  static deleteListFail(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.DeleteListFail>): AssociationAction.DeleteListFail {
    return new associationNameSpace.DeleteListFail(...args);
  }

  static update(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.Update>): AssociationAction.Update {
    return new associationNameSpace.Update(...args);
  }

  static updateSuccess(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.UpdateSuccess>): AssociationAction.UpdateSuccess {
    return new associationNameSpace.UpdateSuccess(...args);
  }

  static updateFail(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.UpdateFail>): AssociationAction.UpdateFail {
    return new associationNameSpace.UpdateFail(...args);
  }

  static clean(associationNameSpace: AssociationNameSpace, ...args: ConstructorParameters<typeof AssociationAction.Clean>): AssociationAction.Clean {
    return new associationNameSpace.Clean(...args);
  }
}
