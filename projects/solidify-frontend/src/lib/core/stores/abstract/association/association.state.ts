/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - association.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Inject} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {RegisterDefaultAction} from "../../../decorators/store.decorator";
import {ApiResourceNamePartialEnum} from "../../../enums/partial/api-resource-name-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {SolidifyStateError} from "../../../errors/solidify-state.error";
import {ApiService} from "../../../http/api.service";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {CollectionTyped} from "../../../models/dto/collection-typed.model";
import {SolidifyHttpErrorResponseModel} from "../../../models/errors/solidify-http-error-response.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {NotifierService} from "../../../models/services/notifier-service.model";
import {BaseResourceStateModel} from "../../../models/stores/base-resource-state.model";
import {ActionSubActionCompletionsWrapper} from "../../../models/stores/base.action";
import {SolidifyStateContext} from "../../../models/stores/state-context.model";
import {SubResourceUpdateModel} from "../../../models/stores/sub-resource-update.model";
import {
  isFalse,
  isFunction,
  isNullOrUndefined,
  isTrue,
} from "../../../../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {ofSolidifyActionCompleted} from "../../../utils/stores/store.tool";
import {StoreUtil} from "../../../utils/stores/store.util";
import {
  BaseResourceState,
  defaultBaseResourceStateInitValue,
} from "../base/base-resource.state";
import {AssociationActionHelper} from "./association-action.helper";
import {AssociationNameSpace} from "./association-namespace.model";
import {AssociationOptions} from "./association-options.model";
import {AssociationStateModel} from "./association-state.model";
import {AssociationAction} from "./association.action";

export const defaultAssociationStateInitValue: <TResourceType extends BaseResourceType> () => AssociationStateModel<TResourceType> = () =>
  ({
    ...defaultBaseResourceStateInitValue(),
    selected: undefined,
    current: undefined,
  });

// @dynamic
export abstract class AssociationState<TStateModel extends BaseResourceStateModel, TResource extends BaseResourceType> extends BaseResourceState<TStateModel> {
  protected readonly _resourceName: ExtendEnum<ApiResourceNamePartialEnum>;
  protected declare readonly _nameSpace: AssociationNameSpace;
  protected declare readonly _optionsState: AssociationOptions;

  protected constructor(protected _apiService: ApiService,
                        protected _store: Store,
                        protected _notificationService: NotifierService,
                        protected _actions$: Actions,
                        @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
                        protected _options: AssociationOptions) {
    super(_apiService, _store, _notificationService, _actions$, _environment, _options, AssociationState);
    this._resourceName = this._optionsState.resourceName;
  }

  protected static _getDefaultOptions(): AssociationOptions | any {
    let defaultOptions: AssociationOptions | any = {
      keepCurrentStateAfterCreate: false,
      keepCurrentStateAfterUpdate: false,
      keepCurrentStateAfterDelete: false,
      refreshAfterCreateFail: false,
      refreshAfterUpdateSuccess: false,
      refreshAfterUpdateFail: false,
    } as AssociationOptions;
    defaultOptions = Object.assign(BaseResourceState._getDefaultOptions(), defaultOptions);
    return defaultOptions;
  }

  private _evaluateSubResourceUrl(parentId: string): string {
    return this._urlResource + SOLIDIFY_CONSTANTS.URL_SEPARATOR + parentId + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this._resourceName;
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.GetAll)
  getAll<U>(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.GetAll): Observable<CollectionTyped<U>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        total: 0,
        selected: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    return this._getAll<U>(ctx, action)
      .pipe(
        tap((collection: CollectionTyped<U>) => {
          ctx.dispatch(AssociationActionHelper.getAllSuccess<U>(this._nameSpace, action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(AssociationActionHelper.getAllFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  protected override _callGetAllEndpoint<U>(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.GetAll, queryParameters: QueryParameters): Observable<CollectionTyped<U>> {
    const url = isFunction(this._optionsState.apiPathGetAll) ? this._optionsState.apiPathGetAll(action.parentId) : this._evaluateSubResourceUrl(action.parentId);
    const customParameters = isFunction(this._optionsState.apiCustomParamGetAll) ? this._optionsState.apiCustomParamGetAll(action.parentId) : undefined;
    return this._apiService.getCollection<U>(url, queryParameters, customParameters);
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.GetAllSuccess<TResource>): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.list);

    ctx.patchState({
      selected: action.list._data,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: queryParameters,
    });
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.GetAllFail)
  getAllFail(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.GetAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.GetById)
  getById<U>(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.GetById): Observable<U> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        current: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });

    let apiResultObs = undefined;
    if (isFunction(this._optionsState.apiPathGetById)) {
      apiResultObs = this._apiService.get<TResource>(this._optionsState.apiPathGetById(action.parentId, action.resId));
    } else {
      apiResultObs = this._apiService.getById<TResource>(this._evaluateSubResourceUrl(action.parentId), action.resId);
    }
    return apiResultObs
      .pipe(
        tap((model: U) => {
          ctx.dispatch(AssociationActionHelper.getByIdSuccess<U>(this._nameSpace, action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(AssociationActionHelper.getByIdFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.GetByIdSuccess<TResource>): void {
    ctx.patchState({
      current: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.GetByIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.Create)
  create(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.Create): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    if (action.listResId.length === 0) {
      ctx.dispatch(AssociationActionHelper.createSuccess(this._nameSpace, action));
      return;
    }
    const url = isFunction(this._optionsState.apiPathCreate) ? this._optionsState.apiPathCreate(action.parentId) : this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.post<string[]>(url, action.listResId)
      .pipe(
        tap(() => {
          ctx.dispatch(AssociationActionHelper.createSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(AssociationActionHelper.createFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.CreateSuccess)
  createSuccess(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.CreateSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._cleanCurrentStateIfDefined(ctx, this._optionsState.keepCurrentStateAfterCreate);
    if (isTrue(this._optionsState.refreshAfterCreateSuccess)) {
      ctx.dispatch(AssociationActionHelper.getAll(this._nameSpace, action.parentAction.parentId));
    }
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.CreateFail)
  createFail(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.CreateFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    if (isTrue(this._optionsState.refreshAfterCreateFail)) {
      ctx.dispatch(AssociationActionHelper.getAll(this._nameSpace, action.parentAction.parentId));
    }
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.Delete)
  delete(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.Delete): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    if (isNullOrUndefined(action.resId)) {
      ctx.dispatch(AssociationActionHelper.deleteSuccess(this._nameSpace, action));
      return;
    }
    const url = isFunction(this._optionsState.apiPathDelete) ? this._optionsState.apiPathDelete(action.parentId, action.resId) : this._evaluateSubResourceUrl(action.parentId) + SOLIDIFY_CONSTANTS.URL_SEPARATOR + action.resId;
    return this._apiService.delete<string[]>(url)
      .pipe(
        tap(() => {
          ctx.dispatch(AssociationActionHelper.deleteSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(AssociationActionHelper.deleteFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.DeleteSuccess)
  deleteSuccess(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.DeleteSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._cleanCurrentStateIfDefined(ctx, this._optionsState.keepCurrentStateAfterDelete);
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.DeleteFail)
  deleteFail(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.DeleteFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.DeleteList)
  deleteList(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.DeleteList): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    if (action.listResId.length === 0) {
      ctx.dispatch(AssociationActionHelper.deleteListSuccess(this._nameSpace, action));
      return;
    }
    const url = isFunction(this._optionsState.apiPathDeleteList) ? this._optionsState.apiPathDeleteList(action.parentId) : this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.delete<string[]>(url, action.listResId)
      .pipe(
        tap(() => {
          ctx.dispatch(AssociationActionHelper.deleteListSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(AssociationActionHelper.deleteListFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.DeleteListSuccess)
  deleteListSuccess(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.DeleteListSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.DeleteListFail)
  deleteListFail(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.DeleteListFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.Update)
  update(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.Update): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const listOldResId = isNullOrUndefined(ctx.getState().selected) ? [] : ctx.getState().selected.map(s => s.resId);
    const listNewResId = action.newResId;

    const subResourceUpdate: SubResourceUpdateModel = StoreUtil.determineSubResourceChange(listOldResId, listNewResId);
    this.subscribe(StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(
        ctx,
        this._computeUpdatesActionsToDispatch(subResourceUpdate, action.parentId),
      ).pipe(
        tap(result => {
          if (result.success) {
            ctx.dispatch(AssociationActionHelper.updateSuccess(this._nameSpace, action, action.parentId));
          } else {
            ctx.dispatch(AssociationActionHelper.updateFail(this._nameSpace, action, action.parentId));
          }
        }),
      ),
    );
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.UpdateSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      selected: undefined,
    });
    this._cleanCurrentStateIfDefined(ctx, this._optionsState.keepCurrentStateAfterUpdate);
    if (isTrue(this._optionsState.refreshAfterUpdateSuccess)) {
      ctx.dispatch(AssociationActionHelper.getAll(this._nameSpace, action.parentId));
    }
  }

  @RegisterDefaultAction((associationNameSpace: AssociationNameSpace) => associationNameSpace.UpdateFail)
  updateFail(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, action: AssociationAction.UpdateFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    if (isTrue(this._optionsState.refreshAfterUpdateFail)) {
      ctx.dispatch(AssociationActionHelper.getAll(this._nameSpace, action.parentId));
    }
  }

  private _computeUpdatesActionsToDispatch(subResourceUpdate: SubResourceUpdateModel, parentId: string): ActionSubActionCompletionsWrapper<AssociationAction.Create | AssociationAction.Delete>[] {
    return [
      {
        action: AssociationActionHelper.deleteList(this._nameSpace, parentId, subResourceUpdate.resourceToRemoved),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.DeleteListSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.DeleteListFail)),
        ],
      },
      {
        action: AssociationActionHelper.create(this._nameSpace, parentId, subResourceUpdate.resourceToAdd),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.CreateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.CreateFail)),
        ],
      },
    ];
  }

  private _cleanCurrentStateIfDefined(ctx: SolidifyStateContext<AssociationStateModel<TResource>>, keepCurrentState: boolean): void {
    if (isNullOrUndefined(keepCurrentState) || isFalse(keepCurrentState)) {
      ctx.dispatch(AssociationActionHelper.clean(this._nameSpace, true));
    }
  }
}
