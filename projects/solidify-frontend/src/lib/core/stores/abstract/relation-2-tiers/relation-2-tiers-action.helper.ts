/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - relation-2-tiers-action.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {BaseRelationResourceType} from "../../../models/dto/base-relation-resource.model";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {Relation2TiersNameSpace} from "./relation-2-tiers-namespace.model";
import {Relation2TiersAction} from "./relation-2-tiers.action";

export class Relation2TiersActionHelper {
  static getAll(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.GetAll>): Relation2TiersAction.GetAll {
    return new relation2TiersNameSpace.GetAll(...args);
  }

  static getAllSuccess<TResource extends BaseResourceType>(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.GetAllSuccess>): Relation2TiersAction.GetAllSuccess<TResource> {
    return new relation2TiersNameSpace.GetAllSuccess(...args);
  }

  static getAllFail(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.GetAllFail>): Relation2TiersAction.GetAllFail {
    return new relation2TiersNameSpace.GetAllFail(...args);
  }

  static getById(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.GetById>): Relation2TiersAction.GetById {
    return new relation2TiersNameSpace.GetById(...args);
  }

  static getByIdSuccess<TResource extends BaseResourceType>(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.GetByIdSuccess>): Relation2TiersAction.GetByIdSuccess<TResource> {
    return new relation2TiersNameSpace.GetByIdSuccess(...args);
  }

  static getByIdFail(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.GetByIdFail>): Relation2TiersAction.GetByIdFail {
    return new relation2TiersNameSpace.GetByIdFail(...args);
  }

  static create(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.Create>): Relation2TiersAction.Create {
    return new relation2TiersNameSpace.Create(...args);
  }

  static createSuccess(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.CreateSuccess>): Relation2TiersAction.CreateSuccess {
    return new relation2TiersNameSpace.CreateSuccess(...args);
  }

  static createFail(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.CreateFail>): Relation2TiersAction.CreateFail {
    return new relation2TiersNameSpace.CreateFail(...args);
  }

  static deleteList(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.DeleteList>): Relation2TiersAction.DeleteList {
    return new relation2TiersNameSpace.DeleteList(...args);
  }

  static deleteListSuccess(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.DeleteListSuccess>): Relation2TiersAction.DeleteListSuccess {
    return new relation2TiersNameSpace.DeleteListSuccess(...args);
  }

  static deleteListFail(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.DeleteListFail>): Relation2TiersAction.DeleteListFail {
    return new relation2TiersNameSpace.DeleteListFail(...args);
  }

  static delete(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.Delete>): Relation2TiersAction.Delete {
    return new relation2TiersNameSpace.Delete(...args);
  }

  static deleteSuccess(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.DeleteSuccess>): Relation2TiersAction.DeleteSuccess {
    return new relation2TiersNameSpace.DeleteSuccess(...args);
  }

  static deleteFail(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.DeleteFail>): Relation2TiersAction.DeleteFail {
    return new relation2TiersNameSpace.DeleteFail(...args);
  }

  static update(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.Update>): Relation2TiersAction.Update {
    return new relation2TiersNameSpace.Update(...args);
  }

  static updateSuccess(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.UpdateSuccess>): Relation2TiersAction.UpdateSuccess {
    return new relation2TiersNameSpace.UpdateSuccess(...args);
  }

  static updateFail(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.UpdateFail>): Relation2TiersAction.UpdateFail {
    return new relation2TiersNameSpace.UpdateFail(...args);
  }

  static updateRelation<TRelation extends BaseRelationResourceType>(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.UpdateRelation>): Relation2TiersAction.UpdateRelation<TRelation> {
    return new relation2TiersNameSpace.UpdateRelation(...args);
  }

  static updateRelationSuccess<TRelation extends BaseRelationResourceType>(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.UpdateRelationSuccess>): Relation2TiersAction.UpdateRelationSuccess<TRelation> {
    return new relation2TiersNameSpace.UpdateRelationSuccess(...args);
  }

  static updateRelationFail<TRelation extends BaseRelationResourceType>(relation2TiersNameSpace: Relation2TiersNameSpace, ...args: ConstructorParameters<typeof Relation2TiersAction.UpdateRelationFail>): Relation2TiersAction.UpdateRelationFail<TRelation> {
    return new relation2TiersNameSpace.UpdateRelationFail(...args);
  }
}
