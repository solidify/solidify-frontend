/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - relation-2-tiers.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Inject} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {RegisterDefaultAction} from "../../../decorators/store.decorator";
import {ApiResourceNamePartialEnum} from "../../../enums/partial/api-resource-name-partial.enum";
import {UpdateOrderEnum} from "../../../enums/update-order.enum";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {SolidifyStateError} from "../../../errors/solidify-state.error";
import {ApiService} from "../../../http/api.service";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {BaseRelationResourceType} from "../../../models/dto/base-relation-resource.model";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {CollectionTyped} from "../../../models/dto/collection-typed.model";
import {SolidifyHttpErrorResponseModel} from "../../../models/errors/solidify-http-error-response.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {NotifierService} from "../../../models/services/notifier-service.model";
import {BaseResourceStateModel} from "../../../models/stores/base-resource-state.model";
import {ActionSubActionCompletionsWrapper} from "../../../models/stores/base.action";
import {SolidifyStateContext} from "../../../models/stores/state-context.model";
import {SubResourceUpdateModel} from "../../../models/stores/sub-resource-update.model";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {ofSolidifyActionCompleted} from "../../../utils/stores/store.tool";
import {StoreUtil} from "../../../utils/stores/store.util";
import {
  BaseResourceState,
  defaultBaseResourceStateInitValue,
} from "../base/base-resource.state";
import {Relation2TiersActionHelper} from "./relation-2-tiers-action.helper";
import {Relation2TiersNameSpace} from "./relation-2-tiers-namespace.model";
import {Relation2TiersOptions} from "./relation-2-tiers-options.model";
import {Relation2TiersStateModel} from "./relation-2-tiers-state.model";
import {Relation2TiersAction} from "./relation-2-tiers.action";

export const defaultRelation2TiersStateInitValue: <TResourceType extends BaseResourceType> () => Relation2TiersStateModel<TResourceType> = () =>
  ({
    ...defaultBaseResourceStateInitValue(),
    selected: undefined,
    current: undefined,
  });

// @dynamic
export abstract class Relation2TiersState<TStateModel extends BaseResourceStateModel, TResource extends BaseResourceType, TRelation extends BaseRelationResourceType> extends BaseResourceState<TStateModel> {
  protected readonly _resourceName: ExtendEnum<ApiResourceNamePartialEnum>;
  protected declare readonly _nameSpace: Relation2TiersNameSpace;
  protected declare readonly _optionsState: Relation2TiersOptions;

  protected constructor(protected _apiService: ApiService,
                        protected _store: Store,
                        protected _notificationService: NotifierService,
                        protected _actions$: Actions,
                        @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
                        protected _options: Relation2TiersOptions) {
    super(_apiService, _store, _notificationService, _actions$, _environment, _options, Relation2TiersState);
    this._resourceName = this._optionsState.resourceName;
  }

  protected static _getDefaultOptions(): Relation2TiersOptions | any {
    let defaultOptions: Relation2TiersOptions | any = {
      updateOrder: UpdateOrderEnum.deleteBeforeCreate,
    } as Relation2TiersOptions;
    defaultOptions = Object.assign(BaseResourceState._getDefaultOptions(), defaultOptions);
    return defaultOptions;
  }

  private _evaluateSubResourceUrl(parentId: string): string {
    return this._urlResource + SOLIDIFY_CONSTANTS.URL_SEPARATOR + parentId + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this._resourceName;
  }

  @RegisterDefaultAction((relation2TiersNameSpace: Relation2TiersNameSpace) => relation2TiersNameSpace.GetAll)
  getAll<U>(ctx: SolidifyStateContext<Relation2TiersStateModel<TResource>>, action: Relation2TiersAction.GetAll): Observable<CollectionTyped<U>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        total: 0,
        selected: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    return this._getAll<U>(ctx, action)
      .pipe(
        tap((collection: CollectionTyped<U>) => {
          ctx.dispatch(Relation2TiersActionHelper.getAllSuccess<U>(this._nameSpace, action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(Relation2TiersActionHelper.getAllFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  protected override _callGetAllEndpoint<U>(ctx: SolidifyStateContext<Relation2TiersStateModel<TResource>>, action: Relation2TiersAction.GetAll, queryParameters: QueryParameters): Observable<CollectionTyped<U>> {
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.getCollection<U>(url, queryParameters);
  }

  @RegisterDefaultAction((relation2TiersNameSpace: Relation2TiersNameSpace) => relation2TiersNameSpace.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<Relation2TiersStateModel<TResource>>, action: Relation2TiersAction.GetAllSuccess<TResource>): void {
    ctx.patchState({
      selected: action.list._data,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((relation2TiersNameSpace: Relation2TiersNameSpace) => relation2TiersNameSpace.GetAllFail)
  getAllFail(ctx: SolidifyStateContext<Relation2TiersStateModel<TResource>>, action: Relation2TiersAction.GetAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((relation2TiersNameSpace: Relation2TiersNameSpace) => relation2TiersNameSpace.GetById)
  getById<U>(ctx: SolidifyStateContext<Relation2TiersStateModel<TResource>>, action: Relation2TiersAction.GetById): Observable<U> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        current: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.getById<U>(url, action.resId)
      .pipe(
        tap((model: U) => {
          ctx.dispatch(Relation2TiersActionHelper.getByIdSuccess<U>(this._nameSpace, action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(Relation2TiersActionHelper.getByIdFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((relation2TiersNameSpace: Relation2TiersNameSpace) => relation2TiersNameSpace.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<Relation2TiersStateModel<TResource>>, action: Relation2TiersAction.GetByIdSuccess<TResource>): void {
    ctx.patchState({
      current: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((relation2TiersNameSpace: Relation2TiersNameSpace) => relation2TiersNameSpace.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<Relation2TiersStateModel<TResource>>, action: Relation2TiersAction.GetByIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((relation2TiersNameSpace: Relation2TiersNameSpace) => relation2TiersNameSpace.Create)
  create(ctx: SolidifyStateContext<Relation2TiersStateModel<TResource>>, action: Relation2TiersAction.Create): Observable<string[]> {
    if (action.listResId.length === 0) {
      ctx.dispatch(Relation2TiersActionHelper.createSuccess(this._nameSpace, action));
      return;
    }
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.post<string[]>(url, action.listResId)
      .pipe(
        tap(() => {
          ctx.dispatch(Relation2TiersActionHelper.createSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(Relation2TiersActionHelper.createFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((relation2TiersNameSpace: Relation2TiersNameSpace) => relation2TiersNameSpace.DeleteList)
  deleteList(ctx: SolidifyStateContext<Relation2TiersStateModel<TResource>>, action: Relation2TiersAction.DeleteList): Observable<string[]> {
    if (action.listResId.length === 0) {
      ctx.dispatch(Relation2TiersActionHelper.deleteListSuccess(this._nameSpace, action));
      return;
    }
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.delete<string[]>(url, action.listResId)
      .pipe(
        tap(() => {
          ctx.dispatch(Relation2TiersActionHelper.deleteListSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(Relation2TiersActionHelper.deleteListFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((relation2TiersNameSpace: Relation2TiersNameSpace) => relation2TiersNameSpace.Delete)
  delete(ctx: SolidifyStateContext<Relation2TiersStateModel<TResource>>, action: Relation2TiersAction.Delete): Observable<number> {
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.delete<number>(url + SOLIDIFY_CONSTANTS.URL_SEPARATOR + action.resId)
      .pipe(
        tap(() => {
          ctx.dispatch(Relation2TiersActionHelper.deleteSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(Relation2TiersActionHelper.deleteFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((relation2TiersNameSpace: Relation2TiersNameSpace) => relation2TiersNameSpace.UpdateRelation)
  updateRelation(ctx: SolidifyStateContext<Relation2TiersStateModel<TResource>>, action: Relation2TiersAction.UpdateRelation<TRelation>): Observable<TRelation> {
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.patchById<TRelation>(url, action.resId, action.relation)
      .pipe(
        tap(() => {
          ctx.dispatch(Relation2TiersActionHelper.updateRelationSuccess<TRelation>(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(Relation2TiersActionHelper.updateRelationFail<TRelation>(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((relation2TiersNameSpace: Relation2TiersNameSpace) => relation2TiersNameSpace.Update)
  update(ctx: SolidifyStateContext<Relation2TiersStateModel<TResource>>, action: Relation2TiersAction.Update): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    this.subscribe(this._internalUpdate(ctx, action).pipe(tap(success => {
      if (success) {
        ctx.dispatch(Relation2TiersActionHelper.updateSuccess(this._nameSpace, action, action.parentId));
      } else {
        ctx.dispatch(Relation2TiersActionHelper.updateFail(this._nameSpace, action, action.parentId));
      }
    })));
  }

  protected _internalUpdate(ctx: SolidifyStateContext<Relation2TiersStateModel<TResource>>, action: Relation2TiersAction.Update): Observable<boolean> {
    const listOldResId = isNullOrUndefined(ctx.getState().selected) ? [] : ctx.getState().selected.map(s => s.resId);
    const listNewResId = action.newResId;

    const subResourceUpdate: SubResourceUpdateModel = StoreUtil.determineSubResourceChange(listOldResId, listNewResId);
    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(
      ctx,
      this._computeUpdatesActionsToDispatch(subResourceUpdate, action.parentId),
    ).pipe(
      map(result => result.success),
    );
  }

  @RegisterDefaultAction((relation2TiersNameSpace: Relation2TiersNameSpace) => relation2TiersNameSpace.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<Relation2TiersStateModel<TResource>>, action: Relation2TiersAction.UpdateSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      selected: undefined,
    });
  }

  @RegisterDefaultAction((relation2TiersNameSpace: Relation2TiersNameSpace) => relation2TiersNameSpace.UpdateFail)
  updateFail(ctx: SolidifyStateContext<Relation2TiersStateModel<TResource>>, action: Relation2TiersAction.UpdateFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(Relation2TiersActionHelper.getAll(this._nameSpace, action.parentId));
  }

  private _computeUpdatesActionsToDispatch(subResourceUpdate: SubResourceUpdateModel, parentId: string): ActionSubActionCompletionsWrapper<Relation2TiersAction.Create | Relation2TiersAction.DeleteList>[] {
    const createAction = {
      action: Relation2TiersActionHelper.create(this._nameSpace, parentId, subResourceUpdate.resourceToAdd),
      subActionCompletions: [
        this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.CreateSuccess)),
        this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.CreateFail)),
      ],
    };

    const deleteListAction = {
      action: Relation2TiersActionHelper.deleteList(this._nameSpace, parentId, subResourceUpdate.resourceToRemoved),
      subActionCompletions: [
        this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.DeleteListSuccess)),
        this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.DeleteListFail)),
      ],
    };

    if (this._optionsState.updateOrder === UpdateOrderEnum.deleteBeforeCreate) {
      return [deleteListAction, createAction];
    } else {
      return [createAction, deleteListAction];
    }
  }
}
