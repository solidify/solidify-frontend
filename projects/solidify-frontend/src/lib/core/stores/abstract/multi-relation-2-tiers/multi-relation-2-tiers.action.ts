/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - multi-relation-2-tiers.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {BaseRelationResourceType} from "../../../models/dto/base-relation-resource.model";
import {BaseResourceWithRelationType} from "../../../models/dto/base-resource-with-relation.model";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {CollectionTyped} from "../../../models/dto/collection-typed.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
} from "../../../models/stores/base.action";
import {MultiRelation2TiersNameSpace} from "./multi-relation-2-tiers-namespace.model";

export namespace MultiRelation2TiersAction {
  export class GetAll extends BaseAction {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Get All";

    constructor(public parentId: string, public queryParameters?: QueryParameters, public keepCurrentContext: boolean = false) {
      super();
    }
  }

  export class GetAllSuccess<TResource extends BaseResourceType> extends BaseSubActionSuccess<GetAll> {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Get All Success";

    constructor(public parentAction: GetAll, public list: CollectionTyped<TResource>) {
      super(parentAction);
    }
  }

  export class GetAllFail extends BaseSubActionFail<GetAll> {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Get All Fail";
  }

  export class GetById extends BaseAction {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Get By Id";

    constructor(public parentId: string, public resId: string, public keepCurrentContext: boolean = false) {
      super();
    }
  }

  export class GetByIdSuccess<TResource extends BaseResourceType> extends BaseSubActionSuccess<GetById> {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Get By Id Success";

    constructor(public parentAction: GetById, public model: TResource) {
      super(parentAction);
    }
  }

  export class GetByIdFail extends BaseSubActionFail<GetById> {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Get By Id Fail";
  }

  export class Create<URelation extends BaseRelationResourceType> extends BaseAction {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Create";

    constructor(public parentId: string, public resId: string, public relation: URelation) {
      super();
    }
  }

  export class CreateSuccess<TResource extends BaseResourceWithRelationType<URelation>, URelation extends BaseRelationResourceType> extends BaseSubActionSuccess<Create<URelation>> {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Create Success";

    constructor(public parentAction: Create<URelation>, public result: TResource) {
      super(parentAction);
    }
  }

  export class CreateFail<URelation extends BaseRelationResourceType> extends BaseSubActionFail<Create<URelation>> {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Create Fail";
  }

  export class DeleteList extends BaseAction {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Delete List";

    constructor(public parentId: string, public listResId: string[]) {
      super();
    }
  }

  export class DeleteListSuccess extends BaseSubActionSuccess<DeleteList> {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Delete ListSuccess";
  }

  export class DeleteListFail extends BaseSubActionFail<DeleteList> {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Delete ListFail";
  }

  export class Delete extends BaseAction {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Delete";

    constructor(public parentId: string, public resId: string, public compositeKey: string) {
      super();
    }
  }

  export class DeleteSuccess extends BaseSubActionSuccess<Delete> {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Delete Success";
  }

  export class DeleteFail extends BaseSubActionFail<Delete> {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Delete Fail";
  }

  export class Update<TResource extends BaseResourceWithRelationType<URelation>, URelation extends BaseRelationResourceType> extends BaseAction {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Update";

    constructor(public parentId: string, public newRelation: TResource[]) {
      super();
    }
  }

  export class UpdateSuccess<TResource extends BaseResourceWithRelationType<URelation>, URelation extends BaseRelationResourceType> extends BaseSubActionSuccess<Update<TResource, URelation>> {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Update Success";

    constructor(public parentAction: Update<TResource, URelation>, public parentId: string) {
      super(parentAction);
    }
  }

  export class UpdateFail<TResource extends BaseResourceWithRelationType<URelation>, URelation extends BaseRelationResourceType> extends BaseSubActionFail<Update<TResource, URelation>> {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Update Fail";

    constructor(public parentAction: Update<TResource, URelation>, public parentId: string) {
      super(parentAction);
    }
  }

  export class UpdateRelation<URelation extends BaseRelationResourceType> extends BaseAction {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Update Relation";

    constructor(public parentId: string, public resId: string, public relation: URelation) {
      super();
    }
  }

  export class UpdateRelationSuccess<URelation extends BaseRelationResourceType> extends BaseSubActionSuccess<UpdateRelation<URelation>> {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Update Relation Success";

    constructor(public parentAction: UpdateRelation<URelation>) {
      super(parentAction);
    }
  }

  export class UpdateRelationFail<URelation extends BaseRelationResourceType> extends BaseSubActionFail<UpdateRelation<URelation>> {
    static readonly type: string = "[{0}] Multi Relation 2 Tiers : Update Relation Fail";

    constructor(public parentAction: UpdateRelation<URelation>) {
      super(parentAction);
    }
  }
}

export const multiRelation2TiersActionNameSpace: MultiRelation2TiersNameSpace = MultiRelation2TiersAction;
