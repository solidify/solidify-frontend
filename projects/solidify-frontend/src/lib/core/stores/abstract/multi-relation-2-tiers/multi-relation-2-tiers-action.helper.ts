/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - multi-relation-2-tiers-action.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {BaseRelationResourceType} from "../../../models/dto/base-relation-resource.model";
import {BaseResourceWithRelationType} from "../../../models/dto/base-resource-with-relation.model";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {MultiRelation2TiersNameSpace} from "./multi-relation-2-tiers-namespace.model";
import {MultiRelation2TiersAction} from "./multi-relation-2-tiers.action";

export class MultiRelation2TiersActionHelper {
  static getAll(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.GetAll>): MultiRelation2TiersAction.GetAll {
    return new relation2TiersNameSpace.GetAll(...args);
  }

  static getAllSuccess<TResource extends BaseResourceType>(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.GetAllSuccess>): MultiRelation2TiersAction.GetAllSuccess<TResource> {
    return new relation2TiersNameSpace.GetAllSuccess(...args);
  }

  static getAllFail(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.GetAllFail>): MultiRelation2TiersAction.GetAllFail {
    return new relation2TiersNameSpace.GetAllFail(...args);
  }

  static getById(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.GetById>): MultiRelation2TiersAction.GetById {
    return new relation2TiersNameSpace.GetById(...args);
  }

  static getByIdSuccess<TResource extends BaseResourceType>(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.GetByIdSuccess>): MultiRelation2TiersAction.GetByIdSuccess<TResource> {
    return new relation2TiersNameSpace.GetByIdSuccess(...args);
  }

  static getByIdFail(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.GetByIdFail>): MultiRelation2TiersAction.GetByIdFail {
    return new relation2TiersNameSpace.GetByIdFail(...args);
  }

  static create<URelation extends BaseRelationResourceType>(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.Create>): MultiRelation2TiersAction.Create<URelation> {
    return new relation2TiersNameSpace.Create(...args);
  }

  static createSuccess<TResource extends BaseResourceWithRelationType<URelation>, URelation extends BaseRelationResourceType>(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.CreateSuccess>): MultiRelation2TiersAction.CreateSuccess<TResource, URelation> {
    return new relation2TiersNameSpace.CreateSuccess(...args);
  }

  static createFail<URelation extends BaseRelationResourceType>(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.CreateFail>): MultiRelation2TiersAction.CreateFail<URelation> {
    return new relation2TiersNameSpace.CreateFail(...args);
  }

  static deleteList(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.DeleteList>): MultiRelation2TiersAction.DeleteList {
    return new relation2TiersNameSpace.DeleteList(...args);
  }

  static deleteListSuccess(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.DeleteListSuccess>): MultiRelation2TiersAction.DeleteListSuccess {
    return new relation2TiersNameSpace.DeleteListSuccess(...args);
  }

  static deleteListFail(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.DeleteListFail>): MultiRelation2TiersAction.DeleteListFail {
    return new relation2TiersNameSpace.DeleteListFail(...args);
  }

  static delete(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.Delete>): MultiRelation2TiersAction.Delete {
    return new relation2TiersNameSpace.Delete(...args);
  }

  static deleteSuccess(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.DeleteSuccess>): MultiRelation2TiersAction.DeleteSuccess {
    return new relation2TiersNameSpace.DeleteSuccess(...args);
  }

  static deleteFail(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.DeleteFail>): MultiRelation2TiersAction.DeleteFail {
    return new relation2TiersNameSpace.DeleteFail(...args);
  }

  static update<TResource extends BaseResourceWithRelationType<URelation>, URelation extends BaseRelationResourceType>(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.Update>): MultiRelation2TiersAction.Update<TResource, URelation> {
    return new relation2TiersNameSpace.Update(...args);
  }

  static updateSuccess<TResource extends BaseResourceWithRelationType<URelation>, URelation extends BaseRelationResourceType>(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.UpdateSuccess>): MultiRelation2TiersAction.UpdateSuccess<TResource, URelation> {
    return new relation2TiersNameSpace.UpdateSuccess(...args);
  }

  static updateFail<TResource extends BaseResourceWithRelationType<URelation>, URelation extends BaseRelationResourceType>(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.UpdateFail>): MultiRelation2TiersAction.UpdateFail<TResource, URelation> {
    return new relation2TiersNameSpace.UpdateFail(...args);
  }

  static updateRelation<TRelation extends BaseRelationResourceType>(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.UpdateRelation>): MultiRelation2TiersAction.UpdateRelation<TRelation> {
    return new relation2TiersNameSpace.UpdateRelation(...args);
  }

  static updateRelationSuccess<TRelation extends BaseRelationResourceType>(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.UpdateRelationSuccess>): MultiRelation2TiersAction.UpdateRelationSuccess<TRelation> {
    return new relation2TiersNameSpace.UpdateRelationSuccess(...args);
  }

  static updateRelationFail<TRelation extends BaseRelationResourceType>(relation2TiersNameSpace: MultiRelation2TiersNameSpace, ...args: ConstructorParameters<typeof MultiRelation2TiersAction.UpdateRelationFail>): MultiRelation2TiersAction.UpdateRelationFail<TRelation> {
    return new relation2TiersNameSpace.UpdateRelationFail(...args);
  }
}
