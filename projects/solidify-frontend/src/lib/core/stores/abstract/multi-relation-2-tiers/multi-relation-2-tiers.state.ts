/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - multi-relation-2-tiers.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Inject} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {
  isNonEmptyArray,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
} from "../../../../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {ArrayUtil} from "../../../../core-resources/utils/array.util";
import {ObjectUtil} from "../../../../core-resources/utils/object.util";
import {RegisterDefaultAction} from "../../../decorators/store.decorator";
import {ApiResourceNamePartialEnum} from "../../../enums/partial/api-resource-name-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {SolidifyStateError} from "../../../errors/solidify-state.error";
import {ApiService} from "../../../http/api.service";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {BaseRelationResourceType} from "../../../models/dto/base-relation-resource.model";
import {BaseResourceWithRelationType} from "../../../models/dto/base-resource-with-relation.model";
import {CollectionTyped} from "../../../models/dto/collection-typed.model";
import {SolidifyHttpErrorResponseModel} from "../../../models/errors/solidify-http-error-response.model";
import {NotifierService} from "../../../models/services/notifier-service.model";
import {BaseResourceStateModel} from "../../../models/stores/base-resource-state.model";
import {ActionSubActionCompletionsWrapper} from "../../../models/stores/base.action";
import {SolidifyStateContext} from "../../../models/stores/state-context.model";
import {ofSolidifyActionCompleted} from "../../../utils/stores/store.tool";
import {StoreUtil} from "../../../utils/stores/store.util";
import {
  BaseResourceState,
  defaultBaseResourceStateInitValue,
} from "../base/base-resource.state";
import {MultiRelation2TiersActionHelper} from "./multi-relation-2-tiers-action.helper";
import {MultiRelation2TiersNameSpace} from "./multi-relation-2-tiers-namespace.model";
import {MultiRelation2TiersOptions} from "./multi-relation-2-tiers-options.model";
import {MultiRelation2TiersStateModel} from "./multi-relation-2-tiers-state.model";
import {MultiRelation2TiersAction} from "./multi-relation-2-tiers.action";

export const defaultMultiRelation2TiersStateInitValue: <TResourceType extends BaseResourceType> () => MultiRelation2TiersStateModel<TResourceType> = () =>
  ({
    ...defaultBaseResourceStateInitValue(),
    selected: undefined,
    current: undefined,
  });

// @dynamic
export abstract class MultiRelation2TiersState<TStateModel extends BaseResourceStateModel, TResource extends BaseResourceWithRelationType<URelation>, URelation extends BaseRelationResourceType> extends BaseResourceState<TStateModel> {
  protected readonly _resourceName: ExtendEnum<ApiResourceNamePartialEnum>;
  protected declare readonly _nameSpace: MultiRelation2TiersNameSpace;
  protected declare readonly _optionsState: MultiRelation2TiersOptions;

  protected constructor(protected _apiService: ApiService,
                        protected _store: Store,
                        protected _notificationService: NotifierService,
                        protected _actions$: Actions,
                        @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
                        protected _options: MultiRelation2TiersOptions) {
    super(_apiService, _store, _notificationService, _actions$, _environment, _options, MultiRelation2TiersState);
    this._resourceName = this._optionsState.resourceName;
  }

  protected static _getDefaultOptions(): MultiRelation2TiersOptions | any {
    let defaultOptions: MultiRelation2TiersOptions | any = {
      joinResourceIdRequestParamName: "joinResourceId",
    } as MultiRelation2TiersOptions;
    defaultOptions = Object.assign(BaseResourceState._getDefaultOptions(), defaultOptions);
    return defaultOptions;
  }

  private _evaluateSubResourceUrl(parentId: string): string {
    return this._urlResource + SOLIDIFY_CONSTANTS.URL_SEPARATOR + parentId + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this._resourceName;
  }

  @RegisterDefaultAction((multiRelation2TiersNameSpace: MultiRelation2TiersNameSpace) => multiRelation2TiersNameSpace.GetAll)
  getAll<U>(ctx: SolidifyStateContext<MultiRelation2TiersStateModel<TResource>>, action: MultiRelation2TiersAction.GetAll): Observable<CollectionTyped<U>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        total: 0,
        selected: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    return this._getAll<U>(ctx, action)
      .pipe(
        tap((collection: CollectionTyped<U>) => {
          ctx.dispatch(MultiRelation2TiersActionHelper.getAllSuccess<U>(this._nameSpace, action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(MultiRelation2TiersActionHelper.getAllFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  protected override _callGetAllEndpoint<U>(ctx: SolidifyStateContext<MultiRelation2TiersStateModel<TResource>>, action: MultiRelation2TiersAction.GetAll, queryParameters: QueryParameters): Observable<CollectionTyped<U>> {
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.getCollection<U>(url, queryParameters);
  }

  @RegisterDefaultAction((multiRelation2TiersNameSpace: MultiRelation2TiersNameSpace) => multiRelation2TiersNameSpace.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<MultiRelation2TiersStateModel<TResource>>, action: MultiRelation2TiersAction.GetAllSuccess<TResource>): void {
    ctx.patchState({
      selected: action.list._data,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((multiRelation2TiersNameSpace: MultiRelation2TiersNameSpace) => multiRelation2TiersNameSpace.GetAllFail)
  getAllFail(ctx: SolidifyStateContext<MultiRelation2TiersStateModel<TResource>>, action: MultiRelation2TiersAction.GetAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((multiRelation2TiersNameSpace: MultiRelation2TiersNameSpace) => multiRelation2TiersNameSpace.GetById)
  getById<U>(ctx: SolidifyStateContext<MultiRelation2TiersStateModel<TResource>>, action: MultiRelation2TiersAction.GetById): Observable<U> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        current: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.getById<U>(url, action.resId)
      .pipe(
        tap((model: U) => {
          ctx.dispatch(MultiRelation2TiersActionHelper.getByIdSuccess<U>(this._nameSpace, action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(MultiRelation2TiersActionHelper.getByIdFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((multiRelation2TiersNameSpace: MultiRelation2TiersNameSpace) => multiRelation2TiersNameSpace.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<MultiRelation2TiersStateModel<TResource>>, action: MultiRelation2TiersAction.GetByIdSuccess<TResource>): void {
    ctx.patchState({
      current: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((multiRelation2TiersNameSpace: MultiRelation2TiersNameSpace) => multiRelation2TiersNameSpace.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<MultiRelation2TiersStateModel<TResource>>, action: MultiRelation2TiersAction.GetByIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((multiRelation2TiersNameSpace: MultiRelation2TiersNameSpace) => multiRelation2TiersNameSpace.Create)
  create(ctx: SolidifyStateContext<MultiRelation2TiersStateModel<TResource>>, action: MultiRelation2TiersAction.Create<URelation>): Observable<TResource> {
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.post<URelation, TResource>(`${url}/${action.resId}`, action.relation)
      .pipe(
        tap(result => {
          ctx.dispatch(MultiRelation2TiersActionHelper.createSuccess<TResource, URelation>(this._nameSpace, action, result));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(MultiRelation2TiersActionHelper.createFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((multiRelation2TiersNameSpace: MultiRelation2TiersNameSpace) => multiRelation2TiersNameSpace.DeleteList)
  deleteList(ctx: SolidifyStateContext<MultiRelation2TiersStateModel<TResource>>, action: MultiRelation2TiersAction.DeleteList): Observable<string[]> {
    if (action.listResId.length === 0) {
      ctx.dispatch(MultiRelation2TiersActionHelper.deleteListSuccess(this._nameSpace, action));
      return;
    }
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.delete<string[]>(url, action.listResId)
      .pipe(
        tap(() => {
          ctx.dispatch(MultiRelation2TiersActionHelper.deleteListSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(MultiRelation2TiersActionHelper.deleteListFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((multiRelation2TiersNameSpace: MultiRelation2TiersNameSpace) => multiRelation2TiersNameSpace.Delete)
  delete(ctx: SolidifyStateContext<MultiRelation2TiersStateModel<TResource>>, action: MultiRelation2TiersAction.Delete): Observable<number> {
    let url = this._evaluateSubResourceUrl(action.parentId);
    url = `${url}/${action.resId}`;
    if (isNotNullNorUndefinedNorWhiteString(action.compositeKey)) {
      url = `${url}?${this._optionsState.joinResourceIdRequestParamName}=${action.compositeKey}`;
    }
    return this._apiService.delete<number>(url)
      .pipe(
        tap(() => {
          ctx.dispatch(MultiRelation2TiersActionHelper.deleteSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(MultiRelation2TiersActionHelper.deleteFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((multiRelation2TiersNameSpace: MultiRelation2TiersNameSpace) => multiRelation2TiersNameSpace.UpdateRelation)
  updateRelation(ctx: SolidifyStateContext<MultiRelation2TiersStateModel<TResource>>, action: MultiRelation2TiersAction.UpdateRelation<URelation>): Observable<URelation> {
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.patchById<URelation>(url, action.resId, action.relation)
      .pipe(
        tap(() => {
          ctx.dispatch(MultiRelation2TiersActionHelper.updateRelationSuccess<URelation>(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(MultiRelation2TiersActionHelper.updateRelationFail<URelation>(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((multiRelation2TiersNameSpace: MultiRelation2TiersNameSpace) => multiRelation2TiersNameSpace.Update)
  update(ctx: SolidifyStateContext<MultiRelation2TiersStateModel<TResource>>, action: MultiRelation2TiersAction.Update<TResource, URelation>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    this.subscribe(this._internalUpdate(ctx, action).pipe(tap(success => {
      if (success) {
        ctx.dispatch(MultiRelation2TiersActionHelper.updateSuccess(this._nameSpace, action, action.parentId));
      } else {
        ctx.dispatch(MultiRelation2TiersActionHelper.updateFail(this._nameSpace, action, action.parentId));
      }
    })));
  }

  protected _internalUpdate(ctx: SolidifyStateContext<MultiRelation2TiersStateModel<TResource>>, action: MultiRelation2TiersAction.Update<TResource, URelation>): Observable<boolean> {
    const listOld = isNullOrUndefined(ctx.getState().selected) ? [] : ctx.getState().selected;
    const listNew = action.newRelation;

    const diff = ArrayUtil.diff(listOld, listNew, (a: TResource, b: TResource) => {
      if (isNullOrUndefinedOrWhiteString(a?.joinResource?.compositeKey) && isNullOrUndefinedOrWhiteString(b?.joinResource?.compositeKey)) {
        return false; // Allow to manage new resource
      }
      return a?.joinResource?.compositeKey === b?.joinResource?.compositeKey;
    });

    const listActions: ActionSubActionCompletionsWrapper[] = [];

    const listActionToAdd: ActionSubActionCompletionsWrapper<MultiRelation2TiersAction.Create<URelation>>[] = listNew
      .filter(resource => isNullOrUndefinedOrWhiteString(resource?.joinResource?.compositeKey))
      .map(resource => ({
        action: MultiRelation2TiersActionHelper.create(this._nameSpace, action.parentId, resource.resId, resource.joinResource),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.CreateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.CreateFail)),
        ],
      }));

    const listActionToDelete: ActionSubActionCompletionsWrapper<MultiRelation2TiersAction.Delete>[] = diff.missingInArrayB.map(resource => ({
      action: MultiRelation2TiersActionHelper.delete(this._nameSpace, action.parentId, resource.resId, resource.joinResource.compositeKey),
      subActionCompletions: [
        this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.DeleteSuccess)),
        this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.DeleteFail)),
      ],
    }));

    let listActionToUpdate = [];
    if (isNonEmptyArray(diff.common)) {
      // Check if difference present on join resource parameters
      const listIdInCommon = diff.common.map(c => c.joinResource.compositeKey);
      const oldInCommon = listOld.filter(item => listIdInCommon.includes(item.joinResource.compositeKey));
      const newInCommon = listNew.filter(item => listIdInCommon.includes(item.joinResource.compositeKey));
      const diffCommon = ArrayUtil.diff(oldInCommon, newInCommon, (a: TResource, b: TResource) => {
        if (a?.joinResource?.compositeKey !== b?.joinResource?.compositeKey) {
          return false;
        }
        const oldJoinResource = ObjectUtil.clone(a?.joinResource);
        delete oldJoinResource.compositeKey;
        delete oldJoinResource.creation;
        delete oldJoinResource.lastUpdate;
        const newJoinResource = ObjectUtil.clone(b?.joinResource);
        delete newJoinResource.compositeKey;
        delete newJoinResource.creation;
        delete newJoinResource.lastUpdate;
        return JSON.stringify(oldJoinResource) === JSON.stringify(newJoinResource);
      });

      listActionToUpdate = diffCommon.presentOnlyInArrayB.map(resource => ({
        action: MultiRelation2TiersActionHelper.updateRelation<URelation>(this._nameSpace, action.parentId, resource.resId, resource.joinResource),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.UpdateRelationSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.UpdateRelationFail)),
        ],
      }));
    }

    listActions.push(...listActionToAdd);
    listActions.push(...listActionToUpdate);
    listActions.push(...listActionToDelete);

    if (isNonEmptyArray(listActions)) {
      return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(
        ctx,
        listActions,
      ).pipe(
        map(result => result.success),
      );
    }
    return of(true);
  }

  @RegisterDefaultAction((multiRelation2TiersNameSpace: MultiRelation2TiersNameSpace) => multiRelation2TiersNameSpace.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<MultiRelation2TiersStateModel<TResource>>, action: MultiRelation2TiersAction.UpdateSuccess<TResource, URelation>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      selected: undefined,
    });
  }

  @RegisterDefaultAction((multiRelation2TiersNameSpace: MultiRelation2TiersNameSpace) => multiRelation2TiersNameSpace.UpdateFail)
  updateFail(ctx: SolidifyStateContext<MultiRelation2TiersStateModel<TResource>>, action: MultiRelation2TiersAction.UpdateFail<TResource, URelation>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(MultiRelation2TiersActionHelper.getAll(this._nameSpace, action.parentId));
  }
}
