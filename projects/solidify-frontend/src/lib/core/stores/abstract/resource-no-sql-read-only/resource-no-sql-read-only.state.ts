/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - resource-no-sql-read-only.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Type,
} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {RegisterDefaultAction} from "../../../decorators/store.decorator";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {SolidifyStateError} from "../../../errors/solidify-state.error";
import {ApiService} from "../../../http/api.service";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {CollectionTyped} from "../../../models/dto/collection-typed.model";
import {SolidifyHttpErrorResponseModel} from "../../../models/errors/solidify-http-error-response.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {NotifierService} from "../../../models/services/notifier-service.model";
import {BaseResourceStateModel} from "../../../models/stores/base-resource-state.model";
import {SolidifyStateContext} from "../../../models/stores/state-context.model";
import {MemoizedUtil} from "../../../utils/stores/memoized.util";
import {StoreUtil} from "../../../utils/stores/store.util";
import {
  BaseResourceState,
  defaultBaseResourceStateInitValue,
} from "../base/base-resource.state";
import {ResourceNoSqlReadOnlyActionHelper} from "./resource-no-sql-read-only-action.helper";
import {ResourceNoSqlReadOnlyNameSpace} from "./resource-no-sql-read-only-namespace.model";
import {ResourceNoSqlReadOnlyOptions} from "./resource-no-sql-read-only-options.model";
import {ResourceNoSqlReadOnlyStateModel} from "./resource-no-sql-read-only-state.model";
import {ResourceNoSqlReadOnlyAction} from "./resource-no-sql-read-only.action";

export const defaultResourceNoSqlReadOnlyStateInitValue: <TResourceType extends BaseResourceType> () => ResourceNoSqlReadOnlyStateModel<TResourceType> = () =>
  ({
    ...defaultBaseResourceStateInitValue(),
    list: undefined,
    current: undefined,
  });

// @dynamic
export abstract class ResourceNoSqlReadOnlyState<TStateModel extends BaseResourceStateModel, TResource extends BaseResourceType> extends BaseResourceState<TStateModel> {
  protected declare readonly _nameSpace: ResourceNoSqlReadOnlyNameSpace;
  protected declare readonly _optionsState: ResourceNoSqlReadOnlyOptions;

  protected constructor(protected readonly _apiService: ApiService,
                        protected readonly _store: Store,
                        protected readonly _notificationService: NotifierService,
                        protected readonly _actions$: Actions,
                        @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
                        protected readonly _options: ResourceNoSqlReadOnlyOptions) {
    super(_apiService, _store, _notificationService, _actions$, _environment, _options, ResourceNoSqlReadOnlyState);
  }

  protected static _getDefaultOptions(): ResourceNoSqlReadOnlyOptions | any {
    let defaultOptions: ResourceNoSqlReadOnlyOptions | any = {} as ResourceNoSqlReadOnlyOptions;
    defaultOptions = Object.assign(BaseResourceState._getDefaultOptions(), defaultOptions);
    return defaultOptions;
  }

  protected static override _checkOptions(stateName: string, options: ResourceNoSqlReadOnlyOptions): void {
    BaseResourceState._checkOptions(stateName, options);
  }

  // TODO remove this statics method when MemoizedUtilStateType typing error fixed for this state
  static list<TStateModel extends ResourceNoSqlReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceNoSqlReadOnlyState<TStateModel, TResource>>): Observable<TResource[]> {
    return MemoizedUtil.select(store, ctor, state => state.list, true);
  }

  static listSnapshot<TStateModel extends ResourceNoSqlReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceNoSqlReadOnlyState<TStateModel, TResource>>): TResource[] {
    return MemoizedUtil.selectSnapshot(store, ctor, state => state.list);
  }

  static total<TStateModel extends ResourceNoSqlReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceNoSqlReadOnlyState<TStateModel, TResource>>): Observable<number> {
    return MemoizedUtil.select(store, ctor, state => state.total, true);
  }

  static totalSnapshot<TStateModel extends ResourceNoSqlReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceNoSqlReadOnlyState<TStateModel, TResource>>): number {
    return MemoizedUtil.selectSnapshot(store, ctor, state => state.total);
  }

  static current<TStateModel extends ResourceNoSqlReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceNoSqlReadOnlyState<TStateModel, TResource>>): Observable<TResource> {
    return MemoizedUtil.select(store, ctor, state => state.current, true);
  }

  static currentSnapshot<TStateModel extends ResourceNoSqlReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceNoSqlReadOnlyState<TStateModel, TResource>>): TResource {
    return MemoizedUtil.selectSnapshot(store, ctor, state => state.current);
  }

  static queryParameters<TStateModel extends ResourceNoSqlReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceNoSqlReadOnlyState<TStateModel, TResource>>): Observable<QueryParameters> {
    return MemoizedUtil.select(store, ctor, state => state.queryParameters, true);
  }

  static queryParametersSnapshot<TStateModel extends ResourceNoSqlReadOnlyStateModel<TResource>, TResource extends BaseResourceType>(store: Store, ctor: Type<ResourceNoSqlReadOnlyState<TStateModel, TResource>>): QueryParameters {
    return MemoizedUtil.selectSnapshot(store, ctor, state => state.queryParameters);
  }

  @RegisterDefaultAction((resourceNoSqlReadOnlyNameSpace: ResourceNoSqlReadOnlyNameSpace) => resourceNoSqlReadOnlyNameSpace.ChangeQueryParameters)
  changeQueryParameters(ctx: SolidifyStateContext<ResourceNoSqlReadOnlyStateModel<TResource>>, action: ResourceNoSqlReadOnlyAction.ChangeQueryParameters): void {
    ctx.patchState({
      queryParameters: action.queryParameters,
    });
    ctx.dispatch(ResourceNoSqlReadOnlyActionHelper.getAll(this._nameSpace, undefined, action.keepCurrentContext));
  }

  @RegisterDefaultAction((resourceNoSqlReadOnlyNameSpace: ResourceNoSqlReadOnlyNameSpace) => resourceNoSqlReadOnlyNameSpace.GetAll)
  getAll(ctx: SolidifyStateContext<ResourceNoSqlReadOnlyStateModel<TResource>>, action: ResourceNoSqlReadOnlyAction.GetAll): Observable<CollectionTyped<TResource>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        total: 0,
        list: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    return this._getAll<TResource>(ctx, action)
      .pipe(
        tap((collection: CollectionTyped<TResource>) => {
          ctx.dispatch(ResourceNoSqlReadOnlyActionHelper.getAllSuccess<TResource>(this._nameSpace, action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(ResourceNoSqlReadOnlyActionHelper.getAllFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  protected override _callGetAllEndpoint<U>(ctx: SolidifyStateContext<ResourceNoSqlReadOnlyStateModel<TResource>>, action: ResourceNoSqlReadOnlyAction.GetAll, queryParameters: QueryParameters): Observable<CollectionTyped<U>> {
    const url = this._urlResource;
    return this._apiService.getCollection<U>(url, queryParameters);
  }

  @RegisterDefaultAction((resourceNoSqlReadOnlyNameSpace: ResourceNoSqlReadOnlyNameSpace) => resourceNoSqlReadOnlyNameSpace.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<ResourceNoSqlReadOnlyStateModel<TResource>>, action: ResourceNoSqlReadOnlyAction.GetAllSuccess<TResource>): void {
    ctx.patchState({
      total: action.list._page?.totalItems,
      list: action.list._data,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((resourceNoSqlReadOnlyNameSpace: ResourceNoSqlReadOnlyNameSpace) => resourceNoSqlReadOnlyNameSpace.GetAllFail)
  getAllFail(ctx: SolidifyStateContext<ResourceNoSqlReadOnlyStateModel<TResource>>, action: ResourceNoSqlReadOnlyAction.GetAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((resourceNoSqlReadOnlyNameSpace: ResourceNoSqlReadOnlyNameSpace) => resourceNoSqlReadOnlyNameSpace.GetById)
  getById(ctx: SolidifyStateContext<ResourceNoSqlReadOnlyStateModel<TResource>>, action: ResourceNoSqlReadOnlyAction.GetById): Observable<TResource> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        current: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });

    return this._apiService.getById<TResource>(this._urlResource, action.id)
      .pipe(
        tap((model: TResource) => {
          ctx.dispatch(ResourceNoSqlReadOnlyActionHelper.getByIdSuccess(this._nameSpace, action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(ResourceNoSqlReadOnlyActionHelper.getByIdFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((resourceNoSqlReadOnlyNameSpace: ResourceNoSqlReadOnlyNameSpace) => resourceNoSqlReadOnlyNameSpace.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<ResourceNoSqlReadOnlyStateModel<TResource>>, action: ResourceNoSqlReadOnlyAction.GetByIdSuccess<TResource>): void {
    ctx.patchState({
      current: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((resourceNoSqlReadOnlyNameSpace: ResourceNoSqlReadOnlyNameSpace) => resourceNoSqlReadOnlyNameSpace.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<ResourceNoSqlReadOnlyStateModel<TResource>>, action: ResourceNoSqlReadOnlyAction.GetByIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
