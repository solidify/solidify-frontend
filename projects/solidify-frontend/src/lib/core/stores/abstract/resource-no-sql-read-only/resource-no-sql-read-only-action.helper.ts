/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - resource-no-sql-read-only-action.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {ResourceNoSqlReadOnlyNameSpace} from "./resource-no-sql-read-only-namespace.model";
import {ResourceNoSqlReadOnlyAction} from "./resource-no-sql-read-only.action";

export class ResourceNoSqlReadOnlyActionHelper {
  static changeQueryParameters(resourceNoSqlReadOnlyNameSpace: ResourceNoSqlReadOnlyNameSpace, ...args: ConstructorParameters<typeof ResourceNoSqlReadOnlyAction.ChangeQueryParameters>): ResourceNoSqlReadOnlyAction.ChangeQueryParameters {
    return new resourceNoSqlReadOnlyNameSpace.ChangeQueryParameters(...args);
  }

  static getAll(resourceNoSqlReadOnlyNameSpace: ResourceNoSqlReadOnlyNameSpace, ...args: ConstructorParameters<typeof ResourceNoSqlReadOnlyAction.GetAll>): ResourceNoSqlReadOnlyAction.GetAll {
    return new resourceNoSqlReadOnlyNameSpace.GetAll(...args);
  }

  static getAllSuccess<TResource extends BaseResourceType>(resourceNoSqlReadOnlyNameSpace: ResourceNoSqlReadOnlyNameSpace, ...args: ConstructorParameters<typeof ResourceNoSqlReadOnlyAction.GetAllSuccess>): ResourceNoSqlReadOnlyAction.GetAllSuccess<TResource> {
    return new resourceNoSqlReadOnlyNameSpace.GetAllSuccess(...args);
  }

  static getAllFail(resourceNoSqlReadOnlyNameSpace: ResourceNoSqlReadOnlyNameSpace, ...args: ConstructorParameters<typeof ResourceNoSqlReadOnlyAction.GetAllFail>): ResourceNoSqlReadOnlyAction.GetAllFail {
    return new resourceNoSqlReadOnlyNameSpace.GetAllFail(...args);
  }

  static getById(resourceNoSqlReadOnlyNameSpace: ResourceNoSqlReadOnlyNameSpace, ...args: ConstructorParameters<typeof ResourceNoSqlReadOnlyAction.GetById>): ResourceNoSqlReadOnlyAction.GetById {
    return new resourceNoSqlReadOnlyNameSpace.GetById(...args);
  }

  static getByIdSuccess<TResource extends BaseResourceType>(resourceNoSqlReadOnlyNameSpace: ResourceNoSqlReadOnlyNameSpace, ...args: ConstructorParameters<typeof ResourceNoSqlReadOnlyAction.GetByIdSuccess>): ResourceNoSqlReadOnlyAction.GetByIdSuccess<TResource> {
    return new resourceNoSqlReadOnlyNameSpace.GetByIdSuccess(...args);
  }

  static getByIdFail(resourceNoSqlReadOnlyNameSpace: ResourceNoSqlReadOnlyNameSpace, ...args: ConstructorParameters<typeof ResourceNoSqlReadOnlyAction.GetByIdFail>): ResourceNoSqlReadOnlyAction.GetByIdFail {
    return new resourceNoSqlReadOnlyNameSpace.GetByIdFail(...args);
  }
}
