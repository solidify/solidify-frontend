/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - association-remote-action.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {AssociationRemoteNameSpace} from "./association-remote-namespace.model";
import {AssociationRemoteAction} from "./association-remote.action";

export class AssociationRemoteActionHelper {
  static getAll(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.GetAll>): AssociationRemoteAction.GetAll {
    return new associationRemoteNameSpace.GetAll(...args);
  }

  static getAllSuccess<TResource extends BaseResourceType>(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.GetAllSuccess>): AssociationRemoteAction.GetAllSuccess<TResource> {
    return new associationRemoteNameSpace.GetAllSuccess(...args);
  }

  static getAllFail(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.GetAllFail>): AssociationRemoteAction.GetAllFail {
    return new associationRemoteNameSpace.GetAllFail(...args);
  }

  static getById(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.GetById>): AssociationRemoteAction.GetById {
    return new associationRemoteNameSpace.GetById(...args);
  }

  static getByIdSuccess<TResource extends BaseResourceType>(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.GetByIdSuccess>): AssociationRemoteAction.GetByIdSuccess<TResource> {
    return new associationRemoteNameSpace.GetByIdSuccess(...args);
  }

  static getByIdFail(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.GetByIdFail>): AssociationRemoteAction.GetByIdFail {
    return new associationRemoteNameSpace.GetByIdFail(...args);
  }

  static create(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.Create>): AssociationRemoteAction.Create {
    return new associationRemoteNameSpace.Create(...args);
  }

  static createSuccess(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.CreateSuccess>): AssociationRemoteAction.CreateSuccess {
    return new associationRemoteNameSpace.CreateSuccess(...args);
  }

  static createFail(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.CreateFail>): AssociationRemoteAction.CreateFail {
    return new associationRemoteNameSpace.CreateFail(...args);
  }

  static delete(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.Delete>): AssociationRemoteAction.Delete {
    return new associationRemoteNameSpace.Delete(...args);
  }

  static deleteSuccess(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.DeleteSuccess>): AssociationRemoteAction.DeleteSuccess {
    return new associationRemoteNameSpace.DeleteSuccess(...args);
  }

  static deleteFail(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.DeleteFail>): AssociationRemoteAction.DeleteFail {
    return new associationRemoteNameSpace.DeleteFail(...args);
  }

  static deleteList(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.DeleteList>): AssociationRemoteAction.DeleteList {
    return new associationRemoteNameSpace.DeleteList(...args);
  }

  static deleteListSuccess(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.DeleteListSuccess>): AssociationRemoteAction.DeleteListSuccess {
    return new associationRemoteNameSpace.DeleteListSuccess(...args);
  }

  static deleteListFail(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.DeleteListFail>): AssociationRemoteAction.DeleteListFail {
    return new associationRemoteNameSpace.DeleteListFail(...args);
  }

  static update(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.Update>): AssociationRemoteAction.Update {
    return new associationRemoteNameSpace.Update(...args);
  }

  static updateSuccess(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.UpdateSuccess>): AssociationRemoteAction.UpdateSuccess {
    return new associationRemoteNameSpace.UpdateSuccess(...args);
  }

  static updateFail(associationRemoteNameSpace: AssociationRemoteNameSpace, ...args: ConstructorParameters<typeof AssociationRemoteAction.UpdateFail>): AssociationRemoteAction.UpdateFail {
    return new associationRemoteNameSpace.UpdateFail(...args);
  }
}
