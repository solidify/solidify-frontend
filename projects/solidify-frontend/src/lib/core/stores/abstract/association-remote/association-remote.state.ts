/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - association-remote.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Inject} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {RegisterDefaultAction} from "../../../decorators/store.decorator";
import {ApiResourceNamePartialEnum} from "../../../enums/partial/api-resource-name-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {SolidifyStateError} from "../../../errors/solidify-state.error";
import {ApiService} from "../../../http/api.service";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {CollectionTyped} from "../../../models/dto/collection-typed.model";
import {SolidifyHttpErrorResponseModel} from "../../../models/errors/solidify-http-error-response.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {NotifierService} from "../../../models/services/notifier-service.model";
import {BaseResourceStateModel} from "../../../models/stores/base-resource-state.model";
import {ActionSubActionCompletionsWrapper} from "../../../models/stores/base.action";
import {SolidifyStateContext} from "../../../models/stores/state-context.model";
import {SubResourceUpdateModel} from "../../../models/stores/sub-resource-update.model";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {ofSolidifyActionCompleted} from "../../../utils/stores/store.tool";
import {StoreUtil} from "../../../utils/stores/store.util";
import {
  BaseResourceState,
  defaultBaseResourceStateInitValue,
} from "../base/base-resource.state";
import {AssociationRemoteActionHelper} from "./association-remote-action.helper";
import {AssociationRemoteNameSpace} from "./association-remote-namespace.model";
import {AssociationRemoteOptions} from "./association-remote-options.model";
import {AssociationRemoteStateModel} from "./association-remote-state.model";
import {AssociationRemoteAction} from "./association-remote.action";

export const defaultAssociationRemoteStateInitValue: <TResourceType extends BaseResourceType> () => AssociationRemoteStateModel<TResourceType> = () =>
  ({
    ...defaultBaseResourceStateInitValue(),
    selected: undefined,
    current: undefined,
  });

// @dynamic
export abstract class AssociationRemoteState<TStateModel extends BaseResourceStateModel, TResource extends BaseResourceType> extends BaseResourceState<TStateModel> {
  protected readonly _resourceName: ExtendEnum<ApiResourceNamePartialEnum>;
  protected declare readonly _nameSpace: AssociationRemoteNameSpace;
  protected declare readonly _optionsState: AssociationRemoteOptions;

  protected constructor(protected _apiService: ApiService,
                        protected _store: Store,
                        protected _notificationService: NotifierService,
                        protected _actions$: Actions,
                        @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
                        protected _options: AssociationRemoteOptions) {
    super(_apiService, _store, _notificationService, _actions$, _environment, _options, AssociationRemoteState);
    this._resourceName = this._optionsState.resourceName;
  }

  private _evaluateSubResourceUrl(parentId: string): string {
    return this._urlResource + SOLIDIFY_CONSTANTS.URL_SEPARATOR + parentId + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this._resourceName;
  }

  @RegisterDefaultAction((associationRemoteNameSpace: AssociationRemoteNameSpace) => associationRemoteNameSpace.GetAll)
  getAll<U>(ctx: SolidifyStateContext<AssociationRemoteStateModel<TResource>>, action: AssociationRemoteAction.GetAll): Observable<CollectionTyped<U>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        total: 0,
        selected: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    return this._getAll<U>(ctx, action)
      .pipe(
        tap((collection: CollectionTyped<U>) => {
          ctx.dispatch(AssociationRemoteActionHelper.getAllSuccess<U>(this._nameSpace, action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(AssociationRemoteActionHelper.getAllFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  protected override _callGetAllEndpoint<U>(ctx: SolidifyStateContext<AssociationRemoteStateModel<TResource>>, action: AssociationRemoteAction.GetAll, queryParameters: QueryParameters): Observable<CollectionTyped<U>> {
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.getCollection<U>(url, queryParameters);
  }

  @RegisterDefaultAction((associationRemoteNameSpace: AssociationRemoteNameSpace) => associationRemoteNameSpace.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<AssociationRemoteStateModel<TResource>>, action: AssociationRemoteAction.GetAllSuccess<TResource>): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.list);

    ctx.patchState({
      selected: action.list._data,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: queryParameters,
    });
  }

  @RegisterDefaultAction((associationRemoteNameSpace: AssociationRemoteNameSpace) => associationRemoteNameSpace.GetAllFail)
  getAllFail(ctx: SolidifyStateContext<AssociationRemoteStateModel<TResource>>, action: AssociationRemoteAction.GetAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((associationRemoteNameSpace: AssociationRemoteNameSpace) => associationRemoteNameSpace.GetById)
  getById<U>(ctx: SolidifyStateContext<AssociationRemoteStateModel<TResource>>, action: AssociationRemoteAction.GetById): Observable<U> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        current: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.getById<U>(url, action.resId)
      .pipe(
        tap((model: U) => {
          ctx.dispatch(AssociationRemoteActionHelper.getByIdSuccess<U>(this._nameSpace, action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(AssociationRemoteActionHelper.getByIdFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((associationRemoteNameSpace: AssociationRemoteNameSpace) => associationRemoteNameSpace.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<AssociationRemoteStateModel<TResource>>, action: AssociationRemoteAction.GetByIdSuccess<TResource>): void {
    ctx.patchState({
      current: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((associationRemoteNameSpace: AssociationRemoteNameSpace) => associationRemoteNameSpace.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<AssociationRemoteStateModel<TResource>>, action: AssociationRemoteAction.GetByIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((associationRemoteNameSpace: AssociationRemoteNameSpace) => associationRemoteNameSpace.Create)
  create(ctx: SolidifyStateContext<AssociationRemoteStateModel<TResource>>, action: AssociationRemoteAction.Create): Observable<string[]> {
    if (action.listResId.length === 0) {
      ctx.dispatch(AssociationRemoteActionHelper.createSuccess(this._nameSpace, action));
      return;
    }
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.post<string[]>(url, action.listResId)
      .pipe(
        tap(() => {
          ctx.dispatch(AssociationRemoteActionHelper.createSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(AssociationRemoteActionHelper.createFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((associationRemoteNameSpace: AssociationRemoteNameSpace) => associationRemoteNameSpace.Delete)
  delete(ctx: SolidifyStateContext<AssociationRemoteStateModel<TResource>>, action: AssociationRemoteAction.Delete): Observable<string[]> {
    if (isNullOrUndefined(action.resId)) {
      ctx.dispatch(AssociationRemoteActionHelper.deleteSuccess(this._nameSpace, action));
      return;
    }
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.delete<string[]>(url + SOLIDIFY_CONSTANTS.URL_SEPARATOR + action.resId)
      .pipe(
        tap(() => {
          ctx.dispatch(AssociationRemoteActionHelper.deleteSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(AssociationRemoteActionHelper.deleteFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((associationRemoteNameSpace: AssociationRemoteNameSpace) => associationRemoteNameSpace.DeleteList)
  deleteList(ctx: SolidifyStateContext<AssociationRemoteStateModel<TResource>>, action: AssociationRemoteAction.DeleteList): Observable<string[]> {
    if (action.listResId.length === 0) {
      ctx.dispatch(AssociationRemoteActionHelper.deleteListSuccess(this._nameSpace, action));
      return;
    }
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.delete<string[]>(url, action.listResId)
      .pipe(
        tap(() => {
          ctx.dispatch(AssociationRemoteActionHelper.deleteListSuccess(this._nameSpace, action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(AssociationRemoteActionHelper.deleteListFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((associationRemoteNameSpace: AssociationRemoteNameSpace) => associationRemoteNameSpace.Update)
  update(ctx: SolidifyStateContext<AssociationRemoteStateModel<TResource>>, action: AssociationRemoteAction.Update): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const listOldResId = isNullOrUndefined(ctx.getState().selected) ? [] : ctx.getState().selected.map(s => s.resId);
    const listNewResId = action.newResId;

    const subResourceUpdate: SubResourceUpdateModel = StoreUtil.determineSubResourceChange(listOldResId, listNewResId);
    this.subscribe(StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(
        ctx,
        this.computeUpdatesActionsToDispatch(subResourceUpdate, action.parentId),
      ).pipe(
        tap(result => {
          if (result.success) {
            ctx.dispatch(AssociationRemoteActionHelper.updateSuccess(this._nameSpace, action, action.parentId));
          } else {
            ctx.dispatch(AssociationRemoteActionHelper.updateFail(this._nameSpace, action, action.parentId));
          }
        })),
    );
  }

  @RegisterDefaultAction((associationRemoteNameSpace: AssociationRemoteNameSpace) => associationRemoteNameSpace.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<AssociationRemoteStateModel<TResource>>, action: AssociationRemoteAction.UpdateSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      selected: undefined,
    });
  }

  @RegisterDefaultAction((associationRemoteNameSpace: AssociationRemoteNameSpace) => associationRemoteNameSpace.UpdateFail)
  updateFail(ctx: SolidifyStateContext<AssociationRemoteStateModel<TResource>>, action: AssociationRemoteAction.UpdateFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(AssociationRemoteActionHelper.getAll(this._nameSpace, action.parentId));
  }

  private computeUpdatesActionsToDispatch(subResourceUpdate: SubResourceUpdateModel, parentId: string): ActionSubActionCompletionsWrapper<AssociationRemoteAction.Create | AssociationRemoteAction.Delete>[] {
    return [
      {
        action: AssociationRemoteActionHelper.deleteList(this._nameSpace, parentId, subResourceUpdate.resourceToRemoved),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.DeleteListSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.DeleteListFail)),
        ],
      },
      {
        action: AssociationRemoteActionHelper.create(this._nameSpace, parentId, subResourceUpdate.resourceToAdd),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.CreateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.CreateFail)),
        ],
      },
    ];
  }
}
