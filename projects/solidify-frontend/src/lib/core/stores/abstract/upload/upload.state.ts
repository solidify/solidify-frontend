/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - upload.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpErrorResponse,
  HttpEventType,
  HttpStatusCode,
} from "@angular/common/http";
import {Inject} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
} from "rxjs/operators";
import {LabelTranslateInterface} from "../../../../label-translate-interface.model";
import {RegisterDefaultAction} from "../../../decorators/store.decorator";
import {ApiActionNamePartialEnum} from "../../../enums/partial/api-action-name-partial.enum";
import {FileUploadStatusEnum} from "../../../enums/upload/file-upload-status.enum";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {SolidifyStateError} from "../../../errors/solidify-state.error";
import {FileUploadHelper} from "../../../helpers/file-upload.helper";
import {ApiService} from "../../../http/api.service";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../../injection-tokens/label-to-translate.injection-token";
import {ErrorDto} from "../../../models/errors/error-dto.model";
import {SolidifyHttpErrorResponseModel} from "../../../models/errors/solidify-http-error-response.model";
import {UploadEventModel} from "../../../models/http/upload-event.model";
import {NotifierService} from "../../../models/services/notifier-service.model";
import {BaseStateModel} from "../../../models/stores/base-state.model";
import {SolidifyStateContext} from "../../../models/stores/state-context.model";
import {FileUploadWrapper} from "../../../models/upload/file-upload-wrapper.model";
import {SolidifyFileUpload} from "../../../models/upload/solidify-file-upload.model";
import {
  isFunction,
  isNotNullNorUndefinedNorWhiteString,
  isNumber,
} from "../../../../core-resources/tools/is/is.tool";
import {FileUtil} from "../../../../core-resources/utils/file.util";
import {ObjectUtil} from "../../../../core-resources/utils/object.util";
import {SolidifyAppAction} from "../app/app.action";
import {BaseOptions} from "../base/base-options.model";
import {
  BaseState,
  defaultBaseStateInitValue,
} from "../base/base.state";
import {UploadActionHelper} from "./upload-action.helper";
import {UploadNameSpace} from "./upload-namespace.model";
import {UploadOptions} from "./upload-options.model";
import {UploadStateModel} from "./upload-state.model";
import {UploadAction} from "./upload.action";

export const defaultUploadStateInitValue: <TResourceType extends SolidifyFileUpload, UFileUploadWrapper extends FileUploadWrapper = FileUploadWrapper> () => UploadStateModel<TResourceType, UFileUploadWrapper> = () =>
  ({
    ...defaultBaseStateInitValue(),
    uploadStatus: [],
  });

// @dynamic
export abstract class UploadState<TStateModel extends BaseStateModel, TResource extends SolidifyFileUpload> extends BaseState<TStateModel> {
  protected declare readonly _nameSpace: UploadNameSpace;
  protected declare readonly _optionsState: UploadOptions<TResource>;

  protected constructor(protected _apiService: ApiService,
                        protected _store: Store,
                        protected _notificationService: NotifierService,
                        protected _actions$: Actions,
                        protected _options: UploadOptions<TResource>,
                        @Inject(ENVIRONMENT) readonly environment: DefaultSolidifyEnvironment,
                        @Inject(LABEL_TRANSLATE) protected readonly _labelTranslateInterface: LabelTranslateInterface,
                        protected readonly _translate: TranslateService) {
    super(_apiService, _store, _notificationService, _actions$, _options, UploadState);
    this._optionsState = Object.assign(UploadState._getDefaultOptions(), _options);
  }

  protected static _getDefaultOptions<TResource>(): UploadOptions<TResource> | any {
    let defaultOptions: UploadOptions<TResource> | any = {} as UploadOptions<TResource>;
    defaultOptions = Object.assign(BaseState._getDefaultOptions(), defaultOptions);
    return defaultOptions;
  }

  protected static override _checkOptions(stateName: string, options: BaseOptions): void {
    BaseState._checkOptions(stateName, options);
  }

  protected _generateUploadFormData(action: UploadAction.UploadFile): FormData {
    return new FormData();
  }

  @RegisterDefaultAction((uploadNameSpace: UploadNameSpace) => uploadNameSpace.UploadFile)
  uploadFile(ctx: SolidifyStateContext<UploadStateModel<TResource>>, action: UploadAction.UploadFile): Observable<void> {
    const formData = this._generateUploadFormData(action);

    const uploadFileStatus = FileUploadHelper.addToUploadStatus(ctx, action.fileUploadWrapper, action.isArchive);

    if (isFunction(this._optionsState.fileSizeLimit)) {
      const fileSizeLimit = this._optionsState.fileSizeLimit();
      if (isNumber(fileSizeLimit) && isNumber(action?.fileUploadWrapper?.file?.size) && action?.fileUploadWrapper?.file?.size > fileSizeLimit) {
        this._notificationService.showInformation(this._labelTranslateInterface.coreNotificationUploadFileSizeLimitXExceeded, {size: FileUtil.transformFileSize(fileSizeLimit)});
        const uploadFileStatusCopied = ObjectUtil.clone(uploadFileStatus);
        const errorMessage = this._translate.instant(this._labelTranslateInterface.coreNotificationUploadFileSizeLimitXExceeded, {size: FileUtil.transformFileSize(fileSizeLimit)});
        FileUploadHelper.updateErrorUploadFileStatus(ctx, uploadFileStatusCopied, errorMessage);
        return of();
      }
    }

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    ctx.dispatch(new SolidifyAppAction.PreventExit(this._labelTranslateInterface.coreNotificationUploadInProgress));

    const apiAction = action.isArchive ? ApiActionNamePartialEnum.UPLOAD_ARCHIVE : ApiActionNamePartialEnum.UPLOAD;
    let url = `${this._urlResource}/${action.parentId}/${apiAction}`;
    if (isFunction(this._optionsState.customUploadUrlSuffix)) {
      const suffix = this._optionsState.customUploadUrlSuffix(action);
      if (isNotNullNorUndefinedNorWhiteString(suffix)) {
        url = `${this._urlResource}/${suffix}`;
      }
    }

    return this._apiService.upload<SolidifyFileUpload>(url, formData)
      .pipe(
        map((event: UploadEventModel) => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              const currentUploadFileStatus = FileUploadHelper.getCurrentUploadFileStatus(ctx, uploadFileStatus.guid);
              if (currentUploadFileStatus.status === FileUploadStatusEnum.canceled) {
                ctx.dispatch(UploadActionHelper.cancelFileSending(this._nameSpace, action.parentId, uploadFileStatus));
                throw this.environment.errorToSkipInErrorHandler;
              }
              FileUploadHelper.updateInProgressUploadFileStatus(ctx, currentUploadFileStatus, event);
              return;
            case HttpEventType.Response:
              if (event.status === HttpStatusCode.Ok) {
                ctx.dispatch(UploadActionHelper.uploadFileSuccess(this._nameSpace, action, action.parentId, uploadFileStatus, event.body));
              } else {
                ctx.dispatch(UploadActionHelper.uploadFileFail(this._nameSpace, action, uploadFileStatus, undefined));
              }
              return;
            default:
              return;
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          if (error === this.environment.errorToSkipInErrorHandler) {
            throw new SolidifyStateError(this, error);
          }
          let errorDto: ErrorDto = undefined;
          if (error instanceof HttpErrorResponse) {
            errorDto = error.error;
          }
          ctx.dispatch(UploadActionHelper.uploadFileFail(this._nameSpace, action, uploadFileStatus, errorDto));
          throw error;
        }),
      );
  }

  @RegisterDefaultAction((uploadNameSpace: UploadNameSpace) => uploadNameSpace.UploadFileSuccess)
  uploadFileSuccess(ctx: SolidifyStateContext<UploadStateModel<TResource>>, action: UploadAction.UploadFileSuccess<TResource>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(this._labelTranslateInterface.coreNotificationUploadSuccess, undefined);
    const uploadFileStatusCopied = ObjectUtil.clone(action.uploadFileStatus);
    FileUploadHelper.updateCompletedUploadFileStatus(ctx, uploadFileStatusCopied, action.solidifyFile);
    if (this._checkIsPendingDownload(ctx)) {
      if (isFunction(this._optionsState.callbackAfterOneUploadFinished)) {
        this._optionsState.callbackAfterOneUploadFinished(action);
      }
    } else {
      if (isFunction(this._optionsState.callbackAfterAllUploadFinished)) {
        this._optionsState.callbackAfterAllUploadFinished(action);
      }
    }
  }

  @RegisterDefaultAction((uploadNameSpace: UploadNameSpace) => uploadNameSpace.UploadFileFail)
  uploadFileFail(ctx: SolidifyStateContext<UploadStateModel<TResource>>, action: UploadAction.UploadFileFail<TResource>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    const uploadFileStatusCopied = ObjectUtil.clone(action.uploadFileStatus);
    FileUploadHelper.updateErrorUploadFileStatus(ctx, uploadFileStatusCopied, action.errorDto.message);
    this._checkIsPendingDownload(ctx);
  }

  private _checkIsPendingDownload(ctx: SolidifyStateContext<UploadStateModel<TResource>>): boolean {
    const isPendingDownload = ctx.getState().uploadStatus
      .filter(u => u.status === FileUploadStatusEnum.inProgress || u.status === FileUploadStatusEnum.started).length > 0;
    if (!isPendingDownload) {
      ctx.dispatch(new SolidifyAppAction.CancelPreventExit());
    }
    return isPendingDownload;
  }

  @RegisterDefaultAction((uploadNameSpace: UploadNameSpace) => uploadNameSpace.RetrySendFile)
  retrySendFile(ctx: SolidifyStateContext<UploadStateModel<TResource>>, action: UploadAction.RetrySendFile<TResource>): void {
    FileUploadHelper.removeToUploadStatus(ctx, action.uploadFileStatus);
    ctx.dispatch(UploadActionHelper.uploadFile(this._nameSpace, action.parentId, action.uploadFileStatus.fileUploadWrapper, action.uploadFileStatus.isArchive));
  }

  @RegisterDefaultAction((uploadNameSpace: UploadNameSpace) => uploadNameSpace.MarkAsCancelFileSending)
  markAsCancelFileSending(ctx: SolidifyStateContext<UploadStateModel<TResource>>, action: UploadAction.MarkAsCancelFileSending<TResource>): void {
    const uploadFileStatus = FileUploadHelper.getCurrentUploadFileStatus(ctx, action.uploadFileStatus.guid);
    if (uploadFileStatus.status === FileUploadStatusEnum.inProgress) {
      FileUploadHelper.updateCancelUploadFileStatus(ctx, uploadFileStatus);
    } else {
      ctx.dispatch(UploadActionHelper.cancelFileSending(this._nameSpace, action.parentId, uploadFileStatus));
    }
  }

  @RegisterDefaultAction((uploadNameSpace: UploadNameSpace) => uploadNameSpace.CancelFileSending)
  cancelFileSending(ctx: SolidifyStateContext<UploadStateModel<TResource>>, action: UploadAction.CancelFileSending<TResource>): void {
    FileUploadHelper.removeToUploadStatus(ctx, action.uploadFileStatus);
    this._notificationService.showInformation(this._labelTranslateInterface.coreNotificationUploadCancelled, undefined);
    this._checkIsPendingDownload(ctx);
  }
}
