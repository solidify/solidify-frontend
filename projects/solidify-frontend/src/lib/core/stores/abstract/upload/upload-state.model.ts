/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - upload-state.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {BaseStateModel} from "../../../models/stores/base-state.model";
import {FileUploadWrapper} from "../../../models/upload/file-upload-wrapper.model";
import {SolidifyFileUpload} from "../../../models/upload/solidify-file-upload.model";
import {UploadFileStatus} from "../../../models/upload/upload-file-status.model";

export interface UploadStateModel<TResource extends SolidifyFileUpload, UFileUploadWrapper extends FileUploadWrapper = FileUploadWrapper> extends BaseStateModel {
  uploadStatus: UploadFileStatus<TResource, UFileUploadWrapper>[];
}
