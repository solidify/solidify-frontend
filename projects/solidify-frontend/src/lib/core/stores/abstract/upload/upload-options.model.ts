/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - upload-options.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {SolidifyFileUpload} from "../../../models/upload/solidify-file-upload.model";
import {BaseOptions} from "../base/base-options.model";
import {UploadNameSpace} from "./upload-namespace.model";
import {UploadAction} from "./upload.action";

export interface UploadOptions<TResource extends SolidifyFileUpload> extends BaseOptions {
  nameSpace: UploadNameSpace;
  callbackAfterAllUploadFinished?: ((action: UploadAction.UploadFileSuccess<TResource>) => void) | undefined;
  callbackAfterOneUploadFinished?: ((action: UploadAction.UploadFileSuccess<TResource>) => void) | undefined;
  fileSizeLimit?: (() => number) | undefined;
  customUploadUrlSuffix?: (action: UploadAction.UploadFile) => string;
}
