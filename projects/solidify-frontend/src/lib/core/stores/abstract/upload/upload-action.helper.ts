/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - upload-action.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {SolidifyFileUpload} from "../../../models/upload/solidify-file-upload.model";
import {UploadNameSpace} from "./upload-namespace.model";
import {UploadAction} from "./upload.action";

export class UploadActionHelper {
  static uploadFile(uploadNameSpace: UploadNameSpace, ...args: ConstructorParameters<typeof UploadAction.UploadFile>): UploadAction.UploadFile {
    return new uploadNameSpace.UploadFile(...args);
  }

  static uploadFileSuccess<TResource extends SolidifyFileUpload>(uploadNameSpace: UploadNameSpace, ...args: ConstructorParameters<typeof UploadAction.UploadFileSuccess>): UploadAction.UploadFileSuccess<TResource> {
    return new uploadNameSpace.UploadFileSuccess(...args);
  }

  static uploadFileFail<TResource extends SolidifyFileUpload>(uploadNameSpace: UploadNameSpace, ...args: ConstructorParameters<typeof UploadAction.UploadFileFail>): UploadAction.UploadFileFail<TResource> {
    return new uploadNameSpace.UploadFileFail(...args);
  }

  static markAsCancelFileSending<TResource extends SolidifyFileUpload>(uploadNameSpace: UploadNameSpace, ...args: ConstructorParameters<typeof UploadAction.MarkAsCancelFileSending>): UploadAction.MarkAsCancelFileSending<TResource> {
    return new uploadNameSpace.MarkAsCancelFileSending(...args);
  }

  static cancelFileSending<TResource extends SolidifyFileUpload>(uploadNameSpace: UploadNameSpace, ...args: ConstructorParameters<typeof UploadAction.CancelFileSending>): UploadAction.CancelFileSending<TResource> {
    return new uploadNameSpace.CancelFileSending(...args);
  }

  static retrySendFile<TResource extends SolidifyFileUpload>(uploadNameSpace: UploadNameSpace, ...args: ConstructorParameters<typeof UploadAction.RetrySendFile>): UploadAction.RetrySendFile<TResource> {
    return new uploadNameSpace.RetrySendFile(...args);
  }
}
