/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - composition-action.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {CompositionNameSpace} from "./composition-namespace.model";
import {CompositionAction} from "./composition.action";

export class CompositionActionHelper {
  static changeQueryParameters(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.ChangeQueryParameters>): CompositionAction.ChangeQueryParameters {
    return new compositionNameSpace.ChangeQueryParameters(...args);
  }

  static getAll(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.GetAll>): CompositionAction.GetAll {
    return new compositionNameSpace.GetAll(...args);
  }

  static getAllSuccess<TResource extends BaseResourceType>(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.GetAllSuccess>): CompositionAction.GetAllSuccess<TResource> {
    return new compositionNameSpace.GetAllSuccess(...args);
  }

  static getAllFail(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.GetAllFail>): CompositionAction.GetAllFail {
    return new compositionNameSpace.GetAllFail(...args);
  }

  static getById(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.GetById>): CompositionAction.GetById {
    return new compositionNameSpace.GetById(...args);
  }

  static getByIdSuccess<TResource extends BaseResourceType>(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.GetByIdSuccess>): CompositionAction.GetByIdSuccess<TResource> {
    return new compositionNameSpace.GetByIdSuccess(...args);
  }

  static getByIdFail(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.GetByIdFail>): CompositionAction.GetByIdFail {
    return new compositionNameSpace.GetByIdFail(...args);
  }

  static create<TResource extends BaseResourceType>(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.Create>): CompositionAction.Create<TResource> {
    return new compositionNameSpace.Create(...args);
  }

  static createSuccess<TResource extends BaseResourceType>(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.CreateSuccess>): CompositionAction.CreateSuccess<TResource> {
    return new compositionNameSpace.CreateSuccess(...args);
  }

  static createFail<TResource extends BaseResourceType>(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.CreateFail>): CompositionAction.CreateFail<TResource> {
    return new compositionNameSpace.CreateFail(...args);
  }

  static delete(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.Delete>): CompositionAction.Delete {
    return new compositionNameSpace.Delete(...args);
  }

  static deleteSuccess(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.DeleteSuccess>): CompositionAction.DeleteSuccess {
    return new compositionNameSpace.DeleteSuccess(...args);
  }

  static deleteFail(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.DeleteFail>): CompositionAction.DeleteFail {
    return new compositionNameSpace.DeleteFail(...args);
  }

  static update<TResource extends BaseResourceType>(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.Update>): CompositionAction.Update<TResource> {
    return new compositionNameSpace.Update(...args);
  }

  static updateSuccess<TResource extends BaseResourceType>(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.UpdateSuccess>): CompositionAction.UpdateSuccess<TResource> {
    return new compositionNameSpace.UpdateSuccess(...args);
  }

  static updateFail<TResource extends BaseResourceType>(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.UpdateFail>): CompositionAction.UpdateFail<TResource> {
    return new compositionNameSpace.UpdateFail(...args);
  }

  static clean(compositionNameSpace: CompositionNameSpace, ...args: ConstructorParameters<typeof CompositionAction.Clean>): CompositionAction.Clean {
    return new compositionNameSpace.Clean(...args);
  }
}
