/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - composition-options.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ApiResourceNamePartialEnum} from "../../../enums/partial/api-resource-name-partial.enum";
import {RoutesPartialEnum} from "../../../enums/partial/routes-partial.enum";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {BaseResourceOptions} from "../base/base-resource-options.model";
import {CompositionNameSpace} from "./composition-namespace.model";

export interface CompositionOptions extends BaseResourceOptions {
  nameSpace: CompositionNameSpace;
  resourceName: ExtendEnum<ApiResourceNamePartialEnum>;
  routeRedirectUrlAfterSuccessCreateAction?: ExtendEnum<RoutesPartialEnum> | undefined | ((parentCompositionId: string, resId: string) => string);
  routeRedirectUrlAfterSuccessUpdateAction?: ExtendEnum<RoutesPartialEnum> | undefined | ((parentCompositionId: string, resId: string) => string);
  routeRedirectUrlAfterSuccessDeleteAction?: ExtendEnum<RoutesPartialEnum> | undefined | ((parentCompositionId: string, resId: string) => string);
  routeReplaceAfterSuccessCreateAction?: boolean | undefined;
  routeReplaceAfterSuccessUpdateAction?: boolean | undefined;
  routeReplaceAfterSuccessDeleteAction?: boolean | undefined;
  keepCurrentStateAfterCreate?: boolean;
  keepCurrentStateAfterUpdate?: boolean;
  keepCurrentStateAfterDelete?: boolean;
  notificationResourceCreateSuccessTextToTranslate?: string;
  notificationResourceUpdateSuccessTextToTranslate?: string;
  notificationResourceDeleteSuccessTextToTranslate?: string;
  notificationResourceCreateFailTextToTranslate?: string;
  notificationResourceUpdateFailTextToTranslate?: string;
  notificationResourceDeleteFailTextToTranslate?: string;
}
