/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - composition.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Inject} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {RegisterDefaultAction} from "../../../decorators/store.decorator";
import {ModelAttributeEnum} from "../../../enums/model-attribute.enum";
import {ApiResourceNamePartialEnum} from "../../../enums/partial/api-resource-name-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../environments/environment.solidify-defaults";
import {SolidifyStateError} from "../../../errors/solidify-state.error";
import {ApiService} from "../../../http/api.service";
import {ENVIRONMENT} from "../../../injection-tokens/environment.injection-token";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {CollectionTyped} from "../../../models/dto/collection-typed.model";
import {SolidifyHttpErrorResponseModel} from "../../../models/errors/solidify-http-error-response.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {NotifierService} from "../../../models/services/notifier-service.model";
import {BaseResourceStateModel} from "../../../models/stores/base-resource-state.model";
import {SolidifyStateContext} from "../../../models/stores/state-context.model";
import {
  isFalse,
  isNullOrUndefined,
  isTrue,
} from "../../../../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {ObjectUtil} from "../../../../core-resources/utils/object.util";
import {StoreUtil} from "../../../utils/stores/store.util";
import {
  BaseResourceState,
  defaultBaseResourceStateInitValue,
} from "../base/base-resource.state";
import {CompositionActionHelper} from "./composition-action.helper";
import {CompositionNameSpace} from "./composition-namespace.model";
import {CompositionOptions} from "./composition-options.model";
import {CompositionStateModel} from "./composition-state.model";
import {CompositionAction} from "./composition.action";

export const defaultCompositionStateInitValue: <TResourceType extends BaseResourceType> () => CompositionStateModel<TResourceType> = () =>
  ({
    ...defaultBaseResourceStateInitValue(),
    list: undefined,
    current: undefined,
  });

// @dynamic
export abstract class CompositionState<TStateModel extends BaseResourceStateModel, TResource extends BaseResourceType> extends BaseResourceState<TStateModel> {
  protected readonly _resourceName: ExtendEnum<ApiResourceNamePartialEnum>;
  protected declare readonly _nameSpace: CompositionNameSpace;
  protected declare readonly _optionsState: CompositionOptions;

  protected constructor(protected readonly _apiService: ApiService,
                        protected readonly _store: Store,
                        protected readonly _notificationService: NotifierService,
                        protected readonly _actions$: Actions,
                        @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
                        protected readonly _options: CompositionOptions) {
    super(_apiService, _store, _notificationService, _actions$, _environment, _options, CompositionState);
    this._resourceName = this._optionsState.resourceName;
  }

  protected static _getDefaultOptions(): CompositionOptions | any {
    let defaultOptions: CompositionOptions | any = {
      keepCurrentStateAfterCreate: false,
      keepCurrentStateAfterUpdate: false,
      keepCurrentStateAfterDelete: false,
    } as CompositionOptions;
    defaultOptions = Object.assign(BaseResourceState._getDefaultOptions(), defaultOptions);
    return defaultOptions;
  }

  protected static override _checkOptions(stateName: string, options: CompositionOptions): void {
    BaseResourceState._checkOptions(stateName, options);
  }

  protected _evaluateSubResourceUrl(parentId: string): string {
    return this._urlResource + SOLIDIFY_CONSTANTS.URL_SEPARATOR + parentId + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this._resourceName;
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.ChangeQueryParameters)
  changeQueryParameters(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.ChangeQueryParameters): void {
    ctx.patchState({
      queryParameters: action.queryParameters,
    });
    if (isTrue(action.getAllAfterChange)) {
      ctx.dispatch(CompositionActionHelper.getAll(this._nameSpace, action.parentId, undefined, action.keepCurrentContext));
    }
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.GetAll)
  getAll<U>(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.GetAll): Observable<CollectionTyped<U>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        total: 0,
        list: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    return this._getAll<U>(ctx, action)
      .pipe(
        tap((collection: CollectionTyped<U>) => {
          ctx.dispatch(CompositionActionHelper.getAllSuccess<U>(this._nameSpace, action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(CompositionActionHelper.getAllFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  protected override _callGetAllEndpoint<U>(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.GetAll, queryParameters: QueryParameters): Observable<CollectionTyped<U>> {
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.getCollection<U>(url, queryParameters);
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.GetAllSuccess<TResource>): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.list);

    ctx.patchState({
      total: action.list._page?.totalItems,
      list: action.list._data,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: queryParameters,
    });
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.GetAllFail)
  getAllFail(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.GetAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.GetById)
  getById<U>(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.GetById): Observable<U> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        current: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.getById<U>(url, action.resId)
      .pipe(
        tap((model: U) => {
          ctx.dispatch(CompositionActionHelper.getByIdSuccess<U>(this._nameSpace, action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(CompositionActionHelper.getByIdFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.GetByIdSuccess<TResource>): void {
    ctx.patchState({
      current: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.GetByIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.Create)
  create(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.Create<TResource>): Observable<TResource> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.post<TResource>(url, action.model)
      .pipe(
        tap((model: TResource) => {
          ctx.dispatch(CompositionActionHelper.createSuccess(this._nameSpace, action, action.parentId, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(CompositionActionHelper.createFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.CreateSuccess)
  createSuccess(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.CreateSuccess<TResource>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      current: action.model,
    });
    StoreUtil.navigateCompositionIfDefined(ctx, this._optionsState.routeRedirectUrlAfterSuccessCreateAction, action.parentId, action.model.resId, this._optionsState.routeReplaceAfterSuccessCreateAction);
    StoreUtil.notifySuccess(this._notificationService, this._optionsState.notificationResourceCreateSuccessTextToTranslate);
    this._cleanCurrentStateIfDefined(ctx, this._optionsState.keepCurrentStateAfterCreate);
    ctx.dispatch(CompositionActionHelper.getAll(this._nameSpace, action.parentId));
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.CreateFail)
  createFail(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.CreateFail<TResource>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    StoreUtil.notifyError(this._notificationService, this._optionsState.notificationResourceCreateFailTextToTranslate);
    this._cleanCurrentStateIfDefined(ctx, this._optionsState.keepCurrentStateAfterCreate);
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.Delete)
  delete(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.Delete): Observable<TResource> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.deleteById<TResource>(url, action.resId)
      .pipe(
        tap(() => {
          ctx.dispatch(CompositionActionHelper.deleteSuccess(this._nameSpace, action, action.parentId));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(CompositionActionHelper.deleteFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.DeleteSuccess)
  deleteSuccess(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.DeleteSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    StoreUtil.navigateCompositionIfDefined(ctx, this._optionsState.routeRedirectUrlAfterSuccessDeleteAction, action.parentId, undefined, this._optionsState.routeReplaceAfterSuccessDeleteAction);
    StoreUtil.notifySuccess(this._notificationService, this._optionsState.notificationResourceDeleteSuccessTextToTranslate);
    this._cleanCurrentStateIfDefined(ctx, this._optionsState.keepCurrentStateAfterDelete);
    ctx.dispatch(CompositionActionHelper.getAll(this._nameSpace, action.parentId));
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.DeleteFail)
  deleteFail(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.DeleteFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    StoreUtil.notifyError(this._notificationService, this._optionsState.notificationResourceDeleteFailTextToTranslate);
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.Update)
  update(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.Update<TResource>): Observable<TResource> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.patchById<TResource>(url, action.model[ModelAttributeEnum.resId], action.model)
      .pipe(
        tap((model: TResource) => {
          ctx.dispatch(CompositionActionHelper.updateSuccess(this._nameSpace, action, action.parentId, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(CompositionActionHelper.updateFail(this._nameSpace, action, action.parentId));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.UpdateSuccess<TResource>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      current: action.model,
    });
    StoreUtil.navigateCompositionIfDefined(ctx, this._optionsState.routeRedirectUrlAfterSuccessUpdateAction, action.parentId, action.model.resId, this._optionsState.routeReplaceAfterSuccessUpdateAction);
    StoreUtil.notifySuccess(this._notificationService, this._optionsState.notificationResourceUpdateSuccessTextToTranslate);
    this._cleanCurrentStateIfDefined(ctx, this._optionsState.keepCurrentStateAfterUpdate);
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.UpdateFail)
  updateFail(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.UpdateFail<TResource>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    StoreUtil.notifyError(this._notificationService, this._optionsState.notificationResourceUpdateFailTextToTranslate);
    ctx.dispatch(CompositionActionHelper.getAll(this._nameSpace, action.parentId));
  }

  @RegisterDefaultAction((compositionNameSpace: CompositionNameSpace) => compositionNameSpace.Clean)
  clean(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, action: CompositionAction.Clean): void {
    this._resetStateToDefault(ctx, isNullOrUndefined(action.preserveList) ? true : action.preserveList, action.queryParameter);
  }

  private _cleanCurrentStateIfDefined(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, keepCurrentState: boolean): void {
    if (isNullOrUndefined(keepCurrentState) || isFalse(keepCurrentState)) {
      ctx.dispatch(CompositionActionHelper.clean(this._nameSpace, true));
    }
  }

  private _resetStateToDefault(ctx: SolidifyStateContext<CompositionStateModel<TResource>>, preserveListData: boolean, newQueryParameters?: QueryParameters): void {
    const oldList = ctx.getState().list;
    const oldTotal = ctx.getState().total;
    const oldQueryParameters = ctx.getState().queryParameters;
    const newState = this._getDefaultData();
    if (isTrue(preserveListData)) {
      newState.list = oldList;
      newState.total = oldTotal;
      newState.queryParameters = oldQueryParameters;
    }
    if (!isNullOrUndefined(newQueryParameters)) {
      newState.queryParameters = newQueryParameters;
    }
    ctx.patchState(newState);
  }

  private _getDefaultData(): CompositionStateModel<TResource> {
    return ObjectUtil.clone(StoreUtil.getDefaultData(this.constructor), true);
  }
}
