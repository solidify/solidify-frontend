/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - user.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {ApplicationRole} from "../../../core/models/dto/application-role.model";
import {BaseResource} from "../../../core-resources/models/dto/base-resource.model";
import {ChangeInfo} from "../../../core-resources/models/dto/change-info.model";
import {SolidifyObject} from "../../../core-resources/types/solidify-object.type";

export interface User extends BaseResource {
  /**
   * The _links_ list of the _users_ resource
   */
  _links?: SolidifyObject;
  /**
   * The application role details of the user
   */
  applicationRole?: ApplicationRole;
  /**
   * The email of the user
   */
  email?: string;
  /**
   * The first name of the user
   */
  firstName?: string;
  /**
   * The home organization of the user
   */
  homeOrganization?: string;
  /**
   * The last name of the user
   */
  lastName?: string;
  /**
   * The _users_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]]
   */
  resId?: string;
  creation?: ChangeInfo;
  lastUpdate?: ChangeInfo;
}
