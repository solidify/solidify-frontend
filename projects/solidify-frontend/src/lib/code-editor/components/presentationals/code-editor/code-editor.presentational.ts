/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - code-editor.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Inject,
  Injector,
  Input,
  OnInit,
  Optional,
  Self,
} from "@angular/core";
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NgControl,
  ValidatorFn,
} from "@angular/forms";
import {Store} from "@ngxs/store";
import {EditorConfiguration} from "codemirror";
import {
  filter,
  tap,
} from "rxjs/operators";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
} from "../../../../core-resources/tools/is/is.tool";
import {AbstractInternalPresentational} from "../../../../core/components/presentationals/abstract-internal/abstract-internal.presentational";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {DefaultSolidifyEnvironment} from "../../../../core/environments/environment.solidify-defaults";
import {FormValidationHelper} from "../../../../core/helpers/form-validation.helper";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {NotificationService} from "../../../../core/services/notification.service";
import {ClipboardUtil} from "../../../../core-resources/utils/clipboard.util";
import {MemoizedUtil} from "../../../../core/utils/stores/memoized.util";

@Component({
  selector: "solidify-code-editor",
  templateUrl: "./code-editor.presentational.html",
  styleUrls: ["./code-editor.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeEditorPresentational extends AbstractInternalPresentational implements OnInit, AfterViewInit {
  get control(): FormControl {
    return this._ngControl.control as FormControl;
  }

  @Input()
  title: string;

  @Input()
  required: boolean;

  @HostBinding("class.readonly")
  get readonly(): boolean {
    return this.control.disabled;
  }

  // supported language should be initialized in main.ts
  @Input()
  mode: string;

  isValid: boolean = undefined;

  syntaxErrorDetail: string;
  needTranslateSyntaxError: boolean = false;

  parser: ((value: string) => any) | undefined = undefined;

  get isDarkModeEnable(): boolean {
    return MemoizedUtil.selectSnapshot(this._store, this._environment.appState, state => state.darkMode);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  syntaxValidMessage: string;
  syntaxInvalidMessage: string;
  COPY_BUTTON_LABEL: string;
  protected _COPY_NOTIFICATION_LABEL: string;

  constructor(protected readonly _injector: Injector,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
              @Self() @Optional() protected readonly _ngControl: NgControl,
              protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _notificationService: NotificationService) {
    super(_injector);
    this.syntaxValidMessage = this.labelTranslateInterface.codeEditorSyntaxValid;
    this.syntaxInvalidMessage = this.labelTranslateInterface.codeEditorSyntaxInvalid;
    this.COPY_BUTTON_LABEL = this.labelTranslateInterface.codeEditorCopy;
    this._COPY_NOTIFICATION_LABEL = this.labelTranslateInterface.codeEditorNotificationCopiedToClipboard;

    if (this._ngControl) {
      // Note: we provide the value accessor through here, instead of
      // the `providers` to avoid running into a circular import.
      // And we use NOOP_VALUE_ACCESSOR so WrappedInput don't do anything with NgControl
      this._ngControl.valueAccessor = NOOP_VALUE_ACCESSOR;
    }
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
    if (isNotNullNorUndefined(this.parser)) {
      this.control.addValidators(this._syntaxValidator);
    }
  }

  private _syntaxValidator(): ValidatorFn {
    return (control: AbstractControl) => this.isValid ? {syntax: "error"} : null;
  }

  // Workaround to force parent to be informed of form value update
  forceRefreshBehavior: boolean = false;

  private _forceRefresh(): void {
    this.forceRefreshBehavior = true;
    this._changeDetector.detectChanges();
    this.forceRefreshBehavior = false;
    this._changeDetector.detectChanges();
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(this.control.valueChanges.pipe(
      tap(value => {
        this._computeIsValidSyntax(value);
        this._forceRefresh();
      }),
    ));

    this.subscribe(this.control.statusChanges.pipe(
      filter(status => status === SOLIDIFY_CONSTANTS.FORM_STATUS_INVALID),
      tap(status => {
        this._changeDetector.detectChanges();
      }),
    ));

    // Allow to change theme when global dark mode is enabled/disabled
    this.subscribe(MemoizedUtil.select(this._store, this._environment.appState, state => state.darkMode).pipe(
      tap(isDarkMode => {
        this._changeDetector.detectChanges();
      }),
    ));
  }

  private _computeIsValidSyntax(value: string): void {
    if (isNullOrUndefined(this.parser)) {
      this.isValid = undefined;
      this.syntaxErrorDetail = undefined;
      return;
    }
    if (isNullOrUndefinedOrWhiteString(value)) {
      this.isValid = undefined;
      this.syntaxErrorDetail = undefined;
      return;
    }
    try {
      const parsedQuery = this.parser(value);
      this.isValid = true;
      this.syntaxErrorDetail = undefined;
      this.control.setErrors(null);
    } catch (e) {
      this.isValid = false;
      this.syntaxErrorDetail = this._getSyntaxErrorMessage(e);
      this.control.setErrors({invalid: true});
    }
  }

  protected _getSyntaxErrorMessage(error: any): string {
    return error.message;
  }

  get codemirrorOption(): EditorConfiguration {
    return {
      lineNumbers: true,
      theme: this.isDarkModeEnable ? this._environment.codeEditorThemeDark : this._environment.codeEditorThemeLight,
      mode: this.mode,
      readOnly: this.control.disabled ? "nocursor" : false,
      lineWrapping: true,
    };
  };

  copyToClipboard(): void {
    const code = this.control.value;
    if (isNotNullNorUndefined(code) && ClipboardUtil.copyStringToClipboard(code)) {
      this._notificationService.showInformation(this._COPY_NOTIFICATION_LABEL);
    }
  }
}

export const NOOP_VALUE_ACCESSOR: ControlValueAccessor = {
  /* eslint-disable prefer-arrow/prefer-arrow-functions */
  writeValue(): void {},
  registerOnChange(): void {},
  registerOnTouched(): void {},
  setDisabledState(isDisabled: boolean): void {},
  /* eslint-enable prefer-arrow/prefer-arrow-functions */
};
