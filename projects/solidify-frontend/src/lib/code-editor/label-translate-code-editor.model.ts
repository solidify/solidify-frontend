/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - label-translate-code-editor.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MARK_AS_TRANSLATABLE} from "../core-resources/utils/translate.util";

export interface LabelSolidifyCodeEditor {
  codeEditorSyntaxValid?: string;
  codeEditorSyntaxInvalid?: string;
  codeEditorSeeErrorDetailsBelow?: string;
  codeEditorCopy?: string;
  codeEditorNotificationCopiedToClipboard?: string;
}

export const labelSolidifyCodeEditor: LabelSolidifyCodeEditor = {
  codeEditorSyntaxValid: MARK_AS_TRANSLATABLE("solidify.codeEditor.syntaxValid"),
  codeEditorSyntaxInvalid: MARK_AS_TRANSLATABLE("solidify.codeEditor.syntaxInvalid"),
  codeEditorSeeErrorDetailsBelow: MARK_AS_TRANSLATABLE("solidify.codeEditor.seeErrorDetailsBelow"),
  codeEditorCopy: MARK_AS_TRANSLATABLE("solidify.codeEditor.copy"),
  codeEditorNotificationCopiedToClipboard: MARK_AS_TRANSLATABLE("solidify.codeEditor.notification.copiedToClipboard"),
};
