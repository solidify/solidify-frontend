/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - oauth2.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpClient,
  HttpHeaders,
  HttpParams,
} from "@angular/common/http";
import {
  Inject,
  Injectable,
  NgZone,
  Optional,
} from "@angular/core";
import {
  NEVER,
  Observable,
  of,
  Subject,
  Subscription,
} from "rxjs";
import {
  catchError,
  delay,
  filter,
  map,
  switchMap,
  tap,
} from "rxjs/operators";
import {DefaultSolidifyEnvironment} from "../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../core/injection-tokens/environment.injection-token";
import {AbstractBaseService} from "../core/services/abstract-base.service";
import {
  isFunction,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefinedOrWhiteString,
  isTrue,
} from "../core-resources/tools/is/is.tool";
import {SsrUtil} from "../core-resources/utils/ssr.util";
import {UrlUtil} from "../core/utils/url.util";
import {
  EventType,
  OAuthErrorEvent,
  OAuthEvent,
  OAuthInfoEvent,
  OAuthSuccessEvent,
} from "./events";
import {LoginMode} from "./login-mode.enum";
import {OauthHelper} from "./oauth.helper";
import {
  LoginOptions,
  OAuthStorage,
  TokenResponse,
} from "./types";

/**
 * Service for logging in and logging out with
 * OIDC and OAuth2. Supports code flow.
 */
@Injectable({
  providedIn: "root",
})
export class OAuth2Service extends AbstractBaseService {
  readonly KEY_ACCESS_TOKEN: string = "access_token";
  readonly KEY_GRANTED_SCOPES: string = "granted_scopes";
  readonly KEY_ID_TOKEN: string = "id_token";
  readonly KEY_REFRESH_TOKEN: string = "refresh_token";
  readonly KEY_NONCE: string = "nonce";
  readonly KEY_EXPIRES_AT: string = "expires_at";
  readonly KEY_ID_TOKEN_CLAIMS_OBJ: string = "id_token_claims_obj";
  readonly KEY_ID_TOKEN_EXPIRES_AT: string = "id_token_expires_at";
  readonly KEY_ID_TOKEN_STORED_AT: string = "id_token_stored_at";
  readonly KEY_ACCESS_TOKEN_STORED_AT: string = "access_token_stored_at";
  readonly KEY_GRANT_TYPE: string = "grant_type";
  readonly KEY_CODE: string = "code";
  readonly KEY_REDIRECT_URI: string = "redirect_uri";
  readonly KEY_CLIENT_ID: string = "client_id";

  private _eventsSubject: Subject<OAuthEvent> = new Subject<OAuthEvent>();
  public events: Observable<OAuthEvent> = this._eventsSubject.asObservable();

  private _storage: OAuthStorage;
  private _accessTokenTimeoutSubscription: Subscription;
  private readonly _RESPONSE_TYPE: string = this.KEY_CODE;
  private readonly _timeoutFactor: number = 0.75;

  constructor(private readonly _ngZone: NgZone,
              private readonly _http: HttpClient,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment,
              @Optional() storage: OAuthStorage) {
    super();

    this.events = this._eventsSubject.asObservable();

    try {
      if (storage) {
        this.setStorage(storage);
      } else if (SsrUtil.window?.sessionStorage) {
        this.setStorage(SsrUtil.window.sessionStorage);
      }
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error("cannot access sessionStorage. " +
        "Consider setting an own storage implementation using setStorage", e);
    }
    this._setupRefreshTimer();
  }

  /**
   * Sets a custom storage used to store the received
   * tokens on client side. By default, the browser's
   * sessionStorage is used.
   *
   * @param storage storage service
   */
  setStorage(storage: OAuthStorage): void {
    this._storage = storage;
  }

  /**
   * Refreshes the token using a refresh_token.
   * This does not work for implicit flow, b/c
   * there is no refresh_token in this flow.
   */
  refreshToken(loginMode: LoginMode = LoginMode.STANDARD): Observable<boolean> {
    const refreshToken = this._storage.getItem(this.KEY_REFRESH_TOKEN);
    if (isNullOrUndefinedOrWhiteString(refreshToken)) {
      this._eventsSubject.next(new OAuthErrorEvent(EventType.token_refresh_error, new Error("Refresh token is not defined")));
      return of(false);
    }
    let params = new HttpParams();
    params = params.set(this.KEY_GRANT_TYPE, this.KEY_REFRESH_TOKEN);
    params = params.set(this.KEY_REFRESH_TOKEN, this._storage.getItem(this.KEY_REFRESH_TOKEN));
    return this._fetchToken(params, loginMode);
  }

  /**
   * Setup an automatic refresh token call when the access token expires
   */
  setupAutomaticRefreshToken(): void {
    this.subscribe(
      this.events.pipe(
        filter(e => e.type === EventType.token_expires),
        switchMap(() => this.refreshToken()),
      ),
    );

    this._restartRefreshTimerIfStillLoggedIn();
  }

  /**
   * Starts the authorization code flow and redirects to user to
   * the auth servers login url.
   */
  initAuthorizationCodeFlow(loginMode: LoginMode = LoginMode.STANDARD, stateToAdd: string = ""): Observable<never | boolean> {
    if (!this._validateUrlForHttps(OauthHelper.getLoginUrl(this._environment.authorization))) {
      throw new Error("loginUrl must use Http. Also check property requireHttps.");
    }
    const observable = this._createLoginUrl(loginMode, stateToAdd, "").pipe(
      tap(url => {
        SsrUtil.window.location.href = url;
      }),
      switchMap(() => NEVER), // Avoid guard to accept or refuse routing before auth process is done
      catchError((error: Error) => {
        // eslint-disable-next-line no-console
        console.error("Error in initAuthorizationCodeFlow", error);
        return of(false);
      }),
    );
    this.subscribe(observable);
    return observable;
  }

  /**
   * Checks whether there are tokens in the hash fragment
   * as a result of the implicit flow. These tokens are
   * parsed, validated and used to sign the user in to the
   * current client.
   *
   * @param options Optional options.
   */
  tryLogin(options: LoginOptions = null): Observable<boolean> {
    if (SsrUtil.isServer) {
      return of(false);
    }
    if (!this._environment.requestAccessToken && !this._environment.oidc) {
      // eslint-disable-next-line no-console
      console.warn("Either requestAccessToken or oidc or both must be true.");
      return of(false);
    }

    if (this.getAccessToken()) {
      return of(true);
    } else if (new Date(this.getAccessTokenExpiration()) >= new Date()) {
      return of(true);
    } else if (SsrUtil.window?.location.search && (SsrUtil.window?.location.search.startsWith("?code=") || SsrUtil.window?.location.search.includes("&code="))) {
      return this._extractCodeAndGetTokenFromCode(SsrUtil.window?.location.search);
    } else if (SsrUtil.window?.location.hash && (SsrUtil.window?.location.hash.includes("?code=") || SsrUtil.window?.location.hash.includes("&code="))) {
      return this._extractCodeAndGetTokenFromCode(SsrUtil.window?.location.hash);
    }
    return of(false);
  }

  private _extractCodeAndGetTokenFromCode(path: string): Observable<boolean> {
    const parameter = path.split("?")[1].split("&");
    const codeParam = parameter.filter(param => param.includes("code="));
    const code = codeParam.length ? codeParam[0].split("code=")[1] : undefined;
    if (code) {
      try {
        const stateParam = parameter.filter(param => param.includes("state="));
        const state = stateParam.length ? stateParam[0].split("state=")[1] : undefined;
        const loginMode = state.split(this._environment.nonceStateSeparator)[isTrue(this._environment.disableNonceCheck) ? 0 : 1] as LoginMode;
        return this._getTokenFromCode(code, loginMode);
      } catch (e) {
        return of(false);
      }
    }
  }

  /**
   * Returns the received claims about the user.
   */
  getIdentityClaims(): any {
    const claims = this._storage.getItem(this.KEY_ID_TOKEN_CLAIMS_OBJ);
    if (!claims) {
      return null;
    }
    return JSON.parse(claims);
  }

  /**
   * Returns the current access_token.
   */
  getAccessToken(): string {
    return this._storage.getItem(this.KEY_ACCESS_TOKEN);
  }

  /**
   * Returns the current refresh_token
   */
  getRefreshToken(): string {
    return this._storage.getItem(this.KEY_REFRESH_TOKEN);
  }

  /**
   * Returns the expiration date of the access_token
   * as milliseconds since 1970.
   */
  getAccessTokenExpiration(): number {
    if (!this._storage.getItem(this.KEY_EXPIRES_AT)) {
      return null;
    }
    return parseInt(this._storage.getItem(this.KEY_EXPIRES_AT), 10);
  }

  /**
   * Checkes, whether there is a valid access_token.
   */
  hasValidAccessToken(): boolean {
    if (this.getAccessToken()) {
      const expiresAt = this._storage.getItem(this.KEY_EXPIRES_AT);
      const now = new Date();

      return !(expiresAt && parseInt(expiresAt, 10) < now.getTime());
    }
    return false;
  }

  /**
   * Removes all tokens and logs the user out.
   * If a logout url is configured, the user is
   * redirected to it.
   *
   * @param noRedirectToLogoutUrl boolean to redirect or not
   */
  logOut(noRedirectToLogoutUrl: boolean = false): void {
    this.clearStorage();
    this._eventsSubject.next(new OAuthInfoEvent(EventType.logout));

    this.externalLogOutIfDefined(() => {
      if (!this._environment.logoutUrl) {
        return;
      }
      if (noRedirectToLogoutUrl) {
        return;
      }
      let logoutUrl: string;

      if (!this._validateUrlForHttps(this._environment.logoutUrl)) {
        throw new Error("logoutUrl must use Http. Also check property requireHttps.");
      }

      // For backward compatibility
      if (this._environment.logoutUrl.indexOf("{{") > -1) {
        logoutUrl = this._environment.logoutUrl
          .replace(/\{\{client_id\}\}/, this._environment.clientId);
      } else {
        logoutUrl =
          this._environment.logoutUrl +
          (this._environment.logoutUrl.indexOf("?") > -1 ? "&" : "?") +
          "id_token_hint=" +
          SsrUtil.window?.encodeURIComponent("") +
          "&post_logout_redirect_uri=" +
          SsrUtil.window?.encodeURIComponent(this._environment.postLogoutRedirectUri || this._environment.redirectUrl);
      }
      if (SsrUtil.window) {
        SsrUtil.window.location.href = logoutUrl;
      }
    });
  }

  /**
   * @ignore
   */
  createAndSaveNonce(): string {
    const nonce = this._createNonce();
    this._storage.setItem(this.KEY_NONCE, nonce);
    return nonce;
  }

  private _createNonce(): string {
    let text = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < 40; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  }

  private _restartRefreshTimerIfStillLoggedIn(): void {
    this._setupExpirationTimers();
  }

  private _createLoginUrl(loginMode: LoginMode = LoginMode.STANDARD,
                          state: string = "",
                          customRedirectUri: string = ""): Observable<string> {

    let redirectUri: string;

    if (customRedirectUri) {
      redirectUri = customRedirectUri;
    } else {
      redirectUri = this._environment.redirectUrl;
    }

    let queryState = "";
    const nonce = isTrue(this._environment.disableNonceCheck) ? null : this.createAndSaveNonce();
    if (isNotNullNorUndefinedNorWhiteString(nonce)) {
      queryState = nonce + this._environment.nonceStateSeparator;
    }

    queryState = queryState + loginMode;

    if (isNotNullNorUndefinedNorWhiteString(state)) {
      queryState = queryState + this._environment.nonceStateSeparator + state;
    }

    if (!this._environment.requestAccessToken && !this._environment.oidc) {
      throw new Error("Either requestAccessToken or oidc or both must be true");
    }

    const separationChar = this._environment.logoutUrl.indexOf("?") > -1 ? "&" : "?";

    const scope = this._environment.logoutUrl;

    let url =
      OauthHelper.getLoginUrl(this._environment.authorization) +
      separationChar +
      "response_type=" +
      SsrUtil.window?.encodeURIComponent(this._RESPONSE_TYPE) +
      "&client_id=" +
      SsrUtil.window?.encodeURIComponent(loginMode === LoginMode.MFA ? this._environment.clientIdMfa : this._environment.clientId) +
      "&state=" +
      SsrUtil.window?.encodeURIComponent(queryState) +
      "&redirect_uri=" +
      SsrUtil.window?.encodeURIComponent(redirectUri) +
      "&scope=" +
      SsrUtil.window?.encodeURIComponent(scope);

    if (nonce && this._environment.oidc) {
      url += "&nonce=" + SsrUtil.window?.encodeURIComponent(nonce);
    }

    return of(url);
  }

  private _storeAccessTokenResponse(accessToken: string, refreshToken: string, expiresIn: number, grantedScopes: string): void {
    this._storage.setItem(this.KEY_ACCESS_TOKEN, accessToken);
    if (grantedScopes) {
      this._storage.setItem(this.KEY_GRANTED_SCOPES, JSON.stringify(grantedScopes.split("+")));
    }
    this._storage.setItem(this.KEY_ACCESS_TOKEN_STORED_AT, "" + Date.now());
    if (expiresIn) {
      const expiresInMilliSeconds = expiresIn * 1000;
      const now = new Date();
      const expiresAt = now.getTime() + expiresInMilliSeconds;
      this._storage.setItem(this.KEY_EXPIRES_AT, "" + expiresAt);
    }

    if (refreshToken) {
      this._storage.setItem(this.KEY_REFRESH_TOKEN, refreshToken);
    }
  }

  private _validateUrlForHttps(url: string): boolean {
    if (!url) {
      return true;
    }

    const lcUrl = url.toLowerCase();

    if (this._environment.requireHttps === false) {
      return true;
    }
    return lcUrl.startsWith("https://");
  }

  private _setupRefreshTimer(): void {
    if (SsrUtil.isServer) {
      return;
    }

    this.subscribe(this.events.pipe(
      filter(e => e.type === EventType.token_received),
      tap(e => {
        this._clearAccessTokenTimer();
        this._setupExpirationTimers();
      }),
    ));
  }

  private _setupExpirationTimers(): void {
    if (this.hasValidAccessToken()) {
      this._setupAccessTokenTimer();
    }
  }

  private _setupAccessTokenTimer(): void {
    const expiration = this.getAccessTokenExpiration();
    const storedAt = this._getAccessTokenStoredAt();
    const timeout = this._calcTimeout(storedAt, expiration);

    this._ngZone.runOutsideAngular(() => {
      this._accessTokenTimeoutSubscription = of(new OAuthInfoEvent(EventType.token_expires, this.KEY_ACCESS_TOKEN))
        .pipe(delay(timeout))
        .subscribe(e => {
          this._ngZone.run(() => {
            this._eventsSubject.next(e);
          });
        });
    });
  }

  private _clearAccessTokenTimer(): void {
    if (this._accessTokenTimeoutSubscription) {
      this._accessTokenTimeoutSubscription.unsubscribe();
    }
  }

  private _calcTimeout(storedAt: number, expiration: number): number {
    return (expiration - storedAt) * this._timeoutFactor;
  }

  /**
   * Get token using an intermediate code. Works for the Authorization Code flow.
   */
  private _getTokenFromCode(code: string, loginMode: LoginMode): Observable<boolean> {
    const params = new HttpParams()
      .set(this.KEY_GRANT_TYPE, "authorization_code")
      .set(this.KEY_CODE, code)
      .set(this.KEY_REDIRECT_URI, this._environment.redirectUrl);
    return this._fetchToken(params, loginMode).pipe(
      tap((success) => this._cleanOAuthInfosInUrl()),
      catchError(err => {
        throw err;
      }),
    );
  }

  private _cleanOAuthInfosInUrl(): void {
    if (SsrUtil.window?.history.replaceState) {
      // Prevents browser from storing in history the query param "code" (prevent that usage of back browser navigation cause new authentification)
      SsrUtil.window?.history.replaceState({}, null, window.location.href.split("?")[0]);
    }
  }

  private _fetchToken(params: HttpParams, loginMode: LoginMode = LoginMode.STANDARD): Observable<boolean> {
    if (!this._validateUrlForHttps(OauthHelper.getTokenEndpoint(this._environment.authorization))) {
      throw new Error("tokenEndpoint must use Http. Also check property requireHttps.");
    }

    params = params.set(this.KEY_CLIENT_ID, loginMode === LoginMode.MFA ? this._environment.clientIdMfa : this._environment.clientId);

    const authData = SsrUtil.window?.btoa((loginMode === LoginMode.MFA ? this._environment.clientIdMfa : this._environment.clientId) + ":" + (loginMode === LoginMode.MFA ? this._environment.dummyClientMfaSecret : this._environment.dummyClientSecret));
    const headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded")
      .set("Authorization", "Basic " + authData);

    return this._http.post<TokenResponse>(OauthHelper.getTokenEndpoint(this._environment.authorization), params, {headers})
      .pipe(
        map((tokenResp) => {
          this._storeAccessTokenResponse(tokenResp.access_token, tokenResp.refresh_token, tokenResp.expires_in, tokenResp.scope);
          this._eventsSubject.next(new OAuthSuccessEvent(EventType.token_received));
          this._eventsSubject.next(new OAuthSuccessEvent(EventType.token_refreshed));
          return true;
        }),
        catchError((err) => {
          // eslint-disable-next-line no-console
          console.error("Error getting token", err);
          this.clearStorage();
          this._eventsSubject.next(new OAuthErrorEvent(EventType.token_refresh_error, err));
          throw err;
        }),
      );
  }

  private _getAccessTokenStoredAt(): number {
    return parseInt(this._storage.getItem(this.KEY_ACCESS_TOKEN_STORED_AT), 10);
  }

  externalLogOutIfDefined(callback?: () => void): void {
    if (this._environment.callExternalLogOut) {
      this.externalLogOut(callback);
    } else {
      if (isFunction(callback)) {
        callback();
      }
    }
  }

  externalLogOut(callback?: () => void): void {
    const authorizationRoot = UrlUtil.getOrigin(this._environment.authorization);
    const externalLogOutUrl = authorizationRoot + this._environment.authorizationExternalLogOutSuffixUrl;
    const tab = SsrUtil.window?.open(externalLogOutUrl, "_blank");
    setTimeout(() => {
      tab.close();
      if (isFunction(callback)) {
        callback();
      }
    }, this._environment.externalLogOutTimeoutCloseTab);
  }

  clearStorage(): void {
    this._storage.removeItem(this.KEY_ACCESS_TOKEN);
    this._storage.removeItem(this.KEY_ID_TOKEN);
    this._storage.removeItem(this.KEY_REFRESH_TOKEN);
    this._storage.removeItem(this.KEY_NONCE);
    this._storage.removeItem(this.KEY_EXPIRES_AT);
    this._storage.removeItem(this.KEY_ID_TOKEN_CLAIMS_OBJ);
    this._storage.removeItem(this.KEY_ID_TOKEN_EXPIRES_AT);
    this._storage.removeItem(this.KEY_ID_TOKEN_STORED_AT);
    this._storage.removeItem(this.KEY_ACCESS_TOKEN_STORED_AT);
  }
}
