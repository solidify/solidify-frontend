/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - oauth2-interceptor.service.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  HTTP_INTERCEPTORS,
  HttpRequest,
  HttpResponse,
} from "@angular/common/http";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {MatSnackBar} from "@angular/material/snack-bar";
import {
  TranslateModule,
  TranslateService,
} from "@ngx-translate/core";
import {EMPTY} from "rxjs";
import {ENVIRONMENT} from "../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../core/injection-tokens/label-to-translate.injection-token";
import {SNACK_BAR} from "../core/models/snackbar/snack-bar.model";
import {SolidifyFrontendMaterialModule} from "../core/solidify-frontend-material.module";
import {MockTranslateService} from "../core/tests/helpers/mock-translate.service";
import {OAuth2Interceptor} from "./oauth2-interceptor.service";
import {OAuthStorage} from "./types";

describe("OAuth2Interceptor", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        SolidifyFrontendMaterialModule,
        TranslateModule,
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: OAuth2Interceptor,
          multi: true,
        },
        {
          provide: TranslateService,
          useClass: MockTranslateService,
        },
        {
          provide: OAuthStorage,
          useValue: sessionStorage,
        },
        {
          provide: SNACK_BAR,
          useClass: MatSnackBar,
        },
        {
          provide: LABEL_TRANSLATE,
          useValue: {},
        },
        {
          provide: ENVIRONMENT,
          useValue: {
            allowedUrls: [],
            sendAccessToken: true,
          },
        },
      ],
    });
  });

  it("should be created", () => {
    const service: OAuth2Interceptor = TestBed.get(OAuth2Interceptor);
    expect(service).toBeTruthy();
  });

  it("should set header authorization", () => {
    const token: string = "token_value";
    const service: OAuth2Interceptor = TestBed.get(OAuth2Interceptor);
    spyOn(sessionStorage, "getItem").and.returnValue(token);
    let response: HttpResponse<any>;
    const next: any = {
      handle: responseHandle => {
        response = responseHandle;
        return EMPTY;
      },
    };
    const request: HttpRequest<any> = new HttpRequest<any>("GET", "/api/deposits");
    service.intercept(request, next);
    expect(response.headers.get("Authorization")).toEqual("Bearer " + token);
  });

});
