/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - oauth.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {isNotNullNorUndefinedNorWhiteString} from "../core-resources/tools/is/is.tool";
import {SsrUtil} from "../core-resources/utils/ssr.util";
import {SOLIDIFY_CONSTANTS} from "../core-resources/constants";
import {DefaultSolidifyEnvironment} from "../core/environments/environment.solidify-defaults";
import {Token} from "../core/models/token.model";
import {UrlUtil} from "../core/utils/url.util";

export class OauthHelper {
  private static readonly _OAUTH: string = "oauth";
  private static readonly _TOKEN: string = "token";
  private static readonly _AUTHORIZE: string = "authorize";
  private static readonly _MFA_SCOPE: string = "auth-mfa";

  static readonly MFA_NEEDED: string = "MFA needed";

  private static _getTokenRootPath(authorizationUrl: string): string {
    return UrlUtil.addUrlSeparatorAtEndIfMissing(authorizationUrl) + this._OAUTH;
  }

  static getTokenEndpoint(authorizationUrl: string): string {
    return this._getTokenRootPath(authorizationUrl) + "/" + this._TOKEN;
  }

  static getLoginUrl(authorizationUrl: string): string {
    return this._getTokenRootPath(authorizationUrl) + "/" + this._AUTHORIZE;
  }

  static isTokenMfa(token: Token): boolean {
    return token.scope.includes(this._MFA_SCOPE);
  }

  static getCurrentPath(environment: DefaultSolidifyEnvironment): string {
    const baseHref = environment.baseHref;
    let currentPath = SsrUtil.window?.location.pathname + SsrUtil.window?.location.search;
    if (isNotNullNorUndefinedNorWhiteString(baseHref) && baseHref !== SOLIDIFY_CONSTANTS.URL_SEPARATOR && currentPath.startsWith(baseHref)) {
      currentPath = currentPath.substring(baseHref.length);
      currentPath = SOLIDIFY_CONSTANTS.URL_SEPARATOR + currentPath;
    }
    if (currentPath === SOLIDIFY_CONSTANTS.URL_SEPARATOR) {
      return "";
    }
    return currentPath;
  }
}
