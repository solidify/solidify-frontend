/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - events.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {SolidifyObject} from "../core-resources/types/solidify-object.type";

export type EventType =
  | "discovery_document_loaded"
  | "received_first_token"
  | "jwks_load_error"
  | "invalid_nonce_in_state"
  | "discovery_document_load_error"
  | "discovery_document_validation_error"
  | "user_profile_loaded"
  | "user_profile_load_error"
  | "token_received"
  | "token_error"
  | "token_refreshed"
  | "token_refresh_error"
  | "silent_refresh_error"
  | "silently_refreshed"
  | "silent_refresh_timeout"
  | "token_validation_error"
  | "token_expires"
  | "session_changed"
  | "session_error"
  | "session_terminated"
  | "logout";
export const EventType = {
  discovery_document_loaded: "discovery_document_loaded" as EventType,
  received_first_token: "received_first_token" as EventType,
  jwks_load_error: "jwks_load_error" as EventType,
  invalid_nonce_in_state: "invalid_nonce_in_state" as EventType,
  discovery_document_load_error: "discovery_document_load_error" as EventType,
  discovery_document_validation_error: "discovery_document_validation_error" as EventType,
  user_profile_loaded: "user_profile_loaded" as EventType,
  user_profile_load_error: "user_profile_load_error" as EventType,
  token_received: "token_received" as EventType,
  token_error: "token_error" as EventType,
  token_refreshed: "token_refreshed" as EventType,
  token_refresh_error: "token_refresh_error" as EventType,
  silent_refresh_error: "silent_refresh_error" as EventType,
  silently_refreshed: "silently_refreshed" as EventType,
  silent_refresh_timeout: "silent_refresh_timeout" as EventType,
  token_validation_error: "token_validation_error" as EventType,
  token_expires: "token_expires" as EventType,
  session_changed: "session_changed" as EventType,
  session_error: "session_error" as EventType,
  session_terminated: "session_terminated" as EventType,
  logout: "logout" as EventType,
};

export abstract class OAuthEvent {
  constructor(readonly type: EventType) {}
}

export class OAuthSuccessEvent extends OAuthEvent {
  constructor(type: EventType, readonly info: any = null) {
    super(type);
  }
}

export class OAuthInfoEvent extends OAuthEvent {
  constructor(type: EventType, readonly info: any = null) {
    super(type);
  }
}

export class OAuthErrorEvent extends OAuthEvent {
  constructor(
    type: EventType,
    readonly reason: SolidifyObject = null,
    readonly params: SolidifyObject = null,
  ) {
    super(type);
  }
}
