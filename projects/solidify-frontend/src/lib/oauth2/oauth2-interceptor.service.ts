/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - oauth2-interceptor.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpBackend,
  HttpClient,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpStatusCode,
} from "@angular/common/http";
import {
  Inject,
  Injectable,
} from "@angular/core";
import {Router} from "@angular/router";
import {
  BehaviorSubject,
  filter,
  Observable,
  switchMap,
} from "rxjs";
import {
  catchError,
  mergeMap,
  tap,
} from "rxjs/operators";
import {SOLIDIFY_CONSTANTS} from "../core-resources/constants";
import {DefaultSolidifyEnvironment} from "../core/environments/environment.solidify-defaults";
import {JwtTokenHelper} from "../core/helpers/jwt-token.helper";
import {HeaderEnum} from "../core/http/header.enum";
import {ENVIRONMENT} from "../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../core/injection-tokens/label-to-translate.injection-token";
import {SolidifyHttpErrorResponseModel} from "../core/models/errors/solidify-http-error-response.model";
import {AbstractBaseService} from "../core/services/abstract-base.service";
import {NotificationService} from "../core/services/notification.service";
import {
  isArray,
  isFalse,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
} from "../core-resources/tools/is/is.tool";
import {LabelTranslateInterface} from "../label-translate-interface.model";

import {EventType} from "./events";
import {LoginMode} from "./login-mode.enum";
import {OauthHelper} from "./oauth.helper";
import {OAuth2Service} from "./oauth2.service";
import {OAuthStorage} from "./types";

@Injectable({
  providedIn: "root",
})
export class OAuth2Interceptor extends AbstractBaseService implements HttpInterceptor {
  private _httpClient: HttpClient;

  private _errorRefreshTokenBrokeApp: boolean = false;
  // Allow to queue the token refresh intention
  // if undefined : there is no token refresh process running
  // if LoginMode : there is a token refresh process running
  private readonly _pendingTokenRefreshedWithMfaBS: BehaviorSubject<LoginMode | undefined> = new BehaviorSubject<LoginMode | undefined>(undefined);

  constructor(private readonly _authStorage: OAuthStorage,
              private readonly _oAuth2Service: OAuth2Service,
              private readonly _router: Router,
              private readonly _notificationService: NotificationService,
              private readonly _httpBackend: HttpBackend,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment,
              @Inject(LABEL_TRANSLATE) private readonly _labelTranslate: LabelTranslateInterface,
  ) {
    super();

    this._httpClient = new HttpClient(_httpBackend);

    this.subscribe(this._oAuth2Service.events.pipe(
      filter(e => e.type === EventType.token_refresh_error),
      tap(e => {
        this._errorRefreshTokenBrokeApp = true;
      }),
    ));
  }

  private _shouldBypassOAuth2TokenHeader(req: HttpRequest<any>): boolean {
    if (req.headers.has(HeaderEnum.skipOauth2TokenHeader) && req.headers.get(HeaderEnum.skipOauth2TokenHeader) === SOLIDIFY_CONSTANTS.STRING_TRUE) {
      return true;
    }
    const url = req.url.toLowerCase();
    if (url === OauthHelper.getTokenEndpoint(this._environment.authorization)) {
      return true;
    }
    if (isArray(this._environment.allowedUrls)) {
      const allowedUrlsToLower = this._environment.allowedUrls.map(a => a.toLowerCase());
      const matchingAllowedUrl = allowedUrlsToLower.find(a => url.indexOf(a) !== -1);
      if (isNotNullNorUndefined(matchingAllowedUrl)) {
        return true;
      }
    }
    if (url.startsWith("assets/") || url.startsWith("./assets/")) {
      return true;
    }
    return false;
  }

  private _shouldAvoidTokenRequestWhenNeeded(req: HttpRequest<any>): boolean {
    if (req.headers.has(HeaderEnum.avoidTokenRequestWhenNeeded) && req.headers.get(HeaderEnum.avoidTokenRequestWhenNeeded) === SOLIDIFY_CONSTANTS.STRING_TRUE) {
      return true;
    }
    return false;
  }

  private _removeUndesiredHeader(req: HttpRequest<any>): HttpRequest<any> {
    let isUpdated = false;
    let headers = req.headers;
    if (req.headers.has(HeaderEnum.skipOauth2TokenHeader)) {
      headers = headers.delete(HeaderEnum.skipOauth2TokenHeader);
      isUpdated = true;
    }
    if (req.headers.has(HeaderEnum.avoidTokenRequestWhenNeeded)) {
      headers = req.headers.delete(HeaderEnum.avoidTokenRequestWhenNeeded);
      isUpdated = true;
    }
    if (isUpdated) {
      req = req.clone({headers});
    }
    return req;
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler, noRetry: boolean = false): Observable<HttpEvent<any>> {
    if (this._shouldBypassOAuth2TokenHeader(req)) {
      return next.handle(this._removeUndesiredHeader(req));
    }

    const shouldAvoidMfaTokenRequestWhenNeeded = this._shouldAvoidTokenRequestWhenNeeded(req);
    req = this._removeUndesiredHeader(req);

    const sendAccessToken = this._environment.sendAccessToken;

    if (sendAccessToken) {
      const accessToken = this._oAuth2Service.getAccessToken();
      if (isNotNullNorUndefinedNorWhiteString(accessToken)) {
        const header = "Bearer " + accessToken;
        const headers = req.headers.set("Authorization", header);
        req = req.clone({headers});
      }
    }

    return next
      .handle(req)
      .pipe(
        catchError((err: SolidifyHttpErrorResponseModel) => {
          if (isFalse(this._errorRefreshTokenBrokeApp) && !noRetry && (err.status === HttpStatusCode.Unauthorized || err.status === HttpStatusCode.NotAcceptable)) {
            if (isFalse(this.isPendingRefreshTokenRequest)) {

              if (shouldAvoidMfaTokenRequestWhenNeeded) {
                throw err;
              }

              const loginMode = this._getLoginModeFromError(err);

              if (loginMode === LoginMode.STANDARD) {
                const refreshToken = this._oAuth2Service.getRefreshToken();
                const isRefreshTokenPresent = isNotNullNorUndefinedNorWhiteString(refreshToken);
                if (isRefreshTokenPresent) {
                  return this._getNewTokenWithRefreshToken(refreshToken, req, next, err);
                } else {
                  this._getNewTokenWithAuthorizationCodeFlow(loginMode);
                }
              }

              if (loginMode === LoginMode.MFA) {
                this._promptMfaNeeded();
              }

            } else {
              return this._waitPendingRefreshTokenRequestEndToContinue(req, next);
            }
          }
          throw err;
        }),
      );
  }

  private _promptMfaNeeded(): void {
    this._notificationService.showWarning(this._labelTranslate.oauth2MfaIsRequiredToPerformThisAction, undefined, {
      text: this._labelTranslate.oauth2ReConnect,
      callback: () => {
        this._oAuth2Service.externalLogOutIfDefined(() => {
          this._getNewTokenWithAuthorizationCodeFlow(LoginMode.MFA);
        });
      },
    });
  }

  private _getNewTokenWithRefreshToken(refreshToken: string, req: HttpRequest<any>, next: HttpHandler, err: SolidifyHttpErrorResponseModel): Observable<HttpEvent<any>> {
    const loginMode = this._getLoginModeFromRefreshToken(refreshToken);
    this._blockOtherRequestWithInvalidToken(loginMode);
    return this._oAuth2Service.refreshToken(loginMode).pipe(
      mergeMap(value => {
        this._unblockOtherPendingRequestWithInvalidToken();
        if (value) {
          return this.intercept(req, next, true);
        }
        throw err;
      }),
      catchError(err2 => {
        this._unblockOtherPendingRequestWithInvalidToken();
        this._oAuth2Service.clearStorage();
        this._oAuth2Service.initAuthorizationCodeFlow(loginMode, OauthHelper.getCurrentPath(this._environment));
        throw err2;
      }),
    );
  }

  private _getNewTokenWithAuthorizationCodeFlow(loginMode: LoginMode): void {
    this._blockOtherRequestWithInvalidToken(loginMode);
    this._oAuth2Service.clearStorage();
    this._oAuth2Service.initAuthorizationCodeFlow(loginMode, OauthHelper.getCurrentPath(this._environment));
  }

  private _getLoginModeFromError(err: SolidifyHttpErrorResponseModel): LoginMode {
    const isMfa = err.error?.error === OauthHelper.MFA_NEEDED;
    return isMfa ? LoginMode.MFA : LoginMode.STANDARD;
  }

  private _getLoginModeFromRefreshToken(refreshToken: string): LoginMode {
    const refreshTokenDecoded = JwtTokenHelper.decodeToken(refreshToken);
    return refreshTokenDecoded.client_id === this._environment.clientIdMfa ? LoginMode.MFA : LoginMode.STANDARD;
  }

  private _waitPendingRefreshTokenRequestEndToContinue(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this._pendingTokenRefreshedWithMfaBS.asObservable().pipe(
      filter(value => isNullOrUndefined(value)),
      switchMap(() => this.intercept(req, next, this._errorRefreshTokenBrokeApp)),
    );
  }

  private get isPendingRefreshTokenRequest(): boolean {
    return isNotNullNorUndefined(this._pendingTokenRefreshedWithMfaBS.value);
  }

  private _blockOtherRequestWithInvalidToken(loginMode: LoginMode): void {
    this._pendingTokenRefreshedWithMfaBS.next(loginMode);
  }

  private _unblockOtherPendingRequestWithInvalidToken(): void {
    this._pendingTokenRefreshedWithMfaBS.next(undefined);
  }
}
