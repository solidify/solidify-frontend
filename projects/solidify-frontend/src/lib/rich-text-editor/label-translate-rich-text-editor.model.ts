/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - label-translate-rich-text-editor.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MARK_AS_TRANSLATABLE} from "../core-resources/utils/translate.util";

export interface LabelSolidifyRichTextEditor {
  richTextEditorCharacters?: string;
  richTextEditorDisplayWithFormatting?: string;
  richTextEditorDisplayWithoutFormatting?: string;
  richTextEditorMaximum?: string;
  richTextEditorToolbarBold?: string;
  richTextEditorToolbarCleanStyle?: string;
  richTextEditorToolbarImage?: string;
  richTextEditorToolbarItalic?: string;
  richTextEditorToolbarLink?: string;
  richTextEditorToolbarStrike?: string;
  richTextEditorToolbarSub?: string;
  richTextEditorToolbarSuper?: string;
  richTextEditorToolbarUnderline?: string;
  richTextEditorToolbarVideo?: string;
  richTextEditorWords?: string;
}

export const labelSolidifyRichTextEditor: LabelSolidifyRichTextEditor = {
  richTextEditorCharacters: MARK_AS_TRANSLATABLE("solidify.richTextEditor.characters"),
  richTextEditorDisplayWithFormatting: MARK_AS_TRANSLATABLE("solidify.richTextEditor.displayWithFormatting"),
  richTextEditorDisplayWithoutFormatting: MARK_AS_TRANSLATABLE("solidify.richTextEditor.displayWithoutFormatting"),
  richTextEditorMaximum: MARK_AS_TRANSLATABLE("solidify.richTextEditor.maximum"),
  richTextEditorToolbarBold: MARK_AS_TRANSLATABLE("solidify.richTextEditor.toolbar.bold"),
  richTextEditorToolbarCleanStyle: MARK_AS_TRANSLATABLE("solidify.richTextEditor.toolbar.cleanStyle"),
  richTextEditorToolbarImage: MARK_AS_TRANSLATABLE("solidify.richTextEditor.toolbar.image"),
  richTextEditorToolbarItalic: MARK_AS_TRANSLATABLE("solidify.richTextEditor.toolbar.italic"),
  richTextEditorToolbarLink: MARK_AS_TRANSLATABLE("solidify.richTextEditor.toolbar.link"),
  richTextEditorToolbarStrike: MARK_AS_TRANSLATABLE("solidify.richTextEditor.toolbar.strike"),
  richTextEditorToolbarSub: MARK_AS_TRANSLATABLE("solidify.richTextEditor.toolbar.sub"),
  richTextEditorToolbarSuper: MARK_AS_TRANSLATABLE("solidify.richTextEditor.toolbar.super"),
  richTextEditorToolbarUnderline: MARK_AS_TRANSLATABLE("solidify.richTextEditor.toolbar.underline"),
  richTextEditorToolbarVideo: MARK_AS_TRANSLATABLE("solidify.richTextEditor.toolbar.video"),
  richTextEditorWords: MARK_AS_TRANSLATABLE("solidify.richTextEditor.words"),
};
