/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - rich-text-editor.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnInit,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
// ssr:comment:start
import {ContentChange} from "ngx-quill";
import Quill from "quill";
// ssr:comment:end
import {tap} from "rxjs/operators";
import {AbstractPresentational} from "../core/components/presentationals/abstract/abstract.presentational";

import {FormValidationHelper} from "../core/helpers/form-validation.helper";
import {LABEL_TRANSLATE} from "../core/injection-tokens/label-to-translate.injection-token";
import {
  isNonEmptyString,
  isNullOrUndefinedOrWhiteString,
  isNumberReal,
  isTrue,
} from "../core-resources/tools/is/is.tool";
// ssr:comment:start
import {MappingObject} from "../core-resources/types/mapping-type.type";
// ssr:comment:end
import {LabelTranslateInterface} from "../label-translate-interface.model";

@Component({
  selector: "solidify-rich-text-editor",
  templateUrl: "./rich-text-editor.presentational.html",
  styleUrls: ["./rich-text-editor.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: RichTextEditorPresentational,
    },
  ],
})
export class RichTextEditorPresentational extends AbstractPresentational implements ControlValueAccessor, OnInit, AfterViewInit {
  private readonly _TAB_KEY_CODE: number = 9;

  @Input()
  label: string;

  @Input()
  placeholder: string = "";

  @Input()
  formControl: FormControl;

  @Input()
  options: RichTextEditionOption = {
    bold: true,
    italic: true,
    underline: true,
    strike: true,
    sub: true,
    super: true,
    clean: true,
    link: true,
    image: false,
    video: false,
  };

  @Input()
  withWordCounter: boolean = true;

  @Input()
  allowTabChar: boolean = false;

  @Input()
  maxLength: number | undefined = undefined;

  isFocused: boolean = false;
  isInRichTextMode: boolean = true;
  displayQuillEditor: boolean = false;

  readonly TOOLBAR_TOOLTIP_DELAY: number = 500;

  get withMaxLength(): boolean {
    return isNumberReal(this.maxLength) && this.maxLength > 0;
  }

  get numberWords(): number {
    const text = this.formControl.value as string;
    if (isNullOrUndefinedOrWhiteString(text)) {
      return 0;
    }
    return text.split(" ").filter(term => isNonEmptyString(term)).length;
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  // ssr:comment:start
  preventLengthToExceed($event: ContentChange): void {
    // ssr:comment:end
    // ssr:uncomment:start
    // preventLengthToExceed($event: any): void {
    // ssr:uncomment:end
    if (this.withMaxLength) {
      if ($event.html?.length > this.maxLength) {
        const exceededCharLength = $event.html?.length - this.maxLength;
        const textLength = $event.text?.length;
        $event.editor.deleteText(textLength - exceededCharLength - 1, exceededCharLength);
        this.formControl.setValue($event.editor.root.innerHTML);
      }
    }
  }

  constructor(@Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();

    this._registerFormatting();
    this._registerCustomIcons();

    this.formControl.registerOnDisabledChange(() => {
      this._changeDetector.detectChanges();
    });
    this.subscribe(this.formControl.statusChanges.pipe(
      tap(() => {
        this._changeDetector.detectChanges();
      }),
    ));
    this.subscribe(this.formControl.valueChanges.pipe(
      tap(() => {
        this._changeDetector.detectChanges();
      }),
    ));
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();

    setTimeout(() => {
      // Workaround to display correctly line break
      this.displayQuillEditor = true;
      this._changeDetector.detectChanges();
    }, 0);
  }

  registerOnChange(fn: any): void {
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(obj: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  toggleTextMode(): void {
    this.isInRichTextMode = !this.isInRichTextMode;
  }

  focus(): void {
    this.isFocused = true;
  }

  blur(): void {
    this.isFocused = false;
    this._cleanUselessWrappingTags();
  }

  private _cleanUselessWrappingTags(): void {
    const value = this.formControl.value;
    if (isNullOrUndefinedOrWhiteString(value)) {
      return;
    }
    if (value.startsWith("<p>") && value.endsWith("</p>")) {
      const valueWithoutTag = value.substring(3, value.length - 4);
      if (valueWithoutTag.indexOf("<p>") === -1) {
        this.formControl.setValue(valueWithoutTag);
      }
    }
  }

  private _registerFormatting(): void {
    // ssr:comment:start
    const Inline = Quill.import("blots/inline") as any;

    class BoldBlot extends Inline {
    }

    BoldBlot["blotName"] = "bold";
    BoldBlot["tagName"] = "b";

    class ItalicBlot extends Inline {
    }

    ItalicBlot["blotName"] = "italic";
    ItalicBlot["tagName"] = "i";

    Quill.register(BoldBlot);
    Quill.register(ItalicBlot);
    // ssr:comment:end
  }

  private _registerCustomIcons(): void {
    // ssr:comment:start
    const Icons: MappingObject<string, string> = Quill.import("ui/icons") as any;
    Icons["clean"] = `<div class="icon-clean"></div>`;
    Quill.register(Icons as any);
    // ssr:comment:end
  }

  onEditorCreated(quill: any): void {
    this._manageTabKey(quill);
  }

  private _manageTabKey(quill: any): void {
    if (isTrue(this.allowTabChar)) {
      return;
    }
    delete quill?.keyboard?.bindings?.[this._TAB_KEY_CODE];
  }

  mousedown($event: MouseEvent): void {
    // Allow to work with cdkDrag
    $event.stopPropagation();
  }
}

export interface RichTextEditionOption {
  bold?: boolean;
  italic?: boolean;
  underline?: boolean;
  strike?: boolean;
  sub?: boolean;
  super?: boolean;
  clean?: boolean;
  link?: boolean;
  image?: boolean;
  video?: boolean;
}

