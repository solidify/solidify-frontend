/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - app-status.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {
  SwUpdate,
  VersionReadyEvent,
} from "@angular/service-worker";
import {
  BehaviorSubject,
  EMPTY,
  from,
  fromEvent,
  merge,
  Observable,
  of,
  timer,
} from "rxjs";
import {
  filter,
  map,
  skipUntil,
  tap,
} from "rxjs/operators";
import {SsrUtil} from "../core-resources/utils/ssr.util";
import {DefaultSolidifyAppStatusEnvironment} from "../core/environments/environment.solidify-app-status";
import {ENVIRONMENT} from "../core/injection-tokens/environment.injection-token";
import {AbstractBaseService} from "../core/services/abstract-base.service";
import {LoggingService} from "../core/services/logging.service";

@Injectable({
  providedIn: "root",
})
export class AppStatusService extends AbstractBaseService {
  private _swUpdatesBS: BehaviorSubject<string | undefined> = new BehaviorSubject(undefined);

  constructor(private readonly _updates: SwUpdate,
              private readonly _loggingService: LoggingService,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyAppStatusEnvironment) {
    super();
    if (this._updates.isEnabled) {
      this.subscribe(timer(this._environment.firstCheckNewVersionAvailableInSeconds * 1000, this._environment.intervalCheckNewVersionAvailableInSeconds * 1000)
        .pipe(
          tap(() => this._updates.checkForUpdate()),
        ),
      );

      // Notify when new service worker version is available
      this.subscribe(this._updates.versionUpdates.pipe(
        tap(v => {
          this._loggingService.logInfo("[AppUpdateAvailable] version update event", v);
        }),
        filter(v => v.type === "VERSION_READY"),
        tap((event: VersionReadyEvent) => {
          this._loggingService.logInfo("[AppUpdateAvailable] current version: ", event.currentVersion.hash);
          this._loggingService.logInfo("[AppUpdateAvailable] available version: ", event.latestVersion.hash);

          if (event.latestVersion) {
            this._swUpdatesBS.next(event.latestVersion.hash.slice(0, 4));
          }
        }),
      ));
    }
  }

  reloadApp(): Observable<void> {
    return from(this._updates.activateUpdate().then(() => SsrUtil.window?.document.location.reload()));
  }

  get updateAvailable(): Observable<string> {
    return this._swUpdatesBS.asObservable();
  }

  get online(): Observable<boolean> {
    const windowsEvent: Observable<boolean> = SsrUtil.window ? merge(
      fromEvent(SsrUtil.window, "online").pipe(map(() => true)),
      fromEvent(SsrUtil.window, "offline").pipe(map(() => false)),
    ) : EMPTY;

    return merge(
      of(SsrUtil.window?.navigator.onLine),
      windowsEvent.pipe(skipUntil(timer(3000))), // Allow to prevent "online event" when we arrived in app in offline mode.
    );
    // TOFIX : Find a way to know when back to online in the case we arrived in app in offline mode.
    // In this case fromEvent(window, "online") dispatch an event, but should not. Then when we go back in online mode we don't get event online...
  }
}
