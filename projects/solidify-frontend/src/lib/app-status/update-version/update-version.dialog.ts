/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - update-version.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {isNullOrUndefined} from "../../core-resources/tools/is/is.tool";
import {AbstractDialog} from "../../core/components/dialogs/abstract/abstract.dialog";
import {ButtonColorEnum} from "../../core/enums/button-color.enum";
import {LABEL_TRANSLATE} from "../../core/injection-tokens/label-to-translate.injection-token";
import {LabelTranslateInterface} from "../../label-translate-interface.model";
import {AppStatusService} from "../app-status.service";

@Component({
  selector: "solidify-update-version-dialog",
  templateUrl: "./update-version.dialog.html",
  styleUrls: ["./update-version.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateVersionDialog extends AbstractDialog<UpdateVersionDialogData, void> {
  constructor(protected readonly _dialogRef: MatDialogRef<UpdateVersionDialog>,
              private readonly _appStatusService: AppStatusService,
              private readonly _httpClient: HttpClient,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface,
              @Inject(MAT_DIALOG_DATA) readonly data: UpdateVersionDialogData,
  ) {
    super(_dialogRef, data);
  }

  update(): void {
    this._appStatusService.reloadApp();
  }

  get colorConfirm(): ButtonColorEnum {
    return isNullOrUndefined(this.data?.colorConfirm) ? ButtonColorEnum.primary : this.data.colorConfirm;
  }

  get colorCancel(): ButtonColorEnum {
    return isNullOrUndefined(this.data?.colorCancel) ? ButtonColorEnum.primary : this.data.colorCancel;
  }
}

export interface UpdateVersionDialogData {
  colorConfirm?: ButtonColorEnum;
  colorCancel?: ButtonColorEnum;
}
