/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - shared-oai-set.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {SOLIDIFY_CONSTANTS} from "../../../core-resources/constants";
import {StatePartialEnum} from "../../../core/enums/partial/state-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../core/environments/environment.solidify-defaults";
import {ApiService} from "../../../core/http/api.service";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {QueryParameters} from "../../../core-resources/models/query-parameters/query-parameters.model";
import {NotificationService} from "../../../core/services/notification.service";
import {ResourceStateModel} from "../../../core/stores/abstract/resource/resource-state.model";
import {
  defaultResourceStateInitValue,
  ResourceState,
} from "../../../core/stores/abstract/resource/resource.state";
import {StoreUtil} from "../../../core/utils/stores/store.util";
import {OaiSet} from "../models/oai-set.model";
import {sharedOaiSetActionNameSpace} from "./shared-oai-set.action";

export interface SharedOaiSetStateModel extends ResourceStateModel<OaiSet> {
}

@Injectable()
@State<SharedOaiSetStateModel>({
  name: StatePartialEnum.shared_oaiSet,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(SOLIDIFY_CONSTANTS.DEFAULT_ENUM_VALUE_PAGE_SIZE_OPTION),
  },
})
export class SharedOaiSetState extends ResourceState<SharedOaiSetStateModel, OaiSet> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_apiService, _store, _notificationService, _actions$, _environment, {
      nameSpace: sharedOaiSetActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return this._environment?.apiOaiInfoOaiSets(this._environment);
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }
}
