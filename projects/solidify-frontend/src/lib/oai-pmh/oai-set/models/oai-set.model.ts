/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - oai-set.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {ChangeInfo} from "../../../core-resources/models/dto/change-info.model";

export class OaiSet {
  /**
   * The _links_ list of the _oai-sets_ resource
   */
  _links?: object;
  creation?: ChangeInfo;
  /**
   * The description of the OAI set
   */
  description?: string;
  /**
   * If the OAI set is enable
   */
  enabled?: boolean;
  lastUpdate?: ChangeInfo;
  /**
   * The name of the OAI set
   */
  name?: string;
  /**
   * The query of the OAI set
   */
  query?: string;
  /**
   * The _oai-sets_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]]
   */
  resId?: string;
  /**
   * The specification of the OAI set
   */
  spec?: string;
}
