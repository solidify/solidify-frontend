/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-oai-set.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ModuleWithProviders,
  NgModule,
} from "@angular/core";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {SolidifyFrontendApplicationModule} from "../../application/solidify-frontend-application.module";
import {SolidifyRoutes} from "../../core/models/route/route.model";
import {AdminOaiSetRoutingModule} from "./admin-oai-set-routing.module";
import {AdminOaiSetFormPresentational} from "./components/presentationals/admin-oai-set-form/admin-oai-set-form.presentational";
import {AdminOaiSetCreateRoutable} from "./components/routables/admin-oai-set-create/admin-oai-set-create.routable";
import {AdminOaiSetDetailEditRoutable} from "./components/routables/admin-oai-set-detail-edit/admin-oai-set-detail-edit.routable";
import {AdminOaiSetListRoutable} from "./components/routables/admin-oai-set-list/admin-oai-set-list.routable";
import {AdminOaiSetState} from "./stores/admin-oai-set.state";

const routables = [
  AdminOaiSetCreateRoutable,
  AdminOaiSetDetailEditRoutable,
  AdminOaiSetListRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminOaiSetFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    // SharedModule,
    SolidifyFrontendApplicationModule,
    AdminOaiSetRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminOaiSetState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminOaiSetModule {
  static forChild(routes: SolidifyRoutes): ModuleWithProviders<AdminOaiSetModule> {
    return {
      ngModule: AdminOaiSetModule,
      providers: RouterModule.forChild(routes).providers,
    };
  }
}
