/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-oai-set-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {AbstractFormPresentational} from "../../../../../core/components/presentationals/abstract-form/abstract-form.presentational";
import {DefaultSolidifyEnvironment} from "../../../../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../../../core/injection-tokens/environment.injection-token";
import {BaseFormDefinition} from "../../../../../core/models/forms/base-form-definition.model";
import {SolidifyValidator} from "../../../../../core/utils/validations/validation.util";
import {OaiSet} from "../../../models/oai-set.model";

@Component({
  selector: "solidify-admin-oai-set-form",
  templateUrl: "./admin-oai-set-form.presentational.html",
  styleUrls: ["./admin-oai-set-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminOaiSetFormPresentational extends AbstractFormPresentational<OaiSet> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number;

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super(_changeDetectorRef, _elementRef, _injector);
    this.TIME_BEFORE_DISPLAY_TOOLTIP = _environment.timeBeforeDisplayTooltip;
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.spec]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.query]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.enabled]: [false, [SolidifyValidator]],
    });
  }

  protected _bindFormTo(oaiSets: OaiSet): void {
    this.form = this._fb.group({
      [this.formDefinition.spec]: [oaiSets.spec, [Validators.required, SolidifyValidator]],
      [this.formDefinition.name]: [oaiSets.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [oaiSets.description, [Validators.required, SolidifyValidator]],
      [this.formDefinition.query]: [oaiSets.query, [Validators.required, SolidifyValidator]],
      [this.formDefinition.enabled]: [oaiSets.enabled, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(oaiSets: OaiSet): OaiSet {
    return oaiSets;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  spec: string = "spec";
  name: string = "name";
  description: string = "description";
  query: string = "query";
  enabled: string = "enabled";
}
