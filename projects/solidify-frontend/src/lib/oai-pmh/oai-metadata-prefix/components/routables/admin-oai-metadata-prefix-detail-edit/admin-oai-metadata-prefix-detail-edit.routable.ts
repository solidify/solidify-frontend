/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-oai-metadata-prefix-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {AbstractDetailEditCommonRoutable} from "../../../../../application/components/routables/abstract-detail-edit-common/abstract-detail-edit-common.routable";
import {StatePartialEnum} from "../../../../../core/enums/partial/state-partial.enum";
import {ResourceNameSpace} from "../../../../../core/stores/abstract/resource/resource-namespace.model";
import {MemoizedUtil} from "../../../../../core/utils/stores/memoized.util";
import {OaiMetadataPrefix} from "../../../models/oai-metadata-prefix.model";
import {adminOaiMetadataPrefixActionNameSpace} from "../../../stores/admin-oai-metadata-prefix.action";
import {
  AdminOaiMetadataPrefixState,
  AdminOaiMetadataPrefixStateModel,
} from "../../../stores/admin-oai-metadata-prefix.state";
import {sharedOaiMetadataPrefixActionNameSpace} from "../../../stores/shared-oai-metadata-prefix.action";

@Component({
  selector: "solidify-admin-oai-metadata-prefix-detail-edit-routable",
  templateUrl: "./admin-oai-metadata-prefix-detail-edit.routable.html",
  styleUrls: ["./admin-oai-metadata-prefix-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminOaiMetadataPrefixDetailEditRoutable extends AbstractDetailEditCommonRoutable<OaiMetadataPrefix, AdminOaiMetadataPrefixStateModel> {
  isLoadingWithDependencyObs: Observable<boolean>;
  isReadyToBeDisplayedObs: Observable<boolean>;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedOaiMetadataPrefixActionNameSpace;

  readonly KEY_PARAM_NAME: keyof OaiMetadataPrefix & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StatePartialEnum.admin_oaiMetadataPrefix, _injector, adminOaiMetadataPrefixActionNameSpace, StatePartialEnum.admin);
    this.isLoadingWithDependencyObs = MemoizedUtil.select(this._store, AdminOaiMetadataPrefixState, state => AdminOaiMetadataPrefixState.isLoadingWithDependency(state));
    this.isReadyToBeDisplayedObs = MemoizedUtil.select(this._store, AdminOaiMetadataPrefixState, state => AdminOaiMetadataPrefixState.isReadyToBeDisplayed(state));
  }

  _getSubResourceWithParentId(id: string): void {
  }
}
