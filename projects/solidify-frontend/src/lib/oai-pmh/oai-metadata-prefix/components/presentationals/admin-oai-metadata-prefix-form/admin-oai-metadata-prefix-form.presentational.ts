/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-oai-metadata-prefix-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {RegexUtil} from "../../../../../core-resources/utils/regex.util";
import {AbstractFormPresentational} from "../../../../../core/components/presentationals/abstract-form/abstract-form.presentational";
import {HighlightJsLanguagePartialEnum} from "../../../../../core/enums/highlight-js-format.enum";
import {DefaultSolidifyEnvironment} from "../../../../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../../../core/injection-tokens/environment.injection-token";
import {BaseFormDefinition} from "../../../../../core/models/forms/base-form-definition.model";
import {SolidifyValidator} from "../../../../../core/utils/validations/validation.util";
import {OaiMetadataPrefix} from "../../../models/oai-metadata-prefix.model";

@Component({
  selector: "solidify-admin-oai-metadata-prefix-form",
  templateUrl: "./admin-oai-metadata-prefix-form.presentational.html",
  styleUrls: ["./admin-oai-metadata-prefix-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminOaiMetadataPrefixFormPresentational extends AbstractFormPresentational<OaiMetadataPrefix> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number;

  get highlightJsLanguageEnum(): typeof HighlightJsLanguagePartialEnum {
    return HighlightJsLanguagePartialEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super(_changeDetectorRef, _elementRef, _injector);
    this.TIME_BEFORE_DISPLAY_TOOLTIP = _environment.timeBeforeDisplayTooltip;
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.prefix]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.schemaNamespace]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.schemaUrl]: ["", [Validators.required, SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.enabled]: [false, [SolidifyValidator]],
      [this.formDefinition.reference]: [false, [SolidifyValidator]],
      [this.formDefinition.xmlClass]: ["", [SolidifyValidator]],
      [this.formDefinition.metadataSchema]: ["", [SolidifyValidator]],
      [this.formDefinition.metadataXmlTransformation]: ["", [SolidifyValidator]],
    });
  }

  protected _bindFormTo(oaiMetadataPrefix: OaiMetadataPrefix): void {
    this.form = this._fb.group({
      [this.formDefinition.prefix]: [oaiMetadataPrefix.prefix, [Validators.required, SolidifyValidator]],
      [this.formDefinition.name]: [oaiMetadataPrefix.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [oaiMetadataPrefix.description, [Validators.required, SolidifyValidator]],
      [this.formDefinition.schemaNamespace]: [oaiMetadataPrefix.schemaNamespace, [Validators.required, SolidifyValidator]],
      [this.formDefinition.schemaUrl]: [oaiMetadataPrefix.schemaUrl, [Validators.required, SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.enabled]: [oaiMetadataPrefix.enabled, [SolidifyValidator]],
      [this.formDefinition.reference]: [oaiMetadataPrefix.reference, [SolidifyValidator]],
      [this.formDefinition.xmlClass]: [oaiMetadataPrefix.xmlClass, [SolidifyValidator]],
      [this.formDefinition.metadataSchema]: [oaiMetadataPrefix.metadataSchema, [SolidifyValidator]],
      [this.formDefinition.metadataXmlTransformation]: [oaiMetadataPrefix.metadataXmlTransformation, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(OaiMetadataPrefixes: OaiMetadataPrefix): OaiMetadataPrefix {
    return OaiMetadataPrefixes;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  prefix: string = "prefix";
  name: string = "name";
  description: string = "description";
  schemaNamespace: string = "schemaNamespace";
  schemaUrl: string = "schemaUrl";
  enabled: string = "enabled";
  reference: string = "reference";
  xmlClass: string = "xmlClass";
  metadataSchema: string = "metadataSchema";
  metadataXmlTransformation: string = "metadataXmlTransformation";
}
