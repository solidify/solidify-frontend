/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-oai-metadata-prefix-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {EmptyContainer} from "../../core/components/containers/empty-container/empty.container";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {AppRoutesPartialEnum} from "../../core/enums/partial/app-routes-partial.enum";
import {CanDeactivateGuard} from "../../core/guards/can-deactivate-guard.service";
import {SolidifyRoutes} from "../../core/models/route/route.model";
import {labelSolidifyOaiPmh} from "../label-translate-oai-pmh.model";
import {AdminOaiMetadataPrefixCreateRoutable} from "./components/routables/admin-oai-metadata-prefix-create/admin-oai-metadata-prefix-create.routable";
import {AdminOaiMetadataPrefixDetailEditRoutable} from "./components/routables/admin-oai-metadata-prefix-detail-edit/admin-oai-metadata-prefix-detail-edit.routable";
import {AdminOaiMetadataPrefixListRoutable} from "./components/routables/admin-oai-metadata-prefix-list/admin-oai-metadata-prefix-list.routable";
import {AdminOaiMetadataPrefixState} from "./stores/admin-oai-metadata-prefix.state";

const routes: SolidifyRoutes = [
  {
    path: AppRoutesPartialEnum.root,
    component: AdminOaiMetadataPrefixListRoutable,
    data: {},
  },
  {
    path: AppRoutesPartialEnum.oaiMetadataPrefixCreate,
    component: AdminOaiMetadataPrefixCreateRoutable,
    data: {
      breadcrumb: labelSolidifyOaiPmh.oaiPmhBreadcrumbCreate,
    },
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: AppRoutesPartialEnum.oaiMetadataPrefixDetail + SOLIDIFY_CONSTANTS.SEPARATOR + AppRoutesPartialEnum.paramId,
    component: AdminOaiMetadataPrefixDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminOaiMetadataPrefixState.currentTitle,
    },
    children: [
      {
        path: AppRoutesPartialEnum.oaiMetadataPrefixEdit,
        component: EmptyContainer,
        data: {
          breadcrumb: labelSolidifyOaiPmh.oaiPmhBreadcrumbEdit,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminOaiMetadataPrefixRoutingModule {
}
