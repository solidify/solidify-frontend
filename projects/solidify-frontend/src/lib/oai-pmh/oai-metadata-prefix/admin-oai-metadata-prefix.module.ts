/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-oai-metadata-prefix.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ModuleWithProviders,
  NgModule,
} from "@angular/core";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {HighlightModule} from "ngx-highlightjs";
import {SolidifyFrontendApplicationModule} from "../../application/solidify-frontend-application.module";
import {SolidifyRoutes} from "../../core/models/route/route.model";
import {AdminOaiMetadataPrefixRoutingModule} from "./admin-oai-metadata-prefix-routing.module";
import {AdminOaiMetadataPrefixFormPresentational} from "./components/presentationals/admin-oai-metadata-prefix-form/admin-oai-metadata-prefix-form.presentational";
import {AdminOaiMetadataPrefixCreateRoutable} from "./components/routables/admin-oai-metadata-prefix-create/admin-oai-metadata-prefix-create.routable";
import {AdminOaiMetadataPrefixDetailEditRoutable} from "./components/routables/admin-oai-metadata-prefix-detail-edit/admin-oai-metadata-prefix-detail-edit.routable";
import {AdminOaiMetadataPrefixListRoutable} from "./components/routables/admin-oai-metadata-prefix-list/admin-oai-metadata-prefix-list.routable";
import {AdminOaiMetadataPrefixState} from "./stores/admin-oai-metadata-prefix.state";

const routables = [
  AdminOaiMetadataPrefixCreateRoutable,
  AdminOaiMetadataPrefixDetailEditRoutable,
  AdminOaiMetadataPrefixListRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminOaiMetadataPrefixFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    // SharedModule,
    SolidifyFrontendApplicationModule,
    HighlightModule,
    AdminOaiMetadataPrefixRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminOaiMetadataPrefixState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminOaiMetadataPrefixModule {
  static forChild(routes: SolidifyRoutes): ModuleWithProviders<AdminOaiMetadataPrefixModule> {
    return {
      ngModule: AdminOaiMetadataPrefixModule,
      providers: RouterModule.forChild(routes).providers,
    };
  }
}
