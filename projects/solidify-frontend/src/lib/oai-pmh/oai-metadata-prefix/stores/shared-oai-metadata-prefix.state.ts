/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - shared-oai-metadata-prefix.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {SOLIDIFY_CONSTANTS} from "../../../core-resources/constants";
import {StatePartialEnum} from "../../../core/enums/partial/state-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../core/environments/environment.solidify-defaults";
import {ApiService} from "../../../core/http/api.service";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {QueryParameters} from "../../../core-resources/models/query-parameters/query-parameters.model";
import {NotificationService} from "../../../core/services/notification.service";
import {ResourceStateModel} from "../../../core/stores/abstract/resource/resource-state.model";
import {
  defaultResourceStateInitValue,
  ResourceState,
} from "../../../core/stores/abstract/resource/resource.state";
import {StoreUtil} from "../../../core/utils/stores/store.util";
import {OaiMetadataPrefix} from "../models/oai-metadata-prefix.model";
import {sharedOaiMetadataPrefixActionNameSpace} from "./shared-oai-metadata-prefix.action";

export interface SharedOaiMetadataPrefixStateModel extends ResourceStateModel<OaiMetadataPrefix> {
}

@Injectable()
@State<SharedOaiMetadataPrefixStateModel>({
  name: StatePartialEnum.shared_oaiMetadataPrefix,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(SOLIDIFY_CONSTANTS.DEFAULT_ENUM_VALUE_PAGE_SIZE_OPTION),
  },
})
export class SharedOaiMetadataPrefixState extends ResourceState<SharedOaiMetadataPrefixStateModel, OaiMetadataPrefix> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_apiService, _store, _notificationService, _actions$, _environment, {
      nameSpace: sharedOaiMetadataPrefixActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return this._environment?.apiOaiInfoOaiMetadataPrefixes(this._environment);
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }
}
