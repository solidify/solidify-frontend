/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-oai-metadata-prefix.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {StatePartialEnum} from "../../../core/enums/partial/state-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../core/environments/environment.solidify-defaults";
import {ApiService} from "../../../core/http/api.service";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../../core/injection-tokens/label-to-translate.injection-token";
import {NotificationService} from "../../../core/services/notification.service";
import {ResourceStateModel} from "../../../core/stores/abstract/resource/resource-state.model";
import {
  defaultResourceStateInitValue,
  ResourceState,
} from "../../../core/stores/abstract/resource/resource.state";
import {isNullOrUndefined} from "../../../core-resources/tools/is/is.tool";
import {StoreUtil} from "../../../core/utils/stores/store.util";
import {LabelTranslateInterface} from "../../../label-translate-interface.model";
import {OaiMetadataPrefix} from "../models/oai-metadata-prefix.model";
import {adminOaiMetadataPrefixActionNameSpace} from "./admin-oai-metadata-prefix.action";

export interface AdminOaiMetadataPrefixStateModel extends ResourceStateModel<OaiMetadataPrefix> {
}

@Injectable()
@State<AdminOaiMetadataPrefixStateModel>({
  name: StatePartialEnum.admin_oaiMetadataPrefix,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class AdminOaiMetadataPrefixState extends ResourceState<AdminOaiMetadataPrefixStateModel, OaiMetadataPrefix> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslate: LabelTranslateInterface,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_apiService, _store, _notificationService, _actions$, _environment, {
      nameSpace: adminOaiMetadataPrefixActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: _environment.oaiMetadataPrefixRouteRedirectUrlAfterSuccessCreateAction,
      routeRedirectUrlAfterSuccessUpdateAction: _environment.oaiMetadataPrefixRouteRedirectUrlAfterSuccessUpdateAction,
      routeRedirectUrlAfterSuccessDeleteAction: _environment.oaiMetadataPrefixRouteRedirectUrlAfterSuccessDeleteAction,
      notificationResourceCreateSuccessTextToTranslate: _labelTranslate.oaiPmhOaiSetNotificationResourceCreateSuccess,
      notificationResourceDeleteSuccessTextToTranslate: _labelTranslate.oaiPmhOaiSetNotificationResourceDeleteSuccess,
      notificationResourceUpdateSuccessTextToTranslate: _labelTranslate.oaiPmhOaiSetNotificationResourceUpdateSuccess,
    });
  }

  protected get _urlResource(): string {
    return this._environment?.apiOaiInfoOaiMetadataPrefixes(this._environment);
  }

  @Selector()
  static isLoading(state: AdminOaiMetadataPrefixStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminOaiMetadataPrefixStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminOaiMetadataPrefixStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminOaiMetadataPrefixStateModel): boolean {
    return true;
  }

  @Selector()
  static currentTitle(state: AdminOaiMetadataPrefixStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }
}
