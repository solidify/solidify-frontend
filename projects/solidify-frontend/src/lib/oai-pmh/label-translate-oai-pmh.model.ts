/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - label-translate-oai-pmh.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MARK_AS_TRANSLATABLE} from "../core-resources/utils/translate.util";

export interface LabelSolidifyOaiPmh {
  oaiPmhOaiSetNotificationResourceCreateSuccess?: string;
  oaiPmhOaiSetNotificationResourceDeleteSuccess?: string;
  oaiPmhOaiSetNotificationResourceUpdateSuccess?: string;
  oaiPmhOaiMetadataPrefixNotificationResourceCreateSuccess?: string;
  oaiPmhOaiMetadataPrefixNotificationResourceDeleteSuccess?: string;
  oaiPmhOaiMetadataPrefixNotificationResourceUpdateSuccess?: string;
  oaiPmhOaiMetadataPrefixBreadcrumb?: string;
  oaiPmhOaiMetadataPrefixTileTitle?: string;
  oaiPmhOaiMetadataPrefixTileSubtitle?: string;
  oaiPmhOaiSetBreadcrumb?: string;
  oaiPmhOaiSetTileTitle?: string;
  oaiPmhOaiSetTileSubtitle?: string;
  oaiPmhBreadcrumbCreate?: string;
  oaiPmhBreadcrumbEdit?: string;
  oaiPmhOaiMetadataPrefixListPrefix?: string;
  oaiPmhOaiMetadataPrefixListName?: string;
  oaiPmhOaiMetadataPrefixListDescription?: string;
  oaiPmhOaiMetadataPrefixListEnabled?: string;
  oaiPmhOaiMetadataPrefixListReference?: string;
  oaiPmhOaiMetadataPrefixListCreated?: string;
  oaiPmhOaiMetadataPrefixListUpdated?: string;
  oaiPmhOaiSetListName?: string;
  oaiPmhOaiSetListDescription?: string;
  oaiPmhOaiSetListSpec?: string;
  oaiPmhOaiSetListEnabled?: string;
  oaiPmhOaiSetListCreated?: string;
  oaiPmhOaiSetListUpdated?: string;
  oaiPmhOaiSetDialogDeleteMessage?: string;
  oaiPmhOaiMetadataPrefixDialogDeleteMessage?: string;
  oaiPmhOaiSetFormQuery?: string;
  oaiPmhOaiSetFormEnabled?: string;
  oaiPmhOaiSetFormSpec?: string;
  oaiPmhOaiSetFormDescription?: string;
  oaiPmhOaiSetFormName?: string;
  oaiPmhOaiMetadataPrefixFormPrefix?: string;
  oaiPmhOaiMetadataPrefixFormName?: string;
  oaiPmhOaiMetadataPrefixFormDescription?: string;
  oaiPmhOaiMetadataPrefixFormSchemaNamespace?: string;
  oaiPmhOaiMetadataPrefixFormMetadataSchema?: string;
  oaiPmhOaiMetadataPrefixFormMetadataXmlTransformation?: string;
  oaiPmhOaiMetadataPrefixFormEnabled?: string;
  oaiPmhOaiMetadataPrefixFormReference?: string;
  oaiPmhOaiMetadataPrefixFormXmlClass?: string;
  oaiPmhOaiSetFormTooltipQuery?: string;
  oaiPmhOaiSetFormTooltipSpec?: string;
  oaiPmhOaiMetadataPrefixFormTooltipSchemaNamespace?: string;
}

export const labelSolidifyOaiPmh: LabelSolidifyOaiPmh = {
  oaiPmhOaiSetNotificationResourceCreateSuccess: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.notification.resourceCreateSuccess"),
  oaiPmhOaiSetNotificationResourceDeleteSuccess: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.notification.resourceDeleteSuccess"),
  oaiPmhOaiSetNotificationResourceUpdateSuccess: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.notification.resourceUpdateSuccess"),
  oaiPmhOaiMetadataPrefixNotificationResourceCreateSuccess: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.notification.resourceCreateSuccess"),
  oaiPmhOaiMetadataPrefixNotificationResourceDeleteSuccess: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.notification.resourceDeleteSuccess"),
  oaiPmhOaiMetadataPrefixNotificationResourceUpdateSuccess: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.notification.resourceUpdateSuccess"),
  oaiPmhOaiMetadataPrefixBreadcrumb: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.breadcrumb.root"),
  oaiPmhOaiMetadataPrefixTileTitle: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.tile.title"),
  oaiPmhOaiMetadataPrefixTileSubtitle: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.tile.subtitle"),
  oaiPmhOaiSetBreadcrumb: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.breadcrumb.root"),
  oaiPmhOaiSetTileTitle: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.tile.title"),
  oaiPmhOaiSetTileSubtitle: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.tile.subtitle"),
  oaiPmhBreadcrumbCreate: MARK_AS_TRANSLATABLE("solidify.oaiPmh.breadcrumb.create"),
  oaiPmhBreadcrumbEdit: MARK_AS_TRANSLATABLE("solidify.oaiPmh.breadcrumb.edit"),
  oaiPmhOaiMetadataPrefixListPrefix: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.list.prefix"),
  oaiPmhOaiMetadataPrefixListName: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.list.name"),
  oaiPmhOaiMetadataPrefixListDescription: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.list.description"),
  oaiPmhOaiMetadataPrefixListEnabled: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.list.enabled"),
  oaiPmhOaiMetadataPrefixListReference: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.list.reference"),
  oaiPmhOaiMetadataPrefixListCreated: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.list.created"),
  oaiPmhOaiMetadataPrefixListUpdated: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.list.updated"),
  oaiPmhOaiSetListName: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.list.name"),
  oaiPmhOaiSetListDescription: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.list.description"),
  oaiPmhOaiSetListSpec: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.list.spec"),
  oaiPmhOaiSetListEnabled: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.list.enabled"),
  oaiPmhOaiSetListCreated: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.list.created"),
  oaiPmhOaiSetListUpdated: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.list.updated"),
  oaiPmhOaiSetDialogDeleteMessage: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.dialog.delete.message"),
  oaiPmhOaiMetadataPrefixDialogDeleteMessage: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.dialog.delete.message"),
  oaiPmhOaiSetFormQuery: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.form.query"),
  oaiPmhOaiSetFormEnabled: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.form.enabled"),
  oaiPmhOaiSetFormSpec: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.form.spec"),
  oaiPmhOaiSetFormDescription: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.form.description"),
  oaiPmhOaiSetFormName: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.form.name"),
  oaiPmhOaiMetadataPrefixFormPrefix: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.form.prefix"),
  oaiPmhOaiMetadataPrefixFormName: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.form.name"),
  oaiPmhOaiMetadataPrefixFormDescription: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.form.description"),
  oaiPmhOaiMetadataPrefixFormSchemaNamespace: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.form.schemaNamespace"),
  oaiPmhOaiMetadataPrefixFormMetadataSchema: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.form.metadataSchema"),
  oaiPmhOaiMetadataPrefixFormMetadataXmlTransformation: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.form.metadataXmlTransformation"),
  oaiPmhOaiMetadataPrefixFormEnabled: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.form.enabled"),
  oaiPmhOaiMetadataPrefixFormReference: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.form.reference"),
  oaiPmhOaiMetadataPrefixFormXmlClass: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.form.xmlClass"),
  oaiPmhOaiSetFormTooltipQuery: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.form.tooltip.query"),
  oaiPmhOaiSetFormTooltipSpec: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiSet.form.tooltip.spec"),
  oaiPmhOaiMetadataPrefixFormTooltipSchemaNamespace: MARK_AS_TRANSLATABLE("solidify.oaiPmh.oaiMetadataPrefix.form.tooltip.schemaNamespace"),
};
