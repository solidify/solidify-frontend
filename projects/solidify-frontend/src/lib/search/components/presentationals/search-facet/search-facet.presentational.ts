/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - search-facet.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  Output,
} from "@angular/core";
import {MatCheckboxChange} from "@angular/material/checkbox";
import {MatDialog} from "@angular/material/dialog";
import {Store} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {AbstractCoreInternalComponent} from "../../../../core/components/abstract-core-internal/abstract-core-internal.component";
import {
  ConfirmDialog,
  ConfirmDialogData,
} from "../../../../core/components/dialogs/confirm/confirm.dialog";
import {ButtonColorEnum} from "../../../../core/enums/button-color.enum";
import {FacetNamePartialEnum} from "../../../../core/enums/partial/facet-name-partial.enum";
import {Facet} from "../../../../core/models/dto/facet.model";
import {BaseFormDefinition} from "../../../../core/models/forms/base-form-definition.model";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {MappingObject} from "../../../../core-resources/types/mapping-type.type";
import {DialogUtil} from "../../../../core/utils/dialog.util";
import {MappingObjectUtil} from "../../../../core-resources/utils/mapping-object.util";
import {ObservableUtil} from "../../../../core/utils/observable.util";
import {MemoizedUtil} from "../../../../core/utils/stores/memoized.util";
import {FacetHelper} from "../../../helpers/facet.helper";
import {FacetProperty} from "../../../models/facet-property.model";
import {TranslateFacetNamePipe} from "../../../pipes/translate-facet-name.pipe";

@Component({
  selector: "solidify-search-facet",
  templateUrl: "./search-facet.presentational.html",
  styleUrls: ["./search-facet.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchFacetPresentational extends AbstractCoreInternalComponent {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  numberActiveCriteria: number = 0;
  facetsSelected: MappingObject<ExtendEnum<FacetNamePartialEnum>, string[]> = {} as MappingObject<ExtendEnum<FacetNamePartialEnum>, string[]>;
  openFacets: string[] = [];
  hide: boolean = true;

  @Input()
  searchFacetProperties: FacetProperty[];

  @Input()
  set facetsSelectedInit(value: MappingObject<ExtendEnum<FacetNamePartialEnum>, string[]>) {

    if (this._isEnabled === true) {
      return;
    }
    this.facetsSelected = MappingObjectUtil.copy(value);
    this._isEnabled = true;

    this.numberActiveCriteria = 0;
    MappingObjectUtil.forEach(this.facetsSelected, values => this.numberActiveCriteria += values.length);
  }

  private _isEnabled: boolean;

  private readonly _facetChangeBS: BehaviorSubject<MappingObject<ExtendEnum<FacetNamePartialEnum>, string[]> | undefined> = new BehaviorSubject<MappingObject<ExtendEnum<FacetNamePartialEnum>, string[]> | undefined>(undefined);
  @Output("facetChange")
  readonly facetObs: Observable<MappingObject<ExtendEnum<FacetNamePartialEnum>, string[]> | undefined> = ObservableUtil.asObservable(this._facetChangeBS);

  get mapFacetsEnabledKey(): string[] | IterableIterator<string> {
    return MappingObjectUtil.keys(this.facetsSelected);
  }

  isChecked(key: string, value: string): boolean {
    if (!MappingObjectUtil.has(this.facetsSelected, key)) {
      return false;
    }
    const list = MappingObjectUtil.get(this.facetsSelected, key);
    return list.findIndex(v => this._isSameValue(v, value)) !== -1;
  }

  getListValue(key: string): string[] {
    if (!MappingObjectUtil.has(this.facetsSelected, key)) {
      return undefined;
    }
    return MappingObjectUtil.get(this.facetsSelected, key);
  }

  private _listFacets: Facet[];

  @Input()
  set listFacets(value: Facet[]) {
    this._listFacets = value;
  }

  get listFacets(): Facet[] {
    return this._listFacets;
  }

  constructor(protected readonly _injector: Injector,
              private readonly _store: Store,
              private readonly _dialog: MatDialog) {
    super(_injector);
  }

  getFacetProperty(facet: Facet): FacetProperty {
    return TranslateFacetNamePipe.getFacetProperty(this.environment, facet.name);
  }

  clearFilters(): void {
    MappingObjectUtil.clear(this.facetsSelected);
    this.numberActiveCriteria = 0;
    this._dispatchChangeSelectionChange();
  }

  removeCriteria(key: string, value: string): void {
    if (MappingObjectUtil.has(this.facetsSelected, key)) {
      const listFacetsSelected = [...MappingObjectUtil.get(this.facetsSelected, key)];
      this.numberActiveCriteria--;

      const indexOf = listFacetsSelected.indexOf(value);
      if (indexOf !== -1) {
        listFacetsSelected.splice(indexOf, 1);
      }

      if (listFacetsSelected.length === 0) {
        MappingObjectUtil.delete(this.facetsSelected, key);
      } else {
        MappingObjectUtil.set(this.facetsSelected, key, listFacetsSelected);
      }
    }

    this._dispatchChangeSelectionChange();
  }

  changeCheckbox($event: MatCheckboxChange, key: string, value: string): void {
    const isActive = $event.checked;
    this._toggleCheckbox(isActive, key, value);
  }

  private _toggleCheckbox(isActive: boolean, key: string, value: string): void {
    if (isActive) {
      this.numberActiveCriteria++;
    } else {
      this.numberActiveCriteria--;
    }

    let listFacetsSelected = [];
    if (MappingObjectUtil.has(this.facetsSelected, key)) {
      listFacetsSelected = [...MappingObjectUtil.get(this.facetsSelected, key)];
    }

    const indexOf = listFacetsSelected.findIndex(v => this._isSameValue(v, value));
    if (isActive && indexOf === -1) {
      listFacetsSelected.push(value);
    } else if (!isActive && indexOf !== -1) {
      listFacetsSelected.splice(indexOf, 1);
    }

    if (listFacetsSelected.length === 0) {
      MappingObjectUtil.delete(this.facetsSelected, key);
    } else {
      MappingObjectUtil.set(this.facetsSelected, key, listFacetsSelected);
    }
    this._dispatchChangeSelectionChange();
  }

  private _dispatchChangeSelectionChange(): void {
    this._facetChangeBS.next(this.facetsSelected);
  }

  isFacetValueVisible(facet: Facet, i: number): boolean {
    if (this.openFacets.indexOf(facet.name) !== -1) {
      return true;
    } else {
      const facetProperty = this._getSearchFacetProperty(facet.name);
      if (isNullOrUndefined(facetProperty?.defaultVisibleValues)) {
        return true;
      } else {
        return i < facetProperty.defaultVisibleValues;
      }
    }
  }

  private _facetHasMoreValuesThanDefaultVisible(facet: Facet): boolean {
    const facetProperty = this._getSearchFacetProperty(facet.name);
    if (isNullOrUndefined(facetProperty?.defaultVisibleValues)) {
      return false;
    } else {
      return facet.values.length > facetProperty.defaultVisibleValues;
    }
  }

  displayShowAllFacetValue(facet: Facet): boolean {
    return (this._facetHasMoreValuesThanDefaultVisible(facet) && this.openFacets.indexOf(facet.name) === -1);
  }

  displayShowLessFacetValue(facet: Facet): boolean {
    return (this._facetHasMoreValuesThanDefaultVisible(facet) && this.openFacets.indexOf(facet.name) !== -1);
  }

  private _getSearchFacetProperty(name: string): FacetProperty | undefined {
    if (isNullOrUndefined(this.searchFacetProperties)) {
      return undefined;
    }
    return this.searchFacetProperties.find(facetProperty => facetProperty.name === name);
  }

  showAllFacetValues(facet: Facet): void {
    this.openFacets.push(facet.name);
  }

  showLessFacetValues(facet: Facet): void {
    const index = this.openFacets.indexOf(facet.name);
    if (index > -1) {
      this.openFacets.splice(index, 1);
    }
  }

  toggleDisplayPanel(): void {
    this.hide = !this.hide;
  }

  hidePanel(): void {
    this.hide = true;
  }

  isExcluded(value: string): boolean {
    return FacetHelper.isExcludedValue(value);
  }

  toggleExclude(name: string, value: string): void {
    if (!this.isChecked(name, value)) {
      this._toggleCheckbox(true, name, value);
    }

    const listValues = [...MappingObjectUtil.get(this.facetsSelected, name)];
    const indexValueAlreadyExisting = listValues.findIndex(v => this._isSameValue(v, value));
    let valueAlreadyExisting = listValues[indexValueAlreadyExisting];
    if (this.isExcluded(valueAlreadyExisting)) {
      valueAlreadyExisting = value;
    } else {
      valueAlreadyExisting = FacetHelper.createExcludedValue(value);
    }
    listValues[indexValueAlreadyExisting] = valueAlreadyExisting;
    MappingObjectUtil.set(this.facetsSelected, name, listValues);
    this._dispatchChangeSelectionChange();
  }

  private _isSameValue(valuePotentiallyExcluded: string, value: string): boolean {
    return valuePotentiallyExcluded === value || (valuePotentiallyExcluded === FacetHelper.createExcludedValue(value));
  }

  openFacetsHelper(facet: Facet): void {
    const currentLanguage = MemoizedUtil.selectSnapshot(this._store, this.environment.appState, state => state.appLanguage);
    const titleTranslated = TranslateFacetNamePipe.getTitleTranslated(this.environment, currentLanguage, facet.name);
    const descriptionTranslated = TranslateFacetNamePipe.getDescriptionTranslated(this.environment, currentLanguage, facet.name);
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: titleTranslated,
      messageToTranslate: descriptionTranslated,
      cancelButtonToTranslate: this.labelTranslateInterface.coreClose,
      colorCancel: ButtonColorEnum.primary,
      innerHtmlOnMessage: true,
    } as ConfirmDialogData, {
      minWidth: "400px",
      maxWidth: "90vw",
    }));
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  nameValue: string = "nameValue";
  name: string = "name";
  value: string = "value";
  isActive: string = "isActive";
}
