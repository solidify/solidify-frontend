/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - search.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {BaseStateModel} from "../../core/models/stores/base-state.model";
import {FacetProperty} from "../models/facet-property.model";
import {Injectable} from "@angular/core";
import {Actions, State, Store} from "@ngxs/store";
import {StatePartialEnum} from "../../core/enums/partial/state-partial.enum";
import {defaultBaseStateInitValue} from "../../core/stores/abstract/base/base.state";
import {BasicState} from "../../core/stores/abstract/base/basic.state";
import {ApiService} from "../../core/http/api.service";
import {NotificationService} from "../../core/services/notification.service";

export interface SolidifySearchStateModel extends BaseStateModel {
  searchFacets: FacetProperty[] | any;
}

@Injectable()
@State<SolidifySearchStateModel>({
  name: StatePartialEnum.search_facet,
  defaults: {
    ...defaultBaseStateInitValue(),
    searchFacets: undefined,
  },
})
export class SolidifySearchState extends BasicState<SolidifySearchStateModel> {
  constructor(private readonly _apiService: ApiService,
              private readonly _store: Store,
              private readonly _notificationService: NotificationService,
              private readonly _actions$: Actions) {
    super();
  }
}
