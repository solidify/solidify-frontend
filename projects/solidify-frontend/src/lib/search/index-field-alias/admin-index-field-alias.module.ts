/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-index-field-alias.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ModuleWithProviders,
  NgModule,
} from "@angular/core";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {SolidifyFrontendApplicationModule} from "../../application/solidify-frontend-application.module";
import {SolidifyRoutes} from "../../core/models/route/route.model";
import {AdminOaiSetModule} from "../../oai-pmh/oai-set/admin-oai-set.module";
import {AdminIndexFieldAliasRoutingModule} from "./admin-index-field-alias-routing.module";
import {AdminIndexFieldAliasFormPresentational} from "./components/presentationals/admin-index-field-alias-form/admin-index-field-alias-form.presentational";
import {AdminIndexFieldAliasCreateRoutable} from "./components/routables/admin-index-field-alias-create/admin-index-field-alias-create.routable";
import {AdminIndexFieldAliasDetailEditRoutable} from "./components/routables/admin-index-field-alias-detail-edit/admin-index-field-alias-detail-edit.routable";
import {AdminIndexFieldAliasListRoutable} from "./components/routables/admin-index-field-alias-list/admin-index-field-alias-list.routable";
import {AdminIndexFieldAliasState} from "./stores/admin-index-field-alias.state";

const routables = [
  AdminIndexFieldAliasListRoutable,
  AdminIndexFieldAliasDetailEditRoutable,
  AdminIndexFieldAliasCreateRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminIndexFieldAliasFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    // SharedModule,
    SolidifyFrontendApplicationModule,
    AdminIndexFieldAliasRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminIndexFieldAliasState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminIndexFieldAliasModule {
  static forChild(routes: SolidifyRoutes): ModuleWithProviders<AdminOaiSetModule> {
    return {
      ngModule: AdminOaiSetModule,
      providers: RouterModule.forChild(routes).providers,
    };
  }
}
