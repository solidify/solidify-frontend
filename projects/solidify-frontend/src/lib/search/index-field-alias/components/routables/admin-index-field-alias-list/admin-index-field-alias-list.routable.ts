/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-index-field-alias-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {AbstractListRoutable} from "../../../../../application/components/routables/abstract-list/abstract-list.routable";
import {DataTableFieldTypeEnum} from "../../../../../core/enums/datatable/data-table-field-type.enum";
import {OrderEnum} from "../../../../../core-resources/enums/order.enum";
import {StatePartialEnum} from "../../../../../core/enums/partial/state-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../../../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../../../../core/injection-tokens/label-to-translate.injection-token";
import {RouterExtensionService} from "../../../../../core/services/router-extension.service";
import {
  isFalse,
  isNullOrUndefined,
} from "../../../../../core-resources/tools/is/is.tool";
import {LabelTranslateInterface} from "../../../../../label-translate-interface.model";
import {IndexFieldAlias} from "../../../models/index-field-alias.model";
import {adminIndexFieldAliasActionNameSpace} from "../../../stores/admin-index-field-alias.action";
import {AdminIndexFieldAliasStateModel} from "../../../stores/admin-index-field-alias.state";

@Component({
  selector: "solidify-admin-index-field-alias-list-routable",
  templateUrl: "../../../../../application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-index-field-alias-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminIndexFieldAliasListRoutable extends AbstractListRoutable<IndexFieldAlias, AdminIndexFieldAliasStateModel> {
  readonly KEY_CREATE_BUTTON: string;
  readonly KEY_BACK_BUTTON: string | undefined;
  readonly KEY_PARAM_NAME: keyof IndexFieldAlias & string = "alias";

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslate: LabelTranslateInterface,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StatePartialEnum.admin_indexFieldAlias, adminIndexFieldAliasActionNameSpace, _injector, {}, StatePartialEnum.admin);
    this.KEY_CREATE_BUTTON = this._labelTranslate.applicationCreate;
    this.KEY_BACK_BUTTON = this._labelTranslate.applicationBackToAdmin;
  }

  conditionDisplayEditButton(model: IndexFieldAlias | undefined): boolean {
    return true;
  }

  conditionDisplayDeleteButton(model: IndexFieldAlias | undefined): boolean {
    return isNullOrUndefined(model.system) || isFalse(model.system);
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "indexName",
        header: this._labelTranslate.searchIndexFieldAliasListIndexName,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        minWidth: "180px",
      },
      {
        field: "alias",
        header: this._labelTranslate.searchIndexFieldAliasListAlias,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "field",
        header: this._labelTranslate.searchIndexFieldAliasListField,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        maxWidth: "450px",
      },
      {
        field: "facet",
        header: this._labelTranslate.searchIndexFieldAliasListFacet,
        type: DataTableFieldTypeEnum.boolean,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        minWidth: "75px",
      },
      {
        field: "system",
        header: this._labelTranslate.searchIndexFieldAliasListSystem,
        type: DataTableFieldTypeEnum.boolean,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        minWidth: "75px",
      },
      {
        field: "facetOrder",
        header: this._labelTranslate.searchIndexFieldAliasListSort,
        type: DataTableFieldTypeEnum.number,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        maxWidth: "80px",
      },
      {
        field: "creation.when" as any,
        header: this._labelTranslate.searchIndexFieldAliasListCreated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
      {
        field: "lastUpdate.when" as any,
        header: this._labelTranslate.searchIndexFieldAliasListUpdated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
    ];
  }
}
