/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-index-field-alias-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {AbstractCreateRoutable} from "../../../../../application/components/routables/abstract-create/abstract-create.routable";
import {SharedLanguageState} from "../../../../../application/stores/language/shared-language.state";
import {StatePartialEnum} from "../../../../../core/enums/partial/state-partial.enum";
import {ResourceNameSpace} from "../../../../../core/stores/abstract/resource/resource-namespace.model";
import {MemoizedUtil} from "../../../../../core/utils/stores/memoized.util";
import {IndexFieldAlias} from "../../../models/index-field-alias.model";
import {Language} from "../../../models/language.model";
import {adminIndexFieldAliasActionNameSpace} from "../../../stores/admin-index-field-alias.action";
import {
  AdminIndexFieldAliasState,
  AdminIndexFieldAliasStateModel,
} from "../../../stores/admin-index-field-alias.state";
import {sharedIndexFieldAliasActionNameSpace} from "../../../stores/shared-index-field-alias.action";

@Component({
  selector: "solidify-admin-index-field-alias-create-routable",
  templateUrl: "./admin-index-field-alias-create.routable.html",
  styleUrls: ["./admin-index-field-alias-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminIndexFieldAliasCreateRoutable extends AbstractCreateRoutable<IndexFieldAlias, AdminIndexFieldAliasStateModel> {
  isLoadingWithDependencyObs: Observable<boolean>;
  isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;
  languagesObs: Observable<Language[]>;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedIndexFieldAliasActionNameSpace;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_store, _actions$, _changeDetector, StatePartialEnum.admin_indexFieldAlias, _injector, adminIndexFieldAliasActionNameSpace, StatePartialEnum.admin);
    this.languagesObs = MemoizedUtil.list(this._store, SharedLanguageState);
    this.isLoadingWithDependencyObs = MemoizedUtil.select(this._store, AdminIndexFieldAliasState, state => AdminIndexFieldAliasState.isLoadingWithDependency(state));
    this.isReadyToBeDisplayedInCreateModeObs = MemoizedUtil.select(this._store, AdminIndexFieldAliasState, state => AdminIndexFieldAliasState.isReadyToBeDisplayedInCreateMode(state));

  }

}
