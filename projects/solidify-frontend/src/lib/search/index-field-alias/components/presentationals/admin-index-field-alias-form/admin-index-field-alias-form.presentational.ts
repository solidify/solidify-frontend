/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-index-field-alias-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {Store} from "@ngxs/store";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {SharedLanguageAction} from "../../../../../application/stores/language/shared-language.action";
import {AbstractFormPresentational} from "../../../../../core/components/presentationals/abstract-form/abstract-form.presentational";
import {IconNamePartialEnum} from "../../../../../core/enums/partial/icon-name-partial.enum";
import {BaseFormDefinition} from "../../../../../core/models/forms/base-form-definition.model";
import {Label} from "../../../../../core/models/label.model";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "../../../../../core-resources/tools/is/is.tool";
import {SolidifyValidator} from "../../../../../core/utils/validations/validation.util";
import {IndexFieldAlias} from "../../../models/index-field-alias.model";
import {Language} from "../../../models/language.model";

@Component({
  selector: "solidify-admin-index-field-alias-form",
  templateUrl: "./admin-index-field-alias-form.presentational.html",
  styleUrls: ["./admin-index-field-alias-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminIndexFieldAliasFormPresentational extends AbstractFormPresentational<IndexFieldAlias> implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  private _languages: Language[];

  @Input()
  set languages(value: Language[]) {
    this._languages = value;
    if (isNullOrUndefined(this.languages)) {
      return;
    }
    this.languages.forEach(language => {
      if (isNullOrUndefined(this.model) || isNullOrUndefined(this.model.labels)) {
        this._addControl(language.resId);
        return;
      }
      const existingValue = this.model.labels.find(label => label.language.resId === language.resId);
      if (isNullOrUndefined(existingValue)) {
        this._addControl(language.resId);
        return;
      }
      this._addControl(language.resId, existingValue.text);
    });
  }

  get languages(): Language[] {
    return this._languages;
  }

  get iconNameEnum(): typeof IconNamePartialEnum {
    return IconNamePartialEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              protected readonly _store: Store) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.indexName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.alias]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.field]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.facet]: [false, [SolidifyValidator]],
      [this.formDefinition.system]: [false, [SolidifyValidator]],
      [this.formDefinition.facetMinCount]: [1, []],
      [this.formDefinition.facetLimit]: ["", []],
      [this.formDefinition.facetOrder]: ["", []],
      [this.formDefinition.facetDefaultVisibleValues]: ["", []],
    });
  }

  protected _bindFormTo(indexFieldAlias: IndexFieldAlias): void {
    this.form = this._fb.group({
      [this.formDefinition.indexName]: [indexFieldAlias.indexName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.alias]: [indexFieldAlias.alias, [Validators.required, SolidifyValidator]],
      [this.formDefinition.field]: [indexFieldAlias.field, [Validators.required, SolidifyValidator]],
      [this.formDefinition.facet]: [indexFieldAlias.facet, [SolidifyValidator]],
      [this.formDefinition.system]: [indexFieldAlias.system, [SolidifyValidator]],
      [this.formDefinition.facetMinCount]: [indexFieldAlias.facetMinCount, []],
      [this.formDefinition.facetLimit]: [indexFieldAlias.facetLimit, []],
      [this.formDefinition.facetOrder]: [indexFieldAlias.facetOrder, []],
      [this.formDefinition.facetDefaultVisibleValues]: [indexFieldAlias.facetDefaultVisibleValues, []],
    });
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._store.dispatch(new SharedLanguageAction.GetAll());
    this._addValidators();
  }

  private _addValidators(): void {
    const facetFormControl = this.form.get(this.formDefinition.facet);
    this.subscribe(facetFormControl.valueChanges.pipe(
      distinctUntilChanged(),
      tap((isFacet: boolean) => {
        this._setValidator(isFacet);
      }),
    ));
    this._setValidator(facetFormControl.value);
  }

  private _setValidator(isFacet: boolean): void {
    const facetMinCountFormControl = this.form.get(this.formDefinition.facetMinCount);
    const facetLimitFormControl = this.form.get(this.formDefinition.facetLimit);
    const facetOrderControl = this.form.get(this.formDefinition.facetOrder);

    if (isFacet) {
      facetMinCountFormControl.setValidators([Validators.required, Validators.min(1), SolidifyValidator]);
      facetLimitFormControl.setValidators([Validators.required, Validators.min(1), SolidifyValidator]);
      facetOrderControl.setValidators([Validators.required, Validators.min(0), SolidifyValidator]);
    } else {
      facetMinCountFormControl.setValidators([]);
      facetMinCountFormControl.setValue(undefined);
      facetLimitFormControl.setValidators([]);
      facetLimitFormControl.setValue(undefined);
      facetOrderControl.setValidators([]);
      facetOrderControl.setValue(undefined);
    }
    this.form.updateValueAndValidity();
    this._changeDetectorRef.detectChanges();
  }

  protected _treatmentBeforeSubmit(indexFieldAlias: IndexFieldAlias): IndexFieldAlias {

    const newLabels: Label[] = [];
    this.languages.forEach(language => {
      if (isNotNullNorUndefined(indexFieldAlias[language.resId])) {
        newLabels.push({
          text: indexFieldAlias[language.resId],
          language: language,
        });
      }
    });
    indexFieldAlias.labels = newLabels;

    return indexFieldAlias;
  }

  private _addControl(languageResId: string, value: string | undefined = undefined): void {
    if (!isNullOrUndefined(this.form)) {
      this.form.addControl(languageResId, this._fb.control(value));
    }
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  indexName: string = "indexName";
  alias: string = "alias";
  field: string = "field";
  facet: string = "facet";
  system: string = "system";
  facetMinCount: string = "facetMinCount";
  facetLimit: string = "facetLimit";
  facetOrder: string = "facetOrder";
  facetDefaultVisibleValues: string = "facetDefaultVisibleValues";
}
