/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-index-field-alias.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {SystemPropertyPartial} from "../../../application/models/system-property.model";
import {AppSystemPropertyAction} from "../../../application/stores/system-property/app-system-property.action";
import {AppSystemPropertyStateModel} from "../../../application/stores/system-property/app-system-property.state";
import {StatePartialEnum} from "../../../core/enums/partial/state-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../core/environments/environment.solidify-defaults";
import {ApiService} from "../../../core/http/api.service";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../../core/injection-tokens/label-to-translate.injection-token";
import {SolidifyStateContext} from "../../../core/models/stores/state-context.model";
import {NotificationService} from "../../../core/services/notification.service";
import {ResourceStateModel} from "../../../core/stores/abstract/resource/resource-state.model";
import {
  defaultResourceStateInitValue,
  ResourceState,
} from "../../../core/stores/abstract/resource/resource.state";
import {isNullOrUndefined} from "../../../core-resources/tools/is/is.tool";
import {StoreUtil} from "../../../core/utils/stores/store.util";
import {LabelTranslateInterface} from "../../../label-translate-interface.model";
import {IndexFieldAlias} from "../models/index-field-alias.model";
import {
  AdminIndexFieldAliasAction,
  adminIndexFieldAliasActionNameSpace,
} from "./admin-index-field-alias.action";

export interface AdminIndexFieldAliasStateModel extends ResourceStateModel<IndexFieldAlias> {
}

@Injectable()
@State<AdminIndexFieldAliasStateModel>({
  name: StatePartialEnum.admin_indexFieldAlias,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
  children: [],
})
export class AdminIndexFieldAliasState extends ResourceState<AdminIndexFieldAliasStateModel, IndexFieldAlias> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslate: LabelTranslateInterface,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_apiService, _store, _notificationService, _actions$, _environment, {
      nameSpace: adminIndexFieldAliasActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: _environment.indexFieldAliasRouteRedirectUrlAfterSuccessCreateAction,
      routeRedirectUrlAfterSuccessUpdateAction: _environment.indexFieldAliasRouteRedirectUrlAfterSuccessUpdateAction,
      routeRedirectUrlAfterSuccessDeleteAction: _environment.indexFieldAliasRouteRedirectUrlAfterSuccessDeleteAction,
      notificationResourceCreateSuccessTextToTranslate: _labelTranslate.searchIndexFieldAliasNotificationResourceCreateSuccess, // MARK_AS_TRANSLATABLE("admin.index-field-alias.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: _labelTranslate.searchIndexFieldAliasNotificationResourceDeleteSuccess, // MARK_AS_TRANSLATABLE("admin.index-field-alias.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: _labelTranslate.searchIndexFieldAliasNotificationResourceUpdateSuccess, // MARK_AS_TRANSLATABLE("admin.index-field-alias.notification.resource.update"),
      notificationResourceDeleteFailTextToTranslate: _labelTranslate.searchIndexFieldAliasNotificationResourceDeleteFail, // MARK_AS_TRANSLATABLE("admin.index-field-alias.notification.resource.deleteFail"),
    });
  }

  protected get _urlResource(): string {
    return this._environment.apiSearchIndexFieldAlias(this._environment);
  }

  @Selector()
  static isLoading(state: AdminIndexFieldAliasStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminIndexFieldAliasStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: AdminIndexFieldAliasStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.alias;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminIndexFieldAliasStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminIndexFieldAliasStateModel): boolean {
    return true;
  }

  @Action([AdminIndexFieldAliasAction.UpdateSuccess, AdminIndexFieldAliasAction.CreateSuccess, AdminIndexFieldAliasAction.DeleteSuccess])
  refreshSystemProperties(ctx: SolidifyStateContext<AppSystemPropertyStateModel<SystemPropertyPartial>>): Observable<any> {
    return ctx.dispatch(new AppSystemPropertyAction.GetSystemProperties());
  }
}
