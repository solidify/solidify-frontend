/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-index-field-alias-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {EmptyContainer} from "../../core/components/containers/empty-container/empty.container";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {AppRoutesPartialEnum} from "../../core/enums/partial/app-routes-partial.enum";
import {CanDeactivateGuard} from "../../core/guards/can-deactivate-guard.service";
import {SolidifyRoutes} from "../../core/models/route/route.model";
import {labelSolidifySearch} from "../label-translate-search.model";
import {AdminIndexFieldAliasCreateRoutable} from "./components/routables/admin-index-field-alias-create/admin-index-field-alias-create.routable";
import {AdminIndexFieldAliasDetailEditRoutable} from "./components/routables/admin-index-field-alias-detail-edit/admin-index-field-alias-detail-edit.routable";
import {AdminIndexFieldAliasListRoutable} from "./components/routables/admin-index-field-alias-list/admin-index-field-alias-list.routable";
import {AdminIndexFieldAliasState} from "./stores/admin-index-field-alias.state";

const routes: SolidifyRoutes = [
  {
    path: AppRoutesPartialEnum.root,
    component: AdminIndexFieldAliasListRoutable,
    data: {},
  },
  {
    path: AppRoutesPartialEnum.indexFieldAliasDetail + SOLIDIFY_CONSTANTS.SEPARATOR + AppRoutesPartialEnum.paramId,
    component: AdminIndexFieldAliasDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminIndexFieldAliasState.currentTitle,
    },
    children: [
      {
        path: AppRoutesPartialEnum.indexFieldAliasEdit,
        component: EmptyContainer,
        data: {
          breadcrumb: labelSolidifySearch.searchIndexFieldAliasBreadcrumbEdit,
        },
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
  {
    component: AdminIndexFieldAliasCreateRoutable,
    path: AppRoutesPartialEnum.indexFieldAliasCreate,
    data: {
      breadcrumb: labelSolidifySearch.searchIndexFieldAliasBreadcrumbCreate,
    },
    canDeactivate: [CanDeactivateGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminIndexFieldAliasRoutingModule {
}
