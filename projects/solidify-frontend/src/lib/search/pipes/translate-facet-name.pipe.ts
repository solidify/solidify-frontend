/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - translate-facet-name.pipe.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Injector,
  Pipe,
  PipeTransform,
} from "@angular/core";
import {Store} from "@ngxs/store";
import {DefaultSolidifySearchEnvironment} from "../../core/environments/environment.solidify-search";
import {Label} from "../../core/models/label.model";
import {
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
} from "../../core-resources/tools/is/is.tool";
import {FacetProperty} from "../models/facet-property.model";
import {TranslateFacetPipe} from "./translate-facet.pipe";

@Pipe({
  name: "translateFacetName",
  pure: false,
})
export class TranslateFacetNamePipe extends TranslateFacetPipe implements PipeTransform {

  constructor(protected readonly _store: Store, protected readonly _injector: Injector) {
    super(_store, _injector);
  }

  transform(facetName: string): string {
    if (this._currentLanguage === this._lastReturnLanguage && !isNullOrUndefined(this._lastReturnedValue)) {
      return this._lastReturnedValue;
    } else {
      let value: string = facetName;
      if (!isNullOrUndefined(this._environment.searchFacets) && !isNullOrUndefined(this._currentLanguage)) {
        value = TranslateFacetNamePipe.getTitleTranslated(this._environment, this._currentLanguage, facetName);
      }

      this._lastReturnLanguage = this._currentLanguage;
      this._lastReturnedValue = value;
      return value;
    }
  }

  static getFacetProperty(environment: DefaultSolidifySearchEnvironment, facetName: string): FacetProperty {
    return environment.searchFacets.find(facetProp => facetProp.name === facetName);
  }

  static getTitleTranslated(environment: DefaultSolidifySearchEnvironment, currentLanguage: string, facetName: string): string | undefined {
    const facetProperty = TranslateFacetNamePipe.getFacetProperty(environment, facetName);
    let title = facetName;
    if (isNotNullNorUndefined(facetProperty)) {
      const labelTranslated = this._getLabelTranslated(facetProperty.labels, currentLanguage);
      if (isNotNullNorUndefinedNorWhiteString(labelTranslated)) {
        title = labelTranslated;
      }
    }
    return title;
  }

  static getDescriptionTranslated(environment: DefaultSolidifySearchEnvironment, currentLanguage: string, facetName: string): string | undefined {
    const facetProperty = TranslateFacetNamePipe.getFacetProperty(environment, facetName);
    if (isNotNullNorUndefined(facetProperty)) {
      return this._getLabelTranslated(facetProperty.descriptionLabels, currentLanguage);
    }
  }

  private static _getLabelTranslated(listLabels: Label[], currentLanguage: string): string | undefined {
    if (isNonEmptyArray(listLabels)) {
      const labels: Label[] = listLabels.filter(label => label.language === currentLanguage);
      if (labels.length === 1) {
        return labels[0].text;
      }
    }
  }
}
