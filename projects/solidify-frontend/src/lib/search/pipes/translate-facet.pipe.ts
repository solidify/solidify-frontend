/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - translate-facet.pipe.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {
  Observable,
  Subscription,
} from "rxjs";
import {ExtendEnum} from "../../core-resources/types/extend-enum.type";
import {LanguagePartialEnum} from "../../core/enums/partial/language-partial.enum";
import {Store} from "@ngxs/store";
import {Injector} from "@angular/core";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {MemoizedUtil} from "../../core/utils/stores/memoized.util";
import {tap} from "rxjs/operators";
import {SubscriptionManager} from "../../core/models/managers/subscription-manager.model";

export abstract class TranslateFacetPipe {
  protected _environment: DefaultSolidifyEnvironment;
  protected _appLanguageObs: Observable<ExtendEnum<LanguagePartialEnum>>;

  protected _currentLanguage: string;

  protected _lastReturnedValue: string;
  protected _lastReturnLanguage: string;

  protected constructor(protected readonly _store: Store, protected readonly _injector: Injector) {
    this._environment = _injector.get(ENVIRONMENT);
    this._appLanguageObs = MemoizedUtil.select(this._store, this._environment.appState, state => state.appLanguage);

    this.subscribe(this._appLanguageObs.pipe(tap(value => {
      this._currentLanguage = value;
      this._lastReturnLanguage = null;
    })));
  }

  protected readonly _subscriptionManager: SubscriptionManager = new SubscriptionManager();

  subscribe<T>(observable: Observable<T>,
               onNext?: (value: T) => void,
               onError?: (error: Error) => void,
               onComplete?: () => void): Subscription {
    return this._subscriptionManager.subscribe(observable, onNext, onError, onComplete);
  }
}
