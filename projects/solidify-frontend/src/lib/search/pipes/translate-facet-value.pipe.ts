/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - translate-facet-value.pipe.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  Injector,
  Pipe,
  PipeTransform,
} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {Label} from "../../core/models/label.model";
import {SubscriptionManager} from "../../core/models/managers/subscription-manager.model";
import {isNullOrUndefined} from "../../core-resources/tools/is/is.tool";
import {FacetHelper} from "../helpers/facet.helper";
import {TranslateFacetPipe} from "./translate-facet.pipe";

@Pipe({
  name: "translateFacetValue",
  pure: false,
})
export class TranslateFacetValuePipe extends TranslateFacetPipe implements PipeTransform {

  constructor(protected readonly _store: Store,
              protected readonly _injector: Injector,
              protected readonly _translateService: TranslateService) {
    super(_store, _injector);
  }

  transform(facetValue: string, facetName: string): string {
    let value: string = FacetHelper.cleanExcludedValue(facetValue);
    if (isNullOrUndefined(this._environment.searchFacetValuesTranslations) || isNullOrUndefined(this._currentLanguage)) {
      return value;
    }

    const facetValueProperty = this._environment.searchFacetValuesTranslations.find(facetProp =>
      facetProp.name === facetName && facetProp.value === value,
    );

    if (isNullOrUndefined(facetValueProperty)) {
      return value;
    }

    if (facetValueProperty.isFrontendTranslation) {
      return this._translateService.instant(facetValueProperty.labelToTranslate);
    }

    const currentLang: string = this._currentLanguage;
    const labels: Label[] = facetValueProperty.labels.filter(label => label.language === currentLang);
    if (labels.length === 1) {
      value = labels[0].text;
    }

    this._lastReturnLanguage = this._currentLanguage;
    this._lastReturnedValue = value;
    return value;
  }

  protected readonly _subscriptionManager: SubscriptionManager = new SubscriptionManager();
}
