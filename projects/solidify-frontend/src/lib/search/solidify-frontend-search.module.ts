/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-frontend-search.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {NgModule} from "@angular/core";
import {NgxsModule} from "@ngxs/store";
import {SolidifyFrontendCoreModule} from "../core/solidify-frontend-core.module";
import {SearchFacetPresentational} from "./components/presentationals/search-facet/search-facet.presentational";
import {TranslateFacetNamePipe} from "./pipes/translate-facet-name.pipe";
import {TranslateFacetValuePipe} from "./pipes/translate-facet-value.pipe";

const containers = [];

const dialogs = [];

const presentationals = [
  SearchFacetPresentational,
];

const routables = [];

const components = [
  ...containers,
  ...dialogs,
  ...presentationals,
  ...routables,
];

const modules = [];

const states = [];

export const pipes = [
  TranslateFacetNamePipe,
  TranslateFacetValuePipe,
];

@NgModule({
  declarations: [
    ...components,
    ...pipes,
  ],
  imports: [
    ...modules,
    NgxsModule.forFeature([
      ...states,
    ]),
    SolidifyFrontendCoreModule,
  ],
  exports: [
    ...modules,
    ...components,
    ...pipes,
    NgxsModule,
  ],
  providers: [],
})
export class SolidifyFrontendSearchModule {
}
