/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - label-translate-search.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MARK_AS_TRANSLATABLE} from "../core-resources/utils/translate.util";

export interface LabelSolidifySearch {
  searchRefineSearch?: string;
  searchIncludeFacet?: string;
  searchExcludeFacet?: string;
  searchQuestionMarkTooltip?: string;
  searchClearFilters?: string;
  searchIndexFieldAliasTileTitle?: string;
  searchIndexFieldAliasTileSubtitle?: string;
  searchIndexFieldAliasNotificationResourceCreateSuccess?: string;
  searchIndexFieldAliasNotificationResourceDeleteSuccess?: string;
  searchIndexFieldAliasNotificationResourceUpdateSuccess?: string;
  searchIndexFieldAliasNotificationResourceDeleteFail?: string;
  searchIndexFieldAliasBreadcrumb?: string;
  searchIndexFieldAliasBreadcrumbEdit?: string;
  searchIndexFieldAliasBreadcrumbCreate?: string;
  searchIndexFieldAliasListIndexName?: string;
  searchIndexFieldAliasListAlias?: string;
  searchIndexFieldAliasListField?: string;
  searchIndexFieldAliasListFacet?: string;
  searchIndexFieldAliasListSystem?: string;
  searchIndexFieldAliasListSort?: string;
  searchIndexFieldAliasListCreated?: string;
  searchIndexFieldAliasListUpdated?: string;
  searchIndexFieldAliasFormIndexName?: string;
  searchIndexFieldAliasFormAlias?: string;
  searchIndexFieldAliasFormField?: string;
  searchIndexFieldAliasFormSystem?: string;
  searchIndexFieldAliasFormFacet?: string;
  searchIndexFieldAliasFormFacetMinCount?: string;
  searchIndexFieldAliasFormFacetLimit?: string;
  searchIndexFieldAliasFormFacetDefaultVisibleValues?: string;
  searchIndexFieldAliasFormFacetOrder?: string;
  searchIndexFieldAliasDialogDeleteMessage?: string;
}

export const labelSolidifySearch: LabelSolidifySearch = {
  searchRefineSearch: MARK_AS_TRANSLATABLE("solidify.search.refineSearch"),
  searchIncludeFacet: MARK_AS_TRANSLATABLE("solidify.search.includeFacet"),
  searchExcludeFacet: MARK_AS_TRANSLATABLE("solidify.search.excludeFacet"),
  searchQuestionMarkTooltip: MARK_AS_TRANSLATABLE("solidify.search.searchQuestionMarkTooltip"),
  searchClearFilters: MARK_AS_TRANSLATABLE("solidify.search.clearFilters"),
  searchIndexFieldAliasTileTitle: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.tile.title"),
  searchIndexFieldAliasTileSubtitle: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.tile.subtitle"),
  searchIndexFieldAliasNotificationResourceCreateSuccess: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.notification.resourceCreate.success"),
  searchIndexFieldAliasNotificationResourceDeleteSuccess: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.notification.resourceDelete.success"),
  searchIndexFieldAliasNotificationResourceUpdateSuccess: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.notification.resourceUpdate.success"),
  searchIndexFieldAliasNotificationResourceDeleteFail: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.notification.resourceDelete.fail"),
  searchIndexFieldAliasBreadcrumb: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.breadcrumb.root"),
  searchIndexFieldAliasBreadcrumbEdit: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.breadcrumb.edit"),
  searchIndexFieldAliasBreadcrumbCreate: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.breadcrumb.create"),
  searchIndexFieldAliasListIndexName: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.list.indexName"),
  searchIndexFieldAliasListAlias: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.list.alias"),
  searchIndexFieldAliasListField: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.list.field"),
  searchIndexFieldAliasListFacet: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.list.facet"),
  searchIndexFieldAliasListSystem: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.list.system"),
  searchIndexFieldAliasListSort: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.list.sort"),
  searchIndexFieldAliasListCreated: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.list.created"),
  searchIndexFieldAliasListUpdated: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.list.updated"),
  searchIndexFieldAliasFormIndexName: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.form.indexName"),
  searchIndexFieldAliasFormAlias: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.form.alias"),
  searchIndexFieldAliasFormField: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.form.field"),
  searchIndexFieldAliasFormSystem: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.form.system"),
  searchIndexFieldAliasFormFacet: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.form.facet"),
  searchIndexFieldAliasFormFacetMinCount: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.form.facetMinCount"),
  searchIndexFieldAliasFormFacetLimit: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.form.facetLimit"),
  searchIndexFieldAliasFormFacetDefaultVisibleValues: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.form.facetDefaultVisibleValues"),
  searchIndexFieldAliasFormFacetOrder: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.form.facetOrder"),
  searchIndexFieldAliasDialogDeleteMessage: MARK_AS_TRANSLATABLE("solidify.search.indexFieldAlias.dialog.delete.message"),
};
