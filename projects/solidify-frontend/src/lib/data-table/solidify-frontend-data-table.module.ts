/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-frontend-data-table.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterLink} from "@angular/router";
import {SolidifyFrontendSelectModule} from "../select/solidify-frontend-select.module";
import {DataTablePresentational} from "./data-table.presentational";

@NgModule({
  declarations: [
    DataTablePresentational,
  ],
  imports: [
    RouterLink,
    SolidifyFrontendSelectModule,
  ],
  exports: [
    RouterLink,
    SolidifyFrontendSelectModule,
    DataTablePresentational,
  ],
  providers: [],
})
export class SolidifyFrontendDataTableModule {
}
