/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - data-table.presentational.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NO_ERRORS_SCHEMA} from "@angular/core";
import {
  ComponentFixture,
  TestBed,
  waitForAsync,
} from "@angular/core/testing";
import {MatSnackBar} from "@angular/material/snack-bar";
import {TranslateService} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {DefaultSolidifyEnvironment} from "../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../core/injection-tokens/label-to-translate.injection-token";

import {QueryParameters} from "../core-resources/models/query-parameters/query-parameters.model";
import {SNACK_BAR} from "../core/models/snackbar/snack-bar.model";
import {IsFalsePipe} from "../core/pipes/shared-is-pipes/is-false.pipe";
import {SolidifyFrontendMaterialModule} from "../core/solidify-frontend-material.module";
import {MockTranslatePipe} from "../core/tests/helpers/mock-translate.pipe";
import {MockTranslateService} from "../core/tests/helpers/mock-translate.service";
import {DateUtil} from "../core/utils/date.util";
import {DataTablePresentational} from "./data-table.presentational";

describe("DataTablePresentational", () => {
  let component: DataTablePresentational<any>;
  let fixture: ComponentFixture<any>;
  const environment = {
    breakpointXs: "0px",
    breakpointSm: "576px",
    breakpointMd: "992px",
    breakpointLg: "1360px",
  } as DefaultSolidifyEnvironment;

  beforeEach(waitForAsync(() => {
    new DateUtil();

    TestBed.configureTestingModule({
      imports: [
        SolidifyFrontendMaterialModule,
        NgxsModule.forRoot([]),
      ],
      declarations: [DataTablePresentational, MockTranslatePipe, IsFalsePipe],
      providers: [
        {
          provide: TranslateService,
          useClass: MockTranslateService,
        },
        {
          provide: LABEL_TRANSLATE,
          useValue: {},
        },
        {
          provide: SNACK_BAR,
          useClass: MatSnackBar,
        },
        {
          provide: ENVIRONMENT,
          useValue: environment,
        },
        {
          provide: TranslateService,
          useClass: MockTranslateService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTablePresentational);
    component = fixture.componentInstance;
    component.queryParameters = new QueryParameters();
    component.columns = [];
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  describe("getData", () => {
    it("should return nested data", () => {
      const when = "2019-08-22T09:34:27.603+0200";
      const result = component.getData({
          creation: {
            when: when,
          },
        },
        {field: "creation.when"} as any);

      expect(result).toBe(when);
    });
  });
});
