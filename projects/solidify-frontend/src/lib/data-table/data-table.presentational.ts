/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - data-table.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  CdkDragDrop,
  CdkDragEnd,
  CdkDragStart,
  moveItemInArray,
} from "@angular/cdk/drag-drop";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  ElementRef,
  HostBinding,
  Inject,
  Input,
  OnChanges,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
  TrackByFunction,
  ViewChild,
  ViewChildren,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
} from "@angular/forms";
import {MatCheckboxChange} from "@angular/material/checkbox";
import {MatDialog} from "@angular/material/dialog";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  filter,
  map,
  take,
  tap,
} from "rxjs/operators";
import {
  isArray,
  isBoolean,
  isEmptyArray,
  isEmptyString,
  isFalse,
  isFunction,
  isInstanceOf,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isNumberReal,
  isObservable,
  isString,
  isTrue,
} from "../core-resources/tools/is/is.tool";
import {MappingObject} from "../core-resources/types/mapping-type.type";
import {ArrayUtil} from "../core-resources/utils/array.util";
import {ClipboardUtil} from "../core-resources/utils/clipboard.util";
import {EnumUtil} from "../core-resources/utils/enum.util";
import {MappingObjectUtil} from "../core-resources/utils/mapping-object.util";
import {ObjectUtil} from "../core-resources/utils/object.util";
import {QueryParametersUtil} from "../core-resources/utils/query-parameters.util";
import {StringUtil} from "../core-resources/utils/string.util";
import {CoreAbstractComponent} from "../core/components/core-abstract/core-abstract.component";
import {DataTableLooseSelectionDialog} from "../core/components/dialogs/data-table-loose-selection/data-table-loose-selection.dialog";
import {PaginatorPresentational} from "../core/components/presentationals/paginator/paginator.presentational";
import {SOLIDIFY_CONSTANTS} from "../core-resources/constants";
import {DataTableAnchorDynamicInjectionDirective} from "../core/directives/data-table-anchor-dynamic-injection/data-table-anchor-dynamic-injection.directive";
import {ButtonColorEnum} from "../core/enums/button-color.enum";
import {ValueType} from "../core/enums/datatable/data-table-component-input-value-type.enum";
import {DataTableFieldTypeEnum} from "../core/enums/datatable/data-table-field-type.enum";
import {DataTableComponentPartialEnum} from "../core/enums/datatable/partial/data-table-component-partial.enum";
import {OrderEnum} from "../core-resources/enums/order.enum";
import {IconNamePartialEnum} from "../core/enums/partial/icon-name-partial.enum";
import {DefaultSolidifyEnvironment} from "../core/environments/environment.solidify-defaults";
import {FormValidationHelper} from "../core/helpers/form-validation.helper";
import {ENVIRONMENT} from "../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../core/injection-tokens/label-to-translate.injection-token";
import {DataTableActions} from "../core/models/datatable/data-table-actions.model";
import {DataTableBulkActions} from "../core/models/datatable/data-table-bulk-actions.model";
import {DataTableColumns} from "../core/models/datatable/data-table-columns.model";
import {DataTableComponentInput} from "../core/models/datatable/data-table-component-input.model";
import {ExtraButtonToolbar} from "../core/models/datatable/extra-button-toolbar.model";
import {BaseResourceWithLabels} from "../core/models/dto/base-resource-with-labels.model";
import {BaseResource} from "../core-resources/models/dto/base-resource.model";
import {KeyValue} from "../core-resources/models/key-value.model";
import {ObservableOrPromiseOrValue} from "../core/models/observables/observable-or-promise-or-value.model";
import {Paging} from "../core-resources/models/query-parameters/paging.model";
import {QueryParameters} from "../core-resources/models/query-parameters/query-parameters.model";
import {Sort} from "../core-resources/models/query-parameters/sort.model";
import {BreakpointService} from "../core/services/breakpoint.service";
import {NotificationService} from "../core/services/notification.service";
import {ScrollService} from "../core/services/scroll.service";
import {DateUtil} from "../core/utils/date.util";
import {DialogUtil} from "../core/utils/dialog.util";
import {LabelUtil} from "../core/utils/label.util";
import {ObservableUtil} from "../core/utils/observable.util";
import {LabelTranslateInterface} from "../label-translate-interface.model";

@Component({
  selector: "solidify-data-table",
  templateUrl: "./data-table.presentational.html",
  styleUrls: ["./data-table.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataTablePresentational<TResource extends BaseResource> extends CoreAbstractComponent implements OnInit, OnChanges {
  context: Context = new Context();

  displayFilters: boolean = true;

  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number;

  private _data: TResource[];
  currentSort: Sort;

  @HostBinding("attr.data-test-info-number-line")
  numberLine: number = undefined;

  @ViewChild("headerTitleLine")
  headerTitleLine: ElementRef;

  @ViewChild("thead")
  thead: ElementRef;

  @ViewChild("tbody")
  tbody: ElementRef;

  @Input()
  focusFirstElement: boolean = true;

  @Input()
  scrollToTopWhenPaginate: boolean = false;

  /**
   * Route used to navigate to detail list. This route should contain {resId} used to inject resId of row clicked
   * if undefined output click event
   */
  @Input()
  detailRouteToInterpolate: string;

  detailRouteInterpolated(rowData: TResource): string {
    return StringUtil.formatKeyValue(this.detailRouteToInterpolate, {
      key: SOLIDIFY_CONSTANTS.RES_ID,
      value: rowData.resId,
    });
  }

  @Input()
  set data(value: TResource[]) {
    if (this.pendingRetrieveAllForSelection) {
      this.pendingRetrieveAllForSelection = false;
      this.multiSelectionValueOnAllPages = value;
      this.computeNumberItemSelectedOnCurrentPage();
      this.displaySelectAllElementSuccess = true;
      return;
    }
    this._data = value;
    this.computeIsDataPresent();
    this.computeNumberItemSelectedOnCurrentPage();
    this.loadComponent();
  }

  get data(): TResource[] {
    return this._data;
  }

  dataPaginatedInMemory: TResource[];

  filterFormGroup: FormGroup;

  typeIntoRowData(value: any): TResource {
    return value;
  }

  typeIntoColumn(value: any): DataTableColumns<TResource> {
    return value;
  }

  typeIntoAction(value: any): DataTableActions<TResource> {
    return value;
  }

  typeIntoActions(value: any): DataTableActions<TResource>[] {
    return value;
  }

  typeIntoBaseResourceWithLabels(value: any): BaseResourceWithLabels {
    return value;
  }

  get isNotNullNorUndefined(): typeof isNotNullNorUndefined {
    return isNotNullNorUndefined;
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (this.isInMemory) {
      this.subscribe(this._queryParametersBS.pipe(
        filter(queryParameters => isNotNullNorUndefined(queryParameters)),
        tap(queryParameters => {
          this.queryParameters = queryParameters;
          this._computeDataPaginatedInMemory();
          this._changeDetector.detectChanges();
        }),
      ));
      this.queryParameters = new QueryParameters(this.inMemoryPageSize);
      this.queryParameters.paging.length = this.data?.length;
      this._queryParametersBS.next(this.queryParameters);
    } else {
      this.backendSort(null);
    }
  }

  private _dataForPage: TResource[];

  get dataForPage(): TResource[] {
    return this._dataForPage;
  }

  forceHoverRowData: TResource | undefined;

  trackByFunction: TrackByFunction<TResource> = (index: number, item: TResource) => item.resId;
  trackByKeyValueFunction: TrackByFunction<KeyValue> = (index: number, item: KeyValue) => item.key;

  @HostBinding("class.is-clickable")
  @Input()
  isClickable: boolean = true;

  @ViewChild("headerButtonAndPagination")
  headerButtonAndPagination: ElementRef;

  private _HEIGHT_PAGINATOR: number = 50;
  private _HEIGHT_HEADER_TITLE: number = 40;
  private _STICKY_FIX_GLITCH_CONTENT_VISIBLE_ON_TOP_OF_HEADER: number = -1;
  private _STICKY_FIX_GLITCH_HORIZONTAL_LINE: number = -1;

  @Input()
  stickyTopPosition: number | undefined;

  get stickyTopPositionText(): string {
    return this.stickyTopPosition + "px";
  }

  get stickyTopPositionTitleNumber(): number {
    return this.stickyTopPosition + (this.headerButtonAndPagination?.nativeElement ? this.headerButtonAndPagination.nativeElement.offsetHeight : this._HEIGHT_PAGINATOR) + this._STICKY_FIX_GLITCH_CONTENT_VISIBLE_ON_TOP_OF_HEADER;
  }

  get stickyTopPositionTitleText(): string {
    return this.stickyTopPositionTitleNumber + "px";
  }

  get stickyTopPositionFilterNumber(): number {
    return this.stickyTopPositionTitleNumber + (this.headerTitleLine?.nativeElement ? this.headerTitleLine.nativeElement.offsetHeight : this._HEIGHT_HEADER_TITLE) + this._STICKY_FIX_GLITCH_HORIZONTAL_LINE;
  }

  get stickyTopPositionFilterText(): string {
    return this.stickyTopPositionFilterNumber + "px";
  }

  get theadHeight(): number {
    return 0; // this.thead?.nativeElement ? this.thead?.nativeElement.offsetHeight : 0;
  }

  get tbodyHeight(): number {
    return this.tbody?.nativeElement ? this.tbody?.nativeElement.offsetHeight : 0;
  }

  classBypassEdit: string;

  @Input()
  preloadRowsForPage: boolean = false;

  @Input()
  isMultiSelectable: boolean = false;

  @Input()
  skipInitialQuery: boolean = false;

  @HostBinding("attr.data-test-info-is-loading")
  @Input()
  isLoading: boolean = false;

  @Input()
  displaySpinnerWhenLoading: boolean = false;

  private _queryParameters: QueryParameters;

  @Input()
  columnsSkippedToClear: string[];

  @Input()
  isInMemory: boolean = false;

  @Input()
  inMemoryPageSize: number = 10;

  @Input()
  defaultMultiSelection: TResource[];

  @Input()
  set queryParameters(value: QueryParameters) {
    if (isTrue(this.pendingRetrieveAllForSelection) || isTrue(this.displaySelectAllElementSuccess)) {
      return;
    }
    const newSearchItems = QueryParametersUtil.getSearchItems(value);
    const oldSearchItems = QueryParametersUtil.getSearchItems(this._queryParameters);
    if (this._isDifferentSearchItems(oldSearchItems, newSearchItems)) {
      this.multiSelectionValueOnAllPages = this.defaultMultiSelection ?? [];
      this.resetCounterItemSelectedOnCurrentPage();
      this.resetBannerSelectAll();
    }
    this._queryParameters = QueryParametersUtil.clone(value);
    if (isNullOrUndefined(value?.paging)) {
      this._queryParameters.paging = new Paging(this.environment?.defaultPageSize);
    }
    this._computeDisplayButtonCleanAllFilter();
    this._initFiltersValues();
  }

  get queryParameters(): QueryParameters {
    return this._queryParameters;
  }

  displayButtonCleanAllFilter: boolean;

  buttonCleanAllFilter: ExtraButtonToolbar<TResource> = {
    color: ButtonColorEnum.primary,
    icon: IconNamePartialEnum.clear,
    displayCondition: current => this.displayButtonCleanAllFilter,
    callback: () => this.clearFilters(),
    labelToTranslate: (resource) => this.labelTranslate.dataTableCleanAllFilter,
    order: 40,
  };

  private _isDifferentSearchItems(oldSearchItems: MappingObject<string, string>, newSearchItems: MappingObject<string, string>): boolean {
    return true;
  }

  private _computeDisplayButtonCleanAllFilter(): void {
    this.displayButtonCleanAllFilter = this.isFilterApplied();
  }

  private _columns: DataTableColumns[];

  @Input()
  set columns(value: DataTableColumns[]) {
    this._columns = value;
    const defaultSort = this._columns.find(c => isTrue(c.isSortable) && c.order !== 0);
    if (isNotNullNorUndefined(defaultSort)) {
      this.currentSort = {
        field: this._getSortableField(defaultSort),
        order: defaultSort.order,
      } as Sort;
    }

    this.filterFormGroup = this._fb.group([]);
    value.forEach(c => {
      const fc = this._fb.control(undefined);
      this.filterFormGroup.addControl(this._replaceDotByDash(c.field as string), fc);
      this.subscribe(fc.valueChanges.pipe(
        tap(v => {
          this.searchChange(c, v);
        }),
      ));
    });
    this._initFiltersValues();
  }

  get columns(): DataTableColumns[] {
    return this._columns;
  }

  private _replaceDotByDash(field: string): string {
    if (isNullOrUndefinedOrWhiteString(field)) {
      return field;
    }
    return StringUtil.replaceAll(field, SOLIDIFY_CONSTANTS.DOT, SOLIDIFY_CONSTANTS.DASH);
  }

  getFilterFormControl(column: DataTableColumns): FormControl {
    return FormValidationHelper.getFormControl(this.filterFormGroup, this._replaceDotByDash(column.field as string));
  }

  @Input()
  displayActionCopyIdToClipboard: boolean = false;

  @Input()
  displayActionShowHistory: boolean = false;

  private readonly _actionCopyIdToClipboard: DataTableActions = {
    logo: IconNamePartialEnum.resId,
    callback: (resource: TResource) => {
      ClipboardUtil.copyStringToClipboard(resource.resId);
      this._notificationService.showInformation(this.labelTranslate.coreNotificationIdCopyToClipboard);
    },
    placeholder: current => this.labelTranslate.dataTableCopyIdToClipboard,
    displayOnCondition: current => this.displayActionCopyIdToClipboard,
    isWrapped: true,
  };

  private readonly _actionShowHistory: DataTableActions = {
    logo: IconNamePartialEnum.history,
    callback: (resource: TResource) => this._showHistoryBS.next(resource),
    placeholder: current => this.labelTranslate.dataTableShowHistoryStatus,
    displayOnCondition: current => this.displayActionShowHistory,
    isWrapped: true,
  };

  private _actions: DataTableActions[] = [this._actionCopyIdToClipboard, this._actionShowHistory];

  @Input()
  set actions(value: DataTableActions[]) {
    if (isNullOrUndefined(value)) {
      value = [];
    }
    this._actions = [...value, this._actionCopyIdToClipboard, this._actionShowHistory];
  }

  get actions(): DataTableActions[] {
    return this._actions.sort((a, b) => a.order - b.order);
  }

  get actionsWrapped(): DataTableActions[] {
    return this.actions
      .filter(a => isTrue(this.environment.dataTableRepeatLineActionsInWrappedButton) || isNullOrUndefined(a.isWrapped) || isTrue(a.isWrapped))
      .sort((a, b) => a.order - b.order);
  }

  get actionsUnwrapped(): DataTableActions[] {
    return this.actions.filter(a => isFalse(a.isWrapped)).sort((a, b) => a.order - b.order);
  }

  private _bulkActions: DataTableBulkActions<TResource>[] = [];

  @Input()
  set bulkActions(value: DataTableBulkActions<TResource>[]) {
    this._bulkActions = value;
  }

  get bulkActions(): DataTableBulkActions<TResource>[] {
    return this._bulkActions;
  }

  private _listHighlightingId: string[] = [];

  @Input()
  set listHighlightingId(value: string[]) {
    this._listHighlightingId = isNullOrUndefined(value) ? [] : value;
  }

  get listHighlightingId(): string[] {
    return isNullOrUndefined(this._listHighlightingId) ? [] : this._listHighlightingId;
  }

  @Input()
  isHighlightCondition: undefined | ((current: TResource) => boolean);

  @Input()
  lineTooltipCallback: undefined | ((current: TResource) => string);

  @Input()
  scrollHeight: string | undefined = undefined;

  private _listCdkDropListId: string[];
  listCdkDropListIdPrefixed: string[];

  @Input()
  set listCdkDropListId(value: string[]) {
    this._listCdkDropListId = value;
    this.listCdkDropListIdPrefixed = value.map(v => this.environment.cdkDropListIdPrefix + v);
  }

  get listCdkDropListId(): string[] {
    return this._listCdkDropListId;
  }

  @Input()
  isDraggable: boolean;

  @Input()
  isDroppable: boolean;

  @Input()
  draggablePreviewMatIcon: IconNamePartialEnum | undefined = undefined;

  @Input()
  draggablePreviewColumn: keyof TResource | string | undefined = undefined;

  @Input()
  alwaysDisplayButtonActionsWrapped: boolean = false;

  private readonly _selectBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("selectChange")
  readonly selectObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._selectBS);

  private readonly _multiSelectionBS: BehaviorSubject<TResource[]> = new BehaviorSubject<TResource[]>([]);
  @Output("multiSelectionChange")
  readonly multiSelectionObs: Observable<TResource[]> = ObservableUtil.asObservable(this._multiSelectionBS);

  private readonly _dragStartBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("dragStartChange")
  readonly dragStartObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._dragStartBS);

  private readonly _dragEndBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("dragEndChange")
  readonly dragEndObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._dragEndBS);

  private readonly _dragEndSendDataBS: BehaviorSubject<TResource[]> = new BehaviorSubject<TResource[]>([]);
  @Output("dragEndSendData")
  readonly dragEndSendDataObs: Observable<TResource[]> = ObservableUtil.asObservable(this._dragEndSendDataBS);

  set multiSelectionValueOnAllPages(value: TResource[]) {
    this._multiSelectionBS.next(isNullOrUndefined(value) ? [] : value);
  }

  get multiSelectionValueOnAllPages(): TResource[] {
    return isNullOrUndefined(this._multiSelectionBS.value) ? [] : this._multiSelectionBS.value;
  }

  multiSelectionValueOnCurrentPageCounter: number = 0;

  private readonly _queryParametersBS: BehaviorSubject<QueryParameters | undefined> = new BehaviorSubject<QueryParameters | undefined>(undefined);
  @Output("queryParametersChange")
  readonly queryParametersObs: Observable<QueryParameters | undefined> = ObservableUtil.asObservable(this._queryParametersBS);

  private readonly _queryParametersWithoutRefreshBS: BehaviorSubject<QueryParameters | undefined> = new BehaviorSubject<QueryParameters | undefined>(undefined);
  @Output("queryParametersWithoutRefreshChange")
  readonly queryParametersWithoutRefreshObs: Observable<QueryParameters | undefined> = ObservableUtil.asObservable(this._queryParametersWithoutRefreshBS);

  private readonly _showHistoryBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("showHistoryChange")
  readonly showHistoryObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._showHistoryBS);

  @ViewChild("paginator")
  paginator: PaginatorPresentational;

  isDataPresent: boolean;
  displayPropositionSelectAllResult: boolean = false;
  pendingRetrieveAllForSelection: boolean = false;
  displaySelectAllElementSuccess: boolean = false;

  get fieldTypeEnum(): typeof DataTableFieldTypeEnum {
    return DataTableFieldTypeEnum;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  get dataTableComponentEnum(): typeof DataTableComponentPartialEnum {
    return DataTableComponentPartialEnum;
  }

  private readonly _SEPARATOR: string = ".";

  constructor(readonly _breakpointService: BreakpointService,
              private readonly _dialog: MatDialog,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _notificationService: NotificationService,
              @Inject(ENVIRONMENT) readonly environment: DefaultSolidifyEnvironment,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface,
              private readonly _componentFactoryResolver: ComponentFactoryResolver,
              private readonly _scrollService: ScrollService,
              private readonly _translateService: TranslateService,
              private readonly _store: Store,
              private readonly _fb: FormBuilder) {
    super();
    this.TIME_BEFORE_DISPLAY_TOOLTIP = this.environment.timeBeforeDisplayTooltipOnDataTable;
    this.classBypassEdit = this.environment.classBypassEdit;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.queryParameters || changes.actions || changes.data) {
      this.computeContext();
    }
    if ((changes.queryParameters || changes.data) && !this.isInMemory) {
      this.computeDataForPage();
    }
  }

  @ViewChildren(DataTableAnchorDynamicInjectionDirective) listAnchorDynamicInjectionDirective: QueryList<DataTableAnchorDynamicInjectionDirective<TResource>>;

  loadComponent(): void {
    setTimeout(() => {
      if (isNullOrUndefined(this.listAnchorDynamicInjectionDirective)) {
        return;
      }
      this.listAnchorDynamicInjectionDirective.forEach(anchor => {
        if (isNullOrUndefined(anchor?.col?.component?.componentType)) {
          return;
        }
        const componentFactory = this._componentFactoryResolver.resolveComponentFactory(anchor.col.component.componentType) as ComponentFactory<CoreAbstractComponent>;

        const viewContainerRef = anchor.viewContainerRef;
        viewContainerRef.clear();
        const componentRef = viewContainerRef.createComponent<CoreAbstractComponent>(componentFactory);

        if (isNullOrUndefined(anchor.col.component.inputs)) {
          return;
        }
        anchor.col.component.inputs.forEach(input => {
          componentRef.instance[input.key] = this._getValueComponent(input, anchor.rowData, anchor.col);
        });
      });
      this._changeDetector.detectChanges();
    }, 0);
  }

  private _getValueComponent(input: DataTableComponentInput, rowData: TResource, col: DataTableColumns): any {
    switch (input.valueType) {
      case ValueType.isRowData:
        return rowData;
      case ValueType.isOtherColData:
        return rowData[input.dataCol];
      case ValueType.isCol:
        return col;
      case ValueType.isField:
        return col.field;
      case ValueType.isAttributeOnCol:
        return col[input.attributeOnCol];
      case ValueType.isColData:
        return this.getData(rowData, col);
      case ValueType.isResId:
        return this.getResId(rowData);
      case ValueType.isStatic:
        return input.staticValue;
      case ValueType.isCallback:
        return input.callbackValue(rowData, col);
      default:
        return undefined;
    }
  }

  private computeContext(): void {
    this.context = new Context();
    this.context.mapAtLeastOneActionToDisplay = new Map<DataTableActions, boolean>();
    if (isNullOrUndefined(this._data) || isNullOrUndefined(this._actions)) {
      return;
    }
    this.actions.forEach(action => {
      this.context.mapAtLeastOneActionToDisplay.set(action, this._shouldDisplayActionColumns(action));
    });
  }

  private _shouldDisplayActionColumns(action: DataTableActions): boolean {
    if (isNullOrUndefined(action.displayOnCondition)) {
      return true;
    }
    return this._data.some(data => isTrue(action.displayOnCondition(data)));
  }

  private computeDataForPage(): void {
    if (!this.preloadRowsForPage) {
      return;
    }
    let count = this.queryParameters && this.queryParameters.paging ? this.queryParameters.paging.pageSize : 0;
    if (count > 100) {
      count = 100;
    }
    if (this._dataForPage?.length >= count && isNullOrUndefined(this.numberLine)) {
      return;
    }
    const defaultValue = this._getEmptyData();
    this._dataForPage = new Array(count || 0).fill(defaultValue);
    (this._data || []).forEach((value, index) => this._dataForPage[index] = value);
  }

  private resetCounterItemSelectedOnCurrentPage(): void {
    this.multiSelectionValueOnCurrentPageCounter = 0;
  }

  private resetBannerSelectAll(): void {
    this.displayPropositionSelectAllResult = false;
    this.displaySelectAllElementSuccess = false;
  }

  private computeNumberItemSelectedOnCurrentPage(): void {
    this.resetCounterItemSelectedOnCurrentPage();
    this.multiSelectionValueOnAllPages.forEach(itemSelected => {
      if (!isNullOrUndefined(this.data) && !isNullOrUndefined(this.data.find(data => data.resId === itemSelected.resId))) {
        this.multiSelectionValueOnCurrentPageCounter++;
      }
    });
  }

  private computeIsDataPresent(): void {
    let isDataPresent = false;
    if (!isNullOrUndefined(this.data) && this.data.length > 0) {
      isDataPresent = true;
      this.numberLine = this.data.length;
    } else {
      this.numberLine = 0;
    }
    this.isDataPresent = isDataPresent;
  }

  showDetail(row: TResource): void {
    this._selectBS.next(row);
  }

  getCellData(row: TResource, column: DataTableColumns): any {
    const data = this.getData(row, column);
    if (column.translate && isNonEmptyArray(column.filterEnum)) {
      const dataStr = data + "";
      const isKeyValue = this.isInstanceOfKeyValue(column.filterEnum[0]);
      const enumValue = isKeyValue ?
        (column.filterEnum as KeyValue[]).find(f => f.key === dataStr)
        : (column.filterEnum as BaseResourceWithLabels[]).find(f => f.resId === dataStr);
      if (enumValue) {
        return isKeyValue ? (enumValue as KeyValue).value : (enumValue as BaseResourceWithLabels).labels;
      }
      return data;
    }
    return this.applyTypeToData(data, column.type);
  }

  convertListToString(row: TResource, column: DataTableColumns): string {
    let data: any[] = this.getData(row, column);
    if (!isArray(data) || isEmptyArray(data) || !isFunction(column.listCallback)) {
      return SOLIDIFY_CONSTANTS.STRING_EMPTY;
    }
    data = this.getMaxItemIfDefined(data, column).map(d => column.listCallback(d));
    return data.join(", ");
  }

  applyCallbackToListItemIfDefined(item: any, column: DataTableColumns): string {
    if (isFunction(column.listCallback)) {
      return column.listCallback(item);
    }
    return item;
  }

  getMaxItemIfDefined(list: any[] | undefined, column: DataTableColumns): any[] | undefined {
    if (isArray(list) && isNumberReal(column.maxListItem) && list.length > column.maxListItem) {
      return list.slice(0, column.maxListItem);
    }
    return list;
  }

  getData(row: TResource, column: DataTableColumns): any {
    const field = column?.field;
    if (isFunction(column.callbackValue)) {
      return column.callbackValue(row, column);
    }
    if (isNullOrUndefinedOrWhiteString(field)) {
      return row;
    }
    if ((field as string).includes(this._SEPARATOR)) {
      return ObjectUtil.getNestedValue(row as any, column.field as string);
    }
    return row[field];
  }

  getResId(row: TResource): string {
    return (row as TResource).resId;
  }

  private applyTypeToData(data: any, type: DataTableFieldTypeEnum): any {
    if (isNullOrUndefined(data)) {
      return StringUtil.stringEmpty;
    }
    if (type === DataTableFieldTypeEnum.date) {
      return DateUtil.convertDateToDateString(data);
    }
    if (type === DataTableFieldTypeEnum.datetime) {
      return DateUtil.convertDateToDateTimeString(data);
    }
    return data;
  }

  sort(columns: DataTableColumns<TResource>): void {
    if (this.isInMemory) {
      this.frontendSort(columns);
    } else {
      this.backendSort(columns);
    }
  }

  frontendSort(columns: DataTableColumns<TResource>): void {
    if (isNotNullNorUndefined(columns) && !columns.isSortable) {
      return;
    }
    if (isNotNullNorUndefined(columns)) {
      this.currentSort = this._generateNewSortObject(columns);
    }
    if (isNotNullNorUndefined(this.currentSort)) {
      this._computeDataPaginatedInMemory();
    }
  }

  private _computeDataPaginatedInMemory(): void {
    if (this.currentSort?.order === OrderEnum.ascending) {
      this.data = ArrayUtil.sortAscendant([...this.data], this.currentSort.field);
    } else if (this.currentSort?.order === OrderEnum.descending) {
      this.data = ArrayUtil.sortDescendant([...this.data], this.currentSort.field);
    }
    const startPage = this.queryParameters.paging.pageIndex * this.queryParameters.paging.pageSize;
    const pageSize = this.queryParameters.paging.pageSize;
    let endPage = startPage + pageSize;
    if (endPage > this.queryParameters.paging.length) {
      endPage = this.queryParameters.paging.length;
    }
    this.dataPaginatedInMemory = this.data.slice(startPage, endPage);
  }

  backendSort(columns: DataTableColumns<TResource>): void {
    if (isTrue(this.skipInitialQuery)) {
      this.skipInitialQuery = false;
      return;
    }
    if (isNotNullNorUndefined(columns) && !columns.isSortable) {
      return;
    }
    const queryParameters = ObjectUtil.clone(this.queryParameters);
    if (isNotNullNorUndefined(columns)) {
      this.currentSort = this._generateNewSortObject(columns);
    }
    if (isNotNullNorUndefined(this.currentSort)) {
      queryParameters.sort = this.currentSort;
    } else {
      queryParameters.sort = undefined;
    }
    this._queryParametersBS.next(queryParameters);
  }

  getSortIcon(columns: DataTableColumns<TResource>): IconNamePartialEnum {
    if (isNullOrUndefined(this.currentSort)) {
      return IconNamePartialEnum.sortUndefined;
    }
    if (this._getSortableField(columns) !== this.currentSort.field) {
      return IconNamePartialEnum.sortUndefined;
    }
    return this.currentSort.order === OrderEnum.ascending ? IconNamePartialEnum.sortAscending : IconNamePartialEnum.sortDescending;
  }

  private _generateNewSortObject(column: DataTableColumns<TResource>): Sort {
    const newSortableField = this._getSortableField(column);
    let newOrder = OrderEnum.ascending;
    if (this.currentSort?.field === newSortableField) {
      newOrder = this.currentSort?.order === OrderEnum.ascending ? OrderEnum.descending : OrderEnum.ascending;
    }
    return {
      field: newSortableField,
      order: newOrder,
    } as Sort;
  }

  pageChange($event: Paging): void {
    this.resetCounterItemSelectedOnCurrentPage();
    this.resetBannerSelectAll();
    this.queryParameters.paging = $event;
    this._queryParametersBS.next(this.queryParameters);
    this._scrollToTop();
  }

  private _scrollToTop(): void {
    if (isTrue(this.scrollToTopWhenPaginate)) {
      this._scrollService.scrollToTop();
    }
  }

  searchChange(col: DataTableColumns, value: string | BaseResource): void {
    if (this.multiSelectionValueOnAllPages.length > 0) {
      this.subscribe(
        DialogUtil.open(this._dialog, DataTableLooseSelectionDialog, {
            numberItemSelected: this.multiSelectionValueOnAllPages.length,
          }, undefined,
          result => this._searchChange(col, value),
          () => this._rollbackChangeOnFilter(col)),
      );
    } else {
      this._searchChange(col, value);
    }
  }

  private _searchChange(col: DataTableColumns, value: string | BaseResource | boolean): void {
    this.multiSelectionValueOnAllPages = [];
    this.resetCounterItemSelectedOnCurrentPage();
    this.resetBannerSelectAll();

    const fieldName = this._getFilterableField(col);
    if (isEmptyString(value) || isNullOrUndefined(value)) {
      MappingObjectUtil.delete(QueryParametersUtil.getSearchItems(this.queryParameters), fieldName);
    } else {
      if (col.type === DataTableFieldTypeEnum.searchableSingleSelect) {
        const valueKey = isNullOrUndefined(col.resourceValueKey) ? "resId" : col.resourceValueKey;
        value = (value as BaseResource)[valueKey];
      } else if (col.type === DataTableFieldTypeEnum.boolean) {
        value = col.invertedBooleanValue ? !value : value;
      }
      MappingObjectUtil.set(QueryParametersUtil.getSearchItems(this.queryParameters), fieldName, value);
    }
    const queryParameters = QueryParametersUtil.resetToFirstPage(this.queryParameters);
    this._queryParametersBS.next(queryParameters);
  }

  private _initFiltersValues(): void {
    if (isNullOrUndefined(this.columns) || isNullOrUndefined(this.queryParameters)) {
      return;
    }
    MappingObjectUtil.forEach(QueryParametersUtil.getSearchItems(this.queryParameters), (value, key) => {
      const column = this.columns.find(c => c.isFilterable && (c.filterableField === key || isNullOrUndefinedOrWhiteString(c.filterableField) && c.field === key));
      if (isNotNullNorUndefined(column)) {
        this.getFilterFormControl(column)?.setValue(value, {
          emitEvent: false,
        });
      }
    });
  }

  private _rollbackChangeOnFilter(col: DataTableColumns<TResource>): void {
    col.isFilterable = false;
    this._changeDetector.detectChanges();
    setTimeout(() => {
      col.isFilterable = true;
      this._changeDetector.detectChanges();
    }, 0);
  }

  getValue(col: DataTableColumns<TResource>): string | null {
    const value = MappingObjectUtil.get(this.queryParameters.search.searchItems, this._getFilterableField(col));
    return isNonEmptyString(value) ? value : null;
  }

  private _getFilterableField(col: DataTableColumns<TResource>): string {
    return isString(col.filterableField as string) ? col.filterableField as string : col.field as string;
  }

  private _getSortableField(col: DataTableColumns): string | undefined {
    return isString(col.sortableField as string) ? col.sortableField as string : col.field as string;
  }

  getSortableFieldByFieldName(fieldName: string): string {
    const column = this.columns.find(col => col.field === fieldName);
    return this._getSortableField(column);
  }

  isFilterApplied(): boolean {
    let toSkip: boolean = false;
    if (MappingObjectUtil.size(QueryParametersUtil.getSearchItems(this.queryParameters)) > 0) {
      if (this.columnsSkippedToClear === undefined || isEmptyArray(this.columnsSkippedToClear)) {
        return true;
      }
      const searchItems: Map<string, string> = MappingObjectUtil.toMap(this.queryParameters.search.searchItems);
      searchItems.forEach((value: string, key: string) => {
        if (!toSkip && this.columnsSkippedToClear.some(c => c.includes(key)) === false) {
          toSkip = true;
        }
      });
    }
    return toSkip;
  }

  clearFilters(): void {
    if (this.multiSelectionValueOnAllPages.length > 0) {
      this.subscribe(DialogUtil.open(this._dialog, DataTableLooseSelectionDialog, {
        numberItemSelected: this.multiSelectionValueOnAllPages.length,
      }, undefined, result => this._clearFilters()));
    } else {
      this._clearFilters();
    }
  }

  private _clearFilters(): void {
    this.multiSelectionValueOnAllPages = [];
    this.resetCounterItemSelectedOnCurrentPage();
    this.resetBannerSelectAll();
    this._cleanQueryParam();
    this._cleanFilterComponent();
  }

  private _cleanQueryParam(): void {
    let queryParameters = QueryParametersUtil.clone(this.queryParameters);
    const searchItems = QueryParametersUtil.getSearchItems(queryParameters);
    if (this.columnsSkippedToClear === undefined || isEmptyArray(this.columnsSkippedToClear)) {
      MappingObjectUtil.clear(searchItems);
    } else {
      MappingObjectUtil.forEach(searchItems, (value: string, key: string) => {
        if (this.columnsSkippedToClear.some(c => c.includes(key)) === false) {
          MappingObjectUtil.delete(searchItems, key);
        }
      });
    }
    queryParameters = QueryParametersUtil.resetToFirstPage(queryParameters);
    this._queryParametersBS.next(queryParameters);
  }

  private _cleanFilterComponent(): void {
    this.columns.forEach(col => {
      if (!col.isFilterable || this.columnsSkippedToClear.includes(col.field as string)) {
        return;
      }
      const fc = this.getFilterFormControl(col);
      fc?.reset();
    });
    this._forceRefreshFilter();
  }

  private _forceRefreshFilter(): void {
    this.displayFilters = false;
    this._changeDetector.detectChanges();
    this.displayFilters = true;
    this._changeDetector.detectChanges();
  }

  callCallback($event: MouseEvent | KeyboardEvent, callback: (current: TResource) => void, rowData: TResource, stopPropagation: boolean = true): void {
    if (isTrue(stopPropagation)) {
      this.avoidOpen($event);
    }
    callback(rowData);
  }

  callMouseoverCallback($event: MouseEvent, callback: ($event: MouseEvent, current: TResource) => void, rowData: TResource): void {
    if (isFunction(callback)) {
      callback($event, rowData);
    }
  }

  isHighlighting(rowData: TResource): boolean {
    return this.listHighlightingId.includes(rowData.resId) || this.multiSelectionValueOnAllPages.findIndex(s => s.resId === rowData.resId) !== -1
      || (isNotNullNorUndefined(this.isHighlightCondition) && this.isHighlightCondition(rowData));
  }

  isHighlightingCell(dataTableColumns: DataTableColumns<TResource>, rowData: TResource): boolean {
    if (!isFunction(dataTableColumns.isHighlightCell)) {
      return false;
    }
    return dataTableColumns.isHighlightCell(rowData);
  }

  displayActionOnCondition(action: DataTableActions<TResource>, resource: TResource = undefined): boolean {
    if (!resource || !action.displayOnCondition) {
      return this.atLeastOneActionRowDisplayed(action);
    }
    return action.displayOnCondition(resource);
  }

  displayActionsWrapperOnCondition(actions: DataTableActions<TResource>[], resource: TResource): boolean {
    return actions.some(action => isFunction(action.displayOnCondition) && action.displayOnCondition(resource));
  }

  private atLeastOneActionRowDisplayed(action: DataTableActions<TResource>): boolean {
    if (!isNonEmptyArray(this._data)) {
      return false;
    }
    if (isNullOrUndefined(action.displayOnCondition)) {
      return true;
    }
    return this.context.mapAtLeastOneActionToDisplay.get(action);
  }

  getWidth(column: DataTableColumns<TResource>): string {
    if (isString(column.width)) {
      return column.width;
    }
    if (isNotNullNorUndefined(column.component?.width)) {
      return column.component?.width;
    }
    if (column.type === DataTableFieldTypeEnum.datetime) {
      return "175px";
    }
    if (column.type === DataTableFieldTypeEnum.date) {
      return "155px";
    }

    return undefined;
  }

  getMinWidth(column: DataTableColumns<TResource>): string {
    if (isString(column.minWidth)) {
      return column.minWidth;
    }
    return undefined;
  }

  getMaxWidth(column: DataTableColumns<TResource>): string {
    if (isString(column.maxWidth)) {
      return column.maxWidth;
    }
    return undefined;
  }

  getAlignment(column: DataTableColumns<TResource>): "left" | "center" | "right" {
    if (isString(column.alignment)) {
      return column.alignment;
    }
    if (column.type === DataTableFieldTypeEnum.number
      || column.type === DataTableFieldTypeEnum.size) {
      return "right";
    }
    if (column.type === DataTableFieldTypeEnum.boolean) {
      return "center";
    }
    return "left";
  }

  isColumnFilterable(): boolean {
    return this.columns.some(c => c.isFilterable);
  }

  isColumnValueInverted(c: DataTableColumns): boolean {
    return isNotNullNorUndefined(c.invertedBooleanValue) && c.invertedBooleanValue === true;
  }

  toggleCheckbox($event: MatCheckboxChange, rowData: TResource): void {
    const listRowSelected = this._multiSelectionBS.value;
    if ($event.checked) {
      listRowSelected.push(rowData);
      this.multiSelectionValueOnCurrentPageCounter++;
    } else {
      const index = listRowSelected.findIndex(r => r.resId === rowData.resId);
      if (index === -1) {
        return;
      }
      this.multiSelectionValueOnCurrentPageCounter--;
      listRowSelected.splice(index, 1);
    }
    this._multiSelectionBS.next(listRowSelected);
  }

  avoidOpen($event: MouseEvent | KeyboardEvent): void {
    $event.stopPropagation();
  }

  toggleCurrentPageCheckbox($event: MatCheckboxChange): void {
    const listRowSelectedOnAllPages = this.multiSelectionValueOnAllPages;
    this.data.forEach((item: TResource) => {
      const indexOf = listRowSelectedOnAllPages.findIndex(r => r.resId === item.resId);
      if ($event.checked) {
        this.displayPropositionSelectAllResult = true;
        if (indexOf !== -1) {
          return;
        }
        listRowSelectedOnAllPages.push(item);
      } else {
        this.resetBannerSelectAll();
        if (indexOf === -1) {
          return;
        }
        listRowSelectedOnAllPages.splice(indexOf, 1);
      }
    });
    this._multiSelectionBS.next(listRowSelectedOnAllPages);
    this.computeNumberItemSelectedOnCurrentPage();
    if ($event.checked && this.data.length === this.queryParameters.paging.length) {
      this.resetBannerSelectAll();
    }
  }

  isLineSelected(rowData: TResource): boolean {
    return this.multiSelectionValueOnAllPages.findIndex(m => m.resId === rowData.resId) !== -1;
  }

  selectAllResult(): Observable<string[]> {
    if (this.queryParameters.paging.length > this.environment.maximalPageSizeToRetrievePaginationInfo) {
      this._notificationService.showInformation(this.labelTranslate.dataTableNotificationOnlyTheMaximalAmountAreSelected, {maximalNumber: this.environment.maximalPageSizeToRetrievePaginationInfo} as any);
    }
    this.pendingRetrieveAllForSelection = true;
    const queryParameters = QueryParametersUtil.clone(this.queryParameters);
    queryParameters.paging = new Paging(this.environment.maximalPageSizeToRetrievePaginationInfo);
    this._queryParametersBS.next(queryParameters);
    this._queryParametersWithoutRefreshBS.next(QueryParametersUtil.clone(this.queryParameters));
    return this.multiSelectionObs.pipe(
      map(list => list.map(m => m.resId)),
    );
  }

  cleanSelection(): void {
    this.resetBannerSelectAll();
    this.multiSelectionValueOnAllPages = [];
    this.resetCounterItemSelectedOnCurrentPage();
    this._changeDetector.detectChanges();
  }

  callBulkAction(bulkAction: DataTableBulkActions<TResource>): void {
    const result = bulkAction.callback(this.multiSelectionValueOnAllPages);
    if (isObservable(result)) {
      if (isInstanceOf(result, Observable<boolean>)) {
        this.subscribe(result.pipe(take(1)), confirmed => {
          if (isTrue(confirmed)) {
            this.cleanSelection();
          }
        });
      } else {
        this.subscribe(result.pipe(take(1)), () => {
          this.cleanSelection();
        });
      }
    } else {
      if (isBoolean(result)) {
        if (isTrue(result)) {
          this.cleanSelection();
        }
      } else {
        this.cleanSelection();
      }
    }
  }

  dragStart($event: CdkDragStart<TResource>): void {
    this.cleanSelection();
    this._dragStartBS.next($event.source.data);
  }

  dragEnd($event: CdkDragEnd<TResource>): void {
    this._dragEndBS.next($event.source.data);
  }

  cleanFilter(formControl: FormControl): void {
    formControl.setValue(undefined);
  }

  isDisabledAction(action: DataTableActions<TResource>, rowData: TResource): ObservableOrPromiseOrValue<boolean> {
    if (isNullOrUndefined(action.disableCondition)) {
      return false;
    }
    return action.disableCondition(rowData);
  }

  isVisible(action: DataTableActions<TResource>): ObservableOrPromiseOrValue<boolean> {
    if (isNullOrUndefined(action.isVisible)) {
      return false;
    }
    return action.isVisible;
  }

  calculateClasses(action: DataTableActions<TResource>): any {
    return {
      "action-button": true,
      visible: this.isVisible(action),
    };
  }

  // Empty data
  private static readonly _emptyData: {} = {};

  static isEmptyData(value: any): boolean {
    return this._emptyData === value;
  }

  private _getEmptyData(): {} {
    return DataTablePresentational._emptyData;
  }

  isEmptyData(value: any): boolean {
    return DataTablePresentational.isEmptyData(value);
  }

  getTooltip(col: DataTableColumns, value: string): string {
    if (isNullOrUndefined(col.tooltip)) {
      return value;
    }
    return col.tooltip(value);
  }

  refresh(): void {
    this._queryParametersBS.next(this.queryParameters);
  }

  preventDefault($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
  }

  setForceHoverRowData(rowData: TResource | undefined): void {
    this.forceHoverRowData = rowData;
  }

  dropToSort(event: CdkDragDrop<any>): void {
    if (!this.isDroppable) {
      return;
    }
    const data = [...this.data];
    moveItemInArray(data, event.previousIndex, event.currentIndex);
    this.data = data;
    this._dragEndSendDataBS.next(this.data);
  }

  isInstanceOfKeyValue(key: KeyValue | BaseResourceWithLabels): key is KeyValue {
    return isNotNullNorUndefined(filter) && isString((key as KeyValue).key) && isString((key as KeyValue).value);
  }

  getFilteredEnumSorted(col: DataTableColumns): KeyValue[] {
    let keyValues: KeyValue[] = [];
    const listFiltersEnum = col.filterEnum;
    if (!isNonEmptyArray(listFiltersEnum)) {
      return [];
    }
    const firstFilter = listFiltersEnum[0];
    if (this.isInstanceOfKeyValue(firstFilter)) {
      keyValues = listFiltersEnum.map(item => ({
        key: item.key,
        value: col.translate ? this._translateService.instant(item.value) : item.value,
      }));
    } else {
      keyValues = listFiltersEnum.map((item: BaseResourceWithLabels) => ({
        key: item.resId,
        value: LabelUtil.getTranslationFromListLabelsWithStore(this._store, this.environment, item.labels),
      }));
    }
    return ArrayUtil.sortAscendant(keyValues, "value");
  }
}

class Context {
  mapAtLeastOneActionToDisplay: Map<DataTableActions, boolean>;
}
