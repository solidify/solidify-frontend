/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - label-translate-data-table.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MARK_AS_TRANSLATABLE} from "../core-resources/utils/translate.util";

export interface LabelSolidifyDataTable {
  dataTableAll?: string;
  dataTableCopyIdToClipboard?: string;
  dataTableAllElementSearchedSelected?: string;
  dataTableCleanAllFilter?: string;
  dataTableClearSelection?: string;
  dataTableCurrentPageElementSelected?: string;
  dataTableDialogLooseSelectionBody?: string;
  dataTableDialogLooseSelectionTitle?: string;
  dataTableNoData?: string;
  dataTableNotificationOnlyTheMaximalAmountAreSelected?: string;
  dataTableSelectAllElement?: string;
  dataTableMoreOptions?: string;
  dataTableShowHistoryStatus?: string;
}

export const labelSolidifyDataTable: LabelSolidifyDataTable = {
  dataTableAll: MARK_AS_TRANSLATABLE("solidify.dataTable.all"),
  dataTableCopyIdToClipboard: MARK_AS_TRANSLATABLE("solidify.dataTable.copyIdToClipboard"),
  dataTableAllElementSearchedSelected: MARK_AS_TRANSLATABLE("solidify.dataTable.allElementSearchedSelected"),
  dataTableCleanAllFilter: MARK_AS_TRANSLATABLE("solidify.dataTable.cleanAllFilter"),
  dataTableClearSelection: MARK_AS_TRANSLATABLE("solidify.dataTable.clearSelection"),
  dataTableCurrentPageElementSelected: MARK_AS_TRANSLATABLE("solidify.dataTable.currentPageElementSelected"),
  dataTableDialogLooseSelectionBody: MARK_AS_TRANSLATABLE("solidify.dataTable.dialog.looseSelection.body"),
  dataTableDialogLooseSelectionTitle: MARK_AS_TRANSLATABLE("solidify.dataTable.dialog.looseSelection.title"),
  dataTableNoData: MARK_AS_TRANSLATABLE("solidify.dataTable.noData"),
  dataTableNotificationOnlyTheMaximalAmountAreSelected: MARK_AS_TRANSLATABLE("solidify.dataTable.notificationOnlyTheMaximalAmountAreSelected"),
  dataTableSelectAllElement: MARK_AS_TRANSLATABLE("solidify.dataTable.selectAllElement"),
  dataTableMoreOptions: MARK_AS_TRANSLATABLE("solidify.dataTable.moreOptions"),
  dataTableShowHistoryStatus: MARK_AS_TRANSLATABLE("solidify.dataTable.showHistoryStatus"),
};
