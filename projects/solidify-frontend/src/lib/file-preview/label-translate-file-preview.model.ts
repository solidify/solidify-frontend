/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - label-translate-file-preview.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MARK_AS_TRANSLATABLE} from "../core-resources/utils/translate.util";

export interface LabelSolidifyFilePreview {
  filePreviewNotificationPreviewDownloadFail?: string;
  filePreviewNotificationPreviewFileContentTypeNotSupported?: string;
  filePreviewNotificationPreviewFileNotSupported?: string;
  filePreviewNotificationPreviewFileTooBig?: string;
  filePreviewNotificationPreviewFileStatusDoesNotAllowIt?: string;
  filePreviewPreviewDialogTitle?: string;
  filePreviewVisualizationDocxPreviewMessageBanner?: string;
  filePreviewZoomIn?: string;
  filePreviewZoomOut?: string;
  filePreviewZoomVal?: string;
}

export const labelSolidifyFilePreview: LabelSolidifyFilePreview = {
  filePreviewNotificationPreviewDownloadFail: MARK_AS_TRANSLATABLE("solidify.filePreview.notification.previewDownloadFail"),
  filePreviewNotificationPreviewFileContentTypeNotSupported: MARK_AS_TRANSLATABLE("solidify.filePreview.notification.previewFileContentTypeNotSupported"),
  filePreviewNotificationPreviewFileNotSupported: MARK_AS_TRANSLATABLE("solidify.filePreview.notification.previewFileNotSupported"),
  filePreviewNotificationPreviewFileTooBig: MARK_AS_TRANSLATABLE("solidify.filePreview.notification.previewFileTooBig"),
  filePreviewNotificationPreviewFileStatusDoesNotAllowIt: MARK_AS_TRANSLATABLE("solidify.filePreview.notification.previewFileStatusDoesNotAllowIt"),
  filePreviewPreviewDialogTitle: MARK_AS_TRANSLATABLE("solidify.filePreview.dialog.title"),
  filePreviewVisualizationDocxPreviewMessageBanner: MARK_AS_TRANSLATABLE("solidify.filePreview.visualizationDocxPreviewMessageBanner"),
  filePreviewZoomIn: MARK_AS_TRANSLATABLE("solidify.filePreview.zoomIn"),
  filePreviewZoomOut: MARK_AS_TRANSLATABLE("solidify.filePreview.zoomOut"),
  filePreviewZoomVal: MARK_AS_TRANSLATABLE("solidify.filePreview.zoomVal"),
};
