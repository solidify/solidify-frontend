/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - csv-file-visualizer.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ComponentFactoryResolver,
  Inject,
  Injectable,
  NgZone,
} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";
import {TranslateService} from "@ngx-translate/core";
import {MappingObjectUtil} from "../../core-resources/utils/mapping-object.util";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../core/injection-tokens/label-to-translate.injection-token";
import {LabelTranslateInterface} from "../../label-translate-interface.model";
import {AddExternalComponentDirective} from "../directives/add-external-component.directive";
import {FileInput} from "../models/file-info.model";
import {AbstractFileVisualizer} from "./abstract-file-visualizer.service";

@Injectable({
  providedIn: "root",
})
export class CsvFileVisualizerService extends AbstractFileVisualizer {
  isFullScreenSupported: boolean = true;
  isZoomSupported: boolean = false;
  divContainer: HTMLDivElement;
  type: string = "csv";

  private _csvSeparator: string;
  private _endChar: string;

  private readonly _COMPONENT_CANVAS_DATAGRID: string = "canvas-datagrid";
  private readonly _ATTRIBUTE_TAB_INDEX: string = "tab-index";
  private readonly _ATTRIBUTE_EDITABLE: string = "editable";
  private readonly _CLASS_HIDE: string = "hide";

  constructor(private readonly _componentFactoryResolver: ComponentFactoryResolver,
              private readonly _ngZone: NgZone,
              private readonly _domSanitizer: DomSanitizer,
              private readonly _translateService: TranslateService,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface,
  ) {
    super(_environment,
      _environment.visualizationCsvPreviewExtensions,
      _environment.visualizationCsvPreviewContentType,
      _environment.visualizationCsvPreviewMimeType,
      _environment.visualizationCsvPreviewPronomId,
      _environment.visualizationCsvPreviewMaxFileSizeInMegabytes);
  }

  isVisualizationOnGoing(fileInfo: FileInput, domElement: Element): boolean {
    return false;
  }

  closeVisualizer(fileInfo: FileInput, domElement: Element): void {
    domElement.remove();
    this.divContainer = null;
  }

  openVisualizer(fileInfo: FileInput, domElement: Element, addExternalComponent?: AddExternalComponentDirective): void {
    this.divContainer = SsrUtil.window?.document.createElement("div");
    this._computeCsvSeparator(fileInfo);
    const reader = new FileReader();
    reader.onloadend = (evt) => {
      if (evt.target.readyState === FileReader.DONE) {
        const arrayBuffer = evt.target.result;
        this._treatWorkBook(arrayBuffer as string);
      }
    };
    reader.readAsText(fileInfo.blob);
    domElement.appendChild(this.divContainer);
  }

  private _treatWorkBook(arrayBuffer: string | null): void {
    const data = [];
    this._computeEndChar(arrayBuffer);
    const listLines = arrayBuffer.split(this._endChar);
    const header = listLines[0];
    let headerSplit = header.split(this._csvSeparator);
    headerSplit = this._avoidDuplicateColumnName(headerSplit);

    let listWithoutHeader = ["\n"];
    if (listLines.length > 1) {
      listWithoutHeader = listLines.slice(1);
    }
    listWithoutHeader.forEach((line, indexLine) => {
      if (indexLine !== 0 && indexLine === listWithoutHeader.length - 1 && line === "\n") {
        return;
      }
      const dataEntry = {};
      const lineSplittedByColumn = line.split(this._csvSeparator);
      headerSplit.forEach(((col, indexColumn) => {
        MappingObjectUtil.set(dataEntry, col, line === "\n" ? "" : lineSplittedByColumn[indexColumn]);
      }));
      data.push(dataEntry);
    });
    const grid = document.createElement(this._COMPONENT_CANVAS_DATAGRID);
    grid["data"] = data;
    grid.setAttribute(this._ATTRIBUTE_EDITABLE, "false");
    grid[this._ATTRIBUTE_EDITABLE] = false;
    this.divContainer.appendChild(grid);
  }

  private _computeCsvSeparator(fileInfo: FileInput): void {
    this._csvSeparator = ";";
    if (fileInfo.dataFile?.mimetype === "text/tab-separated-values") {
      this._csvSeparator = "\t";
    }
  }

  private _computeEndChar(arrayBuffer: string | null): void {
    this._endChar = "\n";
    if (arrayBuffer.indexOf("\r") >= 0) {
      this._endChar = "\r";
    }
  }

  private _avoidDuplicateColumnName(header: string[]): string[] {
    const newHeader: string[] = [];
    header.forEach((colName) => {
      newHeader.push(this._getUniqueName(newHeader, colName));
    });
    return newHeader;
  }

  private _getUniqueName(newHeader: any[], colName: string, count: number = 0): string {
    const candidateColName = count === 0 ? colName : `${colName}_${count}`;
    if (newHeader.includes(candidateColName)) {
      return this._getUniqueName(newHeader, colName, count + 1);
    }
    return candidateColName;
  }

  doAction(fileInfo: FileInput, domElement: Element): void {
  }

  handleZoom(zoomEnable: boolean, fileInfo: FileInput, domElement: Element): void {
  }
}
