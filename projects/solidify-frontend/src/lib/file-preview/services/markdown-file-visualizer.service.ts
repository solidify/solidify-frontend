/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - markdown-file-visualizer.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import md from "markdown-it";
import {isTruthyObject} from "../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {FileInput} from "../models/file-info.model";
import {AbstractFileVisualizer} from "./abstract-file-visualizer.service";

@Injectable({
  providedIn: "root",
})
export class MarkdownFileVisualizerService extends AbstractFileVisualizer {
  isFullScreenSupported: boolean = true;
  isZoomSupported: boolean = false;
  type: string = "markdown";
  pre: HTMLPreElement = null;

  constructor(@Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_environment,
      _environment.visualizationMarkdownExtensions,
      _environment.visualizationMarkdownContentType,
      _environment.visualizationMarkdownMimeType,
      _environment.visualizationMarkdownPronomId,
      _environment.visualizationMarkdownMaxFileSizeInMegabytes);
  }

  override canHandle(fileInfo: FileInput): boolean {
    if (!super.canHandle(fileInfo)) {
      return false;
    }
    const canHandleByExtension = this.canHandleByExtension(fileInfo);
    const canHandleByMimeTypeContentType = this.canHandleByMimeTypeContentType(fileInfo);
    const canHandleByPuid = this.canHandleByPuid(fileInfo);
    // Force that all criteria match to avoid conflict between docx, xlsx and pptx (have same mime type and same Puid)
    return (canHandleByExtension && canHandleByMimeTypeContentType) || canHandleByPuid;
  }

  isVisualizationOnGoing(fileInfo: FileInput, domElement: Element): boolean {
    return isTruthyObject(this.pre);
  }

  closeVisualizer(fileInfo: FileInput, domElement: Element): void {
    domElement.remove();
    this.pre = null;
  }

  openVisualizer(fileInfo: FileInput, domElement: Element): void {
    this.pre = SsrUtil.window?.document.createElement("pre");
    this.pre.style.display = "block";
    this.pre.style.lineHeight = "1.42857143";
    this.pre.style.fontSize = "13px";
    this.pre.style.margin = "0 0 10px";
    this.pre.style.padding = "9.5px";
    this.pre.style.color = "#333";
    this.pre.style.overflowWrap = "break-word";
    this.pre.style.wordWrap = "break-word";
    this.pre.style.whiteSpace = "break-spaces";
    this.pre.style.backgroundColor = "#f5f5f5";
    this.pre.style.border = "1px solid #ccc";
    this.pre.style.borderRadius = "4px";

    const reader = new FileReader();
    reader.onloadend = (evt) => {
      if (evt.target.readyState === FileReader.DONE) {
        const arrayBuffer = evt.target.result;
        const decoder = new TextDecoder();
        const fileText = decoder.decode(arrayBuffer as any);
        const markdownIt = md();
        try {
          this.pre.innerHTML = markdownIt.render(fileText);
        } catch (e) {
          this.pre.innerText = fileText;
        }
      }
    };
    reader.readAsArrayBuffer(fileInfo.blob);
    domElement.appendChild(this.pre);
  }

  doAction(fileInfo: FileInput, domElement: Element): void {
  }

  handleZoom(zoomEnable: boolean, fileInfo: FileInput, domElement: Element): void {
  }
}
