/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - video-file-visualizer.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {Inject} from "@angular/core";
import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {isTruthyObject} from "../../core-resources/tools/is/is.tool";
import {FileInput} from "../models/file-info.model";
import {AbstractFileVisualizer} from "./abstract-file-visualizer.service";

export class VideoFileVisualizerService extends AbstractFileVisualizer {
  isFullScreenSupported: boolean = false;
  type: string = "video";
  isZoomSupported: boolean = false;
  video: HTMLVideoElement = null;

  constructor(@Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_environment,
      _environment.visualizationMovieExtensions,
      _environment.visualizationMovieContentType,
      _environment.visualizationMovieMimeType,
      _environment.visualizationMoviePronomId,
      _environment.visualizationMovieMaxFileSizeInMegabytes);
  }

  isVisualizationOnGoing(fileInfo: FileInput, domElement: Element): boolean {
    return isTruthyObject(this.video);
  }

  closeVisualizer(fileInfo: FileInput, domElement: Element): void {
    domElement.remove();
    this.video = null;
  }

  convertVideoToClassicalFormat(fileInfo: FileInput, domElement: Element): void {
    // TODO
  }

  needToConvert(fileInfo: FileInput, domElement: Element): boolean {
    return false;
  }

  openVisualizer(fileInfo: FileInput, domElement: Element): void {
    if (this.needToConvert(fileInfo, domElement)) {
      this.convertVideoToClassicalFormat(fileInfo, domElement);
    }
    this.video = new HTMLVideoElement();
    const url = URL.createObjectURL(fileInfo.blob);
    this.video.src = url;
    this.video.style.maxHeight = "100%";
    this.video.style.maxWidth = "100%";
    this.video.style.backgroundColor = "black";
    domElement.appendChild(this.video);
  }

  doAction(fileInfo: FileInput, domElement: Element): void {
  }

  handleZoom(zoomEnable: boolean, fileInfo: FileInput, domElement: Element): void {
  }
}
