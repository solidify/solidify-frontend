/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - video-classical-file-visualizer.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {isTruthyObject} from "../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {FileInput} from "../models/file-info.model";
import {AbstractFileVisualizer} from "./abstract-file-visualizer.service";

@Injectable({
  providedIn: "root",
})
export class VideoClassicalFileVisualizerService extends AbstractFileVisualizer {
  isFullScreenSupported: boolean = false;
  isZoomSupported: boolean = false;
  type: string = "video-classical";
  video: HTMLVideoElement = null;

  constructor(@Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_environment,
      _environment.visualizationClassicalMovieExtensions,
      _environment.visualizationClassicalMovieContentType,
      _environment.visualizationClassicalMovieMimeType,
      _environment.visualizationClassicalMoviePronomId,
      _environment.visualizationClassicalMovieMaxFileSizeInMegabytes);
  }

  isVisualizationOnGoing(fileInfo: FileInput, domElement: Element): boolean {
    return isTruthyObject(this.video);
  }

  closeVisualizer(fileInfo: FileInput, domElement: Element): void {
    domElement.remove();
    this.video = null;
  }

  openVisualizer(fileInfo: FileInput, domElement: Element): void {
    const url = URL.createObjectURL(fileInfo.blob);
    this.video = SsrUtil.window?.document.createElement("video");
    this.video.style.backgroundColor = "black";
    this.video.controls = true;
    this.video.autoplay = true;

    const source = SsrUtil.window?.document.createElement("source");
    source.src = url;
    source.type = fileInfo.dataFile.mimetype;
    this.video.appendChild(source);
    this.autoResizeElement();
    domElement.appendChild(this.video);
  }

  autoResizeElement(): void {
    this.video.style.maxHeight = "100%";
    this.video.style.maxWidth = "100%";
  }

  doAction(fileInfo: FileInput, domElement: Element): void {
  }

  handleZoom(zoomEnable: boolean, fileInfo: FileInput, domElement: Element): void {
  }
}
