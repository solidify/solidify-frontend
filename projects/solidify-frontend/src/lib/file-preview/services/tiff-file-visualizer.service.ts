/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - tiff-file-visualizer.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  Inject,
  Injectable,
} from "@angular/core";
import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {
  isNullOrUndefined,
  isTruthyObject,
} from "../../core-resources/tools/is/is.tool";
import {AddExternalComponentDirective} from "../directives/add-external-component.directive";
import {FileInput} from "../models/file-info.model";
import {AbstractFileVisualizer} from "./abstract-file-visualizer.service";

declare const Tiff: any;

@Injectable({
  providedIn: "root",
})
export class TiffFileVisualizerService extends AbstractFileVisualizer {
  isFullScreenSupported: boolean = true;
  isZoomSupported: boolean = true;
  private _zoomEnable: boolean;

  type: string = "tiff";

  canvas: HTMLCanvasElement = null;

  constructor(@Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_environment,
      _environment.visualizationTiffExtensions,
      _environment.visualizationTiffContentType,
      _environment.visualizationTiffMimeType,
      _environment.visualizationTiffPronomId,
      _environment.visualizationTiffMaxFileSizeInMegabytes);
  }

  isVisualizationOnGoing(fileInfo: FileInput, domElement: Element): boolean {
    return isTruthyObject(this.canvas);
  }

  closeVisualizer(fileInfo: FileInput, domElement: Element): void {
    domElement.remove();
    this.canvas = null;
  }

  openVisualizer(fileInfo: FileInput, domElement: Element, addExternalComponent?: AddExternalComponentDirective): void {
    fileInfo.blob.arrayBuffer().then(buffer => {
      const tiff = new Tiff({buffer: buffer});
      this.canvas = tiff.toCanvas();
      this.canvas.style.maxHeight = "100%";
      this.canvas.style.maxWidth = "100%";
      this.canvas.style.objectFit = "contain";
      domElement.appendChild(this.canvas);
      this.handleZoom(this._zoomEnable, fileInfo, domElement);
    });
  }

  doAction(fileInfo: FileInput, domElement: Element): void {
    if (domElement.classList.contains("zoom-in")) {
      domElement.removeChild(this.canvas);
      domElement.classList.remove("zoom-in");
      domElement.classList.add("zoom-out");
      this.canvas.style.height = "1000px";
      this.canvas.style.width = "1000px";
      domElement.appendChild(this.canvas);
      return;
    }
    if (domElement.classList.contains("zoom-out")) {
      domElement.removeChild(this.canvas);
      domElement.classList.remove("zoom-out");
      domElement.classList.add("zoom-in");
      this.canvas.style.height = "500px";
      this.canvas.style.width = "500px";
      domElement.appendChild(this.canvas);
      return;
    }
  }

  handleZoom(zoomEnable: boolean, fileInfo: FileInput, domElement: Element): void {
    this._zoomEnable = zoomEnable;
    if (isNullOrUndefined(this.canvas)) {
      return;
    }
    if (zoomEnable) {
      domElement.classList.add("zoom-in");
      this.doAction(fileInfo, domElement);
    } else {
      //remove zooms properties and resize image
      if (domElement.classList.contains("zoom-in") || domElement.classList.contains("zoom-out")) {
        domElement.removeChild(this.canvas);
        if (domElement.classList.contains("zoom-out")) {
          domElement.classList.remove("zoom-out");
        } else if (domElement.classList.contains("zoom-in")) {
          domElement.classList.remove("zoom-in");
        }
        this.canvas.style.maxHeight = "100%";
        this.canvas.style.maxWidth = "100%";
        this.canvas.style.width = null;
        this.canvas.style.height = null;
        domElement.appendChild(this.canvas);
      }
    }
  }
}
