/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - file-visualizer.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {CoreAbstractAngularElement} from "../../core/models/angular/core-abstract-angular-element.model";
import {AbstractFileVisualizer} from "./abstract-file-visualizer.service";
import {CsvFileVisualizerService} from "./csv-file-visualizer.service";
import {DocxPreviewVisualizerService} from "./docx-preview-file-visualizer.service";
import {ImageFileVisualizerService} from "./image-file-visualizer.service";
import {MarkdownFileVisualizerService} from "./markdown-file-visualizer.service";
import {PdfFileVisualizerWithToolBarService} from "./pdf-file-visualizer-with-tool-bar.service";
import {SoundFileVisualizerService} from "./sound-file-visualizer.service";
import {TextFileVisualizerService} from "./text-file-visualizer.service";
import {TiffFileVisualizerService} from "./tiff-file-visualizer.service";
import {VideoClassicalFileVisualizerService} from "./video-classical-file-visualizer.service";
import {XlsxFileVisualizerService} from "./xlsx-file-visualizer.service";

@Injectable({
  providedIn: "root",
})
export class FileVisualizerService extends CoreAbstractAngularElement {
  readonly listFileVisualizer: AbstractFileVisualizer[];

  constructor(readonly csvFileVisualizerService: CsvFileVisualizerService,
              readonly docxPreviewVisualizerService: DocxPreviewVisualizerService,
              readonly xlsxFileVisualizerService: XlsxFileVisualizerService,
              readonly textFileVisualizerService: TextFileVisualizerService,
              readonly soundFileVisualizerService: SoundFileVisualizerService,
              readonly pdfFileVisualizerWithToolBarService: PdfFileVisualizerWithToolBarService,
              readonly videoClassicalFileVisualizerService: VideoClassicalFileVisualizerService,
              readonly imageFileVisualizerService: ImageFileVisualizerService,
              readonly tiffFileVisualizerService: TiffFileVisualizerService,
              readonly markdownFileVisualizerService: MarkdownFileVisualizerService,
  ) {
    super();
    this.listFileVisualizer = [
      this.tiffFileVisualizerService,
      this.imageFileVisualizerService,
      this.videoClassicalFileVisualizerService,
      this.pdfFileVisualizerWithToolBarService,
      this.soundFileVisualizerService,
      this.markdownFileVisualizerService,
      this.csvFileVisualizerService,
      this.textFileVisualizerService,
      this.docxPreviewVisualizerService,
      this.xlsxFileVisualizerService,
    ];
  }
}
