/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - pdf-file-visualizer.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ComponentFactoryResolver,
  ComponentRef,
  Inject,
  Injectable,
  NgZone,
} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";
// ssr:comment:start
import {PdfViewerComponent} from "ng2-pdf-viewer";
// ssr:comment:end
import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {AddExternalComponentDirective} from "../directives/add-external-component.directive";
import {FileInput} from "../models/file-info.model";
import {AbstractFileVisualizer} from "./abstract-file-visualizer.service";

@Injectable({
  providedIn: "root",
})
export class PdfFileVisualizerService extends AbstractFileVisualizer {
  isFullScreenSupported: boolean = true;
  isZoomSupported: boolean = false;
  type: string = "pdf";

  // ssr:comment:start
  private _componentRef: ComponentRef<PdfViewerComponent>;
  private _pdfViewerComponent: PdfViewerComponent;
  // ssr:comment:end

  // ssr:uncomment:start
  // private _componentRef: ComponentRef<any>;
  // private _pdfViewerComponent: any;
  // ssr:uncomment:end

  constructor(private readonly _componentFactoryResolver: ComponentFactoryResolver,
              private readonly _ngZone: NgZone,
              private readonly _domSanitizer: DomSanitizer,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
  ) {
    super(_environment,
      _environment.visualizationPdfExtensions,
      _environment.visualizationPdfContentType,
      _environment.visualizationPdfMimeType,
      _environment.visualizationPdfPronomId,
      _environment.visualizationPdfMaxFileSizeInMegabytes);
  }

  isVisualizationOnGoing(fileInfo: FileInput, domElement: Element): boolean {
    return domElement.childNodes.item(0).nodeName === "PDF-VIEWER";
  }

  closeVisualizer(fileInfo: FileInput, domElement: Element): void {
    domElement.remove();
  }

  openVisualizer(fileInfo: FileInput, domElement: Element, addExternalComponent?: AddExternalComponentDirective): void {
    if (this.canHandle(fileInfo)) {
      // ssr:comment:start
      const componentFactory = this._componentFactoryResolver.resolveComponentFactory(PdfViewerComponent);
      // ssr:comment:end

      // ssr:uncomment:start
      // const componentFactory = undefined;
      // ssr:uncomment:end
      const addExternalComponentContainerRef = addExternalComponent.viewContainerRef;
      addExternalComponentContainerRef.clear();
      this._componentRef = addExternalComponentContainerRef.createComponent(componentFactory);
      const url = URL.createObjectURL(fileInfo.blob);
      this._pdfViewerComponent = this._componentRef.instance;
      this._pdfViewerComponent.src = url;
      this._pdfViewerComponent.zoom = 1;
      this._pdfViewerComponent.autoresize = true;
      this._pdfViewerComponent.renderText = true;
      this._pdfViewerComponent.showBorders = true;
      this._pdfViewerComponent.fitToPage = true;
      this._pdfViewerComponent.originalSize = true;
      this._pdfViewerComponent.ngOnInit();
      this._pdfViewerComponent.showAll = true;
      this._componentRef.changeDetectorRef.detectChanges();
      domElement.classList.add("zoom-in");
    }
  }

  doAction(fileInfo: FileInput, domElement: Element): void {
    if (domElement.classList.contains("zoom-in")) {
      this._pdfViewerComponent.zoom = 2;
      this._componentRef.changeDetectorRef.detectChanges();
      this._pdfViewerComponent.updateSize();
      this._componentRef.changeDetectorRef.detectChanges();
      domElement.classList.remove("zoom-in");
      domElement.classList.add("zoom-out");
      return;
    }
    if (domElement.classList.contains("zoom-out")) {
      this._pdfViewerComponent.zoom = 1;
      this._componentRef.changeDetectorRef.detectChanges();
      this._pdfViewerComponent.updateSize();
      this._componentRef.changeDetectorRef.detectChanges();
      domElement.classList.remove("zoom-out");
      domElement.classList.add("zoom-in");
      return;
    }
  }

  handleZoom(zoomEnable: boolean, fileInfo: FileInput, domElement: Element): void {
  }
}
