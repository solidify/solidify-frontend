/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-file-visualizer.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Inject} from "@angular/core";
import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {CoreAbstractAngularElement} from "../../core/models/angular/core-abstract-angular-element.model";
import {
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isTruthyObject,
} from "../../core-resources/tools/is/is.tool";
import {AddExternalComponentDirective} from "../directives/add-external-component.directive";
import {FileInput} from "../models/file-info.model";

export abstract class AbstractFileVisualizer extends CoreAbstractAngularElement {
  abstract isZoomSupported: boolean;
  abstract isFullScreenSupported: boolean;
  abstract type: string;

  protected constructor(@Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
                        protected readonly _extensions: string[],
                        protected readonly _contentType: string[],
                        protected readonly _mimeType: string[],
                        protected readonly _pronomId: string[],
                        protected readonly _maxFileSizeInMegabytes: number | undefined) {
    super();
  }

  /**
   * Allow to know if the viewer can handle the file based on extension, mime-type or PUID
   * The size is not taken into account here
   * @param fileInfo
   */
  canHandle(fileInfo: FileInput): boolean {
    if (!isTruthyObject(fileInfo)) {
      return false;
    } else {
      const canHandleByExtension = this.canHandleByExtension(fileInfo);
      const canHandleByMimeTypeContentType = this.canHandleByMimeTypeContentType(fileInfo);
      const canHandleByPuid = this.canHandleByPuid(fileInfo);
      return canHandleByExtension || canHandleByMimeTypeContentType || canHandleByPuid;
    }
  }

  abstract openVisualizer(fileInfo: FileInput, domElement: Element, addExternalComponent?: AddExternalComponentDirective): void;

  abstract closeVisualizer(fileInfo: FileInput, domElement: Element): void;

  /**
   * Allow to know if the viewer can handle the file based on extension only
   * The size is not taken into account here
   * @param fileInfo
   */
  canHandleByExtension(fileInfo: FileInput): boolean {
    if (!isTruthyObject(fileInfo)) {
      return false;
    } else {
      return isNotNullNorUndefinedNorWhiteString(this._getSupportedExtension(fileInfo));
    }
  }

  protected _getSupportedExtension(fileInfo: FileInput): string {
    return this._extensions.find(value => value === fileInfo.fileExtension);
  }

  /**
   * Allow to know if the viewer can handle the file based on mime-type only
   * The size is not taken into account here
   * @param fileInfo
   */
  canHandleByMimeTypeContentType(fileInfo: FileInput): boolean {
    if (!isTruthyObject(fileInfo)) {
      return false;
    } else {
      return this._contentType.some(value => value === fileInfo?.dataFile?.mimetype) || this._mimeType.some(value => value === fileInfo.dataFile.mimetype);
    }
  }

  /**
   * Allow to know if the viewer can handle the file based on puid only
   * The size is not taken into account here
   * @param fileInfo
   */
  canHandleByPuid(fileInfo: FileInput): boolean {
    if (!isTruthyObject(fileInfo)) {
      return false;
    } else {
      return this._pronomId.some(value => value === fileInfo.dataFile.puid);
    }
  }

  /**
   * Allow to know if the viewer can handle the file based on size only
   * The size is not taken into account here
   * @param fileInfo
   */
  canHandleBySize(fileInfo: FileInput): boolean {
    if (!isTruthyObject(fileInfo)) {
      return false;
    } else {
      return !this._isFileExceedLimit(fileInfo.dataFile.fileSize, this._maxFileSizeInMegabytes);
    }
  }

  abstract isVisualizationOnGoing(fileInfo: FileInput, domElement: Element): boolean;

  abstract doAction(fileInfo: FileInput, domElement: Element): void;

  abstract handleZoom(zoomEnable: boolean, fileInfo: FileInput, domElement: Element): void;

  protected _isFileExceedLimit(fileSize: number, sizeLimitInMegabytes: number | undefined): boolean {
    return (fileSize / (1024 * 1024)) > (isNullOrUndefined(sizeLimitInMegabytes) ? this._environment.visualizationDefaultMaxFileSizeInMegabytes : sizeLimitInMegabytes);
  }
}
