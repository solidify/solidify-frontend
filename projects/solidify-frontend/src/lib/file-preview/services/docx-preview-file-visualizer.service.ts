/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - docx-preview-file-visualizer.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ComponentFactoryResolver,
  Inject,
  Injectable,
  NgZone,
} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";
import {TranslateService} from "@ngx-translate/core";
// ssr:comment:start
import * as docxPreview from "docx-preview";
// ssr:comment:end
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../core/injection-tokens/label-to-translate.injection-token";
import {LabelTranslateInterface} from "../../label-translate-interface.model";
import {AddExternalComponentDirective} from "../directives/add-external-component.directive";
import {FileInput} from "../models/file-info.model";
import {AbstractFileVisualizer} from "./abstract-file-visualizer.service";

@Injectable({
  providedIn: "root",
})
export class DocxPreviewVisualizerService extends AbstractFileVisualizer {
  isFullScreenSupported: boolean = true;
  isZoomSupported: boolean = false;
  divContainer: HTMLDivElement;
  type: string = "docx";

  constructor(private readonly _componentFactoryResolver: ComponentFactoryResolver,
              private readonly _ngZone: NgZone,
              private readonly _domSanitizer: DomSanitizer,
              private readonly _translateService: TranslateService,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface,
  ) {
    super(_environment,
      _environment.visualizationDocxPreviewExtensions,
      _environment.visualizationDocxPreviewContentType,
      _environment.visualizationDocxPreviewMimeType,
      _environment.visualizationDocxPreviewPronomId,
      _environment.visualizationDocxPreviewMaxFileSizeInMegabytes);
  }

  isVisualizationOnGoing(fileInfo: FileInput, domElement: Element): boolean {
    return false;
  }

  closeVisualizer(fileInfo: FileInput, domElement: Element): void {
    domElement.remove();
    this.divContainer = null;
  }

  openVisualizer(fileInfo: FileInput, domElement: Element, addExternalComponent?: AddExternalComponentDirective): void {
    this.divContainer = SsrUtil.window?.document.createElement("div");
    const messageContainer = SsrUtil.window?.document.createElement("div");
    messageContainer.classList.add("message-banner");
    const message = this._translateService.instant(this.labelTranslate.filePreviewVisualizationDocxPreviewMessageBanner);
    messageContainer.appendChild(SsrUtil.window?.document.createTextNode(message));
    // ssr:comment:start
    docxPreview.renderAsync(fileInfo.blob, this.divContainer);
    // ssr:comment:end
    domElement.appendChild(messageContainer);
    domElement.appendChild(this.divContainer);
  }

  doAction(fileInfo: FileInput, domElement: Element): void {
  }

  handleZoom(zoomEnable: boolean, fileInfo: FileInput, domElement: Element): void {
  }
}
