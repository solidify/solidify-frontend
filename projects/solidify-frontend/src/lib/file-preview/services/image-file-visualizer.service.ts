/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - image-file-visualizer.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {isTruthyObject} from "../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {AddExternalComponentDirective} from "../directives/add-external-component.directive";
import {FileInput} from "../models/file-info.model";
import {AbstractFileVisualizer} from "./abstract-file-visualizer.service";

@Injectable({
  providedIn: "root",
})
export class ImageFileVisualizerService extends AbstractFileVisualizer {
  isFullScreenSupported: boolean = true;
  isZoomSupported: boolean = true;
  type: string = "image";

  img: HTMLImageElement = null;

  constructor(@Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_environment,
      _environment.visualizationImageExtensions,
      _environment.visualizationImageContentType,
      _environment.visualizationImageMimeType,
      _environment.visualizationImagePronomId,
      _environment.visualizationImageMaxFileSizeInMegabytes);
  }

  isVisualizationOnGoing(fileInfo: FileInput, domElement: Element): boolean {
    return isTruthyObject(this.img);
  }

  closeVisualizer(fileInfo: FileInput, domElement: Element): void {
    domElement.remove();
    this.img = null;
  }

  openVisualizer(fileInfo: FileInput, domElement: Element, addExternalComponent?: AddExternalComponentDirective): void {
    this.img = SsrUtil.window?.document.createElement("img");
    const blob = new Blob([fileInfo.blob], {type: fileInfo.blob.type === "image/svg" ? "image/svg+xml" : fileInfo.blob.type});
    const url = URL.createObjectURL(blob);
    this.img.src = url;
    this.img.style.maxHeight = "100%";
    this.img.style.maxWidth = "100%";
    this.img.style.objectFit = "contain";
    domElement.appendChild(this.img);
  }

  doAction(fileInfo: FileInput, domElement: Element): void {
    if (domElement.classList.contains("zoom-in")) {
      domElement.removeChild(this.img);
      domElement.classList.remove("zoom-in");
      domElement.classList.add("zoom-out");
      this.img.style.height = "1000px";
      this.img.style.width = "1000px";
      domElement.appendChild(this.img);
      return;
    }
    if (domElement.classList.contains("zoom-out")) {
      domElement.removeChild(this.img);
      domElement.classList.remove("zoom-out");
      domElement.classList.add("zoom-in");
      this.img.style.height = "500px";
      this.img.style.width = "500px";
      domElement.appendChild(this.img);
      return;
    }
  }

  handleZoom(zoomEnable: boolean, fileInfo: FileInput, domElement: Element): void {
    if (zoomEnable) {
      domElement.classList.add("zoom-in");
      this.doAction(fileInfo, domElement);
    } else {
      //remove zooms properties and resize image
      if (domElement.classList.contains("zoom-in") || domElement.classList.contains("zoom-out")) {
        domElement.removeChild(this.img);
        if (domElement.classList.contains("zoom-out")) {
          domElement.classList.remove("zoom-out");
        } else if (domElement.classList.contains("zoom-in")) {
          domElement.classList.remove("zoom-in");
        }
        this.img.style.maxHeight = "100%";
        this.img.style.maxWidth = "100%";
        this.img.style.width = null;
        this.img.style.height = null;
        domElement.appendChild(this.img);
      }
    }
  }
}
