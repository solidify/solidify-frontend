/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - sound-file-visualizer.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
// ssr:comment:start
import WaveSurfer from "wavesurfer.js";
// ssr:comment:end
import {
  isNullOrUndefined,
  isTruthyObject,
} from "../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {FileInput} from "../models/file-info.model";
import {AbstractFileVisualizer} from "./abstract-file-visualizer.service";

@Injectable({
  providedIn: "root",
})
export class SoundFileVisualizerService extends AbstractFileVisualizer {
  isFullScreenSupported: boolean = false;
  isZoomSupported: boolean = false;
  type: string = "sound";

  // ssr:comment:start
  wave: WaveSurfer = null;
  // ssr:comment:end

  // ssr:uncomment:start
  // wave: any = null;
  // ssr:uncomment:end

  constructor(@Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_environment,
      _environment.visualizationSoundExtensions,
      _environment.visualizationSoundContentType,
      _environment.visualizationSoundMimeType,
      _environment.visualizationSoundPronomId,
      _environment.visualizationSoundMaxFileSizeInMegabytes);
  }

  isVisualizationOnGoing(fileInfo: FileInput, domElement: Element): boolean {
    return isTruthyObject(this.wave);
  }

  closeVisualizer(fileInfo: FileInput, domElement: Element): void {
    this.wave.unAll();
    this.wave.stop();
    this.wave.destroy();
    this.wave = null;
  }

  openVisualizer(fileInfo: FileInput, domElement: Element): void {
    if (this.canHandle(fileInfo)) {
      const divContainer = SsrUtil.window?.document.createElement("div");
      divContainer.style.display = "grid";
      divContainer.style.gridTemplateRows = "auto auto auto";
      const waveContainer = SsrUtil.window?.document.createElement("div");
      waveContainer.id = "wave";
      waveContainer.style.overflow = "hidden";
      divContainer.appendChild(waveContainer);
      const timelineContainer = SsrUtil.window?.document.createElement("div");
      timelineContainer.style.display = "flex";
      timelineContainer.style.justifyContent = "center";
      const timeElement = SsrUtil.window?.document.createElement("span");
      timeElement.id = "time";
      timelineContainer.appendChild(timeElement);
      const buttonContainer = SsrUtil.window?.document.createElement("div");
      buttonContainer.style.display = "flex";
      buttonContainer.style.alignItems = "center";
      buttonContainer.style.justifyContent = "center";
      const matButton = SsrUtil.window?.document.createElement("button");
      matButton.onclick = (ev => this.wave.playPause());
      matButton.textContent = "Play | Pause";
      matButton.classList.add("mat-button");
      matButton.classList.add("mat-focus-indicator");
      matButton.classList.add("mat-button-base");
      matButton.classList.add("mat-primary");
      buttonContainer.appendChild(matButton);
      divContainer.appendChild(buttonContainer);
      divContainer.appendChild(timelineContainer);
      SsrUtil.window?.document.createElement("div");
      domElement.appendChild(divContainer);
      // ssr:comment:start
      this.wave = WaveSurfer.create({
        container: document.querySelector("#wave"),
        barWidth: 2,
        barHeight: 1,
        overflow: "hidden !important",
        waveColor: "violet",
        progressColor: "purple",
      } as any);
      // ssr:comment:end

      const url = URL.createObjectURL(fileInfo.blob);
      this.wave.load(url);
      this.wave.on("ready", () => {
        this.wave.play();
      });

      this.wave.on("audioprocess", () => this._displayProgressionTime());
      this.wave.on("pause", () => this._displayProgressionTime());
    }
  }

  private _displayProgressionTime(): void {
    const totalTime = this.wave.getDuration();
    const currentTime = this.wave.getCurrentTime();
    const timeElem = SsrUtil.window?.document.getElementById("time");
    if (isNullOrUndefined(timeElem)) {
      return;
    }
    timeElem.innerText = this.getMilliSecondDisplay(Math.round(currentTime * 1000)) + "/" + this.getMilliSecondDisplay(Math.round(totalTime * 1000));
  }

  private getMilliSecondDisplay(millisec: number): string {
    let seconds: any = (millisec / 1000).toFixed(0);
    let minutes: any = Math.floor(seconds / 60);
    let hours: any = "";
    if (minutes > 59) {
      hours = Math.floor(minutes / 60);
      hours = (hours >= 10) ? hours : "0" + hours;
      minutes = minutes - (hours * 60);
      minutes = (minutes >= 10) ? minutes : "0" + minutes;
    }
    seconds = Math.floor(seconds % 60);
    seconds = (seconds >= 10) ? seconds : "0" + seconds;
    if (hours !== "") {
      return hours + ":" + minutes + ":" + seconds;
    }
    return minutes + ":" + seconds;
  }

  doAction(fileInfo: FileInput, domElement: Element): void {
  }

  handleZoom(zoomEnable: boolean, fileInfo: FileInput, domElement: Element): void {
  }
}
