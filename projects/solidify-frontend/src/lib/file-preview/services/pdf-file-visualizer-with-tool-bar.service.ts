/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - pdf-file-visualizer-with-tool-bar.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ComponentFactoryResolver,
  ComponentRef,
  Inject,
  Injectable,
  NgZone,
} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";
// ssr:comment:start
import {PdfViewerComponent} from "ng2-pdf-viewer";
// ssr:comment:end
import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {ViewerToolBarPresentational} from "../components/presentationals/viewer-tool-bar/viewer-tool-bar.presentational";
import {AddExternalComponentDirective} from "../directives/add-external-component.directive";
import {FileInput} from "../models/file-info.model";
import {AbstractFileVisualizer} from "./abstract-file-visualizer.service";

@Injectable({
  providedIn: "root",
})
export class PdfFileVisualizerWithToolBarService extends AbstractFileVisualizer implements AbstractFileVisualizer {
  isFullScreenSupported: boolean = true;
  isZoomSupported: boolean = false;
  type: string = "pdf-with-toolbar";

  private _componentRef: ComponentRef<ViewerToolBarPresentational>;
  private _toolBarPdfViewerComponent: ViewerToolBarPresentational;

  // ssr:comment:start
  private _pdfViewerComponentRef: ComponentRef<PdfViewerComponent>;
  private _pdfViewerComponent: PdfViewerComponent;
  // ssr:comment:end

  // ssr:uncomment:start
  // private _pdfViewerComponentRef: ComponentRef<any>;
  // private _pdfViewerComponent: any;
  // ssr:uncomment:end

  constructor(private readonly _componentFactoryResolver: ComponentFactoryResolver,
              private readonly _ngZone: NgZone,
              private readonly _domSanitizer: DomSanitizer,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
  ) {
    super(_environment,
      _environment.visualizationPdfExtensions,
      _environment.visualizationPdfContentType,
      _environment.visualizationPdfMimeType,
      _environment.visualizationPdfPronomId,
      _environment.visualizationPdfMaxFileSizeInMegabytes);
  }

  isVisualizationOnGoing(fileInfo: FileInput, domElement: Element): boolean {
    return domElement.childNodes.item(0).nodeName === "PDF-VIEWER";
  }

  closeVisualizer(fileInfo: FileInput, domElement: Element): void {
    domElement.remove();
  }

  openVisualizer(fileInfo: FileInput, domElement: Element, addExternalComponent?: AddExternalComponentDirective): void {
    if (this.canHandle(fileInfo)) {
      const componentFactory = this._componentFactoryResolver.resolveComponentFactory(ViewerToolBarPresentational);
      const addExternalComponentContainerRef = addExternalComponent.viewContainerRef;
      addExternalComponentContainerRef.clear();
      this._componentRef = addExternalComponentContainerRef.createComponent(componentFactory);
      this._toolBarPdfViewerComponent = this._componentRef.instance;
      this._toolBarPdfViewerComponent.zoom = 1;
      this.subscribe(this._toolBarPdfViewerComponent.zoomObs,
        value => this.changeZoom(value));
      this._componentRef.changeDetectorRef.detectChanges();

      const url = URL.createObjectURL(fileInfo.blob);
      // ssr:comment:start
      const componentFactoryNext = this._componentFactoryResolver.resolveComponentFactory(PdfViewerComponent);
      // ssr:comment:end

      // ssr:uncomment:start
      // const componentFactoryNext = undefined;
      // ssr:uncomment:end
      this._pdfViewerComponentRef = addExternalComponentContainerRef.createComponent(componentFactoryNext);
      this._pdfViewerComponent = this._pdfViewerComponentRef.instance;
      this._pdfViewerComponent.src = url;
      this._pdfViewerComponent.renderText = true;
      this._pdfViewerComponent.showBorders = true;
      this._pdfViewerComponent.ngOnInit();
      this._pdfViewerComponent.showAll = true;
      this._pdfViewerComponentRef.changeDetectorRef.detectChanges();
      const pdfElement = this._pdfViewerComponentRef.location.nativeElement as HTMLElement;
      const returnedElement = pdfElement.getElementsByClassName("ng2-pdf-viewer-container").item(0) as HTMLElement;
      this._pdfViewerComponentRef.changeDetectorRef.detectChanges();
    }
  }

  changeZoom(newZoom: number): void {
    this._pdfViewerComponent.zoom = newZoom;
    this._pdfViewerComponentRef.changeDetectorRef.detectChanges();
    this._pdfViewerComponent.updateSize();
    this._pdfViewerComponentRef.changeDetectorRef.detectChanges();
  }

  doAction(fileInfo: FileInput, domElement: Element): void {
  }

  handleZoom(zoomEnable: boolean, fileInfo: FileInput, domElement: Element): void {
  }
}
