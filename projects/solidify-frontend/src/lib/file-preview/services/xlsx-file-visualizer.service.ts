/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - xlsx-file-visualizer.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ComponentFactoryResolver,
  Inject,
  Injectable,
  NgZone,
} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";
import {TranslateService} from "@ngx-translate/core";
// ssr:comment:start
import {
  read,
  utils,
} from "xlsx";
// ssr:comment:end
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../core/injection-tokens/label-to-translate.injection-token";
import {LabelTranslateInterface} from "../../label-translate-interface.model";
import {AddExternalComponentDirective} from "../directives/add-external-component.directive";
import {FileInput} from "../models/file-info.model";
import {AbstractFileVisualizer} from "./abstract-file-visualizer.service";

@Injectable({
  providedIn: "root",
})
export class XlsxFileVisualizerService extends AbstractFileVisualizer {
  isFullScreenSupported: boolean = true;
  isZoomSupported: boolean = false;
  divContainer: HTMLDivElement;
  tabsSelectors: HTMLUListElement;
  type: string = "xlsx";

  private readonly _COMPONENT_CANVAS_DATAGRID: string = "canvas-datagrid";
  private readonly _ATTRIBUTE_TAB_INDEX: string = "tab-index";
  private readonly _ATTRIBUTE_EDITABLE: string = "editable";
  private readonly _CLASS_HIDE: string = "hide";
  private readonly _CLASS_ACTIVE: string = "is-active";
  private readonly _CLASS_TABS_SELECTORS: string = "tabs-selectors";

  constructor(private readonly _componentFactoryResolver: ComponentFactoryResolver,
              private readonly _ngZone: NgZone,
              private readonly _domSanitizer: DomSanitizer,
              private readonly _translateService: TranslateService,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface,
  ) {
    super(_environment,
      _environment.visualizationXlsxPreviewExtensions,
      _environment.visualizationXlsxPreviewContentType,
      _environment.visualizationXlsxPreviewMimeType,
      _environment.visualizationXlsxPreviewPronomId,
      _environment.visualizationXlsxPreviewMaxFileSizeInMegabytes);
  }

  isVisualizationOnGoing(fileInfo: FileInput, domElement: Element): boolean {
    return false;
  }

  closeVisualizer(fileInfo: FileInput, domElement: Element): void {
    domElement.remove();
    this.divContainer = null;
  }

  openVisualizer(fileInfo: FileInput, domElement: Element, addExternalComponent?: AddExternalComponentDirective): void {
    this.divContainer = SsrUtil.window?.document.createElement("div");
    const reader = new FileReader();
    reader.onloadend = (evt) => {
      if (evt.target.readyState === FileReader.DONE) {
        const arrayBuffer = evt.target.result;
        this._treatWorkBook(arrayBuffer);
      }
    };
    reader.readAsArrayBuffer(fileInfo.blob);
    domElement.appendChild(this.divContainer);
  }

  private _treatWorkBook(arrayBuffer: string | ArrayBuffer | null): void {
    this.tabsSelectors = SsrUtil.window?.document.createElement("ul");
    this.tabsSelectors.classList.add(this._CLASS_TABS_SELECTORS);
    // ssr:comment:start
    const wb = read(arrayBuffer, {type: "buffer"});
    wb.SheetNames.forEach((sheetName, index) => {
      const ws = wb.Sheets[wb.SheetNames[index]];
      const data = utils.sheet_to_json(ws);
      this._treatWorkSheet(data, sheetName, index);
    });
    // ssr:comment:end
    this.divContainer.appendChild(this.tabsSelectors);
  }

  private _treatWorkSheet(data: any[], sheetName: string, index: number): void {
    const grid = document.createElement(this._COMPONENT_CANVAS_DATAGRID);
    grid["data"] = data;
    grid.setAttribute(this._ATTRIBUTE_EDITABLE, "false");
    grid.setAttribute(this._ATTRIBUTE_TAB_INDEX, index + "");
    grid[this._ATTRIBUTE_EDITABLE] = false;
    this.divContainer.appendChild(grid);
    grid.classList.add(this._CLASS_HIDE);
    const li = SsrUtil.window?.document.createElement("li");
    li.textContent = sheetName;
    li.setAttribute(this._ATTRIBUTE_TAB_INDEX, index + "");
    this.tabsSelectors.appendChild(li);
    this.tabsSelectors.onclick = (event: MouseEvent) => this._tabClick(event);
    if (index === 0) {
      this._setActiveTab(li);
    }
  }

  private _tabClick(event: MouseEvent): void {
    const targetTab = event.target as HTMLLIElement;
    this._setActiveTab(targetTab);
  }

  private _setActiveTab(targetTab: HTMLLIElement): void {
    if (targetTab.tagName.toUpperCase() !== "LI" || targetTab.classList.contains(this._CLASS_ACTIVE)) {
      return;
    }
    this.divContainer.querySelectorAll(this._COMPONENT_CANVAS_DATAGRID).forEach(node => node.classList.add(this._CLASS_HIDE));
    this.divContainer.querySelectorAll(`ul.${this._CLASS_TABS_SELECTORS} > li`).forEach(node => node.classList.remove(this._CLASS_ACTIVE));
    targetTab.classList.add(this._CLASS_ACTIVE);
    const tabIndex = targetTab.getAttribute(this._ATTRIBUTE_TAB_INDEX);
    const gridToDisplay = this.divContainer.querySelector(`${this._COMPONENT_CANVAS_DATAGRID}[${this._ATTRIBUTE_TAB_INDEX}='${tabIndex}']`);
    gridToDisplay?.classList.remove(this._CLASS_HIDE);
  }

  doAction(fileInfo: FileInput, domElement: Element): void {
  }

  handleZoom(zoomEnable: boolean, fileInfo: FileInput, domElement: Element): void {
  }
}
