/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-frontend-file-preview.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {NgModule} from "@angular/core";
import {NgxsModule} from "@ngxs/store";
// ssr:comment:start
import {PdfViewerModule} from "ng2-pdf-viewer";
// ssr:comment:end
import {SolidifyFrontendCoreModule} from "../core/solidify-frontend-core.module";
import {FileVisualizerContainer} from "./components/containers/file-visualizer.container";
import {FilePreviewDialog} from "./components/dialogs/file-preview/file-preview.dialog";
import {ViewerToolBarPresentational} from "./components/presentationals/viewer-tool-bar/viewer-tool-bar.presentational";
import {AddExternalComponentDirective} from "./directives/add-external-component.directive";
import {VisualizationState} from "./stores/visualization.state";

const containers = [
  FileVisualizerContainer,
];

const presentationals = [
  ViewerToolBarPresentational,
];

const dialogs = [
  FilePreviewDialog,
];

const components = [
  ...containers,
  ...presentationals,
  ...dialogs,
];

const directives = [
  AddExternalComponentDirective,
];

const states = [
  VisualizationState,
];

@NgModule({
  declarations: [
    ...components,
    ...directives,
  ],
  imports: [
    SolidifyFrontendCoreModule,
    // ssr:comment:start
    PdfViewerModule,
    // ssr:comment:end
    NgxsModule.forFeature([
      ...states,
    ]),
  ],
  exports: [
    SolidifyFrontendCoreModule,
    // ssr:comment:start
    PdfViewerModule,
    // ssr:comment:end
    NgxsModule,
    ...components,
    ...directives,
  ],
  providers: [],
})
export class SolidifyFrontendFilePreviewModule {
}
