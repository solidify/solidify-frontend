/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - visualization.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {StatePartialEnum} from "../../core/enums/partial/state-partial.enum";
import {SolidifyStateError} from "../../core/errors/solidify-state.error";
import {ApiService} from "../../core/http/api.service";
import {LABEL_TRANSLATE} from "../../core/injection-tokens/label-to-translate.injection-token";
import {SolidifyHttpErrorResponseModel} from "../../core/models/errors/solidify-http-error-response.model";
import {QueryParameters} from "../../core-resources/models/query-parameters/query-parameters.model";
import {BaseResourceStateModel} from "../../core/models/stores/base-resource-state.model";
import {SolidifyStateContext} from "../../core/models/stores/state-context.model";
import {DownloadService} from "../../core/services/download.service";
import {NotificationService} from "../../core/services/notification.service";
import {BasicState} from "../../core/stores/abstract/base/basic.state";
import {defaultResourceStateInitValue} from "../../core/stores/abstract/resource/resource.state";
import {LabelTranslateInterface} from "../../label-translate-interface.model";
import {VisualizationAction} from "./visualization.action";

export interface VisualizationStateModel extends BaseResourceStateModel {
  blob: Blob;
  blobId: number;
}

@Injectable()
@State<VisualizationStateModel>({
  name: StatePartialEnum.application_visualization,
  defaults: {
    blob: undefined,
    blobId: -1,
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(),
  },
})
export class VisualizationState extends BasicState<VisualizationStateModel> {
  constructor(private readonly _apiService: ApiService,
              private readonly _store: Store,
              private readonly _downloadService: DownloadService,
              private readonly _notificationService: NotificationService,
              private readonly _actions$: Actions,
              @Inject(LABEL_TRANSLATE) private readonly _labelTranslateInterface: LabelTranslateInterface,
  ) {
    super();
  }

  @Action(VisualizationAction.Download)
  download(ctx: SolidifyStateContext<VisualizationStateModel>, action: VisualizationAction.Download): Observable<Blob> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._downloadService.downloadInMemory(action.previewDownloadUrl, action.data.dataFile.fileName, false)
      .pipe(
        tap(result => ctx.dispatch(new VisualizationAction.DownloadSuccess(result, action.blobId))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new VisualizationAction.DownloadFailed());
          throw new SolidifyStateError(this as any, error);
        }),
      );
  }

  @Action(VisualizationAction.DownloadFailed)
  downloadFailed(ctx: SolidifyStateContext<VisualizationStateModel>, action: VisualizationAction.DownloadFailed): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(this._labelTranslateInterface.filePreviewNotificationPreviewDownloadFail);
  }

  @Action(VisualizationAction.DownloadSuccess)
  downloadSuccess(ctx: SolidifyStateContext<VisualizationStateModel>, action: VisualizationAction.DownloadSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.patchState({
      blob: action.blob,
      blobId: action.blobId,
    });
  }
}
