/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - file-visualizer.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterContentInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  Injector,
  Input,
  OnDestroy,
  Output,
  Renderer2,
  ViewChild,
} from "@angular/core";
import {Store} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  filter,
  take,
} from "rxjs/operators";
import {AbstractInternalContainer} from "../../../core/components/containers/abstract-internal/abstract-internal.container";
import {SOLIDIFY_CONSTANTS} from "../../../core-resources/constants";
import {NotificationService} from "../../../core/services/notification.service";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isTruthyObject,
} from "../../../core-resources/tools/is/is.tool";
import {ObservableUtil} from "../../../core/utils/observable.util";
import {MemoizedUtil} from "../../../core/utils/stores/memoized.util";
import {AddExternalComponentDirective} from "../../directives/add-external-component.directive";
import {FileVisualizerHelper} from "../../helpers/file-visualizer.helper";
import {BlobElementModel} from "../../models/blob-element.model";
import {FileInput} from "../../models/file-info.model";
import {AbstractFileVisualizer} from "../../services/abstract-file-visualizer.service";
import {FileVisualizerService} from "../../services/file-visualizer.service";
import {VisualizationAction} from "../../stores/visualization.action";
import {VisualizationState} from "../../stores/visualization.state";

@Component({
  selector: "solidify-file-visualizer",
  templateUrl: "./file-visualizer.container.html",
  styleUrls: ["./file-visualizer.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FileVisualizerContainer extends AbstractInternalContainer implements AfterContentInit, OnDestroy {
  private readonly _fileVisualizers: AbstractFileVisualizer[];

  isLoadingVisualizationObs: Observable<boolean>;

  previewFileObs: Observable<BlobElementModel>;

  private static _filePreviewId: number = -1;

  private _currentVisualizer: AbstractFileVisualizer;

  @Input()
  visualizationContainerInput: ElementRef<Element>;

  @Input()
  fileToVisualize: FileInput;

  @Input()
  fileDownloadUrl: string;

  @HostBinding("class.is-in-dialog")
  @Input()
  isInDialog: boolean = false;

  _zoomEnable: boolean = false;

  @Input()
  set zoomEnable(zoomEnable: boolean) {
    this._zoomEnable = zoomEnable;
    this._updateZoom();
  }

  get zoomEnable(): boolean {
    return this._zoomEnable;
  }

  @ViewChild("visualizationContainer", {static: true})
  private _visualizationContainer: ElementRef<Element>;

  @ViewChild(AddExternalComponentDirective, {static: true})
  private _addExternalComponent: AddExternalComponentDirective;

  private readonly _closeBS: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  @Output("closeVisualizerChange")
  readonly closeVisualizerObs: Observable<boolean> = ObservableUtil.asObservable(this._closeBS);

  private readonly _errorMessageBS: BehaviorSubject<string> = new BehaviorSubject<string>(SOLIDIFY_CONSTANTS.STRING_EMPTY);
  @Output("errorMessageChange")
  readonly errorMessageObs: Observable<string> = ObservableUtil.asObservable(this._errorMessageBS);

  private readonly _fullScreenAvailableBS: BehaviorSubject<boolean | undefined> = new BehaviorSubject<boolean>(undefined);
  @Output("fullScreenAvailableChange")
  readonly fullScreenAvailableObs: Observable<boolean | undefined> = ObservableUtil.asObservable(this._fullScreenAvailableBS);

  private readonly _zoomAvailableBS: BehaviorSubject<boolean | undefined> = new BehaviorSubject<boolean>(undefined);
  @Output("zoomAvailableChange")
  readonly zoomAvailableObs: Observable<boolean | undefined> = ObservableUtil.asObservable(this._zoomAvailableBS);

  @HostListener("click", ["$event"])
  click(mouseEvent: MouseEvent): void {
    mouseEvent.stopPropagation();
    const elem = mouseEvent.target as Element;
    if (this._visualizationContainer.nativeElement.contains(elem)) {
      const element = this._fileVisualizers.find(value => value.isVisualizationOnGoing(this.fileToVisualize, this._visualizationContainer.nativeElement));
      element?.doAction(this.fileToVisualize, this._visualizationContainer.nativeElement);
    }
  }

  constructor(fileVisualizerService: FileVisualizerService,
              private readonly _store: Store,
              private readonly _notificationService: NotificationService,
              protected readonly _injector: Injector,
              protected readonly _renderer: Renderer2,
  ) {
    super(_injector);
    this._fileVisualizers = fileVisualizerService.listFileVisualizer;
    this.isLoadingVisualizationObs = MemoizedUtil.isLoading(this._store, VisualizationState);
    this.previewFileObs = MemoizedUtil.select(this._store, VisualizationState, state =>
      ({
        blob: state.blob,
        blobId: state.blobId,
      }));
  }

  ngAfterContentInit(): void {
    if (isNullOrUndefinedOrWhiteString(this.fileDownloadUrl) && isNullOrUndefined(this.fileToVisualize.blob)) {
      // eslint-disable-next-line no-console
      console.error("No download url or blob provided");
      return;
    }
    if (isTruthyObject(this.fileToVisualize) && isTruthyObject(this.fileToVisualize.dataFile)) {
      this._visualize(this.fileToVisualize, this._visualizationContainer.nativeElement);
    }
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._closeVisualization();
  }

  private _canHandleBySize(fileInfo: FileInput): boolean {
    return isTruthyObject(fileInfo) && this._fileVisualizers.some(value => value.canHandleBySize(fileInfo));
  }

  private _visualize(fileInfoParam: FileInput, documentElement: Element): void {
    const fileExtension = FileVisualizerHelper.getFileExtension(fileInfoParam.dataFile.fileName);
    const fileInfo: FileInput = {
      dataFile: fileInfoParam.dataFile,
      blob: fileInfoParam.blob,
      fileExtension: fileExtension,
      container: documentElement,
    };
    if (!FileVisualizerHelper.canHandle(this._fileVisualizers, fileInfo)) {
      this._closeVisualization();
      if (!this._canHandleBySize(fileInfo)) {
        const sizeNotSupported = this.labelTranslateInterface.filePreviewNotificationPreviewFileTooBig;
        this._notificationService.showInformation(sizeNotSupported);
        this._errorMessageBS.next(sizeNotSupported);
      } else {
        const notSupportedMessage = this.labelTranslateInterface.filePreviewNotificationPreviewFileNotSupported;
        this._notificationService.showInformation(notSupportedMessage);
        this._errorMessageBS.next(notSupportedMessage);
      }
      return;
    }

    if (isNotNullNorUndefined(fileInfo.blob)) {
      this._openVisualizerDependingOfCriteria(fileInfo, documentElement);
      return;
    }

    const filePreviewId = FileVisualizerContainer._filePreviewId++;
    this.subscribe(this.previewFileObs.pipe(
        filter(value => isTruthyObject(value.blob) && filePreviewId === value.blobId),
        take(1),
      ),
      value => {
        fileInfo.blob = value.blob;
        this._openVisualizerDependingOfCriteria(fileInfo, documentElement);
      });
    this._store.dispatch(new VisualizationAction.Download(fileInfo, filePreviewId, this.fileDownloadUrl));
  }

  private _openVisualizerDependingOfCriteria(fileInfo: FileInput, documentElement: Element): void {
    const visualizer = this._fileVisualizers.find(value => value.canHandle(fileInfo));
    if (isTruthyObject(visualizer)) {
      this._openVisualizer(visualizer, fileInfo, documentElement, this._addExternalComponent);
    }
  }

  private _openVisualizer(fileVisualizer: AbstractFileVisualizer,
                          fileInfo: FileInput,
                          domElement: Element,
                          addExternalComponent?: AddExternalComponentDirective): void {
    if (isNullOrUndefined(fileVisualizer)) {
      // eslint-disable-next-line no-console
      console.warn("fileVisualizer is undefined");
      return;
    }
    fileVisualizer.openVisualizer(fileInfo, domElement, addExternalComponent);
    this._currentVisualizer = fileVisualizer;
    this._renderer.setAttribute(this._visualizationContainer.nativeElement, "viewer", this._currentVisualizer.type);
    this._fullScreenAvailableBS.next(fileVisualizer.isFullScreenSupported);
    this._zoomAvailableBS.next(fileVisualizer.isZoomSupported);
    this._updateZoom();
  }

  private _isPlainTextFile(fileInfo: FileInput): boolean {
    return fileInfo?.dataFile?.mimetype === "text/plain" || fileInfo?.dataFile?.mimetype === "text/xml";
  }

  private _closeVisualization(): void {
    this._renderer.removeAttribute(this._visualizationContainer.nativeElement, "viewer");
    const visualizerOnGoing = this._fileVisualizers.find(value => value.isVisualizationOnGoing(this.fileToVisualize, this._visualizationContainer.nativeElement));
    if (isTruthyObject(visualizerOnGoing)) {
      visualizerOnGoing.closeVisualizer(this.fileToVisualize, this._visualizationContainer.nativeElement);
    }
    if (isTruthyObject(this._currentVisualizer) && isTruthyObject(visualizerOnGoing) && visualizerOnGoing !== this._currentVisualizer) {
      // eslint-disable-next-line no-console
      console.warn("fileVisualizerOnGoing is not the same than currentVisualizer registered", visualizerOnGoing, this._currentVisualizer);
      this._currentVisualizer.closeVisualizer(this.fileToVisualize, this._visualizationContainer.nativeElement);
    }
    const needToSendCloseEvent = isTruthyObject(visualizerOnGoing) || isTruthyObject(this._currentVisualizer);
    this._currentVisualizer = undefined;
    if (needToSendCloseEvent) {
      this._closeBS.next(true);
    }
  }

  private _updateZoom(): void {
    if (isNotNullNorUndefined(this._currentVisualizer) && this._currentVisualizer.isZoomSupported) {
      this._currentVisualizer.handleZoom(this.zoomEnable, this.fileToVisualize, this._visualizationContainer.nativeElement);
    }
  }
}
