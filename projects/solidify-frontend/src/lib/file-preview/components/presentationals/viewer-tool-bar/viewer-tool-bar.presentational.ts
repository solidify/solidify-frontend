/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - viewer-tool-bar.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  Output,
} from "@angular/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {AbstractInternalPresentational} from "../../../../core/components/presentationals/abstract-internal/abstract-internal.presentational";
import {ObservableUtil} from "../../../../core/utils/observable.util";

@Component({
  selector: "solidify-viewer-tool-bar",
  templateUrl: "./viewer-tool-bar.presentational.html",
  styleUrls: ["./viewer-tool-bar.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewerToolBarPresentational extends AbstractInternalPresentational {
  @Input()
  zoom: number;

  private readonly _zoomBS: BehaviorSubject<number | undefined> = new BehaviorSubject<number | undefined>(undefined);
  @Output("zoomChange")
  readonly zoomObs: Observable<number | undefined> = ObservableUtil.asObservable(this._zoomBS);

  incrementZoom(zoomNumber: number): void {
    this.zoom += zoomNumber;
    if (this.zoom <= 0) {
      this.zoom = 0;
    }
    this._zoomBS.next(this.zoom);
  }

  constructor(protected readonly _injector: Injector) {
    super(_injector);
  }
}
