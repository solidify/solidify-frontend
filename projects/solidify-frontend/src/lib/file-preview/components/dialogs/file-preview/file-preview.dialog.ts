/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - file-preview.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {AbstractInternalDialog} from "../../../../core/components/dialogs/abstract-internal/abstract-internal.dialog";
import {FilePreviewDialogData} from "./file-preview-dialog-data.model";

@Component({
  selector: "solidify-file-preview-dialog",
  templateUrl: "./file-preview.dialog.html",
  styleUrls: ["./file-preview.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilePreviewDialog extends AbstractInternalDialog<FilePreviewDialogData> {

  constructor(protected readonly _dialogRef: MatDialogRef<FilePreviewDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: FilePreviewDialogData,
              protected readonly _injector: Injector,
  ) {
    super(_injector, _dialogRef, data);
  }
}
