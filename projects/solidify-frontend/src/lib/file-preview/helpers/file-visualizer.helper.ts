/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - file-visualizer.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {FileStatusEnum} from "../../core/enums/upload/file-status.enum";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  isTruthyObject,
} from "../../core-resources/tools/is/is.tool";
import {LabelTranslateInterface} from "../../label-translate-interface.model";
import {FileInput} from "../models/file-info.model";
import {AbstractFileVisualizer} from "../services/abstract-file-visualizer.service";

export class FileVisualizerHelper {
  private static _STATUS_ALLOW_PREVIEW: FileStatusEnum[];

  constructor(statusAllowPreview: FileStatusEnum[]) {
    FileVisualizerHelper._STATUS_ALLOW_PREVIEW = statusAllowPreview;
  }

  static canHandle(fileVisualizers: AbstractFileVisualizer[], fileInfo: FileInput): boolean {
    return isNotNullNorUndefined(this.getFileVisualizerThatCanHandle(fileVisualizers, fileInfo));
  }

  static getFileVisualizerThatCanHandle(fileVisualizers: AbstractFileVisualizer[], fileInfo: FileInput): AbstractFileVisualizer | undefined {
    if (!isTruthyObject(fileInfo)) {
      return undefined;
    }
    if (!this.canHandleByStatus(fileInfo)) {
      return undefined;
    }
    return fileVisualizers.find(value => {
      const computeCanHandleBySize = value.canHandleBySize(fileInfo);
      const computeCanHandle = value.canHandle(fileInfo);
      return computeCanHandleBySize && computeCanHandle;
    });
  }

  static canHandleByStatus(fileInfo: FileInput): boolean {
    return FileVisualizerHelper._STATUS_ALLOW_PREVIEW.includes(fileInfo?.dataFile?.status);
  }

  static errorMessage(fileVisualizers: AbstractFileVisualizer[], fileInfo: FileInput, labelTranslateInterface: LabelTranslateInterface): string {
    if (!this.canHandleByStatus(fileInfo)) {
      return labelTranslateInterface.filePreviewNotificationPreviewFileStatusDoesNotAllowIt;
    }
    let matchingVisualizer: AbstractFileVisualizer = undefined;
    let canVisualizeFile: boolean = false;
    fileVisualizers.some(visualizer => {
      const canHandle = visualizer.canHandle(fileInfo);
      const canHandleBySize = visualizer.canHandleBySize(fileInfo);
      if (canHandle && canHandleBySize) {
        canVisualizeFile = true;
        matchingVisualizer = visualizer;
        return true;
      }
      if (isNullOrUndefined(matchingVisualizer) && canHandle) {
        matchingVisualizer = visualizer;
      }
    });
    if (!canVisualizeFile) {
      matchingVisualizer = isNullOrUndefined(matchingVisualizer) ? fileVisualizers[0] : matchingVisualizer;
      return this._cannotHandleErrorMessage(matchingVisualizer, fileInfo, labelTranslateInterface);
    }
  }

  static getFileExtension(fileName: string): string {
    return fileName?.split(".").pop();
  }

  private static _cannotHandleErrorMessage(fileVisualizers: AbstractFileVisualizer, fileInfo: FileInput, labelTranslateInterface: LabelTranslateInterface): string {
    if (!fileVisualizers.canHandleByExtension(fileInfo)) {
      return labelTranslateInterface.filePreviewNotificationPreviewFileNotSupported;
    } else if (!fileVisualizers.canHandleByMimeTypeContentType(fileInfo)) {
      return labelTranslateInterface.filePreviewNotificationPreviewFileContentTypeNotSupported;
    } else if (!fileVisualizers.canHandleBySize(fileInfo)) {
      return labelTranslateInterface.filePreviewNotificationPreviewFileTooBig;
    }
    return SOLIDIFY_CONSTANTS.STRING_EMPTY;
  }
}
