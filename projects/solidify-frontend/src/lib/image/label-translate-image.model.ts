/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - label-translate-image.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MARK_AS_TRANSLATABLE} from "../core-resources/utils/translate.util";

export interface LabelSolidifyImage {
  imageUploadAvatarDialogTitle?: string;
  imageUploadAvatarDialogCancel?: string;
  imageUploadAvatarDialogConfirm?: string;
  imageUploadAvatarNotSupported?: string;
  imageUploadImageDialogTitle?: string;
  imageUploadImageDialogCancel?: string;
  imageUploadImageDialogConfirm?: string;
  imageUploadImageNotSupported?: string;
  imageUploadImageDelete?: string;
  imageUploadAvatarDelete?: string;
}

export const labelSolidifyImage: LabelSolidifyImage = {
  imageUploadAvatarDialogTitle: MARK_AS_TRANSLATABLE("solidify.image.uploadAvatar.dialog.title"),
  imageUploadAvatarDialogCancel: MARK_AS_TRANSLATABLE("solidify.image.uploadAvatar.dialog.cancel"),
  imageUploadAvatarDialogConfirm: MARK_AS_TRANSLATABLE("solidify.image.uploadAvatar.dialog.confirm"),
  imageUploadAvatarNotSupported: MARK_AS_TRANSLATABLE("solidify.image.uploadAvatar.notSupported"),
  imageUploadImageDialogTitle: MARK_AS_TRANSLATABLE("solidify.image.uploadImage.dialog.title"),
  imageUploadImageDialogCancel: MARK_AS_TRANSLATABLE("solidify.image.uploadImage.dialog.cancel"),
  imageUploadImageDialogConfirm: MARK_AS_TRANSLATABLE("solidify.image.uploadImage.dialog.confirm"),
  imageUploadImageNotSupported: MARK_AS_TRANSLATABLE("solidify.image.uploadImage.notSupported"),
  imageUploadImageDelete: MARK_AS_TRANSLATABLE("solidify.image.uploadImage.delete"),
  imageUploadAvatarDelete: MARK_AS_TRANSLATABLE("solidify.image.uploadAvatar.delete"),
};
