/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - logo-wrapper.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs/index";
import {
  distinctUntilChanged,
  filter,
  takeWhile,
  tap,
} from "rxjs/operators";
import {AbstractInternalContainer} from "../../../../core/components/containers/abstract-internal/abstract-internal.container";
import {BaseResource} from "../../../../core-resources/models/dto/base-resource.model";
import {ResourceFileNameSpace} from "../../../../core/stores/abstract/resource-file/resource-file-namespace.model";
import {ResourceFileStateModel} from "../../../../core/stores/abstract/resource-file/resource-file-state.model";
import {
  isFalse,
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {MappingObject} from "../../../../core-resources/types/mapping-type.type";
import {MappingObjectUtil} from "../../../../core-resources/utils/mapping-object.util";
import {MemoizedUtil} from "../../../../core/utils/stores/memoized.util";
import {ImageDisplayModePartialEnum} from "../../../enums/image-display-mode-partial.enum";
import {UserInfo} from "../../../models/user-info.model";

@Component({
  selector: "solidify-logo-wrapper-container",
  templateUrl: "./logo-wrapper.container.html",
  styleUrls: ["./logo-wrapper.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogoWrapperContainer<TResource extends BaseResource> extends AbstractInternalContainer implements OnChanges, OnInit {
  // COMPONENT FOR DATATABLE

  isLoading: boolean = true;

  @Input()
  logoNamespace: ResourceFileNameSpace;

  @Input()
  logoState: any;

  @Input()
  isUser: boolean;

  @Input()
  userInfo: UserInfo;

  private _idResource: string;

  @Input()
  set idResource(idResource: string) {
    this._idResource = idResource;
  }

  get idResource(): string {
    return this._idResource;
  }

  @Input()
  isLogoPresent: boolean = true;

  @Input()
  imageMode: ExtendEnum<ImageDisplayModePartialEnum> = ImageDisplayModePartialEnum.IN_LIST;

  listPhotosObs: Observable<MappingObject<string, string>>;

  photo: string | undefined;

  get imageDisplayModeEnum(): typeof ImageDisplayModePartialEnum {
    return ImageDisplayModePartialEnum;
  }

  constructor(protected readonly _injector: Injector,
              private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef) {
    super(_injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._loadPhoto(this.logoNamespace, this.idResource, this.userInfo);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this._loadPhoto(this.logoNamespace, this.idResource, this.userInfo);
  }

  private _loadPhoto(logoNamespace: ResourceFileNameSpace, idResource: string, userInfo: UserInfo): void {
    if (isFalse(this.isLogoPresent)) {
      this.isLoading = false;
      return;
    }
    if (isNotNullNorUndefined(logoNamespace) && (isNotNullNorUndefined(idResource) || isNotNullNorUndefined(userInfo))) {
      this.photo = undefined;
      this.isLoading = true;
      this.listPhotosObs = MemoizedUtil.select(this._store, this.logoState, (state: ResourceFileStateModel<TResource>) => state.listFile);
      this._getLogo();
    }
  }

  protected _getLogo(): void {
    this._store.dispatch(new this.logoNamespace.GetFileByResId(this.idResource));

    const listPhotos = this._store.selectSnapshot((state: ResourceFileStateModel<TResource>) => state.listFile);
    const photo = this._getPhoto(listPhotos);
    if (isNullOrUndefined(photo)) {
      this.subscribe(this.listPhotosObs.pipe(
        distinctUntilChanged(),
        filter(list => isNotNullNorUndefined(this._getPhoto(list))),
        takeWhile((list) => this.isLoading === true),
        tap((list) => {
          this.photo = MappingObjectUtil.get(MemoizedUtil.selectSnapshot(this._store, this.logoState, (state: ResourceFileStateModel<TResource>) => state.listFile), this._idResource);
          if (isNotNullNorUndefined(this.photo)) {
            this.isLoading = false;
            this._changeDetector.detectChanges();
          }
        })));
    }
  }

  protected _getPhoto(list: MappingObject<string, string>): string | undefined {
    if (isNullOrUndefined(list)) {
      return undefined;
    }

    const photo: string = MappingObjectUtil.get(list, this._idResource);
    if (isNullOrUndefined(photo)) {
      return undefined;
    }
    return photo;
  }

}
