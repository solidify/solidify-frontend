/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - avatar-upload-wrapper.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
  Input,
  OnInit,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {DomSanitizer} from "@angular/platform-browser";
import {ActivatedRoute} from "@angular/router";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  take,
  tap,
} from "rxjs/operators";
import {DefaultSolidifyEnvironment} from "../../../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {ResourceFileStateModel} from "../../../../core/stores/abstract/resource-file/resource-file-state.model";
import {MemoizedUtil} from "../../../../core/utils/stores/memoized.util";
import {ofSolidifyActionCompleted} from "../../../../core/utils/stores/store.tool";
import {StoreUtil} from "../../../../core/utils/stores/store.util";
import {ImageDisplayModePartialEnum} from "../../../enums/image-display-mode-partial.enum";
import {UploadImageDialogData} from "../../dialogs/upload-image/upload-image.dialog";
import {AbstractImageUploadWrapperContainer} from "../abstract-image-upload-wrapper/abstract-image-upload-wrapper.container";

@Component({
  selector: "solidify-avatar-upload-wrapper-container",
  templateUrl: "./avatar-upload-wrapper.container.html",
  styleUrls: ["./avatar-upload-wrapper.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AvatarUploadWrapperContainer extends AbstractImageUploadWrapperContainer implements OnInit {
  @Input()
  userInfo: any; //UserInfo;

  @Input()
  isAvatarMissing: boolean = false;

  roundCropped: boolean = true;

  get imageDisplayModeEnum(): typeof ImageDisplayModePartialEnum {
    return ImageDisplayModePartialEnum;
  }

  constructor(protected readonly _injector: Injector,
              protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _fb: FormBuilder,
              protected readonly _sanitizer: DomSanitizer,
              protected readonly _cd: ChangeDetectorRef,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_injector,
      _store,
      _route,
      _fb,
      _sanitizer,
      _cd,
      _actions$,
      _dialog);
    this.aspectRatio = this._environment.uploadAvatarDefaultAspectRatio;
    this.resizeToHeight = this._environment.uploadAvatarDefaultResizeToHeight;
    this.resizeToWidth = this._environment.uploadAvatarDefaultResizeToWidth;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._getUserPhoto();
  }

  protected _defineSharedImageDialogData(): UploadImageDialogData {
    return {
      titleToTranslate: this.labelTranslateInterface.imageUploadAvatarDialogTitle,
      confirmToTranslate: this.labelTranslateInterface.imageUploadAvatarDialogConfirm,
      cancelToTranslate: this.labelTranslateInterface.imageUploadAvatarDialogCancel,
      imageNotSupportedToTranslate: this.labelTranslateInterface.imageUploadAvatarNotSupported,
      file: undefined,
      isLoadingObs: this.isLoadingObs,
      aspectRatio: this.aspectRatio,
      roundCropped: this.roundCropped,
      resizeToHeight: this.resizeToHeight,
      resizeToWidth: this.resizeToWidth,
    };
  }

  private _getUserPhoto(): void {
    if (this.isAvatarMissing) {
      return;
    }
    this.subscribe(
      StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(this._store, [
        {
          action: new this.resourceFileNameSpace.GetFile(this.resId),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(this.resourceFileNameSpace.GetFileSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(this.resourceFileNameSpace.GetFileFail)),
          ],
        },
      ]).pipe(
        take(1),
        tap((result) => {
          if (result.success) {
            this.photoObs = MemoizedUtil.select(this._store, this.resourceFileState, (state: ResourceFileStateModel<any>) => state.file);
          }
        }),
      ),
    );
  }
}
