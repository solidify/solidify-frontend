/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - image-upload-wrapper.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
  Input,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {DomSanitizer} from "@angular/platform-browser";
import {ActivatedRoute} from "@angular/router";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {IconNamePartialEnum} from "../../../../core/enums/partial/icon-name-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {UploadImageDialogData} from "../../dialogs/upload-image/upload-image.dialog";
import {AbstractImageUploadWrapperContainer} from "../abstract-image-upload-wrapper/abstract-image-upload-wrapper.container";

@Component({
  selector: "solidify-image-upload-wrapper-container",
  templateUrl: "./image-upload-wrapper.container.html",
  styleUrls: ["./image-upload-wrapper.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageUploadWrapperContainer extends AbstractImageUploadWrapperContainer {
  @Input()
  preFallbackImage: string | undefined;

  @Input()
  defaultImage: string;

  @Input()
  defaultIcon: IconNamePartialEnum;

  roundCropped: boolean = false;

  constructor(protected readonly _injector: Injector,
              protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _fb: FormBuilder,
              protected readonly _sanitizer: DomSanitizer,
              protected readonly _cd: ChangeDetectorRef,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_injector,
      _store,
      _route,
      _fb,
      _sanitizer,
      _cd,
      _actions$,
      _dialog);
    this.defaultImage = `assets/themes/${this._environment.theme}/toolbar-header-image.svg`;
    this.aspectRatio = this._environment.uploadImageDefaultAspectRatio;
    this.resizeToHeight = this._environment.uploadImageDefaultResizeToHeight;
    this.resizeToWidth = this._environment.uploadImageDefaultResizeToWidth;
  }

  protected _defineSharedImageDialogData(): UploadImageDialogData {
    return {
      titleToTranslate: this.labelTranslateInterface.imageUploadImageDialogTitle,
      confirmToTranslate: this.labelTranslateInterface.imageUploadImageDialogConfirm,
      cancelToTranslate: this.labelTranslateInterface.imageUploadImageDialogCancel,
      imageNotSupportedToTranslate: this.labelTranslateInterface.imageUploadImageNotSupported,
      file: undefined,
      isLoadingObs: this.isLoadingObs,
      aspectRatio: this.aspectRatio,
      roundCropped: this.roundCropped,
      resizeToHeight: this.resizeToHeight,
      resizeToWidth: this.resizeToWidth,
    };
  }
}
