/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-image-upload-wrapper.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectorRef,
  Directive,
  ElementRef,
  HostBinding,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {
  DomSanitizer,
  SafeResourceUrl,
} from "@angular/platform-browser";
import {ActivatedRoute} from "@angular/router";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {tap} from "rxjs/operators";
import {AbstractInternalContainer} from "../../../../core/components/containers/abstract-internal/abstract-internal.container";
import {ResourceFileNameSpace} from "../../../../core/stores/abstract/resource-file/resource-file-namespace.model";
import {ResourceFileStateModel} from "../../../../core/stores/abstract/resource-file/resource-file-state.model";
import {ResourceFileState} from "../../../../core/stores/abstract/resource-file/resource-file.state";
import {
  isEmptyString,
  isNullOrUndefined,
  isTrue,
} from "../../../../core-resources/tools/is/is.tool";
import {FileEvent} from "../../../../core/types/event.type";
import {DialogUtil} from "../../../../core/utils/dialog.util";
import {ObservableUtil} from "../../../../core/utils/observable.util";
import {MemoizedUtil} from "../../../../core/utils/stores/memoized.util";
import {StoreUtil} from "../../../../core/utils/stores/store.util";
import {
  UploadImageDialog,
  UploadImageDialogData,
} from "../../dialogs/upload-image/upload-image.dialog";

@Directive()
export abstract class AbstractImageUploadWrapperContainer extends AbstractInternalContainer implements OnInit {
  private readonly _MEDIA_TYPE_SVG_XML: string = "image/svg+xml";
  private readonly _LIST_FORMAT_BYPASS_CROP: string[] = [this._MEDIA_TYPE_SVG_XML];

  @Input()
  @HostBinding("class.is-editable")
  isEditable: boolean = true;

  @Input()
  resourceFileNameSpace: ResourceFileNameSpace;

  @Input()
  resourceFileState: typeof ResourceFileState | any;

  isLoadingObs: Observable<boolean>;
  photoObs: Observable<string | undefined>;

  @Input()
  resId: string;

  @Input()
  uploadInstantly: boolean = true;

  @Input()
  aspectRatio: number | undefined;

  @Input()
  resizeToHeight: number | undefined;

  @Input()
  resizeToWidth: number | undefined;

  @Input()
  abstract roundCropped: boolean;

  _imagePendingUpload: Blob | undefined | null; // undefined = not touch, null = deleted

  set imagePendingUpload(value: Blob | undefined | null) {
    this._imagePendingUpload = value;
    if (isNullOrUndefined(value)) {
      this._imagePendingUploadUrl = undefined;
    } else {
      this._imagePendingUploadUrl = URL.createObjectURL(value);
    }
  }

  get imagePendingUpload(): Blob | undefined | null {
    return this._imagePendingUpload;
  }

  private _imagePendingUploadUrl: string;

  get imagePendingUploadUrl(): string {
    return this._imagePendingUploadUrl;
  }

  @ViewChild("fileInput")
  fileInput: ElementRef;

  protected readonly _logoChangeBS: BehaviorSubject<Blob | undefined | null> = new BehaviorSubject<Blob | undefined | null>(undefined);
  @Output("logoChange")
  readonly avatarChangeObs: Observable<Blob | undefined | null> = ObservableUtil.asObservable(this._logoChangeBS);

  protected _sharedUploadImageDialogData: UploadImageDialogData;

  readonly filesAccepted: string[] = [".jpeg", ".png", ".jpg", ".svg"];

  constructor(protected readonly _injector: Injector,
              protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _fb: FormBuilder,
              protected readonly _sanitizer: DomSanitizer,
              protected readonly _cd: ChangeDetectorRef,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog) {
    super(_injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.isLoadingObs = MemoizedUtil.select(this._store, this.resourceFileState, (state: ResourceFileStateModel<any>) => state.isLoadingFile);
    this.photoObs = MemoizedUtil.select(this._store, this.resourceFileState, (state: ResourceFileStateModel<any>) => state.file);

    this.subscribe(this.photoObs.pipe(
      tap(photo => {
        this._imagePendingUploadUrl = photo;
        this._cd.detectChanges();
      }),
    ));
    this._sharedUploadImageDialogData = this._defineSharedImageDialogData();
  }

  protected abstract _defineSharedImageDialogData(): UploadImageDialogData;

  delete(): void {
    if (this.uploadInstantly) {
      this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        new this.resourceFileNameSpace.DeleteFile(this.resId),
        this.resourceFileNameSpace.DeleteFileSuccess,
        resultAction => {
          this._logoChangeBS.next(null);
        }));
    } else {
      this.imagePendingUpload = null;
      this._logoChangeBS.next(null);
    }
  }

  openModal(): void {
    if (isTrue(this.isEditable)) {
      this.fileInput.nativeElement.click();
    }
  }

  sanitizeUrl(url: string | undefined): undefined | SafeResourceUrl {
    if (isNullOrUndefined(url)) {
      return undefined;
    } else {
      return this._sanitizer.bypassSecurityTrustResourceUrl(url);
    }
  }

  fileChangeEventByInput(event: FileEvent): void {
    const target = event.target;
    if (isNullOrUndefined(target.value) || isEmptyString(target.value)) {
      return;
    }
    this.fileChangeEvent(target.files);
    this.fileInput.nativeElement.value = null;
  }

  fileChangeEvent(files: FileList | File[]): void {
    const file = files[0];
    if (this._LIST_FORMAT_BYPASS_CROP.includes(file.type)) {
      this._uploadImage(file);
    } else {
      this._cropImage(file);
    }
  }

  private _cropImage(file: File): void {
    this._sharedUploadImageDialogData.file = file;
    this.subscribe(DialogUtil.open(this._dialog, UploadImageDialog, this._sharedUploadImageDialogData, {
      width: "90%",
    }, blob => {
      this._uploadImage(blob);
      this._cd.detectChanges();
    }, () => {
    }));
  }

  private _uploadImage(blob: Blob): void {
    if (this.uploadInstantly) {
      this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        new this.resourceFileNameSpace.UploadFile(this.resId, {
          file: blob as File,
        }),
        this.resourceFileNameSpace.UploadFileSuccess,
        resultAction => {
          this._store.dispatch(new this.resourceFileNameSpace.GetFile(this.resId));
          this._logoChangeBS.next(blob);
        }));
    } else {
      this.imagePendingUpload = blob;
      this._logoChangeBS.next(blob);
    }
  }
}
