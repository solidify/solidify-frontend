/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - upload-image.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ImageCroppedEvent} from "ngx-image-cropper";
import {Observable} from "rxjs";
import {AbstractInternalDialog} from "../../../../core/components/dialogs/abstract-internal/abstract-internal.dialog";
import {NotificationService} from "../../../../core/services/notification.service";

@Component({
  selector: "solidify-upload-image-dialog",
  templateUrl: "./upload-image.dialog.html",
  styleUrls: ["./upload-image.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UploadImageDialog extends AbstractInternalDialog<UploadImageDialogData, Blob> {
  isLoadingCropper: boolean = true;
  isLoadingImage: boolean = true;

  constructor(protected readonly _injector: Injector,
              protected readonly _store: Store,
              protected readonly _dialogRef: MatDialogRef<UploadImageDialog>,
              private readonly _actions$: Actions,
              private readonly _notificationService: NotificationService,
              @Inject(MAT_DIALOG_DATA) public readonly data: UploadImageDialogData) {
    super(_injector, _dialogRef, data);
  }

  croppedImage: Blob;

  imageCropped(event: ImageCroppedEvent): void {
    this.croppedImage = event.blob;
  }

  imageLoaded(): void {
    this.isLoadingImage = false;
    // show cropper
  }

  cropperReady(): void {
    this.isLoadingCropper = false;
    // cropper ready
  }

  loadImageFailed(): void {
    // show message
    this._notificationService.showError(this.data.imageNotSupportedToTranslate);
  }

  confirm(): void {
    const blob = this.croppedImage;
    Object.assign(blob, {name: this.data.file.name});
    this.submit(blob);
  }

  base64toBlob(dataURI: string): Blob {
    const dataURISplitted = dataURI.split(",");
    const firstPart = dataURISplitted[0];
    const contentTypeData = firstPart.split(";")[0].split(":")[1];
    const byteString = atob(dataURISplitted[1]);
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);

    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], {type: contentTypeData});
  }
}

export interface UploadImageDialogData {
  titleToTranslate: string;
  confirmToTranslate: string;
  cancelToTranslate: string;
  imageNotSupportedToTranslate: string;
  file: File;
  isLoadingObs: Observable<boolean>;
  aspectRatio: number;
  roundCropped: boolean;
  resizeToWidth: number;
  resizeToHeight: number;
}
