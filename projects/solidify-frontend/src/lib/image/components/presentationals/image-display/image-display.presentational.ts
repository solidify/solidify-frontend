/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - image-display.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Inject,
  Injector,
  Input,
} from "@angular/core";
import {
  DomSanitizer,
  SafeResourceUrl,
} from "@angular/platform-browser";
import {AbstractInternalPresentational} from "../../../../core/components/presentationals/abstract-internal/abstract-internal.presentational";
import {IconNamePartialEnum} from "../../../../core/enums/partial/icon-name-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {MappingObjectUtil} from "../../../../core-resources/utils/mapping-object.util";
import {ImageDisplayModePartialEnum} from "../../../enums/image-display-mode-partial.enum";

@Component({
  selector: "solidify-image-display",
  templateUrl: "./image-display.presentational.html",
  styleUrls: ["./image-display.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageDisplayPresentational extends AbstractInternalPresentational {
  imageSanitized: SafeResourceUrl | undefined;

  private _image: string | undefined;

  @Input()
  set image(value: string | undefined) {
    this._image = value;
    this.imageSanitized = this.urlSanitized;
  }

  get image(): string | undefined {
    return this._image;
  }

  @Input()
  @HostBinding("class.hide-fallback-image")
  hideFallbackImage: boolean = false;

  @HostBinding("style.width")
  @HostBinding("style.height")
  get size(): string {
    let size = MappingObjectUtil.get(this._environment.imageSizeMapping, this.mode);
    if (isNullOrUndefined(size)) {
      size = this._environment.imageSizeDefault;
    }
    return size + "px";
  }

  @HostBinding("class")
  @Input()
  mode: ExtendEnum<ImageDisplayModePartialEnum> = ImageDisplayModePartialEnum.MAIN;

  @Input()
  fallbackImage: string;

  @Input()
  fallbackIconName: ExtendEnum<IconNamePartialEnum>;

  @Input()
  fallbackIconSizeClass: string;

  displayFallback: boolean = true;
  loading: boolean = true;

  constructor(protected readonly _injector: Injector,
              protected readonly _sanitizer: DomSanitizer,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super(_injector);
    this.fallbackImage = `assets/themes/${_environment.theme}/toolbar-header-image.svg`;
    this.fallbackIconSizeClass = `size-80`;
  }

  get urlSanitized(): undefined | SafeResourceUrl {
    if (isNullOrUndefined(this.image)) {
      return undefined;
    } else {
      return this._sanitizer.bypassSecurityTrustResourceUrl(this.image);
    }
  }

  determineDisplayFallback(image: HTMLImageElement): void {
    this.loading = false;
    if (image.width > 0) {
      this.displayFallback = false;
    }
  }

  unableToLoad(image: HTMLImageElement): void {
    this.loading = false;
    this.displayFallback = true;
  }
}
