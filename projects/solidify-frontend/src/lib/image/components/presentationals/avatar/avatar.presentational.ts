/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - avatar.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Inject,
  Injector,
  Input,
} from "@angular/core";
import {
  DomSanitizer,
  SafeResourceUrl,
} from "@angular/platform-browser";
import {AbstractInternalPresentational} from "../../../../core/components/presentationals/abstract-internal/abstract-internal.presentational";
import {DefaultSolidifyEnvironment} from "../../../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {MappingObjectUtil} from "../../../../core-resources/utils/mapping-object.util";
import {ImageDisplayModePartialEnum} from "../../../enums/image-display-mode-partial.enum";
import {UserInfo} from "../../../models/user-info.model";

@Component({
  selector: "solidify-avatar",
  templateUrl: "./avatar.presentational.html",
  styleUrls: ["./avatar.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AvatarPresentational extends AbstractInternalPresentational {
  private _userInfo: UserInfo;

  @Input()
  set userInfo(value: UserInfo) {
    this._userInfo = value;
    this.computeInitial();
  }

  get userInfo(): UserInfo {
    return this._userInfo;
  }

  @HostBinding("class.is-photo")
  private _photoUserSanitize: SafeResourceUrl | undefined;

  @Input()
  set photoUser(value: string) {
    if (isNullOrUndefined(value)) {
      this._photoUserSanitize = undefined;
      return;
    }
    this._photoUserSanitize = this.sanitizeUrl(value);
  }

  get photoUserSanitize(): SafeResourceUrl {
    return this._photoUserSanitize;
  }

  @HostBinding("class")
  @Input()
  mode: ExtendEnum<ImageDisplayModePartialEnum> = ImageDisplayModePartialEnum.IN_LIST;

  private readonly _DEFAULT_INITIAL: string = "";

  initial: string = this._DEFAULT_INITIAL;

  @HostBinding("style.width")
  @HostBinding("style.height")
  get size(): string {
    let size = MappingObjectUtil.get(this._environment.avatarSizeMapping, this.mode);
    if (isNullOrUndefined(size)) {
      size = this._environment.avatarSizeDefault;
    }
    return size + "px";
  }

  constructor(protected readonly _injector: Injector,
              private readonly _sanitizer: DomSanitizer,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super(_injector);
  }

  computeInitial(): void {
    if (isNullOrUndefined(this._userInfo)) {
      this.initial = this._DEFAULT_INITIAL;
    } else {
      const firstNameInitial = isNotNullNorUndefinedNorWhiteString(this._userInfo.firstName) ? this._userInfo.firstName.substring(0, 1) : "";
      const lastNameInitial = isNotNullNorUndefinedNorWhiteString(this._userInfo.lastName) ? this._userInfo.lastName.substring(0, 1) : "";
      this.initial = firstNameInitial + lastNameInitial;
    }
  }

  sanitizeUrl(url: string | undefined): undefined | SafeResourceUrl {
    if (isNullOrUndefined(url)) {
      return undefined;
    } else {
      return this._sanitizer.bypassSecurityTrustResourceUrl(url);
    }
  }
}
