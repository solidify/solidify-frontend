/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-frontend-image.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {ImageCropperComponent} from "ngx-image-cropper";
import {SolidifyFrontendCoreModule} from "../core/solidify-frontend-core.module";
import {AvatarUploadWrapperContainer} from "./components/containers/avatar-upload-wrapper/avatar-upload-wrapper.container";
import {ImageUploadWrapperContainer} from "./components/containers/image-upload-wrapper/image-upload-wrapper.container";
import {LogoWrapperContainer} from "./components/containers/logo-wrapper/logo-wrapper.container";
import {UploadImageDialog} from "./components/dialogs/upload-image/upload-image.dialog";
import {AvatarPresentational} from "./components/presentationals/avatar/avatar.presentational";
import {ImageDisplayPresentational} from "./components/presentationals/image-display/image-display.presentational";

const containers = [
  ImageUploadWrapperContainer,
  AvatarUploadWrapperContainer,
  LogoWrapperContainer,
];

const dialogs = [
  UploadImageDialog,
];

const presentationals = [
  AvatarPresentational,
  ImageDisplayPresentational,
];

const routables = [];

const components = [
  ...containers,
  ...dialogs,
  ...presentationals,
  ...routables,
];

const modules = [
  SolidifyFrontendCoreModule,
];

@NgModule({
  declarations: [
    ...components,
  ],
  imports: [
    ...modules,
    ImageCropperComponent,
  ],
  exports: [
    ...modules,
    ...components,
  ],
  providers: [],
})
export class SolidifyFrontendImageModule {
}
