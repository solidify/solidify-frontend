/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - global-banner.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpClient,
  HttpHeaders,
} from "@angular/common/http";
import {
  Inject,
  Injectable,
  Optional,
} from "@angular/core";
import {
  Action,
  State,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";

import {SOLIDIFY_CONSTANTS} from "../core-resources/constants";
import {StatePartialEnum} from "../core/enums/partial/state-partial.enum";
import {DefaultSolidifyEnvironment} from "../core/environments/environment.solidify-defaults";
import {HeaderEnum} from "../core/http/header.enum";
import {APP_OPTIONS} from "../core/injection-tokens/app-options.injection-token";
import {ENVIRONMENT} from "../core/injection-tokens/environment.injection-token";
import {SolidifyHttpErrorResponseModel} from "../core/models/errors/solidify-http-error-response.model";
import {GlobalBanner} from "../core/models/global-banner.model";
import {BaseStateModel} from "../core/models/stores/base-state.model";
import {SolidifyStateContext} from "../core/models/stores/state-context.model";
import {SolidifyAppOptions} from "../core/stores/abstract/app/app-options.model";
import {defaultBaseStateInitValue} from "../core/stores/abstract/base/base.state";
import {BasicState} from "../core/stores/abstract/base/basic.state";
import {isFunction} from "../core-resources/tools/is/is.tool";
import {SolidifyGlobalBannerAction} from "./global-banner.action";

export interface SolidifyGlobalBannerStateModel extends BaseStateModel {
  globalBanner: GlobalBanner;
}

@Injectable()
@State<SolidifyGlobalBannerStateModel>({
  name: StatePartialEnum.application_global_banner,
  defaults: {
    ...defaultBaseStateInitValue(),
    globalBanner: undefined,
  },
})
export class SolidifyGlobalBannerState extends BasicState<SolidifyGlobalBannerStateModel> {

  constructor(protected readonly _httpClient: HttpClient,
              @Optional() @Inject(APP_OPTIONS) protected readonly _optionsState: SolidifyAppOptions,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super();
  }

  @Action(SolidifyGlobalBannerAction.GetGlobalBanner)
  getGlobalBanner(ctx: SolidifyStateContext<SolidifyGlobalBannerStateModel>, action: SolidifyGlobalBannerAction.GetGlobalBanner): Observable<any> {
    if (!isFunction(this._optionsState?.urlActiveGlobalBanner)) {
      return;
    }
    const headers = this._computeHttpHeadersForGetGlobalBanner();
    return this._httpClient.get<GlobalBanner>(this._optionsState.urlActiveGlobalBanner(), {headers})
      .pipe(
        tap(result => {
          ctx.dispatch(new SolidifyGlobalBannerAction.GetGlobalBannerSuccess(action, result));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SolidifyGlobalBannerAction.GetGlobalBannerFail(action));
          throw this._environment?.errorToSkipInErrorHandler;
        }),
      );
  }

  protected _computeHttpHeadersForGetGlobalBanner(): HttpHeaders {
    return new HttpHeaders().set(HeaderEnum.skipOauth2TokenHeader, SOLIDIFY_CONSTANTS.STRING_TRUE);
  }

  @Action(SolidifyGlobalBannerAction.GetGlobalBannerSuccess)
  getGlobalBannerSuccess(ctx: SolidifyStateContext<SolidifyGlobalBannerStateModel>, action: SolidifyGlobalBannerAction.GetGlobalBannerSuccess): void {
    ctx.patchState({
      globalBanner: action.globalBanner,
    });
  }

  @Action(SolidifyGlobalBannerAction.GetGlobalBannerFail)
  getGlobalBannerFail(ctx: SolidifyStateContext<SolidifyGlobalBannerStateModel>, action: SolidifyGlobalBannerAction.GetGlobalBannerFail): void {
    ctx.patchState({
      globalBanner: undefined,
    });
  }

}
