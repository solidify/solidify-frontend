/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-global-banner-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {EmptyContainer} from "../../core/components/containers/empty-container/empty.container";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {AppRoutesPartialEnum} from "../../core/enums/partial/app-routes-partial.enum";
import {CanDeactivateGuard} from "../../core/guards/can-deactivate-guard.service";
import {CombinedGuardService} from "../../core/guards/combined-guard.service";
import {ResourceExistGuardService} from "../../core/guards/resource-exist-guard.service";
import {SolidifyRoutes} from "../../core/models/route/route.model";
import {labelSolidifyGlobalBanner} from "../label-translate-global-banner.model";
import {AdminGlobalBannerCreateRoutable} from "./components/routables/admin-global-banner-create/admin-global-banner-create.routable";
import {AdminGlobalBannerDetailEditRoutable} from "./components/routables/admin-global-banner-detail-edit/admin-global-banner-detail-edit.routable";
import {AdminGlobalBannerListRoutable} from "./components/routables/admin-global-banner-list/admin-global-banner-list.routable";
import {adminGlobalBannerActionNameSpace} from "./stores/admin-global-banner.action";
import {AdminGlobalBannerState} from "./stores/admin-global-banner.state";

const routes: SolidifyRoutes = [
  {
    path: AppRoutesPartialEnum.root,
    component: AdminGlobalBannerListRoutable,
    data: {},
  },
  {
    path: AppRoutesPartialEnum.globalBannerCreate,
    component: AdminGlobalBannerCreateRoutable,
    data: {
      breadcrumb: labelSolidifyGlobalBanner.globalBannerAdminBreadcrumbCreate,
    },
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: AppRoutesPartialEnum.globalBannerDetail + SOLIDIFY_CONSTANTS.SEPARATOR + AppRoutesPartialEnum.paramId,
    component: AdminGlobalBannerDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminGlobalBannerState.currentTitle,
      nameSpace: adminGlobalBannerActionNameSpace,
      resourceState: AdminGlobalBannerState,
      guards: [ResourceExistGuardService],
    },
    canActivate: [CombinedGuardService],
    children: [
      {
        path: AppRoutesPartialEnum.globalBannerEdit,
        component: EmptyContainer,
        data: {
          breadcrumb: labelSolidifyGlobalBanner.globalBannerAdminBreadcrumbEdit,
        },
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminGlobalBannerRoutingModule {
}
