/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - global-banner.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {KeyValue} from "../../../core-resources/models/key-value.model";
import {LabelTranslateInterface} from "../../../label-translate-interface.model";

export namespace GlobalBannerEnum {
  export type TypeEnum = "CRITICAL" | "WARNING" | "INFO";
  export const TypeEnum = {
    CRITICAL: "CRITICAL" as TypeEnum,
    WARNING: "WARNING" as TypeEnum,
    INFO: "INFO" as TypeEnum,
  };
  export const TypeEnumTranslate: (labelTranslate: LabelTranslateInterface) => KeyValue[] = (labelTranslate: LabelTranslateInterface): KeyValue[] => [
    {
      key: TypeEnum.CRITICAL,
      value: labelTranslate.globalBannerEnumTypeCritical,
    },
    {
      key: TypeEnum.WARNING,
      value: labelTranslate.globalBannerEnumTypeWarning,
    },
    {
      key: TypeEnum.INFO,
      value: labelTranslate.globalBannerEnumTypeInfo,
    },
  ];
}
