/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-global-banner-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {AbstractCreateRoutable} from "../../../../../application/components/routables/abstract-create/abstract-create.routable";
import {StatePartialEnum} from "../../../../../core/enums/partial/state-partial.enum";
import {GlobalBanner} from "../../../../../core/models/global-banner.model";
import {ResourceNameSpace} from "../../../../../core/stores/abstract/resource/resource-namespace.model";
import {MemoizedUtil} from "../../../../../core/utils/stores/memoized.util";
import {adminGlobalBannerActionNameSpace} from "../../../stores/admin-global-banner.action";
import {
  AdminGlobalBannerState,
  AdminGlobalBannerStateModel,
} from "../../../stores/admin-global-banner.state";
import {sharedGlobalBannerActionNameSpace} from "../../../stores/shared-global-banner.action";

@Component({
  selector: "solidify-admin-global-banner-create-routable",
  templateUrl: "./admin-global-banner-create.routable.html",
  styleUrls: ["./admin-global-banner-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminGlobalBannerCreateRoutable extends AbstractCreateRoutable<GlobalBanner, AdminGlobalBannerStateModel> {
  isLoadingWithDependencyObs: Observable<boolean>;
  isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedGlobalBannerActionNameSpace;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_store, _actions$, _changeDetector, StatePartialEnum.admin_globalBanner, _injector, adminGlobalBannerActionNameSpace, StatePartialEnum.admin);
    this.isLoadingWithDependencyObs = MemoizedUtil.select(this._store, AdminGlobalBannerState, state => AdminGlobalBannerState.isLoadingWithDependency(state));
    this.isReadyToBeDisplayedInCreateModeObs = MemoizedUtil.select(this._store, AdminGlobalBannerState, state => AdminGlobalBannerState.isReadyToBeDisplayedInCreateMode(state));
  }
}
