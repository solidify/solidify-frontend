/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-global-banner-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {AbstractListRoutable} from "../../../../../application/components/routables/abstract-list/abstract-list.routable";
import {DataTableFieldTypeEnum} from "../../../../../core/enums/datatable/data-table-field-type.enum";
import {OrderEnum} from "../../../../../core-resources/enums/order.enum";
import {StatePartialEnum} from "../../../../../core/enums/partial/state-partial.enum";
import {LABEL_TRANSLATE} from "../../../../../core/injection-tokens/label-to-translate.injection-token";
import {GlobalBanner} from "../../../../../core/models/global-banner.model";
import {RouterExtensionService} from "../../../../../core/services/router-extension.service";
import {LabelTranslateInterface} from "../../../../../label-translate-interface.model";
import {GlobalBannerEnum} from "../../../enums/global-banner.enum";
import {adminGlobalBannerActionNameSpace} from "../../../stores/admin-global-banner.action";
import {AdminGlobalBannerStateModel} from "../../../stores/admin-global-banner.state";

@Component({
  selector: "solidify-admin-global-banner-list-routable",
  templateUrl: "../../../../../application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-global-banner-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminGlobalBannerListRoutable extends AbstractListRoutable<GlobalBanner, AdminGlobalBannerStateModel> {
  readonly KEY_CREATE_BUTTON: string;
  readonly KEY_BACK_BUTTON: string | undefined;
  readonly KEY_PARAM_NAME: keyof GlobalBanner & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslate: LabelTranslateInterface) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StatePartialEnum.admin_globalBanner, adminGlobalBannerActionNameSpace, _injector, {}, StatePartialEnum.admin);
    this.KEY_CREATE_BUTTON = _labelTranslate.applicationCreate;
    this.KEY_BACK_BUTTON = _labelTranslate.applicationBackToAdmin;
  }

  conditionDisplayEditButton(model: GlobalBanner | undefined): boolean {
    return true;
  }

  conditionDisplayDeleteButton(model: GlobalBanner | undefined): boolean {
    return true;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "name",
        header: this._labelTranslate.globalBannerAdminListName,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "type",
        header: this._labelTranslate.globalBannerAdminListType,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        filterableField: "type" as any,
        sortableField: "type" as any,
        translate: true,
        filterEnum: GlobalBannerEnum.TypeEnumTranslate(this.labelTranslateInterface),
      },
      {
        field: "enabled",
        header: this._labelTranslate.globalBannerAdminListEnabled,
        type: DataTableFieldTypeEnum.boolean,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "startDate",
        header: this._labelTranslate.globalBannerAdminListStartDate,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "endDate",
        header: this._labelTranslate.globalBannerAdminListEndDate,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: this._labelTranslate.globalBannerAdminListCreated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
      {
        field: "lastUpdate.when" as any,
        header: this._labelTranslate.globalBannerAdminListUpdated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
    ];
  }
}
