/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - admin-global-banner-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import moment, {Moment} from "moment";
import {tap} from "rxjs/operators";
import {AbstractFormPresentational} from "../../../../../core/components/presentationals/abstract-form/abstract-form.presentational";
import {DefaultSolidifyGlobalBannerEnvironment} from "../../../../../core/environments/environment.solidify-global-banner";
import {ENVIRONMENT} from "../../../../../core/injection-tokens/environment.injection-token";
import {BaseFormDefinition} from "../../../../../core/models/forms/base-form-definition.model";
import {GlobalBanner} from "../../../../../core/models/global-banner.model";
import {KeyValue} from "../../../../../core-resources/models/key-value.model";
import {Label} from "../../../../../core/models/label.model";
import {BreakpointService} from "../../../../../core/services/breakpoint.service";
import {isTrue} from "../../../../../core-resources/tools/is/is.tool";
import {DateUtil} from "../../../../../core/utils/date.util";
import {SolidifyValidator} from "../../../../../core/utils/validations/validation.util";
import {Language} from "../../../../../search/index-field-alias/models/language.model";
import {GlobalBannerEnum} from "../../../enums/global-banner.enum";

@Component({
  selector: "solidify-admin-global-banner-form",
  templateUrl: "./admin-global-banner-form.presentational.html",
  styleUrls: ["./admin-global-banner-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminGlobalBannerFormPresentational extends AbstractFormPresentational<GlobalBanner> implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  typeEnumTranslate: KeyValue[];

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              private readonly _fb: FormBuilder,
              protected readonly _injector: Injector,
              public readonly breakpointService: BreakpointService,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyGlobalBannerEnvironment) {
    super(_changeDetectorRef, _elementRef, _injector);
    this.typeEnumTranslate = GlobalBannerEnum.TypeEnumTranslate(this.labelTranslateInterface);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._watchIsWithDescriptionValueForValidation();
  }

  private _watchIsWithDescriptionValueForValidation(): void {
    this.subscribe(this.form.get(this.formDefinition.withDescription).valueChanges.pipe(
      tap(isWithDescription => {
        this._setValidationDependingIsWithDescription();
        this._changeDetectorRef.detectChanges();
      }),
    ));
    this._setValidationDependingIsWithDescription();
  }

  private _setValidationDependingIsWithDescription(): void {
    const isWithDescription = this.form.get(this.formDefinition.withDescription).value;
    const fcDescriptionLabelsFr = this.form.get(this.formDefinition.descriptionLabelsFr);
    const fcDescriptionLabelsEn = this.form.get(this.formDefinition.descriptionLabelsEn);
    if (isTrue(isWithDescription)) {
      fcDescriptionLabelsFr.setValidators([Validators.required]);
      fcDescriptionLabelsEn.setValidators([Validators.required]);
    } else {
      fcDescriptionLabelsFr.setValidators([]);
      fcDescriptionLabelsEn.setValidators([]);
    }
    fcDescriptionLabelsFr.updateValueAndValidity();
    fcDescriptionLabelsEn.updateValueAndValidity();
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.type]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.titleLabelsFr]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.titleLabelsEn]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.withDescription]: [false, [SolidifyValidator]],
      [this.formDefinition.descriptionLabelsFr]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.descriptionLabelsEn]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.enabled]: [false, [SolidifyValidator]],
      [this.formDefinition.startDate]: [new Date(), [Validators.required, SolidifyValidator]],
      [this.formDefinition.startTime]: ["08:00", [SolidifyValidator]],
      [this.formDefinition.endDate]: [new Date(), [Validators.required, SolidifyValidator]],
      [this.formDefinition.endTime]: ["18:00", [SolidifyValidator]],
    });
  }

  protected _bindFormTo(globalBanner: GlobalBanner): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [globalBanner.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.type]: [globalBanner.type, [Validators.required, SolidifyValidator]],
      [this.formDefinition.titleLabelsFr]: [this._environment.globalBannerGetTranslationFromListLabels(this._environment.globalBannerLanguageKeyFr, globalBanner.titleLabels as Label[]), [Validators.required, SolidifyValidator]],
      [this.formDefinition.titleLabelsEn]: [this._environment.globalBannerGetTranslationFromListLabels(this._environment.globalBannerLanguageKeyEn, globalBanner.titleLabels as Label[]), [Validators.required, SolidifyValidator]],
      [this.formDefinition.withDescription]: [globalBanner.withDescription, [SolidifyValidator]],
      [this.formDefinition.descriptionLabelsFr]: [this._environment.globalBannerGetTranslationFromListLabels(this._environment.globalBannerLanguageKeyFr, globalBanner.descriptionLabels as Label[]), [SolidifyValidator]],
      [this.formDefinition.descriptionLabelsEn]: [this._environment.globalBannerGetTranslationFromListLabels(this._environment.globalBannerLanguageKeyEn, globalBanner.descriptionLabels as Label[]), [SolidifyValidator]],
      [this.formDefinition.enabled]: [globalBanner.enabled, [SolidifyValidator]],
      [this.formDefinition.startDate]: [DateUtil.convertOffsetDateTimeIso8601ToDate(globalBanner.startDate), [Validators.required, SolidifyValidator]],
      [this.formDefinition.startTime]: [DateUtil.getTimeFromOffsetDateTimeIso8601(globalBanner.startDate), [Validators.required, SolidifyValidator]],
      [this.formDefinition.endDate]: [DateUtil.convertOffsetDateTimeIso8601ToDate(globalBanner.endDate), [Validators.required, SolidifyValidator]],
      [this.formDefinition.endTime]: [DateUtil.getTimeFromOffsetDateTimeIso8601(globalBanner.endDate), [Validators.required, SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(globalBanner: GlobalBanner): GlobalBanner {
    const startDate: Moment = moment(DateUtil.convertOffsetDateTimeIso8601ToDate(globalBanner.startDate));
    const endDate: Moment = moment(DateUtil.convertOffsetDateTimeIso8601ToDate(globalBanner.endDate));
    const startTime: string = this.form.get(this.formDefinition.startTime).value;
    const endTime: string = this.form.get(this.formDefinition.endTime).value;
    const startTimeSplit = startTime.split(":");
    const endTimeSplit = endTime.split(":");
    startDate.hour(+startTimeSplit[0]);
    startDate.minute(+startTimeSplit[1]);
    startDate.second(0);
    startDate.millisecond(0);
    endDate.hour(+endTimeSplit[0]);
    endDate.minute(+endTimeSplit[1]);
    endDate.second(0);
    endDate.millisecond(0);
    globalBanner.startDate = DateUtil.convertToOffsetDateTimeIso8601(startDate.toDate());
    globalBanner.endDate = DateUtil.convertToOffsetDateTimeIso8601(endDate.toDate());

    globalBanner.titleLabels = [
      {
        text: globalBanner[this.formDefinition.titleLabelsFr],
        language: {
          resId: this._environment.globalBannerLanguageKeyFr,
        } as Language | any,
      },
      {
        text: globalBanner[this.formDefinition.titleLabelsEn],
        language: {
          resId: this._environment.globalBannerLanguageKeyEn,
        } as Language | any,
      },
    ];
    if (globalBanner.withDescription) {
      globalBanner.descriptionLabels = [
        {
          text: globalBanner[this.formDefinition.descriptionLabelsFr],
          language: {
            resId: this._environment.globalBannerLanguageKeyFr,
          } as Language | any,
        },
        {
          text: globalBanner[this.formDefinition.descriptionLabelsEn],
          language: {
            resId: this._environment.globalBannerLanguageKeyEn,
          } as Language | any,
        },
      ];
    } else {
      globalBanner.descriptionLabels = [];
    }
    delete globalBanner[this.formDefinition.titleLabelsFr];
    delete globalBanner[this.formDefinition.titleLabelsEn];
    delete globalBanner[this.formDefinition.descriptionLabelsFr];
    delete globalBanner[this.formDefinition.descriptionLabelsEn];
    delete globalBanner[this.formDefinition.startTime];
    delete globalBanner[this.formDefinition.endTime];
    return globalBanner;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  name: string = "name";
  type: string = "type";
  titleLabelsFr: string = "titleLabelsFr";
  titleLabelsEn: string = "titleLabelsEn";
  withDescription: string = "withDescription";
  descriptionLabelsFr: string = "descriptionLabelsFr";
  descriptionLabelsEn: string = "descriptionLabelsEn";
  enabled: string = "enabled";
  startDate: string = "startDate";
  startTime: string = "startTime";
  endDate: string = "endDate";
  endTime: string = "endTime";
}
