/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - global-banner.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Inject,
  Injector,
  Input,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {Store} from "@ngxs/store";

import {AbstractInternalContainer} from "../core/components/containers/abstract-internal/abstract-internal.container";
import {ConfirmDialog} from "../core/components/dialogs/confirm/confirm.dialog";
import {CookieType} from "../core/cookie-consent/enums/cookie-type.enum";
import {CookieConsentUtil} from "../core/cookie-consent/utils/cookie-consent.util";
import {ButtonColorEnum} from "../core/enums/button-color.enum";
import {LanguagePartialEnum} from "../core/enums/partial/language-partial.enum";
import {LocalStoragePartialEnum} from "../core/enums/partial/local-storage-partial.enum";
import {DefaultSolidifyEnvironment} from "../core/environments/environment.solidify-defaults";
import {LocalStorageHelper} from "../core/helpers/local-storage.helper";
import {ENVIRONMENT} from "../core/injection-tokens/environment.injection-token";
import {GlobalBanner} from "../core/models/global-banner.model";
import {Label} from "../core/models/label.model";
import {
  isFalse,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isTrue,
} from "../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../core-resources/types/extend-enum.type";
import {DialogUtil} from "../core/utils/dialog.util";
import {MemoizedUtil} from "../core/utils/stores/memoized.util";
import {SolidifyGlobalBannerState} from "./global-banner.state";

@Component({
  selector: "solidify-global-banner",
  templateUrl: "./global-banner.container.html",
  styleUrls: ["./global-banner.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GlobalBannerContainer extends AbstractInternalContainer implements OnInit {
  @Input()
  isClosable: boolean = true;

  isClosed: boolean = false;

  private _globalBanner: GlobalBanner;

  @Input()
  set globalBanner(value: GlobalBanner) {
    this._globalBanner = value;
    this._computeShouldIgnoreBanner();
    this._computeIsClickable();
    this._computeTitleAndDescription();
  }

  get globalBanner(): GlobalBanner {
    return this._globalBanner;
  }

  currentLanguage: ExtendEnum<LanguagePartialEnum>;

  @Input()
  isSameLanguage: (currentLanguage: ExtendEnum<LanguagePartialEnum>, label: Label) => boolean = (currentLanguage, label) =>
    label.language === currentLanguage || label.language?.resId === currentLanguage || label.languageCode === currentLanguage;

  title: string;
  description: string;

  @HostBinding("class.is-visible")
  get isVisible(): boolean {
    return isNotNullNorUndefined(this.globalBanner) && isTrue(this.globalBanner.enabled) && isFalse(this.isClosed);
  }

  @HostBinding("class")
  get type(): string {
    return this.globalBanner.type;
  }

  isClickable: boolean = false;

  constructor(protected readonly _injector: Injector,
              private readonly _store: Store,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _dialog: MatDialog,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super(_injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    const globalBannerObs = MemoizedUtil.select(this._store, SolidifyGlobalBannerState, state => state.globalBanner);
    this.subscribe(globalBannerObs, globalBanner => this.globalBanner = globalBanner);
    const currentLanguageObs = MemoizedUtil.select(this._store, this._environment.appState, state => state.appLanguage);
    this.subscribe(currentLanguageObs, language => {
      this.currentLanguage = language;
      this._computeTitleAndDescription();
      this._changeDetector.detectChanges();
    });
    this._computeTitleAndDescription();
  }

  openDetail(): void {
    if (isTrue(this.globalBanner.withDescription)) {
      DialogUtil.open(this._dialog, ConfirmDialog, {
        titleToTranslate: this.title,
        messageToTranslate: this.description,
        innerHtmlOnTitle: true,
        innerHtmlOnMessage: true,
        cancelButtonToTranslate: this.labelTranslateInterface.coreClose,
        colorCancel: ButtonColorEnum.primary,
      }, {
        minWidth: "400px",
        maxWidth: "90vw",
      });
    }
  }

  private _computeTitleAndDescription(): void {
    this.title = this.globalBanner.titleLabels.find(l => this.isSameLanguage(this.currentLanguage, l))?.text;
    this.description = this.globalBanner.descriptionLabels.find(l => this.isSameLanguage(this.currentLanguage, l))?.text;
    this._changeDetector.detectChanges();
  }

  private _computeIsClickable(): void {
    this.isClickable = isTrue(this.globalBanner?.withDescription);
  }

  private _computeShouldIgnoreBanner(): void {
    if (isNotNullNorUndefined(this.globalBanner) && CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStoragePartialEnum.ignoreGlobalBanner)) {
      const globalBannerStorageValue = LocalStorageHelper.getItem(LocalStoragePartialEnum.ignoreGlobalBanner);
      const globalBannerStorageValueSplit = globalBannerStorageValue?.split("_");
      if (isNonEmptyArray(globalBannerStorageValueSplit)
        && globalBannerStorageValueSplit[0] === this.globalBanner.resId
        && globalBannerStorageValueSplit[1] === this.globalBanner.lastUpdate.when) {
        this.isClosed = true;
      }
    }
  }

  close(mouseEvent?: MouseEvent): void {
    if (isNotNullNorUndefined(mouseEvent)) {
      mouseEvent.stopPropagation();
    }
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStoragePartialEnum.ignoreGlobalBanner)) {
      LocalStorageHelper.setItem(LocalStoragePartialEnum.ignoreGlobalBanner, this.globalBanner.resId + "_" + this.globalBanner.lastUpdate.when);
    }
    this.isClosed = true;
  }
}
