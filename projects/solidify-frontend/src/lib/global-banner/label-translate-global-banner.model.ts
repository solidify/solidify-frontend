/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - label-translate-global-banner.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MARK_AS_TRANSLATABLE} from "../core-resources/utils/translate.util";

export interface LabelSolidifyGlobalBanner {
  globalBannerAdminBreadcrumb?: string;
  globalBannerAdminBreadcrumbCreate?: string;
  globalBannerAdminBreadcrumbEdit?: string;
  globalBannerAdminDialogDeleteMessage?: string;
  globalBannerAdminListName?: string;
  globalBannerAdminListType?: string;
  globalBannerAdminListEnabled?: string;
  globalBannerAdminListStartDate?: string;
  globalBannerAdminListEndDate?: string;
  globalBannerAdminListCreated?: string;
  globalBannerAdminListUpdated?: string;
  globalBannerAdminFormName?: string;
  globalBannerAdminFormType?: string;
  globalBannerAdminFormTitle?: string;
  globalBannerAdminFormIsWithDescription?: string;
  globalBannerAdminFormDescription?: string;
  globalBannerAdminFormEnabled?: string;
  globalBannerAdminFormStartDate?: string;
  globalBannerAdminFormStartHour?: string;
  globalBannerAdminFormEndDate?: string;
  globalBannerAdminFormEndHour?: string;
  globalBannerAdminNotificationResourceCreateSuccess?: string;
  globalBannerAdminNotificationResourceDeleteSuccess?: string;
  globalBannerAdminNotificationResourceUpdateSuccess?: string;
  globalBannerAdminTileTitle?: string;
  globalBannerAdminTileSubtitle?: string;
  globalBannerClose?: string;
  globalBannerClickForMoreInfo?: string;
  globalBannerEnumTypeCritical?: string;
  globalBannerEnumTypeWarning?: string;
  globalBannerEnumTypeInfo?: string;
}

export const labelSolidifyGlobalBanner: LabelSolidifyGlobalBanner = {
  globalBannerAdminBreadcrumb: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.breadcrumb.root"),
  globalBannerAdminBreadcrumbCreate: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.breadcrumb.create"),
  globalBannerAdminBreadcrumbEdit: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.breadcrumb.edit"),
  globalBannerAdminDialogDeleteMessage: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.dialog.delete.message"),
  globalBannerAdminListName: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.list.name"),
  globalBannerAdminListType: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.list.type"),
  globalBannerAdminListEnabled: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.list.enabled"),
  globalBannerAdminListStartDate: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.list.startDate"),
  globalBannerAdminListEndDate: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.list.endDate"),
  globalBannerAdminListCreated: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.list.created"),
  globalBannerAdminListUpdated: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.list.updated"),
  globalBannerAdminFormName: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.form.name"),
  globalBannerAdminFormType: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.form.type"),
  globalBannerAdminFormTitle: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.form.title"),
  globalBannerAdminFormIsWithDescription: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.form.isWithDescription"),
  globalBannerAdminFormDescription: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.form.description"),
  globalBannerAdminFormEnabled: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.form.enabled"),
  globalBannerAdminFormStartDate: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.form.startDate"),
  globalBannerAdminFormStartHour: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.form.startHour"),
  globalBannerAdminFormEndDate: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.form.endDate"),
  globalBannerAdminFormEndHour: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.form.endHour"),
  globalBannerAdminTileTitle: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.tile.title"),
  globalBannerAdminTileSubtitle: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.tile.subtitle"),
  globalBannerClose: MARK_AS_TRANSLATABLE("solidify.globalBanner.close"),
  globalBannerClickForMoreInfo: MARK_AS_TRANSLATABLE("solidify.globalBanner.clickForMoreInfo"),
  globalBannerAdminNotificationResourceCreateSuccess: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.notification.resourceCreateSuccess"),
  globalBannerAdminNotificationResourceDeleteSuccess: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.notification.resourceDeleteSuccess"),
  globalBannerAdminNotificationResourceUpdateSuccess: MARK_AS_TRANSLATABLE("solidify.globalBanner.admin.notification.resourceUpdateSuccess"),
  globalBannerEnumTypeCritical: MARK_AS_TRANSLATABLE("solidify.globalBanner.enum.type.critical"),
  globalBannerEnumTypeWarning: MARK_AS_TRANSLATABLE("solidify.globalBanner.enum.type.warning"),
  globalBannerEnumTypeInfo: MARK_AS_TRANSLATABLE("solidify.globalBanner.enum.type.info"),
};
