#!/bin/sh
# Exit the script if any command fails
set -e

if [ "$1" = 'start' ]; then

  NGINX_FOLDER="/usr/share/nginx"
  ENV_FOLDER="$NGINX_FOLDER/environments"
  BROWSER_FOLDER="$NGINX_FOLDER/html"

  if [ "$WEBCONTEXT" = '/' ]; then
      BROWSER_ENV_CONF_FOLDER="$BROWSER_FOLDER/assets/configurations"
  else
      mkdir -p /tmp/"$WEBCONTEXT"/
      mv -v $BROWSER_FOLDER/* /tmp/"$WEBCONTEXT"/
      rm -rf $BROWSER_FOLDER/*
      mkdir -p $BROWSER_FOLDER/"$WEBCONTEXT"/
      mv -v /tmp/"$WEBCONTEXT"/* $BROWSER_FOLDER/"$WEBCONTEXT"/
      rm -rf /tmp/"$WEBCONTEXT"
      BROWSER_ENV_CONF_FOLDER="$BROWSER_FOLDER/$WEBCONTEXT/assets/configurations"
  fi

  mkdir -p "$BROWSER_ENV_CONF_FOLDER"

  BROWSER_ENV_RUNTIME_SOURCE="$ENV_FOLDER/environment.runtime.json"
  cp $BROWSER_ENV_RUNTIME_SOURCE "$BROWSER_ENV_CONF_FOLDER/environment.runtime.json"

  # Inject BASE_HREF to index.html <base href/> tag
  chmod a+x $BROWSER_FOLDER/patch-base-href.sh
  $BROWSER_FOLDER/patch-base-href.sh $BROWSER_FOLDER $BASE_HREF

  # Start http server
  exec nginx -g 'daemon off;'
fi

# Used to run a custom command on container start
exec "$@"
