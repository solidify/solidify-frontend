/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-frontend-application.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterLink} from "@angular/router";
import {NgxsModule} from "@ngxs/store";
import {ColorPickerModule} from "ngx-color-picker";
import {SolidifyFrontendAppStatusModule} from "../app-status/solidify-frontend-app-status.module";
import {SolidifyFrontendCodeEditorModule} from "../code-editor/solidify-frontend-code-editor.module";
import {SolidifyFrontendCoreModule} from "../core/solidify-frontend-core.module";
import {SolidifyFrontendDataTableModule} from "../data-table/solidify-frontend-data-table.module";
import {SolidifyFrontendErrorHandlingModule} from "../error-handling/solidify-frontend-error-handling.module";
import {SolidifyFrontendFilePreviewModule} from "../file-preview/solidify-frontend-file-preview.module";
import {SolidifyGlobalBannerState} from "../global-banner/global-banner.state";
import {SolidifyFrontendGlobalBannerModule} from "../global-banner/solidify-frontend-global-banner.module";
import {SolidifyFrontendImageModule} from "../image/solidify-frontend-image.module";
import {SolidifyFrontendInputModule} from "../input/solidify-frontend-input.module";
import {SolidifyFrontendRichTextEditorModule} from "../rich-text-editor/solidify-frontend-rich-text-editor.module";
import {SolidifyFrontendSearchModule} from "../search/solidify-frontend-search.module";
import {SolidifySearchState} from "../search/stores/search.state";
import {SolidifyFrontendSelectModule} from "../select/solidify-frontend-select.module";
import {SolidifyFrontendTourModule} from "../tour/solidify-frontend-tour.module";
import {AdditionalInformationPanelContainer} from "./components/containers/additional-information-panel/additional-information-panel.container";
import {ArkMenuContainer} from "./components/containers/ark-menu/ark-menu.container";
import {CarouselContainer} from "./components/containers/carousel/carousel.container";
import {DoiMenuContainer} from "./components/containers/doi-menu/doi-menu.container";
import {StatusHistoryDialog} from "./components/dialogs/status-history/status-history.dialog";
import {TokenDialog} from "./components/dialogs/token/token.dialog";
import {BurgerMenuPresentational} from "./components/presentationals/burger-menu/burger-menu.presentational";
import {ButtonToolbarDetailPresentational} from "./components/presentationals/button-toolbar-detail/button-toolbar-detail.presentational";
import {ButtonToolbarListPresentational} from "./components/presentationals/button-toolbar-list/button-toolbar-list.presentational";
import {DataFilesUploadInProgressPresentational} from "./components/presentationals/data-files-upload-in-progress/data-files-upload-in-progress.presentational";
import {FileUploadTilePresentational} from "./components/presentationals/file-upload-tile/file-upload-tile.presentational";
import {FolderTreePresentational} from "./components/presentationals/folder-tree/folder-tree.presentational";
import {AboutRoutable} from "./components/routables/about/about.routable";
import {ColorCheckRoutable} from "./components/routables/color-check/color-check.routable";
import {SolidifyApplicationMaintenanceModeRoutable} from "./components/routables/maintenance-mode/solidify-application-maintenance-mode.routable";
import {PageNotFoundRoutable} from "./components/routables/page-not-found/page-not-found.routable";

const containers = [
  AdditionalInformationPanelContainer,
  ArkMenuContainer,
  DoiMenuContainer,
  CarouselContainer,
];

const dialogs = [
  StatusHistoryDialog,
  TokenDialog,
];

const presentationals = [
  BurgerMenuPresentational,
  FileUploadTilePresentational,
  DataFilesUploadInProgressPresentational,
  FolderTreePresentational,
  ButtonToolbarDetailPresentational,
  ButtonToolbarListPresentational,
];

const routables = [
  AboutRoutable,
  ColorCheckRoutable,
  SolidifyApplicationMaintenanceModeRoutable,
  PageNotFoundRoutable,
];

const components = [
  ...containers,
  ...dialogs,
  ...presentationals,
  ...routables,
];

const modules = [
  RouterLink,
  SolidifyFrontendCoreModule,
  SolidifyFrontendAppStatusModule,
  SolidifyFrontendCodeEditorModule,
  SolidifyFrontendErrorHandlingModule,
  SolidifyFrontendFilePreviewModule,
  SolidifyFrontendGlobalBannerModule,
  SolidifyFrontendRichTextEditorModule,
  SolidifyFrontendDataTableModule,
  SolidifyFrontendImageModule,
  SolidifyFrontendInputModule,
  SolidifyFrontendSearchModule,
  SolidifyFrontendSelectModule,
  SolidifyFrontendTourModule,
  ColorPickerModule,
];

const states = [
  SolidifyGlobalBannerState,
  SolidifySearchState,
];

@NgModule({
  declarations: [
    ...components,
  ],
  imports: [
    ...modules,
    NgxsModule.forFeature([
      ...states,
    ]),
  ],
  exports: [
    ...modules,
    ...components,
    NgxsModule,
  ],
  providers: [],
})
export class SolidifyFrontendApplicationModule {
}
