/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - orcid.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {Store} from "@ngxs/store";
import {isNotNullNorUndefinedNorWhiteString} from "../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {ApiResourceNamePartialEnum} from "../../core/enums/partial/api-resource-name-partial.enum";
import {AppRoutesPartialEnum} from "../../core/enums/partial/app-routes-partial.enum";
import {ApiService} from "../../core/http/api.service";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {AbstractBaseService} from "../../core/services/abstract-base.service";
import {MemoizedUtil} from "../../core/utils/stores/memoized.util";
import {DefaultSolidifyApplicationEnvironment} from "../environments/environment.solidify-application";
import {PersonWithEmail} from "../models/person-with-email.model";
import {AppSystemPropertyState} from "../stores/system-property/app-system-property.state";

@Injectable({
  providedIn: "root",
})
export class OrcidService extends AbstractBaseService {
  constructor(private readonly _apiService: ApiService,
              private readonly _store: Store,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyApplicationEnvironment) {
    super();
  }

  startOrcidAuth(person?: PersonWithEmail): void {
    const startOrcidAuthUrl = `${this._environment.admin}/${ApiResourceNamePartialEnum.ORCID}/start-orcid-auth`;
    const redirectUrl = SsrUtil.window?.location.origin + this._environment.baseHref + AppRoutesPartialEnum.orcidRedirect;
    this._apiService.getCollection<string>(startOrcidAuthUrl, {}, {originUrl: redirectUrl})
      .subscribe(val => this._getOrcid(val["authTransactionId"], person));
  }

  private _getOrcid(authTransactionId: string, person?: PersonWithEmail): void {
    const clientId: string = MemoizedUtil.selectSnapshot(this._store, AppSystemPropertyState, state => state.current?.orcid?.clientId);
    const authorizeUrl: string = MemoizedUtil.selectSnapshot(this._store, AppSystemPropertyState, state => state.current?.orcid?.authorizeUrl);
    const orcidScope: string = MemoizedUtil.selectSnapshot(this._store, AppSystemPropertyState, state => state.current?.orcid?.scope);
    const currentLanguage = MemoizedUtil.selectSnapshot(this._store, this._environment.appState, state => state.appLanguage);

    const redirectUri: string = `${this._environment.admin}/${ApiResourceNamePartialEnum.ORCID}/landing?authTransactionId=${authTransactionId}`;
    if (SsrUtil.window) {
      SsrUtil.window.location.href = authorizeUrl
        + "?client_id=" + clientId
        + "&response_type=code"
        + "&scope=" + orcidScope
        + "&redirect_uri=" + redirectUri
        + "&lang=" + currentLanguage
        + (isNotNullNorUndefinedNorWhiteString(person?.firstName) ? "&given_names=" + person.firstName : "")
        + (isNotNullNorUndefinedNorWhiteString(person?.lastName) ? "&family_names=" + person.lastName : "")
        + (isNotNullNorUndefinedNorWhiteString(person?.email) ? "&email=" + person.email : "");
    }
  }

  openOrcidPage(orcid: string): void {
    SsrUtil.window?.open(`${this._environment.orcidUrl}/${orcid}`, "_blank");
  }
}
