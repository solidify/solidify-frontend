/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-form-detect-changes.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


/* eslint-disable no-console */
import {Directive} from "@angular/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {BaseResource} from "../../core-resources/models/dto/base-resource.model";
import {AbstractBaseService} from "../../core/services/abstract-base.service";
import {ObservableUtil} from "../../core/utils/observable.util";
import {AbstractApplicationFormPresentational} from "../components/presentationals/abstract-application-form/abstract-application-form.presentational";

@Directive()
export abstract class AbstractFormDetectChangesService<TResource extends BaseResource> extends AbstractBaseService {
  abstract formPresentational: AbstractApplicationFormPresentational<TResource>;
  private readonly _detectChangesBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  readonly detectChangesObs: Observable<void> = ObservableUtil.asObservable(this._detectChangesBS);

  detectChanges(): void {
    this._detectChangesBS.next();
  }
}
