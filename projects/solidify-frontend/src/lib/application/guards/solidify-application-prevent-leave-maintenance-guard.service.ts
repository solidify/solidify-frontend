/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-application-prevent-leave-maintenance-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  Inject,
  Injectable,
  Optional,
} from "@angular/core";
import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {PreventLeaveMaintenanceGuardService} from "../../core/guards/prevent-leave-maintenance-guard.service";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {SECURITY_SERVICE} from "../security/security-service.injection-token";
import {SolidifySecurityService} from "../security/security.service";

@Injectable({
  providedIn: "root",
})
export class SolidifyApplicationPreventLeaveMaintenanceGuardService extends PreventLeaveMaintenanceGuardService {

  constructor(@Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
              @Optional() @Inject(SECURITY_SERVICE) protected readonly _securityService: SolidifySecurityService) {
    super(_environment);
  }

  // Override if necessary
  protected _isLoggedInRoot(): boolean {
    return false;
  }

}
