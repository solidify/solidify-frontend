/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-application.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Type} from "@angular/core";
import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {SolidifyAppUserStateModel} from "../../user/stores/abstract/app-user/app-user-state.model";
import {SolidifyAppUserState} from "../../user/stores/abstract/app-user/app-user.state";
import {SolidifyApplicationAppStateModel} from "../stores/abstract/app/solidify-application-app-state.model";
import {SolidifyApplicationAppState} from "../stores/abstract/app/solidify-application-app.state";

export interface DefaultSolidifyApplicationEnvironment extends DefaultSolidifyEnvironment {
  appState: Type<SolidifyApplicationAppState<SolidifyApplicationAppStateModel>>;
  appUserState: Type<SolidifyAppUserState<SolidifyAppUserStateModel>>;
  admin: string;
  doiLink: string;
  arkLink: string;
  carouselUrl: string | undefined;
  frequencyChangeCarouselTileInSecond: number;
  apiSystemProperties: (environment: DefaultSolidifyApplicationEnvironment) => string;
  allowedUrlsBuilder: (environment: DefaultSolidifyApplicationEnvironment) => string[];
  useTransferStateForCarousel: boolean;
  wcag2Link: string;
  apiSearchLanguage: (environment: DefaultSolidifyApplicationEnvironment) => string;
  tableResourceRoleDefaultPageSize: number;
  tableResourceRolePageSizeOptions: number[];
}
