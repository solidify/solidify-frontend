/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment.solidify-application.default.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {ApiResourceNamePartialEnum} from "../../core/enums/partial/api-resource-name-partial.enum";
import {defaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults.default";
import {DefaultSolidifyApplicationEnvironment} from "./environment.solidify-application";

export const defaultSolidifyApplicationEnvironment: DefaultSolidifyApplicationEnvironment = {
  ...defaultSolidifyEnvironment,
  appState: undefined,
  appUserState: undefined,
  admin: undefined,
  doiLink: "https://doi.org/",
  arkLink: "http://n2t-dev.n2t.net/",
  carouselUrl: undefined,
  frequencyChangeCarouselTileInSecond: 5,
  apiSystemProperties: (environment: DefaultSolidifyApplicationEnvironment) => `${environment.admin}/${ApiResourceNamePartialEnum.SYSTEM_PROPERTY}`,
  allowedUrlsBuilder: (environment: DefaultSolidifyApplicationEnvironment) => [],
  useTransferStateForCarousel: true,
  wcag2Link: "https://www.w3.org/WAI/WCAG2AAA-Conformance",
  apiSearchLanguage: (environment: DefaultSolidifyApplicationEnvironment) => environment.admin + SOLIDIFY_CONSTANTS.SEPARATOR + ApiResourceNamePartialEnum.LANGUAGE,
  tableResourceRoleDefaultPageSize: 5,
  tableResourceRolePageSizeOptions: [5, 10, 15, 20],
};
