/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - shared-language.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {SOLIDIFY_CONSTANTS} from "../../../core-resources/constants";
import {StatePartialEnum} from "../../../core/enums/partial/state-partial.enum";
import {ApiService} from "../../../core/http/api.service";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {QueryParameters} from "../../../core-resources/models/query-parameters/query-parameters.model";
import {NotificationService} from "../../../core/services/notification.service";
import {ResourceStateModel} from "../../../core/stores/abstract/resource/resource-state.model";
import {
  defaultResourceStateInitValue,
  ResourceState,
} from "../../../core/stores/abstract/resource/resource.state";
import {Language} from "../../../search/index-field-alias/models/language.model";
import {DefaultSolidifyApplicationEnvironment} from "../../environments/environment.solidify-application";
import {sharedLanguageActionNameSpace} from "./shared-language.action";

export interface SharedLanguageStateModel extends ResourceStateModel<Language> {
}

@Injectable()
@State<SharedLanguageStateModel>({
  name: StatePartialEnum.shared_language,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(SOLIDIFY_CONSTANTS.DEFAULT_ENUM_VALUE_PAGE_SIZE_OPTION),
  },
})
export class SharedLanguageState extends ResourceState<SharedLanguageStateModel, Language> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyApplicationEnvironment) {
    super(_apiService, _store, _notificationService, _actions$, _environment, {
      nameSpace: sharedLanguageActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return this._environment.apiSearchLanguage(this._environment);
  }
}
