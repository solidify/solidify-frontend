/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - app-system-property.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
  makeStateKey,
} from "@angular/core";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  isFunction,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
} from "../../../core-resources/tools/is/is.tool";
import {StatePartialEnum} from "../../../core/enums/partial/state-partial.enum";
import {SolidifyStateError} from "../../../core/errors/solidify-state.error";
import {ApiService} from "../../../core/http/api.service";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {SolidifyHttpErrorResponseModel} from "../../../core/models/errors/solidify-http-error-response.model";
import {BaseStateModel} from "../../../core/models/stores/base-state.model";
import {SolidifyStateContext} from "../../../core/models/stores/state-context.model";
import {NotificationService} from "../../../core/services/notification.service";
import {TransferStateService} from "../../../core/services/transfer-state.service";
import {
  BaseState,
  defaultBaseStateInitValue,
} from "../../../core/stores/abstract/base/base.state";
import {UrlUtil} from "../../../core/utils/url.util";
import {DefaultSolidifyApplicationEnvironment} from "../../environments/environment.solidify-application";
import {SystemPropertyPartial} from "../../models/system-property.model";
import {
  AppSystemPropertyAction,
  appSystemPropertyActionNameSpace,
} from "./app-system-property.action";

export interface AppSystemPropertyStateModel<TSystemProperty extends Partial<SystemPropertyPartial>> extends BaseStateModel {
  current: TSystemProperty;
}

@Injectable()
@State<AppSystemPropertyStateModel<any>>({
  name: StatePartialEnum.application_systemProperty,
  defaults: {
    ...defaultBaseStateInitValue(),
    current: undefined,
  },
})
export class AppSystemPropertyState<TSystemProperty extends Partial<SystemPropertyPartial>> extends BaseState<AppSystemPropertyStateModel<TSystemProperty>> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _transferState: TransferStateService,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyApplicationEnvironment,
  ) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: appSystemPropertyActionNameSpace,
    }, AppSystemPropertyState);
  }

  protected get _urlResource(): string {
    return this._environment.apiSystemProperties(this._environment);
  }

  @Action(AppSystemPropertyAction.GetSystemProperties)
  getSystemProperty(ctx: SolidifyStateContext<AppSystemPropertyStateModel<TSystemProperty>>, action: AppSystemPropertyAction.GetSystemProperties): Observable<Partial<SystemPropertyPartial>> {
    const STATE_KEY = makeStateKey<Partial<SystemPropertyPartial>>("SystemProperty");
    if (this._transferState.hasKey(STATE_KEY)) {
      const systemProperty = this._transferState.get(STATE_KEY, {});
      ctx.dispatch(new AppSystemPropertyAction.GetSystemPropertiesSuccess(action, systemProperty));
      this._transferState.remove(STATE_KEY);
      return;
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const url = this._urlResource;
    if (isNullOrUndefined(this._environment.allowedUrls)) {
      this._environment.allowedUrls = [];
    }
    this._environment.allowedUrls.push(url);
    if (isFunction(this._environment.allowedUrlsBuilder)) {
      const listAllowedUrls = this._environment.allowedUrlsBuilder(this._environment) ?? [];
      this._environment.allowedUrls = [...listAllowedUrls];
    }
    return this._apiService.get<Partial<SystemPropertyPartial>>(url)
      .pipe(
        tap((systemProperty: Partial<SystemPropertyPartial>) => {
          this._transferState.set(STATE_KEY, systemProperty);
          ctx.dispatch(new AppSystemPropertyAction.GetSystemPropertiesSuccess(action, systemProperty));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AppSystemPropertyAction.GetSystemPropertiesFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AppSystemPropertyAction.GetSystemPropertiesSuccess)
  getSystemPropertiesSuccess(ctx: SolidifyStateContext<AppSystemPropertyStateModel<TSystemProperty>>, action: AppSystemPropertyAction.GetSystemPropertiesSuccess<TSystemProperty>): void {
    this._environment.searchFacets = action.systemProperty.searchFacets;
    if (isNotNullNorUndefinedNorWhiteString(action.systemProperty.orcid?.baseUrl)) {
      this._environment.orcidUrl = UrlUtil.removeUrlSeparatorAtEndIfPresent(action.systemProperty.orcid.baseUrl);
    }
    this._doGetSystemPropertiesSuccess(action.systemProperty);

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      current: action.systemProperty,
    });
  }

  protected _doGetSystemPropertiesSuccess(systemProperty: TSystemProperty): void {
    // override if necessary
  }

  @Action(AppSystemPropertyAction.GetSystemPropertiesFail)
  getSystemPropertiesFail(ctx: SolidifyStateContext<AppSystemPropertyStateModel<TSystemProperty>>, action: AppSystemPropertyAction.GetSystemPropertiesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
