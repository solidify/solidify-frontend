/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - app-doi.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpClient,
  HttpHeaders,
} from "@angular/common/http";
import {
  Inject,
  Injectable,
  makeStateKey,
} from "@angular/core";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {SolidifyObject} from "../../../core-resources/types/solidify-object.type";
import {SsrUtil} from "../../../core-resources/utils/ssr.util";
import {SOLIDIFY_CONSTANTS} from "../../../core-resources/constants";
import {BannerColorEnum} from "../../../core/enums/banner-color.enum";
import {StatePartialEnum} from "../../../core/enums/partial/state-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../core/environments/environment.solidify-defaults";
import {ApiService} from "../../../core/http/api.service";
import {HeaderEnum} from "../../../core/http/header.enum";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {SolidifyHttpErrorResponseModel} from "../../../core/models/errors/solidify-http-error-response.model";
import {BaseStateModel} from "../../../core/models/stores/base-state.model";
import {SolidifyStateContext} from "../../../core/models/stores/state-context.model";
import {NotificationService} from "../../../core/services/notification.service";
import {TransferStateService} from "../../../core/services/transfer-state.service";
import {defaultBaseStateInitValue} from "../../../core/stores/abstract/base/base.state";
import {BasicState} from "../../../core/stores/abstract/base/basic.state";
import {AppDoiAction} from "./app-doi.action";

export interface AppDoiStateModel extends BaseStateModel {
  color: BannerColorEnum | undefined;
  message: string | undefined;
  parameters: SolidifyObject;
  display: boolean;
}

@Injectable()
@State<AppDoiStateModel>({
  name: StatePartialEnum.application_doi,
  defaults: {
    ...defaultBaseStateInitValue(),
    color: undefined,
    message: undefined,
    parameters: {},
    display: false,
  },
})
export class AppDoiState extends BasicState<AppDoiStateModel> {
  constructor(private readonly _apiService: ApiService,
              private readonly _store: Store,
              private readonly _notificationService: NotificationService,
              private readonly _actions$: Actions,
              private readonly _httpClient: HttpClient,
              private readonly _transferState: TransferStateService,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment) {
    super();
  }

  @Action(AppDoiAction.GetShortDoi)
  getShortDoi(ctx: SolidifyStateContext<AppDoiStateModel>, action: AppDoiAction.GetShortDoi): Observable<any> {
    const STATE_KEY = makeStateKey<string>(`AppDoiState-GetShortDoi-${action.longDoi}`);
    if (this._transferState.hasKey(STATE_KEY)) {
      const shortDoiTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(new AppDoiAction.GetShortDoiSuccess(action, shortDoiTransferState));
      return of(shortDoiTransferState);
    }

    if (SsrUtil.isServer) {
      ctx.dispatch(new AppDoiAction.GetShortDoiFail(action));
      return of(false);
    }

    let headers = new HttpHeaders();
    headers = headers.set(HeaderEnum.skipOauth2TokenHeader, SOLIDIFY_CONSTANTS.STRING_TRUE);
    const shortDoiUrl = `/api/short-doi/${action.longDoi}`;
    return this._httpClient.get<ShortDoi>(shortDoiUrl, {
      headers,
    })
      .pipe(
        tap(result => {
          this._transferState.set(STATE_KEY, result.ShortDOI);
          ctx.dispatch(new AppDoiAction.GetShortDoiSuccess(action, result.ShortDOI));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AppDoiAction.GetShortDoiFail(action));
          throw this._environment.errorToSkipInErrorHandler;
        }),
      );
  }
}

interface ShortDoi {
  DOI: string;
  ShortDOI: string;
  IsNew: boolean;
}
