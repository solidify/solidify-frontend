/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - app-banner.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {SolidifyObject} from "../../../core-resources/types/solidify-object.type";
import {BannerColorEnum} from "../../../core/enums/banner-color.enum";
import {StatePartialEnum} from "../../../core/enums/partial/state-partial.enum";
import {ApiService} from "../../../core/http/api.service";
import {LABEL_TRANSLATE} from "../../../core/injection-tokens/label-to-translate.injection-token";
import {BaseStateModel} from "../../../core/models/stores/base-state.model";
import {SolidifyStateContext} from "../../../core/models/stores/state-context.model";
import {NotificationService} from "../../../core/services/notification.service";
import {defaultBaseStateInitValue} from "../../../core/stores/abstract/base/base.state";
import {BasicState} from "../../../core/stores/abstract/base/basic.state";
import {LabelTranslateInterface} from "../../../label-translate-interface.model";
import {AppBannerAction} from "./app-banner.action";

export interface AppBannerStateModel extends BaseStateModel {
  color: BannerColorEnum | undefined;
  message: string | undefined;
  parameters: SolidifyObject;
  display: boolean;
}

@Injectable()
@State<AppBannerStateModel>({
  name: StatePartialEnum.application_banner,
  defaults: {
    ...defaultBaseStateInitValue(),
    color: undefined,
    message: undefined,
    parameters: {},
    display: false,
  },
})
export class AppBannerState extends BasicState<AppBannerStateModel> {
  constructor(private readonly _apiService: ApiService,
              private readonly _store: Store,
              private readonly _notificationService: NotificationService,
              private readonly _actions$: Actions,
              @Inject(LABEL_TRANSLATE) private readonly _labelTranslate: LabelTranslateInterface) {
    super();
  }

  @Action(AppBannerAction.Show)
  show(ctx: SolidifyStateContext<AppBannerStateModel>, action: AppBannerAction.Show): void {
    ctx.patchState({
      message: action.message,
      parameters: action.parameters,
      color: action.color,
      display: true,
    });
  }

  @Action(AppBannerAction.ShowInEditMode)
  showInEditMode(ctx: SolidifyStateContext<AppBannerStateModel>, action: AppBannerAction.ShowInEditMode): void {
    ctx.dispatch(new AppBannerAction.Show(this._labelTranslate.applicationYouAreInEditMode, BannerColorEnum.warn));
  }

  @Action(AppBannerAction.Hide)
  hide(ctx: SolidifyStateContext<AppBannerStateModel>, action: AppBannerAction.Hide): void {
    ctx.patchState({
      message: undefined,
      color: undefined,
      display: false,
    });
  }
}
