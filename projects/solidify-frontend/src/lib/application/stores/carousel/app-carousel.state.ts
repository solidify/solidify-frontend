/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - app-carousel.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpClient,
  HttpHeaders,
  HttpResponse,
} from "@angular/common/http";
import {
  Inject,
  Injectable,
  makeStateKey,
} from "@angular/core";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {parse} from "node-html-parser";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
} from "rxjs/operators";
import {
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "../../../core-resources/tools/is/is.tool";
import {ObjectUtil} from "../../../core-resources/utils/object.util";
import {SOLIDIFY_CONSTANTS} from "../../../core-resources/constants";
import {StatePartialEnum} from "../../../core/enums/partial/state-partial.enum";
import {SolidifyError} from "../../../core/errors/solidify.error";
import {ApiService} from "../../../core/http/api.service";
import {HeaderEnum} from "../../../core/http/header.enum";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {SolidifyHttpErrorResponseModel} from "../../../core/models/errors/solidify-http-error-response.model";
import {BaseStateModel} from "../../../core/models/stores/base-state.model";
import {SolidifyStateContext} from "../../../core/models/stores/state-context.model";
import {NotificationService} from "../../../core/services/notification.service";
import {TransferStateService} from "../../../core/services/transfer-state.service";
import {defaultBaseStateInitValue} from "../../../core/stores/abstract/base/base.state";
import {BasicState} from "../../../core/stores/abstract/base/basic.state";
import {CacheBustingUtil} from "../../../core/utils/cache-busting.util";
import {MemoizedUtil} from "../../../core/utils/stores/memoized.util";
import {DefaultSolidifyApplicationEnvironment} from "../../environments/environment.solidify-application";
import {CarouselResult} from "../../models/carousel-result.model";
import {AppCarouselAction} from "./app-carousel.action";

export interface AppCarouselStateModel extends BaseStateModel {
  carouselResults: CarouselResult[];
  current: CarouselResult;
}

@Injectable()
@State<AppCarouselStateModel>({
  name: StatePartialEnum.application_carousel,
  defaults: {
    ...defaultBaseStateInitValue(),
    carouselResults: [],
    current: undefined,
  },
})
export class AppCarouselState extends BasicState<AppCarouselStateModel> {
  constructor(protected readonly _httpClient: HttpClient,
              protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _transferState: TransferStateService,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyApplicationEnvironment) {
    super();
  }

  @Action(AppCarouselAction.GetCarousel)
  getCarousel(ctx: SolidifyStateContext<AppCarouselStateModel>, action: AppCarouselAction.GetCarousel): Observable<CarouselResult> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    let language = MemoizedUtil.selectSnapshot(this._store, this._environment.appState, state => state.appLanguage);
    if (isNullOrUndefined(language)) {
      language = this._environment.defaultLanguage;
    }
    const theme = this._environment.theme;

    const STATE_KEY = makeStateKey<CarouselResult>(`Carousel-${language}-${theme}`);
    if (this._environment.useTransferStateForCarousel && this._transferState.hasKey(STATE_KEY)) {
      let carouselResult = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      carouselResult = this._computeParseHtml(carouselResult);
      ctx.dispatch(new AppCarouselAction.GetCarouselSuccess(action, carouselResult));
      return of(carouselResult);
    }

    const alreadyExistingCarouselResult = ctx.getState().carouselResults.find(r => r.language === language && r.theme === theme);
    if (isNotNullNorUndefined(alreadyExistingCarouselResult)) {
      ctx.dispatch(new AppCarouselAction.GetCarouselSuccess(action, alreadyExistingCarouselResult));
      return of(alreadyExistingCarouselResult);
    }

    let headers = new HttpHeaders();
    headers = headers.set("Accept", ["application/xml"]);
    headers = headers.set("Content-Type", "text/xml");
    headers = headers.set(HeaderEnum.skipOauth2TokenHeader, SOLIDIFY_CONSTANTS.STRING_TRUE);

    let url = `assets/themes/${theme}/carousel/`;
    if (isNonEmptyString(this._environment.carouselUrl)) {
      url = this._environment.carouselUrl;
    }
    return this._httpClient.get(url + `carousel_${language}.html${CacheBustingUtil.generateCacheBustingQueryParam()}`, {
      headers,
      observe: "response",
      responseType: "text",
    }).pipe(
      map((response: HttpResponse<string>) => response.body),
      map((htmlContent: string) => {
        let carouselResult: CarouselResult = {
          theme: theme,
          language: language,
          tilesNotParsed: htmlContent,
        };
        if (this._environment.useTransferStateForCarousel) {
          this._transferState.set(STATE_KEY, carouselResult);
        }
        carouselResult = this._computeParseHtml(carouselResult);
        ctx.dispatch(new AppCarouselAction.GetCarouselSuccess(action, carouselResult));
        return carouselResult;
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new AppCarouselAction.GetCarouselFail(action));
        throw new SolidifyError("AppCarouselAction GetCarousel", error);
      }),
    );
  }

  private _computeParseHtml(carouselResult: CarouselResult): CarouselResult {
    carouselResult = ObjectUtil.clone(carouselResult);
    carouselResult.tiles = this._parseHtml(carouselResult.tilesNotParsed);
    return carouselResult;
  }

  private _parseHtml(htmlContent: string): Node[] {
    const parsedHtml = parse(htmlContent);
    return parsedHtml.querySelector("ul").childNodes.filter((n: HTMLElement & any) => n.tagName?.toUpperCase() === "LI") as Node[] & any[];
  }

  @Action(AppCarouselAction.GetCarouselSuccess)
  getCarouselSuccess(ctx: SolidifyStateContext<AppCarouselStateModel>, action: AppCarouselAction.GetCarouselSuccess): void {
    let carouselResults = ctx.getState().carouselResults;
    const isMissing = ctx.getState().carouselResults
      .findIndex(r => r.language === action.carouselResult.language
        && r.theme === action.carouselResult.theme) < 0;
    if (isMissing) {
      carouselResults = [...carouselResults, action.carouselResult];
    }
    const patch = {
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      current: action.carouselResult,
      carouselResults: carouselResults,
    };
    ctx.patchState(patch);
  }

  @Action(AppCarouselAction.GetCarouselFail)
  getCarouselFail(ctx: SolidifyStateContext<AppCarouselStateModel>, action: AppCarouselAction.GetCarouselFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
