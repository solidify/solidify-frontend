/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - app-carousel.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {StatePartialEnum} from "../../../core/enums/partial/state-partial.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
} from "../../../core/models/stores/base.action";
import {CarouselResult} from "../../models/carousel-result.model";

const state = StatePartialEnum.application_carousel;

export namespace AppCarouselAction {
  export class GetCarousel extends BaseAction {
    static readonly type: string = `[${state}] Get Carousel`;
  }

  export class GetCarouselSuccess extends BaseSubActionSuccess<GetCarousel> {
    static readonly type: string = `[${state}] Get Carousel Success`;

    constructor(public parentAction: GetCarousel, public carouselResult: CarouselResult) {
      super(parentAction);
    }
  }

  export class GetCarouselFail extends BaseSubActionFail<GetCarousel> {
    static readonly type: string = `[${state}] Get Carousel Fail`;
  }
}

export const appCarouselActionNameSpace = AppCarouselAction;
