/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-application-app.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpClient,
  HttpHeaders,
} from "@angular/common/http";
import {
  Inject,
  LOCALE_ID,
  makeStateKey,
  Optional,
  Type,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {HighlightLoader} from "ngx-highlightjs";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  isArray,
  isFalse,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
  isTruthy,
} from "../../../../core-resources/tools/is/is.tool";
import {MappingObject} from "../../../../core-resources/types/mapping-type.type";
import {MappingObjectUtil} from "../../../../core-resources/utils/mapping-object.util";
import {ObjectUtil} from "../../../../core-resources/utils/object.util";
import {SsrUtil} from "../../../../core-resources/utils/ssr.util";
import {AbstractDialogInterface} from "../../../../core/components/dialogs/abstract/abstract-dialog-interface.model";
import {AppConfigService} from "../../../../core/config/app-config.service";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {CookieType} from "../../../../core/cookie-consent/enums/cookie-type.enum";
import {CookieConsentUtil} from "../../../../core/cookie-consent/utils/cookie-consent.util";
import {RegisterDefaultAction} from "../../../../core/decorators/store.decorator";
import {LocalStoragePartialEnum} from "../../../../core/enums/partial/local-storage-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../../core/environments/environment.solidify-defaults";
import {JwtTokenHelper} from "../../../../core/helpers/jwt-token.helper";
import {LocalStorageHelper} from "../../../../core/helpers/local-storage.helper";
import {ApiService} from "../../../../core/http/api.service";
import {HeaderEnum} from "../../../../core/http/header.enum";
import {APP_OPTIONS} from "../../../../core/injection-tokens/app-options.injection-token";
import {BackendModuleVersion} from "../../../../core/models/backend-modules/backend-module-version.model";
import {SolidifyHttpErrorResponseModel} from "../../../../core/models/errors/solidify-http-error-response.model";
import {ActionSubActionCompletionsWrapper} from "../../../../core/models/stores/base.action";
import {SolidifyStateContext} from "../../../../core/models/stores/state-context.model";
import {Token} from "../../../../core/models/token.model";
import {NativeNotificationService} from "../../../../core/services/native-notification.service";
import {NotificationService} from "../../../../core/services/notification.service";
import {TransferStateService} from "../../../../core/services/transfer-state.service";
import {SolidifyAppAction} from "../../../../core/stores/abstract/app/app.action";
import {
  defaultSolidifyAppStateInitValue,
  SolidifyAppState,
} from "../../../../core/stores/abstract/app/app.state";
import {DialogUtil} from "../../../../core/utils/dialog.util";
import {ofSolidifyActionCompleted} from "../../../../core/utils/stores/store.tool";
import {StoreUtil} from "../../../../core/utils/stores/store.util";
import {LoginMode} from "../../../../oauth2/login-mode.enum";
import {OauthHelper} from "../../../../oauth2/oauth.helper";
import {OAuth2Service} from "../../../../oauth2/oauth2.service";
import {StateApplicationEnum} from "../../../enums/state-application.enum";
import {SolidifySecurityService} from "../../../security/security.service";
import {SolidifyApplicationAppNameSpace} from "./solidify-application-app-namespace.model";
import {SolidifyApplicationAppOptions} from "./solidify-application-app-options.model";
import {SolidifyApplicationAppStateModel} from "./solidify-application-app-state.model";
import {SolidifyApplicationAppAction} from "./solidify-application-app.action";

export const defaultSolidifyApplicationAppStateInitValue: () => SolidifyApplicationAppStateModel = () =>
  ({
    ...defaultSolidifyAppStateInitValue(),
    [StateApplicationEnum.application_user]: undefined,
    [StateApplicationEnum.application_carousel]: undefined,
    [StateApplicationEnum.application_banner]: undefined,
    backendModulesVersion: {} as MappingObject<string, BackendModuleVersion>,
    backendModulesUrl: {} as MappingObject<string, string | string[]>,
  });

// @dynamic
export abstract class SolidifyApplicationAppState<TStateModel extends SolidifyApplicationAppStateModel> extends SolidifyAppState<TStateModel> {
  protected declare readonly _nameSpace: SolidifyApplicationAppNameSpace;

  constructor(@Inject(LOCALE_ID) protected readonly _locale: string,
              protected readonly _store: Store,
              protected readonly _translate: TranslateService,
              protected readonly _oauthService: OAuth2Service,
              protected readonly _actions$: Actions,
              protected readonly _apiService: ApiService,
              protected readonly _httpClient: HttpClient,
              protected readonly _notificationService: NotificationService,
              @Optional() @Inject(APP_OPTIONS) protected readonly _optionsState: SolidifyApplicationAppOptions,
              protected readonly _environment: DefaultSolidifyEnvironment,
              @Optional() protected readonly _highlightLoader: HighlightLoader,
              protected readonly _nativeNotificationService: NativeNotificationService,
              protected readonly _appConfigService: AppConfigService,
              @Optional() protected readonly _dialog: MatDialog,
              @Optional() protected readonly _privacyPolicyTermsOfUseApprovalDialogType: Type<AbstractDialogInterface<any, any>>,
              @Optional() protected readonly _securityService: SolidifySecurityService,
              @Optional() protected readonly _transferState: TransferStateService,
  ) {
    super(
      _locale,
      _store,
      _translate,
      _actions$,
      _apiService,
      _httpClient,
      _notificationService,
      _optionsState,
      _environment,
      _nativeNotificationService,
      _appConfigService,
      _transferState,
    );
  }

  protected override _isLoggedInRoot(): boolean {
    return isTruthy(this._securityService && this._securityService.isLoggedIn() && this._securityService.isRoot());
  }

  protected override _applyDarkMode(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyAppAction.ChangeDarkMode): void {
    if (isNotNullNorUndefined(this._highlightLoader) && SsrUtil.isBrowser) {
      this._highlightLoader.setTheme(action.enabled ? this._environment.highlightJsThemeDark : this._environment.highlightJsThemeLight);
    }
  }

  protected override _loadModule(): ActionSubActionCompletionsWrapper[] {
    return this._optionsState.loadModule ? [{
        action: new this._nameSpace.LoadModuleUrl(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.LoadModuleUrlSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(this._nameSpace.LoadModuleUrlFail)),
        ],
      }]
      :
      [];
  }

  protected override _updateEnvironmentAllowedUrlsIfNecessary(): void {
    const tokenEndpoint = OauthHelper.getTokenEndpoint(this._environment.authorization);
    const loginUrl = OauthHelper.getLoginUrl(this._environment.authorization);
    this._environment.allowedUrls.push(tokenEndpoint, loginUrl);
  }

  protected override _computeHttpHeadersForLoadBackendModuleVersion(): HttpHeaders {
    return super._computeHttpHeadersForLoadBackendModuleVersion().set(HeaderEnum.skipOauth2TokenHeader, SOLIDIFY_CONSTANTS.STRING_TRUE);
  }

  override doLogin(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyAppAction.Login): Observable<boolean> {
    return this._oauthService.tryLogin()
      .pipe(
        tap(e => {
            if (e) {
              ctx.dispatch(new SolidifyAppAction.LoginSuccess(action));
            } else {
              ctx.dispatch(new SolidifyAppAction.LoginFail(action));
            }
          },
        ),
      );
  }

  override doLoginSuccess(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyAppAction.LoginSuccess): Token {
    const accessToken = this._oauthService.getAccessToken();
    const tokenDecoded = JwtTokenHelper.decodeToken(accessToken) as Token;
    ctx.patchState({
      isLoggedIn: true,
      token: tokenDecoded,
    });
    this.subscribe(this._nativeNotificationService.requestNotification());
    return tokenDecoded;
  }

  override doLoginFail(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyAppAction.LoginFail): void {
    if (SsrUtil.isBrowser && isTrue(this._optionsState.forceLogin) && !ctx.getState().isServerOffline && !ctx.getState().isUnableToLoadApp) {
      ctx.dispatch(new SolidifyAppAction.InitiateNewLogin());
    } else {
      ctx.patchState({
        isLoggedIn: false,
      });
    }
  }

  override doLogout(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyAppAction.Logout): void {
    const isLoggedIn = ctx.getState().isLoggedIn;
    if (isFalse(isLoggedIn)) {
      this._oauthService.logOut(true);
      return; // PREVENT INFINITE CANCEL LOOP DUE TO MULTIPLE CALL TO THIS METHOD THAT REDIRECT TO HOME PAGE
    }
    ctx.patchState({
      ignorePreventLeavePopup: true,
      isLoggedIn: false,
      token: undefined,
    });
    // Revoke tokens servers side
    this.subscribe(this._apiService.post(this._optionsState.urlRevokeToken()));
    setTimeout(() => {
      this._treatmentAfterRevokeToken(ctx);
    }, 200);
  }

  override doInitiateNewLogin(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyAppAction.InitiateNewLogin): any {
    const privacyPolicyAndTermsOfUseAccepted = LocalStorageHelper.getItem(LocalStoragePartialEnum.privacyPolicyAndTermsOfUseAccepted);
    if (isFalse(this._environment.maintenanceMode)
      && isNotNullNorUndefined(this._dialog)
      && isNotNullNorUndefined(this._privacyPolicyTermsOfUseApprovalDialogType)
      && isTrue(this._environment.displayPrivacyPolicyAndTermsOfUseApprovalDialog)
      && (isNotNullNorUndefined(this._environment.privacyPolicyLink) || isNotNullNorUndefined(this._environment.termsOfUseLink))
      && privacyPolicyAndTermsOfUseAccepted !== this._environment.versionPrivacyPolicyAndTermsOfUse) {
      this.subscribe(DialogUtil.open(this._dialog, this._privacyPolicyTermsOfUseApprovalDialogType, {
        termsOfUseLink: this._environment.termsOfUseLink,
        privacyPolicyLink: this._environment.privacyPolicyLink,
        language: ctx.getState().appLanguage,
      }, {
        takeOne: true,
      }, result => {
        if (isTrue(result)) {
          if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStoragePartialEnum.privacyPolicyAndTermsOfUseAccepted)) {
            LocalStorageHelper.setItem(LocalStoragePartialEnum.privacyPolicyAndTermsOfUseAccepted, this._environment.versionPrivacyPolicyAndTermsOfUse);
          }
          this._oauthService.initAuthorizationCodeFlow(LoginMode.STANDARD, action.stateToAdd);
        }
      }));
    } else {
      this._oauthService.initAuthorizationCodeFlow(LoginMode.STANDARD, action.stateToAdd);
    }
  }

  protected _treatmentAfterRevokeToken(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>): void {
    ctx.dispatch(new Navigate([this._environment.routeHomePage]));
    this._oauthService.logOut(true);
    ctx.patchState({
      ignorePreventLeavePopup: false,
    });
    if (isTrue(this._environment.reloadAfterLogout)) {
      SsrUtil.window?.location.reload();
    } else {
      this._navigateToMaintenanceIfApplicable();
    }
  }

  @RegisterDefaultAction((solidifyApplicationAppNameSpace: SolidifyApplicationAppNameSpace) => solidifyApplicationAppNameSpace.LoadModuleUrl, {}, SolidifyAppState)
  loadModuleUrl(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyApplicationAppAction.LoadModuleUrl): Observable<MappingObject<string, string>> {
    if (isNullOrUndefined(this._environment.allowedUrls)) {
      this._environment.allowedUrls = [];
    }
    const url = this._optionsState.urlAdminModule();
    this._environment.allowedUrls.push(url);

    const STATE_KEY = makeStateKey<MappingObject<string, string>>("ApplicationAppStateLoadModuleUrl");
    if (this._transferState?.hasKey(STATE_KEY)) {
      const runtimeConfig = this._transferState.get(STATE_KEY, {});
      ctx.dispatch(new SolidifyApplicationAppAction.LoadModuleUrlSuccess(action, runtimeConfig));
      this._transferState.remove(STATE_KEY);
      return of(runtimeConfig);
    }

    return this._apiService.get<MappingObject<string, string>>(url)
      .pipe(
        tap(runtimeConfig => {
          // throw new Error("SIMULATE SERVER OFFLINE");
          this._transferState?.set(STATE_KEY, runtimeConfig);
          ctx.dispatch(new SolidifyApplicationAppAction.LoadModuleUrlSuccess(action, runtimeConfig));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SolidifyApplicationAppAction.LoadModuleUrlFail(action));
          throw error;
        }),
      );
  }

  @RegisterDefaultAction((solidifyApplicationAppNameSpace: SolidifyApplicationAppNameSpace) => solidifyApplicationAppNameSpace.LoadModuleUrlSuccess, {}, SolidifyAppState)
  loadModuleUrlSuccess(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyApplicationAppAction.LoadModuleUrlSuccess): void {
    const initialAuthorizationUrl = this._environment.authorization;
    Object.assign(this._environment, action.modulesUrl);
    ctx.patchState({
      backendModulesUrl: action.modulesUrl,
    });
    if (isFalse(this._environment.computeOAuthEndpoint)) {
      this._environment.authorization = initialAuthorizationUrl;
    }
    if (isNullOrUndefinedOrWhiteString(this._environment.authorization)) {
      // eslint-disable-next-line no-console
      console.warn("'authorization' property should not be empty. Please set the value on backend side or disable 'computeOAuthEndpoint' and set 'authorization' on portal environment file");
    }
    this._updateEnvironmentAllowedUrlsIfNecessary();
  }

  @RegisterDefaultAction((solidifyApplicationAppNameSpace: SolidifyApplicationAppNameSpace) => solidifyApplicationAppNameSpace.LoadModuleUrlFail, {}, SolidifyAppState)
  loadModuleUrlFail(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyApplicationAppAction.LoadModuleUrlFail): void {
    ctx.patchState({
      isServerOffline: true,
    });
    if (isFalse(this._environment.maintenanceMode)) {
      ctx.dispatch(new Navigate([this._optionsState.routeServerOffline]));
    }
  }

  @RegisterDefaultAction((solidifyApplicationAppNameSpace: SolidifyApplicationAppNameSpace) => solidifyApplicationAppNameSpace.LoadBackendModulesVersion, {}, SolidifyAppState)
  loadBackendModulesVersion(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyApplicationAppAction.LoadBackendModulesVersion): Observable<boolean> {
    const actionSubActionCompletionsWrapper: ActionSubActionCompletionsWrapper[] = [];
    MappingObjectUtil.forEach(action.modulesUrl, (value, key) => {
      if (isNullOrUndefinedOrWhiteString(value) || this._environment.aboutModulesToIgnore.includes(key)) {
        return;
      }
      if (isArray(value)) {
        value.forEach((v, index) => {
          actionSubActionCompletionsWrapper.push({
            action: new SolidifyApplicationAppAction.LoadBackendModuleVersion(key + "-" + (index + 1), v),
            subActionCompletions: [
              this._actions$.pipe(ofSolidifyActionCompleted(SolidifyApplicationAppAction.LoadBackendModuleVersionSuccess)),
              this._actions$.pipe(ofSolidifyActionCompleted(SolidifyApplicationAppAction.LoadBackendModuleVersionFail)),
            ],
          });
        });
      } else {
        actionSubActionCompletionsWrapper.push({
          action: new SolidifyApplicationAppAction.LoadBackendModuleVersion(key, value),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SolidifyApplicationAppAction.LoadBackendModuleVersionSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SolidifyApplicationAppAction.LoadBackendModuleVersionFail)),
          ],
        });
      }
    });
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(this._store, actionSubActionCompletionsWrapper).pipe(
      map(result => {
        if (isFalse(result.success)) {
          ctx.dispatch(new SolidifyApplicationAppAction.LoadBackendModulesVersionFail(action));
          return false;
        }
        ctx.dispatch(new SolidifyApplicationAppAction.LoadBackendModulesVersionSuccess(action, ctx.getState().backendModulesVersion));
        return true;
      }),
    );
  }

  @RegisterDefaultAction((solidifyApplicationAppNameSpace: SolidifyApplicationAppNameSpace) => solidifyApplicationAppNameSpace.LoadBackendModulesVersionSuccess, {}, SolidifyAppState)
  loadBackendModulesVersionSuccess(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyApplicationAppAction.LoadBackendModulesVersionSuccess): void {
    const modulesVersion = MappingObjectUtil.copy(ctx.getState().backendModulesVersion);
    ctx.patchState({
      backendModulesVersion: modulesVersion,
    });
  }

  @RegisterDefaultAction((solidifyApplicationAppNameSpace: SolidifyApplicationAppNameSpace) => solidifyApplicationAppNameSpace.LoadBackendModulesVersionFail, {}, SolidifyAppState)
  loadBackendModulesVersionFail(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyApplicationAppAction.LoadBackendModulesVersionFail): void {
  }

  @RegisterDefaultAction((solidifyApplicationAppNameSpace: SolidifyApplicationAppNameSpace) => solidifyApplicationAppNameSpace.LoadBackendModuleVersion, {}, SolidifyAppState)
  loadBackendModuleVersion(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyApplicationAppAction.LoadBackendModuleVersion): Observable<BackendModuleVersion> {
    const urlInfo = SolidifyAppState.getModuleInfoUrl(action.moduleName, action.moduleUrl);

    const STATE_KEY = makeStateKey<BackendModuleVersion>(`ApplicationAppStateLoadBackendModuleVersion-${action.moduleName}`);
    if (isNotNullNorUndefined(this._transferState) && SsrUtil.isBrowser && this._transferState.hasKey(STATE_KEY)) {
      const backendModuleVersion = this._transferState.get(STATE_KEY, {});
      ctx.dispatch(new SolidifyApplicationAppAction.LoadBackendModuleVersionSuccess(action, backendModuleVersion));
      this._transferState.remove(STATE_KEY);
      return of(backendModuleVersion);
    }

    const headers = this._computeHttpHeadersForLoadBackendModuleVersion();
    return this._httpClient.get<BackendModuleVersion>(urlInfo, {
      headers: headers,
    }).pipe(
      tap(backendModuleVersion => {
        backendModuleVersion.urlInfo = urlInfo;
        backendModuleVersion.urlDashboard = SolidifyAppState.getModuleInfoDashboard(action.moduleName, action.moduleUrl);
        this._transferState.set(STATE_KEY, backendModuleVersion);
        ctx.dispatch(new SolidifyApplicationAppAction.LoadBackendModuleVersionSuccess(action, backendModuleVersion));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new SolidifyApplicationAppAction.LoadBackendModuleVersionFail(action));
        throw this._environment.errorToSkipInErrorHandler;
      }),
    );
  }

  @RegisterDefaultAction((solidifyApplicationAppNameSpace: SolidifyApplicationAppNameSpace) => solidifyApplicationAppNameSpace.LoadBackendModuleVersionSuccess, {}, SolidifyAppState)
  loadBackendModuleVersionSuccess(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyApplicationAppAction.LoadBackendModuleVersionSuccess): void {
    const mappingBackendModulesVersion = MappingObjectUtil.copy(ctx.getState().backendModulesVersion);
    const backendModuleVersion = ObjectUtil.clone(action.backendModuleVersion);
    backendModuleVersion.name = action.parentAction.moduleName;
    backendModuleVersion.url = action.parentAction.moduleUrl;
    MappingObjectUtil.set(mappingBackendModulesVersion, action.parentAction.moduleName, backendModuleVersion);
    ctx.patchState({
      backendModulesVersion: mappingBackendModulesVersion,
    });
  }

  @RegisterDefaultAction((solidifyApplicationAppNameSpace: SolidifyApplicationAppNameSpace) => solidifyApplicationAppNameSpace.LoadBackendModuleVersionFail, {}, SolidifyAppState)
  loadBackendModuleVersionFail(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyApplicationAppAction.LoadBackendModuleVersionFail): void {
  }

  doChangeAppLanguageSuccess(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyAppAction.ChangeAppLanguageSuccess): any {
    super.doChangeAppLanguageSuccess(ctx, action);
  }

  override doChangeAppTheme(ctx: SolidifyStateContext<SolidifyApplicationAppStateModel>, action: SolidifyAppAction.ChangeAppTheme): Observable<boolean> {
    return super.doChangeAppTheme(ctx, action);
  }
}
