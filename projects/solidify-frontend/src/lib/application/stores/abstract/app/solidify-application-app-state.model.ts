/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-application-app-state.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MappingObject} from "../../../../core-resources/types/mapping-type.type";
import {BackendModuleVersion} from "../../../../core/models/backend-modules/backend-module-version.model";
import {SolidifyAppStateModel} from "../../../../core/stores/abstract/app/app-state.model";
import {SolidifyAppUserStateModel} from "../../../../user/stores/abstract/app-user/app-user-state.model";
import {StateApplicationEnum} from "../../../enums/state-application.enum";
import {AppBannerStateModel} from "../../banner/app-banner.state";
import {AppCarouselStateModel} from "../../carousel/app-carousel.state";

export interface SolidifyApplicationAppStateModel extends SolidifyAppStateModel {
  [StateApplicationEnum.application_user]?: SolidifyAppUserStateModel | undefined;
  backendModulesVersion: MappingObject<string, BackendModuleVersion>;
  backendModulesUrl: MappingObject<string, string | string[]>;
  [StateApplicationEnum.application_carousel]?: AppCarouselStateModel;
  [StateApplicationEnum.application_banner]?: AppBannerStateModel;
}
