/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - label-translate-application.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MARK_AS_TRANSLATABLE} from "../core-resources/utils/translate.util";

export interface LabelSolidifyApplication {
  applicationAboutInjectedName?: string;
  applicationAccessToken?: string;
  applicationAlreadyUsed?: string;
  applicationAppBackToOnline?: string;
  applicationAppOffline?: string;
  applicationArkButtonNavigateToTheArk?: string;
  applicationArkButtonCopyArk?: string;
  applicationArkButtonCopyUrlArk?: string;
  applicationArkNotificationArkCopyToClipboard?: string;
  applicationArkNotificationUrlArkCopyToClipboard?: string;
  applicationBackToList?: string;
  applicationBackToAdmin?: string;
  applicationChangelog?: string;
  applicationChangeTime?: string;
  applicationCreate?: string;
  applicationCreated?: string;
  applicationCreatedBy?: string;
  applicationDelete?: string;
  applicationDescription?: string;
  applicationDetail?: string;
  applicationDoiButtonCopyLongDoi?: string;
  applicationDoiButtonCopyShortDoi?: string;
  applicationDoiButtonCopyUrlLongDoi?: string;
  applicationDoiButtonCopyUrlShortLongDoi?: string;
  applicationDoiButtonNavigateToTheDoi?: string;
  applicationDoiNotificationShortDoiCopyToClipboard?: string;
  applicationDoiNotificationLongDoiCopyToClipboard?: string;
  applicationDoiNotificationUrlShortDoiCopyToClipboard?: string;
  applicationDoiNotificationUrlLongDoiCopyToClipboard?: string;
  applicationEdit?: string;
  applicationExitEditMode?: string;
  applicationExpiration?: string;
  applicationFileUploadTileButtonRetry?: string;
  applicationFileUploadTileButtonCancel?: string;
  applicationFolderTreeButtonCollapseAll?: string;
  applicationFolderTreeButtonDownloadFolder?: string;
  applicationFolderTreeButtonDeleteFolder?: string;
  applicationFolderTreeButtonExpandAll?: string;
  applicationFolderTreeButtonUploadData?: string;
  applicationPageNotFoundTitle?: string;
  applicationPageNotFoundButtonBackToHome?: string;
  applicationTokenType?: string;
  applicationTokenTypeStandard?: string;
  applicationTokenTypeMfa?: string;
  applicationNavigateToChangelog?: string;
  applicationNavigateToReleaseNotes?: string;
  applicationSave?: string;
  applicationStatus?: string;
  applicationStatusHistoryDialogTitle?: string;
  applicationTablePersonResourceRoleAdd?: string;
  applicationTablePersonResourceRoleDelete?: string;
  applicationTokenDialogCopyToClipboard?: string;
  applicationTokenDialogLogInMfa?: string;
  applicationTokenDialogTitle?: string;
  applicationTokenDialogTokenCopiedToClipboardFail?: string;
  applicationTokenDialogTokenCopiedToClipboardSuccess?: string;
  applicationUpdated?: string;
  applicationUpdatedBy?: string;
  applicationUser?: string;
  applicationYouAreInEditMode?: string;
}

export const labelSolidifyApplication: LabelSolidifyApplication = {
  applicationAboutInjectedName: MARK_AS_TRANSLATABLE("solidify.application.aboutInjectedName"),
  applicationAccessToken: MARK_AS_TRANSLATABLE("solidify.application.accessToken"),
  applicationAlreadyUsed: MARK_AS_TRANSLATABLE("solidify.application.alreadyUsed"),
  applicationAppBackToOnline: MARK_AS_TRANSLATABLE("solidify.application.appBackToOnline"),
  applicationAppOffline: MARK_AS_TRANSLATABLE("solidify.application.appOffline"),
  applicationArkButtonNavigateToTheArk: MARK_AS_TRANSLATABLE("solidify.application.ark.button.navigateToTheArk"),
  applicationArkButtonCopyArk: MARK_AS_TRANSLATABLE("solidify.application.ark.button.copyArk"),
  applicationArkButtonCopyUrlArk: MARK_AS_TRANSLATABLE("solidify.application.ark.button.copyUrlArk"),
  applicationArkNotificationArkCopyToClipboard: MARK_AS_TRANSLATABLE("solidify.application.ark.notification.arkCopyToClipboard"),
  applicationArkNotificationUrlArkCopyToClipboard: MARK_AS_TRANSLATABLE("solidify.application.ark.notification.urlArkCopyToClipboard"),
  applicationBackToList: MARK_AS_TRANSLATABLE("solidify.application.backToList"),
  applicationBackToAdmin: MARK_AS_TRANSLATABLE("solidify.application.backToAdmin"),
  applicationChangelog: MARK_AS_TRANSLATABLE("solidify.application.changelog"),
  applicationChangeTime: MARK_AS_TRANSLATABLE("solidify.application.changeTime"),
  applicationCreate: MARK_AS_TRANSLATABLE("solidify.application.create"),
  applicationCreated: MARK_AS_TRANSLATABLE("solidify.application.created"),
  applicationCreatedBy: MARK_AS_TRANSLATABLE("solidify.application.createdBy"),
  applicationDelete: MARK_AS_TRANSLATABLE("solidify.application.delete"),
  applicationDescription: MARK_AS_TRANSLATABLE("solidify.application.description"),
  applicationDetail: MARK_AS_TRANSLATABLE("solidify.application.detail"),
  applicationDoiButtonCopyLongDoi: MARK_AS_TRANSLATABLE("solidify.application.doi.button.copyLongDoi"),
  applicationDoiButtonCopyShortDoi: MARK_AS_TRANSLATABLE("solidify.application.doi.button.copyShortDoi"),
  applicationDoiButtonCopyUrlLongDoi: MARK_AS_TRANSLATABLE("solidify.application.doi.button.copyUrlLongDoi"),
  applicationDoiButtonCopyUrlShortLongDoi: MARK_AS_TRANSLATABLE("solidify.application.doi.button.copyUrlShortLongDoi"),
  applicationDoiButtonNavigateToTheDoi: MARK_AS_TRANSLATABLE("solidify.application.doi.button.navigateToTheDoi"),
  applicationDoiNotificationShortDoiCopyToClipboard: MARK_AS_TRANSLATABLE("solidify.application.doi.notification.shortDoiCopyToClipboard"),
  applicationDoiNotificationLongDoiCopyToClipboard: MARK_AS_TRANSLATABLE("solidify.application.doi.notification.longDoiCopyToClipboard"),
  applicationDoiNotificationUrlShortDoiCopyToClipboard: MARK_AS_TRANSLATABLE("solidify.application.doi.notification.urlShortDoiCopyToClipboard"),
  applicationDoiNotificationUrlLongDoiCopyToClipboard: MARK_AS_TRANSLATABLE("solidify.application.doi.notification.urlLongDoiCopyToClipboard"),
  applicationEdit: MARK_AS_TRANSLATABLE("solidify.application.edit"),
  applicationExitEditMode: MARK_AS_TRANSLATABLE("solidify.application.exitEditMode"),
  applicationExpiration: MARK_AS_TRANSLATABLE("solidify.application.expiration"),
  applicationFileUploadTileButtonRetry: MARK_AS_TRANSLATABLE("solidify.application.fileUploadTile.button.retry"),
  applicationFileUploadTileButtonCancel: MARK_AS_TRANSLATABLE("solidify.application.fileUploadTile.button.cancel"),
  applicationFolderTreeButtonCollapseAll: MARK_AS_TRANSLATABLE("solidify.application.folderTree.button.collapseAll"),
  applicationFolderTreeButtonDownloadFolder: MARK_AS_TRANSLATABLE("solidify.application.folderTree.button.downloadFolder"),
  applicationFolderTreeButtonDeleteFolder: MARK_AS_TRANSLATABLE("solidify.application.folderTree.button.deleteFolder"),
  applicationFolderTreeButtonUploadData: MARK_AS_TRANSLATABLE("solidify.application.folderTree.button.uploadData"),
  applicationFolderTreeButtonExpandAll: MARK_AS_TRANSLATABLE("solidify.application.folderTree.button.expandAll"),
  applicationPageNotFoundTitle: MARK_AS_TRANSLATABLE("solidify.application.pageNotFound.title"),
  applicationPageNotFoundButtonBackToHome: MARK_AS_TRANSLATABLE("solidify.application.pageNotFound.button.backToHome"),
  applicationTokenType: MARK_AS_TRANSLATABLE("solidify.application.token.type"),
  applicationTokenTypeStandard: MARK_AS_TRANSLATABLE("solidify.application.token.typeStandard"),
  applicationTokenTypeMfa: MARK_AS_TRANSLATABLE("solidify.application.token.typeMfa"),
  applicationNavigateToChangelog: MARK_AS_TRANSLATABLE("solidify.application.navigateToChangelog"),
  applicationNavigateToReleaseNotes: MARK_AS_TRANSLATABLE("solidify.application.navigateToReleaseNotes"),
  applicationSave: MARK_AS_TRANSLATABLE("solidify.application.save"),
  applicationStatus: MARK_AS_TRANSLATABLE("solidify.application.status"),
  applicationStatusHistoryDialogTitle: MARK_AS_TRANSLATABLE("solidify.application.statusHistoryDialogTitle"),
  applicationTablePersonResourceRoleAdd: MARK_AS_TRANSLATABLE("solidify.application.tablePersonResourceRole.add"),
  applicationTablePersonResourceRoleDelete: MARK_AS_TRANSLATABLE("solidify.application.tablePersonResourceRole.delete"),
  applicationTokenDialogCopyToClipboard: MARK_AS_TRANSLATABLE("solidify.application.tokenDialog.copyToClipboard"),
  applicationTokenDialogLogInMfa: MARK_AS_TRANSLATABLE("solidify.application.tokenDialog.logInMfa"),
  applicationTokenDialogTitle: MARK_AS_TRANSLATABLE("solidify.application.tokenDialog.title"),
  applicationTokenDialogTokenCopiedToClipboardFail: MARK_AS_TRANSLATABLE("solidify.application.tokenDialog.copiedToClipboardFail"),
  applicationTokenDialogTokenCopiedToClipboardSuccess: MARK_AS_TRANSLATABLE("solidify.application.tokenDialog.copiedToClipboardSuccess"),
  applicationUpdated: MARK_AS_TRANSLATABLE("solidify.application.updated"),
  applicationUpdatedBy: MARK_AS_TRANSLATABLE("solidify.application.updatedBy"),
  applicationUser: MARK_AS_TRANSLATABLE("solidify.application.user"),
  applicationYouAreInEditMode: MARK_AS_TRANSLATABLE("solidify.application.youAreInEditMode"),
};
