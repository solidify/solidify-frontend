/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - color-check.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  distinctUntilChanged,
  filter,
  skip,
  tap,
} from "rxjs/operators";
import {isNotNullNorUndefinedNorWhiteString} from "../../../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../../../core-resources/utils/ssr.util";
import {AbstractInternalRoutable} from "../../../../core/components/routables/abstract-internal/abstract-internal.routable";
import {ThemePartialEnum} from "../../../../core/enums/partial/theme-partial.enum";
import {FormValidationHelper} from "../../../../core/helpers/form-validation.helper";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {BaseFormDefinition} from "../../../../core/models/forms/base-form-definition.model";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {MemoizedUtil} from "../../../../core/utils/stores/memoized.util";
import {SolidifyValidator} from "../../../../core/utils/validations/validation.util";
import {DefaultSolidifyApplicationEnvironment} from "../../../environments/environment.solidify-application";

@Component({
  selector: "solidify-color-check-routable",
  templateUrl: "./color-check.routable.html",
  styleUrls: ["./color-check.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ColorCheckRoutable extends AbstractInternalRoutable implements OnInit {
  formDefinition: FormComponentFormDefinition;
  form: FormGroup;

  readonly LABEL_RELATIVE_LUMINANCE: string = "Relative Luminance";
  readonly LABEL_CONTRAST_RATIO: string = "Contrast Ratio";
  readonly LABEL_RANKING: string = "Ranking";
  readonly LABEL_WCAG2_0: string = "WCAG 2.0";

  private _defaultColorPrimary: string;
  private _defaultColorPrimaryLighter: string;
  private _defaultColorAccent: string;
  private _defaultColorTextInPrimary: string;

  private readonly _DEFAULT_COLOR_BACKGROUND_LIGHT: string = "#ffffff";
  private readonly _DEFAULT_COLOR_BACKGROUND_DARK: string = "#303030";
  private readonly _DEFAULT_COLOR_INFO: string = "#3598db";
  private readonly _DEFAULT_COLOR_WARN: string = "#db7e06";
  private readonly _DEFAULT_COLOR_ERROR: string = "#e84c3d";
  private readonly _DEFAULT_COLOR_SUCCESS: string = "#15a796";

  listPickerInputs: InputColorPicker[];

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  get backgroundColor(): string {
    return this.form.get(this.formDefinition.backgroundColor).value;
  }

  get buttonFlatTextColor(): string {
    return this.form.get(this.formDefinition.buttonFlatTextColor).value;
  }

  get primaryColor(): string {
    return this.form.get(this.formDefinition.primaryColor).value;
  }

  get primaryColorLighter(): string {
    return this.form.get(this.formDefinition.primaryColorLighter).value;
  }

  get accentColor(): string {
    return this.form.get(this.formDefinition.accentColor).value;
  }

  get infoColor(): string {
    return this.form.get(this.formDefinition.infoColor).value;
  }

  get warnColor(): string {
    return this.form.get(this.formDefinition.warnColor).value;
  }

  get errorColor(): string {
    return this.form.get(this.formDefinition.errorColor).value;
  }

  get successColor(): string {
    return this.form.get(this.formDefinition.successColor).value;
  }

  get listThemeColors(): string[] {
    return [
      this.primaryColor,
      this.primaryColorLighter,
      this.accentColor,
      this.infoColor,
      this.successColor,
      this.warnColor,
      this.errorColor,
    ];
  }

  _body: HTMLBodyElement;

  get ratingEnum(): typeof RatingEnum {
    return RatingEnum;
  }

  get isDarkModeEnable(): boolean {
    return MemoizedUtil.selectSnapshot(this._store, this.environment.appState, state => state.darkMode);
  }

  constructor(protected readonly _injector: Injector,
              private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _fb: FormBuilder,
              private readonly _changeDetector: ChangeDetectorRef,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyApplicationEnvironment) {
    super(_injector);
    this.formDefinition = new FormComponentFormDefinition();
    this.listPickerInputs = [
      {
        formDefinition: this.formDefinition.primaryColor,
        label: "Primary",
      },
      {
        formDefinition: this.formDefinition.primaryColorLighter,
        label: "Primary Lighter",
      },
      {
        formDefinition: this.formDefinition.accentColor,
        label: "Accent",
      },
      {
        formDefinition: this.formDefinition.infoColor,
        label: "Info",
      },
      {
        formDefinition: this.formDefinition.successColor,
        label: "Success",
      },
      {
        formDefinition: this.formDefinition.warnColor,
        label: "Warn",
      },
      {
        formDefinition: this.formDefinition.errorColor,
        label: "Error",
      },
    ];
  }

  readonly _TEXT_COLOR_IN_PRIMARY: string = "--text-color-in-primary";
  readonly _PRIMARY_COLOR: string = "--primary-color";
  readonly _PRIMARY_COLOR_LIGHTER: string = "--primary-color-lighter";
  readonly _ACCENT_COLOR: string = "--accent-color";

  ngOnInit(): void {
    super.ngOnInit();

    this._body = SsrUtil.window.document.querySelector("body");
    this._reloadColorsVariableFromCss();

    this.subscribe(MemoizedUtil.select(this._store, this.environment.appState, state => state.darkMode).pipe(
        distinctUntilChanged(),
        skip(1),
        tap((isEnabled: boolean) => {
          this._resetBackgroundColor();
          this._changeDetector.detectChanges();
        }),
      ),
    );

    this.subscribe(MemoizedUtil.select(this._store, this.environment.appState, state => state.theme).pipe(
        filter(theme => isNotNullNorUndefinedNorWhiteString(theme)),
        distinctUntilChanged(),
        skip(1),
        tap((theme: ExtendEnum<ThemePartialEnum>) => {
          this.reloadAllColors();
          this._changeDetector.detectChanges();
        }),
      ),
    );

    this.form = this._fb.group({
      [this.formDefinition.backgroundColor]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.buttonFlatTextColor]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.primaryColor]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.primaryColorLighter]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.accentColor]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.infoColor]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.warnColor]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.errorColor]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.successColor]: ["", [Validators.required, SolidifyValidator]],
    });

    this._resetFormColors();
  }

  colorChange(formControl: FormControl, $event: string): void {
    formControl.setValue($event);
  }

  reloadAllColors(): void {
    this._reloadColorsVariableFromCss();
    this._resetFormColors();
  }

  private _reloadColorsVariableFromCss(): void {
    const computedStyle = SsrUtil.window?.getComputedStyle(this._body);
    this._defaultColorTextInPrimary = computedStyle.getPropertyValue(this._TEXT_COLOR_IN_PRIMARY);
    this._defaultColorPrimary = computedStyle.getPropertyValue(this._PRIMARY_COLOR);
    this._defaultColorPrimaryLighter = computedStyle.getPropertyValue(this._PRIMARY_COLOR_LIGHTER);
    this._defaultColorAccent = computedStyle.getPropertyValue(this._ACCENT_COLOR);
  }

  private _resetFormColors(): void {
    this._resetBackgroundColor();
    this.form.get(this.formDefinition.buttonFlatTextColor).setValue(this._defaultColorTextInPrimary);
    this.form.get(this.formDefinition.primaryColor).setValue(this._defaultColorPrimary);
    this.form.get(this.formDefinition.primaryColorLighter).setValue(this._defaultColorPrimaryLighter);
    this.form.get(this.formDefinition.accentColor).setValue(this._defaultColorAccent);
    this.form.get(this.formDefinition.infoColor).setValue(this._DEFAULT_COLOR_INFO);
    this.form.get(this.formDefinition.warnColor).setValue(this._DEFAULT_COLOR_WARN);
    this.form.get(this.formDefinition.errorColor).setValue(this._DEFAULT_COLOR_ERROR);
    this.form.get(this.formDefinition.successColor).setValue(this._DEFAULT_COLOR_SUCCESS);
  }

  private _resetBackgroundColor(): void {
    this.form.get(this.formDefinition.backgroundColor)
      .setValue(this.isDarkModeEnable ? this._DEFAULT_COLOR_BACKGROUND_DARK : this._DEFAULT_COLOR_BACKGROUND_LIGHT);
  }

  getRelativeLuminance(color: string): number {
    // https://www.w3.org/WAI/GL/wiki/Relative_luminance
    const RsRGB = parseInt(color.substring(1, 3), 16) / 255;
    const GsRGB = parseInt(color.substring(3, 5), 16) / 255;
    const BsRGB = parseInt(color.substring(5, 7), 16) / 255;
    const R = this.#computeColorChannel(RsRGB);
    const G = this.#computeColorChannel(GsRGB);
    const B = this.#computeColorChannel(BsRGB);
    return this.#roundNumber(0.2126 * R + 0.7152 * G + 0.0722 * B, 4);
  }

  #computeColorChannel(colorValue: number): number {
    if (colorValue <= 0.03928) {
      return colorValue / 12.92;
    } else {
      return Math.pow((colorValue + 0.055) / 1.055, 2.4);
    }
  }

  #roundNumber(value: number, decNumber: number): number {
    const f = Math.pow(10, decNumber);
    return Math.round(value * f) / f;
  }

  getContrastRatio(color1: string, color2: string): number {
    const rl1 = this.getRelativeLuminance(color1);
    const rl2 = this.getRelativeLuminance(color2);
    if (rl1 > rl2) {
      return this.#roundNumber((rl1 + 0.05) / (rl2 + 0.05), 2);
    } else {
      return this.#roundNumber((rl2 + 0.05) / (rl1 + 0.05), 2);
    }
  }

  getRating(color1: string, color2: string): RatingEnum {
    const constrastRatio = this.getContrastRatio(color1, color2);
    if (constrastRatio >= 7) {
      return RatingEnum.AAA;
    } else if (constrastRatio >= 4.5) {
      return this.ratingEnum.AA;
    } else if (constrastRatio >= 3) {
      return RatingEnum.AA18;
    }
    return RatingEnum.Failed;
  }

  navigateToWcag(): void {
    SsrUtil.window.open(this._environment.wcag2Link);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  backgroundColor: string = "backgroundColor";
  buttonFlatTextColor: string = "buttonFlatTextColor";
  primaryColor: string = "primaryColor";
  primaryColorLighter: string = "primaryColorLighter";
  accentColor: string = "accentColor";
  infoColor: string = "infoColor";
  warnColor: string = "warnColor";
  errorColor: string = "errorColor";
  successColor: string = "successColor";
}

interface InputColorPicker {
  formDefinition: string;
  label: string;
}

export type RatingEnum =
  "AAA"
  | "AA"
  | "AA18"
  | "Failed";
export const RatingEnum = {
  AAA: "AAA" as RatingEnum,
  AA: "AA" as RatingEnum,
  AA18: "AA18" as RatingEnum,
  Failed: "Failed" as RatingEnum,
};
