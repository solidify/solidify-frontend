/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-detail-edit-common.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectorRef,
  Directive,
  Injector,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  take,
  tap,
} from "rxjs/operators";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {ModelFormControlEvent} from "../../../../core/models/forms/model-form-control-event.model";
import {ResourceNameSpace} from "../../../../core/stores/abstract/resource/resource-namespace.model";
import {ResourceStateModel} from "../../../../core/stores/abstract/resource/resource-state.model";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {ofSolidifyActionCompleted} from "../../../../core/utils/stores/store.tool";
import {StateApplicationPartialEnum} from "../../../enums/partial/state-application-partial.enum";
import {AbstractDetailEditRoutable} from "../abstract-detail-edit/abstract-detail-edit.routable";

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractDetailEditCommonRoutable<TResourceModel extends BaseResourceType, UResourceStateModel extends ResourceStateModel<TResourceModel>>
  extends AbstractDetailEditRoutable<TResourceModel, UResourceStateModel> implements OnInit, OnDestroy {
  abstract readonly KEY_PARAM_NAME: keyof TResourceModel & string;

  protected constructor(protected readonly _store: Store,
                        protected readonly _route: ActivatedRoute,
                        protected readonly _actions$: Actions,
                        protected readonly _changeDetector: ChangeDetectorRef,
                        protected readonly _dialog: MatDialog,
                        protected readonly _state: ExtendEnum<StateApplicationPartialEnum>,
                        protected readonly _injector: Injector,
                        protected readonly _resourceNameSpace: ResourceNameSpace,
                        protected readonly _parentState?: ExtendEnum<StateApplicationPartialEnum>) {
    super(_store, _route, _actions$, _changeDetector, _dialog, _state, _injector, _resourceNameSpace, _parentState);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.retrieveCurrentModelAndResource();
  }

  update(model: ModelFormControlEvent<TResourceModel>): Observable<any> {
    this.subscribe(this._actions$.pipe(
      ofSolidifyActionCompleted(this._resourceNameSpace.UpdateSuccess),
      take(1),
      tap(result => {
        if (!result.result.successful) {
          return;
        }
        this.retrieveCurrentModelWithUrl();
      }),
    ));
    return super.update(model);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._cleanState();
  }
}
