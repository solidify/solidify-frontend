/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-application-maintenance-mode.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Optional,
} from "@angular/core";
import {Meta} from "@angular/platform-browser";
import {Store} from "@ngxs/store";
import {isTruthy} from "../../../../core-resources/tools/is/is.tool";
import {MaintenanceModeRoutable} from "../../../../core/components/routables/maintenance-mode/maintenance-mode.routable";
import {AppConfigService} from "../../../../core/config/app-config.service";
import {DefaultSolidifyEnvironment} from "../../../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {SolidifyGlobalBannerAction} from "../../../../global-banner/global-banner.action";
import {SECURITY_SERVICE} from "../../../security/security-service.injection-token";
import {SolidifySecurityService} from "../../../security/security.service";

@Component({
  selector: "solidify-application-maintenance-mode-routable",
  templateUrl: "../../../../core/components/routables/maintenance-mode/maintenance-mode.routable.html",
  styleUrls: ["../../../../core/components/routables/maintenance-mode/maintenance-mode.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SolidifyApplicationMaintenanceModeRoutable extends MaintenanceModeRoutable {

  constructor(protected readonly _store: Store,
              protected readonly _injector: Injector,
              protected readonly _appConfigService: AppConfigService,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment,
              protected readonly _meta: Meta,
              @Optional() @Inject(SECURITY_SERVICE) protected readonly _securityService: SolidifySecurityService) {
    super(
      _store,
      _injector,
      _appConfigService,
      _environment,
      _meta,
    );
  }

  protected override _isLoggedInRoot(): boolean {
    return isTruthy(this._securityService && this._securityService.isLoggedIn() && this._securityService.isRoot());
  }

  protected override _onRedirectToHome(): void {
    this._store.dispatch(new SolidifyGlobalBannerAction.GetGlobalBanner());
  }

}
