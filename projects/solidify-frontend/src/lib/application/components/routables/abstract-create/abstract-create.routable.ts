/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectorRef,
  Directive,
  Injector,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {MappingObject} from "../../../../core-resources/types/mapping-type.type";
import {CrudHelper} from "../../../../core/helpers/crud.helper";
import {UrlQueryParamHelper} from "../../../../core/helpers/url-query-param.helper";
import {STORE_ROUTE_SERVICE} from "../../../../core/injection-tokens/store-route-service.injection-token";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {FormControlKey} from "../../../../core/models/forms/form-control-key.model";
import {ModelFormControlEvent} from "../../../../core/models/forms/model-form-control-event.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {AbstractStoreRouteService} from "../../../../core/services/abstract-store-route-service";
import {ResourceActionHelper} from "../../../../core/stores/abstract/resource/resource-action.helper";
import {ResourceNameSpace} from "../../../../core/stores/abstract/resource/resource-namespace.model";
import {ResourceStateModel} from "../../../../core/stores/abstract/resource/resource-state.model";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {StoreUtil} from "../../../../core/utils/stores/store.util";
import {StateApplicationPartialEnum} from "../../../enums/partial/state-application-partial.enum";
import {AbstractCrudRoutable} from "../abstract-crud/abstract-crud.routable";

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractCreateRoutable<TResourceModel extends BaseResourceType, UResourceStateModel extends ResourceStateModel<TResourceModel>>
  extends AbstractCrudRoutable<TResourceModel, UResourceStateModel> implements OnInit, OnDestroy {

  protected readonly _storeRouteService: AbstractStoreRouteService;

  isLoadingObs: Observable<boolean>;
  urlQueryParameters: MappingObject<string, string | undefined>;
  checkAvailableResourceNameSpace: ResourceNameSpace | undefined;
  alreadyUsedLabelToTranslate: string = this.labelTranslateInterface.applicationAlreadyUsed;

  protected constructor(protected readonly _store: Store,
                        protected readonly _actions$: Actions,
                        protected readonly _changeDetector: ChangeDetectorRef,
                        protected readonly _state: ExtendEnum<StateApplicationPartialEnum>,
                        protected readonly _injector: Injector,
                        private readonly _resourceActionNameSpace: ResourceNameSpace,
                        protected readonly _parentState?: ExtendEnum<StateApplicationPartialEnum>) {
    super(_store, _state, _injector, _parentState);
    this._storeRouteService = _injector.get(STORE_ROUTE_SERVICE);
    this.isLoadingObs = this._store.select(s => StoreUtil.isLoadingState(super.getState(s)));
  }

  create(modelFormControlEvent: ModelFormControlEvent<TResourceModel>): void {
    super.saveInProgress();
    this._store.dispatch(ResourceActionHelper.create(this._resourceActionNameSpace, modelFormControlEvent));
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._store.dispatch(ResourceActionHelper.loadResource(this._resourceActionNameSpace));
    this._extractQueryParams();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._cleanState();
  }

  protected _cleanState(): void {
    this._store.dispatch(ResourceActionHelper.clean(this._resourceActionNameSpace, false, new QueryParameters()));
  }

  checkAvailable(formControlKey: FormControlKey): void {
    const resourceNameSpace = isNullOrUndefined(this.checkAvailableResourceNameSpace) ? this._resourceActionNameSpace : this.checkAvailableResourceNameSpace;
    this.subscribe(CrudHelper.checkAvailable(formControlKey, this._store, this._actions$, resourceNameSpace, undefined, this.alreadyUsedLabelToTranslate));
  }

  backToListNavigate(): Navigate {
    return new Navigate([this._storeRouteService.getRootRoute(this._state)]);
  }

  backToList(): void {
    this._store.dispatch(this.backToListNavigate());
  }

  protected _extractQueryParams(): void {
    this.urlQueryParameters = UrlQueryParamHelper.getQueryParamMappingObjectFromCurrentUrl();
  }
}
