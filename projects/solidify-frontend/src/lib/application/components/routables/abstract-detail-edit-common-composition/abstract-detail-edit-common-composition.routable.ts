/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-detail-edit-common-composition.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectorRef,
  Directive,
  Injector,
  OnDestroy,
  OnInit,
  Optional,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  take,
  tap,
} from "rxjs/operators";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {
  DeleteDialog,
  DeleteDialogData,
} from "../../../../core/components/dialogs/delete/delete.dialog";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {ButtonColorEnum} from "../../../../core/enums/button-color.enum";
import {AppRoutesPartialEnum} from "../../../../core/enums/partial/app-routes-partial.enum";
import {STORE_ROUTE_SERVICE} from "../../../../core/injection-tokens/store-route-service.injection-token";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {ModelFormControlEvent} from "../../../../core/models/forms/model-form-control-event.model";
import {CompositionActionHelper} from "../../../../core/stores/abstract/composition/composition-action.helper";
import {CompositionNameSpace} from "../../../../core/stores/abstract/composition/composition-namespace.model";
import {CompositionStateModel} from "../../../../core/stores/abstract/composition/composition-state.model";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {DialogUtil} from "../../../../core/utils/dialog.util";
import {UrlUtil} from "../../../../core/utils/url.util";
import {StateApplicationPartialEnum} from "../../../enums/partial/state-application-partial.enum";
import {AbstractDetailEditCommonRoutable} from "../abstract-detail-edit-common/abstract-detail-edit-common.routable";

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractDetailEditCommonCompositionRoutable<TResourceModel extends BaseResourceType, UResourceStateModel extends CompositionStateModel<TResourceModel>>
  extends AbstractDetailEditCommonRoutable<TResourceModel, any> implements OnInit, OnDestroy {

  protected constructor(protected readonly _store: Store,
                        protected readonly _route: ActivatedRoute,
                        protected readonly _actions$: Actions,
                        protected readonly _changeDetector: ChangeDetectorRef,
                        protected readonly _dialog: MatDialog,
                        protected readonly _state: ExtendEnum<StateApplicationPartialEnum>,
                        protected readonly _injector: Injector,
                        protected readonly _compositionNameSpace: CompositionNameSpace,
                        protected readonly _parentState?: ExtendEnum<StateApplicationPartialEnum>,
                        @Optional() public readonly options?: SharedAbstractDetailEditCommonCompositionRoutableOption<TResourceModel>) {
    super(_store, _route, _actions$, _changeDetector, _dialog, _state, _injector, _compositionNameSpace as any, _parentState);
    this.options = Object.assign(this._getDefaultOptions(), options);
  }

  private _getDefaultOptions(): SharedAbstractDetailEditCommonCompositionRoutableOption<TResourceModel> {
    return {};
  }

  get parentCompositionId(): string {
    const route = this._injector.get(ActivatedRoute);
    return route.snapshot.paramMap.get(AppRoutesPartialEnum.paramIdParentCompositionWithoutPrefixParam);
  }

  backToList(): void {
    const storeRouteService = this._injector.get(STORE_ROUTE_SERVICE);
    this._store.dispatch(new Navigate([storeRouteService.getRootRoute(this._state), this.parentCompositionId]));
  }

  protected retrieveResource(): void {
  }

  protected _cleanState(): void {
    const queryParameters = this._store.selectSnapshot(s => super.getState(s).queryParameters);
    this._store.dispatch(CompositionActionHelper.clean(this._resourceNameSpace, false, queryParameters));
  }

  protected getById(id: string): void {
    this._store.dispatch(CompositionActionHelper.getById(this._resourceNameSpace, this.parentCompositionId, id));
    this._getSubResourceWithParentId(id);
  }

  edit(): void {
    if (this.isEdit) {
      return;
    }
    if (!this.editAvailable) {
      return;
    }
    const editRoute = this._storeRouteService.getDetailRoute(this._state) + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this._resId + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this.parentCompositionId + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this.environment.routeSegmentEdit;
    this._store.dispatch(new Navigate([editRoute], {}, {skipLocationChange: true}));
  }

  backToDetail(path: string[] = undefined): void {
    const fromUrl = UrlUtil.getCurrentUrlFromStoreRouterState(this._store);
    this.subscribe(this._store.dispatch(new Navigate(isNullOrUndefined(path) ? [this._storeRouteService.getDetailRoute(this._state), this._resId, this.parentCompositionId] : path))
      .pipe(
        tap(() => {
          const toUrl = UrlUtil.getCurrentUrlFromStoreRouterState(this._store);
          if (toUrl !== fromUrl) {
            // this.formPresentational.resetFormToInitialValue();
            // TODO : Fix don't need to get by id model from backend but fix resetFormToInitialValue with component user role org unit
            // TODO : Problem currently if redirect to detail page via breadcrumb in case of Deposit
            this.retrieveCurrentModelWithUrl();
          }
        }),
      ));
  }

  delete(): void {
    const data = {} as DeleteDialogData;
    this.subscribe(
      this.currentObs.pipe(take(1)),
      (model: TResourceModel) => {
        data.resId = model.resId;
        data.parentId = this.parentCompositionId;
        data.name = model[this.KEY_PARAM_NAME]?.toString();
        data.message = this.options.messageDialogDelete;
        data.compositionNameSpace = this._compositionNameSpace;
        data.colorConfirm = ButtonColorEnum.warn;
        data.colorCancel = ButtonColorEnum.primary;
      });
    const dialogRef = DialogUtil.open(this._dialog, DeleteDialog, data, {
      width: "400px",
    });
  }

  update(model: ModelFormControlEvent<TResourceModel>): Observable<any> {
    super.saveInProgress();
    return this._store.dispatch(CompositionActionHelper.update<TResourceModel>(this._resourceNameSpace, this.parentCompositionId, model.model));
  }
}

export interface SharedAbstractDetailEditCommonCompositionRoutableOption<TResourceModel> {
  messageDialogDelete?: string;
}
