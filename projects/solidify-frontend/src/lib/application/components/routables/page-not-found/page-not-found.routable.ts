/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - page-not-found.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
} from "@angular/core";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {AbstractInternalRoutable} from "../../../../core/components/routables/abstract-internal/abstract-internal.routable";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {DefaultSolidifyApplicationEnvironment} from "../../../environments/environment.solidify-application";

@Component({
  selector: "solidify-page-not-found-routable",
  templateUrl: "./page-not-found.routable.html",
  styleUrls: ["./page-not-found.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageNotFoundRoutable extends AbstractInternalRoutable {
  constructor(protected readonly _injector: Injector,
              private readonly _store: Store,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyApplicationEnvironment) {
    super(_injector);
  }

  backToHome(): void {
    this._store.dispatch(new Navigate([this._environment.routeHomePage]));
  }
}
