/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-resource.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  Injector,
} from "@angular/core";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {AbstractInternalRoutable} from "../../../../core/components/routables/abstract-internal/abstract-internal.routable";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {ResourceStateModel} from "../../../../core/stores/abstract/resource/resource-state.model";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {StateApplicationPartialEnum} from "../../../enums/partial/state-application-partial.enum";

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractResourceRoutable<TResourceModel extends BaseResourceType, UResourceStateModel extends ResourceStateModel<TResourceModel>>
  extends AbstractInternalRoutable {
  protected constructor(protected readonly _state: ExtendEnum<StateApplicationPartialEnum>,
                        protected readonly _injector: Injector,
                        protected readonly _parentState?: ExtendEnum<StateApplicationPartialEnum>) {
    super(_injector);
  }

  protected getState(rootStateModel: any): UResourceStateModel {
    if (isNullOrUndefined(this._parentState)) {
      return rootStateModel[this._state];
    }
    const state = rootStateModel[this._parentState][this._state];
    if (isNullOrUndefined(state)) {
      // eslint-disable-next-line no-console
      console.error("There is no state corresponding to path " + this._parentState + " > " + this._state);
    }
    return state;
  }
}
