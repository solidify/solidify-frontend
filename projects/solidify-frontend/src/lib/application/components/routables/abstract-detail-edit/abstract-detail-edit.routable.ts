/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectorRef,
  Directive,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  RouterStateSnapshot,
} from "@angular/router";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  map,
  take,
  tap,
} from "rxjs/operators";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {MappingObject} from "../../../../core-resources/types/mapping-type.type";
import {DeleteDialog} from "../../../../core/components/dialogs/delete/delete.dialog";
import {AbstractFormPresentational} from "../../../../core/components/presentationals/abstract-form/abstract-form.presentational";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {CrudHelper} from "../../../../core/helpers/crud.helper";
import {UrlQueryParamHelper} from "../../../../core/helpers/url-query-param.helper";
import {STORE_DIALOG_SERVICE} from "../../../../core/injection-tokens/store-dialog-service.injection-token";
import {STORE_ROUTE_SERVICE} from "../../../../core/injection-tokens/store-route-service.injection-token";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {FormControlKey} from "../../../../core/models/forms/form-control-key.model";
import {ModelFormControlEvent} from "../../../../core/models/forms/model-form-control-event.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {StateModel} from "../../../../core/models/stores/state.model";
import {AbstractStoreDialogService} from "../../../../core/services/abstract-store-dialog.service";
import {AbstractStoreRouteService} from "../../../../core/services/abstract-store-route-service";
import {ResourceActionHelper} from "../../../../core/stores/abstract/resource/resource-action.helper";
import {ResourceNameSpace} from "../../../../core/stores/abstract/resource/resource-namespace.model";
import {ResourceStateModel} from "../../../../core/stores/abstract/resource/resource-state.model";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {DialogUtil} from "../../../../core/utils/dialog.util";
import {MemoizedUtil} from "../../../../core/utils/stores/memoized.util";
import {StoreUtil} from "../../../../core/utils/stores/store.util";
import {UrlUtil} from "../../../../core/utils/url.util";
import {StateApplicationPartialEnum} from "../../../enums/partial/state-application-partial.enum";
import {AppBannerAction} from "../../../stores/banner/app-banner.action";
import {AppBannerState} from "../../../stores/banner/app-banner.state";
import {AbstractCrudRoutable} from "../abstract-crud/abstract-crud.routable";

// @dynamic
@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractDetailEditRoutable<TResourceModel extends BaseResourceType, UResourceStateModel extends ResourceStateModel<TResourceModel>>
  extends AbstractCrudRoutable<TResourceModel, UResourceStateModel> implements OnInit, OnDestroy {

  protected readonly _storeRouteService: AbstractStoreRouteService;
  protected readonly _storeDialogService: AbstractStoreDialogService;

  urlStateObs: Observable<RouterStateSnapshot>;

  checkAvailableResourceNameSpace: ResourceNameSpace | undefined;
  isLoadingObs: Observable<boolean>;
  queryParametersObs: Observable<QueryParameters>;
  currentObs: Observable<TResourceModel>;
  current: TResourceModel;

  @ViewChild("formPresentational", {static: true})
  declare readonly formPresentational: AbstractFormPresentational<any>;

  abstract readonly KEY_PARAM_NAME: keyof TResourceModel & string;

  protected _resId: string;

  isEdit: boolean;
  isEditObs: Observable<boolean>;
  urlQueryParameters: MappingObject<string, string | undefined>;

  getByIdIfAlreadyInState: boolean = true;
  editAvailable: boolean = true;
  deleteAvailable: boolean = true;
  alreadyUsedLabelToTranslate: string = this.labelTranslateInterface.applicationAlreadyUsed;

  protected constructor(protected readonly _store: Store,
                        protected readonly _route: ActivatedRoute,
                        protected readonly _actions$: Actions,
                        protected readonly _changeDetector: ChangeDetectorRef,
                        protected readonly _dialog: MatDialog,
                        protected readonly _state: ExtendEnum<StateApplicationPartialEnum>,
                        protected readonly _injector: Injector,
                        protected readonly _resourceNameSpace: ResourceNameSpace,
                        protected readonly _parentState?: ExtendEnum<StateApplicationPartialEnum>) {
    super(_store, _state, _injector, _parentState);
    this.urlStateObs = this._store.select((state: StateModel) => state.router.state);
    this.isLoadingObs = this._store.select(s => StoreUtil.isLoadingState(super.getState(s)));
    this.queryParametersObs = this._store.select(s => super.getState(s).queryParameters);
    this.currentObs = this._store.select(s => super.getState(s).current).pipe(
      tap(current => this.current = current),
    );
    this._storeRouteService = _injector.get(STORE_ROUTE_SERVICE);
    this._storeDialogService = _injector.get(STORE_DIALOG_SERVICE);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribe(this.getRetrieveEditModeObs());
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  protected retrieveCurrentModelAndResource(): void {
    this.retrieveResource();
    this.retrieveCurrentModelWithUrl();
  }

  protected retrieveResource(): void {
    this._store.dispatch(ResourceActionHelper.loadResource(this._resourceNameSpace));
  }

  protected _cleanState(): void {
    const queryParameters = this._store.selectSnapshot(s => super.getState(s).queryParameters);
    this._store.dispatch(ResourceActionHelper.clean(this._resourceNameSpace, false, queryParameters));
  }

  protected _retrieveResIdFromUrl(): void {
    this._resId = this._route.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    if (isNullOrUndefined(this._resId)) {
      this._resId = this._route.parent.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    }
  }

  protected retrieveCurrentModelWithUrl(): void {
    this._retrieveResIdFromUrl();
    if (this.getByIdIfAlreadyInState || isNullOrUndefined(this.current) || this.current?.resId !== this._resId) {
      this.getById(this._resId);
    }
  }

  protected getById(id: string): void {
    this._store.dispatch(ResourceActionHelper.getById(this._resourceNameSpace, id));
    this._getSubResourceWithParentId(id);
  }

  protected abstract _getSubResourceWithParentId(id: string): void;

  protected getRetrieveEditModeObs(): Observable<boolean> {
    this.isEditObs = this.urlStateObs.pipe(
      map(url => url.url),
      distinctUntilChanged(),
      map(urlRaw => {
        const urlSplitted = urlRaw.split("?");
        const url = urlSplitted[0];
        this._extractQueryParams(urlSplitted.length > 1 ? urlSplitted[1] : undefined);
        this.isEdit = url.endsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + this.environment.routeSegmentEdit);
        this._changeDetector.detectChanges();
        const bannerStateModel = MemoizedUtil.selectSnapshot(this._store, AppBannerState, state => state);
        if (this.isEdit) {
          this._store.dispatch(new AppBannerAction.ShowInEditMode());
        } else if (bannerStateModel.message === this.labelTranslateInterface.applicationYouAreInEditMode) {
          this._store.dispatch(new AppBannerAction.Hide());
        }
        return this.isEdit;
      }),
    );
    return this.isEditObs;
  }

  edit(): void {
    if (this.isEdit) {
      return;
    }
    if (!this.editAvailable) {
      return;
    }
    this._store.dispatch(new Navigate([this._storeRouteService.getEditRoute(this._state, this._resId)], {}, {skipLocationChange: true}));
  }

  update(model: ModelFormControlEvent<TResourceModel>): Observable<any> {
    super.saveInProgress();
    return this._store.dispatch(ResourceActionHelper.update<TResourceModel>(this._resourceNameSpace, model));
  }

  delete(): void {
    const data = this._storeDialogService.deleteData(this._state);
    this.subscribe(
      this.currentObs.pipe(take(1)),
      (model: TResourceModel) => {
        data.name = model[this.KEY_PARAM_NAME]?.toString();
        data.resId = model.resId;
      });
    const dialogRef = DialogUtil.open(this._dialog, DeleteDialog, data, {
      width: "400px",
    });
  }

  backToListNavigate(): Navigate {
    return new Navigate([this._storeRouteService.getRootRoute(this._state)]);
  }

  backToList(): void {
    this._store.dispatch(this.backToListNavigate());
  }

  backToDetail(path: string[] = undefined): void {
    const fromUrl = UrlUtil.getCurrentUrlFromStoreRouterState(this._store);
    this.subscribe(this._store.dispatch(new Navigate(isNullOrUndefined(path) ? [this._storeRouteService.getDetailRoute(this._state), this._resId] : path))
      .pipe(
        tap(() => {
          const toUrl = UrlUtil.getCurrentUrlFromStoreRouterState(this._store);
          if (toUrl !== fromUrl) {
            // this.formPresentational.resetFormToInitialValue();
            // TODO : Fix don't need to get by id model from backend but fix resetFormToInitialValue with component user role org unit
            // TODO : Problem currently if redirect to detail page via breadcrumb in case of Deposit
            this.retrieveCurrentModelWithUrl();
          }
        }),
      ));
  }

  checkAvailable(formControlKey: FormControlKey): void {
    const resourceNameSpace = isNullOrUndefined(this.checkAvailableResourceNameSpace) ? this._resourceNameSpace : this.checkAvailableResourceNameSpace;
    this.subscribe(CrudHelper.checkAvailable(formControlKey, this._store, this._actions$, resourceNameSpace, this._resId, this.alreadyUsedLabelToTranslate));
  }

  private _extractQueryParams(rawQueryParameters: string): void {
    this.urlQueryParameters = UrlQueryParamHelper.getQueryParamMappingObjectFromQueryParamString(rawQueryParameters);
  }
}
