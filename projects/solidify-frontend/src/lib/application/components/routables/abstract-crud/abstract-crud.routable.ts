/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-crud.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  Injector,
  ViewChild,
} from "@angular/core";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {
  isArray,
  isFalse,
  isTrue,
} from "../../../../core-resources/tools/is/is.tool";
import {AbstractFormPresentational} from "../../../../core/components/presentationals/abstract-form/abstract-form.presentational";
import {ICanDeactivate} from "../../../../core/models/can-deactivate.model";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {SolidifyAppAction} from "../../../../core/stores/abstract/app/app.action";
import {ResourceStateModel} from "../../../../core/stores/abstract/resource/resource-state.model";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {StateApplicationPartialEnum} from "../../../enums/partial/state-application-partial.enum";
import {AbstractResourceRoutable} from "../abstract-resource/abstract-resource.routable"; // @dynamic

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractCrudRoutable<TResourceModel extends BaseResourceType, UResourceStateModel extends ResourceStateModel<TResourceModel>>
  extends AbstractResourceRoutable<TResourceModel, UResourceStateModel>
  implements ICanDeactivate {

  @ViewChild("formPresentational")
  readonly formPresentational: AbstractFormPresentational<TResourceModel>;

  private _canDeactivate: boolean = true;

  protected constructor(protected readonly _store: Store,
                        protected readonly _state: ExtendEnum<StateApplicationPartialEnum>,
                        protected readonly _injector: Injector,
                        protected readonly _parentState?: ExtendEnum<StateApplicationPartialEnum>) {
    super(_state, _injector, _parentState);
  }

  canDeactivate(): boolean {
    return this._canDeactivate;
  }

  updateCanDeactivate(isDirty: boolean): void {
    if (isTrue(isDirty) && isDirty === this._canDeactivate) {
      this.preventExit();
    } else if (isFalse(isDirty) && isDirty === this._canDeactivate) {
      this.cancelPreventExit();
    }
    this._canDeactivate = !isDirty;
  }

  saveInProgress(): void {
    this.cancelPreventExit();
  }

  preventExit(): void {
    this._canDeactivate = false;
    this._store.dispatch(new SolidifyAppAction.PreventExit());
  }

  cancelPreventExit(): void {
    this._canDeactivate = true;
    this._store.dispatch(new SolidifyAppAction.CancelPreventExit());
  }

  navigate(path: string | string[]): void {
    this._store.dispatch(new Navigate(isArray(path) ? path : [path]));
  }

  navigateNavigate(navigate: Navigate): void {
    this._store.dispatch(navigate);
  }

  navigateWithQueryParam(navigateAction: Navigate): void {
    this._store.dispatch(navigateAction);
  }
}
