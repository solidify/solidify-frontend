/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-create-composition.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectorRef,
  Directive,
  Injector,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {AppRoutesPartialEnum} from "../../../../core/enums/partial/app-routes-partial.enum";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {ModelFormControlEvent} from "../../../../core/models/forms/model-form-control-event.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {CompositionActionHelper} from "../../../../core/stores/abstract/composition/composition-action.helper";
import {CompositionNameSpace} from "../../../../core/stores/abstract/composition/composition-namespace.model";
import {CompositionStateModel} from "../../../../core/stores/abstract/composition/composition-state.model";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {StateApplicationPartialEnum} from "../../../enums/partial/state-application-partial.enum";
import {AbstractCreateRoutable} from "../abstract-create/abstract-create.routable";

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractCreateCompositionRoutable<TResourceModel extends BaseResourceType, UResourceStateModel extends CompositionStateModel<TResourceModel>>
  extends AbstractCreateRoutable<TResourceModel, any> implements OnInit, OnDestroy {

  get parentId(): string {
    const route = this._injector.get(ActivatedRoute);
    return route.snapshot.paramMap.get(AppRoutesPartialEnum.paramIdWithoutPrefixParam);
  }

  protected constructor(protected readonly _store: Store,
                        protected readonly _actions$: Actions,
                        protected readonly _changeDetector: ChangeDetectorRef,
                        protected readonly _state: ExtendEnum<StateApplicationPartialEnum>,
                        protected readonly _injector: Injector,
                        private readonly _compositionActionNameSpace: CompositionNameSpace,
                        protected readonly _parentState?: ExtendEnum<StateApplicationPartialEnum>) {
    super(_store, _actions$, _changeDetector, _state, _injector, _compositionActionNameSpace as any, _parentState);
  }

  create(modelFormControlEvent: ModelFormControlEvent<TResourceModel>): void {
    super.saveInProgress();
    this._store.dispatch(CompositionActionHelper.create(this._compositionActionNameSpace, this.parentId, modelFormControlEvent.model));
  }

  ngOnInit(): void {
    this._extractQueryParams();
  }

  protected _cleanState(): void {
    this._store.dispatch(CompositionActionHelper.clean(this._compositionActionNameSpace, false, new QueryParameters()));
  }

  backToList(): void {
    this._store.dispatch(new Navigate([this._storeRouteService.getRootRoute(this._state), this.parentId]));
  }
}
