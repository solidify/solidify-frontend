/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - about.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  isNullOrUndefinedOrWhiteString,
  isTrue,
} from "../../../../core-resources/tools/is/is.tool";
import {MappingObject} from "../../../../core-resources/types/mapping-type.type";
import {ClipboardUtil} from "../../../../core-resources/utils/clipboard.util";
import {SsrUtil} from "../../../../core-resources/utils/ssr.util";
import {AbstractInternalRoutable} from "../../../../core/components/routables/abstract-internal/abstract-internal.routable";
import {AppRoutesPartialEnum} from "../../../../core/enums/partial/app-routes-partial.enum";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../../../core/injection-tokens/label-to-translate.injection-token";
import {BackendModuleVersion} from "../../../../core/models/backend-modules/backend-module-version.model";
import {FrontendVersion} from "../../../../core/models/frontend-version.model";
import {NotificationService} from "../../../../core/services/notification.service";
import {SolidifyAppAction} from "../../../../core/stores/abstract/app/app.action";
import {MemoizedUtil} from "../../../../core/utils/stores/memoized.util";
import {ofSolidifyActionCompleted} from "../../../../core/utils/stores/store.tool";
import {StringUtil} from "../../../../core-resources/utils/string.util";
import {LabelTranslateInterface} from "../../../../label-translate-interface.model";
import {DefaultSolidifyApplicationEnvironment} from "../../../environments/environment.solidify-application";
import {SolidifyApplicationAppAction} from "../../../stores/abstract/app/solidify-application-app.action";

@Component({
  selector: "solidify-about-routable",
  templateUrl: "./about.routable.html",
  styleUrls: ["./about.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AboutRoutable extends AbstractInternalRoutable implements OnInit, OnDestroy {
  backendVersionObs: Observable<MappingObject<string, BackendModuleVersion>>;
  frontendVersionObs: Observable<FrontendVersion>;

  get currentTheme(): string {
    return StringUtil.convertToPascalCase(this._environment.theme);
  }

  get currentThemeName(): string {
    return isNullOrUndefinedOrWhiteString(this._environment.themeName) ? this.currentTheme : this._environment.themeName;
  }

  get appRoutesPartialEnum(): typeof AppRoutesPartialEnum {
    return AppRoutesPartialEnum;
  }

  logo: string;

  constructor(protected readonly _injector: Injector,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyApplicationEnvironment,
              @Inject(LABEL_TRANSLATE) readonly labelTranslateInterface: LabelTranslateInterface,
              private readonly notificationService: NotificationService,
              private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef) {
    super(_injector);
    this.backendVersionObs = MemoizedUtil.select(this._store, this._environment.appState, state => state.backendModulesVersion);
    this.frontendVersionObs = MemoizedUtil.select(this._store, this._environment.appState, state => state.frontendVersion);
    this.logo = `assets/themes/${this.environment.theme}/toolbar-header-image.png`;

  }

  ngOnInit(): void {
    super.ngOnInit();

    this._store.dispatch(new SolidifyAppAction.GetAppVersion());
    if (SsrUtil.isServer) {
      this.subscribe(this._actions$.pipe(
        ofSolidifyActionCompleted(SolidifyAppAction.GetAppVersionSuccess, SolidifyAppAction.GetAppVersionFail),
        take(1),
        tap(action => {
          this._changeDetector.detectChanges();
          // Allow to force SSR to wait data
        }),
      ));
    }

    if (isTrue(this._environment.loadBackendModulesInfo)) {
      this.subscribe(MemoizedUtil.select(this._store, this._environment.appState, state => state.isApplicationInitialized).pipe(
        filter(isApplicationInitialized => isApplicationInitialized),
        take(1),
        tap(() => {
          const backendModulesUrl = MemoizedUtil.selectSnapshot(this._store, this._environment.appState, state => state.backendModulesUrl);
          this._store.dispatch(new SolidifyApplicationAppAction.LoadBackendModulesVersion(backendModulesUrl));
        }),
      ));

      if (SsrUtil.isServer) {
        this.subscribe(this._actions$.pipe(
          ofSolidifyActionCompleted(SolidifyApplicationAppAction.LoadBackendModulesVersionSuccess, SolidifyApplicationAppAction.LoadBackendModulesVersionFail),
          take(1),
          tap(action => {
            this._changeDetector.detectChanges();
            // Allow to force SSR to wait data
          }),
        ));
      }
    }
  }

  copy(url: string): void {
    if (ClipboardUtil.copyStringToClipboard(url)) {
      this.notificationService.showInformation(this.labelTranslateInterface.coreNotificationCopiedToClipboard);
    }
  }

  navigate(url: string[]): void {
    this._store.dispatch(new Navigate(url));
  }
}
