/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-list-composite.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterViewInit,
  ChangeDetectorRef,
  Directive,
  ElementRef,
  Injector,
  OnInit,
  Type,
  ViewChild,
} from "@angular/core";
import {FormControl} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  take,
  tap,
} from "rxjs/operators";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {OrderEnum} from "../../../../core-resources/enums/order.enum";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {Sort} from "../../../../core-resources/models/query-parameters/sort.model";
import {
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
} from "../../../../core-resources/tools/is/is.tool";
import {StringUtil} from "../../../../core-resources/utils/string.util";
import {DeleteDialog} from "../../../../core/components/dialogs/delete/delete.dialog";
import {ButtonColorEnum} from "../../../../core/enums/button-color.enum";
import {AppRoutesPartialEnum} from "../../../../core/enums/partial/app-routes-partial.enum";
import {IconNamePartialEnum} from "../../../../core/enums/partial/icon-name-partial.enum";
import {LocalStoragePartialEnum} from "../../../../core/enums/partial/local-storage-partial.enum";
import {STORE_DIALOG_SERVICE} from "../../../../core/injection-tokens/store-dialog-service.injection-token";
import {STORE_ROUTE_SERVICE} from "../../../../core/injection-tokens/store-route-service.injection-token";
import {DataTableActions} from "../../../../core/models/datatable/data-table-actions.model";
import {DataTableColumns} from "../../../../core/models/datatable/data-table-columns.model";
import {ExtraButtonToolbar} from "../../../../core/models/datatable/extra-button-toolbar.model";
import {StatusHistory} from "../../../../core/models/status-history.model";
import {StatusModel} from "../../../../core-resources/models/status.model";
import {AbstractStoreDialogService} from "../../../../core/services/abstract-store-dialog.service";
import {AbstractStoreRouteService} from "../../../../core/services/abstract-store-route-service";
import {RouterExtensionService} from "../../../../core/services/router-extension.service";
import {CompositionActionHelper} from "../../../../core/stores/abstract/composition/composition-action.helper";
import {CompositionNameSpace} from "../../../../core/stores/abstract/composition/composition-namespace.model";
import {CompositionStateModel} from "../../../../core/stores/abstract/composition/composition-state.model";
import {ResourceActionHelper} from "../../../../core/stores/abstract/resource/resource-action.helper";
import {ResourceNameSpace} from "../../../../core/stores/abstract/resource/resource-namespace.model";
import {ResourceAction} from "../../../../core/stores/abstract/resource/resource.action";
import {ResourceState} from "../../../../core/stores/abstract/resource/resource.state";
import {StatusHistoryNamespace} from "../../../../core/stores/abstract/status-history/status-history-namespace.model";
import {StatusHistoryStateModel} from "../../../../core/stores/abstract/status-history/status-history-state.model";
import {StatusHistoryState} from "../../../../core/stores/abstract/status-history/status-history.state";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {DialogUtil} from "../../../../core/utils/dialog.util";
import {RoutingUtil} from "../../../../core/utils/routing.util";
import {MemoizedUtil} from "../../../../core/utils/stores/memoized.util";
import {ofSolidifyActionCompleted} from "../../../../core/utils/stores/store.tool";
import {StoreUtil} from "../../../../core/utils/stores/store.util";
import {DataTablePresentational} from "../../../../data-table/data-table.presentational";
import {StateApplicationPartialEnum} from "../../../enums/partial/state-application-partial.enum";
import {StatusHistoryDialog} from "../../dialogs/status-history/status-history.dialog";
import {AbstractCrudRoutable} from "../abstract-crud/abstract-crud.routable";

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractListCompositeRoutable<TResourceModel extends BaseResourceType, VParentResourceModel extends BaseResourceType, UResourceStateModel extends CompositionStateModel<TResourceModel>>
  extends AbstractCrudRoutable<TResourceModel, any> implements OnInit, AfterViewInit {
  protected readonly _storeRouteService: AbstractStoreRouteService;
  protected readonly _storeDialogService: AbstractStoreDialogService;

  isLoadingObs: Observable<boolean>;
  listObs: Observable<TResourceModel[]>;
  queryParametersObs: Observable<QueryParameters>;

  detailRouteToInterpolate: string;
  skipInitialQuery: boolean = true;
  stickyTopPosition: number = this.environment.defaultStickyDatatableHeight;
  columnsSkippedToClear: string[] = [];

  displayActionCopyIdToClipboard: boolean = true;
  displayActionShowHistory: boolean = false;

  columns: DataTableColumns<TResourceModel>[];
  actions: DataTableActions<TResourceModel>[];
  listNewId: string[];
  isHighlightCondition: (current: TResourceModel) => boolean | undefined = undefined;
  cleanIsNeeded: boolean = true;

  @ViewChild("dataTablePresentational")
  readonly dataTablePresentational: DataTablePresentational<TResourceModel>;

  abstract readonly KEY_CREATE_BUTTON: string;
  abstract readonly KEY_BACK_BUTTON: string | undefined;
  abstract readonly KEY_PARAM_NAME: keyof TResourceModel & string;

  abstract readonly KEY_PARENT_COMPOSITION_PARAM_NAME: keyof VParentResourceModel & string;
  formControlParentCompositionId: FormControl = new FormControl();
  abstract labelParentComposition: string;
  parentCompositionLabelKey: string;
  parentCompositionValueKey: string = SOLIDIFY_CONSTANTS.RES_ID;
  parentCompositionSort: Sort;
  abstract parentCompositionNameSpace: ResourceNameSpace;
  abstract parentCompositionState: typeof ResourceState;
  lastSelectionParentCompositionLocalStorageKey: LocalStoragePartialEnum;

  set parentId(value: string) {
    if (isNotNullNorUndefinedNorWhiteString(value) && value !== this.formControlParentCompositionId.value) {
      this.formControlParentCompositionId.setValue(value);
    }
  }

  get parentId(): string {
    return this.formControlParentCompositionId.value;
  }

  protected constructor(protected readonly _store: Store,
                        protected readonly _changeDetector: ChangeDetectorRef,
                        protected readonly _route: ActivatedRoute,
                        protected readonly _routerExt: RouterExtensionService,
                        protected readonly _actions$: Actions,
                        protected readonly _dialog: MatDialog,
                        protected readonly _state: ExtendEnum<StateApplicationPartialEnum>,
                        protected readonly _compositionNameSpace: CompositionNameSpace,
                        protected readonly _injector: Injector,
                        public readonly options?: SharedAbstractListCompositionRoutableOption<TResourceModel>,
                        protected readonly _parentState?: ExtendEnum<StateApplicationPartialEnum>) {
    super(_store, _state, _injector, _parentState);
    // super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, _state, null, _injector, options, _parentState);
    this._storeRouteService = _injector.get(STORE_ROUTE_SERVICE);
    this._storeDialogService = _injector.get(STORE_DIALOG_SERVICE);
    this.options = Object.assign(this._getDefaultOptions(), options);
    this.isLoadingObs = this._store.select(s => StoreUtil.isLoadingState(super.getState(s)));
    this.listObs = this._store.select(s => this.getState(s).list);
    this.displayActionShowHistory = isNotNullNorUndefined(options.historyState) && isNotNullNorUndefined(options.historyStateAction);
    this.queryParametersObs = this._store.select(s => this.getState(s).queryParameters);
  }

  ngOnInit(): void {
    super.ngOnInit();
    if (!this._routerExt.previousRouteIsChild() && this.cleanIsNeeded) {
      this.clean();
    }
    this.defineColumns();
    this._applyLastSort();
    this.actions = this._defineActions();

    this.parentCompositionLabelKey = this.KEY_PARENT_COMPOSITION_PARAM_NAME;
    this.parentCompositionSort = {
      field: this.KEY_PARENT_COMPOSITION_PARAM_NAME,
      order: OrderEnum.ascending,
    };

    this.subscribe(this.formControlParentCompositionId.valueChanges.pipe(
      distinctUntilChanged(),
      tap(parentId => {
        this.getAll();
      }),
    ));

    this._getParentId();
    this.subscribe(this._route.url.pipe(tap(() => {
      this._getParentId();
      this.changeDetector.detectChanges();
    })));

    if (isNullOrUndefined(this.parentId)) {
      const queryParameters = new QueryParameters(1);
      this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        ResourceActionHelper.getAll(this.parentCompositionNameSpace, queryParameters, true),
        this.parentCompositionNameSpace.GetAllSuccess,
        (action: ResourceAction.GetAllSuccess<VParentResourceModel>) => {
          const list = action.list._data;
          if (isNonEmptyArray(list)) {
            this.parentId = list[0].resId;
          } else {
            this.options.createDisabled = true;
          }
          this._changeDetector.detectChanges();
        }));
    }

    this.subscribe(this.formControlParentCompositionId.valueChanges.pipe(
      tap(parentCompositionResId => {
        this.navigate([this._storeRouteService.getRootRoute(this._state), parentCompositionResId]);
      }),
    ));
  }

  private _getParentId(): void {
    this.parentId = this._route.snapshot.paramMap.get(AppRoutesPartialEnum.paramIdWithoutPrefixParam);
    this.detailRouteToInterpolate = this._storeRouteService.getDetailRoute(this._state) + "/{" + SOLIDIFY_CONSTANTS.RES_ID + "}/" + this.parentId;
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
    if (isNullOrUndefined(this.options.listExtraButtons)) {
      this.options.listExtraButtons = [];
    }
    if (isNullOrUndefined(this.dataTablePresentational?.buttonCleanAllFilter)) {
      return;
    }
    this.options.listExtraButtons = [...this.options.listExtraButtons, this.dataTablePresentational?.buttonCleanAllFilter];
    this._changeDetector.detectChanges();
  }

  private _applyLastSort(): void {
    const queryParameters = this._store.selectSnapshot(s => this.getState(s).queryParameters);
    const sort = queryParameters.sort;
    if (isNullOrUndefined(sort) || isNullOrUndefined(sort.field)) {
      return;
    }
    const index = this.columns.findIndex(c => c.sortableField === sort.field || c.field === sort.field);
    if (index === -1) {
      return;
    }
    this.columns[index].order = sort.order;
  }

  private _getDefaultOptions(): SharedAbstractListCompositionRoutableOption<TResourceModel> {
    return {
      canCreate: true,
      createDisabled: false,
      canGoBack: true,
      canRefresh: true,
    };
  }

  abstract defineColumns(): void;

  protected _defineActions(): DataTableActions<TResourceModel>[] {
    return [
      {
        logo: IconNamePartialEnum.edit,
        callback: (model: TResourceModel) => this.goToEdit(model),
        placeholder: current => this.labelTranslateInterface.applicationEdit,
        displayOnCondition: (model: TResourceModel) => this.conditionDisplayEditButton(model),
        isWrapped: false,
      },
      {
        logo: IconNamePartialEnum.delete,
        callback: (model: TResourceModel) => this.delete(model),
        placeholder: current => this.labelTranslateInterface.applicationDelete,
        displayOnCondition: (model: TResourceModel) => this.conditionDisplayDeleteButton(model),
        isWrapped: true,
      },
    ];
  }

  abstract conditionDisplayEditButton(model: TResourceModel | undefined): boolean;

  abstract conditionDisplayDeleteButton(model: TResourceModel | undefined): boolean;

  goToEdit(model: TResourceModel): void {
    const editRoute = this._storeRouteService.getDetailRoute(this._state) + SOLIDIFY_CONSTANTS.URL_SEPARATOR + model.resId + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this.parentId + SOLIDIFY_CONSTANTS.URL_SEPARATOR + this.environment.routeSegmentEdit;
    this._store.dispatch(new Navigate([editRoute], {}, {skipLocationChange: true}));
  }

  delete(model: TResourceModel): void {
    const data = {
      resId: model.resId,
      parentId: this.parentId,
      name: model[this.KEY_PARAM_NAME]?.toString(),
      message: this.options.messageDialogDelete,
      compositionNameSpace: this._compositionNameSpace,
      colorConfirm: ButtonColorEnum.warn,
      colorCancel: ButtonColorEnum.primary,
    };
    if (isNullOrUndefined(data.name)) {
      data.name = "";
    }

    DialogUtil.open(this._dialog, DeleteDialog, data, {
      width: "400px",
    });

    this.subscribe(this._actions$.pipe(ofSolidifyActionCompleted(this._compositionNameSpace.DeleteSuccess))
      .pipe(
        take(1),
        tap(result => {
          if (result.result.successful) {
            this.getAll();
          }
        }),
      ));
  }

  create(element: ElementRef): void {
    this.navigateNavigate(new Navigate([this._storeRouteService.getCreateRoute(this._state), this.parentId], {}, {skipLocationChange: false}));
  }

  getAll(queryParameters?: QueryParameters): void {
    if (isNullOrUndefinedOrWhiteString(this.parentId)) {
      return;
    }
    this._store.dispatch(CompositionActionHelper.getAll(this._compositionNameSpace, this.parentId, queryParameters, true));
  }

  showDetail(model: TResourceModel): void {
    this._store.dispatch(new Navigate([this._storeRouteService.getDetailRoute(this._state), model.resId, this.parentId]));
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this._store.dispatch(CompositionActionHelper.changeQueryParameters(this._compositionNameSpace, this.parentId, queryParameters, true));
    this._changeDetector.detectChanges(); // Allow to display spinner the first time
  }

  back(): void {
    this._store.dispatch(this.backNavigate());
  }

  backNavigate(): Navigate {
    return new Navigate(RoutingUtil.generateFullUrlFromActivatedRouteNormal(this._route.parent.parent));
  }

  clean(): void {
    this._store.dispatch(CompositionActionHelper.clean(this._compositionNameSpace, false, new QueryParameters()));
  }

  showHistory(resource: TResourceModel): void {
    if (isNullOrUndefined(this.options.historyState) || isNullOrUndefined(this.options.historyStateAction)) {
      return;
    }
    if (isNullOrUndefined(this.options.historyStatusEnumTranslate)) {
      throw new Error(`Missing option historyStatusEnumTranslate for state ${this._state} in list component`);
    }
    const isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, this.options.historyState);
    const queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, this.options.historyState);
    const historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, this.options.historyState, (state: StatusHistoryState<StatusHistoryStateModel<TResourceModel>, TResourceModel> | any) => state.history);
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: null,
      resourceResId: resource.resId,
      name: StringUtil.convertToPascalCase(this._state),
      statusHistory: historyObs,
      isLoading: isLoadingHistoryObs,
      queryParametersObs: queryParametersHistoryObs,
      state: this.options.historyStateAction,
      statusEnums: this.options.historyStatusEnumTranslate,
    }, {
      width: this.environment.modalWidth,
    });
  }
}

export interface SharedAbstractListCompositionRoutableOption<TResourceModel> {
  canCreate?: boolean | Observable<boolean>;
  createDisabled?: boolean | Observable<boolean>;
  canGoBack?: boolean;
  canRefresh?: boolean;
  listExtraButtons?: ExtraButtonToolbar<TResourceModel>[];
  historyState?: Type<StatusHistoryState<StatusHistoryStateModel<TResourceModel>, TResourceModel>> | any;
  historyStateAction?: StatusHistoryNamespace;
  historyStatusEnumTranslate?: StatusModel[];
  messageDialogDelete?: string;
}
