/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - additional-information-panel.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  OnChanges,
  SimpleChanges,
} from "@angular/core";
import {Store} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNullOrUndefined,
  Type,
} from "../../../../core-resources/tools/is/is.tool";
import {AbstractInternalContainer} from "../../../../core/components/containers/abstract-internal/abstract-internal.container";
import {
  BaseResource,
  BaseResourceType,
} from "../../../../core-resources/models/dto/base-resource.model";
import {ChangeInfo} from "../../../../core-resources/models/dto/change-info.model";
import {ResourceNameSpace} from "../../../../core/stores/abstract/resource/resource-namespace.model";
import {ResourceState} from "../../../../core/stores/abstract/resource/resource.state";
import {DateUtil} from "../../../../core/utils/date.util";
import {MemoizedUtil} from "../../../../core/utils/stores/memoized.util";
import {User} from "../../../../user/models/dto/user.model";

@Component({
  selector: "solidify-additional-information-panel-container",
  templateUrl: "./additional-information-panel.container.html",
  styleUrls: ["./additional-information-panel.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdditionalInformationPanelContainer<TResourceModel extends BaseResource> extends AbstractInternalContainer implements OnChanges {
  creationInfo: ChangeInfoDisplay | undefined;
  updateInfo: ChangeInfoDisplay | undefined;

  @Input()
  state: Type<ResourceState<any, User>>;

  @Input()
  resourceNameSpace: ResourceNameSpace;

  @Input()
  correspondingWhoChangeInfoAttributeKey: string;

  @Input()
  actionNameGetUserFromWhoId: string;

  userListObs: Observable<User[]>;

  private _isInitialized: boolean = false;
  readonly classIgnoreEditField: string = this.environment.classInputIgnored;

  @Input()
  resource: BaseResourceType;

  @Input()
  mode: AdditionalInformationPanelMode;

  constructor(private readonly _store: Store,
              private readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_injector);
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
    if (this.mode === "standalone" || (isNotNullNorUndefined(this.resourceNameSpace) && isNotNullNorUndefined(this.correspondingWhoChangeInfoAttributeKey) && isNotNullNorUndefined(this.actionNameGetUserFromWhoId))) {
      if (isNotNullNorUndefined(changes.resource) || (!this._isInitialized && isNotNullNorUndefined(this.resource))) {
        this._initUserListIfNot();
        this._updateInfos();
      }
    }
  }

  private _initUserListIfNot(): void {
    if (this._isInitialized || this.mode === "standalone") {
      return;
    }
    if (isNullOrUndefined(this.state)) {
      // eslint-disable-next-line no-console
      console.error("Input 'state' is not provided");
    }
    if (isNullOrUndefined(this.resourceNameSpace)) {
      // eslint-disable-next-line no-console
      console.error("Input 'resourceNameSpace' is not provided");
    }
    if (isNullOrUndefined(this.correspondingWhoChangeInfoAttributeKey)) {
      // eslint-disable-next-line no-console
      console.error("Input 'correspondingWhoChangeInfoAttributeKey' is not provided");
    }
    if (isNullOrUndefined(this.actionNameGetUserFromWhoId)) {
      // eslint-disable-next-line no-console
      console.error("Input 'actionNameGetUserFromWhoId' is not provided");
    }
    this.userListObs = MemoizedUtil.list(this._store, this.state);
  }

  private _updateInfos(): void {
    this._init();
    if (isNullOrUndefined(this.resource)) {
      return;
    }
    const creationInfos: ChangeInfo = this.resource["creation"];
    this.creationInfo = this._generateChangeInfoDisplay(creationInfos);
    const lastUpdatesInfos: ChangeInfo = this.resource["lastUpdate"];
    this.updateInfo = this._generateChangeInfoDisplay(lastUpdatesInfos);
  }

  private _init(): void {
    this.creationInfo = undefined;
    this.updateInfo = undefined;
  }

  private _generateChangeInfoDisplay(changeInfoType: ChangeInfo): ChangeInfoDisplay {
    const changeInfosDisplay = {} as ChangeInfoDisplay;
    if (isNotNullNorUndefined(changeInfoType)) {
      if (isNotNullNorUndefined(changeInfoType.when)) {
        changeInfosDisplay.date = DateUtil.convertDateToDateTimeString(new Date(changeInfoType.when));
      }
      if (this.mode === "standalone") {
        if (isNotNullNorUndefined(changeInfoType.fullName)) {
          changeInfosDisplay.fullName = changeInfoType.fullName;
        }
      } else if (this.mode === "retrieve-by-external-uid") {
        if (isNotNullNorUndefined(changeInfoType.who)) {
          this.subscribe(this.userListObs.pipe(
            distinctUntilChanged(),
            filter(userList => isNonEmptyArray(userList) && isNotNullNorUndefined(userList.find(u => u[this.correspondingWhoChangeInfoAttributeKey] === changeInfoType.who))),
            take(1),
            tap(userList => {
              const user = userList.find(u => u[this.correspondingWhoChangeInfoAttributeKey] === changeInfoType.who);
              changeInfosDisplay.fullName = user.lastName + ", " + user.firstName;
              this._changeDetector.detectChanges();
            }),
          ));
          this._store.dispatch(new this.resourceNameSpace[this.actionNameGetUserFromWhoId](changeInfoType.who, true));
        }
      }
    }

    return changeInfosDisplay;
  }
}

export interface ChangeInfoDisplay {
  fullName?: string | undefined;
  date?: string | undefined;
}

export type AdditionalInformationPanelMode = "standalone" | "retrieve-by-external-uid";
