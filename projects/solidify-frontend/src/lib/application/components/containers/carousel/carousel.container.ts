/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - carousel.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostListener,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {tap} from "rxjs/operators";
import {SsrUtil} from "../../../../core-resources/utils/ssr.util";
import {AbstractInternalContainer} from "../../../../core/components/containers/abstract-internal/abstract-internal.container";
import {PollingHelper} from "../../../../core/helpers/polling.helper";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {MemoizedUtil} from "../../../../core/utils/stores/memoized.util";
import {DefaultSolidifyApplicationEnvironment} from "../../../environments/environment.solidify-application";
import {AppCarouselAction} from "../../../stores/carousel/app-carousel.action";
import {AppCarouselState} from "../../../stores/carousel/app-carousel.state";

@Component({
  selector: "solidify-carousel-container",
  templateUrl: "./carousel.container.html",
  styleUrls: ["./carousel.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CarouselContainer extends AbstractInternalContainer implements OnInit, AfterViewInit {
  listTiles: Node[];
  activeTile: number = 0;
  private _skipNextChangeDueToVideoPlayed: boolean = false;
  private _skipNextChangeDueToHover: boolean = false;
  private _skipNextChangeDueToManualChangeOfTiles: boolean = false;

  @HostListener("mouseenter")
  onMouseEnter(): void {
    this._skipNextChangeDueToHover = true;
  }

  @HostListener("mouseleave")
  onMouseLeave(): void {
    this._skipNextChangeDueToHover = false;
  }

  constructor(protected readonly _injector: Injector,
              protected readonly _store: Store,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyApplicationEnvironment,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _translateService: TranslateService,
  ) {
    super(_injector);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(MemoizedUtil.select(this._store, AppCarouselState, state => state.current).pipe(
      tap(carousel => {
        this.listTiles = carousel?.tiles;
        this.activeTile = 0;
        this._changeDetector.detectChanges();
      }),
    ));

    this._store.dispatch(new AppCarouselAction.GetCarousel());
    this.subscribe(this._translateService.onLangChange.asObservable().pipe(
      tap(() => {
        this._store.dispatch(new AppCarouselAction.GetCarousel());
      }),
    ));

    this.subscribe(PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: this._environment.frequencyChangeCarouselTileInSecond,
      filter: () => {
        if (this._skipNextChangeDueToManualChangeOfTiles) {
          this._skipNextChangeDueToManualChangeOfTiles = false;
          return false;
        }
        if (this._skipNextChangeDueToHover) {
          return false;
        }
        if (this._skipNextChangeDueToVideoPlayed) {
          return false;
        }
        return true;
      },
      actionToDo: () => this._changeActiveTile(),
    }));
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
    this._registerVideoEvents();
  }

  setActive(numberActiveTile: number): void {
    this.activeTile = numberActiveTile;
    this._skipNextChangeDueToManualChangeOfTiles = true;
    this._registerVideoEvents();
  }

  private _registerVideoEvents(): void {
    if (SsrUtil.isServer) {
      return;
    }
    setTimeout(() => {
      SsrUtil.window?.document?.body?.querySelectorAll("video").forEach((node: HTMLVideoElement) => {
        node.addEventListener("play", event => {
          this._skipNextChangeDueToVideoPlayed = true;
          this._changeDetector.detectChanges();
        });
        node.addEventListener("pause", event => {
          this._skipNextChangeDueToVideoPlayed = false;
          this._changeDetector.detectChanges();
        });
      });
    }, 0);
  }

  private _changeActiveTile(): void {
    this.activeTile++;
    if (this.activeTile >= this.listTiles?.length) {
      this.activeTile = 0;
    }
    this._changeDetector.detectChanges();
  }
}
