/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-application-abstract-app.component.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectorRef,
  Directive,
  Injector,
  Optional,
  Renderer2,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {Router} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  tap,
} from "rxjs/operators";
import {AppStatusService} from "../../../../app-status/app-status.service";
import {UpdateVersionDialog} from "../../../../app-status/update-version/update-version.dialog";
import {
  isFalse,
  isFalsy,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isTrue,
  isTruthy,
  isUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {SolidifyObject} from "../../../../core-resources/types/solidify-object.type";
import {SsrUtil} from "../../../../core-resources/utils/ssr.util";

import {AbstractAppComponent} from "../../../../core/components/containers/abstract-app-component/abstract-app.component";
import {AppConfigService} from "../../../../core/config/app-config.service";
import {CookieConsentService} from "../../../../core/cookie-consent/services/cookie-consent.service";
import {BannerColorEnum} from "../../../../core/enums/banner-color.enum";
import {PollingHelper} from "../../../../core/helpers/polling.helper";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {BreakpointService} from "../../../../core/services/breakpoint.service";
import {GoogleAnalyticsService} from "../../../../core/services/google-analytics.service";
import {LoggingService} from "../../../../core/services/logging.service";
import {MetaService} from "../../../../core/services/meta.service";
import {NotificationService} from "../../../../core/services/notification.service";
import {ScrollService} from "../../../../core/services/scroll.service";
import {TransferStateService} from "../../../../core/services/transfer-state.service";
import {DialogUtil} from "../../../../core/utils/dialog.util";
import {MemoizedUtil} from "../../../../core/utils/stores/memoized.util";
import {SolidifyGlobalBannerAction} from "../../../../global-banner/global-banner.action";
import {User} from "../../../../user/models/dto/user.model";
import {DefaultSolidifyApplicationEnvironment} from "../../../environments/environment.solidify-application";
import {SolidifySecurityService} from "../../../security/security.service";
import {AppBannerState} from "../../../stores/banner/app-banner.state";

declare let gtag: (name: string, id: string, option: SolidifyObject) => void;

// @dynamic
@Directive()
export abstract class SolidifyApplicationAbstractAppComponent extends AbstractAppComponent {

  declare environment: DefaultSolidifyApplicationEnvironment;
  currentUserObs: Observable<User>;

  displayBannerObs: Observable<boolean>;
  messageBannerObs: Observable<string>;
  parametersObs: Observable<SolidifyObject>;
  colorBannerObs: Observable<BannerColorEnum>;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _router: Router,
              protected readonly _translate: TranslateService,
              protected readonly _renderer: Renderer2,
              protected readonly _injector: Injector,
              protected readonly _notificationService: NotificationService,
              protected readonly _loggingService: LoggingService,
              public readonly appStatusService: AppStatusService,
              public readonly breakpointService: BreakpointService,
              protected readonly _dialog: MatDialog,
              protected readonly _scrollService: ScrollService,
              protected readonly _googleAnalyticsService: GoogleAnalyticsService,
              protected readonly _cookieConsentService: CookieConsentService,
              protected readonly _appConfigService: AppConfigService,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _metaService: MetaService,
              @Optional() protected readonly _transferState: TransferStateService,
              protected readonly _securityService: SolidifySecurityService,
  ) {
    super(
      _store,
      _actions$,
      _router,
      _translate,
      _renderer,
      _injector,
      _notificationService,
      _loggingService,
      breakpointService,
      _scrollService,
      _googleAnalyticsService,
      _cookieConsentService,
      _appConfigService,
      _changeDetector,
      _metaService,
      _transferState,
    );
    this.environment = this._injector.get(ENVIRONMENT) as DefaultSolidifyApplicationEnvironment;
    this.displayBannerObs = MemoizedUtil.select(this._store, AppBannerState, state => state.display);
    this.messageBannerObs = MemoizedUtil.select(this._store, AppBannerState, state => state.message);
    this.parametersObs = MemoizedUtil.select(this._store, AppBannerState, state => state.parameters);
    this.colorBannerObs = MemoizedUtil.select(this._store, AppBannerState, state => state.color);

    if (SsrUtil.isBrowser) {
      if (isNotNullNorUndefined(this.environment.appUserState)) {
        this.currentUserObs = MemoizedUtil.current(this._store, this.environment.appUserState);
      }
      this.subscribe(this._observeGlobalBanner());
      this._observeOfflineOnlineMode();
      this.subscribe(this._observeUpdateAvailableForModal());
    }
  }

  protected override _isLoggedInRoot(): boolean {
    return isTruthy(this._securityService && this._securityService.isLoggedIn() && this._securityService.isRoot());
  }

  private _observeGlobalBanner(): Observable<any> {
    return PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: this.environment.refreshGlobalBannerIntervalInSecond,
      incrementInterval: true,
      resetIntervalWhenUserMouseEvent: true,
      maximumIntervalRefreshInSecond: this.environment.refreshGlobalBannerIntervalInSecond * 10,
      filter: () => MemoizedUtil.selectSnapshot(this._store, this.environment.appState, state => isFalse(state.isServerOffline))
        && (this._isLoggedInRoot() || isFalse(this.environment.maintenanceMode)),
      actionToDo: () => {
        this._store.dispatch(new SolidifyGlobalBannerAction.GetGlobalBanner());
      },
    });
  }

  private _observeOfflineOnlineMode(): void {
    let previousStateIsOnline: boolean | undefined = undefined;
    this.subscribe(this.appStatusService.online.pipe(
      distinctUntilChanged(),
      tap((isOnline: boolean) => {
        if ((isUndefined(previousStateIsOnline) || isTrue(previousStateIsOnline)) && isFalsy(isOnline)) {
          this._notificationService.showWarning(this.labelTranslateInterface.applicationAppOffline);
          previousStateIsOnline = isOnline;
          return;
        }
        if (isFalse(previousStateIsOnline) && isTrue(isOnline)) {
          this._notificationService.showInformation(this.labelTranslateInterface.applicationAppBackToOnline);
          previousStateIsOnline = isOnline;
          return;
        }
        return;
      }),
    ));
  }

  private _observeUpdateAvailableForModal(): Observable<string> {
    return this.appStatusService.updateAvailable.pipe(
      distinctUntilChanged(),
      filter(version => !isNullOrUndefined(version)),
      tap((version) => {
        switch (this.environment.actionToDoWhenUpdateAvailable) {
          case "do-nothing":
            break;
          case "prompt":
            DialogUtil.open(this._dialog, UpdateVersionDialog);
            break;
          case "refresh":
          default:
            SsrUtil.window?.location.reload();
            break;
        }
      }),
    );
  }

}
