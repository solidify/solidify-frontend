/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - doi-menu.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import {MatMenu} from "@angular/material/menu";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefinedOrWhiteString,
  isTrue,
} from "../../../../core-resources/tools/is/is.tool";
import {ClipboardUtil} from "../../../../core-resources/utils/clipboard.util";
import {SsrUtil} from "../../../../core-resources/utils/ssr.util";
import {AbstractInternalContainer} from "../../../../core/components/containers/abstract-internal/abstract-internal.container";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {NotificationService} from "../../../../core/services/notification.service";
import {ObservableUtil} from "../../../../core/utils/observable.util";
import {StoreUtil} from "../../../../core/utils/stores/store.util";
import {DefaultSolidifyApplicationEnvironment} from "../../../environments/environment.solidify-application";
import {appDoiActionNameSpace} from "../../../stores/doi/app-doi.action";

@Component({
  selector: "solidify-doi-menu-container",
  templateUrl: "./doi-menu.container.html",
  styleUrls: ["./doi-menu.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  exportAs: "doiMenu",
})
export class DoiMenuContainer extends AbstractInternalContainer implements OnInit {
  declare environment: DefaultSolidifyApplicationEnvironment;

  @Input()
  retrieveShortDoi: boolean = true;

  @Input()
  doi: string;

  shortDoi: string | undefined;

  @ViewChild(MatMenu, {static: true})
  menu: MatMenu;

  get inErrorWhenRetrieveShortDoi(): boolean | undefined {
    return this._inErrorWhenRetrieveShotDoiBS?.value;
  }

  private readonly _inErrorWhenRetrieveShotDoiBS: BehaviorSubject<boolean | undefined> = new BehaviorSubject<boolean | undefined>(undefined);
  @Output("inErrorWhenRetrieveShortDoiChange")
  readonly inErrorWhenRetrieveShotDoiObs: Observable<boolean | undefined> = ObservableUtil.asObservable(this._inErrorWhenRetrieveShotDoiBS);

  constructor(protected readonly _injector: Injector,
              private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _notificationService: NotificationService) {
    super(_injector);
    this.environment = this._injector.get(ENVIRONMENT) as DefaultSolidifyApplicationEnvironment;
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (SsrUtil.isServer) {
      return;
    }

    if (isNotNullNorUndefinedNorWhiteString(this.doi) && isTrue(this.retrieveShortDoi)) {
      this.subscribe(this._getShortDoiObs());
    }
  }

  private _getShortDoiObs(): Observable<any> {
    return StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new appDoiActionNameSpace.GetShortDoi(this.doi),
      appDoiActionNameSpace.GetShortDoiSuccess,
      result => {
        this.shortDoi = result.shortDoi;
        this._inErrorWhenRetrieveShotDoiBS.next(false);
        this._changeDetector.detectChanges();
      },
      appDoiActionNameSpace.GetShortDoiFail,
      () => {
        this._inErrorWhenRetrieveShotDoiBS.next(true);
        this._changeDetector.detectChanges();
      });
  }

  copyShortDoi(): void {
    ClipboardUtil.copyStringToClipboard(this.shortDoi);
    this._notificationService.showInformation(this.labelTranslateInterface.applicationDoiNotificationShortDoiCopyToClipboard);
  }

  copyLongDoi(): void {
    ClipboardUtil.copyStringToClipboard(this.doi);
    this._notificationService.showInformation(this.labelTranslateInterface.applicationDoiNotificationLongDoiCopyToClipboard);
  }

  copyUrlShortDoi(): void {
    ClipboardUtil.copyStringToClipboard(this.environment.doiLink + this.shortDoi);
    this._notificationService.showInformation(this.labelTranslateInterface.applicationDoiNotificationUrlShortDoiCopyToClipboard);
  }

  copyUrlLongDoi(): void {
    ClipboardUtil.copyStringToClipboard(this.environment.doiLink + this.doi);
    this._notificationService.showInformation(this.labelTranslateInterface.applicationDoiNotificationUrlLongDoiCopyToClipboard);
  }

  navigateToDoi(mouseEvent?: MouseEvent): void {
    if (isNotNullNorUndefined(mouseEvent)) {
      mouseEvent.stopPropagation();
    }
    if (isNullOrUndefinedOrWhiteString(this.environment.doiLink)) {
      return;
    }
    SsrUtil.window?.open(this.environment.doiLink + this.doi, "_blank");
  }
}

