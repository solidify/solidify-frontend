/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-table-resource-role.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectorRef,
  Directive,
  ElementRef,
  HostBinding,
  Inject,
  Injector,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  AbstractControl,
  ControlValueAccessor,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {tap} from "rxjs/operators";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  isUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {StringUtil} from "../../../../core-resources/utils/string.util";
import {AbstractInternalContainer} from "../../../../core/components/containers/abstract-internal/abstract-internal.container";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {FormValidationHelper} from "../../../../core/helpers/form-validation.helper";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {BaseResource} from "../../../../core-resources/models/dto/base-resource.model";
import {BaseFormDefinition} from "../../../../core/models/forms/base-form-definition.model";
import {Paging} from "../../../../core-resources/models/query-parameters/paging.model";
import {Sort} from "../../../../core-resources/models/query-parameters/sort.model";
import {ResourceNameSpace} from "../../../../core/stores/abstract/resource/resource-namespace.model";
import {ResourceState} from "../../../../core/stores/abstract/resource/resource.state";
import {ExtendEnum} from "../../../../core-resources/types/extend-enum.type";
import {ExtendModel} from "../../../../core/types/extend-model.type";
import {ObservableUtil} from "../../../../core/utils/observable.util";
import {RolePartialEnum} from "../../../enums/partial/role-partial.enum";
import {DefaultSolidifyApplicationEnvironment} from "../../../environments/environment.solidify-application";
import {Relation3TiersPersonResourceRole} from "../../../models/relation-3-tiers-person-resource-role.model";
import {SolidifyRole} from "../../../models/role.model";

// @dynamic
@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractTableResourceRoleContainer extends AbstractInternalContainer implements ControlValueAccessor, OnInit {
  private readonly _DEFAULT_PAGE_SIZE: number;
  readonly PAGE_SIZE_OPTION: number[];
  readonly classBypassEdit: string;
  readonly classInputIgnored: string;

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  formArrayExisting: FormArray;
  formArrayToAdd: FormArray;

  paging: Paging;

  checkboxDisplayMode: "checkbox" | "radio" = "radio";

  @HostBinding("class.disabled") disabled: boolean = false;

  @Input()
  selectedResourceRole: ExtendModel<Relation3TiersPersonResourceRole>[];

  @Input()
  listResource: BaseResource[];

  @Input()
  listRole: SolidifyRole[];

  @Input()
  isWithLinkToAdmin: boolean = true;

  @Input()
  highlightedResourceId: string;

  @Input()
  preventEdition: boolean = false;

  @Input()
  defaultRole: ExtendEnum<RolePartialEnum> = undefined;

  @Input()
  abstract resourceLabelKey: string;

  @Input()
  abstract resourceLabelToTranslate: string;

  @Input()
  abstract resourceSort: Sort;

  @Input()
  abstract resourceActionNameSpace: ResourceNameSpace;

  @Input()
  abstract resourceState: typeof ResourceState | any;

  @Input()
  formControl: FormControl;

  @Input()
  abstract resourceTooltipNavigateToTranslate: string;

  abstract detailPath: string;

  _readonly: boolean;

  @Input()
  set readonly(value: boolean) {
    this._readonly = value;
    this._updateFormReadonlyState();
  }

  get readonly(): boolean {
    return this._readonly;
  }

  private readonly _valueBS: BehaviorSubject<any[] | undefined> = new BehaviorSubject<any[] | undefined>(undefined);
  @Output("valueChange")
  readonly valueObs: Observable<any[] | undefined> = ObservableUtil.asObservable(this._valueBS);

  protected readonly _navigateBS: BehaviorSubject<string[]> = new BehaviorSubject<string[]>(undefined);
  @Output("navigate")
  readonly navigateObs: Observable<string[]> = ObservableUtil.asObservable(this._navigateBS);

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  getFormControl(formGroup: FormGroup, key: string): FormControl {
    return FormValidationHelper.getFormControl(formGroup, key);
  }

  isRequired(formControl: AbstractControl, key: string): boolean {
    const errors = formControl.get(key).errors;
    return isNullOrUndefined(errors) ? false : errors.required;
  }

  constructor(protected readonly _injector: Injector,
              protected readonly _translate: TranslateService,
              protected readonly _store: Store,
              protected readonly _fb: FormBuilder,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyApplicationEnvironment) {
    super(_injector);

    this._DEFAULT_PAGE_SIZE = _environment.tableResourceRoleDefaultPageSize;
    this.PAGE_SIZE_OPTION = _environment.tableResourceRolePageSizeOptions;
    this.classBypassEdit = _environment.classBypassEdit;
    this.classInputIgnored = _environment.classInputIgnored;
  }

  private _markAsTouched(value: Model[]): void {
    this.propagateChange(value);
    this._valueBS.next(value);
    this.formControl.markAllAsTouched();
  }

  get formArrayCombinedValue(): Model[] {
    return [...this.formArrayExisting.value, ...this.formArrayToAdd.value];
  }

  ngOnInit(): void {
    super.ngOnInit();

    // TODO Manage better form to use directly formControl (allow to share disable and valid state)
    this.formArrayExisting = this._fb.array([]);
    this.formArrayToAdd = this._fb.array([]);
    this._updatePagingExisting();

    if (isNotNullNorUndefined(this.selectedResourceRole)) {
      this.selectedResourceRole.forEach(p => {
        this.formArrayExisting.push(this.createResourceRole(p));
      });
    }
    this._updatePagingExisting();

    this._updateFormReadonlyState();
    this.formControl.setValue(this.formArrayExisting.value);

    this.subscribe(this.formArrayToAdd.valueChanges.pipe(
      tap((value: Model[]) => {
        this._markAsTouched(this.formArrayCombinedValue);
      }),
    ));

    const valueBefore: Model[] = this.formArrayExisting.value;
    this.subscribe(this.formArrayExisting.valueChanges.pipe(
      tap((value: Model[]) => {
        if (valueBefore.length !== value.length) {
          this._markAsTouched(this.formArrayCombinedValue);
          return;
        }

        let isDiff = false;
        valueBefore.forEach((valBefore, index) => {
          if (isDiff) {
            return;
          }
          if (valBefore.id !== value[index].id || valBefore.listId !== value[index].listId) {
            this._markAsTouched(this.formArrayCombinedValue);
            isDiff = true;
            return;
          }
        });

        if (!isDiff) {
          this.formControl.markAsPristine();
        }
      }),
    ));

    this.subscribe(this.formArrayExisting.statusChanges.pipe(
      tap(status => {
        if (status === SOLIDIFY_CONSTANTS.FORM_STATUS_VALID) {
          this.formControl.setErrors(null);
        } else if (status === SOLIDIFY_CONSTANTS.FORM_STATUS_INVALID) {
          this.formControl.setErrors({invalid: true});
        }
      }),
    ));
  }

  private _updateFormReadonlyState(): void {
    if (isNullOrUndefined(this.formArrayExisting) || isNullOrUndefined(this.formControl)) {
      return;
    }
    if (this._readonly) {
      this.disabled = true;
      this.formArrayExisting.disable();
      this.formControl.disable();
    } else {
      this.disabled = false;
      this.formArrayExisting.enable();
      this.formControl.enable();
    }
  }

  createResourceRole(person: Relation3TiersPersonResourceRole | undefined = undefined): FormGroup {
    if (isUndefined(person)) {
      const form = this._fb.group({
        [this.formDefinition.id]: ["", [Validators.required]],
        [this.formDefinition.listId]: [this.defaultRole, [Validators.required]],
      });
      return form;
    } else {
      // We transform here list of roles into unique role because we hide the multiples roles to the final user
      const listId = person.roles.length > 0 ? this._getMostImportantRole(person.roles.map(r => r.resId) as ExtendEnum<RolePartialEnum>[]) : "";
      const form = this._fb.group({
        [this.formDefinition.id]: [person.resId, [Validators.required]],
        [this.formDefinition.listId]: [listId, [Validators.required]],
      });
      if (person.isReadonly) {
        setTimeout(() => {
          form.disable();
        }, 0);
      }
      return form;
    }
  }

  addResource(tryLater: boolean = true): void {
    if (!this.formControl.disabled) {
      this.formArrayToAdd.push(this.createResourceRole());
      this._changeDetector.detectChanges();
    } else {
      if (tryLater) {
        setTimeout(() => {
          this.addResource(false);
          this._changeDetector.detectChanges();
        }, 10);
      }
    }
  }

  delete(addMode: boolean, indexInPaginated: number): void {
    const formArray = (addMode ? this.formArrayToAdd : this.formArrayExisting);
    const formArrayPaginated = (addMode ? this.formArrayToAdd.controls : this.paginatedFormArrayExistingControl);
    const formControl = formArrayPaginated[indexInPaginated];
    const index = formArray.controls.indexOf(formControl);
    formArray.removeAt(index);
    this._markAsTouched(this.formArrayCombinedValue);
    this._updatePagingExisting();
  }

  propagateChange: (__: any) => void = (__: any): void => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(value: string[]): void {
    // if (value) {
    // this.formControl.setValue(value);
    // }
  }

  @Input()
  abstract messageNoItem: string;

  @Input()
  abstract elementsToAddLabelToTranslate: string;

  private _getMostImportantRole(listRoles: ExtendEnum<RolePartialEnum>[]): ExtendEnum<RolePartialEnum> | string {
    if (listRoles.length === 0) {
      return StringUtil.stringEmpty;
    }
    if (listRoles.length === 1) {
      return listRoles[0];
    }
    const roleEnum = this._doGetMostImportantRole(listRoles);
    return isNotNullNorUndefined(roleEnum) ? roleEnum : StringUtil.stringEmpty;
  }

  protected abstract _doGetMostImportantRole(listRoles: ExtendEnum<RolePartialEnum>[]): ExtendEnum<RolePartialEnum> | undefined;

  goToResource(resourceId: string): void {
    this._navigateBS.next([this.detailPath, resourceId]);
  }

  changeRole(f: FormGroup, role: SolidifyRole, $event: MouseEvent): void {
    if (this.readonly || f.disabled) {
      return;
    }
    const formControl = this.getFormControl(f, this.formDefinition.listId);
    if (formControl.value !== role.resId) {
      this.getFormControl(f, this.formDefinition.listId).setValue(role.resId);
    }
  }

  asFormGroup(abstractControl: AbstractControl): FormGroup {
    return abstractControl as FormGroup;
  }

  paginatedFormArrayExistingControl: FormGroup[] = [];

  pageChange(newPaging: Paging): void {
    if (newPaging.pageSize !== this.paging.pageSize) {
      newPaging.pageIndex = 0;
    }
    this.paging = newPaging;
    this.computePaginatedFormArrayExistingControl();
  }

  computePaginatedFormArrayExistingControl(): void {
    const startIndex = this.paging.pageIndex * this.paging.pageSize;
    let endIndex = startIndex + this.paging.pageSize;
    if (endIndex > this.paging.length) {
      endIndex = this.paging.length;
    }
    this.paginatedFormArrayExistingControl = (this.formArrayExisting.controls as FormGroup[]).slice(startIndex, endIndex);
  }

  private _updatePagingExisting(goToEnd: boolean = false): void {
    const newPaging = {
      pageSize: this._DEFAULT_PAGE_SIZE,
      pageIndex: this.paging?.pageIndex ?? 0,
      length: this.formArrayExisting.length,
    };
    const maximalPage = Math.ceil(newPaging.length / (newPaging.pageSize >= 1 ? newPaging.pageSize : 1));
    if (newPaging.pageIndex > 0) {
      if (newPaging.pageIndex + 1 > maximalPage) {
        // Go to last page if current not exist anymore
        newPaging.pageIndex = maximalPage - 1;
      }
    }
    if (goToEnd) {
      newPaging.pageIndex = maximalPage - 1;
    }

    this.paging = newPaging;
    this.computePaginatedFormArrayExistingControl();
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  id: string = "id";
  listId: string = "listId";
}

interface Model {
  id: string;
  listId: string;
}
