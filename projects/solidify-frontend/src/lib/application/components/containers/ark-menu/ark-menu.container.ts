/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - ark-menu.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  ViewChild,
} from "@angular/core";
import {MatMenu} from "@angular/material/menu";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  isNotNullNorUndefined,
  isNullOrUndefinedOrWhiteString,
} from "../../../../core-resources/tools/is/is.tool";
import {ClipboardUtil} from "../../../../core-resources/utils/clipboard.util";
import {SsrUtil} from "../../../../core-resources/utils/ssr.util";
import {AbstractInternalContainer} from "../../../../core/components/containers/abstract-internal/abstract-internal.container";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {NotificationService} from "../../../../core/services/notification.service";
import {DefaultSolidifyApplicationEnvironment} from "../../../environments/environment.solidify-application";

@Component({
  selector: "solidify-ark-menu-container",
  templateUrl: "./ark-menu.container.html",
  styleUrls: ["./ark-menu.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  exportAs: "arkMenu",
})
export class ArkMenuContainer extends AbstractInternalContainer {
  declare environment: DefaultSolidifyApplicationEnvironment;

  @Input()
  ark: string;

  @Input()
  allowNavigateToArk: boolean = false;

  @ViewChild(MatMenu, {static: true})
  menu: MatMenu;

  get arkLink(): string | undefined {
    return this.environment.arkLink;
  }

  constructor(protected readonly _injector: Injector,
              private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _notificationService: NotificationService) {
    super(_injector);
    this.environment = this._injector.get(ENVIRONMENT) as DefaultSolidifyApplicationEnvironment;
  }

  copyArk(): void {
    ClipboardUtil.copyStringToClipboard(this.ark);
    this._notificationService.showInformation(this.labelTranslateInterface.applicationArkNotificationArkCopyToClipboard);
  }

  copyUrlArk(): void {
    ClipboardUtil.copyStringToClipboard(this.environment.arkLink + this.ark);
    this._notificationService.showInformation(this.labelTranslateInterface.applicationArkNotificationUrlArkCopyToClipboard);
  }

  navigateToArk(mouseEvent?: MouseEvent): void {
    if (isNotNullNorUndefined(mouseEvent)) {
      mouseEvent.stopPropagation();
    }
    if (isNullOrUndefinedOrWhiteString(this.environment.arkLink)) {
      return;
    }
    SsrUtil.window?.open(this.environment.arkLink + this.ark, "_blank");
  }
}

