/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - status-history.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {MappingObject} from "../../../../core-resources/types/mapping-type.type";
import {AbstractInternalDialog} from "../../../../core/components/dialogs/abstract-internal/abstract-internal.dialog";
import {ColorHexaEnum} from "../../../../core-resources/enums/color-hexa.enum";
import {DataTableFieldTypeEnum} from "../../../../core/enums/datatable/data-table-field-type.enum";
import {DataTableComponentPartialEnum} from "../../../../core/enums/datatable/partial/data-table-component-partial.enum";
import {OrderEnum} from "../../../../core-resources/enums/order.enum";
import {DataTableComponentPartialHelper} from "../../../../core/helpers/data-table-component-partial.helper";
import {DATA_TABLE_COMPONENT} from "../../../../core/injection-tokens/data-table-component.injection-token";
import {DataTableColumns} from "../../../../core/models/datatable/data-table-columns.model";
import {DataTableComponent} from "../../../../core/models/datatable/data-table-component.model";
import {QueryParameters} from "../../../../core-resources/models/query-parameters/query-parameters.model";
import {StatusHistoryDialogData} from "../../../../core/models/status-history-dialog-data.model";
import {StatusModel} from "../../../../core-resources/models/status.model";

@Component({
  selector: "solidify-status-history-dialog",
  templateUrl: "./status-history.dialog.html",
  styleUrls: ["./status-history.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatusHistoryDialog extends AbstractInternalDialog<StatusHistoryDialogData> implements OnInit {
  paramMessage: { name: string } = {name: ""};
  columns: DataTableColumns[];

  readonly statusCreated: StatusModel = {
    key: "CREATED",
    value: this.labelTranslateInterface.applicationCreated,
    backgroundColorHexa: ColorHexaEnum.blue,
  };

  constructor(protected readonly _dialogRef: MatDialogRef<StatusHistoryDialog>,
              private readonly _translate: TranslateService,
              private readonly _store: Store,
              @Inject(MAT_DIALOG_DATA) public readonly data: StatusHistoryDialogData,
              @Inject(DATA_TABLE_COMPONENT) public readonly dataTableComponent: MappingObject<DataTableComponentPartialEnum, DataTableComponent>,
              protected readonly _injector: Injector) {
    super(_injector, _dialogRef, data);
    this.paramMessage.name = this.data.name;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.columns = [
      {
        field: "changeTime",
        header: this.labelTranslateInterface.applicationChangeTime,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isSortable: true,
        isFilterable: false,
      },
      {
        field: "status",
        header: this.labelTranslateInterface.applicationStatus,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        filterEnum: [...this.data.statusEnums, this.statusCreated],
        translate: true,
        width: "110px",
        component: DataTableComponentPartialHelper.getInternal(this.dataTableComponent, DataTableComponentPartialEnum.status),
      },
      {
        field: "description",
        header: this.labelTranslateInterface.applicationDescription,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: false,
      },
      {
        field: "creatorName",
        header: this.labelTranslateInterface.applicationCreatedBy,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        width: "140px",
      },
    ];
  }

  pagination($event: QueryParameters): void {
    this._store.dispatch(new this.data.state.ChangeQueryParameters($event, this.data.resourceResId, this.data.parentId));
  }
}
