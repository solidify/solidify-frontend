/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - token.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {isNotNullNorUndefinedNorWhiteString} from "../../../../core-resources/tools/is/is.tool";
import {AbstractInternalDialog} from "../../../../core/components/dialogs/abstract-internal/abstract-internal.dialog";
import {DefaultSolidifyEnvironment} from "../../../../core/environments/environment.solidify-defaults";
import {JwtTokenHelper} from "../../../../core/helpers/jwt-token.helper";
import {ENVIRONMENT} from "../../../../core/injection-tokens/environment.injection-token";
import {Token} from "../../../../core/models/token.model";
import {NotificationService} from "../../../../core/services/notification.service";
import {ClipboardUtil} from "../../../../core-resources/utils/clipboard.util";
import {DateUtil} from "../../../../core/utils/date.util";
import {LoginMode} from "../../../../oauth2/login-mode.enum";
import {OauthHelper} from "../../../../oauth2/oauth.helper";
import {OAuth2Service} from "../../../../oauth2/oauth2.service";

@Component({
  selector: "solidify-token-dialog",
  templateUrl: "./token.dialog.html",
  styleUrls: ["./token.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TokenDialog extends AbstractInternalDialog<TokenDialogData> implements OnInit {
  tokenDecoded: Token;

  expiration: string = "";
  isMfa: boolean = false;

  get allowUpgradeToMfaToken(): boolean {
    return !this.isMfa && isNotNullNorUndefinedNorWhiteString(this._environment.clientIdMfa);
  }

  get token(): string {
    return this.oauth2Service.getAccessToken();
  }

  constructor(protected readonly _injector: Injector,
              protected readonly _dialogRef: MatDialogRef<TokenDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: TokenDialogData,
              @Inject(ENVIRONMENT) private readonly _environment: DefaultSolidifyEnvironment,
              public readonly oauth2Service: OAuth2Service,
              public readonly notificationService: NotificationService) {
    super(_injector, _dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.tokenDecoded = JwtTokenHelper.decodeToken(this.token);
    this.expiration = DateUtil.convertDateToDateTimeString(new Date(this.tokenDecoded.exp * 1000));
    this.isMfa = OauthHelper.isTokenMfa(this.tokenDecoded);
  }

  copy(): void {
    if (ClipboardUtil.copyStringToClipboard(this.token)) {
      this.notificationService.showInformation(this.labelTranslateInterface.applicationTokenDialogTokenCopiedToClipboardSuccess);
    } else {
      this.notificationService.showWarning(this.labelTranslateInterface.applicationTokenDialogTokenCopiedToClipboardFail);
    }
  }

  logInMfa(): void {
    this.oauth2Service.externalLogOutIfDefined(() => {
      this.oauth2Service.clearStorage();
      this.oauth2Service.initAuthorizationCodeFlow(LoginMode.MFA, OauthHelper.getCurrentPath(this._environment));
    });
  }
}

export interface TokenDialogData {
  extraInformation: TokenDialogKeyValue[];
}

export interface TokenDialogKeyValue {
  key: string;
  value: (token: Token | any) => string;
}
