/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - data-files-upload-in-progress.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  Output,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {Guid} from "../../../../core-resources/models/guid.model";
import {AbstractInternalPresentational} from "../../../../core/components/presentationals/abstract-internal/abstract-internal.presentational";
import {FileUploadStatusEnum} from "../../../../core/enums/upload/file-upload-status.enum";
import {BaseResource} from "../../../../core-resources/models/dto/base-resource.model";
import {FileUploadWrapper} from "../../../../core/models/upload/file-upload-wrapper.model";
import {UploadFileStatus} from "../../../../core/models/upload/upload-file-status.model";
import {ObservableUtil} from "../../../../core/utils/observable.util";

@Component({
  selector: "solidify-data-files-upload-in-progress",
  templateUrl: "./data-files-upload-in-progress.presentational.html",
  styleUrls: ["./data-files-upload-in-progress.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataFilesUploadInProgressPresentational<TResource extends BaseResource, UFileUploadWrapper extends FileUploadWrapper> extends AbstractInternalPresentational {
  @Input()
  listFilesUploading: UploadFileStatus<TResource, UFileUploadWrapper>[];

  private readonly _cancelBS: BehaviorSubject<UploadFileStatus<TResource> | undefined> = new BehaviorSubject<UploadFileStatus<TResource> | undefined>(undefined);
  @Output("cancelChange")
  readonly cancelObs: Observable<UploadFileStatus<TResource> | undefined> = ObservableUtil.asObservable(this._cancelBS);

  private readonly _retryBS: BehaviorSubject<UploadFileStatus<TResource> | undefined> = new BehaviorSubject<UploadFileStatus<TResource> | undefined>(undefined);
  @Output("retryChange")
  readonly retryObs: Observable<UploadFileStatus<TResource> | undefined> = ObservableUtil.asObservable(this._retryBS);

  constructor(protected readonly _injector: Injector,
              protected readonly _dialog: MatDialog) {
    super(_injector);
  }

  getCurrentNumberOfFileInProgress(): number {
    return this.listFilesUploading.filter(f => f.status === FileUploadStatusEnum.inProgress).length;
  }

  cancel(uploadStatus: UploadFileStatus<TResource>): void {
    this._cancelBS.next(uploadStatus);
  }

  retry(uploadStatus: UploadFileStatus<TResource>): void {
    this._retryBS.next(uploadStatus);
  }

  trackByFn(index: number, item: UploadFileStatus<TResource>): Guid {
    return item.guid;
  }
}
