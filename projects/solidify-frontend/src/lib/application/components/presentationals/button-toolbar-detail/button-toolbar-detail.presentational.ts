/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - button-toolbar-detail.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Component,
  Injector,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {DataTestPartialEnum} from "../../../../core-resources/enums/partial/data-test-partial.enum";
import {BaseResource} from "../../../../core-resources/models/dto/base-resource.model";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {AbstractFormPresentational} from "../../../../core/components/presentationals/abstract-form/abstract-form.presentational";
import {ButtonColorEnum} from "../../../../core/enums/button-color.enum";
import {ButtonThemeEnum} from "../../../../core/enums/button-theme.enum";
import {IconNamePartialEnum} from "../../../../core/enums/partial/icon-name-partial.enum";
import {ObservableUtil} from "../../../../core/utils/observable.util";
import {AbstractButtonToolbarPresentational} from "../abstract-button-toolbar/abstract-button-toolbar.presentational";

// eslint-disable-next-line
@Component({
  selector: "solidify-button-toolbar-detail",
  templateUrl: "./button-toolbar-detail.presentational.html",
  styleUrls: ["./button-toolbar-detail.presentational.scss"],
  // changeDetection: ChangeDetectionStrategy.OnPush, // TODO Find a way to use OnPush strategy and still have ability to update disable and display condition (@Input immutable for change ? Observable ?)
})
export class ButtonToolbarDetailPresentational<TResourceModel extends BaseResource> extends AbstractButtonToolbarPresentational<TResourceModel> implements OnInit {
  @Input()
  readonly keyDeleteButtonToTranslate: string = this.labelTranslateInterface.applicationDelete;

  @Input()
  readonly keyBackToDetailButtonToTranslate: string = this.labelTranslateInterface.applicationExitEditMode;

  @Input()
  readonly keySaveButtonToTranslate: string = this.labelTranslateInterface.applicationSave;

  @Input()
  readonly keyCreateButtonToTranslate: string = this.labelTranslateInterface.applicationCreate;

  @Input()
  editAvailable: boolean = true;

  @Input()
  deleteAvailable: boolean = true;

  @Input()
  saveAvailable: boolean = true;

  @Input()
  historyAvailable: boolean = false;

  @Input()
  currentModel: TResourceModel;

  @Input()
  readonly formPresentational: AbstractFormPresentational<TResourceModel>;

  private _mode: "edit" | "create" | "detail";

  @Input()
  set mode(mode: "edit" | "create" | "detail") {
    if (!isNullOrUndefined(mode) && !isNullOrUndefined(this._mode)) {
      if (this._mode !== mode) {
        this._mode = mode;
        this.focusFirstButtonElement();
      }
    } else {
      this._mode = mode;
    }
  }

  get mode(): "edit" | "create" | "detail" {
    return this._mode;
  }

  @Input()
  readonly typeButtonDelete: ButtonThemeEnum = ButtonThemeEnum.button;

  @Input()
  readonly typeButtonSave: ButtonThemeEnum = ButtonThemeEnum.flatButton;

  @Input()
  readonly colorButtonDelete: ButtonColorEnum = ButtonColorEnum.primary;

  private readonly _showHistoryBS: BehaviorSubject<TResourceModel | undefined> = new BehaviorSubject<TResourceModel | undefined>(undefined);
  @Output("showHistoryChange")
  readonly showHistoryObs: Observable<TResourceModel | undefined> = ObservableUtil.asObservable(this._showHistoryBS);

  private readonly _editBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("editChange")
  readonly editObs: Observable<void> = ObservableUtil.asObservable(this._editBS);

  private readonly _deleteBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("deleteChange")
  readonly deleteObs: Observable<void> = ObservableUtil.asObservable(this._deleteBS);

  private readonly _backToDetailBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("backToDetailChange")
  readonly backToDetailObs: Observable<void> = ObservableUtil.asObservable(this._backToDetailBS);

  constructor(protected readonly _injector: Injector) {
    super(_injector);
  }

  ngOnInit(): void {
    this.listOriginalButton = [
      {
        color: ButtonColorEnum.primary,
        icon: IconNamePartialEnum.back,
        labelToTranslate: (resource) => this.keyBackToDetailButtonToTranslate,
        order: 10,
        callback: () => this.backToDetail(),
        displayCondition: (resource) => this.mode === "edit",
      },
      {
        color: ButtonColorEnum.primary,
        icon: IconNamePartialEnum.back,
        labelToTranslate: (resource) => this.keyBackButtonToTranslate,
        order: 20,
        dataTest: DataTestPartialEnum.back,
        navigate: () => this.backToList(),
        displayCondition: (resource) => this.mode === "detail" || this.mode === "create",
      },
      {
        color: this.colorButtonDelete,
        typeButton: this.typeButtonDelete,
        icon: IconNamePartialEnum.delete,
        labelToTranslate: (resource) => this.keyDeleteButtonToTranslate,
        order: 40,
        dataTest: DataTestPartialEnum.delete,
        callback: () => this.delete(),
        displayCondition: (resource) => this.mode === "detail" && this.deleteAvailable,
        disableCondition: (resource) => !resource,
      },
      {
        color: ButtonColorEnum.primary,
        typeButton: this.typeButtonSave,
        icon: IconNamePartialEnum.save,
        labelToTranslate: (resource) => this.mode === "create" ? this.keyCreateButtonToTranslate : this.keySaveButtonToTranslate,
        order: 50,
        dataTest: DataTestPartialEnum.save,
        callback: () => this.save(),
        displayCondition: (resource) => this.mode !== "detail" && this.saveAvailable,
        disableCondition: () => this.formPresentational?.form?.pristine || this.formPresentational?.form?.invalid,
      },
    ];
    super.ngOnInit();
  }

  edit(): void {
    if (this.editAvailable) {
      this._editBS.next();
    }
  }

  delete(): void {
    if (this.deleteAvailable && !isNullOrUndefined(this.currentModel)) {
      this._deleteBS.next();
    }
  }

  backToDetail(): void {
    this._backToDetailBS.next();
  }

  showHistory(): void {
    this._showHistoryBS.next(this.currentModel);
  }

  private save(): void {
    this.formPresentational.onSubmit();
  }
}
