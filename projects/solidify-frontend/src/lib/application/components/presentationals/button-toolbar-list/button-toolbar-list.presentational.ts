/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - button-toolbar-list.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Component,
  ElementRef,
  Injector,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {DataTestPartialEnum} from "../../../../core-resources/enums/partial/data-test-partial.enum";
import {ButtonColorEnum} from "../../../../core/enums/button-color.enum";
import {ButtonThemeEnum} from "../../../../core/enums/button-theme.enum";
import {IconNamePartialEnum} from "../../../../core/enums/partial/icon-name-partial.enum";
import {BaseResource} from "../../../../core-resources/models/dto/base-resource.model";
import {ObservableUtil} from "../../../../core/utils/observable.util";
import {AbstractButtonToolbarPresentational} from "../abstract-button-toolbar/abstract-button-toolbar.presentational";

// eslint-disable-next-line
@Component({
  selector: "solidify-button-toolbar-list",
  templateUrl: "./button-toolbar-list.presentational.html",
  styleUrls: ["./button-toolbar-list.presentational.scss"],
  // changeDetection: ChangeDetectionStrategy.OnPush, // TODO Find a way to use OnPush strategy and still have ability to update disable and display condition (@Input immutable for change ? Observable ?)
})
export class ButtonToolbarListPresentational<TResourceModel extends BaseResource> extends AbstractButtonToolbarPresentational<TResourceModel> implements OnInit {
  @Input()
  readonly keyCreateButtonToTranslate: string = this.labelTranslateInterface.applicationCreate;

  @Input()
  readonly backAvailable: boolean = true;

  @Input()
  readonly createAvailable: boolean = true;

  @Input()
  readonly createDisabled: boolean = false;

  @Input()
  readonly typeButtonCreate: ButtonThemeEnum = ButtonThemeEnum.flatButton;

  private readonly _createBS: BehaviorSubject<ElementRef> = new BehaviorSubject<ElementRef>(undefined);
  @Output("createChange")
  readonly createObs: Observable<ElementRef> = ObservableUtil.asObservable(this._createBS);

  constructor(protected readonly _injector: Injector) {
    super(_injector);
  }

  ngOnInit(): void {
    this.listOriginalButton = [
      {
        color: ButtonColorEnum.primary,
        icon: IconNamePartialEnum.back,
        labelToTranslate: (resource) => this.keyBackButtonToTranslate,
        order: 10,
        displayCondition: () => this.backAvailable,
        navigate: () => this.backToList(),
        dataTest: DataTestPartialEnum.back,
      },
      {
        color: ButtonColorEnum.primary,
        typeButton: this.typeButtonCreate,
        icon: IconNamePartialEnum.create,
        labelToTranslate: (resource) => this.keyCreateButtonToTranslate,
        order: 20,
        displayCondition: (currentModel) => this.createAvailable,
        disableCondition: (currentModel) => this.createDisabled,
        callback: (currentModel, buttonElementRef) => this.create(buttonElementRef),
        dataTest: DataTestPartialEnum.create,
      },
    ];
    super.ngOnInit();
  }

  create(buttonElementRef: ElementRef): void {
    this._createBS.next(buttonElementRef);
  }
}
