/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-button-toolbar.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  ElementRef,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import {Params} from "@angular/router";
import {Navigate} from "@ngxs/router-plugin";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {BaseResource} from "../../../../core-resources/models/dto/base-resource.model";
import {
  isInstanceOf,
  isNotNullNorUndefined,
  isNullOrUndefinedOrWhiteString,
  isString,
  isTruthyObject,
} from "../../../../core-resources/tools/is/is.tool";
import {SsrUtil} from "../../../../core-resources/utils/ssr.util";
import {AbstractInternalPresentational} from "../../../../core/components/presentationals/abstract-internal/abstract-internal.presentational";
import {ButtonColorEnum} from "../../../../core/enums/button-color.enum";
import {ExtraButtonToolbar} from "../../../../core/models/datatable/extra-button-toolbar.model";
import {ObservableUtil} from "../../../../core/utils/observable.util";

// @dynamic
@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractButtonToolbarPresentational<TResourceModel extends BaseResource> extends AbstractInternalPresentational implements OnInit {
  @Input()
  readonly keyBackButtonToTranslate: string = this.labelTranslateInterface.applicationBackToList;

  @Input()
  isLoading: boolean;

  _listExtraButtons?: ExtraButtonToolbar<TResourceModel>[];

  @Input()
  set listExtraButtons(value: ExtraButtonToolbar<TResourceModel>[]) {
    this._listExtraButtons = value;
    this._mergeExtraButtonWithButtons();
  }

  get listExtraButtons(): ExtraButtonToolbar<TResourceModel>[] {
    return this._listExtraButtons;
  }

  @Input()
  listExtraSmallButtons?: ExtraButtonToolbar<TResourceModel>[];

  @Input()
  backToListNavigate: Navigate;

  listOriginalButton?: ExtraButtonToolbar<TResourceModel>[];
  listButtonMerged?: ExtraButtonToolbar<TResourceModel>[];

  private readonly _navigateBS: BehaviorSubject<Navigate> = new BehaviorSubject<Navigate>(undefined);
  @Output("navigateChange")
  readonly navigateObs: Observable<Navigate> = ObservableUtil.asObservable(this._navigateBS);

  @ViewChild("mainButtonElement", {static: true})
  mainButtonElement: ElementRef;

  get buttonColorEnum(): typeof ButtonColorEnum {
    return ButtonColorEnum;
  }

  constructor(protected readonly _injector: Injector) {
    super(_injector);
  }

  focusFirstButtonElement(): void {
    if (SsrUtil.isServer) {
      return;
    }
    setTimeout(() => {
      if (isTruthyObject(this.mainButtonElement)) {
        this.mainButtonElement.nativeElement.querySelectorAll("*").forEach((elementToFocus: HTMLElement) => {
            elementToFocus.focus();
          },
        );
      }
    });
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._mergeExtraButtonWithButtons();
  }

  private _mergeExtraButtonWithButtons(): void {
    const originalButtons = isNotNullNorUndefined(this.listOriginalButton) ? this.listOriginalButton : [];
    const extraButtons = isNotNullNorUndefined(this.listExtraButtons) ? this._listExtraButtons : [];
    this.listButtonMerged = [...originalButtons, ...extraButtons];
    this.listButtonMerged.sort((a, b) => {
      const orderA = a.order || 0;
      const orderB = b.order || 0;
      if (orderA === orderB) {
        return 0;
      } else if (orderA < orderB) {
        return -1;
      }
      return 1;
    });
  }

  backToList(): Navigate {
    if (isNotNullNorUndefined(this.backToListNavigate)) {
      return this.backToListNavigate;
    }
  }

  navigate(navigate: Navigate | string): void {
    if (isInstanceOf(navigate, Navigate)) {
      this._navigateBS.next(navigate);
    } else if (isString(navigate)) {
      SsrUtil.window.open(navigate, "_blank");
    } else {
      // eslint-disable-next-line no-console
      console.warn("Unmanaged navigate value type", navigate);
    }
  }

  getRouterLink(navigate: Navigate | string): string | undefined {
    if (isInstanceOf(navigate, Navigate)) {
      return "/" + navigate.path.join("/");
    }
    return undefined;
  }

  getHref(navigate: Navigate | string): string {
    if (isString(navigate)) {
      return navigate;
    }
    return undefined;
  }

  getQueryParamValue(navigate: Navigate | string): Params | undefined {
    if (isInstanceOf(navigate, Navigate)) {
      return navigate.queryParams;
    }
    return undefined;
  }

  getTarget(button: ExtraButtonToolbar<TResourceModel>): "_blank" | "_parent" | "_self" | "top" {
    return isNullOrUndefinedOrWhiteString(button.navigateTarget) ? "_self" : button.navigateTarget;
  }
}
