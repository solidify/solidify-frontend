/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - file-upload-tile.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  Output,
} from "@angular/core";
import {ThemePalette} from "@angular/material/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "../../../../core-resources/tools/is/is.tool";
import {AbstractInternalPresentational} from "../../../../core/components/presentationals/abstract-internal/abstract-internal.presentational";
import {FileUploadStatusEnum} from "../../../../core/enums/upload/file-upload-status.enum";
import {UploadFileStatus} from "../../../../core/models/upload/upload-file-status.model";
import {ObservableUtil} from "../../../../core/utils/observable.util";

@Component({
  selector: "solidify-file-upload-tile",
  templateUrl: "./file-upload-tile.presentational.html",
  styleUrls: ["./file-upload-tile.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FileUploadTilePresentational<TDataFile extends any> extends AbstractInternalPresentational {
  @Input()
  dataFile: TDataFile;

  @Input()
  uploadStatus: UploadFileStatus<TDataFile>;

  private readonly _cancelBS: BehaviorSubject<UploadFileStatus<TDataFile> | undefined> = new BehaviorSubject<UploadFileStatus<TDataFile> | undefined>(undefined);
  @Output("cancelChange")
  readonly cancelObs: Observable<UploadFileStatus<TDataFile> | undefined> = ObservableUtil.asObservable(this._cancelBS);

  private readonly _retryBS: BehaviorSubject<UploadFileStatus<TDataFile> | undefined> = new BehaviorSubject<UploadFileStatus<TDataFile> | undefined>(undefined);
  @Output("retryChange")
  readonly retryObs: Observable<UploadFileStatus<TDataFile> | undefined> = ObservableUtil.asObservable(this._retryBS);

  get isUploaded(): boolean {
    return isNotNullNorUndefined(this.dataFile) && isNullOrUndefined(this.uploadStatus);
  }

  get isUploading(): boolean {
    return !this.isUploaded;
  }

  get color(): ThemePalette {
    if (this.uploadStatus.status === FileUploadStatusEnum.inError) {
      return "warn";
    }
    if (this.uploadStatus.status === FileUploadStatusEnum.completed) {
      return "accent";
    }
  }

  get fileUploadStatusEnum(): typeof FileUploadStatusEnum {
    return FileUploadStatusEnum;
  }

  cancel(): void {
    this._cancelBS.next(this.uploadStatus);
  }

  retry(): void {
    this._retryBS.next(this.uploadStatus);
  }
}
