/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - folder-tree.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {CdkDragDrop} from "@angular/cdk/drag-drop";
import {FlatTreeControl} from "@angular/cdk/tree";
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  MatTreeFlatDataSource,
  MatTreeFlattener,
} from "@angular/material/tree";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  isEmptyString,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
} from "../../../../core-resources/tools/is/is.tool";
import {MappingObject} from "../../../../core-resources/types/mapping-type.type";
import {MappingObjectUtil} from "../../../../core-resources/utils/mapping-object.util";
import {ObjectUtil} from "../../../../core-resources/utils/object.util";
import {StringUtil} from "../../../../core-resources/utils/string.util";
import {AbstractInternalPresentational} from "../../../../core/components/presentationals/abstract-internal/abstract-internal.presentational";
import {SOLIDIFY_CONSTANTS} from "../../../../core-resources/constants";
import {ObservableUtil} from "../../../../core/utils/observable.util";

class FileFlatNode {
  constructor(public folderName: string,
              public fullFolderName: string,
              public level: number = 0,
              public expandable: boolean = false,
              public postCreation: boolean = false) {
  }
}

class FileNode {
  constructor(public folderName: string,
              public fullFolderName: string,
              public children: FileNode[]) {
  }
}

@Component({
  selector: "solidify-folder-tree",
  templateUrl: "./folder-tree.presentational.html",
  styleUrls: ["./folder-tree.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FolderTreePresentational<TDataFile extends any> extends AbstractInternalPresentational implements OnInit, AfterViewInit {
  private readonly _SEPARATOR: string = SOLIDIFY_CONSTANTS.SEPARATOR;
  readonly ROOT: string = SOLIDIFY_CONSTANTS.SEPARATOR;
  static readonly ROOT: string = SOLIDIFY_CONSTANTS.SEPARATOR;

  readonly indentation: number = 10;
  readonly CDK_DROP_LIST_PREFIX: string;

  expandedNodes: string[] = [];

  @Input()
  currentFolder: string | undefined;

  @Input()
  relativeLocationKey: string = "relativeLocation";

  private _intermediateFolders: string[];

  @Input()
  set intermediateFolders(value: string[]) {
    this._intermediateFolders = value;
    this.triggerInitIfReady();
  }

  get intermediateFolders(): string[] {
    return this._intermediateFolders;
  }

  private readonly _selectBS: BehaviorSubject<string> = new BehaviorSubject<string>(undefined);
  @Output("selectChange")
  readonly selectObs: Observable<string> = ObservableUtil.asObservable(this._selectBS);

  private readonly _moveBS: BehaviorSubject<TDataFile> = new BehaviorSubject<TDataFile>(undefined);
  @Output("moveChange")
  readonly moveObs: Observable<TDataFile> = ObservableUtil.asObservable(this._moveBS);

  private readonly _downloadFolderBS: BehaviorSubject<string> = new BehaviorSubject<string>(undefined);
  @Output("downloadFolder")
  readonly downloadFolderObs: Observable<string> = ObservableUtil.asObservable(this._downloadFolderBS);

  private readonly _deleteFolderBS: BehaviorSubject<string> = new BehaviorSubject<string>(undefined);
  @Output("deleteFolder")
  readonly deleteFolderObs: Observable<string> = ObservableUtil.asObservable(this._deleteFolderBS);

  private readonly _uploadInFolderBS: BehaviorSubject<string> = new BehaviorSubject<string>(undefined);
  @Output("uploadInFolder")
  readonly uploadInFolderObs: Observable<string> = ObservableUtil.asObservable(this._uploadInFolderBS);

  @Input()
  isLoading: boolean;

  @Input()
  expandFirstLevel: boolean;

  @Input()
  canDelete: boolean = false;

  @Input()
  canUpload: boolean = false;

  @Input()
  canDownload: boolean = true;

  @Input()
  draggingDataFile: TDataFile | undefined;

  isReady: boolean = false;

  private _foldersWithIntermediateFolders: string[];

  @Input()
  set foldersWithIntermediateFolders(value: string[]) {
    this._foldersWithIntermediateFolders = value;
    if (this.isReady) {
      this._setupFoldersWithIntermediateFolders();
    } else {
      this.triggerInitIfReady();
    }
  }

  get foldersWithIntermediateFolders(): string[] {
    return this._foldersWithIntermediateFolders;
  }

  constructor(protected readonly _injector: Injector) {
    super(_injector);
    this.CDK_DROP_LIST_PREFIX = this.environment.cdkDropListIdPrefix;
  }

  private _init(): void {
    this._setupFoldersWithIntermediateFolders();
  }

  private _setupFoldersWithIntermediateFolders(): void {
    this.saveExpandedNodes();
    if (isNullOrUndefined(this._foldersWithIntermediateFolders)) {
      return;
    }
    const listFolders = [...this._foldersWithIntermediateFolders];
    this.dataSource.data = this._transformStringListToFileNodeTree(listFolders);
    this._expandFirstLevelTree();
    this.restoreExpandedNodes();
  }

  private _transformStringListToFileNodeTree(listFoldersPath: string[]): FileNode[] {
    const nodesMap: MappingObject<string, FileNode> = {};
    const rootNode = new FileNode(this.ROOT, this.ROOT, []);
    MappingObjectUtil.set(nodesMap, rootNode.fullFolderName, rootNode);

    listFoldersPath.forEach(folderPath => {
      const listFolders: string[] = folderPath.split(this._SEPARATOR).filter(isNotNullNorUndefinedNorWhiteString);

      let currentParent: FileNode = rootNode;
      let pathBuilder = this.ROOT;

      listFolders.forEach(folder => {
        pathBuilder += (pathBuilder === this.ROOT ? SOLIDIFY_CONSTANTS.STRING_EMPTY : this._SEPARATOR) + folder;

        if (!MappingObjectUtil.has(nodesMap, pathBuilder)) {
          const newNode: FileNode = new FileNode(folder, pathBuilder, []);
          MappingObjectUtil.set(nodesMap, pathBuilder, newNode);
          currentParent.children.push(newNode);
          currentParent = newNode;
        } else {
          currentParent = MappingObjectUtil.get(nodesMap, pathBuilder);
        }
      });
    });
    // Return only root node "/" that contains all other nodes in children or in children of nested nodes
    return [rootNode];
  }

  saveExpandedNodes(): void {
    this.expandedNodes = [];
    if (isNullOrUndefined(this.treeControl.dataNodes)) {
      return;
    }
    this.treeControl.dataNodes.forEach(node => {
      if (node.expandable && this.treeControl.isExpanded(node)) {
        this.expandedNodes.push(node.fullFolderName);
      }
    });
  }

  restoreExpandedNodes(): void {
    this.expandedNodes.forEach(node => {
      this.treeControl.expand(this.treeControl.dataNodes.find(n => n.fullFolderName === node));
    });
  }

  private _transformer = (node: FileNode): FileFlatNode => ({
    expandable: isNonEmptyArray(node.children),
    folderName: node.folderName,
    fullFolderName: node.fullFolderName,
    level: this.getLevel(node.fullFolderName),
    postCreation: this._isIntermediateFolderCreate(node.fullFolderName),
  });

  treeFlattener: MatTreeFlattener<FileNode, FileFlatNode>;
  treeControl: FlatTreeControl<FileFlatNode>;
  dataSource: MatTreeFlatDataSource<FileNode, FileFlatNode>;

  expandedAll: boolean = true;
  eventMouseOver: MouseEvent | undefined = undefined;

  ngOnInit(): void {
    if (this.isReady) {
      return;
    }
    this.treeControl = new FlatTreeControl<FileFlatNode>(node => node.level, node => node.expandable);
    this.treeFlattener = new MatTreeFlattener(this._transformer, node => node.level, node => node.expandable, node => node.children);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
    this.triggerInitIfReady();
  }

  triggerInitIfReady(): void {
    if (this.isReady === false) {
      if (isNotNullNorUndefined(this.treeControl) && isNotNullNorUndefined(this.treeFlattener) && isNotNullNorUndefined(this.dataSource) && isNotNullNorUndefined(this.intermediateFolders) && isNotNullNorUndefined(this.foldersWithIntermediateFolders)) {
        this.isReady = true;
        this._init();
      }
    }
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
    if (isNullOrUndefined(this.treeControl.dataNodes) || this.treeControl.dataNodes.length === 0) {
      return;
    }
    this._expandFirstLevelTree();
  }

  private _expandFirstLevelTree(): void {
    if (this.currentFolder !== this.ROOT) {
      let node = this.treeControl.dataNodes.find(d => d.fullFolderName === this.currentFolder);
      if (isNullOrUndefined(node)) {
        const urlOfFolder = this.currentFolder?.substring(0, this.currentFolder.lastIndexOf(this._SEPARATOR));
        const lastParentPath = isNotNullNorUndefinedNorWhiteString(urlOfFolder) ? urlOfFolder : this.ROOT;
        node = this.treeControl.dataNodes.find(d => d.fullFolderName === lastParentPath);
        this.currentFolder = lastParentPath;
      }
      this._expandRecursivelyParent(node);
      this.treeControl.expand(node);
      return;
    }
    if (this._expandFirstLevelTree) {
      const firstNode = this.treeControl.dataNodes[0];
      this.treeControl.expand(firstNode);
    }
  }

  private _expandRecursivelyParent(node: FileFlatNode): void {
    if (isNullOrUndefined(node)) {
      return;
    }
    if (node.fullFolderName === this.ROOT) {
      return;
    }
    let parentPath = node.fullFolderName.substring(0, node.fullFolderName.lastIndexOf(this._SEPARATOR));
    if (parentPath === StringUtil.stringEmpty) {
      parentPath = this.ROOT;
    }
    const parentNode = this.treeControl.dataNodes.find(d => d.fullFolderName === parentPath);
    this.treeControl.expand(parentNode);
    this._expandRecursivelyParent(parentNode);
  }

  expandAllOrCollapseAll(): void {
    const parentNode = this.treeControl.dataNodes.find(d => d.folderName === this.ROOT);
    if (this.treeControl.isExpanded(parentNode)) {
      this.treeControl.collapseAll();
      this.expandedAll = false;
    } else {
      this.treeControl.expandAll();
      this.expandedAll = true;
    }
  }

  getLevel(fullFolderName: string): number {
    if (fullFolderName === this._SEPARATOR) {
      return 0;
    }
    const splitted = fullFolderName.split(this._SEPARATOR);
    return splitted.length - 1;
  }

  hasChild = (_: number, node: FileFlatNode): boolean => node.expandable;

  select(node: FileFlatNode): void {
    if (node.fullFolderName === this.currentFolder) {
      return;
    }
    this.currentFolder = node.fullFolderName;
    this._selectBS.next(node.fullFolderName);
  }

  private _existChild(currentFullFolderName: string): boolean {
    if (currentFullFolderName === this.ROOT) {
      return true;
    }
    const item = this.dataSource.data.find(node => node.fullFolderName.startsWith(currentFullFolderName + this._SEPARATOR));
    return !isNullOrUndefined(item);
  }

  private _isIntermediateFolderCreate(fullFolderName: string): boolean {
    return isNullOrUndefined(this.intermediateFolders) || this.intermediateFolders.includes(fullFolderName);
  }

  drop($event: CdkDragDrop<any, any>, folderFullName: string): void {
    const researchDataFile = ObjectUtil.clone($event.item.data as TDataFile);
    if (researchDataFile[this.relativeLocationKey] === folderFullName) {
      return;
    }
    researchDataFile[this.relativeLocationKey] = folderFullName;
    this._moveBS.next(researchDataFile);
  }

  openFolder(node: FileFlatNode): void {
    // console.error("ENTER", node);
    // this.treeControl.expand(node);

    // TODO Need to add to dropzone list child node of current node expanded
    // this.treeControl.expand(node);
  }

  downloadFolder(node: FileFlatNode): void {
    this._downloadFolderBS.next(node.fullFolderName);
  }

  deleteFolder(node: FileFlatNode): void {
    this._deleteFolderBS.next(node.fullFolderName);
  }

  upload(node: FileFlatNode): void {
    this._uploadInFolderBS.next(node.fullFolderName);
  }

  mouseOver($event: MouseEvent, node: FileFlatNode): void {
    const dragInProgress = isNotNullNorUndefined(this.draggingDataFile);
    if (dragInProgress) {
      this.eventMouseOver = $event;
      setTimeout(() => {
        if ($event === this.eventMouseOver) {
          this.treeControl.expand(node);
        }
      }, 500);
    }
  }

  mouseOut($event: MouseEvent, node: FileFlatNode): void {
    this.eventMouseOver = undefined;
  }

  static computeCurrentFolderAfterDelete(intermediateFolders: string[], foldersWithIntermediateFolders: string[], searchString: string): string {
    if (isNullOrUndefinedOrWhiteString(searchString)) {
      return undefined;
    }
    const computeRecursively = (currentSearchString: string): string => {
      // Extract the URL without the ROOT
      const url = currentSearchString.substring(0, currentSearchString.lastIndexOf(FolderTreePresentational.ROOT));

      if (isEmptyString(url)) {
        return FolderTreePresentational.ROOT;
      } else {
        const folderWithIntFolders = foldersWithIntermediateFolders.find(folder => folder === url);
        const intFolder = intermediateFolders.find(ft => ft === folderWithIntFolders);

        if (isNotNullNorUndefined(intFolder)) {
          // Find a folder with a child.
          const parentWithChild = foldersWithIntermediateFolders.find(folder => folder.startsWith(intFolder) &&
            (folder.length === searchString.length && folder !== searchString) && folder.substring(intFolder.length).length > 1);

          if (isNotNullNorUndefined(parentWithChild)) {
            return intFolder;
          } else {
            // Recursively call the internal function with the updated search string
            return computeRecursively(url);
          }
        } else {
          return folderWithIntFolders;
        }
      }
    };
    // Call the internal function with the original search string
    return computeRecursively(searchString);
  }

}
