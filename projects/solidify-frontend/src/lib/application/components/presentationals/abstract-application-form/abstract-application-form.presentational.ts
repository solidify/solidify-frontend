/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-application-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectorRef,
  Directive,
  ElementRef,
  Injector,
  OnInit,
} from "@angular/core";
import {
  isNotNullNorUndefined,
  isNull,
} from "../../../../core-resources/tools/is/is.tool";
import {AbstractFormPresentational} from "../../../../core/components/presentationals/abstract-form/abstract-form.presentational";
import {BaseResourceWithFile} from "../../../../core/models/dto/base-resource-with-logo.model";
import {BaseResourceType} from "../../../../core-resources/models/dto/base-resource.model";

// @dynamic
@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractApplicationFormPresentational<TResource extends BaseResourceType> extends AbstractFormPresentational<TResource> implements OnInit {
  currentLogoBlob: Blob | undefined | null = undefined;

  protected constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
                        protected readonly _elementRef: ElementRef,
                        protected readonly _injector: Injector) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  logoChange(blob: Blob | undefined | null): void {
    this.currentLogoBlob = blob;
    if (this.readonly) {
      this.enterInEditMode();
    }
    if (!this.form.dirty) {
      setTimeout(() => {
        this.form.markAsDirty();
        this._dirtyBS.next(this.form.dirty);
        this._changeDetectorRef.detectChanges();
      }, 0);
    }
  }

  override _internalTreatmentBeforeSubmit(model: TResource): TResource {
    if (isNotNullNorUndefined(this.currentLogoBlob) || isNull(this.currentLogoBlob)) {
      (model as BaseResourceWithFile).newPendingFile = this.currentLogoBlob;
    }
    return model;
  }
}
