/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - public_api.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

// Containers

export * from "./containers/abstract-app-component/solidify-application-abstract-app.component";
export * from "./containers/abstract-table-person-resource-role/abstract-table-resource-role.container";
export * from "./containers/additional-information-panel/additional-information-panel.container";
export * from "./containers/ark-menu/ark-menu.container";
export * from "./containers/carousel/carousel.container";
export * from "./containers/doi-menu/doi-menu.container";

// Dialogs

export * from "./dialogs/status-history/status-history.dialog";
export * from "./dialogs/token/token.dialog";

// Presentationals

export * from "./presentationals/abstract-application-form/abstract-application-form.presentational";
export * from "./presentationals/abstract-button-toolbar/abstract-button-toolbar.presentational";
export * from "./presentationals/button-toolbar-detail/button-toolbar-detail.presentational";
export * from "./presentationals/button-toolbar-list/button-toolbar-list.presentational";
export * from "./presentationals/burger-menu/burger-menu.presentational";
export * from "./presentationals/data-files-upload-in-progress/data-files-upload-in-progress.presentational";
export * from "./presentationals/file-upload-tile/file-upload-tile.presentational";
export * from "./presentationals/folder-tree/folder-tree.presentational";

// Routables

export * from "./routables/about/about.routable";
export * from "./routables/abstract-crud/abstract-crud.routable";
export * from "./routables/abstract-create/abstract-create.routable";
export * from "./routables/abstract-create-composition/abstract-create-composition.routable";
export * from "./routables/abstract-detail-edit/abstract-detail-edit.routable";
export * from "./routables/abstract-detail-edit-common/abstract-detail-edit-common.routable";
export * from "./routables/abstract-detail-edit-common-composition/abstract-detail-edit-common-composition.routable";
export * from "./routables/abstract-list/abstract-list.routable";
export * from "./routables/abstract-list-composite/abstract-list-composite.routable";
export * from "./routables/abstract-resource/abstract-resource.routable";
export * from "./routables/color-check/color-check.routable";
export * from "./routables/maintenance-mode/solidify-application-maintenance-mode.routable";
export * from "./routables/page-not-found/page-not-found.routable";
