/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - security.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {isNullOrUndefined} from "../../core-resources/tools/is/is.tool";
import {UserApplicationRoleEnum} from "../../core/enums/user-application-role.enum";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {AbstractBaseService} from "../../core/services/abstract-base.service";
import {MemoizedUtil} from "../../core/utils/stores/memoized.util";
import {User} from "../../user/models/dto/user.model";
import {StateApplicationEnum} from "../enums/state-application.enum";
import {DefaultSolidifyApplicationEnvironment} from "../environments/environment.solidify-application";

@Injectable()
export class SolidifySecurityService extends AbstractBaseService {

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyApplicationEnvironment) {
    super();
  }

  isRootOrAdmin(): boolean {
    return this.isRoot() || this.isAdmin();
  }

  isRoot(): boolean {
    const applicationRole = this.getApplicationRole();
    if (isNullOrUndefined(applicationRole)) {
      return false;
    }
    return applicationRole === UserApplicationRoleEnum.root;
  }

  isAdmin(): boolean {
    const applicationRole = this.getApplicationRole();
    if (isNullOrUndefined(applicationRole)) {
      return false;
    }
    return applicationRole === UserApplicationRoleEnum.admin;
  }

  isUser(): boolean {
    const applicationRole = this.getApplicationRole();
    if (isNullOrUndefined(applicationRole)) {
      return false;
    }
    return applicationRole === UserApplicationRoleEnum.user;
  }

  getApplicationRole(): UserApplicationRoleEnum {
    const currentUser = MemoizedUtil.selectSnapshot(this._store, this._environment.appState, state => state?.application_user?.current);
    return this._getUserRole(currentUser);
  }

  getApplicationRoleObs(): Observable<UserApplicationRoleEnum> {
    return MemoizedUtil.select(this._store, this._environment.appState, state => state?.[StateApplicationEnum.application_user]?.current).pipe(
      map(currentUser => this._getUserRole(currentUser)),
    );
  }

  isLoggedIn(): boolean {
    return MemoizedUtil.selectSnapshot(this._store, this._environment.appState, state => state.isLoggedIn);
  }

  isLoggedInObs(): Observable<boolean> {
    return MemoizedUtil.select(this._store, this._environment.appState, state => state.isLoggedIn);
  }

  private _getUserRole(currentUser: User): UserApplicationRoleEnum | undefined {
    if (isNullOrUndefined(currentUser)) {
      return undefined;
    }
    return isNullOrUndefined(currentUser?.applicationRole?.resId) ? UserApplicationRoleEnum.user : currentUser.applicationRole.resId;
  }
}
