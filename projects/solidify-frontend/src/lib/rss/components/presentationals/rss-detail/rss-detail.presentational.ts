/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - rss-detail.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {AbstractInternalPresentational} from "../../../../core/components/presentationals/abstract-internal/abstract-internal.presentational";
import {isNullOrUndefined} from "../../../../core-resources/tools/is/is.tool";
import {RssItem} from "../../../models/rss.model";

@Component({
  selector: "solidify-rss-detail-presentational",
  templateUrl: "./rss-detail.presentational.html",
  styleUrls: ["./rss-detail.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RssDetailPresentational extends AbstractInternalPresentational {
  @Input()
  item: RssItem;

  getImage(item: RssItem): string | undefined {
    if (isNullOrUndefined(item) || isNullOrUndefined(item.enclosure) || isNullOrUndefined(item.enclosure["@attributes"] || isNullOrUndefined(item.enclosure["@attributes"].url))) {
      return undefined;
    }
    return item.enclosure["@attributes"].url;
  }
}
