/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - rss-wrapper.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {AbstractInternalPresentational} from "../../../../core/components/presentationals/abstract-internal/abstract-internal.presentational";
import {
  Rss,
  RssItem,
} from "../../../models/rss.model";

@Component({
  selector: "solidify-rss-wrapper-presentational",
  templateUrl: "./rss-wrapper.presentational.html",
  styleUrls: ["./rss-wrapper.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RssWrapperPresentational extends AbstractInternalPresentational {
  private readonly _maxNumberOfNewsToDisplay: number = 5;

  @Input()
  rss: Rss;

  get rssItems(): RssItem[] {
    if (this.rss.channel.item.length > this._maxNumberOfNewsToDisplay) {
      return this.rss.channel.item.slice(0, this._maxNumberOfNewsToDisplay);
    }
    return this.rss.channel.item;
  }
}
