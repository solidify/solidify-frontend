/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - server-environment.defaults.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {MappingObject} from "../../core-resources/types/mapping-type.type";
import {ServerLogLevelEnum} from "../enums/server-log-level.enum";
import {SsrRouteOption} from "../models/ssr-route-option.model";

export interface DefaultSolidifyServerEnvironment {
  production: boolean;
  isActiveMQCacheClean: boolean;
  activeMQBrokerUrl: string;
  activeMQBrokerLogin: string;
  activeMQBrokerPassword: string;
  activeMQReconnectDelay: number;
  activeMQHeartbeatIncoming: number;
  activeMQHeartbeatOutgoing: number;
  activeMQEnv: string;
  activeMQSsrCacheQueueName: string;
  activeMQResourceCacheEvictMap: MappingObject<string, string>;
  projectName: string;
  browserLocation: string;
  ssrWorkerLocation: string;
  port: number;
  ssrRoutes: SsrRouteOption[];
  /**
   * Used only for server-environment.runtime.json to provide new routes
   */
  additionalSsrRoutes: SsrRouteOption[];
  generalCacheForbidden: boolean;
  /**
   * Folder used to store ssr cache. If not defined, stored near to main.js in .ssr-cache directory
   */
  cacheLocation: string | undefined;
  /**
   * Size of md5 trim to form cache path
   */
  cachePathMd5TrimSize: number;
  /**
   * Default language used to return ssr page if no cookie is defined (ex: "fr-CH" | "fr-FR" | "en-GB" | "en-US")
   */
  defaultLanguageCulture: string;
  staticRoutes: string[];
  cleanCacheEndpointsBasicAuthLogin: string;
  cleanCacheEndpointsBasicAuthPassword: string;
  logLevel: ServerLogLevelEnum;
  logSsrBrowserExecution: boolean;
  logActiveMQDebug: boolean;
  logHttpTime: boolean;
  logRotationMaxFiles: number | undefined;
  logRotationMaxSize: number | undefined;
  /**
   * The maximum size of the main heap in MB.
   */
  workerResourceLimitsMaxOldGenerationSizeMb: number | undefined;
  /**
   * The default maximum stack size for the thread. Small values may lead to unusable Worker instances.
   *
   * @default 4
   */
  workerResourceLimitsStackSizeMb: number | undefined;
  /**
   * The size of a pre-allocated memory range used for generated code.
   */
  workerResourceLimitsCodeRangeSizeMb: number | undefined;
  /**
   * The maximum size of a heap space for recently created objects.
   */
  workerResourceLimitsMaxYoungGenerationSizeMb: number | undefined;
  /**
   * Represents the command-line arguments passed to a worker thread.
   * This variable stores an array of strings representing the command-line arguments.
   * If the worker thread was not started with any command-line arguments,
   * the value of this variable will be undefined.
   *
   * @type {string[] | undefined}
   */
  workerExecArgv: string[] | undefined;
  /**
   * The workerArgv variable is a parameter that can accept an array of any type or can be undefined.
   * It is used to pass arguments to a worker thread in JavaScript.
   *
   * @type {any[] | undefined}
   */
  workerArgv: any[] | undefined;
  /**
   * Restart the worker when exit
   */
  workerRestartOnExit: boolean;
  /**
   * Number of workers
   */
  workerNumber: number;
}
