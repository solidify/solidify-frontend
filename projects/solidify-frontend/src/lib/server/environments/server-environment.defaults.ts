/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - server-environment.defaults.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {ServerLogLevelEnum} from "../enums/server-log-level.enum";
import {DefaultSolidifyServerEnvironment} from "./server-environment.defaults.model";

export const defaultSolidifyServerEnvironment: DefaultSolidifyServerEnvironment = {
  production: true,
  isActiveMQCacheClean: true,
  activeMQResourceCacheEvictMap: {},
  activeMQBrokerUrl: "ws://localhost:61614/ws",
  activeMQBrokerLogin: undefined,
  activeMQBrokerPassword: undefined,
  activeMQReconnectDelay: 5000,
  activeMQHeartbeatIncoming: 20000,
  activeMQHeartbeatOutgoing: 20000,
  activeMQEnv: "app",
  activeMQSsrCacheQueueName: "ssr-cache",
  projectName: "app-portal",
  browserLocation: "/usr/share/nginx/html",
  ssrWorkerLocation: "/usr/share/nginx/ssr-worker",
  port: 3000,
  ssrRoutes: [],
  additionalSsrRoutes: [],
  generalCacheForbidden: false,
  cacheLocation: undefined,
  cachePathMd5TrimSize: 4,
  defaultLanguageCulture: "en-GB",
  staticRoutes: [
    "assets/**",
    "/assets/**",
    "./assets/**",
    "./**",
    "*.*",
  ],
  cleanCacheEndpointsBasicAuthLogin: "root",
  cleanCacheEndpointsBasicAuthPassword: "123abc",
  logLevel: ServerLogLevelEnum.info,
  logSsrBrowserExecution: false,
  logActiveMQDebug: false,
  logHttpTime: false,
  logRotationMaxFiles: 10,
  logRotationMaxSize: undefined,
  workerResourceLimitsMaxOldGenerationSizeMb: undefined,
  workerResourceLimitsStackSizeMb: undefined,
  workerResourceLimitsCodeRangeSizeMb: undefined,
  workerResourceLimitsMaxYoungGenerationSizeMb: undefined,
  workerRestartOnExit: true,
  workerExecArgv: undefined,
  workerArgv: undefined,
  workerNumber: 1,
};
