/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - server-logger.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {Logger} from "winston";
import {ServerLogLevelEnum} from "../enums/server-log-level.enum";
import {DefaultSolidifyServerEnvironment} from "../environments/server-environment.defaults.model";
import {NodeToolsModel} from "../models/node-tools.model";

export class ServerLoggerHelper {
  static initLogger(nodeTools: NodeToolsModel, serverEnvironment: DefaultSolidifyServerEnvironment, logLocation: string, filename: string): Logger {
    const logsLocation = nodeTools.join(logLocation, "logs");
    const customFormat = nodeTools.winston.format.printf(({level, message, timestamp}) => `${timestamp} ${level}: ${message}`);
    return nodeTools.winston.createLogger({
      level: serverEnvironment.logLevel,
      format: nodeTools.winston.format.combine(
        nodeTools.winston.format.timestamp(),
        customFormat,
      ),
      transports: [
        new nodeTools.winston.transports.Console({
          level: ServerLogLevelEnum.debug,
        }),
        new nodeTools.winstonDailyRotateFile({
          dirname: logsLocation,
          filename: filename + ".log",
          level: serverEnvironment.logLevel,
          maxFiles: serverEnvironment.logRotationMaxFiles,
          maxSize: serverEnvironment.logRotationMaxSize,
        }),
      ],
    });
  }
}
