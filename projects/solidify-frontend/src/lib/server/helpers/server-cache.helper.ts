/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - server-cache.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Logger} from "winston";
import {
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
} from "../../core-resources/tools/is/is.tool";
import {StringUtil} from "../../core-resources/utils/string.util";
import {ThemeModeEnum} from "../enums/theme-mode.enum";
import {DefaultSolidifyServerEnvironment} from "../environments/server-environment.defaults.model";
import {CacheRequest} from "../models/cache.request";
import {SsrRequestInfo} from "../models/ssr-request-info.model";
import {SsrCache} from "../services/server-cache.service";
import {ServerRequestHelper} from "./server-request.helper";

export class ServerCacheHelper {
  static generateCacheRequest(logger: Logger, serverEnvironment: DefaultSolidifyServerEnvironment, ssrRequestInfo: SsrRequestInfo): CacheRequest | undefined {
    const requestOption = ServerRequestHelper.getRequestOption(serverEnvironment.ssrRoutes, ssrRequestInfo.url);
    if (isNullOrUndefined(requestOption)) {
      logger.error("Unable to found requestOption for url " + ssrRequestInfo.url);
      return undefined;
    }
    const cacheRequest: Partial<CacheRequest> = {
      req: ssrRequestInfo,
      requestOption: requestOption,
      language: ServerRequestHelper.getRequestLanguage(),
      themeMode: ServerRequestHelper.getRequestThemeMode(),
    };
    if (requestOption.withId) {
      cacheRequest.resource = requestOption.resource;
      cacheRequest.resId = ssrRequestInfo.paramId;
      if (isNotNullNorUndefinedNorWhiteString(requestOption.idMatcher)) {
        const matching = ssrRequestInfo.url.match(new RegExp(requestOption.idMatcher));
        if (isNotNullNorUndefined(matching) && matching.length >= 2) {
          cacheRequest.resId = matching[1];
          ssrRequestInfo.paramId = cacheRequest.resId;
        }
      }
    }
    cacheRequest.name = requestOption.name;

    cacheRequest.cacheName = this.generateCacheName(cacheRequest.name, cacheRequest.resId);
    cacheRequest.cacheVersion = this.generateCacheVersion(cacheRequest.themeMode, cacheRequest.language);
    cacheRequest.cacheKey = this.generateCacheKey(cacheRequest.cacheName, cacheRequest.cacheVersion);
    return cacheRequest as CacheRequest;
  }

  static async cleanCacheByResourceId(logger: Logger, serverEnvironment: DefaultSolidifyServerEnvironment, ssrCache: SsrCache, routeName: string, id: string): Promise<boolean> {
    const cacheName = ServerCacheHelper.generateCacheName(routeName, id);
    if (ssrCache.has(cacheName)) {
      try {
        await ssrCache.clearResource(cacheName);
        logger.info(`Cache SSR cleaned for route '${routeName}' and id '${id}'`);
        return true;
      } catch (err) {
        logger.error(`Unable to clean SSR cache for route '${routeName}' and id '${id}'. See error ${err}`);
        return false;
      }
    }
    logger.error(`Unable to find SSR cache to clean for route '${routeName}' and id '${id}'`);
    return false;
  };

  static generateCacheKey(cacheName: string, cacheVersion: string): string {
    return `${cacheName}_${cacheVersion}`;
  };

  static generateCacheName(name: string, resId: string | null): string {
    let cacheName = name;
    if (isNotNullNorUndefinedNorWhiteString(resId)) {
      resId = StringUtil.replaceForbiddenFileNameChar(resId, "");
      cacheName = `${cacheName}-${resId}`;
    }
    return cacheName;
  };

  static generateCacheVersion(themeMode: ThemeModeEnum, language: string | null): string {
    let cacheKey = `${themeMode}`;
    if (isNotNullNorUndefinedNorWhiteString(language)) {
      cacheKey = `${cacheKey}-${this._getLanguageWithoutCulture(language)}`;
    }
    return cacheKey;
  };

  private static _getLanguageWithoutCulture(languageWithCulture: string): string {
    return languageWithCulture.substring(0, 2);
  }
}
