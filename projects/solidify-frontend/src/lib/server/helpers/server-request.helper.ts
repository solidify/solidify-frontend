/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - server-request.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {CookiePartialEnum} from "../../core/enums/partial/cookie-partial.enum";
import {CookieHelper} from "../../core/helpers/cookie.helper";
import {isNotNullNorUndefinedNorWhiteString} from "../../core-resources/tools/is/is.tool";
import {UrlUtil} from "../../core/utils/url.util";
import {ThemeModeEnum} from "../enums/theme-mode.enum";
import {SsrRouteOption} from "../models/ssr-route-option.model";

export class ServerRequestHelper {
  static GZIP_EXTENSION: string = ".gz";

  static getRequestOption(ssrRoutes: SsrRouteOption[], requestUrl: string): SsrRouteOption {
    const url = UrlUtil.removeParametersFromUrl(requestUrl).toLowerCase();
    return ssrRoutes.find(r => {
      if (isNotNullNorUndefinedNorWhiteString(r.urlMatcher)) {
        return new RegExp(r.urlMatcher).test(url);
      }
      return r.path === url;
    });
  }

  static getRequestLanguage(): string {
    const languageHeader = window.navigator.language;
    const languageCookie = CookieHelper.getItem(CookiePartialEnum.language);
    const language = isNotNullNorUndefinedNorWhiteString(languageCookie) ? languageCookie : languageHeader;
    if (isNotNullNorUndefinedNorWhiteString(language)) {
      return language;
    }
    return null;
  }

  static getRequestThemeMode(): ThemeModeEnum {
    const darkModeCookie = CookieHelper.getItem(CookiePartialEnum.darkMode);
    if (darkModeCookie === SOLIDIFY_CONSTANTS.STRING_TRUE) {
      return ThemeModeEnum.dark;
    }
    return ThemeModeEnum.light;
  }
}
