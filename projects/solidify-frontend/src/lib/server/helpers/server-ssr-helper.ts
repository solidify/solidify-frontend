/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - server-ssr-helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {APP_BASE_HREF} from "@angular/common";
import {CommonEngine} from "@angular/ssr";
import {Logger} from "winston";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {LanguagePartialEnum} from "../../core/enums/partial/language-partial.enum";
import {DefaultSolidifyEnvironment} from "../../core/environments/environment.solidify-defaults";
import {InMemoryStorage} from "../../core/helpers/in-memory.storage";
import {ENVIRONMENT} from "../../core/injection-tokens/environment.injection-token";
import {
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefinedOrWhiteString,
} from "../../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../../core-resources/types/extend-enum.type";
import {SsrUtil} from "../../core-resources/utils/ssr.util";
import {DefaultSolidifyServerEnvironment} from "../environments/server-environment.defaults.model";
import {NodeToolsModel} from "../models/node-tools.model";
import {SsrRequestInfo} from "../models/ssr-request-info.model";

export class ServerSsrHelper {
  static async ssrRender(commonEngine: CommonEngine,
                         nodeTools: NodeToolsModel,
                         logger: Logger,
                         serverEnvironment: DefaultSolidifyServerEnvironment,
                         environment: DefaultSolidifyEnvironment,
                         fromContext: string,
                         url: string): Promise<string | undefined> {
    try {
      const html = await commonEngine.render({
        // bootstrap: A method which returns an NgModule or a promise which resolves to an ApplicationRef. (already defined in engine constructor)
        bootstrap: nodeTools.serverModule as any,
        // document: The initial DOM to use for bootstrapping the server application.
        document: null,
        // documentFilePath: File path of the initial DOM to use to bootstrap the server application.
        documentFilePath: nodeTools.join(serverEnvironment.browserLocation, "index.html"),
        // url: The url of the page to render.
        url: url,
        // publicPath: Base path for browser files and assets.
        publicPath: null,
        // providers: An array of platform level providers for the current request.
        providers: [
          {provide: APP_BASE_HREF, useValue: environment.baseHref},
          {provide: ENVIRONMENT, useValue: environment},
        ],
        // inlineCriticalCss: Whether to reduce render blocking requests by inlining critical CSS.
        inlineCriticalCss: true,
      });

      const isOnline = html.substring(0, 100).includes(`<meta name="${SOLIDIFY_CONSTANTS.ONLINE}" content="${SOLIDIFY_CONSTANTS.STRING_TRUE}">`);
      if (isOnline) {
        logger.info(`${fromContext}: Render HTML successfully generated for url '${url}'`);
        return html;
      }
      logger.error(`${fromContext}: Render HTML successfully generated but the backend is offline for url '${url}'`);
      return undefined;

    } catch (err) {
      logger.error(`${fromContext}: Render HTML fail generated for url '${url}'`, err);
      return undefined;
    }
  }

  static getIndexHtmlContent(nodeTools: NodeToolsModel, serverEnvironment: DefaultSolidifyServerEnvironment): string {
    const indexHtmlPath = nodeTools.join(serverEnvironment.browserLocation, "index.html");
    return nodeTools.fs.readFileSync(indexHtmlPath).toString();
  };

  static configureBrowserGlobalVariable(nodeTools: NodeToolsModel, serverEnvironment: DefaultSolidifyServerEnvironment, indexHtmlContent: string, ssrRequestInfo: SsrRequestInfo): void {
    this._initBrowserGlobalVariable(nodeTools, serverEnvironment, indexHtmlContent);
    this._setRuntimeValueOnGlobalVariable(nodeTools, ssrRequestInfo);
  }

  private static _initBrowserGlobalVariable(nodeTools: NodeToolsModel, serverEnvironment: DefaultSolidifyServerEnvironment, indexHtmlContent: string): void {
    const listSolidifyStateRegistered = SsrUtil?.window?.[SsrUtil.LIST_SOLIDIFY_STATE_REGISTERED] ?? [];
    // Remove window reference between each render to avoid memory leak
    delete nodeTools.global[SsrUtil.WINDOW_PROPERTY_KEY];
    delete SsrUtil[SsrUtil.WINDOW_PROPERTY_KEY];
    const window: Window & any = nodeTools.domino.createWindow(indexHtmlContent);
    window.localStorage = new InMemoryStorage();
    window.sessionStorage = new InMemoryStorage();
    window.navigator.language = serverEnvironment.defaultLanguageCulture;
    window.navigator.onLine = true;
    window.encodeURIComponent = (v: string) => v;
    window.decodeURIComponent = (v: string) => v;
    window[SsrUtil.SSR_PROPERTY_KEY] = true;
    window[SsrUtil.LIST_SOLIDIFY_STATE_REGISTERED] = listSolidifyStateRegistered;
    nodeTools.global[SsrUtil.WINDOW_PROPERTY_KEY] = window;
    SsrUtil[SsrUtil.WINDOW_PROPERTY_KEY] = window;
    window[SsrUtil.PORT_PROPERTY_KEY] = serverEnvironment.port;
  };

  private static _setRuntimeValueOnGlobalVariable(nodeTools: NodeToolsModel, ssrRequestInfo: SsrRequestInfo): void {
    window.location.protocol = ssrRequestInfo.protocol + ":";
    const hostSplit = ssrRequestInfo.host.split(":");
    window.location.hostname = hostSplit[0];
    if (hostSplit.length > 1) {
      window.location.port = hostSplit[1];
    }
    window.location.host = `${window.location.hostname}:${window.location.port}`;
    const indexOfQueryParam = ssrRequestInfo.url.indexOf("?");
    if (indexOfQueryParam >= 0) {
      window.location.pathname = ssrRequestInfo.url.substring(0, indexOfQueryParam);
      window.location.search = ssrRequestInfo.url.substring(indexOfQueryParam);
    } else {
      window.location.pathname = ssrRequestInfo.url;
      window.location.search = "";
    }
    window.location.href = window.location.origin + window.location.pathname;

    if (isNotNullNorUndefinedNorWhiteString(ssrRequestInfo.acceptLanguage)) {
      const fullLanguage = ssrRequestInfo.acceptLanguage.split(SOLIDIFY_CONSTANTS.SEMICOLON)[0];
      const language = fullLanguage.split(SOLIDIFY_CONSTANTS.COMMA)[0];
      (window.navigator as any)["language"] = language;
    }

    window.document[SsrUtil.COOKIE_SSR_PROPERTY_KEY] = ssrRequestInfo.cookie;
  };

  static getSsrUrl(): string {
    return window.location.origin + window.location.pathname;
  }

  static computeValidAcceptLanguage(appLanguages: ExtendEnum<LanguagePartialEnum>[],
                                    defaultLanguageCulture: string,
                                    requestAcceptLanguage: string): string {
    if (isNullOrUndefinedOrWhiteString(requestAcceptLanguage) || requestAcceptLanguage.length < 2) {
      return defaultLanguageCulture;
    }
    const startLanguage = requestAcceptLanguage.substring(0, 2);
    if (appLanguages.findIndex((l: string) => l.startsWith(startLanguage)) >= 0) {
      return requestAcceptLanguage;
    }
    return defaultLanguageCulture;
  }
}
