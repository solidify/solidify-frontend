/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - active-mq.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Client} from "@stomp/stompjs";
import {Logger} from "winston";
import {DefaultSolidifyServerEnvironment} from "../environments/server-environment.defaults.model";

export class ActiveMqHelper {
  static configureActiveMQ(logger: Logger, serverEnvironment: DefaultSolidifyServerEnvironment, fromContext: string, callback: (client: Client) => void): void {
    const client = new Client({
      onStompError: message => {
        if (serverEnvironment.logActiveMQDebug) {
          logger.error(`${fromContext} ActiveMQ stomp error : ${message}`);
        }
      },
      debug: message => {
        if (serverEnvironment.logActiveMQDebug && ![
          "Received data",
          "<<< PONG",
          ">>> PING",
        ].includes(message)) {
          logger.verbose(`${fromContext} ActiveMQ: ${message}`);
        }
      },
      connectHeaders: {
        login: serverEnvironment.activeMQBrokerLogin,
        passcode: serverEnvironment.activeMQBrokerPassword,
      },
      brokerURL: serverEnvironment.activeMQBrokerUrl,
      reconnectDelay: serverEnvironment.activeMQReconnectDelay,
      heartbeatIncoming: serverEnvironment.activeMQHeartbeatIncoming,
      heartbeatOutgoing: serverEnvironment.activeMQHeartbeatOutgoing,
      onConnect: () => callback(client),
    });
    client.activate();
  }
}
