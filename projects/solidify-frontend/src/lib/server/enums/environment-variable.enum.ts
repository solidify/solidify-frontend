/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - environment-variable.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


export type EnvironmentVariableEnum =
  "BROWSER_LOCATION"
  | "SSR_WORKER_LOCATION"
  | "PORT"
  | "LOG_LEVEL"
  | "LOG_SSR_BROWSER_EXECUTION"
  | "LOG_HTTP_TIME";
export const EnvironmentVariableEnum = {
  BROWSER_LOCATION: "BROWSER_LOCATION" as EnvironmentVariableEnum,
  SSR_WORKER_LOCATION: "SSR_WORKER_LOCATION" as EnvironmentVariableEnum,
  PORT: "PORT" as EnvironmentVariableEnum,
  LOG_LEVEL: "LOG_LEVEL" as EnvironmentVariableEnum,
  LOG_SSR_BROWSER_EXECUTION: "LOG_SSR_BROWSER_EXECUTION" as EnvironmentVariableEnum,
  LOG_HTTP_TIME: "LOG_HTTP_TIME" as EnvironmentVariableEnum,
};
