/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - server.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {EventEmitter} from "@angular/core";
import {CommonEngine} from "@angular/ssr";
/* eslint-disable no-console */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
import express from "express";
import {SendFileOptions} from "express-serve-static-core";
import http from "http";
import {tap} from "rxjs/operators";
import {Logger} from "winston";
import {SOLIDIFY_CONSTANTS} from "../core-resources/constants";
import {DefaultSolidifyEnvironment} from "../core/environments/environment.solidify-defaults";
import {
  isFalse,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrEmptyArray,
  isNullOrUndefinedOrWhiteString,
  isTrue,
} from "../core-resources/tools/is/is.tool";
import {MappingObjectUtil} from "../core-resources/utils/mapping-object.util";
import {EnvironmentVariableEnum} from "./enums/environment-variable.enum";
import {ServerLogLevelEnum} from "./enums/server-log-level.enum";
import {ThemeModeEnum} from "./enums/theme-mode.enum";
import {DefaultSolidifyServerEnvironment} from "./environments/server-environment.defaults.model";
import {ActiveMqHelper} from "./helpers/active-mq.helper";
import {ServerCacheHelper} from "./helpers/server-cache.helper";
import {ServerLoggerHelper} from "./helpers/server-logger.helper";
import {ServerRequestHelper} from "./helpers/server-request.helper";
import {ServerSsrHelper} from "./helpers/server-ssr-helper";
import {CacheMessage} from "./models/cache-message.model";
import {CacheRequest} from "./models/cache.request";
import {NodeToolsModel} from "./models/node-tools.model";
import {SsrRequestInfo} from "./models/ssr-request-info.model";
import {SsrRouteOption} from "./models/ssr-route-option.model";
import {SsrWorkerMessage} from "./models/ssr-worker-message.model";
import {WorkerInitData} from "./models/worker-init.data";
import {SsrCache} from "./services/server-cache.service";

const SERVER_ENVIRONMENT_RUNTIME_FILENAME: string = "server-environment.runtime.json";

export class SsrServer {
  private readonly _SSR_MAIN_THREAD: string = "ssr-main-thread";
  private readonly _logger: Logger;
  private readonly _nodeTools: NodeToolsModel;
  private readonly _environment: DefaultSolidifyEnvironment;
  private readonly _serverEnvironment: DefaultSolidifyServerEnvironment;
  private readonly _serverLocation: string;
  private readonly _ssrCache: SsrCache;
  private readonly _activeMQSsrCachePostingEE: EventEmitter<SsrWorkerMessage> = new EventEmitter<SsrWorkerMessage>();
  private _host: string;
  private _protocol: string;

  constructor(nodeTools: NodeToolsModel, environment: DefaultSolidifyEnvironment, serverEnvironment: DefaultSolidifyServerEnvironment) {
    this._nodeTools = nodeTools;
    this._serverLocation = nodeTools.process["mainModule"].path;
    this._environment = environment;
    this._serverEnvironment = serverEnvironment;
    this._completeServerEnvironment();
    this._logger = ServerLoggerHelper.initLogger(nodeTools, serverEnvironment, this._serverLocation, this._SSR_MAIN_THREAD);
    this._ssrCache = new SsrCache(this._logger, nodeTools, serverEnvironment, this._serverLocation);
    Object.assign(this._nodeTools.global, {WebSocket: this._nodeTools.webSocket});
    this._configureActiveMQ();

    for (let i = 1; i <= this._serverEnvironment.workerNumber; i++) {
      this._initWorker(i);
    }
  }

  run(): http.Server {
    // Start up the Node server
    return this._app()
      .listen(this._serverEnvironment.port, () => {
        // eslint-disable-next-line no-console
        this._logger.info(`SSR Node Express server listening on http://localhost:${this._serverEnvironment.port}.`);
        this._logger.info(`Environment config used: ${JSON.stringify(this._serverEnvironment, null, 2)}`);
      });
  }

  private _initWorker(workerId: number): void {
    const worker = new this._nodeTools.workerThreads.Worker(
      this._nodeTools.join(this._serverEnvironment.ssrWorkerLocation, "main.js"), {
        resourceLimits: {
          maxOldGenerationSizeMb: this._serverEnvironment.workerResourceLimitsMaxOldGenerationSizeMb,
          stackSizeMb: this._serverEnvironment.workerResourceLimitsMaxOldGenerationSizeMb,
          codeRangeSizeMb: this._serverEnvironment.workerResourceLimitsMaxOldGenerationSizeMb,
          maxYoungGenerationSizeMb: this._serverEnvironment.workerResourceLimitsMaxOldGenerationSizeMb,
        },
        execArgv: this._serverEnvironment.workerExecArgv,
        argv: this._serverEnvironment.workerArgv,
        workerData: {
          serverLocation: this._serverLocation,
          serverEnvironment: this._serverEnvironment,
          workerId: workerId,
        } as WorkerInitData,
      },
    );
    worker.on("online", () => {
      this._logger.info(`server.ts: Worker ${workerId} is online`);
    });
    worker.on("message", (cacheKey: string) => {
      this._logger.info(`server.ts: Worker ${workerId} set in cache result for ${cacheKey}`);
    });
    worker.on("error", (err) => {
      this._logger.error(`server.ts: Worker ${workerId} fall in error '${err.message}'`);
    });
    worker.on("exit", () => {
      this._logger.info(`server.ts: Worker ${workerId} exit`);
      if (this._serverEnvironment.workerRestartOnExit) {
        this._logger.info(`server.ts: Worker ${workerId} restart`);
        this._initWorker(workerId);
      }
    });
  }

  private _allowSsrCache(cacheRequest: CacheRequest | undefined): boolean {
    return isNotNullNorUndefined(cacheRequest)
      &&
      (
        isNullOrUndefined(this._serverEnvironment.generalCacheForbidden)
        || isFalse(this._serverEnvironment.generalCacheForbidden)
      )
      &&
      (
        isNullOrUndefined(cacheRequest.requestOption.cacheForbidden)
        || isFalse(cacheRequest.requestOption.cacheForbidden)
      );
  }

  private _app(): express.Express {
    // Allow to not display warning : (node:2743305) [DEP0005] DeprecationWarning: Buffer() is deprecated due to security and usability issues. Please use the Buffer.alloc(), Buffer.allocUnsafe(), or Buffer.from() methods instead.
    Object.defineProperties(this._nodeTools.process, {
      noDeprecation: {
        value: true,
        writable: false,
      },
    });

    const server = this._nodeTools.express();

    const commonEngine = new CommonEngine(this._nodeTools.serverModule as any);
    const indexHtmlContent = ServerSsrHelper.getIndexHtmlContent(this._nodeTools, this._serverEnvironment);
    this._environment.showDebugInformation = this._serverEnvironment.logSsrBrowserExecution;

    if (!this._serverEnvironment.production) {
      const proxyConfig = this._nodeTools.proxyConfLocal.obj(undefined);
      Object.keys(proxyConfig).forEach(key => {
        server.use(key, this._nodeTools.createProxyMiddleware(proxyConfig[key]));
      });
    }

    server.use(this._nodeTools.express.json());

    if (isNonEmptyArray(this._serverEnvironment.staticRoutes)) {
      server.get(this._serverEnvironment.staticRoutes, async (req, res) => {
        this.defineHostAndProtocol(req);
        this._logger.verbose("server.ts: serve static file : " + req.url);
        let url = req.url;
        const indexOfQueryParam = url.indexOf("?");
        if (indexOfQueryParam !== -1) {
          url = url.substring(0, indexOfQueryParam);
          if (!this._serverEnvironment.production) {
            this._logger.verbose("server.ts: clean url : " + req.url + " => " + url);
          }
        }
        const start = Date.now();
        return res.sendFile(this._serverEnvironment.browserLocation + "/" + url, (err: Error) => {
          if (isNullOrUndefined(err)) {
            const end = Date.now();
            if (this._serverEnvironment.logHttpTime) {
              this._logger.verbose(`server.ts: execution time for static file '${req.url}' : ${end - start} ms`);
            }
          } else if (err?.message?.startsWith("ENOENT: no such file or directory")) {
            this._logger.verbose(`server.ts: static file '${req.url}' not found`);
            res.status(404).send("<h1>404! Page not found</h1>");
          }
        });
      });
    }

    // All regular routes use the Universal engine
    if (isNonEmptyArray(this._serverEnvironment.ssrRoutes)) {
      server.get(this._serverEnvironment.ssrRoutes.map(r => r.path), async (req, res, next) => {
        this.defineHostAndProtocol(req);
        const ssrRequestInfo: SsrRequestInfo = {
          url: req.url,
          paramId: req.params?.id,
          protocol: this._protocol,
          host: this._host,
          acceptLanguage: ServerSsrHelper.computeValidAcceptLanguage(this._environment.appLanguages, this._serverEnvironment.defaultLanguageCulture, req.header("Accept-Language")),
          cookie: req.header("cookie"),
        };
        ServerSsrHelper.configureBrowserGlobalVariable(this._nodeTools, this._serverEnvironment, indexHtmlContent, ssrRequestInfo);

        const url = ServerSsrHelper.getSsrUrl();

        const cacheRequest = ServerCacheHelper.generateCacheRequest(this._logger, this._serverEnvironment, ssrRequestInfo);
        const isClientAcceptGzip = this._isClientAcceptGzip(req);
        const isAllowSsrCache = this._allowSsrCache(cacheRequest);

        if (isAllowSsrCache) {
          const cacheHtmlPath = await this._ssrCache.getPath(cacheRequest, isClientAcceptGzip);
          const isCacheFound = isNotNullNorUndefined(cacheHtmlPath);
          if (isCacheFound) {
            const isGzipVersionMissing = isClientAcceptGzip && !cacheHtmlPath.endsWith(ServerRequestHelper.GZIP_EXTENSION);
            if (isGzipVersionMissing) {
              this._logger.info(`server.ts: the gzip version for url ${url} is missing`);
              this._sendMessageToGenerateCache(cacheRequest.cacheKey, ssrRequestInfo);
            }

            const isValidCache = await this._ssrCache.isValidCache(cacheRequest, cacheHtmlPath);
            this._logger.info(`server.ts: return cached result for url : ${url} with key ${cacheRequest.cacheKey}`);
            return res.status(200).sendFile(cacheHtmlPath, this._generateGzipHeaderIfNeeded(isClientAcceptGzip, cacheHtmlPath), () => {
              if (!isValidCache) {
                this._logger.info(`server.ts: regenerate cache for expired result : ${url} with key ${cacheRequest.cacheKey}`);
                this._sendMessageToGenerateCache(cacheRequest.cacheKey, ssrRequestInfo, true);
              }
            });
          }

          const isAsynchronousWorkerAllowed = isNullOrUndefined(cacheRequest.requestOption.forceSynchronousRendering) || isFalse(cacheRequest.requestOption.forceSynchronousRendering);
          if (isAsynchronousWorkerAllowed) {
            this._sendMessageToGenerateCache(cacheRequest.cacheKey, ssrRequestInfo);
            this._logger.info("server.ts: request return default index.html file for url during worker process : " + url);
            return this.returnDefaultIndex(res, isClientAcceptGzip);
          }
        }

        this._logger.info("server.ts: run ssr computed page for url : " + url);
        const html = await ServerSsrHelper.ssrRender(commonEngine, this._nodeTools, this._logger, this._serverEnvironment, this._environment, "MainThread", url);
        if (isNotNullNorUndefinedNorWhiteString(html)) {
          if (isAllowSsrCache) {
            this._setInCache(cacheRequest, html, url);
          }
          res.status(200).send(html);
          return;
        }
        return this.returnDefaultIndex(res, isClientAcceptGzip);
      });
    }

    const router = this._nodeTools.express.Router();

    server.use("/clean-ssr-cache", this._nodeTools.basicAuth({
      users: {[this._serverEnvironment.cleanCacheEndpointsBasicAuthLogin]: this._serverEnvironment.cleanCacheEndpointsBasicAuthPassword},
    }), router);

    server.post(["/clean-ssr-cache/all"], async (req, res) => {
      this.defineHostAndProtocol(req);
      try {
        await this._ssrCache.clearAll();
        res.status(200).send("Cache SSR cleaned");
      } catch (e) {
        res.status(400).send("Unable to clean SSR cache");
      }
    });

    server.post([`/clean-ssr-cache/:routeName`], async (req, res) => {
      this.defineHostAndProtocol(req);
      const routeName = req.params.routeName;
      const listId = req.body as string[];
      if (isNullOrUndefinedOrEmptyArray(listId)) {
        return ServerCacheHelper.cleanCacheByResourceId(this._logger, this._serverEnvironment, this._ssrCache, routeName, undefined).then(result => {
          if (result) {
            res.status(200).send(`Cache SSR cleaned for route '${routeName}'`);
          } else {
            res.status(400).send(`Unable to clean SSR cache for route '${routeName}'`);
          }
        }).catch(() => {
          res.status(400).send(`Unable to clean SSR cache for route '${routeName}'`);
        });
      }

      return Promise.all(listId.map(id => ServerCacheHelper.cleanCacheByResourceId(this._logger, this._serverEnvironment, this._ssrCache, routeName, id)))
        .then(() => {
          res.status(200).send(`Cache SSR cleaned for route '${routeName}' and id list [${listId.join(", ")}]`);
        }).catch(err => {
          res.status(400).send("Unable to clean SSR cache.");
        });
    });

    server.post([`/clean-ssr-cache/:routeName/:id`], async (req, res) => {
      this.defineHostAndProtocol(req);
      const routeName = req.params.routeName;
      const id = req.params.id;
      ServerCacheHelper.cleanCacheByResourceId(this._logger, this._serverEnvironment, this._ssrCache, routeName, id).then(result => {
        if (result) {
          res.status(200).send(`Cache SSR cleaned for route '${routeName}' and id '${id}'`);
        } else {
          res.status(400).send(`Unable to clean SSR cache for route '${routeName}' and id '${id}'`);
        }
      }).catch(() => {
        res.status(400).send(`Unable to clean SSR cache for route '${routeName}' and id '${id}'`);
      });
    });

    server.post([`/simulate-cache-evict/:routeName/:id`], async (req, res) => {
      this.defineHostAndProtocol(req);
      const routeName = req.params.routeName;
      const id = req.params.id;
      this._cleanCacheEntryAndRegenerateCache(routeName, {
        resId: id,
      });
      res.status(200).send(`Simulate cache evict for route '${routeName}' and id '${id}'`);
    });

    server.get("*", (req, res) => {
      this.defineHostAndProtocol(req);
      this._logger.info("server.ts: request return default index.html file for url : " + req.url);
      return this.returnDefaultIndex(res, this._isClientAcceptGzip(req));
    });

    return server;
  }

  private _sendMessageToGenerateCache(cacheKey: string, ssrRequestInfo: SsrRequestInfo, forceRefreshCache: boolean = false, timeout?: number): void {
    if (isNullOrUndefined(timeout) || timeout === 0) {
      this._sendMessageToGenerateCacheImmediately(cacheKey, ssrRequestInfo, forceRefreshCache);
      return;
    }
    this._logger.info(`server.ts: Wait ${timeout} ms that indexation is finished before push an entry in cache queue for key ${cacheKey}`);
    setTimeout(() => {
      this._sendMessageToGenerateCacheImmediately(cacheKey, ssrRequestInfo, forceRefreshCache);
    }, timeout);
  }

  private _sendMessageToGenerateCacheImmediately(cacheKey: string, ssrRequestInfo: SsrRequestInfo, forceRefreshCache: boolean): void {
    this._logger.info(`server.ts: Push an entry in cache queue for key ${cacheKey}`);
    this._activeMQSsrCachePostingEE.emit({
      ssrRequestInfo: ssrRequestInfo,
      forceRefreshCache: forceRefreshCache,
    });
  }

  private _isClientAcceptGzip(req: Request | any): boolean {
    return req.header("Accept-Encoding")?.includes("gzip");
  }

  private _generateGzipHeaderIfNeeded(isClientAcceptGzip: boolean, filePath: string): SendFileOptions {
    const sendCacheFileOptions: SendFileOptions = {
      headers: {},
    };
    if (isClientAcceptGzip && filePath.endsWith(ServerRequestHelper.GZIP_EXTENSION)) {
      sendCacheFileOptions.headers["Content-Encoding"] = "gzip";
      sendCacheFileOptions.headers["Content-Type"] = "text/html; charset=utf-8";
    }
    return sendCacheFileOptions;
  }

  private returnDefaultIndex(res: Response | any, isClientAcceptGzip: boolean): void {
    let indexFilePath = this._serverEnvironment.browserLocation + "/index.html";
    if (isClientAcceptGzip) {
      indexFilePath = indexFilePath + ServerRequestHelper.GZIP_EXTENSION;
    }
    return res.status(200).sendFile(indexFilePath, this._generateGzipHeaderIfNeeded(isClientAcceptGzip, indexFilePath));
  }

  private _setInCache(cacheRequest: CacheRequest, html: string, url: string): void {
    this._ssrCache.set(cacheRequest.cacheName, cacheRequest.cacheKey, html)
      .then(() => this._logger.info(`server.ts: set ssr computed page in cache for url '${url}'`))
      .catch(cacheErr => this._logger.error(`server.ts: unable to set ssr computed page in cache for url '${url}'. ${cacheErr}`));
  }

  private _completeServerEnvironment(): void {
    this._overrideServerEnvironmentWithRuntimeFile();
    this._overrideServerEnvironmentWithEnvVariables();

    if (isNullOrUndefinedOrWhiteString(this._serverEnvironment.browserLocation)) {
      if (this._serverEnvironment.production) {
        throw new Error(`Missing absolute path in 'browserLocation'. Please provide in '${SERVER_ENVIRONMENT_RUNTIME_FILENAME}' or with env variable '${EnvironmentVariableEnum.BROWSER_LOCATION}'`);
      }
      this._serverEnvironment.browserLocation = this._nodeTools.join(this._nodeTools.process.cwd(), "dist", this._serverEnvironment.projectName, "browser");
    }

    if (isNullOrUndefinedOrWhiteString(this._serverEnvironment.ssrWorkerLocation)) {
      if (this._serverEnvironment.production) {
        throw new Error(`Missing absolute path in 'ssrWorkerLocation'. Please provide in '${SERVER_ENVIRONMENT_RUNTIME_FILENAME}' or with env variable '${EnvironmentVariableEnum.SSR_WORKER_LOCATION}'`);
      }
      this._serverEnvironment.ssrWorkerLocation = this._nodeTools.join(this._nodeTools.process.cwd(), "dist", this._serverEnvironment.projectName, "ssr-worker");
    }
  }

  private _overrideServerEnvironmentWithRuntimeFile(): void {
    const pathServerEnvironmentRuntimeFile = this._nodeTools.join(this._serverLocation, SERVER_ENVIRONMENT_RUNTIME_FILENAME);
    if (this._nodeTools.fs.existsSync(pathServerEnvironmentRuntimeFile)) {
      try {
        const environmentRuntime = JSON.parse(this._nodeTools.fs.readFileSync(pathServerEnvironmentRuntimeFile).toString());
        Object.assign(this._serverEnvironment, environmentRuntime);
        this._serverEnvironment.ssrRoutes = [...this._serverEnvironment.ssrRoutes, ...this._serverEnvironment.additionalSsrRoutes];
        delete this._serverEnvironment.additionalSsrRoutes;
      } catch (e) {
        console.error(`Error when try to parse '${SERVER_ENVIRONMENT_RUNTIME_FILENAME}' file defined in ${this._serverLocation}.`, e);
      }
    } else {
      console.log(`There is no '${SERVER_ENVIRONMENT_RUNTIME_FILENAME}' file defined in ${this._serverLocation}.`);
    }
  }

  private _overrideServerEnvironmentWithEnvVariables(): void {
    const envPort = this._nodeTools.process.env[EnvironmentVariableEnum.PORT]; // Port to run server
    if (isNotNullNorUndefinedNorWhiteString(envPort)) {
      this._serverEnvironment.port = +envPort;
    }
    const envLogLevel = this._nodeTools.process.env[EnvironmentVariableEnum.LOG_LEVEL]; // Log level
    if (isNotNullNorUndefinedNorWhiteString(envLogLevel)) {
      this._serverEnvironment.logLevel = envLogLevel as ServerLogLevelEnum;
    }
    const envLogSsrBrowserExecution = this._nodeTools.process.env[EnvironmentVariableEnum.LOG_SSR_BROWSER_EXECUTION]; // Log browser info when compute ssr
    if (isNotNullNorUndefinedNorWhiteString(envLogSsrBrowserExecution)) {
      this._serverEnvironment.logSsrBrowserExecution = !!envLogSsrBrowserExecution;
    }
    const envLogHttpTime = this._nodeTools.process.env[EnvironmentVariableEnum.LOG_HTTP_TIME]; // Log requests execution times
    if (isNotNullNorUndefinedNorWhiteString(envLogHttpTime)) {
      this._serverEnvironment.logHttpTime = !!envLogHttpTime;
    }
    const envBrowserLocation = this._nodeTools.process.env[EnvironmentVariableEnum.BROWSER_LOCATION]; // Absolute path to browser folder (that contain index.html)
    if (isNotNullNorUndefinedNorWhiteString(envBrowserLocation)) {
      this._serverEnvironment.browserLocation = envBrowserLocation;
    }
    const envSsrWorkerLocation = this._nodeTools.process.env[EnvironmentVariableEnum.SSR_WORKER_LOCATION]; // Absolute path to ssr worker location (that contain main.js)
    if (isNotNullNorUndefinedNorWhiteString(envSsrWorkerLocation)) {
      this._serverEnvironment.ssrWorkerLocation = envSsrWorkerLocation;
    }
  }

  private _configureActiveMQ(): void {
    ActiveMqHelper.configureActiveMQ(this._logger, this._serverEnvironment, "main", client => {
      // Used to communicate with Worker that render page and generate cache
      this._activeMQSsrCachePostingEE.asObservable().pipe(
        tap(ssrRequestInfo => {
          client.publish({
            destination: `/queue/${this._serverEnvironment.activeMQEnv}.ssr-cache`,
            body: JSON.stringify(ssrRequestInfo),
          });
        }),
      ).subscribe();

      if (this._serverEnvironment.isActiveMQCacheClean) {
        MappingObjectUtil.forEach(this._serverEnvironment.activeMQResourceCacheEvictMap, (topicName: string, resource: string) => {
          // Used to communicate with backend to force refresh cache
          client.subscribe(`/topic/${this._serverEnvironment.activeMQEnv}.${topicName}`, message => {
            const jsonObject = message.headers["jsonObject"];
            if (isNullOrUndefined(jsonObject)) {
              return;
            }
            const cacheMessage = JSON.parse(jsonObject) as CacheMessage;
            this._logger.info(`Received message from topic: ${message.headers["destination"]} to remove cache on archive id : ${cacheMessage.resId}`);
            this._cleanCacheEntryAndRegenerateCache(resource, cacheMessage);
          });
        });
      }
    });
  }

  private _cleanCacheEntryAndRegenerateCache(resource: string, cacheMessage: CacheMessage): void {
    const listMatchingSsrRoute = this._serverEnvironment.ssrRoutes.filter(s => {
      let ssrRouteMatch = s.resource === resource;
      if (isNotNullNorUndefinedNorWhiteString(s.cacheEvictionMessageCustomAttributeForMatchingRoute)) {
        const customAttribute: string = cacheMessage[s.cacheEvictionMessageCustomAttributeForMatchingRoute];
        if (isNotNullNorUndefinedNorWhiteString(customAttribute)
          && isNotNullNorUndefinedNorWhiteString(s.cacheEvictionMessageRouteMatchingRegExp)) {
          ssrRouteMatch = new RegExp(s.cacheEvictionMessageRouteMatchingRegExp).test(customAttribute);
        }
      }
      return ssrRouteMatch;
    });
    if (listMatchingSsrRoute) {
      this._logger.warn(`More than one SSR route (${listMatchingSsrRoute.map(s => s.name)
        .join(", ")}) match with cache message ${JSON.stringify(cacheMessage)}`);
    }
    listMatchingSsrRoute.forEach(matchingSsrRoute => {
      const routeName = matchingSsrRoute.name;
      let ids: string[];
      if (isTrue(matchingSsrRoute.cacheEvictionMessageIsMultiResources)) {
        ids = [...this._getListResourceIdInCacheMessage(matchingSsrRoute, cacheMessage)];
      } else {
        ids = [this._getResourceIdInCacheMessage(matchingSsrRoute, cacheMessage)];
      }
      ids.forEach(id => {
        ServerCacheHelper.cleanCacheByResourceId(this._logger, this._serverEnvironment, this._ssrCache, routeName, id).then(result => {
          if (result) {
            this._logger.info(`Cache SSR cleaned for route ${routeName} and id ${id}`);
          } else {
            this._logger.error(`Unable to clean SSR cache for route ${routeName} and id ${id}`);
          }
          this._sendInternalCacheRegenerationMessage(matchingSsrRoute, cacheMessage, id);
        }).catch(() => {
          this._logger.error(`Unable to clean SSR cache for route ${routeName} and id ${id}`);
          this._sendInternalCacheRegenerationMessage(matchingSsrRoute, cacheMessage, id);
        });
      });
    });
  }

  private _getResourceIdInCacheMessage(matchingSsrRoute: SsrRouteOption, cacheMessage: CacheMessage): string {
    let id = cacheMessage.resId;
    if (isNotNullNorUndefinedNorWhiteString(matchingSsrRoute?.cacheEvictionMessageCustomResourceId)) {
      id = cacheMessage[matchingSsrRoute.cacheEvictionMessageCustomResourceId];
    }
    return id;
  }

  private _getListResourceIdInCacheMessage(matchingSsrRoute: SsrRouteOption, cacheMessage: CacheMessage): string[] {
    if (isNotNullNorUndefinedNorWhiteString(matchingSsrRoute?.cacheEvictionMessageCustomListResourceId)) {
      return cacheMessage[matchingSsrRoute.cacheEvictionMessageCustomListResourceId];
    }
    throw Error(`'cacheEvictionMessageCustomListResourceId' should be defined for route '${matchingSsrRoute.name}' because 'cacheEvictionMessageCustomListResourceId' is set to true`);
  }

  private _sendInternalCacheRegenerationMessage(matchingSsrRoute: SsrRouteOption, cacheMessage: CacheMessage, id: string): void {
    const routeName = matchingSsrRoute.name;
    if (isNotNullNorUndefinedNorWhiteString(this._host)) {
      let url = matchingSsrRoute.path;
      if (matchingSsrRoute.withId) {
        if (isNotNullNorUndefinedNorWhiteString(matchingSsrRoute.pathFromCacheEvict)) {
          url = matchingSsrRoute.pathFromCacheEvict;
        }
        const pathIdKey = isNotNullNorUndefinedNorWhiteString(matchingSsrRoute.pathIdKey) ? matchingSsrRoute.pathIdKey : SOLIDIFY_CONSTANTS.URL_PARAM_ID;
        url = url.replace(pathIdKey, id);
      }

      const ssrRequestInfo: SsrRequestInfo = {
        url: url,
        paramId: id,
        protocol: this._protocol,
        host: this._host,
        acceptLanguage: this._serverEnvironment.defaultLanguageCulture,
        cookie: "",
      };

      const cacheName = ServerCacheHelper.generateCacheName(routeName, id);
      const defaultLanguage = this._serverEnvironment.defaultLanguageCulture.substring(0, 2);
      const cacheVersion = ServerCacheHelper.generateCacheVersion(ThemeModeEnum.light, defaultLanguage);
      const cacheKey = ServerCacheHelper.generateCacheKey(cacheName, cacheVersion);
      const shouldRefreshCacheAfterDeletion = isNullOrUndefined(matchingSsrRoute.cacheEvictionMessageRefreshCacheAfterDeletion) || isTrue(matchingSsrRoute.cacheEvictionMessageRefreshCacheAfterDeletion);
      if (shouldRefreshCacheAfterDeletion) {
        const isDeleted = isNotNullNorUndefinedNorWhiteString(matchingSsrRoute.cacheEvictionMessageIsDeletedAttributeKey) && isTrue(cacheMessage[matchingSsrRoute.cacheEvictionMessageIsDeletedAttributeKey]);
        if (!isDeleted) {
          this._sendMessageToGenerateCache(cacheKey, ssrRequestInfo, false, matchingSsrRoute.timeoutAfterActiveMQMessageToRefreshCache);
        }
      }
    } else {
      this._logger.error(`Property host is missing at the moment, can't send message to generate cache for route ${routeName} and id ${id}`);
    }
  }

  private defineHostAndProtocol(req: any): void {
    this._host = req.get("host");
    this._protocol = req.protocol + ":";
  }
}
