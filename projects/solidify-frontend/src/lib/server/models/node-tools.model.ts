/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - node-tools.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {CommonEngine} from "@angular/ssr";
import * as Domino from "domino";
import express from "express";
import fs from "fs";
import * as fsp from "fs/promises";
import md5 from "md5";
import * as workerThreads from "node:worker_threads";
import * as winston from "winston";
import DailyRotateFile from "winston-daily-rotate-file";
import {WebSocket} from "ws";
import * as zlib from "zlib";
import {SolidifyFrontendAbstractAppServerModule} from "../solidify-frontend-abstract-app-server.module";

export interface NodeToolsModel {
  join: (...paths: string[]) => string;
  md5: typeof md5;
  fs: typeof fs;
  fsp: typeof fsp;
  zlib: typeof zlib;
  process: typeof process;
  domino: typeof Domino;
  global: typeof global;
  express: typeof express;
  basicAuth: any; // (users: { [username: string]: string }) => void;
  commonEngine: typeof CommonEngine;
  serverModule: SolidifyFrontendAbstractAppServerModule;
  proxyConfLocal: any;
  createProxyMiddleware: any;
  webSocket: typeof WebSocket;
  winston: typeof winston;
  winstonDailyRotateFile: typeof DailyRotateFile;
  workerThreads: typeof workerThreads;
  heapdump: any;
}
