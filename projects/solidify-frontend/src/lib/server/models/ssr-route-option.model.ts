/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - ssr-route-option.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

export interface SsrRouteOption {
  name: string;
  path: string;
  /**
   * Optional
   * Used to generation original path from cache message for specific case where 'path' value not work.
   * The value should contain the value defined in 'pathIdKey' value.
   * Only applicable if 'withId' is true
   * If not defined, use 'path' value
   */
  pathFromCacheEvict?: string;
  /**
   * The unique identifier for a path. Usually is ":id". Required if withId is true
   *
   * @type {string}
   */
  pathIdKey?: string;
  withId: boolean;
  resource?: string;
  /**
   * RegExp string that allow to find matching route (override 'path' setting when retrieve SSR route)
   */
  urlMatcher?: string;
  /**
   * RegExp string that allow to find id in request url
   * Used only if 'withId' is true
   * If not defined, id used is req.params.id
   */
  idMatcher?: string;
  forceSynchronousRendering?: boolean;
  cacheForbidden?: boolean;
  cacheEvictionTopic?: string;
  /**
   * Optional
   * Allow to define a custom attribute of cache message to retrieve the property to check with 'cacheEvictionMessageRouteMatchingRegExp' regexp to assign SSR route to cache message.
   * Need a valid regexp in 'cacheEvictionMessageRouteMatchingRegExp'
   */
  cacheEvictionMessageCustomAttributeForMatchingRoute?: string;
  /**
   * Optional
   * Regexp used to check if attribute of cache message match with the SSR route.
   * Needed if 'cacheEvictionMessageCustomAttributeForMatchingRoute' is defined
   */
  cacheEvictionMessageRouteMatchingRegExp?: string;
  /**
   * Allow to define a custom attribute of cache message to retrieve the id of the resource in the cache message received in activeMQ
   * If not define, it's the resId of cache message that is used
   * Ignored if 'cacheEvictionMessageIsMultiResources' is true
   */
  cacheEvictionMessageCustomResourceId?: string;

  /**
   * Allow to define a custom attribute of cache message to retrieve the list id of resources in the cache message received in activeMQ
   * Use only if 'cacheEvictionMessageIsMultiResources' is true
   */
  cacheEvictionMessageCustomListResourceId?: string;

  /**
   * Allow to define if we want to treat a list of id in cache message received in activeMQ
   * If true, you need to define `cacheEvictionMessageCustomListResourceId`
   * False if not defined
   * If false, the resId in cache message is used, or the 'cacheEvictionMessageCustomResourceId' is used if defined
   */
  cacheEvictionMessageIsMultiResources?: boolean;
  /**
   * Allow to ask refresh after cache deletion via activeMQ message.
   * True by default, if not defined
   */
  cacheEvictionMessageRefreshCacheAfterDeletion?: boolean;
  /**
   * Allow to define the name of boolean attribute in cache eviction message that inform if the resource is delete.
   * If the attribute value is true, the regeneration of cache is trigger after cache deletion via activeMQ message.
   * If false or if this property is not defined, the cache regeneration is triggered after cache deletion via activeMQ message.
   */
  cacheEvictionMessageIsDeletedAttributeKey?: string;
  /**
   * Time to Live of the cache
   */
  cacheTimeToLiveInMinutes?: number | undefined;
  /**
   * Allow to define a delay to wait after active MQ cache message to request a new generation of cache (to wait backend indexation)
   */
  timeoutAfterActiveMQMessageToRefreshCache?: number | undefined;
}
