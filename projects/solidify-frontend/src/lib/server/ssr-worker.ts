/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - ssr-worker.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {CommonEngine} from "@angular/ssr";
import {Logger} from "winston";
import {DefaultSolidifyEnvironment} from "../core/environments/environment.solidify-defaults";
import {
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
} from "../core-resources/tools/is/is.tool";
import {DefaultSolidifyServerEnvironment} from "./environments/server-environment.defaults.model";
import {ActiveMqHelper} from "./helpers/active-mq.helper";
import {ServerCacheHelper} from "./helpers/server-cache.helper";
import {ServerLoggerHelper} from "./helpers/server-logger.helper";
import {ServerRequestHelper} from "./helpers/server-request.helper";
import {ServerSsrHelper} from "./helpers/server-ssr-helper";
import {NodeToolsModel} from "./models/node-tools.model";
import {SsrRequestInfo} from "./models/ssr-request-info.model";
import {SsrWorkerMessage} from "./models/ssr-worker-message.model";
import {WorkerInitData} from "./models/worker-init.data";
import {SsrCache} from "./services/server-cache.service";

export class SsrWorker {
  private readonly _nodeTools: NodeToolsModel;
  private readonly _environment: DefaultSolidifyEnvironment;
  private _serverEnvironment: DefaultSolidifyServerEnvironment;
  private _serverLocation: string;
  private _workerId: number;
  private _logger: Logger;
  private _ssrCache: SsrCache;
  private _commonEngine: CommonEngine;
  private _indexHtmlContent: string;

  constructor(nodeTools: NodeToolsModel, environment: DefaultSolidifyEnvironment) {
    this._nodeTools = nodeTools;
    this._environment = environment;
    Object.assign(this._nodeTools.global, {WebSocket: this._nodeTools.webSocket});
  }

  async init(workerInitData: WorkerInitData): Promise<void> {
    this._serverEnvironment = workerInitData.serverEnvironment;
    this._serverLocation = workerInitData.serverLocation;
    this._workerId = workerInitData.workerId;
    this._logger = ServerLoggerHelper.initLogger(this._nodeTools, this._serverEnvironment, this._serverLocation, `ssr-worker-${this._workerId}-thread`);
    this._ssrCache = new SsrCache(this._logger, this._nodeTools, this._serverEnvironment, this._serverLocation);

    this._indexHtmlContent = ServerSsrHelper.getIndexHtmlContent(this._nodeTools, this._serverEnvironment);
    this._commonEngine = new CommonEngine(this._nodeTools.serverModule as any);

    ActiveMqHelper.configureActiveMQ(this._logger, this._serverEnvironment, `SsrWorker[${this._workerId}]`, client => {
      client.subscribe(`/queue/${this._serverEnvironment.activeMQEnv}.${this._serverEnvironment.activeMQSsrCacheQueueName}`, message => {
        this._logger.info(`SsrWorker[${this._workerId}]: Received new ActiveMQ message: ${message.body}`);
        try {
          const ssrWorkerMessage: SsrWorkerMessage = JSON.parse(message.body);
          this.render(ssrWorkerMessage.ssrRequestInfo, ssrWorkerMessage.forceRefreshCache).finally(() => {
            message.ack();
          });
        } catch (e) {
          this._logger.error(`SsrWorker[${this._workerId}]: unable to consume ActiveMQ message '${message.body}', ignore it`);
          message.ack();
        }
      }, {
        "activemq.prefetchSize": 1,
        ack: "client",
      } as any);
    });
  }

  async render(ssrRequestInfo: SsrRequestInfo, forceRefreshCache: boolean): Promise<void> {
    this._logger.debug(`SsrWorker[${this._workerId}]: SSR Worker start with workerData ${JSON.stringify(ssrRequestInfo)}`);

    ServerSsrHelper.configureBrowserGlobalVariable(this._nodeTools, this._serverEnvironment, this._indexHtmlContent, ssrRequestInfo);

    const cacheRequest = ServerCacheHelper.generateCacheRequest(this._logger, this._serverEnvironment, ssrRequestInfo);
    const cacheHtmlPath = await this._ssrCache.getPath(cacheRequest, true);
    const isCacheFound = isNotNullNorUndefined(cacheHtmlPath);
    if (isCacheFound) {
      if (forceRefreshCache) {
        this._logger.info(`SsrWorker[${this._workerId}]: cache already exist for url '${ssrRequestInfo.url}', regenerate it`);
      } else {
        const isGzipVersionMissing = !cacheHtmlPath.endsWith(ServerRequestHelper.GZIP_EXTENSION);
        if (!isGzipVersionMissing) {
          this._logger.info(`SsrWorker[${this._workerId}]: cache already exist for url '${ssrRequestInfo.url}', do nothing`);
          return;
        } else {
          this._logger.info(`SsrWorker[${this._workerId}]: cache already exist for url '${ssrRequestInfo.url}' but gzip version is missing, regenerate cache`);
        }
      }
    }

    const url = ServerSsrHelper.getSsrUrl();
    const html = await ServerSsrHelper.ssrRender(this._commonEngine, this._nodeTools, this._logger, this._serverEnvironment, this._environment, `SsrWorker[${this._workerId}]`, url);

    if (isNotNullNorUndefinedNorWhiteString(html)) {
      try {
        await this._ssrCache.set(cacheRequest.cacheName, cacheRequest.cacheKey, html);
        this._logger.info(`SsrWorker[${this._workerId}]: set ssr computed page in cache for url '${url}'`);
        this._nodeTools.workerThreads.parentPort.postMessage(cacheRequest.cacheKey);
      } catch (error) {
        this._logger.error(`SsrWorker[${this._workerId}]: unable to set ssr computed page in cache for url '${url}'. ${error}`);
      }
    }
  }
}
