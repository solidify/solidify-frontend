/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - server-cache.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Stats} from "node:fs";
import {Logger} from "winston";
import {ZlibOptions} from "zlib";
import {SOLIDIFY_CONSTANTS} from "../../core-resources/constants";
import {
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
} from "../../core-resources/tools/is/is.tool";
import {DefaultSolidifyServerEnvironment} from "../environments/server-environment.defaults.model";
import {ServerRequestHelper} from "../helpers/server-request.helper";
import {CacheRequest} from "../models/cache.request";
import {NodeToolsModel} from "../models/node-tools.model";

export class SsrCache {
  private readonly _DEFAULT_CACHE_FOLDER: string = ".ssr-cache";
  private readonly _GZIP_EXTENSION: string = ServerRequestHelper.GZIP_EXTENSION;
  private readonly _HTML_EXTENSION: string = ".html";
  private readonly _HTML_ENCODING: BufferEncoding = "utf8";
  private readonly _GZIP_ENCODING: BufferEncoding = "hex";
  private readonly _MD5_TRIM_REG_EXP: RegExp;

  private readonly _logger: Logger;
  private readonly _nodeTools: NodeToolsModel;
  private readonly _cacheLocation: string;

  // CACHE NAME
  // home
  // archive-58eb331b-a45f-4848-8c3c-1e613730d213

  // CACHE VERSION
  // fr-light,
  // fr-dark,

  constructor(logger: Logger, nodeTools: NodeToolsModel, serverEnvironment: DefaultSolidifyServerEnvironment, serverLocation: string) {
    this._logger = logger;
    this._nodeTools = nodeTools;
    this._cacheLocation = isNullOrUndefinedOrWhiteString(serverEnvironment.cacheLocation) ? nodeTools.join(serverLocation, this._DEFAULT_CACHE_FOLDER) : serverEnvironment.cacheLocation;
    this._createDirectoryIfNotExist();
    this._MD5_TRIM_REG_EXP = new RegExp(`.{1,${serverEnvironment.cachePathMd5TrimSize}}`, "g");
  }

  private _getAbsolutePath(...relativePath: string[]): string {
    return this._nodeTools.join(this._cacheLocation, ...relativePath);
  }

  private async _createDirectoryIfNotExist(...relativePath: string[]): Promise<void> {
    const absolutePath = this._getAbsolutePath(...relativePath);
    const exist = await this._fileExist(absolutePath);
    if (!exist) {
      await this._nodeTools.fsp.mkdir(absolutePath, {recursive: true});
    }
  }

  private async _deleteDirectoryIfExit(...relativePath: string[]): Promise<void> {
    const absolutePath = this._getAbsolutePath(...relativePath);
    return await this._deleteDirectoryIfExitAbsolute(absolutePath);
  }

  private async _deleteDirectoryIfExitAbsolute(absolutePath: string): Promise<void> {
    if (await this._fileExist(absolutePath)) {
      return this._deleteDirectoryAbsolute(absolutePath);
    }
    return Promise.resolve();
  }

  private _deleteDirectory(...relativePath: string[]): Promise<void> {
    const absolutePath = this._getAbsolutePath(...relativePath);
    return this._deleteDirectoryAbsolute(absolutePath);
  }

  private _deleteDirectoryAbsolute(absolutePath: string): Promise<void> {
    return this._nodeTools.fsp.rm(absolutePath, {recursive: true});
  }

  /**
   * Check if a cache folder exist for the requested resource
   *
   * @param cacheName resource and eventually id (ex: home | archive-58eb331b-a45f-4848-8c3c-1e613730d213)
   */
  has(cacheName: string): boolean {
    const md5Path = this._generateMd5RelativePath(cacheName).join(SOLIDIFY_CONSTANTS.SEPARATOR);
    const absolutePath = this._getAbsolutePath(md5Path);
    return this._nodeTools.fs.existsSync(absolutePath);
  }

  /**
   * Get cache file path if exist, return undefined if not
   *
   * @param cacheRequest
   * @param acceptGzip
   */
  getPath(cacheRequest: CacheRequest, acceptGzip: boolean): Promise<string | undefined> {
    const cacheName = cacheRequest.cacheName;
    const md5Path = this._generateMd5RelativePath(cacheName).join(SOLIDIFY_CONSTANTS.SEPARATOR);
    const path = this._nodeTools.join(md5Path, cacheRequest.cacheKey + this._HTML_EXTENSION);
    const absolutePath = this._getAbsolutePath(path);

    if (acceptGzip && this._nodeTools.fs.existsSync(absolutePath + this._GZIP_EXTENSION)) {
      return this._getAbsolutePathCache(absolutePath, true);
    }

    if (this._nodeTools.fs.existsSync(absolutePath)) {
      return this._getAbsolutePathCache(absolutePath, false);
    }

    return undefined;
  }

  async isValidCache(cacheRequest: CacheRequest, absolutePathUsed: string): Promise<boolean | undefined> {
    return await this._isValidCache(cacheRequest.requestOption.cacheTimeToLiveInMinutes, absolutePathUsed);
  }

  async invalidCache(cacheKey: string, absolutePathUsed: string): Promise<void> {
    let absolutePath = absolutePathUsed;
    let absolutePathGzip = absolutePathUsed;
    if (absolutePathUsed.endsWith(this._GZIP_EXTENSION)) {
      absolutePath = absolutePath.substring(0, absolutePath.length - this._GZIP_EXTENSION.length);
    } else {
      absolutePathGzip = absolutePath + this._GZIP_EXTENSION;
    }
    this._logger.info(`server-cache.service.ts: Remove expired cache for version ${cacheKey}`);
    await this._deleteDirectoryIfExitAbsolute(absolutePath);
    await this._deleteDirectoryIfExitAbsolute(absolutePathGzip);
  }

  private async _getAbsolutePathCache(absolutePath: string, returnCompressed: boolean): Promise<string | undefined> {
    const absolutePathGzip = absolutePath + this._GZIP_EXTENSION;
    return returnCompressed ? absolutePathGzip : absolutePath;
  }

  /**
   * Set in cache file system a new entry
   *
   * @param cacheName
   * @param cacheKey
   * @param value
   */
  async set(cacheName: string, cacheKey: string, value: string): Promise<void> {
    if (isNullOrUndefinedOrWhiteString(value)) {
      this._logger.verbose(`server-cache.service.ts: Value to write in cache is empty`);
      return;
    }
    const md5RelativePath = this._generateMd5RelativePath(cacheName).join(SOLIDIFY_CONSTANTS.SEPARATOR);
    await this._createDirectoryIfNotExist(md5RelativePath);

    const relativePath = this._nodeTools.join(md5RelativePath, cacheKey + this._HTML_EXTENSION);
    const absolutePath = this._getAbsolutePath(relativePath);
    await this._deleteDirectoryIfExit(relativePath);

    try {
      await this._nodeTools.fsp.appendFile(absolutePath, value, {encoding: this._HTML_ENCODING});

      if (await this._isCacheIntegrityValid(cacheKey, value, absolutePath, false)) {
        this._logger.verbose(`server-cache.service.ts: Successfully generate file for cache result ${cacheKey}`);

        const relativePathGzip = this._nodeTools.join(md5RelativePath, cacheKey + this._HTML_EXTENSION + this._GZIP_EXTENSION);
        const absolutePathGzip = this._getAbsolutePath(relativePathGzip);
        const valueGzip = await this._zlibPromised(value, {level: 9});
        await this._deleteDirectoryIfExit(relativePathGzip);

        try {
          await this._nodeTools.fsp.appendFile(absolutePathGzip, valueGzip, {encoding: this._GZIP_ENCODING});

          if (await this._isCacheIntegrityValid(cacheKey, valueGzip, absolutePathGzip, true)) {
            this._logger.verbose(`server-cache.service.ts: Successfully generate gzip file for cache result ${cacheKey}`);
          }
        } catch (e) {
          this._logger.error(`server-cache.service.ts: Unable to generate gzip file for cache result ${cacheKey}`, e);
        }
      }
    } catch (e) {
      this._logger.error(`server-cache.service.ts: Unable to generate gzip file for cache result ${cacheKey}`, e);
    }
  }

  private async _isCacheIntegrityValid(cacheKey: string, value: string, absolutePath: string, isGzip: boolean): Promise<boolean> {
    const isGzipLabel = isGzip ? "gzip" : "";
    const encoding = isGzip ? this._GZIP_ENCODING : this._HTML_ENCODING;
    const expectedMd5 = this._generateMd5(value, encoding);
    let cachedFileContent = undefined;
    try {
      cachedFileContent = this._nodeTools.fs.readFileSync(absolutePath, {encoding: isGzip ? this._GZIP_ENCODING : this._HTML_ENCODING}).toString();
    } catch (e) {
      this._logger.error(`server-cache.service.ts: Unable to read ${isGzipLabel} cache file ${cacheKey} on path ${absolutePath}`, e);
      throw new Error(e);
    }
    const actualGzipMd5 = this._generateMd5(cachedFileContent, encoding);
    if (actualGzipMd5 !== expectedMd5) {
      this._logger.error(`server-cache.service.ts: Invalid ${isGzipLabel} cache ${cacheKey}, expected md5 ${expectedMd5}, actual md5 ${actualGzipMd5}, destroy ${isGzipLabel} cache`);
      await this._deleteDirectoryIfExitAbsolute(absolutePath);
      return false;
    }
    return true;
  }

  /**
   * Clear ssr cache for only one resource (delete all versions)
   *
   * @param cacheName resource and eventually id (ex: home | archive-58eb331b-a45f-4848-8c3c-1e613730d213)
   */
  async clearResource(cacheName: string): Promise<any> {
    const relativePath = this._generateMd5RelativePath(cacheName);
    return await this._deleteDirectoryIfExit(...relativePath);
  }

  /**
   * Clear all ssr cache
   */
  async clearAll(): Promise<void> {
    return await this._deleteDirectoryIfExit();
  }

  private _generateMd5RelativePath(cacheName: string): string[] {
    return this._generateMd5(cacheName, this._HTML_ENCODING).match(this._MD5_TRIM_REG_EXP);
  }

  private _generateMd5(cacheName: string, encoding: string): string {
    return this._nodeTools.md5(cacheName);
  }

  private async _isValidCache(timeToLiveInMinutes: number | undefined, absolutePath: string): Promise<boolean | undefined> {
    if (isNullOrUndefined(timeToLiveInMinutes)) {
      return undefined;
    }
    try {
      const lastModificationDate = await this._getFileLastModificationDate(absolutePath);
      if (isNullOrUndefined(lastModificationDate)) {
        return undefined;
      }
      const currentDate = new Date();
      const dateExpiration = new Date(lastModificationDate.setMinutes(lastModificationDate.getMinutes() + timeToLiveInMinutes));
      const isValidCache = currentDate <= dateExpiration;
      return isValidCache;
    } catch (e) {
      return undefined;
    }
  }

  private async _getFileLastModificationDate(absolutePath: string): Promise<Date | undefined> {
    try {
      const stats = await this._fileStats(absolutePath);
      return stats.mtime;
    } catch (e) {
      return undefined;
    }
  }

  private async _fileExist(absoluteFilePath: string): Promise<boolean> {
    try {
      await this._fileStats(absoluteFilePath);
      return true;
    } catch (e) {
      return false;
    }
  }

  private async _fileStats(absoluteFilePath: string): Promise<Stats> {
    return await this._nodeTools.fsp.stat(absoluteFilePath);
  }

  private _zlibPromised(value: string, options: ZlibOptions): Promise<string> {
    return new Promise(((resolve, reject) => {
      this._nodeTools.zlib.gzip(value, options, (err, res: Buffer) => {
        if (err) {
          return reject(err);
        }
        return resolve(res.toString(this._GZIP_ENCODING));
      });
    }));
  }
}

