/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - solidify-frontend-select.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {NgModule} from "@angular/core";
import {SolidifyFrontendCoreModule} from "../core/solidify-frontend-core.module";
import {MultiSelectContentPresentational} from "./presentationals/multi-select-content/multi-select-content.presentational";
import {MultiSelectDefaultValueContentPresentational} from "./presentationals/multi-select-default-value-content/multi-select-default-value-content.presentational";
import {MultiSelectPresentational} from "./presentationals/multi-select/multi-select.presentational";
import {SearchableMultiSelectContentPresentational} from "./presentationals/searchable-multi-select-content/searchable-multi-select-content.presentational";
import {SearchableMultiSelectPresentational} from "./presentationals/searchable-multi-select/searchable-multi-select.presentational";
import {SearchableSingleSelectContentPresentational} from "./presentationals/searchable-single-select-content/searchable-single-select-content.presentational";
import {SearchableSingleSelectPresentational} from "./presentationals/searchable-single-select/searchable-single-select.presentational";
import {SearchableTreeMultiSelectContentPresentational} from "./presentationals/searchable-tree-multi-select-content/searchable-tree-multi-select-content.presentational";
import {SearchableTreeMultiSelectPresentational} from "./presentationals/searchable-tree-multi-select/searchable-tree-multi-select.presentational";
import {SearchableTreeSingleSelectContentPresentational} from "./presentationals/searchable-tree-single-select-content/searchable-tree-single-select-content.presentational";
import {SearchableTreeSingleSelectPresentational} from "./presentationals/searchable-tree-single-select/searchable-tree-single-select.presentational";

const presentationals = [
  MultiSelectPresentational,
  SearchableMultiSelectPresentational,
  SearchableSingleSelectPresentational,
  SearchableTreeSingleSelectPresentational,
  SearchableTreeMultiSelectPresentational,
];
const contents = [
  SearchableSingleSelectContentPresentational,
  SearchableMultiSelectContentPresentational,
  SearchableTreeSingleSelectContentPresentational,
  SearchableTreeMultiSelectContentPresentational,
  MultiSelectContentPresentational,
  MultiSelectDefaultValueContentPresentational,
];

const components = [
  ...presentationals,
  ...contents,
];

const modules = [
  SolidifyFrontendCoreModule,
];

@NgModule({
  declarations: [
    ...components,
  ],
  imports: [
    ...modules,
  ],
  exports: [
    ...modules,
    ...components,
  ],
  providers: [],
})
export class SolidifyFrontendSelectModule {
}
