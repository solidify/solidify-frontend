/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - multi-select.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {Overlay} from "@angular/cdk/overlay";
import {ComponentPortal} from "@angular/cdk/portal";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  Input,
  OnInit,
  ViewContainerRef,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormBuilder,
  FormControl,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {TranslateService} from "@ngx-translate/core";
import {distinctUntilChanged} from "rxjs/operators";
import {DefaultSolidifyEnvironment} from "../../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../../core/injection-tokens/label-to-translate.injection-token";
import {BaseResourceType} from "../../../core-resources/models/dto/base-resource.model";
import {LabelTranslateInterface} from "../../../label-translate-interface.model";
import {NotificationService} from "../../../core/services/notification.service";
import {TabulationService} from "../../../core/services/tabulation.service";
import {
  isEmptyString,
  isNonEmptyArray,
  isNullOrUndefined,
} from "../../../core-resources/tools/is/is.tool";
import {AbstractMultiSelectPresentational} from "../abstract-multi-select/abstract-multi-select.presentational";
import {MultiSelectContentPresentational} from "../multi-select-content/multi-select-content.presentational";
import {MultiSelectDefaultValueContentPresentational} from "../multi-select-default-value-content/multi-select-default-value-content.presentational";

@Component({
  selector: "solidify-multi-select",
  templateUrl: "./multi-select.presentational.html",
  styleUrls: ["./multi-select.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: MultiSelectPresentational,
    },
  ],
})
export class MultiSelectPresentational<TResource extends BaseResourceType> extends AbstractMultiSelectPresentational<TResource> implements ControlValueAccessor, OnInit {
  @Input()
  list: TResource[];

  @Input()
  formControlDefault: FormControl | undefined;

  get isWithDefaultValue(): boolean {
    return !isNullOrUndefined(this.formControlDefault);
  }

  constructor(protected readonly _fb: FormBuilder,
              protected readonly _elementRef: ElementRef,
              protected readonly _viewContainerRef: ViewContainerRef,
              protected readonly _overlay: Overlay,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _notificationService: NotificationService,
              protected readonly _tabulationService: TabulationService,
              @Inject(ENVIRONMENT) readonly environment: DefaultSolidifyEnvironment,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface,
              protected readonly _translateService: TranslateService) {
    super(_fb,
      _elementRef,
      _viewContainerRef,
      _overlay,
      _changeDetector,
      _notificationService,
      _tabulationService,
      environment,
      labelTranslate,
      _translateService);
  }

  override ngOnInit(): void {
    super.ngOnInit();
    if (this.isWithDefaultValue) {
      this.subscribe(this.formControlDefault.valueChanges.pipe(distinctUntilChanged()), () => this._changeDetector.detectChanges());
    }
  }

  override remove(item: TResource): void {
    super.remove(item);
    if (this.isWithDefaultValue) {
      const defaultValueId = this.formControlDefault.value;
      if (item.resId === defaultValueId) {
        const listValueSelected = this.formControl.value;
        if (listValueSelected.length > 0) {
          this.formControlDefault.setValue(listValueSelected[0]);
        } else {
          this.formControlDefault.setValue(undefined);
        }
        this.formControlDefault.markAsTouched();
      }
    }
  }

  add(item: TResource): void {
    super.add(item);
    if (this.isWithDefaultValue) {
      const defaultValueId = this.formControlDefault.value;
      if (isNullOrUndefined(defaultValueId) || isEmptyString(defaultValueId)) {
        const listValueSelected = this.formControl.value;
        if (listValueSelected.length > 0) {
          this.formControlDefault.setValue(listValueSelected[0]);
        }
        this.formControlDefault.markAsTouched();
      }
    }
  }

  protected _manageInitExistingValue(): void {
    if (isNullOrUndefined(this.list)) {
      return;
    }
    const keys = this.formControl.value;
    if (isNonEmptyArray(keys)) {
      this.selectedItems = [];
      keys.forEach(key => {
        const element = this.list.find(item => item[this.valueKey] === key);
        if (isNullOrUndefined(element)) {
          throw new Error(`Element with key ${key} not find in list for multi-select '${this.title}'`);
        }
        this.selectedItems.push(element);
      });
    }
  }

  getComponentPortal(): ComponentPortal<MultiSelectDefaultValueContentPresentational<TResource>> | ComponentPortal<MultiSelectContentPresentational<TResource>> {
    if (this.isWithDefaultValue) {
      return new ComponentPortal<MultiSelectDefaultValueContentPresentational<TResource>>(MultiSelectDefaultValueContentPresentational);
    }
    return new ComponentPortal<MultiSelectContentPresentational<TResource>>(MultiSelectContentPresentational);
  }

  protected extraBehaviorOnValueSelectedInOverlay(value: TResource): void {
  }
}
