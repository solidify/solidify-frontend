/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - searchable-multi-select-content.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ElementRef,
  Inject,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {DefaultSolidifyEnvironment} from "../../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../../core/injection-tokens/label-to-translate.injection-token";
import {BaseResourceType} from "../../../core-resources/models/dto/base-resource.model";
import {LabelTranslateInterface} from "../../../label-translate-interface.model";
import {ResourceStateModel} from "../../../core/stores/abstract/resource/resource-state.model";
import {isNullOrUndefined} from "../../../core-resources/tools/is/is.tool";
import {ObservableUtil} from "../../../core/utils/observable.util";
import {
  SearchableAbstractContentModeEnum,
  SearchableAbstractContentPresentational,
} from "../searchable-abstract-content/searchable-abstract-content.presentational";
import {SearchableMultiSelectPresentational} from "../searchable-multi-select/searchable-multi-select.presentational";

@Component({
  selector: "solidify-searchable-multi-select-content",
  templateUrl: "../searchable-abstract-content/searchable-abstract-content.presentational.html",
  styleUrls: ["./searchable-multi-select-content.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchableMultiSelectContentPresentational<TResource extends BaseResourceType> extends SearchableAbstractContentPresentational<TResource> implements OnInit, AfterViewInit {
  mode: SearchableAbstractContentModeEnum = SearchableAbstractContentModeEnum.multiple;

  @Input()
  host: SearchableMultiSelectPresentational<ResourceStateModel<TResource>, TResource>;

  private readonly _valueAddedBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("valueAddedChange")
  readonly valueAddedObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._valueAddedBS);

  private readonly _valueRemovedBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("valueRemovedChange")
  readonly valueRemovedObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._valueRemovedBS);

  constructor(protected readonly _store: Store,
              protected readonly _fb: FormBuilder,
              protected readonly _elementRef: ElementRef,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _actions$: Actions,
              @Inject(ENVIRONMENT) readonly environment: DefaultSolidifyEnvironment,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface,
              protected readonly _componentFactoryResolver: ComponentFactoryResolver) {
    super(_store,
      _fb,
      _elementRef,
      _changeDetector,
      _actions$,
      environment,
      _componentFactoryResolver);
  }

  select(value: TResource, force: boolean = false): void {
    if (isNullOrUndefined(value)) {
      return;
    }
    if (this.isActive(value)) {
      this._valueRemovedBS.next(value);
      return;
    }
    this._valueAddedBS.next(value);
    const indexOf = this.listLastSelection.indexOf(value);
    if (indexOf !== -1) {
      this.listLastSelection.splice(indexOf, 1);
    }
  }
}



