/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - searchable-single-select.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  FlexibleConnectedPositionStrategy,
  Overlay,
  OverlayRef,
} from "@angular/cdk/overlay";
import {ComponentPortal} from "@angular/cdk/portal";
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  ElementRef,
  Inject,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormBuilder,
  FormControl,
  FormGroup,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {
  FloatLabelType,
  MatFormFieldAppearance,
  SubscriptSizing,
} from "@angular/material/form-field";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
  tap,
} from "rxjs/operators";
import {CoreAbstractComponent} from "../../../core/components/core-abstract/core-abstract.component";
import {AbstractOverlayPresentational} from "../../../core/components/presentationals/abstract-overlay/abstract-overlay.presentational";
import {SOLIDIFY_CONSTANTS} from "../../../core-resources/constants";
import {OverlayPositionEnum} from "../../../core/enums/overlay-position.enum";
import {IconNamePartialEnum} from "../../../core/enums/partial/icon-name-partial.enum";
import {LocalStoragePartialEnum} from "../../../core/enums/partial/local-storage-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../core/environments/environment.solidify-defaults";
import {FormValidationHelper} from "../../../core/helpers/form-validation.helper";
import {LocalStorageHelper} from "../../../core/helpers/local-storage.helper";
import {OverlayHelper} from "../../../core/helpers/overlay.helper";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../../core/injection-tokens/label-to-translate.injection-token";
import {BaseResourceType} from "../../../core-resources/models/dto/base-resource.model";
import {BaseFormDefinition} from "../../../core/models/forms/base-form-definition.model";
import {Sort} from "../../../core-resources/models/query-parameters/sort.model";
import {TabulationService} from "../../../core/services/tabulation.service";
import {ResourceNameSpace} from "../../../core/stores/abstract/resource/resource-namespace.model";
import {ResourceStateModel} from "../../../core/stores/abstract/resource/resource-state.model";
import {ResourceState} from "../../../core/stores/abstract/resource/resource.state";
import {
  isEmptyString,
  isFunction,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
  Type,
} from "../../../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../../../core-resources/types/extend-enum.type";
import {MappingObject} from "../../../core-resources/types/mapping-type.type";
import {ObservableUtil} from "../../../core/utils/observable.util";
import {SsrUtil} from "../../../core-resources/utils/ssr.util";
import {MemoizedUtil} from "../../../core/utils/stores/memoized.util";
import {StoreUtil} from "../../../core/utils/stores/store.util";
import {StringUtil} from "../../../core-resources/utils/string.util";
import {LabelTranslateInterface} from "../../../label-translate-interface.model";
import {SearchableSingleSelectContentPresentational} from "../searchable-single-select-content/searchable-single-select-content.presentational";

@Component({
  selector: "solidify-searchable-single-select",
  templateUrl: "./searchable-single-select.presentational.html",
  styleUrls: ["./searchable-single-select.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SearchableSingleSelectPresentational,
    },
  ],
})
export class SearchableSingleSelectPresentational<TStateModel extends ResourceStateModel<TResource>, TResource extends BaseResourceType> extends CoreAbstractComponent implements ControlValueAccessor, OnInit, AfterViewInit, OnDestroy {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  formLabel: FormGroup;

  protected _overlayRefs: Set<OverlayRef> = new Set<OverlayRef>();

  readonly APPEARANCE_OUTLINE: MatFormFieldAppearance = "outline";

  @Input()
  set value(value: string) {
    // Allow to reset component value from external
    if (isNullOrUndefinedOrWhiteString(value)) {
      if (isNotNullNorUndefined(this.formControl)) {
        this.formControl.setValue(undefined);
      }
      const formControlLabel = this.formLabel?.get(this.formDefinition.value);
      if (isNotNullNorUndefined(formControlLabel) && isNotNullNorUndefined(formControlLabel.value)) {
        formControlLabel.setValue(undefined);
      }
    }
  }

  @Input()
  placeholder: string;

  @Input()
  valueKey: string;

  @Input()
  labelKey: string;

  @Input()
  appearance: MatFormFieldAppearance | "legacy" = this.appearanceInputMaterial;

  @Input()
  positionLabel: FloatLabelType = this.positionLabelInputMaterial;

  @Input()
  subscriptSizing: SubscriptSizing = this.subscriptSizingInputMaterial;

  floatLabelComputed: FloatLabelType;

  @Input()
  hideItemSelected: boolean = false;

  _labelCallback: (value: TResource) => string = (value => value[this.labelKey]);

  @Input()
  set labelCallback(value: (value: TResource) => string) {
    this._labelCallback = value;
  }

  get labelCallback(): (value: TResource) => string {
    if (isNullOrUndefined(this._labelCallback)) {
      return (value => value[this.labelKey]);
    }
    return this._labelCallback;
  }

  @Input()
  extraInfoSecondLineLabelCallback: (value: TResource) => string;

  @Input()
  colorHexaForLineCallback: (value: TResource) => string;

  @Input()
  extraInfoLabelKey: string;

  @Input()
  extraInfoDisplayOnContent: boolean = true;

  @Input()
  extraInfoImage: ExtendEnum<IconNamePartialEnum>;

  @Input()
  formControl: FormControl;

  @Input()
  required: boolean;

  @Input()
  canDeleteValue: boolean = true;

  @Input()
  resourceNameSpace: ResourceNameSpace;

  @Input()
  state: Type<ResourceState<TStateModel, TResource>>;

  @Input()
  urlKey: string;

  @Input()
  isWithLink: boolean = false;

  @Input()
  tooltipNavigateToTranslate: string;

  @Input()
  searchKey: string;

  @Input()
  extraSearchQueryParam: MappingObject<string, string>;

  @Input()
  defaultValue: string | undefined;

  @Input()
  sort: Sort | undefined;

  @Input("overlayComponent")
  overlayComponent: AbstractOverlayPresentational<TResource>;

  @Input("overlayExtra")
  overlayExtra: any;

  classMatFormField: string;

  private _isDisabled: boolean = false;

  @Input()
  set isDisabled(value: boolean) {
    this._isDisabled = value;
    if (this._isDisabled) {
      this.classMatFormField = this.classInputIgnored;
    } else {
      this.classMatFormField = StringUtil.stringEmpty;
    }
    this._updateFormReadonlyState();
  }

  get isDisabled(): boolean {
    return this._isDisabled;
  }

  get isOverlayOpen(): boolean {
    return this._overlayRefs?.size > 0;
  }

  @Input()
  extraGetAllParameters: any[] = [];

  @Input()
  storeLastSelectionKey: ExtendEnum<LocalStoragePartialEnum> | undefined = undefined;

  @Input()
  displayCommonValuesListKey: string[] = [];

  @Input()
  displayCommonValuesLabel: string;

  @Input()
  hintToTranslate: string;

  isDefaultValueSelected: boolean = false;

  private _url: string;

  private _readonly: boolean;

  @Input()
  set readonly(value: boolean) {
    this._readonly = value;
    this._updateFormReadonlyState();
  }

  get readonly(): boolean {
    return this._readonly || this.isDisabled;
  }

  currentObs: Observable<TResource>;
  isLoadingObs: Observable<boolean>;

  alreadyInit: boolean = false;
  noFormControl: boolean = false;

  labelInitialized: boolean = false;

  classInputHide: string;
  classInputIgnored: string;

  MAX_ITEM_TO_DISPLAY_IN_LAST_SELECTION: number = 3;

  @Input("overlayPositionStrategy")
  overlayPositionStrategy: ((element: ElementRef, overlay: Overlay) => FlexibleConnectedPositionStrategy) | undefined;

  @Input("overlayPosition")
  overlayPosition: OverlayPositionEnum = OverlayPositionEnum.bottom;

  @Input()
  showEmptyRequiredFieldInError: boolean = this.displayEmptyRequiredFieldInError;

  @Input()
  preTreatmentHighlightText: undefined | ((result: string) => string);

  @Input()
  postTreatmentHighlightText: undefined | ((result: string, resultBeforePreTreatment: string) => string);

  @Input()
  prefixIcon: ExtendEnum<IconNamePartialEnum>;

  @ViewChild("inputElementRef")
  readonly input: ElementRef;

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  private readonly _valueBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("valueChange")
  readonly valueObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._valueBS);

  get currentValue(): TResource | undefined {
    return this._valueBS.value;
  }

  protected readonly _navigateBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("navigate")
  readonly navigateObs: Observable<string | undefined> = ObservableUtil.asObservable(this._navigateBS);

  lastActiveElement: Element;
  objectNotFound: boolean = false;

  constructor(private readonly _store: Store,
              protected readonly _fb: FormBuilder,
              private readonly _elementRef: ElementRef,
              private readonly _viewContainerRef: ViewContainerRef,
              private readonly _overlay: Overlay,
              private readonly _ngZone: NgZone,
              private readonly _tabulationService: TabulationService,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _translate: TranslateService,
              @Inject(ENVIRONMENT) readonly environment: DefaultSolidifyEnvironment,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface) {
    super();
    this.displayCommonValuesLabel = this.labelTranslate.selectMostCommonValues;
    this.classInputHide = this.environment.classInputHide;
    this.classInputIgnored = this.environment.classInputIgnored;
    this._ngZone.runOutsideAngular(() => SsrUtil.window?.document.addEventListener("focusin", focusEvent => this.lastActiveElement = document.activeElement));
  }

  ngOnDestroy(): void {
    this.closeOverlays(false, false);
    super.ngOnDestroy();
  }

  ngOnInit(): void {
    if (isNullOrUndefined(this.formControl)) {
      this.noFormControl = true;
      this.formControl = this._fb.control(undefined);
    }
    if (isTrue(this.alreadyInit)) {
      // When validator is updated, the ngOnInit is call a new time
      // This hack prevent this to happen (case with deposit license validator impacted by access level
      return;
    }
    if (isNotNullNorUndefined(this.formControl)) {
      this.subscribe(this.formControl.statusChanges.pipe(
        distinctUntilChanged(),
        filter(status => status === SOLIDIFY_CONSTANTS.FORM_STATUS_INVALID),
        tap(status => {
          setTimeout(() => {
            this._changeDetector.detectChanges();
          });
        }),
      ));
    }
    this._initFormLabel();
    this._setDefaultValue();
    this._dispatchGetActionToRetrieveLabel();
    this._observeLabelAndUrlUpdate();
    this._computeFloatLabel();
    this.alreadyInit = true;
  }

  private _initFormLabel(): void {
    this.formLabel = this._fb.group({
      [this.formDefinition.value]: [undefined],
    });

    this._updateFormReadonlyState();
  }

  private _updateFormReadonlyState(): void {
    if (isNullOrUndefined(this.formLabel)) {
      return;
    }
    if (this.readonly) {
      this.formLabel.disable();
    } else {
      this.formLabel.enable();
    }
  }

  private _setDefaultValue(): void {
    if (isNullOrUndefined(this.defaultValue) || (isNotNullNorUndefined(this.formControl.value) && isNonEmptyString(this.formControl.value))) {
      return;
    }
    this.formControl.setValue(this.defaultValue);
    this.isDefaultValueSelected = true;
  }

  private _dispatchGetActionToRetrieveLabel(): void {
    const key = this.formControl.value;
    this.currentObs = MemoizedUtil.current(this._store, this.state);
    this.isLoadingObs = MemoizedUtil.isLoading(this._store, this.state);
    if (!isNullOrUndefined(key) && isNonEmptyString(key)) {
      StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        new this.resourceNameSpace.GetById(key),
        this.resourceNameSpace.GetByIdSuccess,
        action => {
        },
        this.resourceNameSpace.GetByIdFail,
        action => {
          this.objectNotFound = true;
          this.formLabel.get(this.formDefinition.value)
            .setValue(this._translate.instant(this.labelTranslate.selectObjectNotFound));
          this._changeDetector.detectChanges();
        });
    } else {
      this.labelInitialized = true;
    }
  }

  private _observeLabelAndUrlUpdate(): void {
    this.subscribe(this.currentObs.pipe(
      distinctUntilChanged(),
      filter((value) => !isNullOrUndefined(value) && value[this.valueKey] === this.formControl.value),
      take(1),
      tap((value: TResource) => {
        if (isNullOrUndefined(this.formControl.value) || isEmptyString(this.formControl.value)) {
          return;
        }
        this.formLabel.get(this.formDefinition.value).setValue(this.labelCallback(value));
        if (!isNullOrUndefined(value[this.urlKey])) {
          this._url = value[this.urlKey];
        }
        this._valueBS.next(value);
        this.labelInitialized = true;
        this._computeFloatLabel();
      }),
    ));
  }

  propagateChange: (__: any) => void = (__: any) => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(value: string): void {
    // if (!isNullOrUndefined(value) && !this.noFormControl) {
    //   this.formControl.setValue(value);
    //   this._dispatchGetActionToRetrieveLabel();
    // }
  }

  private _openElementToRefocus: Element;

  set openElementToRefocus(element: Element) {
    this._openElementToRefocus = element;
  }

  get openElementToRefocus(): Element {
    return this._openElementToRefocus;
  }

  @Input()
  defaultValueTooltip: string | undefined;

  openOverlay(focusEvent: FocusEvent): void {
    this.openElementToRefocus = this.lastActiveElement;
    const widthField = this._elementRef.nativeElement.getBoundingClientRect().width;
    const overlayRef = this._overlay.create({
      positionStrategy: this._getPosition(this._elementRef),
      scrollStrategy: this._overlay.scrollStrategies.block(),
      hasBackdrop: true,
      backdropClass: "cdk-overlay-transparent-backdrop",
      width: widthField,
    });

    this._overlayRefs.add(overlayRef);

    const overlayContentComponent = new ComponentPortal<SearchableSingleSelectContentPresentational<TResource>>(SearchableSingleSelectContentPresentational);
    const componentRef = overlayRef.attach(overlayContentComponent);
    componentRef.instance.host = this;

    this._observeValueSelectedInOverlay(componentRef, overlayRef);
    this._observeCloseEventInOverlay(componentRef, overlayRef);
    this._observeClickBackdrop(componentRef, overlayRef);
    this._simulateClickEvent();
  }

  private _simulateClickEvent(): void {
    // For enter in edit mode
    this.input.nativeElement.dispatchEvent(new MouseEvent("click", {
      bubbles: true,
      cancelable: true,
      view: SsrUtil.window,
    }));
  }

  private _observeValueSelectedInOverlay(componentRef: ComponentRef<SearchableSingleSelectContentPresentational<TResource>>, overlayRef: OverlayRef): void {
    this.subscribe(componentRef.instance.valueObs.pipe(
      distinctUntilChanged(),
      tap(value => {
        const valueKey = value[this.valueKey];

        if (isNotNullNorUndefined(this.storeLastSelectionKey)) {
          LocalStorageHelper.addItemInList(this.storeLastSelectionKey, valueKey, this.MAX_ITEM_TO_DISPLAY_IN_LAST_SELECTION + 1);
        }
        this.propagateChange(valueKey);
        this.formControl.setValue(valueKey);
        this._valueBS.next(value);
        this.objectNotFound = false;
        if (!isNullOrUndefined(value[this.urlKey])) {
          this._url = value[this.urlKey];
        }
        this.formLabel.get(this.formDefinition.value).setValue(this.labelCallback(value));
        this._computeFloatLabel();
        this._closeOverlay(overlayRef);
      }),
    ));
  }

  closeOverlays(isFocusNext: boolean = true, isFocusedByTab: boolean = false): void {
    this._overlayRefs.forEach(overlayRef => this._closeOverlay(overlayRef, true));
    this._overlayRefs.clear();
    if (isFocusNext) {
      const elementToFocus = isFocusedByTab ? this._tabulationService.getByDelta(2, this.openElementToRefocus) : this._tabulationService.getNext(this.openElementToRefocus);
      this._tabulationService.focusNext(elementToFocus);
    } else {
      const elementToFocus = isFocusedByTab ? this._tabulationService.getByDelta(-2, this.openElementToRefocus) : this._tabulationService.getPrev(this.openElementToRefocus);
      this._tabulationService.focusPrev(elementToFocus);
    }
  }

  private _observeCloseEventInOverlay(componentRef: ComponentRef<SearchableSingleSelectContentPresentational<TResource>>, overlayRef: OverlayRef): void {
    this.subscribe(componentRef.instance.closeObs.pipe(
      tap(() => {
        this._closeOverlay(overlayRef);
      }),
    ));
  }

  private _observeClickBackdrop(componentRef: ComponentRef<SearchableSingleSelectContentPresentational<TResource>>, overlayRef: OverlayRef): void {
    this.subscribe(overlayRef.backdropClick().pipe(
      tap(() => {
        this._closeOverlay(overlayRef);
      }),
    ));
    this.subscribe(componentRef.instance.closeByTabObs.pipe(
      tap(() => {
        this.closeOverlays();
      }),
    ));
  }

  private _closeOverlay(overlayRef: OverlayRef, keepInOverlayRefs?: boolean): void {
    overlayRef.detach();
    if (!keepInOverlayRefs) {
      this._overlayRefs.delete(overlayRef);
    }
    this.formControl.markAsTouched();
    this._changeDetector.detectChanges();
  }

  private _getPosition(elementToConnectTo: ElementRef): FlexibleConnectedPositionStrategy {
    if (isFunction(this.overlayPositionStrategy)) {
      return this.overlayPositionStrategy(elementToConnectTo, this._overlay);
    }
    return OverlayHelper.getFlexibleConnectedPositionStrategy(this._overlay, elementToConnectTo, this.overlayPosition);
  }

  isValuePresent(): boolean {
    return isNonEmptyString(this.formControl.value);
  }

  isUrlPresent(): boolean {
    return this.isValuePresent() && !isNullOrUndefined(this._url) && isNonEmptyString(this._url);
  }

  clearValue($event: MouseEvent): void {
    if (this.defaultValue) {
      this.formControl.setValue(this.defaultValue);
      this.formControl.markAsDirty();
      this.labelInitialized = false;
      this._dispatchGetActionToRetrieveLabel();
      this._observeLabelAndUrlUpdate();
      this.isDefaultValueSelected = true;
    } else {
      this.formControl.setValue("");
      this.formControl.markAsDirty();
      this.formLabel.get(this.formDefinition.value).setValue(null);
      this._valueBS.next(undefined);
      this._computeFloatLabel();
    }
    $event.stopPropagation();
  }

  navigateTo(event: Event): void {
    if (isNotNullNorUndefined(event)) {
      event.stopPropagation();
      event.preventDefault();
    }
    this._navigateBS.next(this.formControl.value);
  }

  navigateToUrl($event?: MouseEvent): void {
    SsrUtil.window?.open(this._url, "_blank");
    if (!isNullOrUndefined($event)) {
      $event.stopPropagation();
    }
  }

  private _computeFloatLabel(): void {
    const value = this.formLabel?.get(this.formDefinition.value)?.value;
    if (isNullOrUndefined(value) || isEmptyString(value)) {
      this.floatLabelComputed = this.positionLabel;
    } else {
      this.floatLabelComputed = "always";
    }
    this._changeDetector.detectChanges();
  }

  focusInputIfNotClickTarget($event: MouseEvent): void {
    if (!isNullOrUndefined(this.input) && $event.target !== this.input.nativeElement) {
      this.input.nativeElement.focus();
    }
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  search: string = "search";
  value: string = "value";
}
