/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - searchable-abstract-content.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {OverlayRef} from "@angular/cdk/overlay";
import {
  AfterViewInit,
  ChangeDetectorRef,
  ComponentFactoryResolver,
  Directive,
  ElementRef,
  HostBinding,
  HostListener,
  Inject,
  Input,
  OnInit,
  ViewChild,
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
} from "@angular/forms";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  isArray,
  isEmptyArray,
  isEmptyString,
  isFunction,
  isNonEmptyString,
  isNonWhiteString,
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "../../../core-resources/tools/is/is.tool";
import {MappingObjectUtil} from "../../../core-resources/utils/mapping-object.util";
import {StringUtil} from "../../../core-resources/utils/string.util";
import {AbstractContentPresentational} from "../../../core/components/presentationals/abstract-content/abstract-content.presentational";
import {SOLIDIFY_CONSTANTS} from "../../../core-resources/constants";
import {DefaultSolidifyEnvironment} from "../../../core/environments/environment.solidify-defaults";
import {LocalStorageHelper} from "../../../core/helpers/local-storage.helper";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {BaseResourceType} from "../../../core-resources/models/dto/base-resource.model";
import {QueryParameters} from "../../../core-resources/models/query-parameters/query-parameters.model";
import {ResourceStateModel} from "../../../core/stores/abstract/resource/resource-state.model";
import {MemoizedUtil} from "../../../core/utils/stores/memoized.util";
import {StoreUtil} from "../../../core/utils/stores/store.util";
import {SearchableMultiSelectPresentational} from "../searchable-multi-select/searchable-multi-select.presentational";
import {SearchableSingleSelectPresentational} from "../searchable-single-select/searchable-single-select.presentational";

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class SearchableAbstractContentPresentational<TResource extends BaseResourceType> extends AbstractContentPresentational<TResource> implements OnInit, AfterViewInit {
  abstract mode: SearchableAbstractContentModeEnum;

  listObs: Observable<TResource[]>;
  totalObs: Observable<number>;
  isLoadingObs: Observable<boolean>;
  isLoadingChunkObs: Observable<boolean>;
  navigationResId: string;

  form: FormGroup;

  protected _overlayRefs: Set<OverlayRef> = new Set<OverlayRef>();

  @HostBinding("class.is-loading")
  isLoading: boolean;

  list: TResource[];

  listLastSelection: TResource[] = [];

  listCommonValues: TResource[] = [];

  @Input()
  abstract host: SearchableMultiSelectPresentational<ResourceStateModel<TResource>, TResource> | SearchableSingleSelectPresentational<ResourceStateModel<TResource>, TResource>;

  @ViewChild("input")
  input: ElementRef;

  @HostListener("scroll", ["$event"])
  onWindowScroll(scrollEvent: Event): void {
    if (MemoizedUtil.totalSnapshot(this._store, this.host.state) === MemoizedUtil.listSnapshot(this._store, this.host.state)?.length) {
      return;
    }
    const currentPosition = this._elementRef.nativeElement.scrollTop + this._elementRef.nativeElement.offsetHeight;
    const maxPosition = this._elementRef.nativeElement.scrollHeight - 1;

    if (currentPosition >= maxPosition) {
      this.loadNextChunk();
    }
  }

  protected readonly _debounceTime: number = 250;
  protected readonly _pageSize: number;

  get modeEnum(): typeof SearchableAbstractContentModeEnum {
    return SearchableAbstractContentModeEnum;
  }

  trackByFn(index: number, item: TResource): string {
    return item.resId;
  }

  constructor(protected readonly _store: Store,
              protected readonly _fb: FormBuilder,
              protected readonly _elementRef: ElementRef,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _actions$: Actions,
              @Inject(ENVIRONMENT) readonly environment: DefaultSolidifyEnvironment,
              protected readonly _componentFactoryResolver: ComponentFactoryResolver) {
    super();
    this._pageSize = this.environment.defaultEnumValuePageSizeLazyLoad;
  }

  ngOnInit(): void {
    super.ngOnInit();

    this._initSearchForm();
    this._dispatchInitDataLoad();
    this._retrieveSelector();
    this._observeSearchInput();
    this._observeIsLoading();
    this._observeList();
    this._manageCommonValue();
    this._manageLastSelection();
  }

  sortLastResultAlphabetically(a: TResource, b: TResource, key: string): number {
    if (a[key] < b[key]) {
      return -1;
    }
    if (a[key] > b[key]) {
      return 1;
    }
    return 0;
  }

  isActiveSearch(): boolean {
    return isNonEmptyString(this.form.get(this.host.formDefinition.search).value);
  }

  private _initSearchForm(): void {
    this.form = this._fb.group({
      [this.host.formDefinition.search]: [""],
    });
  }

  getFocusedResourceAndIndex(): { value: TResource | undefined, index: number } {
    const list = this.list;
    for (let index = 0; index < list.length; index++) {
      if (this.navigationResId === list[index][this.host.valueKey]) {
        return {value: list[index], index};
      }
    }
    return {value: undefined, index: -1};
  }

  getFocusedResource(): TResource | undefined {
    return this.getFocusedResourceAndIndex().value;
  }

  getFocusedIndex(): number {
    return this.getFocusedResourceAndIndex().index;
  }

  selectNextResource(): void {
    const list = this.list;
    const focusedIndex = this.getFocusedIndex();
    if (focusedIndex !== list.length - 1) {
      this.navigationResId = list[focusedIndex + 1][this.host.valueKey];
    }
  }

  selectPreviousResource(): void {
    const list = this.list;
    const focusedIndex = this.getFocusedIndex();
    if (focusedIndex > 0) {
      this.navigationResId = list[focusedIndex - 1][this.host.valueKey];
    } else {
      this.navigationResId = undefined;
    }
  }

  @HostListener("keydown.arrowDown", ["$event"])
  onArrowDown(keyboardEvent: KeyboardEvent): void {
    if (keyboardEvent && !keyboardEvent.defaultPrevented) {
      keyboardEvent.preventDefault();
      this.selectNextResource();
    }
  }

  @HostListener("keydown.arrowUp", ["$event"])
  onArrowUp(keyboardEvent: KeyboardEvent): void {
    if (keyboardEvent && !keyboardEvent.defaultPrevented) {
      keyboardEvent.preventDefault();
      this.selectPreviousResource();
    }
  }

  @HostListener("keydown.enter", ["$event"])
  onSelectValue(keyboardEvent: KeyboardEvent): void {
    if (keyboardEvent && !keyboardEvent.defaultPrevented) {
      keyboardEvent.preventDefault();
      this.select(this.getFocusedResource(), false);
    }
  }

  private _dispatchInitDataLoad(): void {
    const queryParameters = new QueryParameters(this._pageSize, this.host.sort);
    if (isNotNullNorUndefined(this.host.extraSearchQueryParam)) {
      queryParameters.search.searchItems = MappingObjectUtil.copy(this.host.extraSearchQueryParam);
    }
    this._store.dispatch(new this.host.resourceNameSpace.GetAll(queryParameters, false, true, ...this.host.extraGetAllParameters));
  }

  private _retrieveSelector(): void {
    this.listObs = MemoizedUtil.list(this._store, this.host.state);
    this.totalObs = MemoizedUtil.total(this._store, this.host.state);
    this.isLoadingObs = MemoizedUtil.isLoading(this._store, this.host.state);
    this.isLoadingChunkObs = MemoizedUtil.select(this._store, this.host.state, state => state.isLoadingChunk);
  }

  private _observeSearchInput(): void {
    this.subscribe(this.form.get(this.host.formDefinition.search).valueChanges.pipe(
      debounceTime(this._debounceTime),
      distinctUntilChanged(),
      tap(v => {
        const queryParameters = new QueryParameters(this._pageSize, this.host.sort);
        if (isNotNullNorUndefined(this.host.extraSearchQueryParam)) {
          queryParameters.search.searchItems = MappingObjectUtil.copy(this.host.extraSearchQueryParam);
        }
        MappingObjectUtil.set(queryParameters.search.searchItems, (isNotNullNorUndefined(this.host.searchKey) ? this.host.searchKey : this.host.labelKey), v);
        this._store.dispatch(new this.host.resourceNameSpace.GetAll(queryParameters, true, true, ...this.host.extraGetAllParameters));
      }),
    ));
  }

  ngAfterViewInit(): void {
    this.setFocusOnInput();
  }

  private setFocusOnInput(): void {
    setTimeout(() => {
      this.input.nativeElement.focus();
    }, 0);
  }

  private _observeIsLoading(): void {
    this.subscribe(this.isLoadingObs.pipe(
      distinctUntilChanged(),
      tap(isLoading => this.isLoading = isLoading),
    ));
  }

  private _observeList(): void {
    this.subscribe(this.listObs.pipe(
      distinctUntilChanged(),
      tap(list => this.list = list),
    ));
  }

  computeHighlightText(valueElement: string): string {
    const fc = this.form.get(this.host.formDefinition.search);
    if (isNullOrUndefined(fc)) {
      return valueElement;
    }
    let value: string = fc.value;
    if (isNullOrUndefined(value) || isEmptyString(value)) {
      return valueElement;
    }
    value = value.trim()
      .replace(SOLIDIFY_CONSTANTS.COMMA, SOLIDIFY_CONSTANTS.STRING_EMPTY)
      .replace(SOLIDIFY_CONSTANTS.SEMICOLON, SOLIDIFY_CONSTANTS.STRING_EMPTY);
    let listTerms = value.split(SOLIDIFY_CONSTANTS.SPACE);
    listTerms = listTerms.filter(t => isNonWhiteString(t));
    let result = valueElement;
    if (isFunction(this.host.preTreatmentHighlightText)) {
      result = this.host.preTreatmentHighlightText(result);
    }
    listTerms.forEach(term => {
      result = result.replace(new RegExp(term, "gi"), match => `{{${match}}}`);
    });
    result = StringUtil.replaceAll(result, "{{", `<span class="highlight-text">`);
    result = StringUtil.replaceAll(result, "}}", `</span>`);
    if (isFunction(this.host.postTreatmentHighlightText)) {
      result = this.host.postTreatmentHighlightText(result, valueElement);
    }
    return result;
  }

  loadNextChunk(): void {
    this._store.dispatch(new this.host.resourceNameSpace.LoadNextChunkList());
  }

  private _manageCommonValue(): void {
    if (isNullOrUndefined(this.host.displayCommonValuesListKey)) {
      return;
    }

    const listCommonValuesId = [...this.host.displayCommonValuesListKey];
    listCommonValuesId.forEach(id => {
      this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        new this.host.resourceNameSpace.GetById(id),
        this.host.resourceNameSpace.GetByIdSuccess,
        action => {
          this.listCommonValues.push(action.model);
          this.listCommonValues.sort((a, b) => this.sortLastResultAlphabetically(a, b, this.host.labelKey));
          this._changeDetector.detectChanges();
        }));
    });
  }

  private _manageLastSelection(): void {
    if (isNullOrUndefined(this.host.storeLastSelectionKey)) {
      return;
    }

    let listLastSelectionId = LocalStorageHelper.getListItem(this.host.storeLastSelectionKey);
    listLastSelectionId = this.reworkListLastSelectedValues(listLastSelectionId);
    listLastSelectionId.forEach(id => {
      this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        new this.host.resourceNameSpace.GetById(id),
        this.host.resourceNameSpace.GetByIdSuccess,
        action => {
          this.listLastSelection.push(action.model);
          this.listLastSelection.sort((a, b) => this.sortLastResultAlphabetically(a, b, this.host.labelKey));
          this._changeDetector.detectChanges();
        },
        this.host.resourceNameSpace.GetByIdFail,
        action => {
          const listItem = LocalStorageHelper.getListItem(this.host.storeLastSelectionKey);
          if (listItem.indexOf(id) !== -1) {
            LocalStorageHelper.removeItemInList(this.host.storeLastSelectionKey, id);
          }
        }));
    });
  }

  private reworkListLastSelectedValues(listLastSelectionId: string[]): string[] {
    listLastSelectionId = this.removeCurrentValueInLastSelectedValues(listLastSelectionId);
    listLastSelectionId = this.removeSuperfluousLastSelectedValues(listLastSelectionId);
    return listLastSelectionId;
  }

  private removeCurrentValueInLastSelectedValues(listLastSelectionId: string[]): string[] {
    const currentSelectedId = this.host.formControl.value;
    if (isNullOrUndefined(currentSelectedId) || isEmptyString(currentSelectedId) || isEmptyArray(currentSelectedId)) {
      return listLastSelectionId;
    }
    let listCurrentSelectedId = [];
    if (isArray(currentSelectedId)) {
      listCurrentSelectedId = currentSelectedId;
    } else {
      listCurrentSelectedId = [currentSelectedId];
    }
    listCurrentSelectedId.forEach(id => {
      const indexOf = listLastSelectionId.indexOf(id);
      if (indexOf !== -1) {
        listLastSelectionId.splice(indexOf, 1);
      }
    });
    return listLastSelectionId;
  }

  private removeSuperfluousLastSelectedValues(listLastSelectionId: string[]): string[] {
    if (listLastSelectionId.length > this.host.MAX_ITEM_TO_DISPLAY_IN_LAST_SELECTION) {
      listLastSelectionId = listLastSelectionId.splice(listLastSelectionId.length - this.host.MAX_ITEM_TO_DISPLAY_IN_LAST_SELECTION, listLastSelectionId.length);
    }
    return listLastSelectionId;
  }

  doNothing(): void {
  }
}

export enum SearchableAbstractContentModeEnum {
  single = "single",
  multiple = "multiple"
}
