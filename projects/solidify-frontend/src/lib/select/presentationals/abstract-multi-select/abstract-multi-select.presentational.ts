/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - abstract-multi-select.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  FlexibleConnectedPositionStrategy,
  Overlay,
  OverlayRef,
} from "@angular/cdk/overlay";
import {ComponentPortal} from "@angular/cdk/portal";
import {
  AfterViewInit,
  ChangeDetectorRef,
  ComponentRef,
  Directive,
  ElementRef,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormBuilder,
  FormControl,
} from "@angular/forms";
import {
  FloatLabelType,
  MatFormFieldAppearance,
  SubscriptSizing,
} from "@angular/material/form-field";
import {TranslateService} from "@ngx-translate/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  tap,
} from "rxjs/operators";
import {CoreAbstractComponent} from "../../../core/components/core-abstract/core-abstract.component";
import {AbstractContentPresentational} from "../../../core/components/presentationals/abstract-content/abstract-content.presentational";
import {SOLIDIFY_CONSTANTS} from "../../../core-resources/constants";
import {OverlayPositionEnum} from "../../../core/enums/overlay-position.enum";
import {IconNamePartialEnum} from "../../../core/enums/partial/icon-name-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../core/environments/environment.solidify-defaults";
import {FormValidationHelper} from "../../../core/helpers/form-validation.helper";
import {OverlayHelper} from "../../../core/helpers/overlay.helper";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../../core/injection-tokens/label-to-translate.injection-token";
import {BaseResourceType} from "../../../core-resources/models/dto/base-resource.model";
import {BaseFormDefinition} from "../../../core/models/forms/base-form-definition.model";
import {NotificationService} from "../../../core/services/notification.service";
import {TabulationService} from "../../../core/services/tabulation.service";
import {
  isFunction,
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "../../../core-resources/tools/is/is.tool";
import {ExtendEnum} from "../../../core-resources/types/extend-enum.type";
import {ClipboardUtil} from "../../../core-resources/utils/clipboard.util";
import {ObservableUtil} from "../../../core/utils/observable.util";
import {LabelTranslateInterface} from "../../../label-translate-interface.model";
import {MultiSelectContentPresentational} from "../multi-select-content/multi-select-content.presentational";
import {MultiSelectDefaultValueContentPresentational} from "../multi-select-default-value-content/multi-select-default-value-content.presentational";
import {SearchableMultiSelectContentPresentational} from "../searchable-multi-select-content/searchable-multi-select-content.presentational";

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractMultiSelectPresentational<TResource extends BaseResourceType> extends CoreAbstractComponent implements ControlValueAccessor, OnInit, AfterViewInit, OnDestroy {

  protected _overlayRefs: Set<OverlayRef> = new Set<OverlayRef>();

  protected _elementToReFocus: Element;

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  readonly APPEARANCE_OUTLINE: MatFormFieldAppearance = "outline";

  @Input()
  title: string;

  @Input()
  placeholder: string;

  @Input()
  valueKey: string;

  @Input()
  labelKey: string;

  @Input()
  translateLabel: boolean = false;

  @Input()
  labelCallback: (value: TResource) => string = (value => value[this.labelKey]);

  @Input()
  extraInfoLabelKey: string;

  @Input()
  extraInfoDisplayOnContent: boolean = true;

  @Input()
  extraInfoImage: ExtendEnum<IconNamePartialEnum>;

  @Input()
  extraInfoSecondLineLabelCallback: (value: TResource) => string;

  @Input()
  colorHexaForLineCallback: (value: TResource) => string;

  @Input()
  formControl: FormControl;

  @Input()
  required: boolean;

  @Input()
  closeAfterValueSelected: boolean = false;

  @Input()
  hideItemSelected: boolean = false;

  @Input()
  isWithLink: boolean = false;

  @Input()
  tooltipNavigateToTranslate: string;

  @Input()
  prefixIcon: ExtendEnum<IconNamePartialEnum>;

  selectedItems: TResource[] = [];
  removable: boolean = true;
  classInputHide: string;

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  get isOverlayOpen(): boolean {
    return this._overlayRefs?.size > 0;
  }

  @Input()
  hintToTranslate: string;

  @Input()
  appearance: MatFormFieldAppearance = this.appearanceInputMaterial;

  @Input()
  positionLabel: FloatLabelType = this.positionLabelInputMaterial;

  @Input()
  subscriptSizing: SubscriptSizing = this.subscriptSizingInputMaterial;

  @Input("overlayPositionStrategy")
  overlayPositionStrategy: ((element: ElementRef, overlay: Overlay) => FlexibleConnectedPositionStrategy) | undefined;

  @Input("overlayPosition")
  overlayPosition: OverlayPositionEnum = OverlayPositionEnum.bottom;

  @Input()
  showEmptyRequiredFieldInError: boolean = this.displayEmptyRequiredFieldInError;

  @ViewChild("inputElementRef")
  readonly input: ElementRef;

  protected readonly _valueBS: BehaviorSubject<TResource[] | undefined> = new BehaviorSubject<TResource[] | undefined>(undefined);
  @Output("valueChange")
  readonly valueObs: Observable<TResource[] | undefined> = ObservableUtil.asObservable(this._valueBS);

  protected readonly _navigateBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("navigate")
  readonly navigateObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._navigateBS);

  constructor(protected readonly _fb: FormBuilder,
              protected readonly _elementRef: ElementRef,
              protected readonly _viewContainerRef: ViewContainerRef,
              protected readonly _overlay: Overlay,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _notificationService: NotificationService,
              protected readonly _tabulationService: TabulationService,
              @Inject(ENVIRONMENT) readonly environment: DefaultSolidifyEnvironment,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface,
              protected readonly _translateService: TranslateService) {
    super();
    this.classInputHide = this.environment.classInputHide;
  }

  ngOnInit(): void {
    if (isNotNullNorUndefined(this.formControl)) {
      this.subscribe(this.formControl.statusChanges.pipe(
        distinctUntilChanged(),
        filter(status => status === SOLIDIFY_CONSTANTS.FORM_STATUS_INVALID),
        tap(status => {
          setTimeout(() => {
            this._changeDetector.detectChanges();
          });
        }),
      ));
    }
    this._manageInitExistingValue();
  }

  ngOnDestroy(): void {
    this.closeOverlays();
    super.ngOnDestroy();
  }

  protected abstract _manageInitExistingValue(): void;

  propagateChange: (__: any) => void = (__: any) => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(value: string[]): void {
    this.orderSelectedItemByValue(value);
  }

  private orderSelectedItemByValue(value: string[]): void {
    this.selectedItems = this.selectedItems.sort((a, b) => value.indexOf(a.resId) - value.indexOf(b.resId));
  }

  abstract getComponentPortal(): any
    | ComponentPortal<SearchableMultiSelectContentPresentational<TResource>>
    | ComponentPortal<MultiSelectDefaultValueContentPresentational<TResource>>
    | ComponentPortal<MultiSelectContentPresentational<TResource>>;

  openOverlay(inputElementRef: Element): void {
    this._elementToReFocus = inputElementRef;
    const minWidthField = this._elementRef.nativeElement.getBoundingClientRect().width;
    const overlayRef = this._overlay.create({
      positionStrategy: this._getPosition(this._elementRef),
      scrollStrategy: this._overlay.scrollStrategies.block(),
      hasBackdrop: true,
      backdropClass: "cdk-overlay-transparent-backdrop",
      width: minWidthField,
    });

    this._overlayRefs.add(overlayRef);

    const overlayContentComponent = this.getComponentPortal();
    const componentRef = overlayRef.attach(overlayContentComponent);
    componentRef.instance.host = this;

    this._observeValueSelectedInOverlay(componentRef, overlayRef);
    this._observeCloseEventInOverlay(componentRef, overlayRef);
    this._observeClickBackdrop(componentRef, overlayRef);
  }

  protected _observeValueSelectedInOverlay(componentRef: ComponentRef<MultiSelectContentPresentational<TResource>>
                                             | ComponentRef<SearchableMultiSelectContentPresentational<TResource>>,
                                           overlayRef: OverlayRef): void {
    this.subscribe(componentRef.instance.valueAddedObs.pipe(
      tap(value => {
        this.add(value);
        this.extraBehaviorOnValueSelectedInOverlay(value);
        this.formControl.markAsTouched();
        if (this.closeAfterValueSelected) {
          this._closeOverlay(overlayRef);
          this._tabulationService.focusNext(this._elementToReFocus);
        } else {
          overlayRef.updatePosition();
        }
      }),
    ));

    this.subscribe(componentRef.instance.valueRemovedObs.pipe(
      tap(value => {
        this.remove(value);
        this.formControl.markAsTouched();
        if (this.closeAfterValueSelected) {
          this._closeOverlay(overlayRef);
          this._tabulationService.focusNext(this._elementToReFocus);
        } else {
          overlayRef.updatePosition();
        }
      }),
    ));
  }

  protected abstract extraBehaviorOnValueSelectedInOverlay(value: TResource): void;

  private _observeCloseEventInOverlay(componentRef: ComponentRef<AbstractContentPresentational<TResource>>, overlayRef: OverlayRef): void {
    this.subscribe(componentRef.instance.closeObs.pipe(
      tap(() => {
        this._closeOverlay(overlayRef);
        this._tabulationService.focusNext(this._elementToReFocus);
      }),
    ));
  }

  private _observeClickBackdrop(componentRef: ComponentRef<AbstractContentPresentational<TResource>>, overlayRef: OverlayRef): void {

    this.subscribe(overlayRef.backdropClick().pipe(
      tap(() => {
        this._closeOverlay(overlayRef);
        this._tabulationService.focusNext(this._elementToReFocus);
      }),
    ));
    this.subscribe(componentRef.instance.closeByTabObs.pipe(
      tap(() => {
          this._closeOverlay(overlayRef);
          this._tabulationService.focusNext(this._elementToReFocus);
        },
      )));
    this.subscribe(componentRef.instance.closeByShiftTabObs.pipe(
      tap(() => {
          this._closeOverlay(overlayRef);
          this._tabulationService.focusPrev(this._elementToReFocus);
        },
      )));
  }

  private _closeOverlay(overlayRef: OverlayRef, keepInOverlayRefs?: boolean): void {
    overlayRef.detach();
    if (!keepInOverlayRefs) {
      this._overlayRefs.delete(overlayRef);
    }
    this.formControl.markAsTouched();
    this._changeDetector.detectChanges();
  }

  private _getPosition(elementToConnectTo: ElementRef): FlexibleConnectedPositionStrategy {
    if (isFunction(this.overlayPositionStrategy)) {
      return this.overlayPositionStrategy(elementToConnectTo, this._overlay);
    }
    return OverlayHelper.getFlexibleConnectedPositionStrategy(this._overlay, elementToConnectTo, this.overlayPosition);
  }

  focusInputIfNotClickTarget($event: MouseEvent): void {
    if (!isNullOrUndefined(this.input) && $event.target !== this.input.nativeElement) {
      this.input.nativeElement.focus();
    }
  }

  add(item: TResource): void {
    if (!isNullOrUndefined(this.searchItemInSelectedItems(item))) {
      return;
    }
    this.selectedItems.push(item);
    this._changeDetector.detectChanges();
    this._updateFormControlWithSelectedItem();
  }

  remove(item: TResource): void {
    const index = this.searchIndexItemInSelectedItems(item);
    if (index === -1) {
      return;
    }
    this.selectedItems.splice(index, 1);
    this._updateFormControlWithSelectedItem();
    this._changeDetector.detectChanges();
  }

  private _updateFormControlWithSelectedItem(): void {
    const arrayKeys = this.selectedItems.map(c => c[this.valueKey]);
    this.formControl.setValue(arrayKeys);
    this.propagateChange(arrayKeys);
    this._valueBS.next(arrayKeys);
  }

  protected searchItemInSelectedItemsWithId(id: string): TResource | undefined {
    const index = this.searchIndexItemInSelectedItems({[this.valueKey]: id} as any);
    if (index === -1) {
      return this.selectedItems[index];
    }
  }

  private searchItemInSelectedItems(item: TResource): TResource | undefined {
    const index = this.searchIndexItemInSelectedItems(item);
    if (index === -1) {
      return undefined;
    }
    return this.selectedItems[index];
  }

  private searchIndexItemInSelectedItems(item: TResource): number {
    return this.selectedItems.findIndex(s => s[this.valueKey] === item[this.valueKey]);
  }

  navigateTo(event: Event, value: TResource): void {
    if (isNotNullNorUndefined(event)) {
      event.stopPropagation();
      event.preventDefault();
    }
    this._navigateBS.next(value);
  }

  copyToClipboard(value: string, $event?: Event): void {
    if (isNotNullNorUndefined($event)) {
      $event.stopPropagation();
    }
    ClipboardUtil.copyStringToClipboard(value);
    this._notificationService.showInformation(this.labelTranslate.inputMultiSelectNotificationExtraInfoCopyToClipboard);
  }

  closeOverlays(): void {
    this._overlayRefs.forEach(overlayRef => this._closeOverlay(overlayRef, true));
    this._overlayRefs.clear();
  }

  trackByFn(index: number, item: TResource): string {
    return item.resId;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  search: string = "search";
  value: string = "value";
}
