/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - multi-select-content.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  Output,
} from "@angular/core";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {AbstractContentPresentational} from "../../../core/components/presentationals/abstract-content/abstract-content.presentational";
import {LABEL_TRANSLATE} from "../../../core/injection-tokens/label-to-translate.injection-token";
import {BaseResourceType} from "../../../core-resources/models/dto/base-resource.model";
import {isNullOrUndefined} from "../../../core-resources/tools/is/is.tool";
import {ObservableUtil} from "../../../core/utils/observable.util";
import {LabelTranslateInterface} from "../../../label-translate-interface.model";

@Component({
  selector: "solidify-multi-select-content",
  templateUrl: "./multi-select-content.presentational.html",
  styleUrls: ["./multi-select-content.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MultiSelectContentPresentational<TResource extends BaseResourceType> extends AbstractContentPresentational<TResource> {

  @Input()
  host: any; // (MultiSelectPresentational<TResource>;) CAUSE RUNTIME ERROR WHEN AOT BUILD WITHOUT SOURCEMAP. Following error on polyfills.js: Uncaught TypeError: Object prototype may only be an Object or null: undefined

  private readonly _valueAddedBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("valueAddedChange")
  readonly valueAddedObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._valueAddedBS);

  private readonly _valueRemovedBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("valueRemovedChange")
  readonly valueRemovedObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._valueRemovedBS);

  readonly noDataToSelect: string;

  constructor(@Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface) {
    super();
    this.noDataToSelect = this.labelTranslate.inputMultiSelectNoDataToSelect;
  }

  select(value: TResource): void {
    if (isNullOrUndefined(value)) {
      return;
    }
    if (this.isActive(value)) {
      this._valueRemovedBS.next(value);
      return;
    }
    this._valueAddedBS.next(value);
  }
}



