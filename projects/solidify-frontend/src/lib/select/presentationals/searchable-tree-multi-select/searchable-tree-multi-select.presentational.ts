/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - searchable-tree-multi-select.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  FlexibleConnectedPositionStrategy,
  Overlay,
  OverlayRef,
} from "@angular/cdk/overlay";
import {ComponentPortal} from "@angular/cdk/portal";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  ElementRef,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormBuilder,
  FormControl,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {
  FloatLabelType,
  MatFormFieldAppearance,
  SubscriptSizing,
} from "@angular/material/form-field";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  tap,
} from "rxjs/operators";
import {
  isEmptyArray,
  isFunction,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "../../../core-resources/tools/is/is.tool";
import {ClipboardUtil} from "../../../core-resources/utils/clipboard.util";
import {StringUtil} from "../../../core-resources/utils/string.util";
import {CoreAbstractComponent} from "../../../core/components/core-abstract/core-abstract.component";
import {AbstractContentPresentational} from "../../../core/components/presentationals/abstract-content/abstract-content.presentational";
import {AbstractOverlayPresentational} from "../../../core/components/presentationals/abstract-overlay/abstract-overlay.presentational";
import {SOLIDIFY_CONSTANTS} from "../../../core-resources/constants";
import {OverlayPositionEnum} from "../../../core/enums/overlay-position.enum";
import {IconNamePartialEnum} from "../../../core/enums/partial/icon-name-partial.enum";
import {LocalStoragePartialEnum} from "../../../core/enums/partial/local-storage-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../core/environments/environment.solidify-defaults";
import {FormValidationHelper} from "../../../core/helpers/form-validation.helper";
import {LocalStorageHelper} from "../../../core/helpers/local-storage.helper";
import {OverlayHelper} from "../../../core/helpers/overlay.helper";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../../core/injection-tokens/label-to-translate.injection-token";
import {BaseResourceType} from "../../../core-resources/models/dto/base-resource.model";
import {BaseFormDefinition} from "../../../core/models/forms/base-form-definition.model";
import {NotificationService} from "../../../core/services/notification.service";
import {TabulationService} from "../../../core/services/tabulation.service";
import {ExtendEnum} from "../../../core-resources/types/extend-enum.type";
import {ObservableUtil} from "../../../core/utils/observable.util";
import {LabelTranslateInterface} from "../../../label-translate-interface.model";
import {SearchableTreeMultiSelectContentPresentational} from "../searchable-tree-multi-select-content/searchable-tree-multi-select-content.presentational";

@Component({
  selector: "solidify-searchable-tree-multi-select",
  templateUrl: "./searchable-tree-multi-select.presentational.html",
  styleUrls: ["./searchable-tree-multi-select.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SearchableTreeMultiSelectPresentational,
    },
  ],
})
export class SearchableTreeMultiSelectPresentational<TResource extends BaseResourceType> extends CoreAbstractComponent /*AbstractMultiSelectPresentational<TResource>*/ implements ControlValueAccessor, OnInit, OnDestroy {
  @Input()
  list: TResource[];

  @Input()
  extraInfoPlaceholderCallback: ((resource: TResource) => string) | undefined = undefined;

  protected readonly _extraInfoCallbackBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("extraInfoCallback")
  readonly extraInfoCallbackObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._extraInfoCallbackBS);

  @ViewChild("inputElementRef", {static: true})
  inputElementRef: ElementRef;

  @Input()
  appearance: MatFormFieldAppearance = this.appearanceInputMaterial;

  @Input()
  positionLabel: FloatLabelType = this.positionLabelInputMaterial;

  @Input()
  subscriptSizing: SubscriptSizing = this.subscriptSizingInputMaterial;

  classMatFormField: string;
  MAX_ITEM_TO_DISPLAY_IN_LAST_SELECTION: number = 3;

  private _isDisabled: boolean = false;

  @Input()
  set isDisabled(value: boolean) {
    this._isDisabled = value;
    if (this._isDisabled) {
      this.classMatFormField = this.classInputIgnored;
    } else {
      this.classMatFormField = StringUtil.stringEmpty;
    }
  }

  get isDisabled(): boolean {
    return this._isDisabled;
  }

  @Input()
  displayCommonValuesListKey: string[] = [];

  @Input()
  displayCommonValuesLabel: string;

  @Input()
  defaultValueToHighlight: string[] | undefined;

  @Input()
  defaultValueLabel: string;

  @Input("overlayComponent")
  overlayComponent: AbstractOverlayPresentational<TResource>;

  @Input("overlayExtra")
  overlayExtra: any;

  @Input()
  expandNodeWhenChecked: boolean = true;

  @Input()
  preTreatmentHighlightText: undefined | ((result: string) => string);

  @Input()
  postTreatmentHighlightText: undefined | ((result: string, resultBeforePreTreatment: string) => string);

  labelInitialized: boolean = false;
  isInitialized: boolean = false;

  classInputIgnored: string;

  isADefaultValue(resId: string): boolean {
    if (isNonEmptyArray(this.defaultValueToHighlight)) {
      const index = this.defaultValueToHighlight.findIndex(defaultResId => defaultResId === resId);
      return index !== -1;
    }
    return false;
  }

  constructor(protected readonly _fb: FormBuilder,
              protected readonly _elementRef: ElementRef,
              protected readonly _viewContainerRef: ViewContainerRef,
              protected readonly _overlay: Overlay,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _notificationService: NotificationService,
              protected readonly _tabulationService: TabulationService,
              @Inject(ENVIRONMENT) readonly environment: DefaultSolidifyEnvironment,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface,
              private readonly _store: Store,
              protected readonly _translateService: TranslateService) {
    super();
    this.classInputHide = this.environment.classInputHide;
    this.classInputIgnored = this.environment.classInputIgnored;
    this.displayCommonValuesLabel = this.labelTranslate.selectMostCommonValues;
    if (!isNonEmptyString(this.defaultValueToHighlight)) {
      this.defaultValueLabel = this.labelTranslate.inputMultiSelectDefaultValue;
    }
  }

  getComponentPortal(): ComponentPortal<SearchableTreeMultiSelectContentPresentational<TResource>> {
    return new ComponentPortal<SearchableTreeMultiSelectContentPresentational<TResource>>(SearchableTreeMultiSelectContentPresentational);
  }

  protected _overlayRefs: Set<OverlayRef> = new Set<OverlayRef>();

  protected _elementToReFocus: Element;

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  readonly APPEARANCE_OUTLINE: MatFormFieldAppearance = "outline";

  @Input()
  title: string;

  @Input()
  placeholder: string;

  @Input()
  valueKey: string = "resId";

  @Input()
  parentKey: string = "parent";

  @Input()
  labelKey: string = "name";

  @Input()
  translateLabel: boolean = false;

  @Input()
  labelCallback: (value: TResource) => string = (value => value[this.labelKey]);

  @Input()
  extraInfoLabelKey: string;

  @Input()
  extraInfoDisplayOnContent: boolean = true;

  @Input()
  storeLastSelectionKey: ExtendEnum<LocalStoragePartialEnum> | undefined = undefined;

  @Input()
  extraInfoImage: ExtendEnum<IconNamePartialEnum>;

  @Input()
  extraInfoSecondLineLabelCallback: (value: TResource) => string;

  @Input()
  colorHexaForLineCallback: (value: TResource) => string;

  @Input()
  formControl: FormControl;

  @Input()
  required: boolean;

  @Input()
  closeAfterValueSelected: boolean = false;

  @Input()
  hideItemSelected: boolean = false;

  @Input()
  isWithLink: boolean = false;

  @Input()
  tooltipNavigateToTranslate: string;

  @Input()
  prefixIcon: ExtendEnum<IconNamePartialEnum>;

  selectedItems: TResource[] = [];
  removable: boolean = true;
  classInputHide: string;

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  get isOverlayOpen(): boolean {
    return this._overlayRefs?.size > 0;
  }

  @Input()
  hintToTranslate: string;

  @Input("overlayPositionStrategy")
  overlayPositionStrategy: ((element: ElementRef, overlay: Overlay) => FlexibleConnectedPositionStrategy) | undefined;

  @Input("overlayPosition")
  overlayPosition: OverlayPositionEnum = OverlayPositionEnum.bottom;

  @Input()
  showEmptyRequiredFieldInError: boolean = this.displayEmptyRequiredFieldInError;

  @ViewChild("inputElementRef")
  readonly input: ElementRef;

  protected readonly _valueBS: BehaviorSubject<TResource[] | undefined> = new BehaviorSubject<TResource[] | undefined>(undefined);
  @Output("valueChange")
  readonly valueObs: Observable<TResource[] | undefined> = ObservableUtil.asObservable(this._valueBS);

  protected readonly _navigateBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("navigate")
  readonly navigateObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._navigateBS);

  ngOnInit(): void {
    if (isNotNullNorUndefined(this.formControl)) {
      this.subscribe(this.formControl.statusChanges.pipe(
        distinctUntilChanged(),
        filter(status => status === SOLIDIFY_CONSTANTS.FORM_STATUS_INVALID),
        tap(status => {
          setTimeout(() => {
            this._changeDetector.detectChanges();
          });
        }),
      ));
    }
    this._manageInitExistingValue();
  }

  ngOnDestroy(): void {
    this.closeOverlays();
    super.ngOnDestroy();
  }

  propagateChange: (__: any) => void = (__: any) => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(value: string[]): void {
    this.orderSelectedItemByValue(value);
  }

  private orderSelectedItemByValue(value: string[]): void {
    if (isNullOrUndefined(value) || isEmptyArray(value)) {
      return;
    }
    this.selectedItems = this.selectedItems.sort((a, b) => value.indexOf(a.resId) - value.indexOf(b.resId));
  }

  openOverlay(inputElementRef: Element): void {
    this._elementToReFocus = inputElementRef;
    const minWidthField = this._elementRef.nativeElement.getBoundingClientRect().width;
    const overlayRef = this._overlay.create({
      positionStrategy: this._getPosition(this._elementRef),
      scrollStrategy: this._overlay.scrollStrategies.block(),
      hasBackdrop: true,
      backdropClass: "cdk-overlay-transparent-backdrop",
      width: minWidthField,
    });

    this._overlayRefs.add(overlayRef);

    const overlayContentComponent = this.getComponentPortal();
    const componentRef = overlayRef.attach(overlayContentComponent);
    componentRef.instance.host = this;

    this._observeValueSelectedInOverlay(componentRef, overlayRef);
    this._observeCloseEventInOverlay(componentRef, overlayRef);
    this._observeClickBackdrop(componentRef, overlayRef);
  }

  protected _observeValueSelectedInOverlay(componentRef: ComponentRef<SearchableTreeMultiSelectContentPresentational<TResource>>,
                                           overlayRef: OverlayRef): void {
    this.subscribe(componentRef.instance.valueAddedObs.pipe(
      tap(value => {
        this.add(value);
        this.extraBehaviorOnValueSelectedInOverlay(value);
        this.formControl.markAsTouched();
        if (this.closeAfterValueSelected) {
          this._closeOverlay(overlayRef);
          this._tabulationService.focusNext(this._elementToReFocus);
        } else {
          overlayRef.updatePosition();
        }
      }),
    ));

    this.subscribe(componentRef.instance.valueRemovedObs.pipe(
      tap(value => {
        this.remove(value);
        this.formControl.markAsTouched();
        if (this.closeAfterValueSelected) {
          this._closeOverlay(overlayRef);
          this._tabulationService.focusNext(this._elementToReFocus);
        } else {
          overlayRef.updatePosition();
        }
      }),
    ));
  }

  private _observeCloseEventInOverlay(componentRef: ComponentRef<AbstractContentPresentational<TResource>>, overlayRef: OverlayRef): void {
    this.subscribe(componentRef.instance.closeObs.pipe(
      tap(() => {
        this._closeOverlay(overlayRef);
        this._tabulationService.focusNext(this._elementToReFocus);
      }),
    ));
  }

  private _observeClickBackdrop(componentRef: ComponentRef<AbstractContentPresentational<TResource>>, overlayRef: OverlayRef): void {

    this.subscribe(overlayRef.backdropClick().pipe(
      tap(() => {
        this._closeOverlay(overlayRef);
        this._tabulationService.focusNext(this._elementToReFocus);
      }),
    ));
    this.subscribe(componentRef.instance.closeByTabObs.pipe(
      tap(() => {
          this._closeOverlay(overlayRef);
          this._tabulationService.focusNext(this._elementToReFocus);
        },
      )));
    this.subscribe(componentRef.instance.closeByShiftTabObs.pipe(
      tap(() => {
          this._closeOverlay(overlayRef);
          this._tabulationService.focusPrev(this._elementToReFocus);
        },
      )));
  }

  private _closeOverlay(overlayRef: OverlayRef, keepInOverlayRefs?: boolean): void {
    overlayRef.detach();
    if (!keepInOverlayRefs) {
      this._overlayRefs.delete(overlayRef);
    }
    this.formControl.markAsTouched();
    this._changeDetector.detectChanges();
  }

  private _getPosition(elementToConnectTo: ElementRef): FlexibleConnectedPositionStrategy {
    if (isFunction(this.overlayPositionStrategy)) {
      return this.overlayPositionStrategy(elementToConnectTo, this._overlay);
    }
    return OverlayHelper.getFlexibleConnectedPositionStrategy(this._overlay, elementToConnectTo, this.overlayPosition);
  }

  focusInputIfNotClickTarget($event: MouseEvent): void {
    if (!isNullOrUndefined(this.input) && $event.target !== this.input.nativeElement) {
      this.input.nativeElement.focus();
    }
  }

  add(item: TResource): void {
    if (!isNullOrUndefined(this.searchItemInSelectedItems(item))) {
      return;
    }
    this.selectedItems.push(item);
    this._changeDetector.detectChanges();
    this._updateFormControlWithSelectedItem();
  }

  remove(item: TResource): void {
    const index = this.searchIndexItemInSelectedItems(item);
    if (index === -1) {
      return;
    }
    this.selectedItems.splice(index, 1);
    this._updateFormControlWithSelectedItem();
    this._changeDetector.detectChanges();
  }

  private _updateFormControlWithSelectedItem(): void {
    const arrayKeys = this.selectedItems.map(c => c[this.valueKey]);
    this.formControl.setValue(arrayKeys);
    this.propagateChange(arrayKeys);
    this._valueBS.next(arrayKeys);
  }

  protected searchItemInSelectedItemsWithId(id: string): TResource | undefined {
    const index = this.searchIndexItemInSelectedItems({[this.valueKey]: id} as any);
    if (index === -1) {
      return this.selectedItems[index];
    }
  }

  private searchItemInSelectedItems(item: TResource): TResource | undefined {
    const index = this.searchIndexItemInSelectedItems(item);
    if (index === -1) {
      return undefined;
    }
    return this.selectedItems[index];
  }

  private searchIndexItemInSelectedItems(item: TResource): number {
    return this.selectedItems.findIndex(s => s[this.valueKey] === item[this.valueKey]);
  }

  getTooltip(value: TResource): string {
    let extraInfo = StringUtil.stringEmpty;
    if (!isNullOrUndefined(this.extraInfoLabelKey) && !isNullOrUndefined(value[this.extraInfoLabelKey])) {
      extraInfo = " - " + value[this.extraInfoLabelKey];
    }
    let label = this.labelCallback(value);
    if (this.translateLabel) {
      label = this._translateService.instant(label);
    }
    return label + extraInfo;
  }

  navigateTo(event: Event, value: TResource): void {
    if (isNotNullNorUndefined(event)) {
      event.stopPropagation();
      event.preventDefault();
    }
    this._navigateBS.next(value);
  }

  copyToClipboard(value: string, $event?: Event): void {
    if (isNotNullNorUndefined($event)) {
      $event.stopPropagation();
    }
    ClipboardUtil.copyStringToClipboard(value);
    this._notificationService.showInformation(this.labelTranslate.inputMultiSelectNotificationExtraInfoCopyToClipboard);
  }

  closeOverlays(): void {
    this._overlayRefs.forEach(overlayRef => this._closeOverlay(overlayRef, true));
    this._overlayRefs.clear();
  }

  protected _manageInitExistingValue(): void {
    const keys = this.formControl.value;
    if (isNonEmptyArray(keys)) {
      this._getLabelInitialSelectedValue();
    } else {
      this.labelInitialized = true;
    }
  }

  private _getLabelInitialSelectedValue(): void {
    if (isNullOrUndefined(this.list) || isEmptyArray(this.list)) {
      return;
    }
    const keys = this.formControl.value;
    keys.forEach(key => {
      const elementAlreadyPresent = this.selectedItems.find(i => i[this.valueKey] === key);
      if (!isNullOrUndefined(elementAlreadyPresent)) {
        return;
      }
      const element = this.list.find(i => i[this.valueKey] === key);
      if (isNullOrUndefined(element)) {
        // Element with key ${key} not found in list for searchable-multi-select '${this.title}'. Wait next list update
        return;
      }
      this.selectedItems.push(element);
    });

    if (this.selectedItems.length === this.formControl.value.length) {
      this.labelInitialized = true;
      this._changeDetector.detectChanges();
    }
  }

  protected extraBehaviorOnValueSelectedInOverlay(value: TResource): void {
    if (isNotNullNorUndefined(this.storeLastSelectionKey)) {
      LocalStorageHelper.addItemInList(this.storeLastSelectionKey, value[this.valueKey], this.MAX_ITEM_TO_DISPLAY_IN_LAST_SELECTION + 1);
    }
  }

  trackByFn(index: number, item: TResource): string {
    return item.resId;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  search: string = "search";
  value: string = "value";
}
