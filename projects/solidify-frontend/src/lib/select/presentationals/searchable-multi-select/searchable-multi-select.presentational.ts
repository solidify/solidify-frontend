/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - searchable-multi-select.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Overlay} from "@angular/cdk/overlay";
import {ComponentPortal} from "@angular/cdk/portal";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormBuilder,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {
  FloatLabelType,
  MatFormFieldAppearance,
  SubscriptSizing,
} from "@angular/material/form-field";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  takeWhile,
  tap,
} from "rxjs/operators";
import {
  isEmptyArray,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isTrue,
  Type,
} from "../../../core-resources/tools/is/is.tool";
import {MappingObject} from "../../../core-resources/types/mapping-type.type";
import {StringUtil} from "../../../core-resources/utils/string.util";
import {AbstractOverlayPresentational} from "../../../core/components/presentationals/abstract-overlay/abstract-overlay.presentational";
import {LocalStoragePartialEnum} from "../../../core/enums/partial/local-storage-partial.enum";
import {DefaultSolidifyEnvironment} from "../../../core/environments/environment.solidify-defaults";
import {LocalStorageHelper} from "../../../core/helpers/local-storage.helper";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../../core/injection-tokens/label-to-translate.injection-token";
import {BaseResourceType} from "../../../core-resources/models/dto/base-resource.model";
import {BaseFormDefinition} from "../../../core/models/forms/base-form-definition.model";
import {Sort} from "../../../core-resources/models/query-parameters/sort.model";
import {NotificationService} from "../../../core/services/notification.service";
import {TabulationService} from "../../../core/services/tabulation.service";
import {ResourceNameSpace} from "../../../core/stores/abstract/resource/resource-namespace.model";
import {ResourceStateModel} from "../../../core/stores/abstract/resource/resource-state.model";
import {ResourceState} from "../../../core/stores/abstract/resource/resource.state";
import {ExtendEnum} from "../../../core-resources/types/extend-enum.type";
import {ObservableUtil} from "../../../core/utils/observable.util";
import {MemoizedUtil} from "../../../core/utils/stores/memoized.util";
import {LabelTranslateInterface} from "../../../label-translate-interface.model";
import {AbstractMultiSelectPresentational} from "../abstract-multi-select/abstract-multi-select.presentational";
import {SearchableMultiSelectContentPresentational} from "../searchable-multi-select-content/searchable-multi-select-content.presentational";

@Component({
  selector: "solidify-searchable-multi-select",
  templateUrl: "./searchable-multi-select.presentational.html",
  styleUrls: ["./searchable-multi-select.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SearchableMultiSelectPresentational,
    },
  ],
})
export class SearchableMultiSelectPresentational<TStateModel extends ResourceStateModel<TResource>, TResource extends BaseResourceType> extends AbstractMultiSelectPresentational<TResource> implements ControlValueAccessor, OnInit {
  @Input()
  resourceNameSpace: ResourceNameSpace;

  @Input()
  state: Type<ResourceState<TStateModel, TResource>>;

  @Input()
  extraSearchQueryParam: MappingObject<string, string>;

  @Input()
  extraInfoImageOverrideMethod: boolean;

  @Input()
  extraInfoPlaceholderCallback: ((resource: TResource) => string) | undefined = undefined;

  protected readonly _extraInfoCallbackBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("extraInfoCallback")
  readonly extraInfoCallbackObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._extraInfoCallbackBS);

  @Input()
  searchKey: string;

  @Input()
  sort: Sort | undefined;

  @Input()
  extraGetAllParameters: any[] = [];

  @ViewChild("inputElementRef", {static: true})
  inputElementRef: ElementRef;

  @Input()
  storeLastSelectionKey: ExtendEnum<LocalStoragePartialEnum> | undefined = undefined;

  @Input()
  appearance: MatFormFieldAppearance = this.appearanceInputMaterial;

  @Input()
  positionLabel: FloatLabelType = this.positionLabelInputMaterial;

  @Input()
  subscriptSizing: SubscriptSizing = this.subscriptSizingInputMaterial;

  classMatFormField: string;
  MAX_ITEM_TO_DISPLAY_IN_LAST_SELECTION: number = 3;

  private _isDisabled: boolean = false;

  @Input()
  set isDisabled(value: boolean) {
    this._isDisabled = value;
    if (this._isDisabled) {
      this.classMatFormField = this.classInputIgnored;
    } else {
      this.classMatFormField = StringUtil.stringEmpty;
    }
  }

  get isDisabled(): boolean {
    return this._isDisabled;
  }

  @Input()
  displayCommonValuesListKey: string[] = [];

  @Input()
  displayCommonValuesLabel: string;

  @Input()
  defaultValueToHighlight: string[] | undefined;

  @Input()
  defaultValueLabel: string;

  @Input("overlayComponent")
  overlayComponent: AbstractOverlayPresentational<TResource>;

  @Input("overlayExtra")
  overlayExtra: any;

  @Input()
  getByListId: (listResId: string[]) => void = listResId => new this.resourceNameSpace.GetByListId(listResId);

  @Input()
  preTreatmentHighlightText: undefined | ((result: string) => string);

  @Input()
  postTreatmentHighlightText: undefined | ((result: string, resultBeforePreTreatment: string) => string);

  listObs: Observable<TResource[]>;
  isLoadingObs: Observable<boolean>;

  labelInitialized: boolean = false;
  isInitialized: boolean = false;

  classInputIgnored: string;

  isADefaultValue(resId: string): boolean {
    if (isNonEmptyArray(this.defaultValueToHighlight)) {
      const index = this.defaultValueToHighlight.findIndex(defaultResId => defaultResId === resId);
      return index !== -1;
    }
    return false;
  }

  constructor(protected readonly _fb: FormBuilder,
              protected readonly _elementRef: ElementRef,
              protected readonly _viewContainerRef: ViewContainerRef,
              protected readonly _overlay: Overlay,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _notificationService: NotificationService,
              protected readonly _tabulationService: TabulationService,
              @Inject(ENVIRONMENT) readonly environment: DefaultSolidifyEnvironment,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface,
              private readonly _store: Store,
              protected readonly _translateService: TranslateService) {
    super(_fb,
      _elementRef,
      _viewContainerRef,
      _overlay,
      _changeDetector,
      _notificationService,
      _tabulationService,
      environment,
      labelTranslate,
      _translateService);
    if (!isNonEmptyString(this.defaultValueToHighlight)) {
      this.defaultValueLabel = this.labelTranslate.inputMultiSelectDefaultValue;
    }
    this.displayCommonValuesLabel = this.labelTranslate.selectMostCommonValues;
    this.classInputIgnored = this.environment.classInputIgnored;

  }

  getComponentPortal(): ComponentPortal<SearchableMultiSelectContentPresentational<TResource>> {
    return new ComponentPortal<SearchableMultiSelectContentPresentational<TResource>>(SearchableMultiSelectContentPresentational);
  }

  ngOnInit(): void {
    if (this.isInitialized) {
      return;
    }
    super.ngOnInit();
    // this._managePostExternalChange();
    this.isInitialized = true;
    if (this.isDisabled) {
      this.isDisabled = true;
    }
  }

  private _managePostExternalChange(): void {
    this.subscribe(this.formControl.valueChanges.pipe(
      distinctUntilChanged(),
      tap((values: string[]) => {
        values.forEach(v => {
          if (!isNullOrUndefined(this.searchItemInSelectedItemsWithId(v))) {
            return;
          }
          // TODO manage here external add with id (need to retrieve full object in backend)
        });
      }),
    ));
  }

  protected _manageInitExistingValue(): void {
    const keys = this.formControl.value;
    if (isNonEmptyArray(keys)) {
      this._dispatchGetActionToRetrieveLabel(keys);
      this._getLabelInitialSelectedValue();
    } else {
      this.labelInitialized = true;
    }
  }

  private _dispatchGetActionToRetrieveLabel(listResId: string[]): void {
    this.listObs = MemoizedUtil.list(this._store, this.state);
    this.isLoadingObs = MemoizedUtil.isLoading(this._store, this.state);
    this._store.dispatch(this.getByListId(listResId));
  }

  private _getLabelInitialSelectedValue(): void {
    this.subscribe(this.listObs.pipe(
      distinctUntilChanged(),
      filter((list: TResource[]) => !isNullOrUndefined(list) && !isEmptyArray(list)),
      takeWhile((list: TResource[]) => this.selectedItems.length < this.formControl.value.length),
      tap((list) => {
        const keys = this.formControl.value;
        keys.forEach(key => {
          const elementAlreadyPresent = this.selectedItems.find(i => i[this.valueKey] === key);
          if (!isNullOrUndefined(elementAlreadyPresent)) {
            return;
          }
          const element = list.find(i => i[this.valueKey] === key);
          if (isNullOrUndefined(element)) {
            // Element with key ${key} not found in list for searchable-multi-select '${this.title}'. Wait next list update
            return;
          }
          this.selectedItems.push(element);
        });

        if (this.selectedItems.length === this.formControl.value.length) {
          this.labelInitialized = true;
          this._changeDetector.detectChanges();
        }
      }),
    ));
  }

  protected extraBehaviorOnValueSelectedInOverlay(value: TResource): void {
    if (isNotNullNorUndefined(this.storeLastSelectionKey)) {
      LocalStorageHelper.addItemInList(this.storeLastSelectionKey, value[this.valueKey], this.MAX_ITEM_TO_DISPLAY_IN_LAST_SELECTION + 1);
    }
  }

  clickExtraInfo(value: TResource, $event: Event): void {
    if (isTrue(this.extraInfoImageOverrideMethod)) {
      this._extraInfoCallbackBS.next(value);
    } else {
      this.copyToClipboard(value[this.extraInfoLabelKey], $event);
    }
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  search: string = "search";
  value: string = "value";
}
