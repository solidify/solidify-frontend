/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - searchable-single-select-content.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */


import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ElementRef,
  Inject,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {DefaultSolidifyEnvironment} from "../../../core/environments/environment.solidify-defaults";
import {ENVIRONMENT} from "../../../core/injection-tokens/environment.injection-token";
import {LABEL_TRANSLATE} from "../../../core/injection-tokens/label-to-translate.injection-token";
import {BaseResourceType} from "../../../core-resources/models/dto/base-resource.model";
import {LabelTranslateInterface} from "../../../label-translate-interface.model";
import {ResourceStateModel} from "../../../core/stores/abstract/resource/resource-state.model";
import {
  isFalse,
  isNullOrUndefined,
} from "../../../core-resources/tools/is/is.tool";
import {ObservableUtil} from "../../../core/utils/observable.util";
import {
  SearchableAbstractContentModeEnum,
  SearchableAbstractContentPresentational,
} from "../searchable-abstract-content/searchable-abstract-content.presentational";
import {SearchableSingleSelectPresentational} from "../searchable-single-select/searchable-single-select.presentational";

@Component({
  selector: "solidify-searchable-single-select-content",
  templateUrl: "../searchable-abstract-content/searchable-abstract-content.presentational.html",
  styleUrls: ["./searchable-single-select-content.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchableSingleSelectContentPresentational<TResource extends BaseResourceType> extends SearchableAbstractContentPresentational<TResource> implements OnInit {
  mode: SearchableAbstractContentModeEnum = SearchableAbstractContentModeEnum.single;

  @Input()
  host: SearchableSingleSelectPresentational<ResourceStateModel<TResource>, TResource>;

  private readonly _valueBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("valueChange")
  readonly valueObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._valueBS);

  constructor(protected readonly _store: Store,
              protected readonly _fb: FormBuilder,
              protected readonly _elementRef: ElementRef,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _actions$: Actions,
              @Inject(ENVIRONMENT) readonly environment: DefaultSolidifyEnvironment,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface,
              protected readonly _componentFactoryResolver: ComponentFactoryResolver) {
    super(_store,
      _fb,
      _elementRef,
      _changeDetector,
      _actions$,
      environment,
      _componentFactoryResolver);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  select(value: TResource, force: boolean = false): void {
    if (isNullOrUndefined(value)) {
      return;
    }
    this.host.isDefaultValueSelected = false;
    if (this.isActive(value) && isFalse(force)) {
      return;
    }
    this._valueBS.next(value);
  }
}
