/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - searchable-tree-single-select-content.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {FlatTreeControl} from "@angular/cdk/tree";
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
} from "@angular/forms";
import {
  MatTreeFlatDataSource,
  MatTreeFlattener,
} from "@angular/material/tree";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  isEmptyArray,
  isEmptyString,
  isFunction,
  isNonEmptyString,
  isNonWhiteString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
} from "../../../core-resources/tools/is/is.tool";
import {MappingObject} from "../../../core-resources/types/mapping-type.type";
import {MappingObjectUtil} from "../../../core-resources/utils/mapping-object.util";
import {StringUtil} from "../../../core-resources/utils/string.util";
import {AbstractContentPresentational} from "../../../core/components/presentationals/abstract-content/abstract-content.presentational";
import {SOLIDIFY_CONSTANTS} from "../../../core-resources/constants";
import {LocalStorageHelper} from "../../../core/helpers/local-storage.helper";
import {LABEL_TRANSLATE} from "../../../core/injection-tokens/label-to-translate.injection-token";
import {BaseResourceType} from "../../../core-resources/models/dto/base-resource.model";
import {ObservableUtil} from "../../../core/utils/observable.util";
import {LabelTranslateInterface} from "../../../label-translate-interface.model";
import {SearchableTreeSingleSelectPresentational} from "../searchable-tree-single-select/searchable-tree-single-select.presentational";

@Component({
  selector: "solidify-searchable-tree-single-select-content",
  templateUrl: "./searchable-tree-single-select-content.presentational.html",
  styleUrls: ["./searchable-tree-single-select-content.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchableTreeSingleSelectContentPresentational<TResource extends BaseResourceType> extends AbstractContentPresentational<TResource> implements OnInit, AfterViewInit {
  form: FormGroup;

  list: TResource[];

  listLastSelection: TResource[] = [];

  listCommonValues: TResource[] = [];

  @Input()
  host: SearchableTreeSingleSelectPresentational<TResource>;

  protected readonly _debounceTime: number = 250;

  private readonly _valueAddedBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("valueAddedChange")
  readonly valueAddedObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._valueAddedBS);

  private readonly _valueRemovedBS: BehaviorSubject<TResource | undefined> = new BehaviorSubject<TResource | undefined>(undefined);
  @Output("valueRemovedChange")
  readonly valueRemovedObs: Observable<TResource | undefined> = ObservableUtil.asObservable(this._valueRemovedBS);

  constructor(protected readonly _fb: FormBuilder,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface) {
    super();
  }

  originalTree: ResourceNode<TResource>[] = [];

  ngOnInit(): void {
    super.ngOnInit();

    this._initSearchForm();
    this._observeSearchInput();

    this._manageCommonValue();
    this._manageLastSelection();

    this.originalTree = this._createDataTree(this.host.list);
    this.dataSourceFullData.data = [...this.originalTree];
    this.dataSource.data = [...this.originalTree];

    this._showSelectedNode();
  }

  private _showSelectedNode(): void {
    if (isNullOrUndefinedOrWhiteString(this.host.formControl.value)) {
      return;
    }
    const node = this.treeControl.dataNodes.find(c => c.resource[this.host.valueKey] === this.host.formControl.value);
    this._recursivelyExpandParent(node);
  }

  computeHighlightText(valueElement: string): string {
    const fc = this.form.get(this.host.formDefinition.search);
    if (isNullOrUndefined(fc)) {
      return valueElement;
    }
    let value: string = fc.value;
    if (isNullOrUndefined(value) || isEmptyString(value)) {
      return valueElement;
    }
    value = value.trim()
      .replace(SOLIDIFY_CONSTANTS.COMMA, SOLIDIFY_CONSTANTS.STRING_EMPTY)
      .replace(SOLIDIFY_CONSTANTS.SEMICOLON, SOLIDIFY_CONSTANTS.STRING_EMPTY);
    let listTerms = value.split(SOLIDIFY_CONSTANTS.SPACE);
    listTerms = listTerms.filter(t => isNonWhiteString(t));
    let result = valueElement;
    if (isFunction(this.host.preTreatmentHighlightText)) {
      result = this.host.preTreatmentHighlightText(result);
    }
    listTerms.forEach(term => {
      result = result.replace(new RegExp(term, "gi"), match => `{{${match}}}`);
    });
    result = StringUtil.replaceAll(result, "{{", `<span class="highlight-text">`);
    result = StringUtil.replaceAll(result, "}}", `</span>`);
    if (isFunction(this.host.postTreatmentHighlightText)) {
      result = this.host.postTreatmentHighlightText(result, valueElement);
    }
    return result;
  }

  sortLastResultAlphabetically(a: TResource, b: TResource, key: string): number {
    if (a[key] < b[key]) {
      return -1;
    }
    if (a[key] > b[key]) {
      return 1;
    }
    return 0;
  }

  private _manageCommonValue(): void {
    if (isNullOrUndefined(this.host.displayCommonValuesListKey)) {
      return;
    }
    const listCommonValuesId = [...this.host.displayCommonValuesListKey];
    listCommonValuesId.forEach(id => {
      const resource = this.host.list.find(r => r[this.host.valueKey] === id);
      this.listCommonValues.push(resource);
      this.listCommonValues.sort((a, b) => this.sortLastResultAlphabetically(a, b, this.host.labelKey));
    });
  }

  private _manageLastSelection(): void {
    if (isNullOrUndefined(this.host.storeLastSelectionKey)) {
      return;
    }

    const listLastSelectionId = LocalStorageHelper.getListItem(this.host.storeLastSelectionKey);
    listLastSelectionId.forEach(id => {
      const resource = this.host.list.find(r => r[this.host.valueKey] === id);
      this.listLastSelection.push(resource);
      this.listLastSelection.sort((a, b) => this.sortLastResultAlphabetically(a, b, this.host.labelKey));
    });
  }

  isActiveSearch(): boolean {
    return isNonEmptyString(this.form.get(this.host.formDefinition.search).value);
  }

  private _initSearchForm(): void {
    this.form = this._fb.group({
      [this.host.formDefinition.search]: [""],
    });
  }

  select(value: TResource, force: boolean = false): void {
    if (isNullOrUndefined(value)) {
      return;
    }
    if (this.isActive(value)) {
      this._valueRemovedBS.next(value);
      return;
    }
    this._valueAddedBS.next(value);
  }

  private _observeSearchInput(): void {
    this.subscribe(this.form.get(this.host.formDefinition.search).valueChanges.pipe(
      debounceTime(this._debounceTime),
      distinctUntilChanged(),
      tap(filterText => {
        let filteredTreeData;
        if (filterText) {
          // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
          const filter = (array: ResourceNode<TResource>[], text: string) => {
            // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
            const getChildren = (result, object) => {
              if (object[this.host.labelKey].toLowerCase().indexOf(text.toLowerCase()) !== -1) {
                result.push(object);
                return result;
              }
              if (Array.isArray(object.children)) {
                const children = object.children.reduce(getChildren, []);
                if (children.length) {
                  result.push({...object, children});
                }
              }
              return result;
            };

            return array.reduce(getChildren, []);
          };

          filteredTreeData = filter(this.originalTree, filterText);
        } else {
          // Return the initial tree
          filteredTreeData = this.originalTree;
        }

        this.dataSource.data = [...filteredTreeData];
        this.filterChanged(filterText);
      }),
    ));
  }

  private _transformer: (node: ResourceNode<TResource>, level: number) => ResourceFlatNode<TResource> = (node: ResourceNode<TResource>, level: number) =>
    ({
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
      resId: node.resId,
      resource: node.resource,
    } as ResourceFlatNode<TResource>);

  treeControlFullData: FlatTreeControl<ResourceFlatNode<TResource>> = new FlatTreeControl<ResourceFlatNode<TResource>>(node => node.level, node => node.expandable);
  treeControl: FlatTreeControl<ResourceFlatNode<TResource>> = new FlatTreeControl<ResourceFlatNode<TResource>>(node => node.level, node => node.expandable);
  treeFlattener: MatTreeFlattener<ResourceNode<TResource>, ResourceFlatNode<TResource>> = new MatTreeFlattener(this._transformer, node => node.level, node => node.expandable, node => node.children);
  dataSourceFullData: MatTreeFlatDataSource<ResourceNode<TResource>, ResourceFlatNode<TResource>> = new MatTreeFlatDataSource(this.treeControlFullData, this.treeFlattener);
  dataSource: MatTreeFlatDataSource<ResourceNode<TResource>, ResourceFlatNode<TResource>> = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  getLevel: (node: ResourceFlatNode<TResource>) => number = (node: ResourceFlatNode<TResource>) => node.level;

  hasChild: (_: number, _nodeData: ResourceFlatNode<TResource>) => boolean = (_: number, _nodeData: ResourceFlatNode<TResource>) => _nodeData.expandable;

  /** Whether all the descendants of the node are selected. */
  private _descendantsAllSelected(node: ResourceFlatNode<TResource>): boolean {
    const descendants = this._getDescendants(node);
    return descendants.every(child => this.isCheckedNode(child));
  }

  /** Whether part of the descendants are selected */
  isDescendantsPartiallySelected(node: ResourceFlatNode<TResource>): boolean {
    const descendants = this._getDescendants(node);
    const result = descendants.some(child => this.isCheckedNode(child));
    return result && !this._descendantsAllSelected(node);
  }

  private _getDescendants(node: ResourceFlatNode<TResource>): ResourceFlatNode<TResource>[] {
    const nodeInFullData = this.treeControlFullData.dataNodes.find(n => n.resId === node.resId);
    return this.treeControlFullData.getDescendants(nodeInFullData);
  }

  toggleTreeElement(node: ResourceFlatNode<TResource>): void {
    let shouldClose = false;
    this.select(node.resource);

    if (this.isCheckedNode(node)) {
      shouldClose = true;
      this._recursivelyUnselectParent(node);
    }

    const descendants = this._getDescendants(node);
    if (isEmptyArray(descendants)) {
      // is leaf node
      if (shouldClose) {
        this.close();
      }
      return;
    }

    if (isTrue(this.host.expandNodeWhenChecked) && !this.treeControl.isExpanded(node) && this.isActive(node.resource)) {
      this.treeControl.expand(node);
    }

    descendants.forEach(d => {
      if (this.isCheckedNode(d)) {
        this._valueRemovedBS.next(d.resource);
      }
    });

    // Force update for the parent
    descendants.every(child => this.isCheckedNode(node));
    this._checkAllParentsSelection(node);
    if (shouldClose) {
      this.close();
    }
  }

  private _recursivelyExpandParent(currentNode: ResourceFlatNode<TResource>): void {
    const parent = this._getParentNode(currentNode);

    if (isNotNullNorUndefined(parent)) {
      this.treeControl.expand(parent);
      this._recursivelyExpandParent(parent);
    }
  }

  private _recursivelyUnselectParent(currentNode: ResourceFlatNode<TResource>): void {
    const parent = this._getParentNode(currentNode);
    if (isNotNullNorUndefined(parent)) {
      if (this.isCheckedNode(parent)) {
        this._valueRemovedBS.next(parent.resource);
      }
      this._recursivelyUnselectParent(parent);
    }
  }

  /** Checks all the parents when a leaf node is selected/unselected */
  private _checkAllParentsSelection(node: ResourceFlatNode<TResource>): void {
    let parent: ResourceFlatNode<TResource> | null = this._getParentNode(node);
    while (parent) {
      this._checkRootNodeSelection(parent);
      parent = this._getParentNode(parent);
    }
  }

  /** Check root node checked state and change it accordingly */
  private _checkRootNodeSelection(node: ResourceFlatNode<TResource>): void {
    const nodeSelected = this.isCheckedNode(node);
    const descendants = this._getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.isCheckedNode(child),
    );
    if (nodeSelected && !descAllSelected) {
      this._valueRemovedBS.next(node.resource);
    } else if (!nodeSelected && descAllSelected) {
      this._valueAddedBS.next(node.resource);
    }
  }

  private _getParentNode(node: ResourceFlatNode<TResource>): ResourceFlatNode<TResource> | null {
    const currentLevel = this.getLevel(node);

    if (currentLevel < 1) {
      return null;
    }

    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;

    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];

      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }

  filterChanged(filterText: string): void {
    if (filterText) {
      this.treeControl.expandAll();
    } else {
      this.treeControl.collapseAll();
    }
  }

  private _createDataTree(list: TResource[]): ResourceNode<TResource>[] {
    if (isNullOrUndefined(list)) {
      return [];
    }

    const map = {} as MappingObject<string, ResourceNode<TResource>>;

    list.forEach(resource => MappingObjectUtil.set(map, resource[this.host.valueKey], {
      resId: resource[this.host.valueKey],
      name: this.host.labelCallback(resource),
      parentId: resource?.[this.host.valueKey],
      children: [],
      resource: resource,
    }));

    const tree: ResourceNode<TResource>[] = [];

    list.forEach(resource => {
      if (resource[this.host.parentKey]?.[this.host.valueKey]) {
        MappingObjectUtil.get(map, resource[this.host.parentKey][this.host.valueKey])
          ?.children
          .push(MappingObjectUtil.get(map, resource[this.host.valueKey]));
      } else {
        tree.push(MappingObjectUtil.get(map, resource[this.host.valueKey]));
      }
    });

    return tree;
  }

  isCheckedNode(node: ResourceNode<TResource>): boolean {
    return this.isActive(node.resource);
  }

  selectLastSelectionOrCommonValue(value: TResource): void {
    const node = this.treeControl.dataNodes.find(c => c.resId === value[this.host.valueKey]);
    this.toggleTreeElement(node);
  }
}

class ResourceFlatNode<TResource> {
  resId: string;
  name: string;
  expandable: boolean;
  level: number;
  parent: ResourceFlatNode<TResource>;
  resource?: TResource;
}

class ResourceNode<TResource> {
  resId: string;
  name: string;
  children?: ResourceNode<TResource>[];
  parentId?: string;
  resource?: TResource;
}

