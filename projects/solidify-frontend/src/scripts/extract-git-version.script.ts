/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - extract-git-version.script.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

// FILE FROM SOLIDIFY, DO NOT EDIT IF IT'S COPIED IN YOUR PROJECT OR YOU WILL LOSE YOUR CHANGES

/* eslint-disable no-console,solidify/copyright-header */
import {
  promises as fsPromises,
  writeFileSync,
} from "fs";
import {parseString} from "xml2js";

const util = require("util");
const exec = util.promisify(require("child_process").exec);

const createVersionsFile = async (filename: string): Promise<void> => {
  let version = undefined;
  fsPromises.readFile("./pom.xml", "utf-8")
    .then((xml) => {
      parseString(xml, (err, result) => {
        if (err) {
          console.log("There was an error when parsing the file pom.xml to json: " + err);
        } else {
          version = result.project.version;
        }
      });
    })
    .catch((reason) => {
      console.log("There was some problem reading the file pom.xml: " + reason);
    });

  const revision = (await exec("git rev-parse --short HEAD")).stdout.toString().trim();
  const branch = (await exec("git rev-parse --abbrev-ref HEAD")).stdout.toString().trim();
  const date = (await exec("git show -s --date=format:\"%d.%m.%Y %H:%M\" --format=%cd")).stdout.toString().trim();

  console.log(`version: '${version}', revision: '${revision}', branch: '${branch}', date: '${date}'`);

  const content = `{
  "version": "${version}",
  "revision": "${revision}",
  "branch": "${branch}",
  "date": "${date}"
}`;

  writeFileSync(filename, content, {encoding: "utf8"});
};

createVersionsFile("src/assets/frontend-version.json");
