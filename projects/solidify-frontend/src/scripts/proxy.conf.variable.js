exports.shortDoi = (logLevel) => {
    return {
        "/api/short-doi": {
            "target": "https://shortdoi.org",
            "pathRewrite": {
                "^/api/short-doi": ""
            },
            "changeOrigin": true,
            "logLevel": logLevel,
            "logProvider": () => console,
        }
    }
}

exports.oauth = (logLevel, port) => {
    return {
        "/oauth": {
            "target": `http://localhost:${port}`,
            "logLevel": logLevel,
            "logProvider": () => console,
        }
    }
}

exports.shiblogin = (logLevel, port, firstName = "Marty", lastName = "McFly", externalUid = "999999@unige.ch", userMail = `${firstName}.${lastName}@unige.ch`) => {
    return _login(logLevel, "/shiblogin", port, firstName, lastName, externalUid, userMail);
}

exports.shibloginMfa = (logLevel, port, firstName = "Marty", lastName = "McFly", externalUid = "999999@unige.ch", userMail = `${firstName}.${lastName}@unige.ch`) => {
    return _login(logLevel, "/mfa-shiblogin", port, firstName, lastName, externalUid, userMail);
}

_login = (logLevel, route, port, firstName, lastName, externalUid, userMail) => {
    return {
        [route]: {
            "target": `http://localhost:${port}`,
            "onProxyReq": (proxyReq, req, res) => {
                proxyReq.setHeader('mail', userMail);
                proxyReq.setHeader('uniqueid', externalUid);
                proxyReq.setHeader('givenname', firstName);
                proxyReq.setHeader('surname', lastName);
                proxyReq.setHeader('homeorganization', 'unige.ch');
                proxyReq.setHeader('preferredlanguage', 'fr-ch');
            },
            "onProxyRes": (proxyRes, req, res) => {
                proxyRes.headers['Access-Control-Allow-Origin'] = `http://localhost:${port}`;
            },
            "logLevel": logLevel,
            "logProvider": () => console,
        }
    }
}