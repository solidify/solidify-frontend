/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - update-package-lock-version.script.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  existsSync,
  promises as fsPromises,
  writeFileSync,
} from "fs";

export const updatePackageLockVersion = async (packageLockJsonPath: string, newVersion: string, replaceVersion: boolean = false, spaceIndentation: number = 2): Promise<void> => {
  if (!existsSync(packageLockJsonPath)) {
    // file is missing in case of pnpm project
    return Promise.resolve();
  }
  return fsPromises.readFile(packageLockJsonPath, "utf-8")
    .then((jsonString) => {
      const packageJson = JSON.parse(jsonString);
      if (replaceVersion) {
        packageJson.version = newVersion;
        packageJson.packages[""].version = newVersion;
      } else {
        packageJson.version = packageJson.version + newVersion;
        packageJson.packages[""].version = packageJson.packages[""].version + newVersion;
      }
      const content = JSON.stringify(packageJson, null, spaceIndentation);
      writeFileSync(packageLockJsonPath, content, {encoding: "utf8"});
      // eslint-disable-next-line no-console
      console.log("Update " + packageLockJsonPath + " to version '" + packageJson.version + "'");
    })
    .catch((reason) => {
      // eslint-disable-next-line no-console
      console.error("There was some problem reading the file " + packageLockJsonPath + ": " + reason);
    });
};
