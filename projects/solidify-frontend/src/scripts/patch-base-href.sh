#!/bin/sh

echo "=== Start patch base href in index.html with value $2 ==="

# BROWSER_FOLDER : The folder where index.html is present.
# - from VM installation is probably ./ if you are in the folder where this script is
# - from docker-entrypoint.sh is probably /usr/share/nginx/html
BROWSER_FOLDER=$1
# BASE_HREF : The base href to apply.
# - example: /portal/
BASE_HREF=$2
INDEX_FILE=$BROWSER_FOLDER/index.html
NGSW_FILE=$BROWSER_FOLDER/ngsw.json

if [ "$BASE_HREF" = "" ]; then
    echo "Standard base href '/' used."
else

    if [ -f "$INDEX_FILE" ]; then
        echo "$INDEX_FILE exists."

        if grep -q "<base href=\"" $INDEX_FILE; then
          baseUrl=$(echo "/$BASE_HREF/" | sed s#//*#/#g)
          sed -i -e 's|\(base href\)="[^"]*"|\1="'"$baseUrl"'"|' $INDEX_FILE
          echo "Base href in '$INDEX_FILE' updated in '$BASE_HREF'"
          gzip -c -f -9 $INDEX_FILE > $INDEX_FILE.gz

          if [ -f "$NGSW_FILE" ]; then
            SHA1_INDEX_FILE=$(sha1sum $INDEX_FILE | awk '{print $1}')
            sed -i -- "s#\"/index.html\": \".*\"#\"/index.html\": \"$SHA1_INDEX_FILE\"#g" $NGSW_FILE
            echo "Entry '/index.html' hash updated in '$NGSW_FILE' with new sha1 value '$SHA1_INDEX_FILE'"
            gzip -c -f -9 $NGSW_FILE > $NGSW_FILE.gz
          else
            echo "$NGSW_FILE does not exist, abort patch ngsw.json."
          fi

        else
          echo "No <base href=\"\"> tag found in this file $INDEX_FILE" 1>&2
          exit 1
        fi

    else
        echo "$INDEX_FILE does not exist, abort patch index."
    fi

fi

echo "=== End patch ==="
