/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - prepare-release.script.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {promises as fsPromises} from "fs";
import * as process from "node:process";
import {generateChangelog} from "./generate-changelog.script";
import {
  checkMandatoryStringParamInEnv,
  isNullOrUndefinedOrWhiteString,
  logParams,
  readPackageJsonFile,
} from "./script-tool.util";
import {updatePackageLockVersion} from "./update-package-lock-version.script";
import {updatePackageVersion} from "./update-package-version.script";

/* eslint-disable no-console */

export const prepareRelease = async (): Promise<void> => {
  console.log("Prepare release - Started");
  const processEnv: ScriptParam = process.env as any;
  const defaultTagPrefix = (await readPackageJsonFile())["default-tag-prefix"];
  logParams(processEnv, [
    "RELEASE_VERSION",
    "TAG_NAME",
    "RELEASE_FROM_TAG_NAME",
  ], (env: ScriptParam) => {
    if (isNullOrUndefinedOrWhiteString(env.TAG_NAME) && !isNullOrUndefinedOrWhiteString(defaultTagPrefix)) {
      processEnv.TAG_NAME = defaultTagPrefix + env.RELEASE_VERSION;
      console.log(`- (Computed) [TAG_NAME]: ${env.TAG_NAME}`);
    }
  });

  checkMandatoryStringParamInEnv(processEnv, "RELEASE_VERSION");
  checkMandatoryStringParamInEnv(processEnv, "TAG_NAME");

  await updatePackageVersion("./package.json", processEnv.RELEASE_VERSION, true);
  await updatePackageLockVersion("./package-lock.json", processEnv.RELEASE_VERSION, true);
  await generateChangelog("./CHANGELOG.md", undefined, processEnv.TAG_NAME);
  await fsPromises.rm("./RELEASE_NOTES.md");
  await generateChangelog("./RELEASE_NOTES.md", processEnv.RELEASE_FROM_TAG_NAME, processEnv.TAG_NAME);

  console.log("Prepare release - Finished");
};

prepareRelease();

interface ScriptParam {
  RELEASE_VERSION: string;
  TAG_NAME: string;
  RELEASE_FROM_TAG_NAME: string;
}
