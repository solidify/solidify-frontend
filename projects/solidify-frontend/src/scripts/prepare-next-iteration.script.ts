/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - prepare-next-iteration.script.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import * as process from "node:process";
import {
  checkMandatoryStringParamInEnv,
  logParams,
} from "./script-tool.util";
import {updatePackageLockVersion} from "./update-package-lock-version.script";
import {updatePackageVersion} from "./update-package-version.script";

/* eslint-disable no-console */

export const prepareNextIteration = async (): Promise<void> => {
  console.log("Prepare next iteration - Started");
  const processEnv: ScriptParam = process.env as any;
  logParams(processEnv, [
    "SNAPSHOT_VERSION",
  ]);

  checkMandatoryStringParamInEnv(processEnv, "SNAPSHOT_VERSION");

  await updatePackageVersion("./package.json", processEnv.SNAPSHOT_VERSION, true);
  await updatePackageLockVersion("./package-lock.json", processEnv.SNAPSHOT_VERSION, true);

  console.log("Prepare next iteration - Finished");
};

prepareNextIteration();

interface ScriptParam {
  SNAPSHOT_VERSION: string;
}
