/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - generate-changelog.script.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {exec} from "child_process";
import {
  existsSync,
  promises as fsPromises,
  writeFileSync,
} from "fs";
import {generate} from "generate-changelog";
import {
  checkMandatoryStringParam,
  isNullOrUndefinedOrWhiteString,
  isValidStringParam,
} from "./script-tool.util";

/* eslint-disable prefer-arrow/prefer-arrow-functions,no-console */

export const generateChangelog = async (targetFile: string, fromTag: string | undefined, toTag: string): Promise<void> => {
  checkMandatoryStringParam(targetFile, "targetFile");
  checkMandatoryStringParam(toTag, "toTag");
  const isToTagExist = await tagExist(toTag);
  const previousTag = await getLastTag();
  if (!isValidStringParam(fromTag)) {
    fromTag = previousTag;
  }
  console.log(`Generating '${targetFile}' from ${fromTag} to ${isToTagExist ? toTag : "now"}`);
  const packageJsonString = await fsPromises.readFile("./package.json", "utf-8");
  const packageJson = JSON.parse(packageJsonString);
  const repositoryUrl = packageJson.repository?.url;

  let changelogJsonString = await generate({
    tag: isToTagExist ? `${fromTag}...${toTag}}` : fromTag,
    exclude: "chore",
  });
  const now = new Date().toISOString().split("T")[0];
  const title = `#### [${toTag}](${repositoryUrl}/compare/${fromTag}...${toTag}) (${now})`;
  const changes = changelogJsonString.substring(changelogJsonString.indexOf("\n"));
  changelogJsonString = title + changes;
  if (existsSync(targetFile)) {
    changelogJsonString = changelogJsonString + await fsPromises.readFile(targetFile, "utf-8");
  }
  writeFileSync(targetFile, changelogJsonString, {encoding: "utf-8"});
};

function tagExist(tag: string): Promise<boolean> {
  return new Promise(resolve => {
    exec(`git tag -l ${tag}`, (error, stdout, stderr) => {
      resolve(stdout.trim().length > 0);
    });
  });
}

function getLastTag(): Promise<string | undefined> {
  return new Promise(resolve => {
    exec(`git describe --tags --abbrev=0`, (error, stdout, stderr) => {
      const foundingTag = stdout;
      if (isNullOrUndefinedOrWhiteString(foundingTag)) {
        return undefined;
      }
      resolve(stdout.trim());
    });
  });
}
