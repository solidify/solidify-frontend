/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Frontend - script-tool.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {promises as fsPromises} from "fs";
import * as process from "node:process";

/* eslint-disable no-console */

/* eslint-disable prefer-arrow/prefer-arrow-functions */

export function checkMandatoryStringParamInEnv(processEnv: any, paramName: string): void {
  const param = processEnv[paramName];
  checkMandatoryStringParam(param, paramName);
}

export function checkMandatoryStringParam(param: string, paramName: string): void {
  if (!isValidStringParam(param)) {
    console.error(`'${paramName}' parameters is missing, command aborted`);
    process.exit(1);
  }
}

export function isValidStringParam(param: string): boolean {
  return param !== null && param !== undefined && typeof param === "string" && param.trim() !== "";
}

export function logParams(processEnv: any, listToLog: string[] = [], computeParametersCallback?: (processEnv: any) => void): void {
  console.log("");
  console.log("Parameters:");
  listToLog.forEach(param => {
    console.log(`- [${param}]: ${processEnv[param]}`);
  });
  if (!isNullOrUndefined(computeParametersCallback)) {
    computeParametersCallback(processEnv);
  }
  console.log("");
}

export function readPackageJsonFile(): Promise<object> {
  return readJsonFile("./package.json");
}

export function readJsonFile(jsonFilePath): Promise<object> {
  return fsPromises.readFile(jsonFilePath, "utf-8")
    .then((jsonString) => JSON.parse(jsonString));
}

export function isNull(value: any): value is null {
  return value === null;
}

export function isUndefined(value: any): value is undefined {
  return typeof value === "undefined";
}

export function isNullOrUndefined(value: any): value is null | undefined {
  return isNull(value) || isUndefined(value);
}

export function isString(value: any): value is string {
  return typeof value === "string";
}

export function isWhiteString(value: any): boolean {
  return isString(value) && value.trim() === "";
}

export function isNullOrUndefinedOrWhiteString(value: any): boolean {
  return isNull(value) || isUndefined(value) || isWhiteString(value);
}
