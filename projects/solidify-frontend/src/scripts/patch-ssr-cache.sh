#!/bin/sh

echo "=== Patch SSR cache ==="

# BROWSER_FOLDER : The folder where index.html is present.
# - from VM installation is probably ./ if you are in the folder where this script is
# - from docker-entrypoint.sh is probably /usr/share/nginx/html
BROWSER_FOLDER=$1
INDEX_FILE=$BROWSER_FOLDER/index.html

echo -e "\n\nPatch SSR cache ONLY if public pages don't change in this new release !\n\n"

if [ -f "$INDEX_FILE" ]; then
  echo "Extract javascript and style hash"

  id_runtime=$(cat $INDEX_FILE | sed -n 's/.*<script src="runtime\.\([a-zA-Z0-9]*\)\.js" type="module"><\/script>.*/\1/p')
  id_polyfills=$(cat $INDEX_FILE | sed -n 's/.*<script src="polyfills\.\([a-zA-Z0-9]*\)\.js" type="module"><\/script>.*/\1/p')
  id_scripts=$(cat $INDEX_FILE | sed -n 's/.*<script src="scripts\.\([a-zA-Z0-9]*\)\.js" defer><\/script>.*/\1/p')
  id_main=$(cat $INDEX_FILE | sed -n 's/.*<script src="main\.\([a-zA-Z0-9]*\)\.js" type="module"><\/script>.*/\1/p')
  id_styles=$(cat $INDEX_FILE | sed -n 's/.*<link rel="stylesheet" href="styles\.\([a-zA-Z0-9]*\)\.css"><\/head>.*/\1/p')

  echo " - runtime: $id_runtime"
  echo " - polyfills: $id_polyfills"
  echo " - scripts: $id_scripts"
  echo " - main: $id_main"
  echo " - styles: $id_styles"

  echo -e "\nRemove already existing folder '.ssr-cache-to-recycle'"

  rm -fr .ssr-cache-to-recycle

  echo "Rename '.ssr-cache' into '.ssr-cache-to-recycle'"

  mv .ssr-cache .ssr-cache-to-recycle

  echo "Create '.ssr-cache' folder"

  mkdir .ssr-cache

  echo "Delete gzip version"

  find .ssr-cache-to-recycle/ -maxdepth 9 -type f -name "*.gz" -delete

  echo "Patch html javascript and style hash"

  for file in $(find .ssr-cache-to-recycle/ -maxdepth 9 -type f -name "*.html"); do
    echo -e "\nTreat html file $file"
    sed -E -i "s/(<script src=\"runtime\.)[a-zA-Z0-9]+(\.js\")/\1$id_runtime\2/" "$file"
    sed -E -i "s/(<script src=\"polyfills\.)[a-zA-Z0-9]+(\.js\")/\1$id_polyfills\2/" "$file"
    sed -E -i "s/(<script src=\"scripts\.)[a-zA-Z0-9]+(\.js\")/\1$id_scripts\2/" "$file"
    sed -E -i "s/(<script src=\"main\.)[a-zA-Z0-9]+(\.js\")/\1$id_main\2/" "$file"
    sed -E -i "s/(<link rel=\"stylesheet\" href=\"styles\.)[a-zA-Z0-9]+(\.css\")/\1$id_styles\2/" "$file"
    echo "html patched"
    gzip -c -f -9 $file > $file.gz
    echo -e "gzip generated"
  done

  echo -e "\nRemove already existing folder '.ssr-cache'"

  rm -fr .ssr-cache

  echo "Rename .ssr-cache-to-recycle into '.ssr-cache'"

  mv .ssr-cache-to-recycle .ssr-cache

else
  echo "$INDEX_FILE does not exist, abort patch ssr-cache. Please provide a valid path to browser folder where index.html is present"
fi

echo "=== Patch SSR finished ==="